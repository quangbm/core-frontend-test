import axios from 'axios'
import { QueryFunctionContext } from 'react-query'

export type ID = {
  id: string
}

export type GUID = {
  guid: string
}

export type RequestParams = Partial<SearchParams & PaginationParams & SortParams>

export type SearchParams = {
  searchText?: string
}

export type PaginationParams = {
  limit?: number
  offset?: number
}

export enum SortOrder {
  Asc = 'ASC',
  Desc = 'DESC',
}

export type SortParams = {
  sortBy?: string
  sortOrder?: SortOrder
}

export enum PostAction {
  Create = 'create',
  Update = 'update',
  Delete = 'delete',
}

export type QueryContext<Params> = QueryFunctionContext<
  readonly [string, Params] | readonly [string, Params, string | undefined]
>

export function createRequest(baseURL: string, callback?: () => void) {
  let token: string | undefined = ''

  function setToken(newToken?: string) {
    token = newToken
  }

  function clearToken() {
    token = ''
  }

  function setBaseUrl(newBaseURL: string) {
    request.defaults.baseURL = newBaseURL
  }

  const request = axios.create({ baseURL })

  request.interceptors.request.use(configs => {
    return {
      ...configs,
      headers: {
        ...configs.headers,
        ...((token && {
          Authorization: `Bearer ${token}`,
        }) ||
          {}),
        'Content-Type': 'application/json; charset=utf-8',
      },
    }
  })

  request.interceptors.response.use(
    response => response,
    error => {
      if (error) {
        if (error.response && error.response.status === 401) {
          token = ''
          if (callback) {
            callback()
          }
        }
        throw error
      }
    },
  )

  return {
    setToken,
    clearToken,
    request,
    setBaseUrl,
  }
}
