import { AxiosError, AxiosInstance } from 'axios'
import { App } from './endpoints/app'
import { Auth } from './endpoints/auth'
import { Builder } from './endpoints/builder'
import { Currencies } from './endpoints/currencies'
import { Data } from './endpoints/data'
import { Events } from './endpoints/events'
import { Flows } from './endpoints/flows'
import { Integrations } from './endpoints/integrations'
import { Layout } from './endpoints/layout'
import { Me } from './endpoints/me'
import { Media } from './endpoints/media'
import { Metadata } from './endpoints/metadata'
import { ObjectRecordPages } from './endpoints/objRecordPages'
import { Organization } from './endpoints/organization'
import { PickLists } from './endpoints/pick-lists'
import { Profiles } from './endpoints/profiles'
import { Roles } from './endpoints/roles'
import { Session } from './endpoints/session'
import { SharingRules } from './endpoints/sharing-rules'
import { UploadFile } from './endpoints/upload-file'
import { Users } from './endpoints/users'

export function createApi(request: AxiosInstance) {
  const APIServices = Object.seal({
    App: App(request),
    Auth: Auth(request),
    Builder: Builder(request),
    Currencies: Currencies(request),
    Data: Data(request),
    Flows: Flows(request),
    Integrations: Integrations(request),
    Layout: Layout(request),
    Media: Media(request),
    Metadata: Metadata(request),
    ObjectRecordPages: ObjectRecordPages(request),
    Organization: Organization(request),
    PickLists: PickLists(request),
    Profiles: Profiles(request),
    Roles: Roles(request),
    Sessions: Session(request),
    UploadFile: UploadFile(request),
    Users: Users(request),
    SharingRules: SharingRules(request),
    Me: Me(request),
    Events: Events(request),
  })
  const QueryKeys = getKeys(APIServices)
  return {
    APIServices,
    QueryKeys,
  }
}

function getKeys<T>(data: T) {
  const result = {} as { [group in keyof T]: { [endpoint in keyof T[group]]: string } }
  for (const [group, endpoints] of Object.entries(data)) {
    // @ts-expect-error skip
    result[group] = {}
    for (const key of Object.keys(endpoints)) {
      // @ts-expect-error skip
      result[group][key] = `${group}.${key}`
    }
  }
  return result
}

export type ErrorResponse = AxiosError<{
  error?: string
  message: string
  statusCode: number
}>
