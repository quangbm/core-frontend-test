import { AxiosInstance } from 'axios'
import { LoginDto, LoginResponse } from '../models'

export function Auth(request: AxiosInstance) {
  return {
    login(dto: LoginDto) {
      return request.post<LoginResponse>('/v1/auth/login', dto)
    },
    logout() {
      return request.post<void>('/v1/auth/logout')
    },
  }
}
