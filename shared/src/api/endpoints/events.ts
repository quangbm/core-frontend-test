import { AxiosInstance, AxiosPromise } from 'axios'
import {
  EventCollection,
  EventCreateDto,
  EventDto,
  EventFieldDto,
  EventFieldResponse,
  EventResponse,
  EventUpdateDto,
  FieldResponse,
} from '../models'
import { PaginationParams, PostAction, QueryContext, SearchParams, SortParams } from '../request'

export function Events(request: AxiosInstance) {
  return {
    getEventList: ({
      queryKey,
    }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<EventCollection> => {
      const params = queryKey[1]
      return request.get('/v1/events', {
        params,
      })
    },
    getEvent: ({ queryKey }: QueryContext<{ name: string }>): AxiosPromise<EventResponse> => {
      const params = queryKey[1]
      return request.get(`/v1/events/${params.name}`)
    },
    postEvent: ({
      action,
      data,
      name,
    }: {
      data?: EventCreateDto | EventUpdateDto
      action: PostAction
      name?: string
    }): AxiosPromise<EventResponse> => {
      return request.post('/v1/events', {
        name,
        data,
        action,
      } as EventDto)
    },
    getEventFields: ({ queryKey }: QueryContext<{ eventName: string }>): AxiosPromise<EventFieldResponse[]> => {
      const params = queryKey[1]
      return request.get(`/v1/events/${params.eventName}/fields`)
    },
    getEventField: ({
      queryKey,
    }: QueryContext<{ eventName: string; fieldName: string }>): AxiosPromise<EventFieldResponse[]> => {
      const params = queryKey[1]
      return request.get(`/v1/events/${params.eventName}/fields/${params.fieldName}`)
    },
    postEventField: ({ eventName, body }: { eventName: string; body: EventFieldDto }): AxiosPromise<FieldResponse> => {
      return request.post(`/v1/events/${eventName}/fields`, body)
    },
  }
}
