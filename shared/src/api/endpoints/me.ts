import { AxiosInstance, AxiosPromise } from 'axios'
import { MeResponse } from '../models'

export function Me(request: AxiosInstance) {
  return {
    getMe: (): AxiosPromise<MeResponse> => {
      return request.get(`/v1/me`)
    },
    updateMe: (body: object): AxiosPromise<MeResponse> => {
      return request.put(`/v1/me`, body)
    },
  }
}
