import { AxiosInstance, AxiosPromise } from 'axios'
import { CtxFlowResponse, ObjectLayoutResponse } from '../models'
import { QueryContext } from '../request'

export function Layout(request: AxiosInstance) {
  return {
    getObjectLayout: ({ queryKey }: QueryContext<{ objName: string }>): AxiosPromise<ObjectLayoutResponse> => {
      return request.get(`/v1/layouts/${queryKey[1].objName}`)
    },
    getObjectMutationLayout: ({ queryKey }: QueryContext<{ objName: string }>): AxiosPromise<ObjectLayoutResponse> => {
      return request.get(`/v1/layouts/${queryKey[1].objName}/mutation`)
    },
    triggers({
      layoutId,
      componentId,
      ...body
    }: {
      layoutId: string
      componentId: string
    } & Record<string, any>): AxiosPromise<CtxFlowResponse | ObjectLayoutResponse> {
      return request.post(`/v1/triggers/${layoutId}/${componentId}`, body)
    },
  }
}
