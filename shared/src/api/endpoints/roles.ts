import { AxiosInstance, AxiosPromise } from 'axios'
import { CreateRoleDto, RoleResponse, UpdateRoleDto } from '../models'
import { QueryContext } from '../request'

export function Roles(request: AxiosInstance) {
  return {
    getRoles() {
      return request.get<RoleResponse[]>('/v1/roles')
    },
    createRole: (body: CreateRoleDto): AxiosPromise<RoleResponse> => {
      return request.post(`/v1/roles`, body)
    },
    getSingleRole({ queryKey }: QueryContext<{ roleName: string }>) {
      return request.get<RoleResponse>(`/v1/roles/${queryKey[1].roleName}`)
    },
    updateRole: (body: UpdateRoleDto & { roleName: string }): AxiosPromise<RoleResponse> => {
      const { roleName, ...restBody } = body
      return request.put(`/v1/roles/${roleName}`, restBody)
    },
    deleteRole: (roleName: string): AxiosPromise<RoleResponse> => {
      return request.delete(`/v1/roles/${roleName}`)
    },
  }
}
