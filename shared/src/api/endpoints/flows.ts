import { AxiosInstance, AxiosPromise } from 'axios'
import {
  ActionFlowItemResponse,
  AssignmentFlowItemResponse,
  AssignmentOperator,
  CollectionAggregationFlowItemResponse,
  CreateActionFlowItemDto,
  CreateAssignmentFlowItemDto,
  CreateCollectionAggregationFlowItemDto,
  CreateDecisionFlowItemDto,
  CreateFlowItemConnectionDto,
  CreateFlowVariableDto,
  CreateImportRecordsDto,
  CreateLoopFlowItemDto,
  CreateRecordCreateDto,
  CreateRecordDeleteDto,
  CreateRecordReadDto,
  CreateRecordUpdateDto,
  CreateScreenFlowItemDto,
  CreateSubflowItemDto,
  CreateUserCreateItemDto,
  DecisionFlowItemResponse,
  ExecutionContextLogResponse,
  FlowCollection,
  FlowCreateDto,
  FlowExecutionResponse,
  FlowInputVariableResponse,
  FlowItemConnectionResponse,
  FlowResponse,
  FlowTemplateCloneDto,
  FlowTemplateResponse,
  FlowTemplateWithDataResponse,
  FlowVariableResponse,
  FlowVariableSchemaResponse,
  ImportRecordsFlowItemResponse,
  LoopFlowItemResponse,
  RecordCreateFlowItemResponse,
  RecordDeleteFlowItemResponse,
  RecordReadFlowItemResponse,
  RecordUpdateFlowItemResponse,
  ScreenFlowItemResponse,
  ScreenTemplateResponse,
  SubflowFlowItemResponse,
  UpdateActionFlowItemDto,
  UpdateAssignmentFlowItemDto,
  UpdateCollectionAggregationFlowItemDto,
  UpdateDataFlowDto,
  UpdateDecisionFlowItemDto,
  UpdateFlowVariableDto,
  UpdateImportRecordsDto,
  UpdateLoopFlowItemDto,
  UpdateRecordCreateDto,
  UpdateRecordDeleteDto,
  UpdateRecordReadDto,
  UpdateRecordUpdateDto,
  UpdateScheduleFlowDto,
  UpdateScreenFlowDto,
  UpdateScreenFlowItemDto,
  UpdateSubflowItemDto,
  UpdateUserCreateItemDto,
  UserCreateFlowItemResponse,
} from '../models'
import { PaginationParams, QueryContext, RequestParams, SearchParams, SortParams } from '../request'

export type BaseCreateFlowItemDto =
  | CreateActionFlowItemDto
  | CreateAssignmentFlowItemDto
  | CreateDecisionFlowItemDto
  | CreateLoopFlowItemDto
  | CreateRecordCreateDto
  | CreateRecordDeleteDto
  | CreateRecordReadDto
  | CreateRecordUpdateDto
  | CreateScreenFlowItemDto
  | CreateSubflowItemDto
  | CreateImportRecordsDto
  | CreateUserCreateItemDto
  | CreateCollectionAggregationFlowItemDto

export type BaseUpdateFlowItemDto =
  | UpdateActionFlowItemDto
  | UpdateAssignmentFlowItemDto
  | UpdateDecisionFlowItemDto
  | UpdateLoopFlowItemDto
  | UpdateRecordCreateDto
  | UpdateRecordDeleteDto
  | UpdateRecordReadDto
  | UpdateRecordUpdateDto
  | UpdateScreenFlowItemDto
  | UpdateSubflowItemDto
  | UpdateImportRecordsDto
  | UpdateUserCreateItemDto
  | UpdateCollectionAggregationFlowItemDto

export type FlowType = 'screen' | 'action' | 'time-based' | 'data' | 'event'

export type FlowItem =
  | ScreenFlowItemResponse
  | SubflowFlowItemResponse
  | DecisionFlowItemResponse
  | AssignmentFlowItemResponse
  | ActionFlowItemResponse
  | LoopFlowItemResponse
  | RecordCreateFlowItemResponse
  | RecordReadFlowItemResponse
  | RecordUpdateFlowItemResponse
  | RecordDeleteFlowItemResponse
  | ImportRecordsFlowItemResponse
  | UserCreateFlowItemResponse
  | CollectionAggregationFlowItemResponse

export function Flows(request: AxiosInstance) {
  return {
    // screen flow
    getScreenFlows({ queryKey }: QueryContext<RequestParams>) {
      return request.get<FlowCollection>('/v1/flow-builder/screen-flows', { params: queryKey[1] })
    },
    startScreenFlow: ({ flowId }: { flowId: string }): AxiosPromise<FlowExecutionResponse> => {
      return request.post('/v1/flow-runner/screen', { flowId })
    },
    getScreenFlowLayout: ({ queryKey }: QueryContext<{ ctxId: string }>): AxiosPromise<ScreenTemplateResponse> => {
      const { ctxId } = queryKey[1]
      return request.get(`/v1/flow-runner/screen/${ctxId}`)
    },
    submitScreenFlowData: ({ ctxId, body }: { ctxId: string; body?: Record<string, any> }): AxiosPromise<void> => {
      return request.post(`/v1/flow-runner/screen/${ctxId}`, body)
    },

    // flow items
    getFlowTemplateItems: ({
      queryKey,
    }: QueryContext<{ flowName: string; version: string }>): AxiosPromise<FlowItem[]> => {
      return request.get(`/v1/flows/${queryKey[1].flowName}/templates/${queryKey[1].version}/items`)
    },
    getFlowTemplateItem: ({
      queryKey,
    }: QueryContext<{ flowName: string; flowItemName: string; version: string }>): AxiosPromise<FlowItem> => {
      return request.get(
        `/v1/flows/${queryKey[1].flowName}/templates/${queryKey[1].version}/items/${queryKey[1].flowItemName}`,
      )
    },
    createFlowTemplateItem: ({
      flowName,
      version,
      data,
    }: {
      flowName: string
      version: string
      data: BaseCreateFlowItemDto
    }): AxiosPromise<FlowItem> => {
      return request.post(`/v1/flows/${flowName}/templates/${version}/items`, data)
    },
    updateFlowTemplateItem: ({
      flowName,
      version,
      flowItemName,
      data,
    }: {
      flowName: string
      version: string
      flowItemName: string
      data: BaseUpdateFlowItemDto
    }): AxiosPromise<FlowItem> => {
      return request.put(`/v1/flows/${flowName}/templates/${version}/items/${flowItemName}`, data)
    },
    deleteFlowTemplateItem: ({
      flowName,
      version,
      flowItemName,
    }: {
      flowName: string
      version: string
      flowItemName: string
    }): AxiosPromise<void> => {
      return request.delete(`/v1/flows/${flowName}/templates/${version}/items/${flowItemName}`)
    },

    // sub flows
    getSubFlows: ({
      queryKey,
    }: QueryContext<
      { flowName: string } & SearchParams & PaginationParams & SortParams
    >): AxiosPromise<FlowCollection> => {
      return request.get(`/v1/flow-builder/${queryKey[1].flowName}/sub-flows`, {
        params: { searchText: queryKey[1].searchText },
      })
    },

    getFlowInputVariables: ({
      queryKey,
    }: QueryContext<{ flowName: string; version: string }>): AxiosPromise<FlowInputVariableResponse[]> => {
      return request.get(`/v1/flows/${queryKey[1].flowName}/templates/${queryKey[1].version}/input-variables`)
    },

    //
    getFlowsList: ({
      queryKey,
    }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<FlowCollection> => {
      return request.get('/v1/flows', { params: queryKey[1] })
    },
    createFlow: (args: FlowCreateDto): AxiosPromise<FlowResponse> => {
      return request.post('/v1/flows', args)
    },
    getFlow: ({ queryKey }: QueryContext<{ flowName: string }>): AxiosPromise<FlowResponse> => {
      return request.get(`/v1/flows/${queryKey[1].flowName}`)
    },
    //
    getFlowTemplates: ({ queryKey }: QueryContext<{ flowName: string }>): AxiosPromise<FlowTemplateResponse[]> => {
      return request.get(`/v1/flows/${queryKey[1].flowName}/templates`)
    },
    getFlowTemplateVersion: ({
      queryKey,
    }: QueryContext<{ flowName: string; version: string }>): AxiosPromise<FlowTemplateWithDataResponse> => {
      return request.get(`/v1/flows/${queryKey[1].flowName}/templates/${queryKey[1].version}`)
    },
    updateFlowTemplate: (params: {
      flowName: string
      version: string
      data: UpdateDataFlowDto | UpdateScreenFlowDto | UpdateScheduleFlowDto
    }): AxiosPromise<FlowTemplateResponse> => {
      const { flowName, data, version } = params
      return request.put(`/v1/flows/${flowName}/templates/${version}`, data)
    },
    cloneFlowTemplate: (params: {
      flowName: string
      version: string
      data: FlowTemplateCloneDto
    }): AxiosPromise<FlowTemplateResponse> => {
      const { flowName, data, version } = params
      return request.post(`/v1/flows/${flowName}/templates/${version}/clone`, data)
    },
    //
    activateFlowTemplate: ({
      flowName,
      version,
    }: {
      flowName: string
      version: string
    }): AxiosPromise<FlowResponse> => {
      return request.post(`/v1/flows/${flowName}/templates/${version}/activate`)
    },
    deactivateFlowTemplate: ({
      flowName,
      version,
    }: {
      flowName: string
      version: string
    }): AxiosPromise<FlowResponse> => {
      return request.post(`/v1/flows/${flowName}/templates/${version}/deactivate`)
    },
    //
    getFlowTemplateConnections: ({
      queryKey,
    }: QueryContext<{ flowName: string; version: string }>): AxiosPromise<FlowItemConnectionResponse[]> => {
      return request.get(`/v1/flows/${queryKey[1].flowName}/templates/${queryKey[1].version}/connections`)
    },
    createFlowTemplateConnection: ({
      flowName,
      version,
      ...body
    }: {
      flowName: string
      version: string
    } & CreateFlowItemConnectionDto): AxiosPromise<FlowItemConnectionResponse> => {
      return request.post(`/v1/flows/${flowName}/templates/${version}/connections`, body)
    },
    deleteFlowTemplateConnection: ({
      flowName,
      version,
      id,
    }: {
      flowName: string
      version: string
      id: string
    }): AxiosPromise<void> => {
      return request.delete(`/v1/flows/${flowName}/templates/${version}/connections/${id}`)
    },
    // flow variables
    getFlowTemplateVariables: ({
      queryKey,
    }: QueryContext<{ flowName: string; version: string }>): AxiosPromise<FlowVariableResponse[]> => {
      const { flowName, version } = queryKey[1]
      return request.get(`/v1/flows/${flowName}/templates/${version}/variables`)
    },
    createFlowVariables: ({
      flowName,
      version,
      body,
    }: {
      flowName: string
      version: string
      body: CreateFlowVariableDto
    }): AxiosPromise<FlowVariableResponse> => {
      return request.post(`/v1/flows/${flowName}/templates/${version}/variables`, body)
    },
    updateFlowVariables: ({
      flowName,
      version,
      varName,
      body,
    }: {
      flowName: string
      version: string
      varName: string
      body: UpdateFlowVariableDto
    }): AxiosPromise<FlowVariableResponse> => {
      return request.put(`/v1/flows/${flowName}/templates/${version}/variables/${varName}`, body)
    },
    deleteFlowVariables: ({
      flowName,
      varName,
      version,
    }: {
      flowName: string
      varName: string
      version: string
    }): AxiosPromise<void> => {
      return request.delete(`/v1/flows/${flowName}/templates/${version}/variables/${varName}`)
    },
    getFlowVariableSchema: ({
      queryKey,
    }: QueryContext<{ flowName: string; variable: string; version: string }>): AxiosPromise<
      FlowVariableSchemaResponse[]
    > => {
      const { flowName, variable, version } = queryKey[1]
      return request.get(`/v1/flows/${flowName}/templates/${version}/variables/${variable}/schema`)
    },
    //
    getAssignmentOperations: (): AxiosPromise<Record<string, AssignmentOperator[]>> => {
      return request.get(`/v1/flow-builder/assignment-operators`)
    },
    getConditionOperators: (): AxiosPromise<string[]> => {
      return request.get(`/v1/flow-builder/condition-operators`)
    },
    //
    debugFlow: ({ queryKey }: QueryContext<{ flowName: string }>): AxiosPromise<ExecutionContextLogResponse> => {
      return request.get(`/v1/flow-builder/${queryKey[1].flowName}/debug-logs`)
    },
  }
}
