import { AxiosInstance, AxiosPromise, AxiosRequestConfig } from 'axios'
import { MediaResponse } from '../models'

export function UploadFile(request: AxiosInstance) {
  return {
    uploadFile(
      { name, formData }: { name: string; formData: FormData },
      options?: AxiosRequestConfig,
    ): AxiosPromise<MediaResponse> {
      return request.post(`/v1/upload-file/${name}`, formData, options)
    },
  }
}
