import { AxiosInstance, AxiosPromise } from 'axios'
import { ObjRecordLayoutResponse } from '../models'
import { QueryContext } from '../request'

export function ObjectRecordPages(request: AxiosInstance) {
  return {
    getObjectRecordPages: ({
      queryKey,
    }: QueryContext<{ objName: string; fieldName: string }>): AxiosPromise<ObjRecordLayoutResponse[]> => {
      const { objName, fieldName } = queryKey[1]
      return request.get(`/v1/obj-record-pages/${objName}/${fieldName}`)
    },
  }
}
