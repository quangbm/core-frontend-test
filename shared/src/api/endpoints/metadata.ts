import { AxiosInstance, AxiosPromise } from 'axios'
import {
  FieldDto,
  FieldResponse,
  ObjectCollection,
  ObjectCreateDto,
  ObjectDto,
  ObjectResponse,
  ObjectUpdateDto,
  RuleCreateDto,
  RuleDto,
  RuleUpdateDto,
  ValidationRuleResponse,
} from '../models'
import { PaginationParams, PostAction, QueryContext, SearchParams, SortParams } from '../request'

export function Metadata(request: AxiosInstance) {
  return {
    getObjects: ({
      queryKey,
    }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<ObjectCollection> => {
      const params = queryKey[1]

      return request.get('/v1/mo', {
        params,
      })
    },
    getObject: ({ queryKey }: QueryContext<{ name: string }>): AxiosPromise<ObjectResponse> => {
      const params = queryKey[1]
      return request.get(`/v1/mo/${params.name}`)
    },
    postObject: ({
      action,
      data,
      name,
    }: {
      data?: ObjectCreateDto | ObjectUpdateDto
      action: PostAction
      name?: string
    }): AxiosPromise<ObjectResponse> => {
      return request.post('/v1/mo', {
        name,
        data,
        action,
      } as ObjectDto)
    },
    getObjectFields: ({ queryKey }: QueryContext<{ name: string }>): AxiosPromise<FieldResponse[]> => {
      const params = queryKey[1]
      return request.get(`/v1/mo/${params.name}/fields`)
    },
    postObjectField: ({ objName, body }: { objName: string; body: FieldDto }): AxiosPromise<FieldResponse> => {
      return request.post(`/v1/mo/${objName}/fields`, body)
    },
    getObjectRules: ({ queryKey }: QueryContext<{ name: string }>): AxiosPromise<ValidationRuleResponse[]> => {
      const params = queryKey[1]
      return request.get(`/v1/mo/${params.name}/rules`)
    },
    postObjectRule: ({
      action,
      data,
      objName,
      ruleName,
    }: {
      data?: RuleCreateDto | RuleUpdateDto
      action: PostAction
      objName?: string
      ruleName?: string
    }): AxiosPromise<ValidationRuleResponse> => {
      return request.post(`/v1/mo/${objName}/rules`, {
        name: ruleName,
        data,
        action,
      } as RuleDto)
    },
    getObjectRule: ({
      queryKey,
    }: QueryContext<{ objName: string; ruleName: string }>): AxiosPromise<ValidationRuleResponse> => {
      const params = queryKey[1]
      return request.get(`/v1/mo/${params.objName}/rules/${params.ruleName}`)
    },
  }
}
