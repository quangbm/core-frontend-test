import { AxiosInstance, AxiosPromise } from 'axios'
import { OrganizationResponse, UpdateOrgDto } from '../models'

type LocalesResponse = { [key: string]: string }

export function Organization(request: AxiosInstance) {
  return {
    getOrganization(): AxiosPromise<OrganizationResponse> {
      return request.get('/v1/organization')
    },
    updateOrganization(body: UpdateOrgDto): AxiosPromise<OrganizationResponse> {
      return request.put('/v1/organization', body)
    },
    getLocales(): AxiosPromise<LocalesResponse> {
      return request.get('/v1/organization/locales')
    },
  }
}
