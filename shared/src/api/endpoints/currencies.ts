import { AxiosInstance } from 'axios'
import {
  AvailableCurrencyResponse,
  CreateOrgCurrencyDto,
  CurrencyRateCollection,
  OrganizationCurrencyCollection,
  OrganizationCurrencyResponse,
  UpdateOrgCurrencyDto,
} from '../models'
import { PaginationParams, QueryContext, RequestParams, SortParams } from '../request'

type CurrencyCode = { currencyCode: string }

export function Currencies(request: AxiosInstance) {
  return {
    getCurrenciesList({ queryKey: [, params] }: QueryContext<RequestParams>) {
      return request.get<OrganizationCurrencyCollection>('/v1/currencies', { params })
    },
    getCurrency({ queryKey: [, params] }: QueryContext<CurrencyCode>) {
      return request.get<OrganizationCurrencyResponse>(`/v1/currencies/${params.currencyCode}`)
    },
    createCurrency(body: CreateOrgCurrencyDto) {
      return request.post<OrganizationCurrencyResponse>(`/v1/currencies`, body)
    },
    updateCurrency({ currencyCode, ...body }: CurrencyCode & UpdateOrgCurrencyDto) {
      return request.put<OrganizationCurrencyResponse>(`/v1/currencies/${currencyCode}`, body)
    },
    getCurrencyRates({ queryKey: [, params] }: QueryContext<CurrencyCode & PaginationParams & SortParams>) {
      const { currencyCode, ...body } = params
      return request.get<CurrencyRateCollection>(`/v1/currencies/${currencyCode}/rate`, { params: body })
    },
    getAvailableCurrencies() {
      return request.get<AvailableCurrencyResponse[]>('/v1/available-currencies')
    },
  }
}
