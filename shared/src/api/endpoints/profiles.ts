import { AxiosInstance, AxiosPromise } from 'axios'
import {
  CreateProfileDto,
  ProfileAccessResponse,
  ProfileCollection,
  ProfileResponse,
  UpdateProfileDto,
  UpsertProfileAccessDto,
  UpsertProfileFeatureDto,
} from '../models'
import { PaginationParams, QueryContext, SearchParams, SortParams } from '../request'

type Id = {
  id: string
}

export function Profiles(request: AxiosInstance) {
  return {
    // profile - object permission
    getProfileObjects: ({ queryKey }: QueryContext<Id>): AxiosPromise<Record<string, ProfileAccessResponse>> => {
      const { id } = queryKey[1]
      return request.get(`/v1/profile-object/${id}`)
    },
    updateProfileObjects: ({
      id,
      items,
    }: Id & UpsertProfileAccessDto): AxiosPromise<Record<string, ProfileAccessResponse>> => {
      return request.put(`/v1/profile-object/${id}`, { items })
    },
    // profile CRUD
    getProfiles: ({
      queryKey,
    }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<ProfileCollection> => {
      return request.get('/v1/profiles', { params: queryKey[1] })
    },
    getProfile: ({ queryKey }: QueryContext<Id>): AxiosPromise<ProfileResponse> => {
      const { id } = queryKey[1]
      return request.get(`/v1/profiles/${id}`)
    },
    createProfile: (body: CreateProfileDto): AxiosPromise<ProfileResponse> => {
      return request.post(`/v1/profiles`, body)
    },
    updateProfile: ({ id, ...body }: Id & UpdateProfileDto): AxiosPromise<ProfileResponse> => {
      return request.put(`/v1/profiles/${id}`, body)
    },
    deleteProfile: ({ id }: Id): AxiosPromise<ProfileResponse> => {
      return request.delete(`/v1/profiles/${id}`)
    },
    // profile - feature permission
    getProfileFeatures: ({ queryKey }: QueryContext<Id>): AxiosPromise<Record<string, ProfileAccessResponse>> => {
      const { id } = queryKey[1]
      return request.get(`/v1/profile-feature/${id}`)
    },
    updateProfileFeatures: ({
      id,
      items,
    }: Id & UpsertProfileFeatureDto): AxiosPromise<Record<string, ProfileAccessResponse>> => {
      return request.put(`/v1/profile-feature/${id}`, { items })
    },
  }
}
