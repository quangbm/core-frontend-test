import { AxiosInstance } from 'axios'
import { SessionResponse, UpdateSessionDto } from '../models'

export function Session(request: AxiosInstance) {
  return {
    getSession() {
      return request.get<SessionResponse>('/v1/sessions')
    },
    updateSession(dto: UpdateSessionDto) {
      return request.put<SessionResponse>('/v1/sessions/', dto)
    },
  }
}
