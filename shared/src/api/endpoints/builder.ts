import { AxiosInstance } from 'axios'
import { QueryFunctionContext } from 'react-query'
import {
  AppBuilderLayoutResponse,
  AppBuilderPageResponse,
  ApplicationCollection,
  ApplicationResponse,
  AppProfileLayoutResponse,
  AppProfileResponse,
  AssignAppPageToAppsDto,
  AssignProfileToAppDto,
  AssignToAppAndProfileDto,
  ComponentResponse,
  CreateApplicationDto,
  CreateLayoutDto,
  DataTypeExtendedResponse,
  LayoutCollection,
  LayoutResponse,
  ListFilterOperator,
  ObjectCollection,
  ObjectSchemaResponse,
  TriggerFlowResponse,
  TriggerObjectResponse,
  UpdateApplicationDto,
  UpdateAppPageOrderDto,
  UpdateLayoutDto,
  UpdateScriptDto,
} from '../models'
import { ID, PaginationParams, QueryContext, RequestParams, SearchParams } from '../request'

export function Builder(request: AxiosInstance) {
  return {
    getLayouts(context: QueryContext<RequestParams>) {
      const [, params] = context.queryKey
      return request.get<LayoutCollection>(`/v1/builder/layouts`, { params })
    },
    getLayout({ queryKey: [, params] }: QueryContext<{ id: string }>) {
      return request.get<LayoutResponse>(`/v1/builder/layouts/${params.id}`)
    },
    postLayout(body: CreateLayoutDto) {
      return request.post<LayoutResponse>(`/v1/builder/layouts`, body)
    },
    putLayout({ id, ...body }: ID & UpdateLayoutDto) {
      return request.put<LayoutResponse>(`/v1/builder/layouts/${id}`, body)
    },
    getLayoutScript({ queryKey: [, params] }: QueryContext<ID>) {
      return request.get<Record<string, ComponentResponse>>(`/v1/builder/layouts/${params.id}/script`)
    },
    putLayoutScript({ id, ...body }: ID & UpdateScriptDto) {
      return request.put<Record<string, ComponentResponse>>(`/v1/builder/layouts/${id}/script`, body)
    },
    // assign stuff
    getLayoutApps({ queryKey: [, params] }: QueryContext<ID>) {
      return request.get<Array<ApplicationResponse>>(`/v1/builder/layouts/${params.id}/apps`)
    },
    assignAppPageToApps({ id, ...body }: ID & AssignAppPageToAppsDto) {
      return request.post<Array<ApplicationResponse>>(`/v1/builder/layouts/${id}/apps`, body)
    },
    assignDefaultRecord({ id }: ID) {
      return request.post<void>(`/v1/builder/layouts/${id}/object-default`)
    },
    getLayoutAppsProfiles({ queryKey: [, params] }: QueryContext<ID>) {
      return request.get<Array<AppProfileLayoutResponse>>(`/v1/builder/layouts/${params.id}/apps-profiles`)
    },
    assignLayoutAppsProfiles({ id, ...body }: ID & AssignToAppAndProfileDto) {
      return request.post<Array<AppProfileLayoutResponse>>(`/v1/builder/layouts/${id}/apps-profiles`, body)
    },
    // App builder
    getApps({ queryKey }: QueryContext<RequestParams>) {
      const params = queryKey[1]
      return request.get<ApplicationCollection>(`/v1/builder/apps`, { params })
    },
    getApp({ queryKey: [, params] }: QueryContext<{ id: string }>) {
      return request.get<ApplicationResponse>(`/v1/builder/apps/${params.id}`)
    },
    createApp(body: CreateApplicationDto) {
      return request.post<ApplicationResponse>(`/v1/builder/apps`, body)
    },
    updateApp({ id, ...body }: { id: string } & UpdateApplicationDto) {
      return request.put<ApplicationResponse>(`/v1/builder/apps/${id}`, body)
    },
    getAppPages({ queryKey: [, params] }: QueryContext<{ id: string }>) {
      return request.get<AppBuilderPageResponse>(`/v1/builder/apps/${params.id}/pages`)
    },
    orderPages({ id, ...body }: { id: string } & UpdateAppPageOrderDto) {
      return request.post<Array<AppBuilderLayoutResponse>>(`/v1/builder/apps/${id}/order-pages`, body)
    },
    // app profiles
    getAppProfiles({ queryKey: [, params] }: QueryContext<{ id: string }>) {
      return request.get<Array<AppProfileResponse>>(`/v1/builder/apps/${params.id}/profiles`)
    },
    updateAppProfiles({ id, ...body }: { id: string } & AssignProfileToAppDto) {
      return request.post<void>(`/v1/builder/apps/${id}/profiles`, body)
    },
    // all object api
    getAllObjects({ queryKey }: QueryContext<SearchParams & PaginationParams>) {
      const params = queryKey[1]
      return request.get<ObjectCollection>('/v1/builder/obj', {
        params,
      })
    },
    getObjectSchema({ queryKey }: QueryContext<{ objName: string }>) {
      const { objName } = queryKey[1]
      return request.get<ObjectSchemaResponse>(`/v1/builder/obj/${objName}/schema`)
    },
    getObjectDataTypes({ queryKey }: QueryContext<{ objName: string }>) {
      return request.get<Array<DataTypeExtendedResponse>>(`/v1/builder/data-types/${queryKey[1].objName}`)
    },
    getFilterOperators(_params: QueryFunctionContext) {
      return request.get<Record<string, Array<ListFilterOperator>>>(`/v1/obj/filter-operators`)
    },
    getTriggers({ queryKey }: QueryContext<{ id: string }>) {
      const { id } = queryKey[1]
      return request.get<Array<TriggerObjectResponse | TriggerFlowResponse>>(`/v1/builder/layouts/${id}/triggers`)
    },
  }
}
