import { AxiosInstance, AxiosPromise } from 'axios'
import { InviteUserDto, UpdateUserDto, UserCollection, UserResponse } from '../models'
import { PaginationParams, QueryContext, SearchParams, SortParams } from '../request'

export function Users(request: AxiosInstance) {
  return {
    getUsers: ({
      queryKey,
    }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<UserCollection> => {
      return request.get('/v1/users', { params: queryKey[1] })
    },
    getUser: ({ queryKey }: QueryContext<{ userId: string }>): AxiosPromise<UserResponse> => {
      return request.get(`/v1/users/${queryKey[1].userId}`)
    },
    activateUser: ({ userId }: { userId: string }): AxiosPromise<UserResponse> => {
      return request.post(`/v1/users/${userId}/activate`)
    },
    deactivateUser: ({ userId }: { userId: string }): AxiosPromise<void> => {
      return request.post(`/v1/users/${userId}/deactivate`)
    },
    inviteUser: (body: InviteUserDto): AxiosPromise<void> => {
      return request.post(`/v1/users/invite`, body)
    },
    updateUser: ({ userId, ...body }: { userId: string } & UpdateUserDto): AxiosPromise<void> => {
      return request.put(`/v1/users/${userId}`, body)
    },
  }
}
