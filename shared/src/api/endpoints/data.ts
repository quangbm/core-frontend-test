import { AxiosInstance } from 'axios'
import { DataCollection, DataResponse, ListRecordsQueryWithFilters, ManagedByResponse } from '../models'
import { GUID, PaginationParams, QueryContext, RequestParams, SearchParams, SortParams } from '../request'

export function Data(request: AxiosInstance) {
  return {
    getObjectData({ queryKey }: QueryContext<{ name: string } & RequestParams>) {
      const { name, ...params } = queryKey[1]
      return request.get<DataCollection>(`/v1/data/${name}`, {
        params,
      })
    },
    getSingleObjectData({ queryKey }: QueryContext<{ name: string } & GUID>) {
      const params = queryKey[1]
      return request.get<DataResponse>(`/v1/data/${params.name}/${params.guid}`)
    },
    createObjectData({ name, body }: { name: string; body: object }) {
      return request.post<DataResponse>(`/v1/data/${name}`, body)
    },
    updateObjectData({ name, guid, body }: { name: string; body: object } & GUID) {
      return request.put<DataResponse>(`/v1/data/${name}/${guid}`, body)
    },
    deleteObjectData({ name, guid }: { name: string } & GUID) {
      return request.delete<DataResponse>(`/v1/data/${name}/${guid}`)
    },
    getObjectRelatedData({
      queryKey,
    }: QueryContext<{ name: string; relation: string } & GUID & SearchParams & PaginationParams & SortParams>) {
      const { name, guid, relation, ...params } = queryKey[1]
      return request.get<DataCollection>(`/v1/data/${name}/${guid}/${relation}`, { params })
    },
    searchObjectData({ queryKey }: QueryContext<{ name: string } & ListRecordsQueryWithFilters>) {
      const { name, ...body } = queryKey[1]
      return request.post<DataCollection>(`/v1/data/${name}/search`, body)
    },
    searchObjectRelatedData({
      queryKey,
    }: QueryContext<{ name: string; relation: string } & GUID & ListRecordsQueryWithFilters>) {
      const { name, guid, relation, ...body } = queryKey[1]
      return request.post<DataCollection>(`/v1/data/${name}/${guid}/${relation}/search`, body)
    },
    setManagedBy(params: { objName: string; guids: string[]; managedBy: string }) {
      const { objName, ...restBody } = params
      return request.post<ManagedByResponse[]>(`/v1/data/${objName}/managed-by`, restBody)
    },
  }
}
