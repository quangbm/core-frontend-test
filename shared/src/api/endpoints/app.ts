import { AxiosInstance } from 'axios'
import { ApplicationCollection, AppViewerResponse, ComponentResponse, LayoutResponse } from '../models'
import { PaginationParams, QueryContext, SearchParams, SortParams } from '../request'

export function App(request: AxiosInstance) {
  return {
    getApps(context: QueryContext<SearchParams & PaginationParams & SortParams>) {
      const [, params] = context.queryKey
      return request.get<ApplicationCollection>('/v1/apps', { params })
    },
    getAppPages(context: QueryContext<{ id: string }>) {
      const [, { id }] = context.queryKey
      return request.get<LayoutResponse[]>(`/v1/apps/${id}/pages`)
    },
    getAppScript(context: QueryContext<{ layoutId: string }>) {
      const [, { layoutId }] = context.queryKey
      return request.get<Record<string, ComponentResponse>>(`/v1/app-pages/${layoutId}`)
    },
    getApp(context: QueryContext<{ id: string }>) {
      const [, { id }] = context.queryKey
      return request.get<AppViewerResponse>(`/v1/apps/${id}`)
    },
  }
}
