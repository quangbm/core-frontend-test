import { AxiosInstance, AxiosPromise } from 'axios'
import { CreateSharingRuleDto, SharingRuleResponse, UpdateRoleDto } from '../models'
import { QueryContext } from '../request'

export function SharingRules(request: AxiosInstance) {
  return {
    getSharingRuleList() {
      return request.get<SharingRuleResponse[]>('/v1/sharing-rules')
    },
    createSharingRule: (body: CreateSharingRuleDto): AxiosPromise<SharingRuleResponse> => {
      return request.post(`/v1/sharing-rules`, body)
    },
    getSharingRule({ queryKey }: QueryContext<{ name: string }>) {
      return request.get<SharingRuleResponse>(`/v1/sharing-rules/${queryKey[1].name}`)
    },
    updateSharingRule: (body: UpdateRoleDto & { name: string }): AxiosPromise<SharingRuleResponse> => {
      const { name, ...restBody } = body
      return request.put(`/v1/sharing-rules/${name}`, restBody)
    },
    deleteSharingRule: (name: string): AxiosPromise<SharingRuleResponse> => {
      return request.delete(`/v1/sharing-rules/${name}`)
    },
  }
}
