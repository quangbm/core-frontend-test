import { AxiosInstance, AxiosPromise } from 'axios'
import { MediaCollection, MediaResponse, UpdateMediaDto } from '../models'
import { PaginationParams, QueryContext, SearchParams, SortParams } from '../request'

export function Media(request: AxiosInstance) {
  return {
    getAllMedia({
      queryKey,
    }: QueryContext<SearchParams & PaginationParams & SortParams>): AxiosPromise<MediaCollection> {
      const params = queryKey[1]
      return request.get(`/v1/media`, { params })
    },
    getMedia({ queryKey }: QueryContext<{ id: string }>): AxiosPromise<MediaResponse> {
      return request.get(`/v1/media/${queryKey[1].id}`)
    },
    updateMedia({ id, ...body }: { id: string } & UpdateMediaDto): AxiosPromise<MediaResponse> {
      return request.put(`/v1/media/${id}`, body)
    },
  }
}
