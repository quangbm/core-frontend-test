/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface BundleCredentialResponse {
  name: string;
  displayName: string;
}

export interface HeaderInputType {
  isRequired: boolean;
  default?: string;
  description?: string;
}

export interface RequestInputType {
  type: "string" | "number" | "object" | "array" | "boolean";
  default?: string | number | object | boolean | (string | number | object | boolean)[];
  isRequired: boolean;
  description?: string;
}

export interface InputMetadata {
  header: Record<string, HeaderInputType>;
  query: Record<string, RequestInputType>;
  path: string[];
  body?: RequestInputType;
}

export interface ActionMetadataResponse {
  outputs: Record<string, ResponseOutputType>;
  isStream: boolean;
  credential: BundleCredentialResponse;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  id: string;
  path: string;
  method: "post" | "get" | "put" | "delete";
  inputs: InputMetadata;
}

export interface PageInfoType {
  limit: number;
  offset: number;
  total: number;
}

export interface ActionMetadataCollection {
  data: ActionMetadataResponse[];
  pageInfo: PageInfoType;
}

export interface ResponseOutputType {
  type: "string" | "number" | "object" | "array" | "boolean";
  description?: string;
}

export interface InputMapDto {
  header: Record<string, HeaderInputType>;
  query: Record<string, RequestInputType>;
  body?: RequestInputType;
}

export interface ActionMetadataCreateDto {
  outputs?: Record<string, ResponseOutputType>;
  name: string;
  displayName: string;
  description?: string;
  credentialName: string;
  path: string;
  method: "post" | "get" | "put" | "delete";
  inputs?: InputMapDto;
}

export interface TestActionMetadataResponse {
  headers?: Record<string, string>;
  data?: string | number | object | boolean | (string | number | object | boolean)[];
  url?: string;
  error?: string;
  status?: number;
}

export interface ActionMetadataUpdateDto {
  outputs?: Record<string, ResponseOutputType>;
  displayName?: string;
  description?: string;
  credentialName?: string;
  path?: string;
  method?: "post" | "get" | "put" | "delete";
  inputs?: InputMapDto;
}

export interface LoginDto {
  username: string;
  password: string;
}

export interface LoginResponse {
  accessToken: string;
}

export interface RegisterDto {
  username: string;
  password: string;
  profileName: string;
  roleName: string;
  userInfo?: object;
}

export interface UserRegistrationResponse {
  id: string;
}

export interface SessionResponse {
  id: string;
  lastUsedAppId?: string;
  lastUrl?: string;
  defaultCurrencyCode: object;
  defaultLocale: string;
  userId: string;
}

export interface UpdateSessionDto {
  lastUsedAppId?: string;
  lastUrl?: string;
}

export type ProfileAccessResponseMap = object;

export interface MeProfileResponse {
  /** Map<[objectName]: ProfileAccessResponse> */
  objectsAccess: ProfileAccessResponseMap;

  /** Map<[objectName]: ProfileAccessResponse> */
  featuresAccess: ProfileAccessResponseMap;
  name: string;
  description: string;
}

export interface MeRoleResponse {
  name: string;
  displayName: string;
}

export interface RelatedDataField {
  objName: string;

  /** Display content for relation selection box */
  name: string;
}

export interface FilterItem {
  fieldName: string;
  operator: "===" | "!==" | "isIn" | "isNotIn" | "contains" | "notContains" | "<=" | ">=" | "<" | ">";
  value: object;
}

export interface FieldSchema {
  filters?: FilterItem[][];

  /** Use with relation field. 'SET_NULL' requires 'isRequired'=false */
  onDelete?: "noAction" | "setNull" | "cascade";

  /** Currently applied for short text only. 'Partial' will only expose the last 4 characters. 'All' will hide everything & return '****' */
  sensitivity?: "none" | "partial" | "all";
  name: string;
  displayName: string;
  description?: string;
  typeName:
    | "numeric"
    | "text"
    | "dateTime"
    | "boolean"
    | "pickList"
    | "json"
    | "generated"
    | "currency"
    | "externalRelation"
    | "indirectRelation"
    | "relation";
  subType?: string;
  value?: string | string[];
  targetObjId?: number;
  isRequired: boolean;
  defaultValue?: string;
  isUnique?: boolean;
  isSearchable?: boolean;
  isSortable?: boolean;
  isReadOnly?: boolean;
}

export interface ObjectSchemaResponse {
  id: number;
  name: string;
  displayName: string;
  description?: string;
  fields: FieldSchema[];
  related: Record<string, RelatedDataField>;
  pickList?: Record<string, PickListItemResponse[]>;
}

export interface MeUserInfo {
  schema: ObjectSchemaResponse;
  data: object;
}

export interface MeResponse {
  profile: MeProfileResponse;
  role: MeRoleResponse;
  userInfo: MeUserInfo;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  ssoId: string;
  isActive: boolean;
}

export interface FieldAttributeMeta {
  displayName: string;
  options?: string[];
}

export interface AttributeType {
  subTypes: string[];
}

export interface DataTypeExtendedResponse {
  availableAttributes: Record<string, FieldAttributeMeta>;
  name: string;
  displayName: string;
  description?: string;
  systemType:
    | "numeric"
    | "text"
    | "dateTime"
    | "boolean"
    | "pickList"
    | "json"
    | "generated"
    | "currency"
    | "externalRelation"
    | "indirectRelation"
    | "relation";
  validateScript: string;
  attributes: AttributeType;
}

export interface ObjectRecordName {
  label: string;
  type: "text";
}

export interface ObjectResponse {
  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  isExternal: boolean;
  recordName: ObjectRecordName;
  owd: "PublicRead" | "PublicReadWrite" | "Private";
}

export interface ObjectCollection {
  data: ObjectResponse[];
  pageInfo: PageInfoType;
}

export interface ScriptDetailComponent {
  id: string;
  label: string;
  isPrimary: boolean;
  fieldExists: boolean;
}

export interface ObjRecordLayoutResponse {
  id: string;
  name: string;
  components: ScriptDetailComponent[];
}

export interface UserProfileResponse {
  id: string;
  name: string;
}

export interface UserRoleResponse {
  id: string;
  name: string;
  displayName: string;
}

export interface UserResponse {
  userInfo: object;
  profile: UserProfileResponse;
  role: UserRoleResponse;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  ssoId: string;
  isActive: boolean;
}

export interface UserCollection {
  data: UserResponse[];
  pageInfo: PageInfoType;
}

export interface InviteUserDto {
  username: string;
  profileName?: string;
  roleName?: string;
  userInfo?: object;
}

export interface UpdateUserDto {
  profileName?: string;
  roleName?: string;
  userInfo?: object;
}

export interface AppViewerResponse {
  customProps?: Record<string, any>;
  id: string;
  name: string;
  description?: string;
}

export interface AppViewerCollection {
  data: AppViewerResponse[];
  pageInfo: PageInfoType;
}

export interface AppTabResponse {
  id: string;
  name: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface AppProfileResponse {
  id: string;
  name: string;
}

export interface ApplicationResponse {
  customProps?: Record<string, any>;
  name: string;
  description?: string;
  id: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface ApplicationCollection {
  data: ApplicationResponse[];
  pageInfo: PageInfoType;
}

export interface AppBuilderLayoutResponse {
  id: string;
  name: string;
  description?: string;
  type: "dashboard" | "app-page" | "record-page";
  objectName?: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface AppBuilderPageResponse {
  app_page: AppBuilderLayoutResponse[];
  record_page: AppBuilderLayoutResponse[];
}

export interface UpdateAppPageOrderDto {
  /** List layoutIds to update, in order */
  layoutIds: string[];
}

export interface CreateApplicationDto {
  name: string;
  description?: string;
  customProps?: object;
}

export interface UpdateApplicationDto {
  name?: string;
  description?: string;
  customProps?: object;
}

export interface AssignProfileToAppDto {
  profileIds: string[];
}

export interface AppProfileMapDto {
  appIds: string[];
}

export interface PasswordCredential {
  username: string;
  password: string;
}

export interface Oauth2Credential {
  additionalConfig?: object;
  endpoint: string;
  clientId: string;
  clientSecret: string;
}

export interface CredentialResponse {
  url: string;
  authProtocol?: "password" | "oauth" | "jwt";
  identityType: "anon" | "orgWide" | "perUser";
  id: string;
  name: string;
  displayName: string;
  description?: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface CredentialCollection {
  data: CredentialResponse[];
  pageInfo: PageInfoType;
}

export interface CredentialCreateDto {
  credential?: PasswordCredential | Oauth2Credential;
  name: string;
  displayName: string;
  url: string;
  identityType: "anon" | "orgWide" | "perUser";
  authProtocol?: "password" | "oauth" | "jwt";
}

export interface CredentialUpdateDto {
  credential?: PasswordCredential | Oauth2Credential;
  displayName?: string;
  url?: string;
  identityType?: "anon" | "orgWide" | "perUser";
  authProtocol?: "password" | "oauth" | "jwt";
}

export interface OrganizationCurrencyResponse {
  currencyCode: string;
  amount: number;
  scale: number;
  displayName: string;
  isActive: boolean;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface OrganizationCurrencyCollection {
  data: OrganizationCurrencyResponse[];
  pageInfo: PageInfoType;
}

export interface UpdateOrgCurrencyDto {
  amount?: number;
  scale: number;
  isActive?: boolean;
}

export interface CreateOrgCurrencyDto {
  currencyCode: string;
  amount: number;
  scale: number;
  isActive?: boolean;
}

export interface CurrencyRateResponse {
  toCurrencyCode: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;

  /** @format date-time */
  dateFrom: string;
  amount: number;
  scale: number;
}

export interface CurrencyRateCollection {
  data: CurrencyRateResponse[];
  pageInfo: PageInfoType;
}

export interface AvailableCurrencyResponse {
  code: string;
  displayName: string;
}

export interface DataResponseMetadata {
  /** { relationFieldName: relationGuid } */
  relations?: Record<string, string>;

  /** { [mediaId]: [DataMediaResponse] } */
  media?: Record<string, DataMediaResponse>;

  /** { [fieldName]: [PickListItemResponse] } */
  pickList?: Record<string, PickListItemResponse[]>;
  creatorId?: string;
  managerId?: string;
}

export interface MOSchemaResponse {
  id: number;
  name: string;
  displayName: string;
  description?: string;
  fields: FieldSchema[];
  pickList?: Record<string, PickListItemResponse[]>;
}

export interface DataResponse {
  objName: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  guid: string;
  currencyCode: string;
  createdBy: string;
  managedBy: string;
  $metadata?: DataResponseMetadata;
  $schema?: MOSchemaResponse;
}

export interface DataCollection {
  data: DataResponse[];
  $schema?: MOSchemaResponse;
  pageInfo: PageInfoType;
}

export interface ExtDataResponse {
  objName: string;
  externalId: string;
  displayUrl: string;
  $metadata?: DataResponseMetadata;
  $schema?: MOSchemaResponse;
}

export interface ExtDataCollection {
  data: ExtDataResponse[];
  pageInfo: PageInfoType;
}

export interface MediaDto {
  /** Map of attached media files: {[mediaId]: tagName} */
  save?: Record<string, string>;

  /** Array of tagName to remove from attached files */
  del?: string[];
}

export interface Metadata {
  media?: MediaDto;
}

export interface MetadataDto {
  $metadata?: Metadata;
}

export interface BundleActionResponse {
  name: string;
  displayName: string;
}

export interface MediaResponse {
  actionMetadata: BundleActionResponse;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  id: string;
  size: number;
  url: string;
  mimeType: string;
  referenceKey: string;
  actionMetadataId: string;
  createdBy: string;
}

export interface DataMediaResponse {
  tagName?: string;
  name: string;
  displayName: string;
  description?: string;
  id: string;
  size: number;
  url: string;
  mimeType: string;
  createdBy: string;
}

export interface ListRecordsQueryWithFilters {
  /** @min 0 */
  offset?: number;
  limit?: number;
  sortOrder?: "ASC" | "DESC";
  filters?: FilterItem[][];
  resSchema?: string;
  sortBy?: string;
  searchText?: string;
}

export interface UpdateManagedByDto {
  guids: string[];
  managedBy: string;
}

export interface ManagedByMetadata {
  managerId?: string;
}

export interface ManagedByResponse {
  $metadata: ManagedByMetadata;
  guid: string;
  managedBy: string;
}

export interface CreateRecordNameDto {
  label: string;
  type: "text";
}

export interface ObjectCreateDto {
  displayName: string;
  recordName: CreateRecordNameDto;
  owd?: "PublicRead" | "PublicReadWrite" | "Private";
  name: string;
  description?: string;
}

export interface UpdateRecordNameDto {
  label?: string;
  type?: "text";
}

export interface ObjectUpdateDto {
  displayName?: string;
  recordName?: UpdateRecordNameDto;
  owd?: "PublicRead" | "PublicReadWrite" | "Private";
  description?: string;
}

export interface ObjectDto {
  data?: ObjectCreateDto | ObjectUpdateDto;
  action: "create" | "update" | "delete";
  name?: string;
}

export interface ExtDataSourceResponseMap {
  total?: string;
  data?: string;
  externalId?: string;
}

export interface ActionConfigurationResponse {
  outputs: Record<string, ResponseOutputType>;
  name: string;
  displayName: string;
  inputs: InputMetadata;
  method: "post" | "get" | "put" | "delete";
}

export interface RequestVarType {
  variable: string;
  isRequired: boolean;
}

export interface ExtObjectDataSourceResponse {
  inputMap: Record<string, ExtDataSourceInputMap>;
  responseMap: ExtDataSourceResponseMap;

  /** Status code for data */
  statusCode: number;
  actionMetadata: ActionConfigurationResponse;
  availableRequestVars: RequestVarType[];
  availableResponseVars: string[];

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface ExtDataSourceInputMap {
  /** For template, to using variable, content should look like: {\$variable-name} */
  value: string | number | object | boolean;
  type: "value" | "variable" | "template";
}

export interface UpdateDataSourceDto {
  /** Key should start with path.,body,header.,query. */
  inputMap?: Record<string, ExtDataSourceInputMap>;

  /**
   * Status code for data
   * @min 100
   * @max 599
   */
  statusCode?: number;
  responseMap?: object;
  actionMetadataName?: string;
}

export interface TestDataSourceRequestResponse {
  headers?: Record<string, string>;
  data?: string | number | object | boolean | (string | number | object | boolean)[];
  url?: string;
  error?: string;
  status?: number;
}

export interface ExtDataTypeResponse {
  systemType: object;
  name: string;
  displayName: string;
  description?: string;
  validateScript: string;
  attributes: AttributeType;
}

export interface ExtFieldAttributes {
  filters?: FilterItem[][];
  subType?: string;
  isSearchable?: boolean;
  isSortable?: boolean;
}

export interface ExtFieldResponse {
  dataType: ExtDataTypeResponse;
  attributes: ExtFieldAttributes;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  id: string;
  isRequired: boolean;
  value?: string;
  isExternalId: boolean;
  pickListId?: number;
}

export interface ExtFieldAttributesDto {
  filters?: FilterItem[][];
  subType?: string;
  isSearchable?: boolean;
  isSortable?: boolean;
}

export interface ExtFieldCreateDto {
  typeName:
    | "numeric"
    | "text"
    | "dateTime"
    | "boolean"
    | "pickList"
    | "json"
    | "currency"
    | "externalRelation"
    | "indirectRelation"
    | "relation";
  isRequired?: boolean;
  isExternalId?: boolean;

  /**
   * value can be:
   *
   *     For relation/externalRelation: targetObjectName
   *     For indirectRelation: targetObjectName.externalFieldName
   *
   */
  value?: string;
  pickListId?: number;
  attributes: ExtFieldAttributesDto;
  name: string;
  displayName: string;
  description?: string;
}

export interface ExtFieldUpdateDto {
  isRequired?: boolean;
  isExternalId?: boolean;

  /**
   * value can be:
   *
   *     For relation/externalRelation: targetObjectName
   *     For indirectRelation: targetObjectName.externalFieldName
   *
   */
  value?: string;
  pickListId?: number;
  attributes?: ExtFieldAttributes;
  displayName?: string;
  description?: string;
}

export interface LayoutComponent {
  layoutId: string;
  componentIds: string[];
}

export interface ExtFieldDto {
  data?: ExtFieldCreateDto | ExtFieldUpdateDto;
  updateLayouts?: LayoutComponent[];
  action: "create" | "update" | "delete";
  name?: string;
}

export interface ExternalIdFieldResponse {
  name: string;
  displayName: string;
}

export interface IndirectRelationResponse {
  fields: ExternalIdFieldResponse[];
  name: string;
  displayName: string;
}

export interface ContentValue {
  type: "variable" | "value" | "script";
  value: string;
}

export interface ConditionType {
  conditionNumber: number;
  field: string;
  sign:
    | "==="
    | "!=="
    | "<="
    | ">="
    | "<"
    | ">"
    | "isNull"
    | "isNotNull"
    | "startsWith"
    | "endsWith"
    | "contains"
    | "notContains"
    | "wasSet";
  value?: ContentValue;
}

export interface DataFlowCondition {
  conditionLogic: "none" | "and" | "or" | "custom";
  customConditionLogic?: string;
  conditionClauses: ConditionType[];
}

export interface DataFlowAttributesDto {
  objectName?: string;
  conditions?: DataFlowCondition;
  action: "create" | "update" | "delete";
}

export interface UpdateDataFlowDto {
  type: "data";
  attributes?: DataFlowAttributesDto;
  displayName?: string;
  credentialName?: string;
  description?: string;
}

export interface UpdateScreenFlowDto {
  displayName?: string;
  credentialName?: string;
  description?: string;
}

export interface ScheduleFlowAttributesDto {
  /** Format: yyyy-MM-dd */
  startDate: string;

  /** Accept 24-hour clock format: HH:mm, with 15 minute step */
  startTime: string;
  frequency: "once" | "daily" | "weekly";
}

export interface UpdateScheduleFlowDto {
  type: "time-based";
  attributes?: ScheduleFlowAttributesDto;
  displayName?: string;
  credentialName?: string;
  description?: string;
}

export interface UpdateActionFlowDto {
  type: "time-based";
  displayName?: string;
  credentialName?: string;
  description?: string;
}

export interface EventFlowAttributesDto {
  eventName?: string;
  conditions?: DataFlowCondition;
}

export interface UpdateEventFlowDto {
  type: "event";
  attributes?: EventFlowAttributesDto;
  displayName?: string;
  credentialName?: string;
  description?: string;
}

export interface FlowCollection {
  data: (DataFlowResponse | ScreenFlowResponse | ScheduleFlowResponse | ActionFlowResponse | EventFlowResponse)[];
  pageInfo: PageInfoType;
}

export interface FlowResponse {
  /** @format date-time */
  updatedAt: string;
  id: string;
  name: string;
  type: "screen" | "action" | "time-based" | "data" | "event";

  /** @format date-time */
  createdAt: string;
  version: string;
  displayName: string;
  status: "active" | "inactive" | "draft";
  attributes: object;
  credentialName?: string;
  description?: string;
}

export interface FlowCreateDto {
  name: string;
  displayName: string;
  type: "screen" | "action" | "time-based" | "data" | "event";
  credentialName?: string;
  description?: string;
}

export interface FlowTemplateResponse {
  type: "screen" | "action" | "time-based" | "data" | "event";
  credentialName?: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  version: string;
  displayName: string;
  description?: string;
  status: "active" | "inactive" | "draft";
  attributes: object;
}

export interface FlowItemConnectionResponse {
  srcItemName: string;
  trgItemName: string;
  id: string;
  srcOutcomeName: string;
  isFault: boolean;
  nodeAttributes: object;
}

export interface FlowTemplateWithDataResponse {
  type: "screen" | "action" | "time-based" | "data" | "event";
  credentialName?: string;
  items: (
    | ActionFlowItemResponse
    | AssignmentFlowItemResponse
    | DecisionFlowItemResponse
    | ImportRecordsFlowItemResponse
    | LoopFlowItemResponse
    | RecordCreateFlowItemResponse
    | RecordDeleteFlowItemResponse
    | RecordReadFlowItemResponse
    | RecordUpdateFlowItemResponse
    | ScreenFlowItemResponse
    | SubflowFlowItemResponse
    | CollectionAggregationFlowItemResponse
    | EmitEventFlowItemResponse
    | UserCreateFlowItemResponse
  )[];
  connections: FlowItemConnectionResponse[];

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  version: string;
  displayName: string;
  description?: string;
  status: "active" | "inactive" | "draft";
  attributes: object;
}

export interface FlowTemplateCloneDto {
  displayName?: string;
  description?: string;
}

export interface DataFlowAttributes {
  action: "create" | "update" | "delete";
  objectName?: string;
  conditions?: DataFlowCondition;
}

export interface DataFlowResponse {
  type: "data";
  credentialName?: string;
  attributes: DataFlowAttributes;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  version: string;
  displayName: string;
  description?: string;
  status: "active" | "inactive" | "draft";
}

export interface ScreenFlowResponse {
  type: "screen";
  credentialName?: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  version: string;
  displayName: string;
  description?: string;
  status: "active" | "inactive" | "draft";
  attributes: object;
}

export interface ScheduleFlowAttributes {
  startDate: string;
  startTime: string;
  frequency: "once" | "daily" | "weekly";
}

export interface ScheduleFlowResponse {
  type: "time-based";
  credentialName?: string;
  attributes: ScheduleFlowAttributes;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  version: string;
  displayName: string;
  description?: string;
  status: "active" | "inactive" | "draft";
}

export interface ActionFlowResponse {
  type: "action";
  credentialName?: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  version: string;
  displayName: string;
  description?: string;
  status: "active" | "inactive" | "draft";
  attributes: object;
}

export interface EventFlowAttributes {
  eventName?: string;
  conditions?: DataFlowCondition;
}

export interface EventFlowResponse {
  type: "event";
  credentialName?: string;
  attributes: EventFlowAttributes;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  version: string;
  displayName: string;
  description?: string;
  status: "active" | "inactive" | "draft";
}

export interface AssignmentOperator {
  displayName: string;
  value:
    | "assign"
    | "add"
    | "subTract"
    | "addAtEnd"
    | "addAtStart"
    | "removeAll"
    | "removeFirst"
    | "removeLast"
    | "removeAtIndex";
}

export interface BundleFlowItemResponse {
  attributes:
    | ActionFlowItemAttributes
    | AssignmentFlowItemAttributes
    | CollectionAggregationFlowItemAttributes
    | DataCreateAttributes
    | DataReadAttributes
    | DataUpdateAttributes
    | DataDeleteAttributes
    | DecisionFlowItemAttributes
    | EmitEventFlowItemAttributes
    | ImportRecordsAttributes
    | LoopFlowItemAttributes
    | ScreenFlowItemAttributes
    | SubflowFlowItemAttributes;
  name: string;
  displayName: string;
  type:
    | "screen"
    | "action"
    | "sub-flow"
    | "decision"
    | "assignment"
    | "loop"
    | "create"
    | "update"
    | "read"
    | "delete"
    | "import-records"
    | "collection-aggregation"
    | "emit-event"
    | "user-create";
}

export interface BundleFlowVarResponse {
  attributes?:
    | FlowVariableAttributes
    | FlowPickListVariableAttributes
    | FlowRecordVariableAttributes
    | FlowCollectionVariableAttributes
    | FlowEventRecordVariableAttributes;
  name: string;
  displayName: string;
  type:
    | "boolean"
    | "collection"
    | "date"
    | "date-time"
    | "float"
    | "integer"
    | "json"
    | "object"
    | "pick-list"
    | "record"
    | "string"
    | "time";
  isForInput: boolean;
  isForOutput: boolean;
}

export interface ParentCtxResponse {
  flowName: string;
  flowType: "screen" | "action" | "time-based" | "data" | "event";
  exeCtxId: string;
  eventName?: string;
}

export interface DecisionTree {
  success: object;
  fault: object;
}

export interface FlowTemplateLogResponse {
  attributes: DataFlowAttributes | ScheduleFlowAttributes | EventFlowAttributes;
  items: Record<string, BundleFlowItemResponse>;
  variables: Record<string, BundleFlowVarResponse>;
  decisionTree: DecisionTree;
  version: string;
}

export interface ExecutionContextLogResponse {
  status: "start" | "finished";
  ctx: object;
  execItemCounter: number;
  parentCtx?: ParentCtxResponse;
  flowTemplate: FlowTemplateLogResponse;
}

export interface CreateFlowItemConnectionDto {
  srcItemName: string;
  trgItemName: string;
  srcOutcomeName?: string;
  isFault?: boolean;
  nodeAttributes?: object;
}

export interface UpdateFlowItemConnectionDto {
  nodeAttributes?: object;
}

export interface ActionFlowItemAttributes {
  actionMetadataName: string;

  /** key should be <location>.<name> */
  inputMap?: Record<string, ContentValue>;
}

export interface ActionFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: ActionFlowItemAttributes;
  type: "action";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface AssignmentVariableDto {
  variable: string;
  sign:
    | "assign"
    | "add"
    | "subTract"
    | "addAtEnd"
    | "addAtStart"
    | "removeAll"
    | "removeFirst"
    | "removeLast"
    | "removeAtIndex";
  value: ContentValue;
}

export interface AssignmentFlowItemAttributes {
  description: string;
  assignmentVariables: AssignmentVariableDto[];
}

export interface AssignmentFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: AssignmentFlowItemAttributes;
  type: "assignment";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface DecisionConditionType {
  conditionNumber: number;
  sign:
    | "==="
    | "!=="
    | "<="
    | ">="
    | "<"
    | ">"
    | "isNull"
    | "isNotNull"
    | "startsWith"
    | "endsWith"
    | "contains"
    | "notContains"
    | "wasSet";
  value?: ContentValue;
  field: ContentValue;
}

export interface DecisionOutcomeData {
  /** Api name of the decision outcome */
  name: string;
  displayName: string;

  /** Use AND, OR and parentheses to customize the logic. For example, (1 AND 2 AND 3) OR 4 */
  customConditionLogic?: string;
  conditionLogic: "and" | "or" | "custom";
  conditions: DecisionConditionType[];
}

export interface DecisionFlowItemAttributes {
  defaultOutcomeLabel: string;
  order: string[];
  outcomes: Record<string, DecisionOutcomeData>;
}

export interface DecisionFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: DecisionFlowItemAttributes;
  type: "decision";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface ImportRecordsConfig {
  objectName: string;

  /** { fieldName: ContentValue } */
  payload?: Record<string, ContentValue>;
}

export interface ImportRecordsBatchPayload {
  userRefId?: ContentValue;
  batchCtx?: ContentValue;
}

export interface ImportRecordsAttributes {
  fileId: ContentValue;
  config: ImportRecordsConfig;
  batchPayload: ImportRecordsBatchPayload;
}

export interface ImportRecordsFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: ImportRecordsAttributes;
  type: "import-records";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface LoopFlowItemAttributes {
  collectionVariableName: string;
  loopOrder: "ASC" | "DESC";
}

export interface LoopFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: LoopFlowItemAttributes;
  type: "loop";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface RecordMediaUpdate {
  mediaId: ContentValue;
  tagName: ContentValue;
}

export interface RecordMediaConfig {
  update?: RecordMediaUpdate[];

  /** Config tagName to remove */
  remove?: ContentValue[];
}

export interface RecordCreateConfig {
  objectName: string;

  /** { fieldName: ContentValue } */
  payload: Record<string, ContentValue>;

  /** String Variable to store created guid */
  storeGuidToVar?: string;
}

export interface DataCreateAttributes {
  variableName?: string;
  media?: RecordMediaConfig;
  managedBy?: ContentValue;
  config?: RecordCreateConfig;
}

export interface RecordCreateFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: DataCreateAttributes;
  type: "create";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface RecordQueryCondition {
  fieldName: string;
  sign:
    | "equals"
    | "notEquals"
    | "isNull"
    | "isNotNull"
    | "isIn"
    | "isNotIn"
    | "isLike"
    | "isILike"
    | "isMoreThan"
    | "isLessThan"
    | "isMoreThanOrEqual"
    | "isLessThanOrEqual";
  value: ContentValue;
}

export interface RecordDeleteConfig {
  conditions: RecordQueryCondition[];
  conditionLogic: "and";
  objectName: string;
}

export interface DataDeleteAttributes {
  variableName?: string;
  config?: RecordDeleteConfig;
}

export interface RecordDeleteFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: DataDeleteAttributes;
  type: "delete";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface RecordReadConfig {
  conditions: RecordQueryCondition[];
  conditionLogic: "and";
  objectName: string;

  /** FieldName to sort the result */
  sortBy?: string;
  sortOrder?: "ASC" | "DESC";

  /** Get and store all or only the first record */
  getOptions: "one" | "all";

  /** Selected FieldNames to store from Get Result, stores all fields by default */
  getFieldNames?: string[];

  /** Record/Record Collection Variable to store from Get Result */
  storeDataToVar?: string;
}

export interface DataReadAttributes {
  config: RecordReadConfig;
}

export interface RecordReadFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: DataReadAttributes;
  type: "read";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface RecordUpdateConfig {
  conditions: RecordQueryCondition[];
  conditionLogic: "and";
  objectName: string;

  /** { fieldName: ContentValue } */
  payload: Record<string, ContentValue>;
}

export interface DataUpdateAttributes {
  variableName?: string;
  media?: RecordMediaConfig;
  managedBy?: ContentValue;
  config?: RecordUpdateConfig;
}

export interface RecordUpdateFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: DataUpdateAttributes;
  type: "update";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface ScreenFlowItemAttributes {
  layoutScript: Record<string, BaseScreenFIComponent>;
}

export interface ScreenFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: ScreenFlowItemAttributes;
  type: "screen";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface SubflowFlowItemAttributes {
  subflowName: string;
  inputMap?: Record<string, ContentValue>;
}

export interface SubflowFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: SubflowFlowItemAttributes;
  type: "sub-flow";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface CollectionAggregationFlowItemAttributes {
  collectionVariableName: string;
  fieldName: string;
  method: "sum" | "average" | "max" | "min" | "count" | "concat";
}

export interface CollectionAggregationFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: CollectionAggregationFlowItemAttributes;
  type: "collection-aggregation";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface EmitEventFlowItemAttributes {
  /** { fieldName: ContentValue } */
  payload: Record<string, ContentValue>;
  eventName: string;
}

export interface EmitEventFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: EmitEventFlowItemAttributes;
  type: "emit-event";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface UserCreateAttributes {
  /** { fieldName: ContentValue } */
  payload: Record<string, ContentValue>;
  ssoId: ContentValue;
  profileName: ContentValue;
  roleName: ContentValue;
}

export interface UserCreateFlowItemResponse {
  nodeAttributes: Record<string, any>;
  attributes: UserCreateAttributes;
  type: "user-create";
  name: string;
  displayName: string;
  description?: string;
  id: string;
  maxConnections: number;
  hasFaultConnection: boolean;
}

export interface ActionFlowItemAttributesDto {
  actionMetadataName: string;

  /** key should be <location>.<name> */
  inputMap?: Record<string, ContentValue>;
}

export interface CreateActionFlowItemDto {
  nodeAttributes: Record<string, any>;
  attributes: ActionFlowItemAttributesDto;
  type: "action";
  name: string;
  displayName: string;
  description?: string;
}

export interface AssignmentFlowItemAttributesDto {
  description?: string;
  assignmentVariables: AssignmentVariableDto[];
}

export interface CreateAssignmentFlowItemDto {
  nodeAttributes: Record<string, any>;
  attributes: AssignmentFlowItemAttributesDto;
  type: "assignment";
  name: string;
  displayName: string;
  description?: string;
}

export interface UserCreateAttributesDto {
  /** { fieldName: ContentValue } */
  payload: Record<string, ContentValue>;
  ssoId: ContentValue;
  profileName: ContentValue;
  roleName: ContentValue;
}

export interface CreateUserCreateItemDto {
  nodeAttributes: Record<string, any>;
  attributes: UserCreateAttributesDto;
  type: "user-create";
  name: string;
  displayName: string;
  description?: string;
}

export interface DecisionFlowItemAttributesDto {
  defaultOutcomeLabel: string;
  order: string[];
  outcomes: Record<string, DecisionOutcomeData>;
}

export interface CreateDecisionFlowItemDto {
  nodeAttributes: Record<string, any>;
  attributes: DecisionFlowItemAttributesDto;
  type: "decision";
  name: string;
  displayName: string;
  description?: string;
}

export interface ImportRecordsAttributesDto {
  fileId: ContentValue;
  config: ImportRecordsConfig;
  batchPayload: ImportRecordsBatchPayload;
}

export interface CreateImportRecordsDto {
  nodeAttributes: Record<string, any>;
  attributes: ImportRecordsAttributesDto;
  type: "import-records";
  name: string;
  displayName: string;
  description?: string;
}

export interface LoopFlowItemAttributesDto {
  collectionVariableName: string;
  loopOrder: "ASC" | "DESC";
}

export interface CreateLoopFlowItemDto {
  nodeAttributes: Record<string, any>;
  attributes: LoopFlowItemAttributesDto;
  type: "loop";
  name: string;
  displayName: string;
  description?: string;
}

export interface DataCreateAttributesDto {
  variableName?: string;
  media?: RecordMediaConfig;
  managedBy?: ContentValue;
  config?: RecordCreateConfig;
}

export interface CreateRecordCreateDto {
  nodeAttributes: Record<string, any>;
  attributes: DataCreateAttributesDto;
  type: "create";
  name: string;
  displayName: string;
  description?: string;
}

export interface DataDeleteAttributesDto {
  variableName?: string;
  config?: RecordDeleteConfig;
}

export interface CreateRecordDeleteDto {
  nodeAttributes: Record<string, any>;
  attributes: DataDeleteAttributesDto;
  type: "delete";
  name: string;
  displayName: string;
  description?: string;
}

export interface DataReadAttributesDto {
  config: RecordReadConfig;
}

export interface CreateRecordReadDto {
  nodeAttributes: Record<string, any>;
  attributes: DataReadAttributesDto;
  type: "read";
  name: string;
  displayName: string;
  description?: string;
}

export interface DataUpdateAttributesDto {
  media?: RecordMediaConfig;
  managedBy?: ContentValue;
  config?: RecordUpdateConfig;
  variableName?: string;
}

export interface CreateRecordUpdateDto {
  nodeAttributes: Record<string, any>;
  attributes: DataUpdateAttributesDto;
  type: "update";
  name: string;
  displayName: string;
  description?: string;
}

export interface ScreenFlowItemAttributesDto {
  layoutScript: Record<string, BaseScreenFIComponent>;
}

export interface CreateScreenFlowItemDto {
  nodeAttributes: Record<string, any>;
  attributes: ScreenFlowItemAttributesDto;
  type: "screen";
  name: string;
  displayName: string;
  description?: string;
}

export interface SubflowFlowItemAttributesDto {
  subflowName: string;
  inputMap?: Record<string, ContentValue>;
}

export interface CreateSubflowItemDto {
  nodeAttributes: Record<string, any>;
  attributes: SubflowFlowItemAttributesDto;
  type: "sub-flow";
  name: string;
  displayName: string;
  description?: string;
}

export interface CollectionAggregationFlowItemAttributesDto {
  collectionVariableName: string;
  fieldName?: string;
  method: "sum" | "average" | "max" | "min" | "count" | "concat";
}

export interface CreateCollectionAggregationFlowItemDto {
  nodeAttributes: Record<string, any>;
  attributes: CollectionAggregationFlowItemAttributesDto;
  type: "collection-aggregation";
  name: string;
  displayName: string;
  description?: string;
}

export interface CreateEmitEventFlowItemDto {
  nodeAttributes: Record<string, any>;
  attributes: EmitEventFlowItemAttributes;
  type: "emit-event";
  name: string;
  displayName: string;
  description?: string;
}

export interface UpdateActionFlowItemDto {
  nodeAttributes?: Record<string, any>;
  attributes?: ActionFlowItemAttributesDto;
  type: "action";
  displayName?: string;
  description?: string;
}

export interface UpdateAssignmentFlowItemDto {
  nodeAttributes?: Record<string, any>;
  attributes?: AssignmentFlowItemAttributesDto;
  type: "assignment";
  displayName?: string;
  description?: string;
}

export interface UpdateUserCreateItemDto {
  nodeAttributes?: Record<string, any>;
  attributes?: UserCreateAttributesDto;
  type: "user-create";
  displayName?: string;
  description?: string;
}

export interface UpdateDecisionFlowItemDto {
  nodeAttributes?: Record<string, any>;
  attributes?: DecisionFlowItemAttributesDto;
  type: "decision";
  displayName?: string;
  description?: string;
}

export interface UpdateImportRecordsDto {
  nodeAttributes?: Record<string, any>;
  attributes?: ImportRecordsAttributesDto;
  type: "import-records";
  displayName?: string;
  description?: string;
}

export interface UpdateLoopFlowItemDto {
  nodeAttributes?: Record<string, any>;
  attributes?: LoopFlowItemAttributesDto;
  type: "loop";
  displayName?: string;
  description?: string;
}

export interface UpdateRecordCreateDto {
  nodeAttributes?: Record<string, any>;
  attributes?: DataCreateAttributesDto;
  type: "create";
  displayName?: string;
  description?: string;
}

export interface UpdateRecordDeleteDto {
  nodeAttributes?: Record<string, any>;
  attributes?: DataDeleteAttributesDto;
  type: "delete";
  displayName?: string;
  description?: string;
}

export interface UpdateRecordReadDto {
  nodeAttributes?: Record<string, any>;
  attributes?: DataReadAttributesDto;
  type: "read";
  displayName?: string;
  description?: string;
}

export interface UpdateRecordUpdateDto {
  nodeAttributes?: Record<string, any>;
  attributes?: DataUpdateAttributesDto;
  type: "update";
  displayName?: string;
  description?: string;
}

export interface UpdateScreenFlowItemDto {
  nodeAttributes?: Record<string, any>;
  attributes?: ScreenFlowItemAttributesDto;
  type: "screen";
  displayName?: string;
  description?: string;
}

export interface UpdateSubflowItemDto {
  nodeAttributes?: Record<string, any>;
  attributes?: SubflowFlowItemAttributesDto;
  type: "sub-flow";
  displayName?: string;
  description?: string;
}

export interface UpdateCollectionAggregationFlowItemDto {
  nodeAttributes?: Record<string, any>;
  attributes?: CollectionAggregationFlowItemAttributesDto;
  type: "collection-aggregation";
  displayName?: string;
  description?: string;
}

export interface UpdateEmitEventFlowItemDto {
  nodeAttributes?: Record<string, any>;
  attributes?: EmitEventFlowItemAttributes;
  type: "emit-event";
  displayName?: string;
  description?: string;
}

export type RootProps = object;

export interface InputProps {
  label: string;
  isRequired?: boolean;
  inputType: "string" | "integer" | "float" | "date" | "time" | "date-time" | "boolean" | "object" | "array";
  defaultValue?: ContentValue;
}

export interface SelectionProps {
  label: string;
  isRequired?: boolean;
  selectionType: "single" | "multi";
  isRadio?: boolean;
  options: ContentValue[];
  defaultIndexes?: number[];
}

export interface TextProps {
  content: ContentValue;
}

export interface PickListItemResponse {
  id: string;
  name: string;
  displayName: string;
  isDeprecated: boolean;
}

export interface PickListProps {
  label: string;
  isRequired?: boolean;
  selectionType: "single" | "multi";
  isRadio?: boolean;
  pickListId: number;
  pickListItems?: PickListItemResponse[];
  defaultValue?: string[];
}

export interface BaseScreenFIComponent {
  id: string;
  name: string;
  type: string;
  component: "Input" | "Selection" | "DisplayText" | "PickList" | "Selection2" | "FileDropzone" | string;
  props: RootProps | InputProps | SelectionProps | TextProps | PickListProps;
  children: string[];
}

export interface ScreenTemplateResponse {
  script: Record<string, BaseScreenFIComponent>;
  content: Record<string, any>;
  displayName: string;
  ctx: Record<string, any>;
}

export interface FlowNameParams {
  flowName: string;
}

export interface FlowExecutionResponse {
  ctxId: string;
}

export interface FlowInputVariableResponse {
  attributes:
    | FlowVariableAttributes
    | FlowPickListVariableAttributes
    | FlowRecordVariableAttributes
    | FlowCollectionVariableAttributes
    | FlowEventRecordVariableAttributes;
  name: string;
  displayName: string;
  type:
    | "boolean"
    | "collection"
    | "date"
    | "date-time"
    | "float"
    | "integer"
    | "json"
    | "object"
    | "pick-list"
    | "record"
    | "string"
    | "time";
}

export interface FlowVariableAttributes {
  defaultValue?: object;
}

export interface FlowPickListVariableAttributes {
  pickListId: number;
  isMulti?: boolean;
}

export interface FlowRecordVariableAttributes {
  recordObjectName: string;
}

export interface FlowCollectionVariableAttributes {
  itemType: "boolean" | "date" | "date-time" | "float" | "integer" | "json" | "record" | "string" | "time";
  recordObjectName?: string;
}

export interface FlowEventRecordVariableAttributes {
  recordEventName: string;
}

export interface FlowVariableResponse {
  attributes:
    | FlowVariableAttributes
    | FlowPickListVariableAttributes
    | FlowRecordVariableAttributes
    | FlowCollectionVariableAttributes
    | FlowEventRecordVariableAttributes;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  type:
    | "boolean"
    | "collection"
    | "date"
    | "date-time"
    | "float"
    | "integer"
    | "json"
    | "object"
    | "pick-list"
    | "record"
    | "string"
    | "time";
  isForInput: boolean;
  isForOutput: boolean;
}

export interface CreateFlowVariableDto {
  type:
    | "boolean"
    | "collection"
    | "date"
    | "date-time"
    | "float"
    | "integer"
    | "json"
    | "pick-list"
    | "record"
    | "string"
    | "time";
  attributes:
    | FlowVariableAttributes
    | FlowPickListVariableAttributes
    | FlowRecordVariableAttributes
    | FlowCollectionVariableAttributes
    | FlowEventRecordVariableAttributes;
  isForInput?: boolean;
  isForOutput?: boolean;
  name: string;
  displayName: string;
}

export interface UpdateFlowVariableDto {
  type:
    | "boolean"
    | "collection"
    | "date"
    | "date-time"
    | "float"
    | "integer"
    | "json"
    | "pick-list"
    | "record"
    | "string"
    | "time";
  attributes:
    | FlowVariableAttributes
    | FlowPickListVariableAttributes
    | FlowRecordVariableAttributes
    | FlowCollectionVariableAttributes
    | FlowEventRecordVariableAttributes;
  isForInput?: boolean;
  isForOutput?: boolean;
  displayName?: string;
}

export interface FlowVariableSchemaResponse {
  type:
    | "boolean"
    | "collection"
    | "date"
    | "date-time"
    | "float"
    | "integer"
    | "json"
    | "object"
    | "pick-list"
    | "record"
    | "string"
    | "time";
  attributes?:
    | FlowPickListVariableAttributes
    | FlowRecordVariableAttributes
    | FlowCollectionVariableAttributes
    | FlowEventRecordVariableAttributes;
  name: string;
  displayName: string;
}

export interface CtxFlowResponse {
  ctxId: string;
}

export interface ListProps {
  filters?: FilterItem[][];
  objName: string;
  fields: string[];
  actions: string[];
  pageSize?: number;
  limit?: number;
}

export interface RecordProps {
  isPrimary?: boolean;
  numOfCols?: number;
  fields: string[];
  actions: string[];
  title?: string;
  description?: string;
}

export interface RelatedProps {
  filters?: FilterItem[][];
  objName: string;
  fields: string[];
  actions: string[];
  pageSize?: number;
  limit?: number;

  /**
   * Relation field name: key of 'ObjectSchemaResponse.related' field.
   *
   * To load data for this related list please use '/data/{objName}/{guid}/{relation}' with:
   *
   *   'objName' is record-page's objName
   *
   *   'guid' is current record's guid
   *
   *   'relation' is value of this field
   *
   *
   */
  relationField: string;
  title?: string;
  description?: string;
}

export interface SectionProps {
  columns: number[];
  title?: string;
}

export interface TabProps {
  type: "default" | "filled";
  showUnderlineGg: boolean;
}

export interface TriggerObjectInputMap {
  guid?: string;
  parentGuid?: string;
  parentName?: string;
  relationField?: string;
}

export interface TriggerObjectProps {
  title?: string;
  type: "object";
  action: "new" | "get" | "edit" | "delete";
  objName: string;
  inputMap: TriggerObjectInputMap;
}

export interface TriggerFlowProps {
  title?: string;
  type: "flow";
  flowId: string;
  inputMap: Record<string, string>;
}

export interface ComponentResponse {
  id: string;
  label: string;
  type: string;
  component: "ObjectList" | "RecordDetail" | "Flow" | "Button" | "RelatedList" | "Section" | "Tab" | string;
  props:
    | RootProps
    | ListProps
    | RecordProps
    | RelatedProps
    | SectionProps
    | TabProps
    | TriggerObjectProps
    | TriggerFlowProps;
  children: string[];
  schema?: ObjectSchemaResponse;
}

export interface ObjectLayoutResponse {
  script: Record<string, ComponentResponse>;
  content?: object;
  id: string;
  name: string;
  objectName?: string;
}

export interface ListFilterOperator {
  displayName: string;
  value: "===" | "!==" | "isIn" | "isNotIn" | "contains" | "notContains" | "<=" | ">=" | "<" | ">";
}

export interface TriggerObjectResponse {
  type: "object";
  action: "new" | "get" | "edit" | "delete";
  displayName: string;
}

export interface TriggerFlowResponse {
  type: "flow";
  flowId: string;
  name: string;
  displayName: string;
}

export interface LayoutResponse {
  id: string;
  name: string;
  description?: string;
  type: "dashboard" | "app-page" | "record-page";
  objectName?: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface LayoutCollection {
  data: LayoutResponse[];
  pageInfo: PageInfoType;
}

export interface CreateLayoutDto {
  /** Only required for record-page */
  objectName?: string;
  name: string;
  description?: string;
  type: "dashboard" | "app-page" | "record-page";
}

export interface UpdateLayoutDto {
  name?: string;
  description?: string;
}

export interface ComponentDto {
  id: string;
  label: string;
  type: string;
  component: "ObjectList" | "RecordDetail" | "Flow" | "Button" | "RelatedList" | "Section" | "Tab" | string;
  props:
    | RootProps
    | ListProps
    | RecordProps
    | RelatedProps
    | SectionProps
    | TabProps
    | TriggerObjectProps
    | TriggerFlowProps;
  children: string[];
}

export interface UpdateScriptDto {
  script: Record<string, ComponentDto>;
}

export interface AssignAppPageToAppsDto {
  appIds: string[];
}

export interface AppProfileDto {
  appId: string;
  profileId: string;
}

export interface AssignToAppAndProfileDto {
  input: AppProfileDto[];
}

export interface AppLayoutResponse {
  id: string;
  name: string;
}

export interface ProfileLayoutResponse {
  id: string;
  name: string;
}

export interface AppProfileLayoutResponse {
  application: AppLayoutResponse;
  profile: ProfileLayoutResponse;
}

export interface MediaCollection {
  data: MediaResponse[];
  pageInfo: PageInfoType;
}

export interface UpdateMediaDto {
  displayName?: string;
  description?: string;
}

export interface DataTypeResponse {
  name: string;
  displayName: string;
  description?: string;
  systemType:
    | "numeric"
    | "text"
    | "dateTime"
    | "boolean"
    | "pickList"
    | "json"
    | "generated"
    | "currency"
    | "externalRelation"
    | "indirectRelation"
    | "relation";
  validateScript: string;
  attributes: AttributeType;
}

export interface FieldAttributes {
  filters?: FilterItem[][];

  /** Use with relation field. 'SET_NULL' requires 'isRequired'=false */
  onDelete?: "noAction" | "setNull" | "cascade";

  /** Currently applied for short text only. 'Partial' will only expose the last 4 characters. 'All' will hide everything & return '****' */
  sensitivity?: "none" | "partial" | "all";
  subType?: string;
  defaultValue?: string;
  isUnique?: boolean;
  isSearchable?: boolean;
  isSortable?: boolean;
  isReadOnly?: boolean;
}

export interface FieldResponse {
  dataType: DataTypeResponse;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  id: string;
  isRequired: boolean;
  attributes: FieldAttributes;
  value?: string;
  isExternalId: boolean;
  pickListId?: number;
}

export interface FieldAttributesDto {
  filters?: FilterItem[][];

  /** Use with relation field. 'SET_NULL' requires 'isRequired'=false */
  onDelete?: "noAction" | "setNull" | "cascade";

  /** Currently applied for short text only. 'Partial' will only expose the last 4 characters. 'All' will hide everything & return '****' */
  sensitivity?: "none" | "partial" | "all";
  subType?: string;
  defaultValue?: string;
  isUnique?: boolean;
  isSearchable?: boolean;
  isSortable?: boolean;
}

export interface FieldCreateDto {
  typeName:
    | "numeric"
    | "text"
    | "dateTime"
    | "boolean"
    | "pickList"
    | "json"
    | "generated"
    | "currency"
    | "externalRelation"
    | "relation";
  isRequired?: boolean;
  isExternalId?: boolean;

  /**
   * value can be:
   *
   *     For relation/externalRelation: targetObjectName
   *     For indirectRelation: targetObjectName.externalFieldName
   *
   */
  value?: string;
  pickListId?: number;
  name: string;
  displayName: string;
  attributes: FieldAttributesDto;
  description?: string;
}

export interface FieldUpdateDto {
  isRequired?: boolean;
  isExternalId?: boolean;

  /**
   * value can be:
   *
   *     For relation/externalRelation: targetObjectName
   *     For indirectRelation: targetObjectName.externalFieldName
   *
   */
  value?: string;
  pickListId?: number;
  displayName?: string;
  attributes?: FieldAttributesDto;
  description?: string;
}

export interface FieldDto {
  data?: FieldCreateDto | FieldUpdateDto;
  updateLayouts?: LayoutComponent[];
  action: "create" | "update" | "delete";
  name?: string;
}

export interface RuleItem {
  fieldName: string;
  operator:
    | "==="
    | "!=="
    | "isDefined"
    | "isEmpty"
    | "isNotEmpty"
    | "isIn"
    | "isNotIn"
    | "isNegative"
    | "isPositive"
    | "<="
    | ">="
    | "<"
    | ">"
    | "minDate"
    | "maxDate"
    | "contains"
    | "notContains";
  value?: string;
}

export interface AndLogicRule {
  rules: RuleItem[];
}

export interface ValidationRuleResponse {
  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  id: string;
  script: AndLogicRule[];
  execStript?: string;
  errorMessage: string;
}

export interface RuleCreateDto {
  displayName: string;
  script: AndLogicRule[];
  errorMessage: string;
  name: string;
  description?: string;
}

export interface RuleUpdateDto {
  displayName?: string;
  script?: AndLogicRule[];
  errorMessage?: string;
  description?: string;
}

export interface RuleDto {
  data?: RuleCreateDto | RuleUpdateDto;
  action: "create" | "update" | "delete";
  name?: string;
}

export interface ObjectNameOwdDto {
  owd: "PublicRead" | "PublicReadWrite" | "Private";

  /** Object name */
  name: string;
}

export interface ObjectBulkUpdateOwd {
  data: ObjectNameOwdDto[];
}

export interface BaseOrgCurrencyResponse {
  currencyCode: string;
  displayName: string;
}

export interface OrganizationResponse {
  name: string;
  displayName: string;
  description?: string;
  url?: string;
  email?: string;
  phoneNumber?: string;
  contactPerson?: string;
  defaultCurrency: BaseOrgCurrencyResponse;
  defaultLocale: string;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
}

export interface UpdateOrgDto {
  defaultLocale?:
    | "af-NA"
    | "af-ZA"
    | "ak-GH"
    | "am-ET"
    | "ar-DZ"
    | "ar-BH"
    | "ar-EG"
    | "ar-IQ"
    | "ar-JO"
    | "ar-KW"
    | "ar-LB"
    | "ar-LY"
    | "ar-MA"
    | "ar-OM"
    | "ar-QA"
    | "ar-SA"
    | "ar-SD"
    | "ar-SY"
    | "ar-TN"
    | "ar-AE"
    | "ar-YE"
    | "hy-AM"
    | "as-IN"
    | "asa-TZ"
    | "az-Cyrl"
    | "az-Latn"
    | "bm-ML"
    | "eu-ES"
    | "be-BY"
    | "bem-ZM"
    | "bez-TZ"
    | "bn-BD"
    | "bn-IN"
    | "bs-BA"
    | "bg-BG"
    | "my-MM"
    | "yue-Hant-HK"
    | "ca-ES"
    | "tzm-Latn"
    | "chr-US"
    | "cgg-UG"
    | "zh-Hans"
    | "zh-Hant"
    | "kw-GB"
    | "hr-HR"
    | "cs-CZ"
    | "da-DK"
    | "nl-BE"
    | "nl-NL"
    | "ebu-KE"
    | "en-AS"
    | "en-AU"
    | "en-BE"
    | "en-BZ"
    | "en-BW"
    | "en-CA"
    | "en-GU"
    | "en-HK"
    | "en-IN"
    | "en-IE"
    | "en-IL"
    | "en-JM"
    | "en-MT"
    | "en-MH"
    | "en-MU"
    | "en-NA"
    | "en-NZ"
    | "en-MP"
    | "en-PK"
    | "en-PH"
    | "en-SG"
    | "en-ZA"
    | "en-TT"
    | "en-UM"
    | "en-VI"
    | "en-GB"
    | "en-US"
    | "en-ZW"
    | "et-EE"
    | "ee-GH"
    | "ee-TG"
    | "fo-FO"
    | "fil-PH"
    | "fi-FI"
    | "fr-BE"
    | "fr-BJ"
    | "fr-BF"
    | "fr-BI"
    | "fr-CM"
    | "fr-CA"
    | "fr-CF"
    | "fr-TD"
    | "fr-KM"
    | "fr-CG"
    | "fr-CD"
    | "fr-CI"
    | "fr-DJ"
    | "fr-GQ"
    | "fr-FR"
    | "fr-GA"
    | "fr-GP"
    | "fr-GN"
    | "fr-LU"
    | "fr-MG"
    | "fr-ML"
    | "fr-MQ"
    | "fr-MC"
    | "fr-NE"
    | "fr-RW"
    | "fr-RE"
    | "fr-BL"
    | "fr-MF"
    | "fr-SN"
    | "fr-CH"
    | "fr-TG"
    | "ff-SN"
    | "gl-ES"
    | "lg-UG"
    | "ka-GE"
    | "de-AT"
    | "de-BE"
    | "de-DE"
    | "de-LI"
    | "de-LU"
    | "de-CH"
    | "el-CY"
    | "el-GR"
    | "gu-IN"
    | "guz-KE"
    | "ha-Latn"
    | "haw-US"
    | "he-IL"
    | "hi-IN"
    | "hu-HU"
    | "is-IS"
    | "ig-NG"
    | "id-ID"
    | "ga-IE"
    | "it-IT"
    | "it-CH"
    | "ja-JP"
    | "kea-CV"
    | "kab-DZ"
    | "kl-GL"
    | "kln-KE"
    | "kam-KE"
    | "kn-IN"
    | "kk-Cyrl"
    | "km-KH"
    | "ki-KE"
    | "rw-RW"
    | "kok-IN"
    | "ko-KR"
    | "khq-ML"
    | "ses-ML"
    | "lag-TZ"
    | "lv-LV"
    | "lt-LT"
    | "luo-KE"
    | "luy-KE"
    | "mk-MK"
    | "jmc-TZ"
    | "kde-TZ"
    | "mg-MG"
    | "ms-BN"
    | "ms-MY"
    | "ml-IN"
    | "mt-MT"
    | "gv-GB"
    | "mr-IN"
    | "mas-KE"
    | "mas-TZ"
    | "mer-KE"
    | "mfe-MU"
    | "naq-NA"
    | "ne-IN"
    | "ne-NP"
    | "nd-ZW"
    | "nb-NO"
    | "nn-NO"
    | "nyn-UG"
    | "or-IN"
    | "om-ET"
    | "om-KE"
    | "ps-AF"
    | "fa-AF"
    | "fa-IR"
    | "pl-PL"
    | "pt-BR"
    | "pt-GW"
    | "pt-MZ"
    | "pt-PT"
    | "pa-Arab"
    | "ro-MD"
    | "ro-RO"
    | "rm-CH"
    | "rof-TZ"
    | "ru-MD"
    | "ru-RU"
    | "ru-UA"
    | "rwk-TZ"
    | "saq-KE"
    | "sg-CF"
    | "seh-MZ"
    | "sr-Cyrl"
    | "sr-Latn"
    | "sn-ZW"
    | "ii-CN"
    | "si-LK"
    | "sk-SK"
    | "sl-SI"
    | "xog-UG"
    | "so-DJ"
    | "so-ET"
    | "so-KE"
    | "so-SO"
    | "es-AR"
    | "es-BO"
    | "es-CL"
    | "es-CO"
    | "es-CR"
    | "es-DO"
    | "es-EC"
    | "es-SV"
    | "es-GQ"
    | "es-GT"
    | "es-HN"
    | "es-419"
    | "es-MX"
    | "es-NI"
    | "es-PA"
    | "es-PY"
    | "es-PE"
    | "es-PR"
    | "es-ES"
    | "es-US"
    | "es-UY"
    | "es-VE"
    | "sw-KE"
    | "sw-TZ"
    | "sv-FI"
    | "sv-SE"
    | "gsw-CH"
    | "shi-Latn"
    | "shi-Tfng"
    | "dav-KE"
    | "ta-IN"
    | "ta-LK"
    | "te-IN"
    | "teo-KE"
    | "teo-UG"
    | "th-TH"
    | "bo-CN"
    | "bo-IN"
    | "ti-ER"
    | "ti-ET"
    | "to-TO"
    | "tr-TR"
    | "uk-UA"
    | "ur-IN"
    | "ur-PK"
    | "uz-Arab"
    | "uz-Cyrl"
    | "uz-Latn"
    | "vi-VN"
    | "vun-TZ"
    | "cy-GB"
    | "yo-NG"
    | "zu-ZA";
  name?: string;
  description?: string;
  url?: string;
  email?: string;
  phoneNumber?: string;
  contactPerson?: string;
}

export interface PickListResponse {
  items: PickListItemResponse[];

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  id: number;
  isActive: boolean;
}

export interface PickListCollection {
  data: PickListResponse[];
  pageInfo: PageInfoType;
}

export interface MutatePickListItemDto {
  action?: "CREATE" | "UPDATE" | "DELETE" | "RESTORE";
  name: string;
  displayName?: string;
}

export interface CreatePickListDto {
  items?: MutatePickListItemDto[];
  name: string;
  displayName: string;
  description?: string;
}

export interface UpdatePickListDto {
  displayName?: string;
  description?: string;
  items?: MutatePickListItemDto[];
}

export interface ProfileAccessResponse {
  create: boolean;
  read: boolean;
  update: boolean;
  delete: boolean;
  displayName: string;
}

export interface ProfileAccess {
  create: boolean;
  read: boolean;
  update: boolean;
  delete: boolean;
}

export interface ProfileObjectItemDto {
  objName: string;
  access: ProfileAccess;
}

export interface UpsertProfileAccessDto {
  items: ProfileObjectItemDto[];
}

export interface ProfileFeatureDto {
  featureName: "application-layout" | "user" | "organization" | "flow" | "mo-integration" | "event";
  access: ProfileAccess;
}

export interface UpsertProfileFeatureDto {
  items: ProfileFeatureDto[];
}

export interface ProfileResponse {
  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  name: string;
  description: string;
}

export interface ProfileCollection {
  data: ProfileResponse[];
  pageInfo: PageInfoType;
}

export interface CreateProfileDto {
  name: string;
}

export interface UpdateProfileDto {
  name?: string;
}

export interface RoleResponse {
  reportsToRole: RoleResponse;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  name: string;
  displayName: string;
}

export interface CreateRoleDto {
  name: string;
  displayName: string;
  reportsTo: string;
}

export interface UpdateRoleDto {
  displayName?: string;
  reportsTo?: string;
}

export interface UserPoolResponse {
  id: string;
  username: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  enabled?: boolean;
  createdAt: string;
}

export interface UserPoolCollection {
  data: UserPoolResponse[];
  pageInfo: PageInfoType;
}

export interface CreateUserPoolDto {
  username: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  enabled?: boolean;
}

export interface UpdateUserPoolDto {
  firstName?: string;
  lastName?: string;
  email?: string;
  enabled?: boolean;
}

export interface SetUserPwdDto {
  password: string;
}

export interface RoleBasicResponse {
  name: string;
  displayName: string;
}

export interface OwnerAttributesResponse {
  shareFromRole: RoleBasicResponse;
  shareToRole: RoleBasicResponse;
  shareFromType: "role" | "role-and-subordinates";
  shareToType: "role" | "role-and-subordinates";
}

export type CriteriaAttributesResponse = object;

export interface ObjectBasicResponse {
  name: string;
  displayName: string;
}

export interface SharingRuleResponse {
  attributes: OwnerAttributesResponse | CriteriaAttributesResponse;
  object: ObjectBasicResponse;

  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  id: string;
  name: string;
  displayName: string;
  description?: string;
  type: "record-owner" | "criteria";
  accessType: "read-write" | "read-only";
}

export type CriteriaSharingAttributes = object;

export interface OwnerSharingAttributes {
  shareFromRoleName: string;
  shareFromType: "role" | "role-and-subordinates";
  shareToRoleName: string;
  shareToType: "role" | "role-and-subordinates";
}

export interface CreateSharingRuleDto {
  attributes: CriteriaSharingAttributes | OwnerSharingAttributes;
  name: string;
  type: "record-owner" | "criteria";
  displayName: string;
  description?: string;
  objectName: string;
  accessType: "read-write" | "read-only";
}

export interface UpdateSharingRuleDto {
  attributes?: CriteriaSharingAttributes | OwnerSharingAttributes;
  type: "record-owner" | "criteria";
  displayName?: string;
  description?: string;
  objectName?: string;
  accessType?: "read-write" | "read-only";
}

export interface EventResponse {
  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  isExternal: boolean;
  recordName: ObjectRecordName;
  owd: "PublicRead" | "PublicReadWrite" | "Private";
}

export interface EventCollection {
  data: EventResponse[];
  pageInfo: PageInfoType;
}

export interface EventCreateDto {
  displayName: string;
  recordName: CreateRecordNameDto;
  owd?: "PublicRead" | "PublicReadWrite" | "Private";
  name: string;
  description?: string;
}

export interface EventUpdateDto {
  displayName?: string;
  recordName?: UpdateRecordNameDto;
  owd?: "PublicRead" | "PublicReadWrite" | "Private";
  description?: string;
}

export interface EventDto {
  data?: EventCreateDto | EventUpdateDto;
  action: "create" | "update" | "delete";
  name?: string;
}

export interface EventFieldResponse {
  /** @format date-time */
  createdAt: string;

  /** @format date-time */
  updatedAt: string;
  name: string;
  displayName: string;
  description?: string;
  isSystemDefault: boolean;
  id: string;
  isRequired: boolean;
  attributes: FieldAttributes;
  value?: string;
  pickListId?: number;
  dataType: DataTypeResponse;
}

export interface EventFieldCreateDto {
  isRequired?: boolean;

  /**
   * value can be:
   *
   *     For relation/externalRelation: targetObjectName
   *     For indirectRelation: targetObjectName.externalFieldName
   *
   */
  value?: string;
  pickListId?: number;
  typeName: "numeric" | "text" | "dateTime" | "boolean" | "pickList" | "json";
  name: string;
  displayName: string;
  description?: string;
  attributes: FieldAttributesDto;
}

export interface EventFieldUpdateDto {
  isRequired?: boolean;

  /**
   * value can be:
   *
   *     For relation/externalRelation: targetObjectName
   *     For indirectRelation: targetObjectName.externalFieldName
   *
   */
  value?: string;
  pickListId?: number;
  displayName?: string;
  attributes?: FieldAttributesDto;
  description?: string;
}

export interface EventFieldDto {
  data?: FieldCreateDto | EventFieldUpdateDto;
  action: "create" | "update" | "delete";
  name?: string;
  updateLayouts?: LayoutComponent[];
}

export interface KeycloakCredentialDto {
  realmName: string;
  clientId: string;
  clientSecret: string;
  username: string;
  password: string;
}

export interface AuthProviderConfigDto {
  adminUserName: string;
  authProviderName: "keycloak";
  url: string;
  credential: KeycloakCredentialDto;
}

export interface CreateOrgDto {
  defaultLocale:
    | "af-NA"
    | "af-ZA"
    | "ak-GH"
    | "am-ET"
    | "ar-DZ"
    | "ar-BH"
    | "ar-EG"
    | "ar-IQ"
    | "ar-JO"
    | "ar-KW"
    | "ar-LB"
    | "ar-LY"
    | "ar-MA"
    | "ar-OM"
    | "ar-QA"
    | "ar-SA"
    | "ar-SD"
    | "ar-SY"
    | "ar-TN"
    | "ar-AE"
    | "ar-YE"
    | "hy-AM"
    | "as-IN"
    | "asa-TZ"
    | "az-Cyrl"
    | "az-Latn"
    | "bm-ML"
    | "eu-ES"
    | "be-BY"
    | "bem-ZM"
    | "bez-TZ"
    | "bn-BD"
    | "bn-IN"
    | "bs-BA"
    | "bg-BG"
    | "my-MM"
    | "yue-Hant-HK"
    | "ca-ES"
    | "tzm-Latn"
    | "chr-US"
    | "cgg-UG"
    | "zh-Hans"
    | "zh-Hant"
    | "kw-GB"
    | "hr-HR"
    | "cs-CZ"
    | "da-DK"
    | "nl-BE"
    | "nl-NL"
    | "ebu-KE"
    | "en-AS"
    | "en-AU"
    | "en-BE"
    | "en-BZ"
    | "en-BW"
    | "en-CA"
    | "en-GU"
    | "en-HK"
    | "en-IN"
    | "en-IE"
    | "en-IL"
    | "en-JM"
    | "en-MT"
    | "en-MH"
    | "en-MU"
    | "en-NA"
    | "en-NZ"
    | "en-MP"
    | "en-PK"
    | "en-PH"
    | "en-SG"
    | "en-ZA"
    | "en-TT"
    | "en-UM"
    | "en-VI"
    | "en-GB"
    | "en-US"
    | "en-ZW"
    | "et-EE"
    | "ee-GH"
    | "ee-TG"
    | "fo-FO"
    | "fil-PH"
    | "fi-FI"
    | "fr-BE"
    | "fr-BJ"
    | "fr-BF"
    | "fr-BI"
    | "fr-CM"
    | "fr-CA"
    | "fr-CF"
    | "fr-TD"
    | "fr-KM"
    | "fr-CG"
    | "fr-CD"
    | "fr-CI"
    | "fr-DJ"
    | "fr-GQ"
    | "fr-FR"
    | "fr-GA"
    | "fr-GP"
    | "fr-GN"
    | "fr-LU"
    | "fr-MG"
    | "fr-ML"
    | "fr-MQ"
    | "fr-MC"
    | "fr-NE"
    | "fr-RW"
    | "fr-RE"
    | "fr-BL"
    | "fr-MF"
    | "fr-SN"
    | "fr-CH"
    | "fr-TG"
    | "ff-SN"
    | "gl-ES"
    | "lg-UG"
    | "ka-GE"
    | "de-AT"
    | "de-BE"
    | "de-DE"
    | "de-LI"
    | "de-LU"
    | "de-CH"
    | "el-CY"
    | "el-GR"
    | "gu-IN"
    | "guz-KE"
    | "ha-Latn"
    | "haw-US"
    | "he-IL"
    | "hi-IN"
    | "hu-HU"
    | "is-IS"
    | "ig-NG"
    | "id-ID"
    | "ga-IE"
    | "it-IT"
    | "it-CH"
    | "ja-JP"
    | "kea-CV"
    | "kab-DZ"
    | "kl-GL"
    | "kln-KE"
    | "kam-KE"
    | "kn-IN"
    | "kk-Cyrl"
    | "km-KH"
    | "ki-KE"
    | "rw-RW"
    | "kok-IN"
    | "ko-KR"
    | "khq-ML"
    | "ses-ML"
    | "lag-TZ"
    | "lv-LV"
    | "lt-LT"
    | "luo-KE"
    | "luy-KE"
    | "mk-MK"
    | "jmc-TZ"
    | "kde-TZ"
    | "mg-MG"
    | "ms-BN"
    | "ms-MY"
    | "ml-IN"
    | "mt-MT"
    | "gv-GB"
    | "mr-IN"
    | "mas-KE"
    | "mas-TZ"
    | "mer-KE"
    | "mfe-MU"
    | "naq-NA"
    | "ne-IN"
    | "ne-NP"
    | "nd-ZW"
    | "nb-NO"
    | "nn-NO"
    | "nyn-UG"
    | "or-IN"
    | "om-ET"
    | "om-KE"
    | "ps-AF"
    | "fa-AF"
    | "fa-IR"
    | "pl-PL"
    | "pt-BR"
    | "pt-GW"
    | "pt-MZ"
    | "pt-PT"
    | "pa-Arab"
    | "ro-MD"
    | "ro-RO"
    | "rm-CH"
    | "rof-TZ"
    | "ru-MD"
    | "ru-RU"
    | "ru-UA"
    | "rwk-TZ"
    | "saq-KE"
    | "sg-CF"
    | "seh-MZ"
    | "sr-Cyrl"
    | "sr-Latn"
    | "sn-ZW"
    | "ii-CN"
    | "si-LK"
    | "sk-SK"
    | "sl-SI"
    | "xog-UG"
    | "so-DJ"
    | "so-ET"
    | "so-KE"
    | "so-SO"
    | "es-AR"
    | "es-BO"
    | "es-CL"
    | "es-CO"
    | "es-CR"
    | "es-DO"
    | "es-EC"
    | "es-SV"
    | "es-GQ"
    | "es-GT"
    | "es-HN"
    | "es-419"
    | "es-MX"
    | "es-NI"
    | "es-PA"
    | "es-PY"
    | "es-PE"
    | "es-PR"
    | "es-ES"
    | "es-US"
    | "es-UY"
    | "es-VE"
    | "sw-KE"
    | "sw-TZ"
    | "sv-FI"
    | "sv-SE"
    | "gsw-CH"
    | "shi-Latn"
    | "shi-Tfng"
    | "dav-KE"
    | "ta-IN"
    | "ta-LK"
    | "te-IN"
    | "teo-KE"
    | "teo-UG"
    | "th-TH"
    | "bo-CN"
    | "bo-IN"
    | "ti-ER"
    | "ti-ET"
    | "to-TO"
    | "tr-TR"
    | "uk-UA"
    | "ur-IN"
    | "ur-PK"
    | "uz-Arab"
    | "uz-Cyrl"
    | "uz-Latn"
    | "vi-VN"
    | "vun-TZ"
    | "cy-GB"
    | "yo-NG"
    | "zu-ZA";
  name: string;
  displayName: string;
  description?: string;
  url?: string;
  email?: string;
  phoneNumber?: string;
  contactPerson?: string;
  defaultCurrencyCode: object;
  authProviderConfig: AuthProviderConfigDto;
}
