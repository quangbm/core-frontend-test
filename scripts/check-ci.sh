#!/usr/bin/env bash
set -e

if [[ -z "$1"  ]]; then
  echo "not a valid option"
  exit 1
fi

if [[ $1 = "web" ]]; then
  yarn shared lint --quiet -c ./.eslintrc.push.js
  yarn web lint --quiet -c ./.eslintrc.push.js
  yarn web tsc -p tsconfig.json
fi

if [[ $1 = "mobile" ]]; then
  yarn shared lint --quiet -c ./.eslintrc.push.js
  yarn mobile lint --quiet -c ./.eslintrc.push.js
  yarn mobile tsc -p tsconfig.json
fi

