#!/bin/bash
set -e

[ "$(git rev-parse --abbrev-ref HEAD)" = "master" ] && HEAD="HEAD^" || HEAD="origin/master"
FILES=$(git diff --name-only $HEAD --diff-filter=ACMR "*.js" "*.tsx" "*.ts" "*.json" "*.mdx" | sed 's| |\\ |g')

if [ -z "$1" ]; then
  if (grep -q 'shared/' <<< $FILES); then
    yarn shared
    yarn shared lint --quiet -c ./.eslintrc.push.js
  fi
  if (grep -q 'web/' <<< $FILES); then
    yarn web
    yarn web lint --quiet -c ./.eslintrc.push.js
    yarn web tsc -p tsconfig.json
  fi
  if (grep -q 'mobile/' <<< $FILES); then
    yarn mobile
    yarn mobile lint --quiet -c ./.eslintrc.push.js
    yarn mobile tsc -p tsconfig.json
  fi
fi

if [ ! -z "$FILES" ]; then
  echo "$FILES" | xargs ./node_modules/.bin/prettier --write
fi

if [[ -z "$1" && `git status --porcelain` ]]; then
  echo Lint problem: Please commit fixed files
  exit 255
fi
