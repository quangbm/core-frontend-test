#!/usr/bin/env bash
set -e

function yarn_install() {
  [ -f "$1/node_modules/lock.sha" ] && CHECKSUM=$(cat $1/node_modules/lock.sha | shasum -cq) || CHECKSUM=FAIL

  if [ "$CHECKSUM" ]; then 
    echo INVALIDATE $1
    rm -rf $1/node_modules
  fi

  yarn --cwd $1 --ignore-scripts --frozen-lockfile
  shasum $1/yarn.lock > $1/node_modules/lock.sha
}

yarn_install shared
yarn_install mobile
yarn_install web
