#!/usr/bin/env bash
set -e

# variables
habor_project=$CI_ENV_HARBOR_PROJECT
harbor_uri=$CI_ENV_HARBOR_URI
harbor_username=$CI_ENV_HARBOR_USERNAME
harbor_password=$CI_ENV_HARBOR_PASSWORD
commit_hash=$BITBUCKET_COMMIT

img_name='core-frontend'
image=$harbor_uri/$habor_project/$img_name:commit-$commit_hash
image_latest=$harbor_uri/$habor_project/$img_name:latest

cd web
docker login $harbor_uri -u$harbor_username -p$harbor_password
docker build --compress --build-arg DIST_FOLDER=build -t $image .
docker push $image

# not in pr
if [[ -z "${BITBUCKET_PR_ID}" ]]; then
  docker tag $image $image_latest
  docker push $image_latest
fi
