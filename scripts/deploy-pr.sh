#!/bin/sh

## variables
#### docker image definitions
habor_project=$CI_ENV_HARBOR_PROJECT
harbor_uri=$CI_ENV_HARBOR_URI
commit_hash=$BITBUCKET_COMMIT
pr=$BITBUCKET_PR_ID
img_tag=commit-$commit_hash
img_name='core-frontend'
image=$harbor_uri/$habor_project/$img_name:$img_tag

#### k8s deployment definitions
namespace=ncore-pr
name=$img_name-$pr
host=$pr.$HOST_SUFFIX

cat > manifest.yaml <<- EOF
---
apiVersion: v1
kind: Service
metadata:
  name: $name 
  namespace: $namespace 
spec:
  type: ClusterIP
  ports:
    - port: 80
      targetPort: http
      protocol: TCP
      name: http
  selector:
    app.kubernetes.io/name: $name 
    app.kubernetes.io/instance: $name 
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: $name 
  namespace: $namespace 
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: $name 
      app.kubernetes.io/instance: $name 
  template:
    metadata:
      labels:
        app.kubernetes.io/name: $name
        app.kubernetes.io/instance: $name 
    spec:
      imagePullSecrets:
        - name: missing-registry
      containers:
        - name: nc-common-deployment
          image: "$image"
          imagePullPolicy: Always
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
          livenessProbe:
            httpGet:
              path: /
              port: 80
          readinessProbe:
            httpGet:
              path: /
              port: 80
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: $name
  namespace: $namespace 
  annotations:
    kubernetes.io/ingress.class: nginx
spec:
  rules:
    - host: "$host"
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: $name 
                port:
                  number: 80
EOF

echo served at http://$host
