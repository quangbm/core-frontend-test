#!/bin/sh

psql -Upg << EOSQL
  create database keycloak;
  create database core_server;
EOSQL

psql -Upg -d core_server < /docker-entrypoint-initdb.d/core-server.data
psql -Upg -d keycloak < /docker-entrypoint-initdb.d/keycloak.data