import { createRef } from 'react'
import { QueryClientProvider } from 'react-query'
import { Provider } from 'react-redux'
import 'react-toastify/dist/ReactToastify.css'
import { ThemeProvider } from 'styled-components/macro'
import { GlobalStyle, theme } from './components/GlobalStyle'
import { ToastContainer } from './components/NToast'
import { store } from './redux'
import { Main } from './routes/Main'
import './services/i18n'
import { queryClient } from './services/react-query'
export const rootPortal = createRef<HTMLDivElement>()

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <QueryClientProvider client={queryClient}>
          <GlobalStyle />
          <Main />
        </QueryClientProvider>
        <ToastContainer />
      </ThemeProvider>
    </Provider>
  )
}

export default App
