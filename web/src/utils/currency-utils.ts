import { Currency } from '@dinero.js/currencies'
import { Dinero, dinero, toFormat } from 'dinero.js'

export function createIntlFormatter(locale: string, options = {}): (dineroObject: Dinero<number>) => string {
  function transformer({ amount, currency }: { amount: number; currency: Currency<number> }) {
    return `${currency.code} ${amount.toLocaleString(locale, {
      ...options,
    })}`
  }

  return function formatter(dineroObject: Dinero<number>) {
    return toFormat(dineroObject, transformer)
  }
}

export function dineroFromFloat({ float, currency }: { float: number; currency: Currency<number> }) {
  const factor = currency.base ** currency.exponent
  const amount = Math.round(float * factor)

  return dinero({ amount, currency })
}
