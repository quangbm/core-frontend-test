import { AxiosResponse } from 'axios'
import { PageInfoType } from '../../../shared/src/api'

export const DEFAULT_TABLE_DATA = {
  pageData: [],
  pageInfo: { total: 0 },
  totalPage: 1,
}

export function getTableData(data?: AxiosResponse) {
  if (!data) {
    return DEFAULT_TABLE_DATA
  }
  const { data: pageData, pageInfo } = data.data as { data: any[]; pageInfo: PageInfoType }
  return { pageData, pageInfo, totalPage: Math.ceil(pageInfo.total / pageInfo.limit) }
}
