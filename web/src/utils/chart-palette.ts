const CHART_COLORS = [
  '#35938B',
  '#66BBB5',
  '#003f5c',
  '#2f4b7c',
  '#665191',
  '#a05195',
  '#d45087',
  '#f95d6a',
  '#ff7c43',
  '#ffa600',
  '#1f555c',
  '#006864',
  '#007a61',
  '#2c8a54',
  '#5a983e',
  '#8ba321',
  '#c2a800',
  '#ffa600',
  '#5c560f',
  '#626e28',
  '#658643',
  '#679e62',
  '#66b784',
  '#64cfaa',
  '#65e7d2',
  '#6bfffb',
]

export function chartPalette(index: number): string {
  return CHART_COLORS[index % CHART_COLORS.length]
}
