import { useEffect } from 'react'
import { queryClient } from '../services/react-query'

export function useRQRefetchOnWindowFocus(enabled = true) {
  useEffect(() => {
    if (process.env.NODE_ENV === 'development') {
      console.info(`refetchOnWindowFocus: ${enabled}`)
    }
    const defaultOptions = queryClient.getDefaultOptions()
    queryClient.setDefaultOptions({
      ...defaultOptions,
      queries: {
        ...defaultOptions.queries,
        refetchOnWindowFocus: enabled,
      },
    })
  }, [enabled])
}
