import { RefObject, useMemo } from 'react'
import { useMeasure } from './useMeasure'

export type PopoverPlacement =
  | 'top'
  | 'left'
  | 'right'
  | 'bottom'
  | 'topLeft'
  | 'topRight'
  | 'bottomLeft'
  | 'bottomRight'
  | 'leftTop'
  | 'leftBottom'
  | 'rightTop'
  | 'rightBottom'

export const usePopoverPosition = (
  containerRef: RefObject<HTMLElement>,
  overlayRef: RefObject<HTMLElement>,
  placement: PopoverPlacement,
) => {
  const containerSizes = useMeasure(containerRef)
  const overlaySizes = useMeasure(overlayRef)
  const { top, left } = useMemo(() => {
    switch (placement) {
      case 'bottomLeft':
        return {
          top: containerSizes.top + containerSizes.height,
          left: containerSizes.left,
        }
      case 'bottomRight':
        return {
          top: containerSizes.top + containerSizes.height,
          left: containerSizes.left - (overlaySizes.width - containerSizes.width),
        }
      case 'bottom':
        return {
          top: containerSizes.top + containerSizes.height,
          left: containerSizes.left - (overlaySizes.width - containerSizes.width) / 2,
        }
      case 'topLeft':
        return {
          top: containerSizes.top - overlaySizes.height,
          left: containerSizes.left,
        }
      case 'topRight':
        return {
          top: containerSizes.top - overlaySizes.height,
          left: containerSizes.left - (overlaySizes.width - containerSizes.width),
        }
      case 'top':
        return {
          top: containerSizes.top - overlaySizes.height,
          left: containerSizes.left - (overlaySizes.width - containerSizes.width) / 2,
        }
      case 'leftTop':
        return {
          top: containerSizes.top,
          left: containerSizes.left - overlaySizes.width,
        }
      case 'left':
        return {
          top: containerSizes.top - (overlaySizes.height - containerSizes.height) / 2,
          left: containerSizes.left - overlaySizes.width,
        }
      case 'leftBottom':
        return {
          top: containerSizes.top - (overlaySizes.height - containerSizes.height),
          left: containerSizes.left - overlaySizes.width,
        }
      case 'rightTop':
        return {
          top: containerSizes.top,
          left: containerSizes.left + containerSizes.width,
        }
      case 'right':
        return {
          top: containerSizes.top - (overlaySizes.height - containerSizes.height) / 2,
          left: containerSizes.left + containerSizes.width,
        }
      case 'rightBottom':
        return {
          top: containerSizes.top - (overlaySizes.height - containerSizes.height),
          left: containerSizes.left + containerSizes.width,
        }
      default:
        return {
          top: 0,
          left: 0,
        }
    }
  }, [placement, containerSizes, overlaySizes])
  return {
    top,
    left,
  }
}
