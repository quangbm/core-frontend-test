import { useMutation, useQuery, useQueryClient } from 'react-query'
import { APIServices, clearToken, QueryKeys, setToken } from '../services/api/api'

export const ACCESS_TOKEN_KEY = '@nCore/ACCESS_TOKEN'

export function useAccessToken() {
  return useQuery([ACCESS_TOKEN_KEY], () => localStorage.getItem(ACCESS_TOKEN_KEY), {
    onSuccess(data) {
      if (data) {
        setToken(data)
      }
    },
    cacheTime: Infinity,
    staleTime: Infinity,
  })
}

export function useSignIn() {
  const queryClient = useQueryClient()
  return useMutation(APIServices.Auth.login, {
    onSuccess(response) {
      localStorage.setItem(ACCESS_TOKEN_KEY, response.data.accessToken)
      queryClient.invalidateQueries([ACCESS_TOKEN_KEY])
    },
  })
}

export function useSignOut() {
  const queryClient = useQueryClient()
  return useMutation(APIServices.Auth.logout, {
    onSettled() {
      clearToken()
      localStorage.removeItem(ACCESS_TOKEN_KEY)
      queryClient.invalidateQueries()
      // need to remove else profile routing will not change
      queryClient.removeQueries(QueryKeys.Me.getMe)
    },
  })
}
