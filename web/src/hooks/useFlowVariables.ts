import * as React from 'react'
import { useQuery } from 'react-query'
import { FlowVariableResponse } from '../../../shared/src/api'
import { APIServices, QueryKeys } from '../services/api'

export function useFlowVariables(flowName: string, version: string, filter?: string) {
  const [variableName, setVariableName] = React.useState('')
  const { data: { variables, variablesByName } = { variables: [], variablesByName: {} } } = useQuery(
    [QueryKeys.Flows.getFlowTemplateVariables, { flowName, version }],
    APIServices.Flows.getFlowTemplateVariables,
    {
      enabled: !!flowName,
      select(response) {
        const variables = [] as Array<{ name: string; displayName: string }>
        const variablesByName = {} as Record<string, FlowVariableResponse>

        for (const variable of response.data.sort((a, b) => a.name.length - b.name.length) || []) {
          variables.push({ name: variable.name, displayName: variable.displayName })
          variablesByName[variable.name] = variable
        }
        return { variables, variablesByName }
      },
    },
  )

  const { data: nestedVariables } = useQuery(
    [QueryKeys.Flows.getFlowVariableSchema, { flowName, variable: variableName!, version }],
    APIServices.Flows.getFlowVariableSchema,
    {
      enabled: Boolean(variableName),
      select(response) {
        return response.data.map(variable => ({
          name: `${variableName}.${variable.name}`,
          displayName: variable.displayName,
        }))
      },
    },
  )

  // if filter keyword is a variable type === record || object
  // set variable name
  // then fetch nested schema
  React.useEffect(() => {
    if (filter) {
      const variableType = variablesByName[filter]?.type
      if (variableType === 'record' || variableType === 'object') {
        setVariableName(filter)
        return
      }
    }
    if (!filter) {
      setVariableName('')
    }
  }, [filter, variablesByName])

  // if filter keyword and nestedVariables exists
  // concat nestedVariables
  return filter && nestedVariables ? variables.concat(nestedVariables) : variables
}
