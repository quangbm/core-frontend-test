import { AxiosResponse } from 'axios'
import { useQuery, UseQueryOptions } from 'react-query'
import { ErrorResponse, MeResponse } from '../../../shared/src/api'
import { APIServices, QueryKeys } from '../services/api'

export function useMe(options?: UseQueryOptions<AxiosResponse<MeResponse>, ErrorResponse, MeResponse>) {
  return useQuery([QueryKeys.Me.getMe], APIServices.Me.getMe, {
    ...options,
    cacheTime: 1000 * 60 * 60,
    staleTime: Infinity,
    select(response) {
      return response.data
    },
  })
}

export const getCurUserFromMe = (meData?: MeResponse): Record<string, any> => {
  if (meData) {
    return {
      id: meData.id,
      ...meData.userInfo.data,
    }
  }
  return {}
}
