import { AxiosResponse } from 'axios'
import { useMutation, useQuery, useQueryClient, UseQueryOptions } from 'react-query'
import { ErrorResponse, SessionResponse } from '../../../shared/src/api'
import { APIServices, QueryKeys } from '../services/api/api'

export function useSession(options?: UseQueryOptions<AxiosResponse<SessionResponse>, ErrorResponse, SessionResponse>) {
  const queryClient = useQueryClient()
  const state = useQuery([QueryKeys.Sessions.getSession], APIServices.Sessions.getSession, {
    ...options,
    cacheTime: 1000 * 60 * 60,
    staleTime: Infinity,
    select(response) {
      return response.data
    },
  })
  const { mutate } = useMutation(APIServices.Sessions.updateSession, {
    onMutate(dto) {
      queryClient.cancelQueries(QueryKeys.Sessions.getSession)

      const previousSession = queryClient.getQueryData(QueryKeys.Sessions.getSession)
      queryClient.setQueryData<SessionResponse>(QueryKeys.Sessions.getSession, old => {
        return {
          ...old!,
          ...dto,
        }
      })
      return previousSession
    },
    onError(_, __, previousSession) {
      queryClient.setQueryData(QueryKeys.Sessions.getSession, previousSession)
    },
    onSuccess(response) {
      queryClient.setQueryData(QueryKeys.Sessions.getSession, response)
    },
  })
  return [state, mutate] as [typeof state, typeof mutate]
}
