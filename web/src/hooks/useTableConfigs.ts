import debounce from 'lodash/debounce'
import * as React from 'react'
import { useHistory, useLocation } from 'react-router'
import { SortingRule } from 'react-table'
import { SortOrder } from '../../../shared/src/api'

export type TableConfigs = {
  searchText?: string
  page?: number
  sortBy?: string
  sortOrder?: SortOrder
}

const DEFAULT_CONFIGS = { timeout: 300, method: 'replace' }

export function useTableConfigs(
  defaultValues: TableConfigs = { page: 1 },
  options?: { timeout?: number; method?: 'replace' | 'push' },
): [
  TableConfigs & { page: number; searchValue?: string },
  {
    onChangePage(page: number): void
    onChangeSearch(search: string): void
    onChangeSort(sortByArray: SortingRule<any>[]): void
  },
] {
  const configs = { ...DEFAULT_CONFIGS, ...options }
  const location = useLocation()
  const history = useHistory()

  const [searchValue, setSearchValue] = React.useState<string>('')

  const urlSearchParams = React.useRef(new URLSearchParams(location.search))

  React.useEffect(() => {
    const searchValue = urlSearchParams.current?.get('searchText')
    setSearchValue(searchValue || '')
  }, [])

  const navigate = () => {
    const search = urlSearchParams.current?.toString()
    // @ts-ignore
    history[configs.method]({ pathname: location.pathname, search })
  }

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleChangeSearchText = React.useCallback(
    debounce((searchText?: string) => {
      const value = searchText?.trim()
      if (!value) {
        urlSearchParams.current?.delete('searchText')
      } else {
        urlSearchParams.current?.set('searchText', value)
      }
      urlSearchParams.current?.delete('page')

      navigate()
    }, configs.timeout),
    [configs.timeout],
  )

  const onChangeSearch = React.useCallback(
    (newValue: string) => {
      setSearchValue(newValue)
      handleChangeSearchText(newValue)
    },
    [handleChangeSearchText],
  )

  const onChangePage = React.useCallback((page: number) => {
    if (page <= 1) {
      urlSearchParams.current?.delete('page')
    } else {
      urlSearchParams.current?.set('page', `${page}`)
    }
    navigate()
  }, [])

  const onChangeSort = React.useCallback((sortByArray: SortingRule<any>[]) => {
    const sortBy = sortByArray[0]
    if (!sortBy) {
      urlSearchParams.current?.delete('sortBy')
      urlSearchParams.current?.delete('sortOrder')
    } else {
      urlSearchParams.current?.set('sortBy', sortBy.id)
      urlSearchParams.current?.set('sortOrder', sortBy.desc ? SortOrder.Desc : SortOrder.Asc)
    }
    navigate()
  }, [])

  const params = transformParams(urlSearchParams.current, defaultValues)

  return [
    { searchValue, ...params },
    { onChangePage, onChangeSearch, onChangeSort },
  ]
}

function transformParams(urlSearchParams: URLSearchParams, defaultValues: TableConfigs) {
  const page = Number(urlSearchParams.get('page') || 1)
  const sortBy = urlSearchParams.get('sortBy') || undefined
  const sortOrder = urlSearchParams.get('sortOrder') || undefined
  const searchText = urlSearchParams.get('searchText') || undefined
  return Object.assign(defaultValues, { searchText, page, sortBy, sortOrder })
}
