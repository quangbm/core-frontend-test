import { useEffect } from 'react'
import { setBaseUrl } from '../services/api'

export function useQADebug(enabled = false) {
  const { search } = window.location
  useEffect(() => {
    if (enabled && search) {
      const urlParams = new URLSearchParams(search)
      const backendUrl = urlParams.get('backend')
      if (backendUrl) {
        console.info('Backend Url:', backendUrl)
        setBaseUrl(backendUrl)
      }
    }
    // Should run only once!
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
}
