import styled from 'styled-components/macro'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { BuilderElement, useAppBuilder } from '../../AppBuilder/contexts/AppBuilderContext'
import { TextInputProps } from './TextInputWidget'

const Wrapper = styled('div')`
  width: 100%;
  height: 100%;
  display: flex;
`

export default function TextInput({ elementId }: { elementId: string }) {
  const { elements } = useAppBuilder()
  const { props, displayName } = elements[elementId] as BuilderElement<TextInputProps>
  return (
    <Wrapper>
      <NTextInput label={props.showLabel && (props?.label?.value || displayName)} caption={props.caption?.value} />
    </Wrapper>
  )
}
