import { NCheckbox } from '../../../../components/NCheckbox/NCheckbox'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { useAppBuilder, useAppBuilderDispatch } from '../../AppBuilder/contexts/AppBuilderContext'

export default function TextInputConfigs() {
  const dispatch = useAppBuilderDispatch()
  const { elements, selectedElements } = useAppBuilder()
  if (!selectedElements) {
    return null
  }
  const element = elements[selectedElements]
  return (
    <div>
      <NCheckbox
        title="Show label"
        checked={element.props.showLabel ?? false}
        onChange={e => {
          const showLabel = e.target.checked
          dispatch({
            type: 'UPDATE_SELECTED_ELEMENT',
            payload: {
              props: {
                showLabel,
              },
              layoutProps: {
                height: showLabel ? 5 : 4,
              },
            },
          })
        }}
      />
      <NTextInput
        label="Label"
        value={element.props.label?.value || ''}
        onChange={e => {
          dispatch({
            type: 'UPDATE_SELECTED_ELEMENT',
            payload: {
              props: {
                label: {
                  value: e.target.value,
                },
              },
            },
          })
        }}
      />
    </div>
  )
}

TextInputConfigs.displayName = 'TextInputConfigs'
