import { ContentValue } from '../../../../../../shared/src/api'
import { AppBuilderWidget, WidgetCategory } from '../AppWidget'
import component from './TextInput'
import { ReactComponent as icon } from './TextInput.svg'
import sidebar from './TextInputConfigs'

export type TextInputProps = {
  showLabel: boolean
  label?: ContentValue
  caption?: ContentValue
  error?: ContentValue
  defaultValue?: ContentValue
  placeholder?: ContentValue
}

export const TextInputWidget: AppBuilderWidget<{ elementId: string }> = {
  name: 'com.nuclent.ncore.textinput',
  displayName: 'Text Input',
  variableName: 'textInput',
  category: [WidgetCategory.TextInput],
  resizeable: {
    vertical: false,
    horizontal: true,
  },
  icon,
  viewerComponent: component,
  builderComponent: component,
  sidebar,
  layoutProps: { width: 4, height: 4 },
  initialProps: {
    elementId: '',
  },
}
