import { AppBuilderWidget, WidgetCategory } from '../AppWidget'
import component from './Section'
import { ReactComponent as icon } from './Section.svg'
import sidebar from './SectionConfigs'

export type SectionWidgetProps = {}

export const SectionWidget: AppBuilderWidget<SectionWidgetProps> = {
  name: 'com.nuclent.ncore.section',
  displayName: 'Section',
  variableName: 'section',
  category: [WidgetCategory.Layout],
  resizeable: {
    vertical: true,
    horizontal: true,
  },
  icon,
  viewerComponent: component,
  builderComponent: component,
  sidebar,
  layoutProps: { width: 6, height: 16 },
  initialProps: {},
}
