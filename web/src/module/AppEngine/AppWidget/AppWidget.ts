import { ComponentType } from 'react'

export enum WidgetCategory {
  TextInput = 'Text Input',
  NumberInput = 'Number Input',
  DateInput = 'Date Input',
  SpecialInput = 'Special Input',
  Layout = 'Layout',
  Data = 'Data',
  Other = 'Other',
}

export type WidgetLayoutProps = {
  startCol?: number
  startRow?: number
  endCol?: number
  endRow?: number
  width: number
  height: number
}

export type AppBuilderWidget<T extends Record<string, unknown> = Record<string, unknown>> = {
  name: string
  displayName: string
  variableName: string
  category: Array<WidgetCategory>
  resizeable: Record<'vertical' | 'horizontal', boolean>
  icon: ComponentType
  viewerComponent: ComponentType<{ elementId: string }>
  builderComponent: ComponentType<{ elementId: string }>
  sidebar: ComponentType<{ elementId: string }>
  layoutProps: WidgetLayoutProps
  initialProps: T
  documentation?: string
}

// export type AppViewerWidget<T extends object = {}> = {
//   name: string
//   displayName: string
//   variableName: string
//   layoutProps: WidgetLayoutProps
//   props: T
//   component: ComponentType<T>
// }
