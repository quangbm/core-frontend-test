import { HTMLAttributes, useEffect } from 'react'
import { useDrag } from 'react-dnd'
import { getEmptyImage } from 'react-dnd-html5-backend'
import styled from 'styled-components/macro'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { typography } from '../../../../../components/NTypography'
import { useAppBuilderConfigs } from '../../contexts/AppBuilderContext'
import { LeftSidebarElement } from '../../models'

const Wrapper = styled('aside')`
  background: white;
  min-height: 100%;
`

const Category = styled('div')`
  display: grid;
  background: ${color('Neutral300')};
  grid-template: auto / repeat(3, 83px);
  grid-gap: 1px;
  border-bottom: 1px solid ${color('Neutral300')};
`
const CategoryName = styled('div')`
  grid-column: span 3;
  background: white;
  ${typography('overline')};
  padding: ${spacing('xs')} ${spacing('sm')};
`
const Widget = styled(Draggable)`
  height: 83px;
  background: white;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  cursor: grab;
`

const WidgetName = styled('div')`
  color: ${color('Neutral500')};
  margin-top: ${spacing('xs')};
  text-align: center;
  ${typography('x-small-ui-text')}
`

export default function LeftSidebar() {
  const { widgetsByCategory, category } = useAppBuilderConfigs()
  return (
    <Wrapper>
      {category.map(category => {
        const widgets = widgetsByCategory[category] || []
        const span = 3 - (widgets.length % 3)
        return (
          <Category key={category}>
            <CategoryName>{category}</CategoryName>
            {widgets?.map(widget => {
              const { icon: Icon, displayName, name, variableName, layoutProps, initialProps } = widget
              return (
                <Widget item={{ name, displayName, variableName, layoutProps, initialProps }} key={widget.name}>
                  <Icon />
                  <WidgetName>{displayName}</WidgetName>
                </Widget>
              )
            })}
            {span > 0 && span < 3 && <Widget disabled style={{ cursor: 'auto', gridColumn: `span ${span}` }} />}
          </Category>
        )
      })}
    </Wrapper>
  )
}

LeftSidebar.displayName = 'LeftSidebar'

function Draggable({
  item,
  disabled = false,
  ...restProps
}: {
  disabled?: boolean
  item?: LeftSidebarElement
} & HTMLAttributes<HTMLDivElement>) {
  const [{ isDragging }, drag, preview] = useDrag({
    type: 'Create',
    item,
    canDrag() {
      return !disabled
    },
    collect(monitor) {
      return {
        isDragging: monitor.isDragging(),
      }
    },
    options: {
      dropEffect: 'copy',
    },
  })
  useEffect(() => {
    preview(getEmptyImage(), { captureDraggingState: true })
  }, [preview])
  return (
    <div
      ref={drag}
      {...restProps}
      style={{
        ...restProps.style,
        opacity: isDragging ? 0.6 : 1,
      }}
    />
  )
}
