import { nanoid } from 'nanoid'
import { useDrop } from 'react-dnd'
import mergeRefs from 'react-merge-refs'
import useMeasure from 'react-use-measure'
import styled from 'styled-components/macro'
import {
  BuilderElement,
  ROOT_ELEMENT_ID,
  useAppBuilder,
  useAppBuilderConfigs,
  useAppBuilderDispatch,
} from '../../contexts/AppBuilderContext'
import { LeftSidebarElement } from '../../models'
import ElementContainer from '../ElementContainer'
import DragLayer, { getCoords } from './DragLayer'

const Wrapper = styled('div')`
  background: white;
  margin: 24px;
  padding: 16px;
  display: flex;
  border-radius: 4px;
`

const Content = styled('div')`
  flex: 1;
  isolation: isolate;
  position: relative;
  display: grid;
`

const Svg = styled('svg')`
  position: absolute;
  inset: 0;
  z-index: -1;
`

type CanvasProps = {
  id?: string
}

export default function Canvas({ id = ROOT_ELEMENT_ID }: CanvasProps) {
  const { elements } = useAppBuilder()
  const { columns } = useAppBuilderConfigs()
  const element = elements[id]
  const dispatch = useAppBuilderDispatch()
  const [measure, bounds] = useMeasure()
  const cellWidth = bounds.width / 12
  const cellHeight = 12
  const [{ isOver }, drop] = useDrop({
    accept: ['Create', 'Update'],
    collect(monitor) {
      return {
        isOver: monitor.isOver({ shallow: true }),
      }
    },
    drop(item: any, monitor) {
      if (monitor.didDrop()) {
        return
      }
      const { column, width, row } = getCoords(monitor.getClientOffset() || { x: 0, y: 0 }, {
        offsetX: bounds.left,
        offsetY: bounds.top,
        cellWidth,
        cellHeight,
        maxWidth: item.layoutProps.width,
        maxHeight: item.layoutProps.height,
        columns,
      })
      const itemType = monitor.getItemType()
      if (itemType === 'Create') {
        const { name, displayName, variableName, layoutProps, initialProps } = item as LeftSidebarElement
        const element: BuilderElement = {
          id: `e${nanoid(5)}`,
          name,
          displayName,
          variableName,
          layoutProps: {
            ...layoutProps,
            startCol: column,
            startRow: row,
            width,
          },
          props: initialProps,
          parentId: id,
          children: [],
        }
        dispatch({
          type: 'ADD_ELEMENT',
          payload: {
            element,
          },
        })
        return
      }
      if (itemType === 'Update') {
        const element = item as BuilderElement
        const newElement: BuilderElement = {
          ...element,
          layoutProps: {
            ...element.layoutProps,
            startCol: column,
            startRow: row,
            width: element.layoutProps.width > width ? width : element.layoutProps.width,
          },
        }
        dispatch({
          type: 'UPDATE_ELEMENT',
          payload: {
            element: newElement,
            targetId: id,
          },
        })
      }
    },
  })
  return (
    <Wrapper
      onClick={e => {
        e.stopPropagation()
        dispatch({ type: 'SELECT_ELEMENT', payload: { elementId: id } })
      }}>
      <Content
        ref={mergeRefs([measure, drop])}
        style={{
          // @ts-ignore
          '--cell-width': `${bounds.width / 12}px`,
          '--cell-height': '12px',
        }}>
        <Svg
          width={bounds.width}
          height={bounds.height}
          viewBox={`0 0 ${bounds.width + 1} ${bounds.height}`}
          style={{ opacity: isOver ? 1 : 0 }}>
          <defs>
            <pattern
              id="background-pattern"
              x={0}
              y={0}
              width={bounds.width > 0 ? cellWidth : 0}
              height={12}
              patternUnits="userSpaceOnUse">
              <rect x="0" y="0" width="100%" height="100%" fill="none" stroke="#ddd" strokeDasharray="4 2" />
            </pattern>
          </defs>
          <rect x="0" y="0" width="100%" height="100%" fill="url(#background-pattern)" />
        </Svg>
        {element.children.map(elementId => (
          <ElementContainer key={elementId} element={elements[elementId]} />
        ))}
        <DragLayer isOver={isOver} cellWidth={cellWidth} cellHeight={12} offsetX={bounds.left} offsetY={bounds.top} />
      </Content>
    </Wrapper>
  )
}

Canvas.displayName = 'Canvas'
