import { useDragLayer, XYCoord } from 'react-dnd'
import styled from 'styled-components/macro'
import { color } from '../../../../../components/GlobalStyle'
import { useAppBuilderConfigs } from '../../contexts/AppBuilderContext'

const Wrapper = styled('div')`
  position: absolute;
  inset: 0;
  user-select: none;
`

const DragItem = styled('div')`
  box-shadow: 0 0 0 1px ${color('Primary700')};
  overflow: hidden;
  user-select: none;
  position: absolute;
  &::after {
    content: '';
    position: absolute;
    user-select: none;
    inset: 2px;
    background: ${color('Primary200')};
    opacity: 0.6;
  }
`

type DragLayerProps = {
  cellWidth: number
  cellHeight: number
  offsetX: number
  offsetY: number
  isOver: boolean
}

export default function DragLayer({ cellWidth, cellHeight, offsetX, offsetY, isOver }: DragLayerProps) {
  const { columns } = useAppBuilderConfigs()
  const { isDragging, transform } = useDragLayer(monitor => {
    if (!monitor.isDragging()) {
      return {
        isDragging: false,
        transform: {
          column: 0,
          row: 0,
          width: 0,
          height: 0,
        },
      }
    }

    // console.log('monitor state')
    // console.log('offset', { offsetX, offsetY })
    // console.log('getInitialClientOffset', monitor.getInitialClientOffset())
    // console.log('getInitialSourceClientOffset', monitor.getInitialSourceClientOffset())
    // console.log('getClientOffset', monitor.getClientOffset())
    // console.log('getDifferenceFromInitialOffset', monitor.getDifferenceFromInitialOffset())
    // console.log('getSourceClientOffset', monitor.getSourceClientOffset())
    // console.log('end monitor state')

    const item = monitor.getItem()

    const xy = monitor.getClientOffset() || { x: 0, y: 0 }

    return {
      isDragging: monitor.isDragging(),
      transform: getCoords(xy, {
        cellWidth,
        cellHeight,
        offsetX,
        offsetY,
        columns,
        maxWidth: item.layoutProps.width,
        maxHeight: item.layoutProps.height,
      }),
    }
  })
  if (!isDragging || !isOver) {
    return null
  }
  const { width, height, column, row } = transform
  return (
    <Wrapper>
      <DragItem
        style={{
          width: width * cellWidth,
          height: height * cellHeight,
          transform: `translate(${column * cellWidth}px, ${row * cellHeight}px)`,
        }}
      />
    </Wrapper>
  )
}

DragLayer.displayName = 'DragLayer'

export function getCoords(
  xy: XYCoord,
  {
    cellWidth,
    cellHeight,
    offsetX,
    offsetY,
    columns,
    maxWidth = 4,
    maxHeight = 4,
  }: Record<'offsetX' | 'offsetY' | 'cellWidth' | 'cellHeight' | 'columns' | 'maxWidth' | 'maxHeight', number>,
) {
  const column = Math.min(Math.round((xy.x - offsetX) / cellWidth - 0.3), columns - 1)
  const width = Math.max(Math.min(columns - column, maxWidth), 1)
  const row = Math.round((xy.y - offsetY) / cellHeight)
  return {
    column,
    width,
    row,
    height: maxHeight,
  }
}
