import { createRef } from 'react'
import styled from 'styled-components/macro'

export const sidebarRef = createRef<HTMLDivElement>()

const Wrapper = styled('div')`
  background: white;
`

export default function RightSidebar() {
  return <Wrapper ref={sidebarRef} />
}

RightSidebar.displayName = 'RightSidebar'
