import { useEffect } from 'react'
import { useDrag } from 'react-dnd'
import { getEmptyImage } from 'react-dnd-html5-backend'
import styled from 'styled-components/macro'
import { color } from '../../../../components/GlobalStyle'
import { Portal } from '../../../../components/Portal'
import {
  BuilderElement,
  useAppBuilder,
  useAppBuilderConfigs,
  useAppBuilderDispatch,
} from '../contexts/AppBuilderContext'
import { sidebarRef } from './RightSidebar'

const DragHandle = styled('div')`
  cursor: move;
  position: absolute;
  bottom: calc(100% + 2px);
  left: 0;
  font-size: 10px;
  color: white;
  background: ${color('Primary700')};
  padding: 4px;
  border-radius: 2px;
  transition: opacity 0.2s ease-in-out;
  will-change: opaciy;
  opacity: 0;
  pointer-events: none;
`

const Wrapper = styled('div')`
  position: absolute;
  padding: 2px;
  display: flex;
  transition: background 0.2s ease-in-out, box-shadow 0.2s ease-in-out;
  will-change: background, box-shadow;
  &[data-selected='true'],
  &:hover {
    box-shadow: 0 0 0 1px ${color('Primary700')};
    background: ${color('Primary200')};
    & > .drag-handle {
      opacity: 1;
    }
  }
`

const Content = styled('div')`
  border: 1px solid ${color('Neutral300')};
  background: white;
  flex: 1;
  max-width: 100%;
`

type ElementContainerProps<Item extends Record<string, unknown>> = {
  element: BuilderElement<Item>
}

export default function ElementContainer<Item extends Record<string, unknown> = Record<string, unknown>>({
  element,
}: ElementContainerProps<Item>) {
  const { widgetsByName } = useAppBuilderConfigs()
  const { selectedElements } = useAppBuilder()
  const dispatch = useAppBuilderDispatch()
  const [{ isDragging }, drag, preview] = useDrag({
    type: 'Update',
    item: element,
    collect(monitor) {
      return {
        isDragging: monitor.isDragging(),
      }
    },
  })
  useEffect(() => {
    preview(getEmptyImage())
  }, [preview])
  const { layoutProps } = element
  const widget = widgetsByName[element.name]
  const Component = widget.builderComponent
  const Sidebar = widget.sidebar
  const isSelected = selectedElements === element.id
  return (
    <Wrapper
      ref={drag}
      onClick={e => {
        e.stopPropagation()
        dispatch({
          type: 'SELECT_ELEMENT',
          payload: {
            elementId: element.id,
          },
        })
      }}
      style={{
        opacity: isDragging ? 0 : 1,
        transform: `translate(calc(var(--cell-width) * ${layoutProps.startCol}), calc(var(--cell-height) * ${layoutProps.startRow}))`,
        width: `calc(var(--cell-width) * ${layoutProps.width})`,
        minHeight: `calc(var(--cell-height) * ${layoutProps.height})`,
      }}
      data-selected={`${isSelected}`}>
      <DragHandle className="drag-handle">{element.variableName || element.displayName}</DragHandle>
      <Content>
        {Component && <Component elementId={element.id} />}
        {!Component && <div>Unsupported: {element.name}</div>}
      </Content>
      {isSelected && (
        <Portal mountNode={sidebarRef}>
          <Sidebar elementId={element.id} />
        </Portal>
      )}
    </Wrapper>
  )
}

ElementContainer.displayName = 'ElementContainer'
