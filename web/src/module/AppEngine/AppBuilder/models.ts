import { AppBuilderWidget } from '../AppWidget/AppWidget'

export type LeftSidebarElement = Pick<
  AppBuilderWidget,
  'name' | 'displayName' | 'variableName' | 'layoutProps' | 'initialProps'
>
export type UpdateElement = {}
