import * as React from 'react'
import { ReactNode } from 'react-markdown'
import { createReducerContext } from '../../../../services/create-context'
import { AppBuilderWidget, WidgetCategory, WidgetLayoutProps } from '../../AppWidget/AppWidget'

type DeepPartial<T> = Partial<
  {
    [P in keyof T]: DeepPartial<T[P]> | any
  }
>

export const ROOT_ELEMENT_ID = 'ROOT_ELEMENT'

export type BuilderElement<Props = any> = {
  id: string
  name: string
  displayName: string
  variableName?: string
  layoutProps: WidgetLayoutProps
  props: Props
  parentId: string
  children: Array<string>
}

type DispatchActions =
  | {
      type: 'ADD_ELEMENT'
      payload: {
        element: BuilderElement
      }
    }
  | {
      type: 'UPDATE_ELEMENT'
      payload: {
        element: BuilderElement
        targetId: string
      }
    }
  | {
      type: 'SELECT_ELEMENT'
      payload: {
        elementId: string
      }
    }
  | {
      type: 'UPDATE_SELECTED_ELEMENT'
      payload: DeepPartial<BuilderElement<any>>
    }

type AppBuilderValue = {
  dataSources: Record<string, unknown>
  elements: Record<string, BuilderElement>
  selectedElements?: string
}

const initialValue: AppBuilderValue = {
  dataSources: {},
  elements: {
    [ROOT_ELEMENT_ID]: {
      id: ROOT_ELEMENT_ID,
      name: 'Root',
      displayName: 'Root',
      layoutProps: {
        width: 12,
        height: 12,
      },
      props: {},
      parentId: 'Root',
      children: [],
    },
  },
}

function reducer(store: AppBuilderValue, action: DispatchActions) {
  switch (action.type) {
    case 'ADD_ELEMENT': {
      const target = store.elements[action.payload.element.parentId]
      const newTarget = {
        ...target,
        children: [...target.children, action.payload.element.id],
      }
      return {
        ...store,
        elements: {
          ...store.elements,
          [action.payload.element.parentId]: newTarget,
          [action.payload.element.id]: action.payload.element,
        },
      }
    }
    case 'UPDATE_ELEMENT': {
      return {
        ...store,
        elements: {
          ...store.elements,
          [action.payload.element.id]: action.payload.element,
        },
      }
    }
    case 'SELECT_ELEMENT': {
      return {
        ...store,
        selectedElements: action.payload.elementId,
      }
    }
    case 'UPDATE_SELECTED_ELEMENT': {
      const element = store.elements[store.selectedElements!]
      return {
        ...store,
        elements: {
          ...store.elements,
          [element.id]: {
            ...element,
            props: {
              ...element.props,
              ...action.payload.props,
            },
            layoutProps: {
              ...element.layoutProps,
              ...action.payload.layoutProps,
            },
          },
        },
      }
    }
    default:
      return store
  }
}

const [Provider, useAppBuilder, useAppBuilderDispatch] = createReducerContext<AppBuilderValue, DispatchActions>(
  'AppBuilder',
)

type Configs = {
  versionCode?: number
  version?: string
  columns: number
  category: Array<WidgetCategory>
  widgets: Array<AppBuilderWidget>
}

type WidgetsByName = Record<string, AppBuilderWidget>
type WidgetsByCategory = Record<WidgetCategory, Array<AppBuilderWidget>>

type AppBuilderConfigs = Configs & { widgetsByName: WidgetsByName; widgetsByCategory: WidgetsByCategory }

const AppBuilderConfigsContext = React.createContext<AppBuilderConfigs | Error>(
  new Error('useAppBuilderConfigs should be used inside AppBuilderProvider'),
)

export const useAppBuilderConfigs = () => {
  const context = React.useContext(AppBuilderConfigsContext)
  if (context instanceof Error) {
    throw context
  }
  return context
}

type AppBuilderProviderProps = {
  children: ReactNode
  configs: Configs
}

export default function AppBuilderProvider({ children, configs }: AppBuilderProviderProps) {
  const builderConfigs = React.useMemo(() => {
    const widgetsByName = {} as WidgetsByName
    const widgetsByCategory = {} as WidgetsByCategory
    for (const widget of configs.widgets) {
      widgetsByName[widget.name] = widget
      widget.category.forEach(category => {
        widgetsByCategory[category] = (widgetsByCategory[category] || []).concat(widget)
      })
    }
    return {
      ...configs,
      widgetsByName,
      widgetsByCategory,
      versionCode: 2,
      version: '2.0.0',
    }
  }, [configs])
  return (
    <AppBuilderConfigsContext.Provider value={builderConfigs}>
      <Provider reducer={reducer} initialValue={initialValue}>
        {children}
      </Provider>
    </AppBuilderConfigsContext.Provider>
  )
}
export { useAppBuilder, useAppBuilderDispatch }
