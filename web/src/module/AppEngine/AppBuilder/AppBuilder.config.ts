import { WidgetCategory } from '../AppWidget/AppWidget'
import { TextInputWidget } from '../AppWidget/TextInput'

export const AppBuilderConfigs = {
  columns: 12,
  category: [WidgetCategory.Layout, WidgetCategory.TextInput, WidgetCategory.Data],
  widgets: [TextInputWidget],
}
