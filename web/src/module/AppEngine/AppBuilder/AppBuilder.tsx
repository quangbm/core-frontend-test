import styled from 'styled-components/macro'
import { color } from '../../../components/GlobalStyle'
import { AppBuilderConfigs } from './AppBuilder.config'
import { Canvas } from './components/Canvas'
import { LeftSidebar } from './components/LeftSidebar'
import { RightSidebar } from './components/RightSidebar'
import AppBuilderProvider from './contexts/AppBuilderContext'

const Wrapper = styled('div')`
  display: grid;
  grid-template: auto 1fr / 251px 1fr 250px;
  grid-gap: 2px;
  background: ${color('Neutral300')};
  min-height: 100vh;
`

const HeaderWrapper = styled('div')`
  grid-column: span 3;
  display: flex;
  min-height: 40px;
  background: white;
`

export default function AppBuilder() {
  return (
    <AppBuilderProvider configs={AppBuilderConfigs}>
      <Wrapper>
        <HeaderWrapper>Header</HeaderWrapper>
        <LeftSidebar />
        <Canvas />
        <RightSidebar />
      </Wrapper>
    </AppBuilderProvider>
  )
}

AppBuilder.displayName = 'AppBuilder'
