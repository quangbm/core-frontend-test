import { ReactComponent as Flow } from './Component/Flow.svg'
import { ReactComponent as ObjectList } from './Component/ObjectList.svg'
import { ReactComponent as RecordDetails } from './Component/RecordDetails.svg'
import { ReactComponent as RelatedList } from './Component/RelatedList.svg'
import { ReactComponent as Button } from './Field/Button.svg'
import { ReactComponent as Checkbox } from './Field/Checkbox.svg'
import { ReactComponent as Dropdown } from './Field/Dropdown.svg'
import { ReactComponent as Input } from './Field/Input.svg'
import { ReactComponent as Link } from './Field/Link.svg'
import { ReactComponent as Paragraph } from './Field/Paragraph.svg'
import { ReactComponent as Radio } from './Field/Radio.svg'
import { ReactComponent as Title } from './Field/Title.svg'
import { ReactComponent as Accordion } from './Layout/Accordion.svg'
import { ReactComponent as Divider } from './Layout/Divider.svg'
import { ReactComponent as Section } from './Layout/Section.svg'
import { ReactComponent as Tab } from './Layout/Tab.svg'
import { ReactComponent as Preview } from './Nav/Preview.svg'
import { ReactComponent as Redo } from './Nav/Redo.svg'
import { ReactComponent as Undo } from './Nav/Undo.svg'

export const FieldIcons = {
  Button,
  Checkbox,
  Dropdown,
  Input,
  Link,
  Paragraph,
  Radio,
  Title,
}
export const LayoutIcons = {
  Accordion,
  Divider,
  Section,
  Tab,
}
export const NavIcons = {
  Preview,
  Redo,
  Undo,
}

export const ComponentIcons = {
  ObjectList,
  RecordDetails,
  Flow,
  RelatedList,
}
