import * as React from 'react'

export function createReducerContext<Value, Actions>(name: string) {
  const ValueContext = React.createContext<Value | Error>(new Error(`use${name} should be used inside ${name}Provider`))
  const ActionContext = React.createContext<React.Dispatch<Actions> | Error>(
    new Error(`use${name}Dispatch should be used inside ${name}Provider`),
  )
  function useValue() {
    const ctx = React.useContext(ValueContext)
    if (ctx instanceof Error) {
      throw ctx
    }
    return ctx
  }
  function useDispatch() {
    const ctx = React.useContext(ActionContext)
    if (ctx instanceof Error) {
      throw ctx
    }
    return ctx
  }
  function Provider({
    children,
    reducer,
    initialValue = {} as Value,
  }: {
    children: React.ReactNode
    reducer: (v: Value, a: Actions) => Value
    initialValue?: Value
  }) {
    const [value, dispatch] = React.useReducer(reducer, initialValue)
    return (
      <ActionContext.Provider value={dispatch}>
        <ValueContext.Provider value={value}>{children}</ValueContext.Provider>
      </ActionContext.Provider>
    )
  }
  return [Provider, useValue, useDispatch] as const
}
