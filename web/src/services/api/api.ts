import { createApi, createRequest } from '../../../../shared/src/api'
import { ACCESS_TOKEN_KEY } from '../../hooks/useAuth'
import { queryClient } from '../react-query'

const { request, setToken, clearToken, setBaseUrl } = createRequest(process.env.REACT_APP_API_HOST || '', () => {
  localStorage.removeItem(ACCESS_TOKEN_KEY)
  queryClient.invalidateQueries()
})

export function getScreenFlowSSEUrl(ctxId: string) {
  return `${process.env.REACT_APP_API_HOST || ''}/v1/sse/flow/${ctxId}`
}

const { APIServices, QueryKeys } = createApi(request)

export { setToken, clearToken, APIServices, QueryKeys, request, setBaseUrl }
