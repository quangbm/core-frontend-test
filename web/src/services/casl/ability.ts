import { defineAbility } from '@casl/ability'
import { ProfileAccessResponse } from '../../../../shared/src/api'

export const getAbility = (feature?: Record<string, ProfileAccessResponse>) => {
  return defineAbility((can, cannot) => {
    if (feature) {
      const hasSetupPermission =
        feature &&
        Object.keys(feature).some(key => {
          if (feature[key].read) {
            return true
          }
          return false
        })

      hasSetupPermission && can('read', 'setup')

      Object.keys(feature).forEach(featureKey => {
        feature[featureKey].read ? can('read', featureKey) : cannot('read', featureKey)
        feature[featureKey].create ? can('create', featureKey) : cannot('create', featureKey)
        feature[featureKey].update ? can('update', featureKey) : cannot('update', featureKey)
        feature[featureKey].delete ? can('delete', featureKey) : cannot('delete', featureKey)
      })
    }
  })
}
