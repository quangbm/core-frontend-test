import { defineAbility } from '@casl/ability'
import { createContextualCan } from '@casl/react'
import { createContext } from 'react'

export const AbilityContext = createContext(defineAbility(() => {}))
export const Can = createContextualCan(AbilityContext.Consumer)
