import { SelectOption } from '../../../components/NSelect/model'

export const FLOW_BUILDER_NAME = 'flow-builder'
export const FLOW_LEFT_SIDEBAR = 'FLOW_LEFT_SIDEBAR'

export const FlowIconContainerClassName = 'flow-icon-container'

export const NODE_WIDTH = 48
export const NODE_HEIGHT = 48
export const CELL_WIDTH = 24
export const CELL_HEIGHT = 24
export const BORDER_RADIUS = 12

export const TRIGGER_NAME = '*'
export const TRIGGER_POSITION = 144

export const TRIGGER_CONNECTION = {
  maxConnections: 1,
  hasFaultConnection: false,
}

export const TRIGGER_WIDTH = NODE_WIDTH
export const TRIGGER_HEIGHT = NODE_HEIGHT

export const BIG_TRIGGER_WIDTH = 220
export const BIG_TRIGGER_HEIGHT = 140

export const ATTRIBUTE_OPTIONS: SelectOption[] = [
  {
    label: 'Variable',
    value: 'variable',
  },
  {
    label: 'Custom',
    value: 'config',
  },
]
