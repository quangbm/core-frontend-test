import { CELL_HEIGHT, CELL_WIDTH, TRIGGER_NAME, TRIGGER_POSITION } from './constants'
import { FlowNode, SourceXY, TargetXY, XY } from './types'

export function snapToGrid(xy: XY, grid = [CELL_WIDTH, CELL_HEIGHT]): XY {
  const [width, height] = grid
  const { x, y } = xy
  return {
    x: Math.round(x / width) * width,
    y: Math.round(y / height) * height,
  }
}

export function getSourceTargetXY(
  nodes: FlowNode[],
  ids: { targetName: string; sourceName: string },
): TargetXY & SourceXY
export function getSourceTargetXY(nodes: FlowNode[], ids: { sourceName: string; targetName?: string }): SourceXY
export function getSourceTargetXY(nodes: FlowNode[], ids: { targetName: string; sourceId?: string }): TargetXY
export function getSourceTargetXY(nodes: any, names: any) {
  const { sourceName, targetName } = names
  if (!sourceName && !targetName) {
    throw Error('Required one of sourceId or targetId')
  }
  const { sourceX, sourceY, targetX, targetY } = nodes.reduce(
    (accumulator: any, node: FlowNode) => {
      if (node.name === sourceName) {
        return {
          ...accumulator,
          sourceX: node.position.x,
          sourceY: node.position.y,
        }
      }
      if (node.name === targetName) {
        return {
          ...accumulator,
          targetX: node.position.x,
          targetY: node.position.y,
        }
      }
      return accumulator
    },
    {
      sourceX: sourceName === TRIGGER_NAME ? TRIGGER_POSITION : undefined,
      sourceY: sourceName === TRIGGER_NAME ? TRIGGER_POSITION : undefined,
    } as Partial<SourceXY & TargetXY>,
  )
  if (sourceName && targetName) {
    return { sourceX, sourceY, targetX, targetY } as SourceXY & TargetXY
  }
  if (sourceName) {
    return { sourceX, sourceY } as SourceXY
  }
  return { targetX, targetY } as TargetXY
}
