import { get } from 'lodash'
import { ContentValue } from '../../../../../shared/src/api'
import { FlowItem } from '../../../../../shared/src/api/endpoints/flows'
import { DataFlowItemConfigType, FlowNode, NodeType } from './types'

export function fromFlowItemToFlowNode(flowItem: FlowItem): FlowNode | undefined {
  const { position } = flowItem.nodeAttributes as Record<string, any>

  return {
    id: flowItem.id,
    name: flowItem.name,
    position: position || { x: 0, y: 0 },
    type: flowItem.type as NodeType,
    outcome: [],
    data: flowItem,
  }
}

//// SUPPORT FUNCTIONS

export function parseConfigArrayToConfigObject(array: DataFlowItemConfigType[]) {
  return array.reduce((object, curField) => {
    return {
      [curField.fieldName]: curField.value,
      ...object,
    }
  }, {} as Record<string, ContentValue>)
}

export function parseConfigObjectToConfigArray(object: Record<string, ContentValue> = {}) {
  const keys = Object.keys(object)
  return keys.map(key => {
    return {
      fieldName: key,
      value: object[key],
    } as DataFlowItemConfigType
  })
}

export const renderVariableValue = (value?: ContentValue, content?: Record<string, any>) => {
  if (!value) {
    return ''
  }

  if (value.type === 'variable') {
    if (content) {
      return get(content, value.value)
    }
    return `{${value.value}}`
  }

  return value.value
}
