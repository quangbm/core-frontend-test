import { FC, useMemo } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useParams } from 'react-router'
import styled from 'styled-components/macro'
import { CollectionAggregationFlowItemResponse, ContentValue } from '../../../../../../shared/src/api'
import { spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../components/NPerfectScrollbar'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../services/api'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { ContentValuePicker } from '../../FlowBuilderVariable/ContentValuePicker'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'

const METHODS = [
  {
    value: 'sum',
    label: 'SUM',
  },
  {
    value: 'average',
    label: 'AVERAGE',
  },
  {
    value: 'max',
    label: 'MAX',
  },
  {
    value: 'min',
    label: 'MIN',
  },
  {
    value: 'count',
    label: 'COUNT',
  },
  {
    value: 'concat',
    label: 'CONCAT',
  },
]

const ScrollWrapper = styled(NPerfectScrollbar)`
  max-height: 70vh;
`
const FormWrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`

export const collectionAggregationWidget: WidgetItemType = {
  leftIcon: <FlowIcons.CollectionAggregation />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Secondary900">
      <FlowIcons.CollectionAggregation />
    </FlowIconContainer>
  ),
  label: <abbr title="Collection Aggregation">Collection Agg</abbr>,
  type: NodeType.CollectionAggregation,
  data: {
    name: '',
    displayName: '',
    attributes: {},
  },
}

export const CollectionAggregationModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  const { validateFunction } = useValidateString()
  const { register, getValues, setValue, formState, handleSubmit, control, watch } = useForm({
    defaultValues: nodeModal?.data
      ? flowItemToCollectionAgg(nodeModal.data as CollectionAggregationFlowItemResponse)
      : {},
  })
  const selectedVariableName = watch('collectionVariableName')
  const { flowName, version } = useParams<{ flowName: string; version: string }>()

  const { data: variables } = useQuery(
    [QueryKeys.Flows.getFlowTemplateVariables, { flowName, version }],
    APIServices.Flows.getFlowTemplateVariables,
  )

  const selectedVariable = useMemo(() => {
    if (selectedVariableName && variables?.data) {
      return variables?.data.find(variable => variable.name === selectedVariableName.value)
    }
  }, [selectedVariableName, variables?.data])

  const { data: fieldOptions = [], isLoading: isLoadingFields } = useQuery(
    [QueryKeys.Flows.getFlowVariableSchema, { variable: selectedVariable?.name || '', flowName, version }],
    APIServices.Flows.getFlowVariableSchema,
    {
      enabled:
        !!selectedVariable?.name && //@ts-ignore server type
        (selectedVariable?.attributes?.itemType === 'object' ||
          //@ts-ignore
          selectedVariable?.attributes?.itemType === 'record'),
      select: res => {
        return res.data.map(field => ({
          value: field.name,
          label: field.displayName,
        }))
      },
    },
  )

  const onFormSubmit = (data: any) => {
    onSubmit({
      name: data.name,
      displayName: data.displayName,
      description: data.description,
      type: nodeModal.type,
      attributes: {
        method: data.method,
        collectionVariableName: data.collectionVariableName.value,
        fieldName: data.fieldName,
      },
      nodeAttributes: {
        position: nodeModal.position,
      },
    })
  }

  return (
    <>
      <NModal.Header onClose={onCancel} title={`${nodeModal.nodeId ? 'Edit' : 'New'} Collection Aggregation`} />
      <NModal.Body>
        <ScrollWrapper>
          <FormWrapper>
            <NRow>
              <NColumn flex={1}>
                <NTextInput
                  {...register('displayName', { validate: validateFunction })}
                  required
                  error={formState.errors.displayName?.message || formState.errors.displayName?.type}
                  placeholder="Display name"
                  label="Display Name"
                  onBlur={e => {
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>

              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput
                  disabled={!!nodeModal.nodeId}
                  {...register('name', { validate: !nodeModal.nodeId ? validateFunction : undefined })}
                  required
                  error={formState.errors.name?.message || formState.errors.name?.type}
                  placeholder="API Name"
                  label="API Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={1}>
                <NTextArea rows={4} {...register('description')} label="Description" />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn>
                <Controller
                  control={control}
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  name="collectionVariableName"
                  render={({ field: { value, onChange }, fieldState }) => {
                    return (
                      <ContentValuePicker
                        label="Collection Variable"
                        placeholder="Select variable"
                        error={
                          fieldState.error?.message ||
                          (selectedVariableName && selectedVariable?.type !== 'collection'
                            ? 'Invalid collection variable'
                            : undefined)
                        }
                        selectedValue={value}
                        onSelectValue={value => {
                          onChange(value)
                          setValue('fieldName', undefined)
                        }}
                        onClear={() => {
                          setValue('collectionVariableName', undefined)
                          setValue('fieldName', undefined)
                        }}
                        allowTypes={{ variable: true }}
                        required
                      />
                    )
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn>
                <Controller
                  control={control}
                  name="fieldName"
                  render={({ field: { value, onChange }, fieldState }) => {
                    return (
                      <NSingleSelect
                        disabled={
                          selectedVariable?.type !== 'collection' ||
                          //@ts-ignore server type
                          (selectedVariable?.attributes?.itemType !== 'object' &&
                            //@ts-ignore
                            selectedVariable?.attributes?.itemType !== 'record')
                        }
                        fullWidth
                        options={fieldOptions}
                        placeholder="Select field"
                        error={fieldState.error?.message}
                        value={value}
                        onValueChange={onChange}
                        label="Field"
                        isLoading={isLoadingFields}
                      />
                    )
                  }}
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={0.5}>
                <Controller
                  control={control}
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  name="method"
                  render={({ field: { value, onChange }, fieldState }) => {
                    return (
                      <NSingleSelect
                        fullWidth
                        options={METHODS}
                        placeholder="Select method"
                        error={fieldState.error?.message}
                        value={value}
                        onValueChange={onChange}
                        label="Method"
                        required
                      />
                    )
                  }}
                />
              </NColumn>
              <NDivider size="xl" vertical />
            </NRow>
          </FormWrapper>
        </ScrollWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(onFormSubmit)} />
    </>
  )
}

const flowItemToCollectionAgg = (flowItem: CollectionAggregationFlowItemResponse) => {
  return {
    name: flowItem.name,
    displayName: flowItem.displayName,
    description: flowItem.description,
    method: flowItem.attributes?.method,
    collectionVariableName: flowItem.attributes?.collectionVariableName
      ? ({
          value: flowItem.attributes?.collectionVariableName,
          type: 'variable',
        } as ContentValue)
      : undefined,
    fieldName: flowItem.attributes?.fieldName || undefined,
  }
}
