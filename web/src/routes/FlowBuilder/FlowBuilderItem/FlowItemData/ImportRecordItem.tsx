import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useTheme } from 'styled-components/macro'
import {
  ContentValue,
  FieldSchema,
  ImportRecordsBatchPayload,
  ImportRecordsFlowItemResponse,
} from '../../../../../../shared/src/api'
import { color } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NAccordion } from '../../../../components/NAccordion/NAccordion'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../services/api'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { ContentValuePicker } from '../../FlowBuilderVariable/ContentValuePicker'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { AddBtn } from './components/AddBtn'
import { AccordionTitle, ArrowWrapper, Row, ScrollableWrapper, Section } from './components/CommonComponents'

type ObjectSchemaType = {
  fieldOptions: SelectOption[]
  fieldsByName: Record<string, FieldSchema>
}

export function ImportRecordModal({ nodeModal, isSubmitting, onCancel, onSubmit }: FlowNodeModalProps) {
  const { control, register, formState, getValues, setValue, watch, handleSubmit } = useForm<{
    name: string
    displayName: string
    description: string
    attributes: {
      fileId: ContentValue
      objectName: string
      payload: Array<{ fieldName: string; value: ContentValue }>
      batchPayload: ImportRecordsBatchPayload
    }
  }>({
    defaultValues: nodeModal.data
      ? transformFlowItemToImportRecord(nodeModal.data as ImportRecordsFlowItemResponse)
      : {},
  })

  const attributes = watch('attributes') || {}

  const { fields, append, remove } = useFieldArray({ control, name: `attributes.payload` })

  const theme = useTheme()
  const { validateFunction } = useValidateString()

  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')

  // get all objects
  const { data: objectOptions, isFetching: isLoadingObjects } = useQuery(
    [QueryKeys.Builder.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  // get object fields schema
  const { data: { fieldOptions } = { fieldOptions: [], fieldsByName: {} } as ObjectSchemaType } = useQuery(
    [QueryKeys.Builder.getObjectSchema, { objName: attributes?.objectName }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: Boolean(attributes.objectName),
      select: data => {
        const { fieldOptions, fieldsByName } = data.data.fields.reduce(
          (allFields, field) => {
            const newFieldOptions = field.isReadOnly
              ? allFields.fieldOptions
              : [...allFields.fieldOptions, { value: field.name, label: field.displayName }]
            return {
              fieldOptions: newFieldOptions,
              fieldsByName: { ...allFields.fieldsByName, [field.name]: field },
            }
          },
          { fieldOptions: [], fieldsByName: {} } as {
            fieldOptions: SelectOption[]
            fieldsByName: Record<string, FieldSchema>
          },
        )
        return { fieldOptions, fieldsByName, actions: [] }
      },
    },
  )

  const submitForm = (data: any) => {
    onSubmit({
      name: data.name,
      displayName: data.displayName,
      description: data.description,
      type: nodeModal.type,
      nodeAttributes: {
        position: nodeModal.position,
      },
      attributes: {
        fileId: data.attributes.fileId,
        batchPayload: data.attributes.batchPayload,
        config: {
          objectName: data.attributes.objectName,
          payload: data.attributes.payload.reduce(
            (a: Record<string, ContentValue>, i: { fieldName: string; value: ContentValue }) => {
              a[i.fieldName] = i.value
              return a
            },
            {} as Record<string, ContentValue>,
          ),
        },
      },
    })
  }

  return (
    <>
      <NModal.Header onClose={onCancel} title={`${!!nodeModal.nodeId ? 'Edit' : 'New'} import record`} />
      <NModal.Body>
        <ScrollableWrapper>
          <Section>
            <NRow>
              <NColumn flex={1}>
                <NTextInput
                  {...register('displayName', {
                    validate: validateFunction,
                  })}
                  required
                  name="displayName"
                  error={formState.errors.displayName?.message}
                  placeholder="Display name"
                  label="Display Name"
                  onBlur={e => {
                    if (!!nodeModal.nodeId) {
                      return
                    }
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput
                  disabled={!!nodeModal.nodeId}
                  {...register('name', {
                    validate: validateFunction,
                    pattern: {
                      value: fieldNameRegex,
                      message: 'Invalid pattern',
                    },
                  })}
                  required
                  error={formState.errors.name?.message}
                  name="name"
                  placeholder="API name"
                  label="API Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={1}>
                <NTextArea rows={3} {...register('description')} name="description" label="Description" />
              </NColumn>
            </NRow>
          </Section>
          <NDivider lineColor={color('Neutral200')({ theme })} />
          <Section>
            <NRow>
              <NColumn flex={1}>
                <Controller
                  name="attributes.fileId"
                  control={control}
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  render={({ field: { value, onChange }, fieldState }) => {
                    return (
                      <ContentValuePicker
                        required
                        label="File Id"
                        error={fieldState.error?.message}
                        selectedValue={value}
                        onSelectValue={onChange}
                        allowTypes={{ variable: true, value: true }}
                      />
                    )
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <Controller
                  name="attributes.objectName"
                  defaultValue=""
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  control={control}
                  render={({ field, fieldState }) => {
                    return (
                      <NSingleSelect
                        disabled={!Boolean(attributes.fileId)}
                        label="CSV Object"
                        required
                        options={objectOptions || []}
                        searchValue={searchValue}
                        onSearchValueChange={handleSearchChange}
                        isLoading={isLoadingObjects}
                        error={fieldState.error?.message}
                        isSearchable
                        fullWidth
                        placeholder="Select object"
                        value={field.value}
                        onValueChange={value => {
                          field.onChange(value)
                        }}
                      />
                    )
                  }}
                />
              </NColumn>
            </NRow>
          </Section>
          {attributes.objectName && (
            <>
              <NDivider size={1} lineSize={1} lineColor={color('Neutral200')({ theme })} />
              <Section>
                <NAccordion title={<AccordionTitle>Record Configs</AccordionTitle>} iconPosition="right">
                  {fields.map((field, index) => {
                    return (
                      <Row key={field.id}>
                        <Controller
                          name={`attributes.payload.${index}.fieldName`}
                          defaultValue={field.fieldName || ''}
                          rules={{
                            required: {
                              value: true,
                              message: 'Required',
                            },
                          }}
                          control={control}
                          render={({ field: { value, onChange }, fieldState }) => {
                            let options: SelectOption[] = []
                            if (fieldOptions) {
                              options = fieldOptions.filter(option => {
                                return (
                                  option.value === value || !attributes?.payload.some(f => f.fieldName === option.value)
                                )
                              })
                            }

                            return (
                              <NSingleSelect
                                required
                                containerClassName="picker"
                                options={options}
                                placeholder="Select field"
                                error={fieldState.error?.message}
                                value={value}
                                onValueChange={onChange}
                              />
                            )
                          }}
                        />
                        <ArrowWrapper className="icon">
                          <GlobalIcons.LeftArrow />
                        </ArrowWrapper>
                        <Controller
                          name={`attributes.payload.${index}.value`}
                          defaultValue={field.value}
                          control={control}
                          rules={{
                            required: {
                              value: true,
                              message: 'Required',
                            },
                          }}
                          render={({ field: { value, onChange }, fieldState }) => {
                            return (
                              <ContentValuePicker
                                className="picker"
                                error={fieldState.error?.message}
                                selectedValue={value}
                                onSelectValue={onChange}
                                allowTypes={{ variable: true, value: true }}
                              />
                            )
                          }}
                        />
                        <NButton
                          type="ghost"
                          icon={<GlobalIcons.Trash />}
                          disabled={fields.length < 1}
                          onClick={() => remove(index)}
                        />
                      </Row>
                    )
                  })}
                  <AddBtn
                    onClick={() =>
                      append({
                        fieldName: undefined,
                        value: undefined,
                      })
                    }
                  />
                </NAccordion>
                <NAccordion title={<AccordionTitle>Batch Configs</AccordionTitle>} iconPosition="right">
                  <NRow>
                    <NColumn flex={1}>
                      <Controller
                        name="attributes.batchPayload.userRefId"
                        control={control}
                        render={({ field, fieldState }) => {
                          return (
                            <ContentValuePicker
                              required
                              label="User Ref Id"
                              error={fieldState.error?.message}
                              selectedValue={field.value}
                              onSelectValue={field.onChange}
                              allowTypes={{ variable: true, value: true }}
                            />
                          )
                        }}
                      />
                    </NColumn>
                    <NDivider vertical size="xl" />
                    <NColumn flex={1}>
                      <Controller
                        name="attributes.batchPayload.batchCtx"
                        control={control}
                        render={({ field, fieldState }) => {
                          return (
                            <ContentValuePicker
                              required
                              label="Batch Context"
                              error={fieldState.error?.message}
                              selectedValue={field.value}
                              onSelectValue={field.onChange}
                              allowTypes={{ variable: true, value: true }}
                            />
                          )
                        }}
                      />
                    </NColumn>
                  </NRow>
                </NAccordion>
              </Section>
            </>
          )}
        </ScrollableWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(submitForm)} />
    </>
  )
}

export const importRecordWidget: WidgetItemType = {
  leftIcon: <FlowIcons.ImportRecord />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Tertiary900">
      <FlowIcons.ImportRecord />
    </FlowIconContainer>
  ),
  label: <abbr title="Import Record">Import Rcrd</abbr>,
  type: NodeType.ImportRecord,
  data: {
    name: '',
    displayName: '',
    attributes: {
      batchPayload: {},
    },
  },
}

const transformFlowItemToImportRecord = (flowItem: ImportRecordsFlowItemResponse) => {
  const payload = []
  for (const [fieldName, value] of Object.entries(flowItem.attributes?.config?.payload || {})) {
    payload.push({ fieldName, value })
  }
  return {
    name: flowItem.name,
    displayName: flowItem.displayName,
    description: flowItem.description,
    attributes: {
      fileId: flowItem.attributes?.fileId,
      objectName: flowItem.attributes.config?.objectName,
      payload,
      batchPayload: flowItem.attributes?.batchPayload,
    },
  }
}
