import React, { FC } from 'react'
import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { useTheme } from 'styled-components/macro'
import { color } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { RequiredIndicator } from '../../../../components/NTypography'
import { useValidateString } from '../../../../hooks/useValidateString'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { ContentValuePicker } from '../../FlowBuilderVariable/ContentValuePicker'
import { DataFormType } from '../../models'
import { FlowIconContainerClassName } from '../../utils/constants'
import { parseConfigObjectToConfigArray } from '../../utils/functions'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { AssignValueForm } from './components/AssignValueForm'
import { Header, ScrollableWrapper, Section } from './components/CommonComponents'

export const UserCreateModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  const formMethods = useForm<DataFormType>({
    defaultValues: nodeModal.data || {
      config: [{ fieldName: '', value: '' }],
    },
  })
  const { register, formState, setValue, control, handleSubmit, getValues } = formMethods
  const theme = useTheme()
  const { validateFunction } = useValidateString()
  const fieldArray = useFieldArray({
    control,
    name: 'payload',
  })

  const submitForm = (data: any) => {
    onSubmit({
      name: data.name,
      displayName: data.displayName,
      description: data.description,
      type: nodeModal.type,
      nodeAttributes: { position: nodeModal.position },
      payload: parseConfigObjectToConfigArray(data.attributes.payload),
      ssoId: data.attributes.ssoId,
      profileName: data.attributes.profileName,
      roleName: data.attributes.roleName,
    })
  }

  return (
    <>
      <NModal.Header onClose={onCancel} title={`${!!nodeModal.nodeId ? 'Edit' : 'New'} create user`} />
      <NModal.Body>
        <ScrollableWrapper>
          <Section>
            <NRow>
              <NColumn flex={1}>
                <NTextInput
                  {...register('displayName', {
                    validate: validateFunction,
                  })}
                  required
                  name="displayName"
                  error={formState.errors.displayName?.message}
                  placeholder="Display name"
                  label="Display Name"
                  onBlur={e => {
                    if (!!nodeModal.nodeId) {
                      return
                    }
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput
                  disabled={!!nodeModal.nodeId}
                  {...register('name', {
                    validate: validateFunction,
                    pattern: {
                      value: fieldNameRegex,
                      message: 'Invalid pattern',
                    },
                  })}
                  required
                  error={formState.errors.name?.message}
                  name="name"
                  placeholder="API name"
                  label="API Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={1}>
                <NTextArea rows={3} {...register('description')} name="description" label="Description" />
              </NColumn>
            </NRow>
          </Section>
          <NDivider lineColor={color('Neutral200')({ theme })} />
          <Section>
            <Header>
              <RequiredIndicator>*</RequiredIndicator>Create an User
            </Header>
          </Section>
          <NDivider size={1} lineSize={1} lineColor={color('Neutral200')({ theme })} />

          <Section>
            <NRow>
              <NColumn>
                <Controller
                  name={`ssoId`}
                  control={control}
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  render={({ field: { value, onChange }, fieldState }) => {
                    return (
                      <ContentValuePicker
                        required
                        label="Sso ID"
                        error={fieldState.error?.message}
                        selectedValue={value}
                        onSelectValue={onChange}
                        allowTypes={{ variable: true, value: true }}
                      />
                    )
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn>
                <Controller
                  name={`profileName`}
                  control={control}
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  render={({ field: { value, onChange }, fieldState }) => {
                    return (
                      <ContentValuePicker
                        required
                        label="Profile Name"
                        error={fieldState.error?.message}
                        selectedValue={value}
                        onSelectValue={onChange}
                        allowTypes={{ variable: true, value: true }}
                      />
                    )
                  }}
                />
              </NColumn>
            </NRow>
            <NDivider size="md" />
            <NRow>
              <NColumn flex={0.5}>
                <Controller
                  name={`roleName`}
                  control={control}
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  render={({ field: { value, onChange }, fieldState }) => {
                    return (
                      <ContentValuePicker
                        required
                        label="Role Name"
                        error={fieldState.error?.message}
                        selectedValue={value}
                        onSelectValue={onChange}
                        allowTypes={{ variable: true, value: true }}
                      />
                    )
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
            </NRow>
          </Section>
          <Section>
            <AssignValueForm fieldArray={fieldArray} formMethods={formMethods} objectName={'user'} />
          </Section>
        </ScrollableWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(submitForm)} />
    </>
  )
}

export const userCreateWidget: WidgetItemType = {
  leftIcon: <FlowIcons.UserCreate />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Tertiary900">
      <FlowIcons.UserCreate />
    </FlowIconContainer>
  ),
  label: 'Create User',
  type: NodeType.UserCreate,
  data: {
    name: '',
    displayName: '',
    objectName: '',
  },
}
