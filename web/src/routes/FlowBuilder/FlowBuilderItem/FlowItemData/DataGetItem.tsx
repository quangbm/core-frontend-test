import React, { FC } from 'react'
import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled, { useTheme } from 'styled-components/macro'
import {
  ContentValue,
  FieldSchema,
  RecordReadConfig,
  RecordReadFlowItemResponse,
} from '../../../../../../shared/src/api'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { NRadio } from '../../../../components/NRadio/NRadio'
import { SelectOption } from '../../../../components/NSelect/model'
import { NMultiSelect } from '../../../../components/NSelect/NMultiSelect'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { RequiredIndicator, typography } from '../../../../components/NTypography'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../services/api'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { ContentValuePicker } from '../../FlowBuilderVariable/ContentValuePicker'
import { DataFormType, ObjectSchemaType } from '../../models'
import { FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { Header, ScrollableWrapper, Section } from './components/CommonComponents'
import { FilterRecordRules } from './components/FilterRecordRules'

const SubHeader = styled.p`
  ${typography('small-bold-text')}
  color: ${color('Neutral400')};
  margin-bottom: ${spacing('md')};
`
const ErrorText = styled.p`
  color: ${color('Red700')};
  margin-top: ${spacing('xs')};
`

export const DataGetModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onSubmit, onCancel }) => {
  const formMethods = useForm<DataFormType>({
    defaultValues: nodeModal.data
      ? transformFlowItemToGetRecord(nodeModal.data as RecordReadFlowItemResponse)
      : {
          condition: 'and',
          query: [{ fieldName: '' }],
        },
  })
  const { handleSubmit, formState, setValue, register, control, watch, getValues } = formMethods
  const fieldArray = useFieldArray({
    control,
    name: 'query',
  })

  const theme = useTheme()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')

  const watchObjectName = watch('objectName')
  const { validateFunction } = useValidateString()

  // get all objects
  const { data: objectOptions, isFetching: isLoadingObjects } = useQuery(
    [QueryKeys.Builder.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  // get object fields schema
  const {
    data: { fieldOptions } = { fieldOptions: [], fieldsByName: {} } as ObjectSchemaType,
    isLoading: isLoadingFields,
  } = useQuery([QueryKeys.Builder.getObjectSchema, { objName: watchObjectName }], APIServices.Builder.getObjectSchema, {
    enabled: !!watchObjectName,
    select: res => {
      const { fieldOptions, fieldsByName } = res.data.fields.reduce(
        (allFields, field) => {
          return {
            fieldOptions: [...allFields.fieldOptions, { value: field.name, label: field.displayName }],
            fieldsByName: { ...allFields.fieldsByName, [field.name]: field },
          }
        },
        { fieldOptions: [], fieldsByName: {} } as {
          fieldOptions: SelectOption[]
          fieldsByName: Record<string, FieldSchema>
        },
      )
      return { fieldOptions, fieldsByName, actions: [] }
    },
  })

  const submitForm = (data: any) => {
    onSubmit({
      type: nodeModal.type,
      name: data.name,
      displayName: data.displayName,
      description: data.description,
      attributes: {
        config: {
          objectName: data.objectName,
          conditionLogic: data.condition as string,
          conditions: data.query,
          sortBy: data.sortBy as RecordReadConfig['sortBy'],
          sortOrder: data.sortOrder as RecordReadConfig['sortOrder'],
          getOptions: data.getOptions as RecordReadConfig['getOptions'],
          getFieldNames: data.getFieldNames,
          storeDataToVar: data.storeDataToVar?.value,
        } as RecordReadConfig,
      },
      nodeAttributes: { position: nodeModal.position },
    })
  }

  return (
    <>
      <NModal.Header title={`${!!nodeModal.nodeId ? 'Edit' : 'New'} get record`} onClose={onCancel} />
      <NModal.Body>
        <ScrollableWrapper>
          <Section>
            <NRow>
              <NColumn flex={1}>
                <NTextInput
                  {...register('displayName', {
                    validate: validateFunction,
                  })}
                  required
                  error={formState.errors.displayName?.message}
                  placeholder="Display name"
                  label="Display Name"
                  onBlur={e => {
                    if (!!nodeModal.nodeId) {
                      return
                    }
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput
                  disabled={!!nodeModal.nodeId}
                  {...register('name', {
                    validate: validateFunction,
                    pattern: {
                      value: fieldNameRegex,
                      message: 'Invalid pattern',
                    },
                  })}
                  required
                  error={formState.errors.name?.message}
                  placeholder="API name"
                  label="API Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={1}>
                <NTextArea rows={3} {...register('description')} label="Description" />
              </NColumn>
            </NRow>
          </Section>
          <NDivider lineColor={color('Neutral200')({ theme })} />
          <Section>
            <Header>
              <RequiredIndicator>*</RequiredIndicator>Get record from this Object
            </Header>
            <NRow>
              <NColumn>
                <Controller
                  name="objectName"
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  control={control}
                  render={({ field, fieldState }) => (
                    <NSingleSelect
                      required
                      options={objectOptions || []}
                      searchValue={searchValue}
                      onSearchValueChange={handleSearchChange}
                      isLoading={isLoadingObjects}
                      isSearchable
                      fullWidth
                      label="Object"
                      placeholder="Select object"
                      error={fieldState.error?.message}
                      value={field.value}
                      onValueChange={value => {
                        if (value !== watchObjectName) {
                          setValue('query', [])
                          setValue('getFieldNames', undefined)
                        }
                        field.onChange(value)
                      }}
                    />
                  )}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn>
                <Controller
                  name="storeDataToVar"
                  control={control}
                  render={({ field: { value, onChange }, fieldState }) => {
                    return (
                      <ContentValuePicker
                        label="Store data to var"
                        error={fieldState.error?.message}
                        selectedValue={value}
                        onSelectValue={onChange}
                        allowTypes={{ variable: true, value: true }}
                      />
                    )
                  }}
                />
              </NColumn>
            </NRow>
            <NDivider size="sm" />
            <Controller
              name="getFieldNames"
              control={control}
              render={({ field, fieldState }) => (
                <NMultiSelect
                  options={fieldOptions || []}
                  label="Get field names"
                  placeholder="Select fields"
                  error={fieldState.error?.message}
                  values={field.value || []}
                  onValuesChange={value => {
                    field.onChange(value)
                  }}
                  allowClear
                  onClear={() => setValue('getFieldNames', undefined)}
                  fullWidth
                />
              )}
            />
          </Section>
          {watchObjectName && (
            <>
              <NDivider lineColor={color('Neutral200')({ theme })} />
              <Section>
                <FilterRecordRules
                  fieldArray={fieldArray}
                  formMethods={formMethods}
                  fieldOptions={fieldOptions}
                  required
                />
              </Section>

              <NDivider lineColor={color('Neutral200')({ theme })} />
              <Section>
                <Header>Sort record</Header>
                <NRow>
                  <Controller
                    name="sortOrder"
                    control={control}
                    render={({ field, fieldState }) => (
                      <NSingleSelect
                        options={[{ value: 'DESC' }, { value: 'ASC' }]}
                        placeholder="Select order"
                        value={field.value}
                        onValueChange={field.onChange}
                        label="Sort Order"
                        error={fieldState.error?.message}
                      />
                    )}
                  />
                  <NDivider vertical size="sm" />
                  <Controller
                    name="sortBy"
                    control={control}
                    render={({ field, fieldState }) => (
                      <NSingleSelect
                        isLoading={isLoadingFields}
                        options={fieldOptions}
                        placeholder="Select field"
                        value={field.value}
                        onValueChange={value => {
                          field.onChange(value)
                        }}
                        label="Sort By"
                        error={fieldState.error?.message}
                      />
                    )}
                  />
                </NRow>
                <NDivider size="sm" />
                <SubHeader>How Many Records to Store</SubHeader>
                <Controller
                  control={control}
                  name="getOptions"
                  render={({ field, fieldState }) => {
                    return (
                      <>
                        <NRadio
                          name="getOptions"
                          value="one"
                          checked={field.value === 'one'}
                          onChange={e => field.onChange(e.target.value)}
                          title="Only the first record"
                        />
                        <NDivider size="sm" />
                        <NRadio
                          name="getOptions"
                          value="all"
                          onChange={e => field.onChange(e.target.value)}
                          checked={field.value === 'all'}
                          title="All records"
                        />
                        {fieldState.error?.message && <ErrorText>{fieldState.error.message}</ErrorText>}
                      </>
                    )
                  }}
                  rules={{ required: { value: true, message: 'Required' } }}
                />
              </Section>
            </>
          )}
        </ScrollableWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(submitForm)} />
    </>
  )
}

export const dataGetWidget: WidgetItemType = {
  leftIcon: <FlowIcons.DataGet />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Tertiary900">
      <FlowIcons.DataGet />
    </FlowIconContainer>
  ),
  label: <abbr title="Get Record">Get Rcrd</abbr>,
  type: NodeType.DataGet,
  data: {
    name: '',
    displayName: '',
    condition: 'and',
    config: [{ fieldName: '' }],
    objectName: '',
    sortOrder: 'DESC',
  },
}

const transformFlowItemToGetRecord = (flowItem: RecordReadFlowItemResponse) => {
  return {
    name: flowItem.name,
    displayName: flowItem.displayName,
    objectName: flowItem.attributes?.config?.objectName,
    description: flowItem.description,
    sortBy: flowItem.attributes?.config?.sortBy,
    sortOrder: flowItem.attributes?.config?.sortOrder,
    getOptions: flowItem.attributes?.config?.getOptions,
    condition: flowItem.attributes?.config?.conditionLogic,
    query: flowItem.attributes?.config?.conditions,
    storeDataToVar:
      flowItem.attributes?.config?.storeDataToVar &&
      ({ value: flowItem.attributes?.config.storeDataToVar, type: 'variable' } as ContentValue),
    getFieldNames: flowItem.attributes?.config?.getFieldNames,
  }
}
