import * as React from 'react'
import { Controller, useFieldArray, UseFormReturn } from 'react-hook-form'
import { GlobalIcons } from '../../../../../components/Icons'
import { NAccordion } from '../../../../../components/NAccordion/NAccordion'
import { NButton } from '../../../../../components/NButton/NButton'
import { ContentValuePicker } from '../../../FlowBuilderVariable/ContentValuePicker'
import { DataFormType } from '../../../models'
import { AddBtn } from './AddBtn'
import { AccordionTitle, Row } from './CommonComponents'

type AttachmentsProps = {
  children?: React.ReactNode
  formMethods: UseFormReturn<DataFormType>
}

export const Attachments = React.memo<AttachmentsProps>(function Node({ formMethods }) {
  const updateFields = useFieldArray({ control: formMethods.control, name: 'mediaUpdate' })
  const removeFields = useFieldArray({ control: formMethods.control, name: 'mediaRemove' })
  return (
    <React.Fragment>
      <NAccordion title={<AccordionTitle>Update Media</AccordionTitle>} iconPosition="right" initialOpen={false}>
        {updateFields.fields.map((field, fieldIndex) => {
          return (
            <Row key={field.id}>
              <Controller
                name={`mediaUpdate.${fieldIndex}.mediaId`}
                defaultValue={field.mediaId || ''}
                control={formMethods.control}
                rules={{
                  required: {
                    value: true,
                    message: 'Required',
                  },
                }}
                render={({ field: { value, onChange }, fieldState }) => {
                  return (
                    <ContentValuePicker
                      label={(fieldIndex < 1 && 'Media Id') || undefined}
                      error={fieldState.error?.message}
                      selectedValue={value}
                      onSelectValue={onChange}
                      allowTypes={{ variable: true, value: true }}
                    />
                  )
                }}
              />
              <Controller
                name={`mediaUpdate.${fieldIndex}.tagName`}
                control={formMethods.control}
                rules={{
                  required: {
                    value: true,
                    message: 'Required',
                  },
                }}
                render={({ field: { value, onChange }, fieldState }) => {
                  return (
                    <ContentValuePicker
                      label={(fieldIndex < 1 && 'Tag Name') || undefined}
                      error={fieldState.error?.message}
                      selectedValue={value}
                      onSelectValue={onChange}
                      allowTypes={{ variable: true, value: true }}
                    />
                  )
                }}
              />
              <NButton type="ghost" icon={<GlobalIcons.Trash />} onClick={() => updateFields.remove(fieldIndex)} />
            </Row>
          )
        })}
        <AddBtn
          onClick={() => {
            updateFields.append({ mediaId: undefined, tagName: undefined })
          }}
        />
      </NAccordion>
      <NAccordion title={<AccordionTitle>Remove Media</AccordionTitle>} iconPosition="right" initialOpen={false}>
        {removeFields.fields.map((field, fieldIndex) => {
          return (
            <Row key={field.id}>
              <Controller
                name={`mediaRemove.${fieldIndex}.mediaId`}
                defaultValue={field.mediaId || ''}
                control={formMethods.control}
                rules={{
                  required: {
                    value: true,
                    message: 'Required',
                  },
                }}
                render={({ field: { value, onChange }, fieldState }) => {
                  return (
                    <ContentValuePicker
                      label={(fieldIndex < 1 && 'Media Id') || undefined}
                      error={fieldState.error?.message}
                      selectedValue={value}
                      onSelectValue={onChange}
                      allowTypes={{ variable: true, value: true }}
                    />
                  )
                }}
              />
              <NButton type="ghost" icon={<GlobalIcons.Trash />} onClick={() => removeFields.remove(fieldIndex)} />
            </Row>
          )
        })}
        <AddBtn
          onClick={() => {
            removeFields.append({ mediaId: undefined })
          }}
        />
      </NAccordion>
    </React.Fragment>
  )
})

Attachments.displayName = 'Attachments'
