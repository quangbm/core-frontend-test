import React, { FC } from 'react'
import { Controller, UseFieldArrayReturn, UseFormReturn } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled, { useTheme } from 'styled-components/macro'
import { FieldSchema } from '../../../../../../../shared/src/api'
import { color } from '../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../components/Icons'
import { NAccordion } from '../../../../../components/NAccordion/NAccordion'
import { NButton } from '../../../../../components/NButton/NButton'
import { SelectOption } from '../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { ContentValuePicker } from '../../../FlowBuilderVariable/ContentValuePicker'
import { DataFormType } from '../../../models'
import { CommonWidgetDataType, DataFlowItemConfigType } from '../../../utils/types'
import { AddBtn } from './AddBtn'
import { AccordionTitle, ArrowWrapper, Row } from './CommonComponents'

const LoadingContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 44px;
  justify-content: center;
  align-items: center;
`

const ErrorContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 44px;
`

export type FormType = {
  config: DataFlowItemConfigType[]
} & CommonWidgetDataType

type Props = {
  objectName: string
  formMethods: UseFormReturn<DataFormType>
  fieldArray: UseFieldArrayReturn<DataFormType, 'payload', 'id'>
}

type ObjectSchemaType = {
  fieldOptions: SelectOption[]
  fieldsByName: Record<string, FieldSchema>
}

export const AssignValueForm: FC<Props> = ({ formMethods, objectName, fieldArray }) => {
  const theme = useTheme()
  const { control, watch } = formMethods

  // get object fields schema
  const {
    data: { fieldOptions, fieldsByName } = { fieldOptions: [], fieldsByName: {} } as ObjectSchemaType,
    isLoading,
  } = useQuery([QueryKeys.Builder.getObjectSchema, { objName: objectName }], APIServices.Builder.getObjectSchema, {
    select: data => {
      const { fieldOptions, fieldsByName } = data.data.fields.reduce(
        (allFields, field) => {
          const newFieldOptions = field.isReadOnly
            ? allFields.fieldOptions
            : [...allFields.fieldOptions, { value: field.name, label: field.displayName }]
          return {
            fieldOptions: newFieldOptions,
            fieldsByName: { ...allFields.fieldsByName, [field.name]: field },
          }
        },
        { fieldOptions: [], fieldsByName: {} } as {
          fieldOptions: SelectOption[]
          fieldsByName: Record<string, FieldSchema>
        },
      )
      return { fieldOptions, fieldsByName, actions: [] }
    },
  })

  const { fields, append, remove } = fieldArray

  const watchConfig = watch('payload') as DataFlowItemConfigType[]

  if (isLoading) {
    return (
      <LoadingContainer>
        <NSpinner size={22} strokeWidth={2} color={color('Primary700')({ theme })} />
      </LoadingContainer>
    )
  }

  if (!fieldOptions || !fieldsByName || fieldOptions.length === 0) {
    return <ErrorContainer>Object '{objectName}' doesn't have any fields</ErrorContainer>
  }

  return (
    <NAccordion
      initialOpen
      title={<AccordionTitle>Set Field Values for this Object</AccordionTitle>}
      iconPosition="right">
      {fields.map((field, index) => {
        return (
          <Row key={field.id}>
            <Controller
              name={`payload.${index}.fieldName`}
              defaultValue={field.fieldName || ''}
              rules={{
                required: {
                  value: true,
                  message: 'Required',
                },
              }}
              control={control}
              render={({ field: { value, onChange }, fieldState }) => {
                let options: SelectOption[] = []
                if (fieldOptions) {
                  options = fieldOptions.filter(option => {
                    return option.value === value || !watchConfig.some(f => f.fieldName === option.value)
                  })
                }

                return (
                  <NSingleSelect
                    required
                    containerClassName="picker"
                    options={options}
                    placeholder="Select field"
                    error={fieldState.error?.message}
                    value={value}
                    onValueChange={onChange}
                  />
                )
              }}
            />
            <ArrowWrapper className="icon">
              <GlobalIcons.LeftArrow />
            </ArrowWrapper>
            <Controller
              name={`payload.${index}.value`}
              defaultValue={field.value}
              control={control}
              rules={{
                required: {
                  value: true,
                  message: 'Required',
                },
              }}
              render={({ field: { value, onChange }, fieldState }) => {
                return (
                  <ContentValuePicker
                    className="picker"
                    error={fieldState.error?.message}
                    selectedValue={value}
                    onSelectValue={onChange}
                    allowTypes={{ variable: true, value: true }}
                  />
                )
              }}
            />
            <NButton
              type="ghost"
              icon={<GlobalIcons.Trash />}
              disabled={fields.length < 1}
              onClick={() => remove(index)}
            />
          </Row>
        )
      })}
      {fields.length < fieldOptions.length && (
        <AddBtn
          onClick={() =>
            append({
              fieldName: undefined,
              value: undefined,
            })
          }
        />
      )}
    </NAccordion>
  )
}
