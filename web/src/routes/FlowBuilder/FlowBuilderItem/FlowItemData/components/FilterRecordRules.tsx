import React, { FC } from 'react'
import { Controller, UseFieldArrayReturn, UseFormReturn } from 'react-hook-form'
import { GlobalIcons } from '../../../../../components/Icons'
import { NAccordion } from '../../../../../components/NAccordion/NAccordion'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../components/NDivider'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { SelectOption } from '../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { ContentValuePicker } from '../../../FlowBuilderVariable/ContentValuePicker'
import { DataFormType } from '../../../models'
import { AccordionTitle, AssignElement } from '../components/CommonComponents'
import { AddBtn } from './AddBtn'

const CONDITION_OPTIONS: SelectOption[] = [
  { value: 'and', label: 'All Conditions Are Met (AND)' },
  // { value: 'or', label: 'Any Conditions Is Met (OR)' },
  // { value: 'custom', label: 'Custom Condition Logic Is Met' },
]

const OPERATOR_OPTIONS: SelectOption[] = [
  { value: 'equals', label: 'Equal' },
  { value: 'notEquals', label: 'Not Equal' },
  { value: 'isNull', label: 'Is Null' },
  { value: 'isNotNull', label: 'Is Not Null' },
  { value: 'isIn', label: 'Is In' },
  { value: 'isNotIn', label: 'Is Not In' },
  { value: 'isLike', label: 'Is Like' },
  { value: 'isILike', label: 'Is ILike' },
  { value: 'isMoreThan', label: 'Is More Than' },
  { value: 'isLessThan', label: 'Is Less Than' },
  { value: 'isMoreThanOrEqual', label: 'Is More Than Or Equal' },
  { value: 'isLessThanOrEqual', label: 'Is Less Than Or Equal' },
]

const notRequiredValueOperators = ['isNull', 'isNotNull']

type Props = {
  formMethods: UseFormReturn<DataFormType>
  fieldOptions: SelectOption[]
  required?: boolean
  fieldArray: UseFieldArrayReturn<DataFormType, 'query', 'id'>
}

export const FilterRecordRules: FC<Props> = ({ formMethods, fieldOptions, required, fieldArray }) => {
  const { control, watch } = formMethods

  const { fields, append, remove } = fieldArray

  return (
    <NAccordion title={<AccordionTitle>Filter record</AccordionTitle>} iconPosition="right" initialOpen={true}>
      <Controller
        control={control}
        name="condition"
        render={({ field, fieldState }) => (
          <NSingleSelect
            options={CONDITION_OPTIONS}
            placeholder="Select Condition"
            value={field.value}
            label="Condition Requirements to Execute Outcome"
            onValueChange={field.onChange}
            error={fieldState.error?.message}
          />
        )}
        rules={{ required: required && { value: required, message: 'Required' } }}
      />
      <NDivider size="lg" />

      {fields.map((field, index) => {
        return (
          <AssignElement key={`rule-${index}`}>
            <NRow className="picker-container">
              <Controller
                name={`query[${index}].fieldName`}
                defaultValue={field.fieldName || ''}
                control={control}
                render={({ field: { value, onChange }, fieldState }) => (
                  <NSingleSelect
                    fullWidth
                    options={fieldOptions}
                    placeholder="Select field"
                    value={value}
                    onValueChange={onChange}
                    error={fieldState.error?.message}
                  />
                )}
                rules={{ required: required && { value: required, message: 'Required' } }}
              />
              <NDivider vertical size="sm" />
              <Controller
                name={`query[${index}].sign`}
                defaultValue={field.sign || ''}
                control={control}
                render={({ field: { value, onChange }, fieldState }) => (
                  <NSingleSelect
                    options={OPERATOR_OPTIONS}
                    placeholder="Select operator"
                    value={value}
                    onValueChange={onChange}
                    error={fieldState.error?.message}
                  />
                )}
                rules={{ required: required && { value: required, message: 'Required' } }}
              />
              <NDivider vertical size="sm" />
              <Controller
                name={`query[${index}].value`}
                defaultValue={field.value || ''}
                control={control}
                render={({ field: { value, onChange }, fieldState }) => (
                  <ContentValuePicker
                    selectedValue={value}
                    onSelectValue={onChange}
                    error={fieldState.error?.message}
                    style={{ width: '100%' }}
                  />
                )}
                rules={{
                  required: required &&
                    !notRequiredValueOperators.find(op => op === watch(`query[${index}].sign`)) && {
                      value: true,
                      message: 'Required',
                    },
                }}
              />
            </NRow>
            <NButton
              type="ghost"
              icon={<GlobalIcons.Trash />}
              disabled={fields.length < 1}
              onClick={() => remove(index)}
            />
          </AssignElement>
        )
      })}
      <AddBtn
        onClick={() =>
          append({
            fieldName: '',
          })
        }
      />
    </NAccordion>
  )
}
