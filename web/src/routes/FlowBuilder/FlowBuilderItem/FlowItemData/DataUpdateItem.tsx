import { FC, useState } from 'react'
import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useTheme } from 'styled-components/macro'
import { FieldSchema, RecordUpdateConfig, RecordUpdateFlowItemResponse } from '../../../../../../shared/src/api'
import { ContentValue, RecordCreateFlowItemResponse } from '../../../../../../shared/src/api/models'
import { color } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { RequiredIndicator } from '../../../../components/NTypography'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../services/api'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { ContentValuePicker } from '../../FlowBuilderVariable/ContentValuePicker'
import { AttributeType, DataFormType, ObjectSchemaType } from '../../models'
import { ATTRIBUTE_OPTIONS, FlowIconContainerClassName } from '../../utils/constants'
import { parseConfigArrayToConfigObject, parseConfigObjectToConfigArray } from '../../utils/functions'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { AssignValueForm } from './components/AssignValueForm'
import { Attachments } from './components/Attachments'
import { Header, ScrollableWrapper, Section } from './components/CommonComponents'
import { FilterRecordRules } from './components/FilterRecordRules'

export const DataUpdateModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  const formMethods = useForm<DataFormType>({
    defaultValues: nodeModal.data
      ? transformFlowItemToUpdateRecord(nodeModal.data as RecordUpdateFlowItemResponse)
      : {
          payload: [{ fieldName: '' }],
          query: [{ fieldName: '' }],
        },
  })
  const { register, formState, setValue, control, handleSubmit, watch, getValues } = formMethods
  const [attributeType, setAttributeType] = useState<AttributeType | undefined>(
    ((nodeModal.data as RecordCreateFlowItemResponse)?.attributes?.variableName && 'variable') ||
      ((nodeModal.data as RecordCreateFlowItemResponse)?.attributes?.config?.payload && 'config'),
  )

  const theme = useTheme()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')
  const watchObjectName = watch('objectName')
  const { validateFunction } = useValidateString()
  const payloadFieldArray = useFieldArray({
    control,
    name: 'payload',
  })
  const query = useFieldArray({
    control,
    name: 'query',
  })

  const { data: { fieldOptions } = { fieldOptions: [], fieldsByName: {} } as ObjectSchemaType } = useQuery(
    [QueryKeys.Builder.getObjectSchema, { objName: watchObjectName }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: !!watchObjectName,
      select: res => {
        const { fieldOptions, fieldsByName } = res.data.fields.reduce(
          (allFields, field) => {
            return {
              fieldOptions: [...allFields.fieldOptions, { value: field.name, label: field.displayName }],
              fieldsByName: { ...allFields.fieldsByName, [field.name]: field },
            }
          },
          { fieldOptions: [], fieldsByName: {} } as {
            fieldOptions: SelectOption[]
            fieldsByName: Record<string, FieldSchema>
          },
        )
        return { fieldOptions, fieldsByName, actions: [] }
      },
    },
  )

  // get all objects
  const { data: objectOptions, isFetching: isLoadingObjects } = useQuery(
    [QueryKeys.Builder.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  const submitForm = (data: any) => {
    onSubmit({
      name: data.name,
      displayName: data.displayName,
      description: data.description,
      type: nodeModal.type,
      attributes: {
        config: data.objectName
          ? ({
              objectName: data.objectName,
              payload: parseConfigArrayToConfigObject(data.payload),
              conditionLogic: data.condition && (data.condition as string),
              conditions: data.query,
            } as RecordUpdateConfig)
          : undefined,
        media: {
          update: data.mediaUpdate,
          remove: data.mediaRemove?.map((item: any) => item.mediaId),
        },
        variableName: data.variable?.value,
      },
      nodeAttributes: {
        position: nodeModal.position,
      },
    })
  }

  return (
    <>
      <NModal.Header title={`${!!nodeModal.nodeId ? 'Edit' : 'New'} update record`} onClose={onCancel} />
      <NModal.Body>
        <ScrollableWrapper>
          <Section>
            <NRow>
              <NColumn flex={1}>
                <NTextInput
                  {...register('displayName', {
                    validate: validateFunction,
                  })}
                  required
                  error={formState.errors.displayName?.message || formState.errors.displayName?.type}
                  placeholder="Display name"
                  label="Display Name"
                  onBlur={e => {
                    if (!!nodeModal.nodeId) {
                      return
                    }
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput
                  disabled={!!nodeModal.nodeId}
                  {...register('name', {
                    validate: validateFunction,
                    pattern: {
                      value: fieldNameRegex,
                      message: 'Invalid pattern',
                    },
                  })}
                  required
                  error={formState.errors.name?.message}
                  placeholder="API name"
                  label="API Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={1}>
                <NTextArea rows={3} {...register('description')} name="description" label="Description" />
              </NColumn>
            </NRow>
          </Section>
          <NDivider lineColor={color('Neutral200')({ theme })} />
          <Section>
            <Header>
              <RequiredIndicator>*</RequiredIndicator>Update a Record
            </Header>
            <NRow>
              <NColumn flex={0.5}>
                <NSingleSelect
                  fullWidth
                  value={attributeType}
                  options={ATTRIBUTE_OPTIONS}
                  onValueChange={value => {
                    if (value === 'variable') {
                      formMethods.setValue('payload', undefined as never)
                      formMethods.setValue('objectName', undefined as never)
                      formMethods.setValue('query', undefined as never)
                    }
                    if (value === 'config') {
                      formMethods.setValue('variable', undefined)
                    }
                    setAttributeType(value as AttributeType)
                  }}
                />
              </NColumn>
            </NRow>
          </Section>
          {attributeType === 'variable' && (
            <Section>
              <NRow>
                <NColumn flex={0.5}>
                  <Controller
                    name={`variable`}
                    control={control}
                    rules={{
                      required: {
                        value: true,
                        message: 'Required',
                      },
                    }}
                    render={({ field: { value, onChange }, fieldState }) => {
                      return (
                        <ContentValuePicker
                          label="Variable"
                          error={fieldState.error?.message}
                          selectedValue={value}
                          onSelectValue={onChange}
                          allowTypes={{ variable: true, value: true }}
                        />
                      )
                    }}
                  />
                </NColumn>
              </NRow>
              <NDivider size="md" />
              <Attachments formMethods={formMethods} />
            </Section>
          )}
          {attributeType === 'config' && (
            <Section>
              <NRow>
                <NColumn flex={0.5}>
                  <Controller
                    name="objectName"
                    rules={{
                      required: {
                        value: true,
                        message: 'Required',
                      },
                    }}
                    control={control}
                    render={({ field, fieldState }) => {
                      return (
                        <NSingleSelect
                          required
                          options={objectOptions || []}
                          searchValue={searchValue}
                          onSearchValueChange={handleSearchChange}
                          isLoading={isLoadingObjects}
                          isSearchable
                          fullWidth
                          placeholder="Select object"
                          error={fieldState.error?.message}
                          value={field.value}
                          onValueChange={value => {
                            if (value !== watchObjectName) {
                              setValue('payload', [])
                              setValue('query', [])
                              payloadFieldArray.append({ fieldName: '' })
                            }
                            field.onChange(value)
                          }}
                        />
                      )
                    }}
                  />
                </NColumn>
              </NRow>
              <NDivider size="md" />
              {watchObjectName && (
                <>
                  <AssignValueForm
                    fieldArray={payloadFieldArray}
                    formMethods={formMethods}
                    objectName={watchObjectName}
                  />
                  <NDivider size="md" />
                  <FilterRecordRules
                    fieldArray={query}
                    formMethods={formMethods}
                    fieldOptions={fieldOptions}
                    required
                  />
                  <Attachments formMethods={formMethods} />
                </>
              )}
            </Section>
          )}
        </ScrollableWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(submitForm)} />
    </>
  )
}

export const dataUpdateWidget: WidgetItemType = {
  leftIcon: <FlowIcons.DataUpdate />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Tertiary900">
      <FlowIcons.DataUpdate />
    </FlowIconContainer>
  ),
  label: <abbr title="Update Record">Update Rcrd</abbr>,
  type: NodeType.DataUpdate,
  data: {
    name: '',
    displayName: '',
    objectName: '',
    guid: '',
    config: [{ fieldName: '' }],
  },
}

const transformFlowItemToUpdateRecord = (flowItem: RecordUpdateFlowItemResponse) => {
  return {
    name: flowItem.name,
    displayName: flowItem.displayName,
    description: flowItem.description,
    payload: parseConfigObjectToConfigArray(flowItem.attributes?.config?.payload),
    query: flowItem.attributes?.config?.conditions,
    condition: flowItem.attributes?.config?.conditionLogic,
    objectName: flowItem.attributes?.config?.objectName,
    variable:
      flowItem.attributes?.variableName &&
      ({ value: flowItem.attributes?.variableName, type: 'variable' } as ContentValue),
    mediaUpdate: flowItem.attributes?.media?.update,
    mediaRemove: flowItem.attributes?.media?.remove?.map(item => ({ mediaId: item })),
  }
}
