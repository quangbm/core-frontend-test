import { FC, useState } from 'react'
import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useTheme } from 'styled-components/macro'
import {
  ContentValue,
  FieldSchema,
  RecordDeleteConfig,
  RecordDeleteFlowItemResponse,
} from '../../../../../../shared/src/api'
import { color } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { RequiredIndicator } from '../../../../components/NTypography'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../services/api'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { ContentValuePicker } from '../../FlowBuilderVariable/ContentValuePicker'
import { AttributeType, DataFormType, ObjectSchemaType } from '../../models'
import { ATTRIBUTE_OPTIONS, FlowIconContainerClassName } from '../../utils/constants'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { Header, Section } from './components/CommonComponents'
import { FilterRecordRules } from './components/FilterRecordRules'

export const DataDeleteModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  const formMethods = useForm<DataFormType>({
    defaultValues: nodeModal.data
      ? transformFlowItemToDeleteRecord(nodeModal.data as RecordDeleteFlowItemResponse)
      : {
          name: '',
          displayName: '',
          objectName: '',
          guid: '',
        },
  })

  const { handleSubmit, formState, setValue, register, control, getValues, watch } = formMethods
  const watchObjectName = watch('objectName')

  const theme = useTheme()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')
  const { validateFunction } = useValidateString()
  const [attributeType, setAttributeType] = useState<AttributeType | undefined>(
    ((nodeModal.data as RecordDeleteFlowItemResponse)?.attributes?.variableName && 'variable') ||
      ((nodeModal.data as RecordDeleteFlowItemResponse)?.attributes?.config && 'config'),
  )

  // get all objects
  const { data: objectOptions, isFetching: isLoadingObjects } = useQuery(
    [QueryKeys.Builder.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  const query = useFieldArray({
    control,
    name: 'query',
  })

  const { data: { fieldOptions } = { fieldOptions: [], fieldsByName: {} } as ObjectSchemaType } = useQuery(
    [QueryKeys.Builder.getObjectSchema, { objName: watchObjectName }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: !!watchObjectName,
      select: res => {
        const { fieldOptions, fieldsByName } = res.data.fields.reduce(
          (allFields, field) => {
            return {
              fieldOptions: [...allFields.fieldOptions, { value: field.name, label: field.displayName }],
              fieldsByName: { ...allFields.fieldsByName, [field.name]: field },
            }
          },
          { fieldOptions: [], fieldsByName: {} } as {
            fieldOptions: SelectOption[]
            fieldsByName: Record<string, FieldSchema>
          },
        )
        return { fieldOptions, fieldsByName, actions: [] }
      },
    },
  )

  const submitForm = (data: any) => {
    onSubmit({
      name: data.name,
      displayName: data.displayName,
      description: data.description,
      type: nodeModal.type,
      attributes: {
        config: data.objectName
          ? ({
              objectName: data.objectName,
              conditionLogic: data.condition && (data.condition as string),
              conditions: data.query,
            } as RecordDeleteConfig)
          : undefined,
        variableName: data.variable?.value,
      },
      nodeAttributes: {
        position: nodeModal.position,
      },
    })
  }

  return (
    <>
      <NModal.Header title={`${!!nodeModal.nodeId ? 'Edit' : 'New'} delete record`} onClose={onCancel} />
      <NModal.Body>
        <Section>
          <NRow>
            <NColumn flex={1}>
              <NTextInput
                {...register('displayName', {
                  validate: validateFunction,
                })}
                required
                error={formState.errors.displayName?.message}
                placeholder="Display name"
                label="Display Name"
                onBlur={e => {
                  if (!!nodeModal.nodeId) {
                    return
                  }
                  const formatName = getNameFromDisplayName(e.target.value)
                  !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                }}
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn flex={1}>
              <NTextInput
                disabled={!!nodeModal.nodeId}
                {...register('name', {
                  validate: validateFunction,
                  pattern: {
                    value: fieldNameRegex,
                    message: 'Invalid pattern',
                  },
                })}
                required
                error={formState.errors.name?.message}
                name="name"
                placeholder="API name"
                label="API Name"
              />
            </NColumn>
          </NRow>
          <NDivider size="xl" />
          <NRow>
            <NColumn flex={1}>
              <NTextArea rows={3} {...register('description')} label="Description" />
            </NColumn>
          </NRow>
        </Section>
        <NDivider lineColor={color('Neutral200')({ theme })} />
        <Section>
          <Header>
            <RequiredIndicator>*</RequiredIndicator>Delete a Record
          </Header>
          <NRow>
            <NColumn flex={0.5}>
              <NSingleSelect
                fullWidth
                value={attributeType}
                options={ATTRIBUTE_OPTIONS}
                onValueChange={value => {
                  if (value === 'variable') {
                    formMethods.setValue('query', undefined as never)
                    formMethods.setValue('objectName', undefined as never)
                  }
                  if (value === 'config') {
                    formMethods.setValue('variable', undefined)
                  }
                  setAttributeType(value as AttributeType)
                }}
              />
            </NColumn>
          </NRow>
        </Section>
        {attributeType === 'variable' && (
          <Section>
            <NRow>
              <NColumn flex={0.5}>
                <Controller
                  name={`variable`}
                  control={control}
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  render={({ field: { value, onChange }, fieldState }) => {
                    return (
                      <ContentValuePicker
                        label="Variable"
                        error={fieldState.error?.message}
                        selectedValue={value}
                        onSelectValue={onChange}
                        allowTypes={{ variable: true, value: true }}
                      />
                    )
                  }}
                />
              </NColumn>
            </NRow>
          </Section>
        )}
        {attributeType === 'config' && (
          <Section>
            <NRow>
              <NColumn flex={0.5}>
                <Controller
                  name="objectName"
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  control={control}
                  render={({ field, fieldState }) => (
                    <NSingleSelect
                      required
                      options={objectOptions || []}
                      searchValue={searchValue}
                      onSearchValueChange={handleSearchChange}
                      isLoading={isLoadingObjects}
                      isSearchable
                      fullWidth
                      placeholder="Select object"
                      error={fieldState.error?.message}
                      value={field.value}
                      onValueChange={value => {
                        if (value !== watchObjectName) {
                          setValue('query', [])
                        }
                        field.onChange(value)
                      }}
                    />
                  )}
                />
              </NColumn>
            </NRow>
            <NDivider size="md" />
            {watchObjectName && (
              <FilterRecordRules fieldArray={query} formMethods={formMethods} fieldOptions={fieldOptions} required />
            )}
          </Section>
        )}
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(submitForm)} />
    </>
  )
}

export const dataDeleteWidget: WidgetItemType = {
  leftIcon: <FlowIcons.DataDelete />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Tertiary900">
      <FlowIcons.DataDelete />
    </FlowIconContainer>
  ),
  label: <abbr title="Delete Record">Delete Rcrd</abbr>,
  type: NodeType.DataDelete,
  data: {
    name: '',
    displayName: '',
    objectName: '',
    guid: '',
  },
}

const transformFlowItemToDeleteRecord = (flowItem: RecordDeleteFlowItemResponse) => {
  return {
    name: flowItem.name,
    displayName: flowItem.displayName,
    description: flowItem.description,
    objectName: flowItem.attributes?.config?.objectName,
    query: flowItem.attributes?.config?.conditions,
    condition: flowItem.attributes?.config?.conditionLogic,
    variable:
      flowItem.attributes?.variableName &&
      ({ value: flowItem.attributes?.variableName, type: 'variable' } as ContentValue),
  }
}
