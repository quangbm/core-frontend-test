import React, { FC, useState } from 'react'
import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useTheme } from 'styled-components/macro'
import { ContentValue, RecordCreateConfig, RecordCreateFlowItemResponse } from '../../../../../../shared/src/api/models'
import { color } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { RequiredIndicator } from '../../../../components/NTypography'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../services/api'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { ContentValuePicker } from '../../FlowBuilderVariable/ContentValuePicker'
import { AttributeType, DataFormType } from '../../models'
import { ATTRIBUTE_OPTIONS, FlowIconContainerClassName } from '../../utils/constants'
import { parseConfigArrayToConfigObject, parseConfigObjectToConfigArray } from '../../utils/functions'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { AssignValueForm } from './components/AssignValueForm'
import { Attachments } from './components/Attachments'
import { Header, ScrollableWrapper, Section } from './components/CommonComponents'

export const DataCreateModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  const formMethods = useForm<DataFormType>({
    defaultValues: nodeModal.data
      ? transformFlowItemToCreateRecord(nodeModal.data as RecordCreateFlowItemResponse)
      : {},
  })
  const { register, formState, setValue, control, handleSubmit, watch, getValues } = formMethods
  const theme = useTheme()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')
  const watchObjectName = watch('objectName')
  const { validateFunction } = useValidateString()
  const fieldArray = useFieldArray({
    control,
    name: 'payload',
  })

  const [attributeType, setAttributeType] = useState<AttributeType | undefined>(
    ((nodeModal.data as RecordCreateFlowItemResponse)?.attributes?.variableName && 'variable') ||
      ((nodeModal.data as RecordCreateFlowItemResponse)?.attributes?.config?.payload && 'config'),
  )

  // get all objects
  const { data: objectOptions, isLoading: isLoadingObjects } = useQuery(
    [QueryKeys.Builder.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  const submitForm = (data: any) => {
    onSubmit({
      name: data.name,
      displayName: data.displayName,
      description: data.description,
      type: nodeModal.type,
      attributes: {
        config: data.objectName
          ? ({
              objectName: data.objectName,
              payload: data.payload && parseConfigArrayToConfigObject(data.payload),
              storeGuidToVar: data.storeGuidToVar?.value,
            } as RecordCreateConfig)
          : undefined,
        media: {
          update: data.mediaUpdate,
          remove: data.mediaRemove?.map((item: any) => item.mediaId),
        },
        variableName: data.variable?.value,
      },
      nodeAttributes: {
        position: nodeModal.position,
      },
    })
  }

  return (
    <>
      <NModal.Header onClose={onCancel} title={`${!!nodeModal.nodeId ? 'Edit' : 'New'} create record`} />
      <NModal.Body>
        <ScrollableWrapper>
          <Section>
            <NRow>
              <NColumn flex={1}>
                <NTextInput
                  {...register('displayName', {
                    validate: validateFunction,
                  })}
                  required
                  name="displayName"
                  error={formState.errors.displayName?.message}
                  placeholder="Display name"
                  label="Display Name"
                  onBlur={e => {
                    if (!!nodeModal.nodeId) {
                      return
                    }
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput
                  disabled={!!nodeModal.nodeId}
                  {...register('name', {
                    validate: validateFunction,
                    pattern: {
                      value: fieldNameRegex,
                      message: 'Invalid pattern',
                    },
                  })}
                  required
                  error={formState.errors.name?.message}
                  name="name"
                  placeholder="API name"
                  label="API Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={1}>
                <NTextArea rows={3} {...register('description')} name="description" label="Description" />
              </NColumn>
            </NRow>
          </Section>
          <NDivider lineColor={color('Neutral200')({ theme })} />
          <Section>
            <Header>
              <RequiredIndicator>*</RequiredIndicator>Create a Record
            </Header>
            <NRow>
              <NColumn flex={0.5}>
                <NSingleSelect
                  fullWidth
                  value={attributeType}
                  options={ATTRIBUTE_OPTIONS}
                  onValueChange={value => {
                    if (value === 'variable') {
                      formMethods.setValue('payload', undefined as never)
                      formMethods.setValue('objectName', undefined as never)
                    }
                    if (value === 'config') {
                      formMethods.setValue('variable', undefined)
                    }
                    setAttributeType(value as AttributeType)
                  }}
                />
              </NColumn>
            </NRow>
          </Section>
          <NDivider size={1} lineSize={1} lineColor={color('Neutral200')({ theme })} />

          {attributeType === 'variable' && (
            <Section>
              <NRow>
                <NColumn flex={0.5}>
                  <Controller
                    name={`variable`}
                    control={control}
                    rules={{
                      required: {
                        value: true,
                        message: 'Required',
                      },
                    }}
                    render={({ field: { value, onChange }, fieldState }) => {
                      return (
                        <ContentValuePicker
                          label="Variable"
                          error={fieldState.error?.message}
                          selectedValue={value}
                          onSelectValue={onChange}
                          allowTypes={{ variable: true, value: true }}
                        />
                      )
                    }}
                  />
                </NColumn>
              </NRow>
              <NDivider size="md" />
              <Attachments formMethods={formMethods} />
            </Section>
          )}
          {attributeType === 'config' && (
            <Section>
              <NRow>
                <NColumn>
                  <Controller
                    name="objectName"
                    defaultValue={''}
                    rules={{
                      required: {
                        value: true,
                        message: 'Required',
                      },
                    }}
                    control={control}
                    render={({ field, fieldState }) => (
                      <NSingleSelect
                        required
                        options={objectOptions || []}
                        searchValue={searchValue}
                        onSearchValueChange={handleSearchChange}
                        isLoading={isLoadingObjects}
                        error={fieldState.error?.message}
                        isSearchable
                        fullWidth
                        label="Object"
                        placeholder="Select object"
                        value={field.value}
                        onValueChange={value => {
                          if (value !== watchObjectName) {
                            setValue('payload', [])
                            fieldArray.append({ fieldName: '' })
                          }
                          field.onChange(value)
                        }}
                      />
                    )}
                  />
                </NColumn>
                <NDivider vertical size="xl" />
                <NColumn>
                  <Controller
                    name="storeGuidToVar"
                    control={control}
                    render={({ field: { value, onChange }, fieldState }) => {
                      return (
                        <ContentValuePicker
                          label="Store guid to var"
                          error={fieldState.error?.message}
                          selectedValue={value}
                          onSelectValue={onChange}
                          allowTypes={{ variable: true, value: true }}
                        />
                      )
                    }}
                  />
                </NColumn>
              </NRow>
              <NDivider size="md" />
              {watchObjectName && (
                <>
                  <AssignValueForm fieldArray={fieldArray} formMethods={formMethods} objectName={watchObjectName} />
                  <Attachments formMethods={formMethods} />
                </>
              )}
            </Section>
          )}
        </ScrollableWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(submitForm)} />
    </>
  )
}

export const dataCreateWidget: WidgetItemType = {
  leftIcon: <FlowIcons.DataCreate />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Tertiary900">
      <FlowIcons.DataCreate />
    </FlowIconContainer>
  ),
  label: <abbr title="Create Record">Create Rcrd</abbr>,
  type: NodeType.DataCreate,
  data: {
    name: '',
    displayName: '',
    objectName: '',
  },
}

const transformFlowItemToCreateRecord = (flowItem: RecordCreateFlowItemResponse) => {
  return {
    name: flowItem.name,
    displayName: flowItem.displayName,
    description: flowItem.description,
    payload: parseConfigObjectToConfigArray(flowItem.attributes?.config?.payload),
    objectName: flowItem.attributes?.config?.objectName,
    variable:
      flowItem.attributes?.variableName &&
      ({ value: flowItem.attributes?.variableName, type: 'variable' } as ContentValue),
    mediaUpdate: flowItem.attributes?.media?.update,
    mediaRemove: flowItem.attributes?.media?.remove?.map(item => ({ mediaId: item })),
    storeGuidToVar:
      flowItem.attributes?.config?.storeGuidToVar &&
      ({ value: flowItem.attributes?.config.storeGuidToVar, type: 'variable' } as ContentValue),
  }
}
