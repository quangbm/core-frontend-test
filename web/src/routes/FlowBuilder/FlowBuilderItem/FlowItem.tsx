import { FC } from 'react'
import { FlowType } from '../../../../../shared/src/api/endpoints/flows'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../utils/types'
import { ActionBuilder, actionWidget } from './ActionBuilder/ActionBuilder'
import {
  CollectionAggregationModal,
  collectionAggregationWidget,
} from './CollectionAggregation/CollectionAggregationBuilder'
import { EventEmitModal, eventEmitWidget } from './EventEmit/EventEmitBuilder'
import { AssignmentModal, assignmentWidget } from './FlowItemAssignment/AssignmentModal'
import { DataCreateModal, dataCreateWidget } from './FlowItemData/DataCreateItem'
import { DataDeleteModal, dataDeleteWidget } from './FlowItemData/DataDeleteItem'
import { DataGetModal, dataGetWidget } from './FlowItemData/DataGetItem'
import { DataUpdateModal, dataUpdateWidget } from './FlowItemData/DataUpdateItem'
import { ImportRecordModal, importRecordWidget } from './FlowItemData/ImportRecordItem'
import { UserCreateModal, userCreateWidget } from './FlowItemData/UserCreateItem'
import { DecisionFlowWidget, DecisionModal } from './FlowItemDecision/DecisionModal'
import { LoopBuilder, loopBuilderWidget } from './LoopBuilder/LoopBuilder'
import { ScreenBuilder, screenBuilderWidget } from './ScreenBuilder/ScreenBuilder'
import { SubFlowBuilderModal, subflowWidget } from './SubFlow/SubFlowBuilderModal'

export type GroupName = 'Interaction' | 'Logic' | 'Data'
export function getWidgetByGroups(type?: FlowType): Record<GroupName, WidgetItemType[]> {
  if (type === 'screen') {
    return {
      Interaction: [screenBuilderWidget, actionWidget, subflowWidget],
      Logic: [assignmentWidget, DecisionFlowWidget, loopBuilderWidget, collectionAggregationWidget],
      Data: [
        dataCreateWidget,
        dataUpdateWidget,
        dataGetWidget,
        dataDeleteWidget,
        importRecordWidget,
        eventEmitWidget,
        userCreateWidget,
      ],
    }
  }
  return {
    Interaction: [actionWidget, subflowWidget],
    Logic: [assignmentWidget, DecisionFlowWidget, loopBuilderWidget, collectionAggregationWidget],
    Data: [
      dataCreateWidget,
      dataUpdateWidget,
      dataGetWidget,
      dataDeleteWidget,
      importRecordWidget,
      eventEmitWidget,
      userCreateWidget,
    ],
  }
}

export const WIDGET_FLOW_ITEMS: Record<NodeType, WidgetItemType> = {
  [NodeType.Screen]: screenBuilderWidget,
  [NodeType.Action]: actionWidget,
  [NodeType.Subflow]: subflowWidget,

  [NodeType.Loop]: loopBuilderWidget,
  [NodeType.Decision]: DecisionFlowWidget,
  [NodeType.Assignment]: assignmentWidget,
  [NodeType.CollectionAggregation]: collectionAggregationWidget,

  [NodeType.DataCreate]: dataCreateWidget,
  [NodeType.DataUpdate]: dataUpdateWidget,
  [NodeType.DataGet]: dataGetWidget,
  [NodeType.DataDelete]: dataDeleteWidget,
  [NodeType.ImportRecord]: importRecordWidget,
  [NodeType.EmitEvent]: eventEmitWidget,

  [NodeType.UserCreate]: userCreateWidget,
}

export const FLOW_ITEM_BUILDER: Record<NodeType, FC<FlowNodeModalProps>> = {
  [NodeType.Screen]: ScreenBuilder,
  [NodeType.Action]: ActionBuilder,
  [NodeType.Subflow]: SubFlowBuilderModal,

  [NodeType.Loop]: LoopBuilder,
  [NodeType.Decision]: DecisionModal,
  [NodeType.Assignment]: AssignmentModal,
  [NodeType.CollectionAggregation]: CollectionAggregationModal,

  [NodeType.DataCreate]: DataCreateModal,
  [NodeType.DataUpdate]: DataUpdateModal,
  [NodeType.DataGet]: DataGetModal,
  [NodeType.DataDelete]: DataDeleteModal,
  [NodeType.ImportRecord]: ImportRecordModal,
  [NodeType.EmitEvent]: EventEmitModal,

  [NodeType.UserCreate]: UserCreateModal,
}
