import React, { useMemo } from 'react'
import { Controller, UseFormReturn } from 'react-hook-form'
import styled from 'styled-components/macro'
import { ActionMetadataResponse, ContentValue, RequestInputType } from '../../../../../../shared/src/api'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NSwitch } from '../../../../components/NSwitch/NSwitch'
import { NTypography } from '../../../../components/NTypography'
import { ContentValuePicker } from '../../FlowBuilderVariable/ContentValuePicker'
import { ActionFormType } from '../../models'
import { Header } from '../CommonStyledComponents'

const Wrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`

const SetValueContainer = styled.div`
  display: flex;
  padding: ${spacing('md')} 0;
  justify-content: space-between;

  .variable_switch {
    height: fit-content;
  }

  &:not(:last-child) {
    border-bottom: 1px solid ${color('Neutral200')};
  }
`

const SetValueContainerLeft = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 40%;
`

const VariableNameText = styled(NTypography.Headline)``

type ActionInputMapProps = {
  actionMetadata: ActionMetadataResponse
  formMethods: UseFormReturn<ActionFormType>
}

type ExtendRequestInputType = RequestInputType & {
  name: string
  displayName: string
}

export const ActionInputMap: React.FC<ActionInputMapProps> = ({ actionMetadata, formMethods }) => {
  const { control } = formMethods

  const actionInputs = useMemo(() => {
    const { inputs } = actionMetadata

    // get input from path
    let result: ExtendRequestInputType[] = inputs.path.map(inputName => {
      return {
        name: `path.${inputName}`,
        displayName: `${inputName} (path input)`,
        isRequired: true,
        type: 'string',
      }
    })

    const headerKeys = Object.keys(inputs.header)
    const queryKeys = Object.keys(inputs.query)

    headerKeys.map(key => {
      const input = {
        ...inputs.header[key],
        type: 'string',
        name: `header.${key}`,
        displayName: `${key} (header input)`,
      } as ExtendRequestInputType
      result = [...result, input]
      return input
    })

    queryKeys.map(key => {
      const input = {
        ...inputs.query[key],
        name: `query.${key}`,
        displayName: `${key} (query input)`,
      }
      result = [...result, input]
      return input
    })

    if (inputs.body) {
      result = [...result, { ...inputs.body, name: 'body', displayName: `body` }]
    }

    return result
  }, [actionMetadata])

  return (
    <Wrapper>
      <Header>Set input values</Header>
      <Controller
        control={control}
        name="inputMap"
        render={({ field }) => {
          return (
            <>
              {actionInputs.map(input => {
                const isChecked =
                  input.isRequired || (input.name in field.value && field.value[input.name] !== input.name)

                const toggleVariable = (newVal: boolean) => {
                  const newInputMap = { ...field.value }
                  if (newVal) {
                    newInputMap[input.name] = undefined
                  } else {
                    delete newInputMap[input.name]
                  }
                  field.onChange(newInputMap)
                }

                const setVariableToInput = (selectedVariable?: ContentValue) => {
                  const newInputMap = { ...field.value }
                  newInputMap[input.name] = selectedVariable
                  field.onChange(newInputMap)
                }

                return (
                  <SetValueContainer key={`inputMap${input.name}`}>
                    <SetValueContainerLeft>
                      <VariableNameText>{input.displayName}</VariableNameText>
                      {isChecked && (
                        <>
                          <NDivider size="sm" />
                          <ContentValuePicker
                            label="Input value"
                            selectedValue={field.value[input.name]}
                            placeholder={`Select '${input.type}' variable`}
                            allowTypes={{ variable: true, value: input.type === 'string' || input.type === 'number' }}
                            onSelectValue={varOption => {
                              // if (
                              //   flowVar
                              //     ? input.type !== flowVar.type
                              //     : input.type !== 'string' && input.type !== 'number'
                              // ) {
                              //   NToast.error({ title: `Variable type should be: ${input.type}` })
                              //   return
                              // }
                              setVariableToInput(varOption)
                            }}
                          />
                        </>
                      )}
                    </SetValueContainerLeft>
                    {!input.isRequired && (
                      <NSwitch className="variable_switch" checked={isChecked} onChange={toggleVariable} />
                    )}
                  </SetValueContainer>
                )
              })}
            </>
          )
        }}
      />
    </Wrapper>
  )
}
