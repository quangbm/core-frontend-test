import React, { FC } from 'react'
import { Controller, FieldArrayWithId, useFieldArray, UseFormReturn } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useParams } from 'react-router'
import styled from 'styled-components/macro'
import { AssignmentVariableDto, FlowVariableResponse } from '../../../../../../shared/src/api'
import { color, spacing } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NToast } from '../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../services/api'
import { ContentValuePicker } from '../../FlowBuilderVariable/ContentValuePicker'
import { AssignmentFormType } from '../../models'

///////////////////////////////
const RuleWrapper = styled.div`
  width: 100%;
  background-color: ${color('white')};
`
const InnerRuleWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: ${spacing('xl')};
  .operator-select {
    width: 200px;
  }
  .picker {
    flex: 1;
  }
`
const FullRow = styled.div`
  display: flex;
  flex: 1;
`

const StyledButton = styled(NButton)`
  flex: 1;
  padding: ${spacing('sm')} ${spacing('sm')};
  background: ${color('white')} !important;
`
const StyledColumn = styled(NColumn)`
  border: 2px solid ${color('Primary700')};
  border-radius: 6px;
  background: ${color('white')};
`

type FlowVarByNameType = Record<string, FlowVariableResponse>

type Props = {
  formMethods: UseFormReturn<AssignmentFormType>
}

export const AssignmentRules: FC<Props> = ({ formMethods }) => {
  const { flowName, version } = useParams<{ flowName: string; version: string }>()

  const { control } = formMethods
  const { fields, append, remove } = useFieldArray({
    control,
    name: `assignmentVariables`,
  })

  const { data: operationData, isLoading: isLoadingOperator } = useQuery(
    [QueryKeys.Flows.getAssignmentOperations],
    APIServices.Flows.getAssignmentOperations,
    {
      select: data => {
        const result = {} as Record<string, Array<SelectOption>>
        for (const [variableType, values] of Object.entries(data.data)) {
          result[variableType] = values.map(option => ({ value: option.value, label: option.displayName }))
        }
        return result
      },
    },
  )

  // get flow item variables
  const { data: flowVarByName = {} } = useQuery(
    [QueryKeys.Flows.getFlowTemplateVariables, { flowName, version }],
    APIServices.Flows.getFlowTemplateVariables,
    {
      enabled: !!flowName,
      onError: error => {
        NToast.error({ title: 'Get flow variable error', subtitle: error.response?.data.message })
      },
      select: data => {
        const result = {} as FlowVarByNameType
        for (const variable of data.data) {
          result[variable.name] = variable
        }
        return result
      },
    },
  )

  return (
    <RuleWrapper>
      {fields.map((clause, rowIndex) => (
        <RuleRow
          key={clause.id}
          clause={clause}
          operationData={operationData || {}}
          isLoadingOperator={isLoadingOperator}
          flowVarByName={flowVarByName}
          formMethods={formMethods}
          onRemove={fields.length > 1 ? () => remove(rowIndex) : undefined}
          name={`assignmentVariables.${rowIndex}`}
        />
      ))}
      <NRow>
        <StyledColumn flex={1}>
          <StyledButton
            onClick={() =>
              append({
                variable: '',
                value: undefined,
              })
            }>
            <GlobalIcons.Plus /> Add Assignment
          </StyledButton>
        </StyledColumn>
      </NRow>
    </RuleWrapper>
  )
}

////////////////////////////////////////////////////////
type RuleRowProps = {
  clause: FieldArrayWithId<AssignmentFormType, 'assignmentVariables', 'id'>
  isLoadingOperator?: boolean
  operationData: Record<string, SelectOption[]>
  flowVarByName: FlowVarByNameType
  formMethods: UseFormReturn<AssignmentFormType>
  onRemove?: () => void
  name: `assignmentVariables.${number}`
}
const RuleRow: FC<RuleRowProps> = ({
  name,
  clause,
  isLoadingOperator,
  operationData,
  flowVarByName,
  formMethods,
  onRemove,
}) => {
  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  const { control, watch, setValue } = formMethods
  const rowData = watch(name) as AssignmentVariableDto | undefined

  // Get nested variable
  // support 1 level nested only
  const [variable, nestedVariable] = (rowData && rowData.variable?.split('.')) || ['', '']

  const { data: type, isLoading } = useQuery(
    [QueryKeys.Flows.getFlowVariableSchema, { flowName, variable, version }],
    APIServices.Flows.getFlowVariableSchema,
    {
      enabled: rowData?.variable?.includes('.') || false,
      select(response) {
        return response.data.find(item => item.name === nestedVariable)?.type || ''
      },
    },
  )

  // prefer nested variable type if exists
  const variableType = type || flowVarByName[variable]?.type

  const signOptions = (variableType && operationData[variableType]) || []

  return (
    <InnerRuleWrapper key={clause.id}>
      <FullRow>
        <Controller
          control={control}
          rules={{
            required: {
              value: true,
              message: 'Required',
            },
          }}
          name={`${name}.variable`}
          render={({ field: { value }, fieldState }) => (
            <ContentValuePicker
              className="picker"
              placeholder="Select variable"
              error={fieldState.error?.message}
              selectedValue={value ? { value, type: 'variable' } : undefined}
              onSelectValue={varOption => {
                // FIXME: Don't know why onChange is not work?!?!?
                // Hacked by using setValue
                setValue(`${name}.variable`, varOption?.value, {
                  shouldDirty: true,
                  shouldValidate: true,
                  shouldTouch: true,
                })
                setValue(`${name}.sign`, undefined)
                setValue(`${name}.value`, undefined)
              }}
              allowTypes={{ variable: true }}
            />
          )}
        />
        <NDivider vertical size="md" />
        <Controller
          control={control}
          rules={{
            required: {
              value: true,
              message: 'Required',
            },
          }}
          name={`${name}.sign`}
          render={({ field: { value, onChange }, fieldState }) => {
            return (
              <NSingleSelect
                disabled={!rowData?.variable}
                isLoading={isLoadingOperator || isLoading}
                options={signOptions}
                placeholder="Operator"
                error={fieldState.error?.message}
                value={value}
                onValueChange={onChange}
                className="operator-select"
              />
            )
          }}
        />
        <NDivider vertical size="md" />
        <Controller
          control={control}
          rules={{
            required: {
              value: true,
              message: 'Required',
            },
          }}
          name={`${name}.value`}
          render={({ field: { value, onChange }, fieldState }) => {
            return (
              <ContentValuePicker
                disabled={!rowData?.variable}
                className="picker"
                placeholder="Enter or pick value"
                error={fieldState.error?.message}
                selectedValue={value}
                onSelectValue={onChange}
                allowTypes={{ variable: true, value: true, script: true }}
              />
            )
          }}
        />
      </FullRow>
      <NDivider vertical size="md" />
      <NButton type="ghost" icon={<GlobalIcons.Trash />} disabled={!onRemove} onClick={onRemove} />
    </InnerRuleWrapper>
  )
}
