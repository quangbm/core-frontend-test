import {
  BaseScreenFIComponent,
  InputProps,
  PickListProps,
  RootProps,
  SelectionProps,
  TextProps,
} from '../../../../../../../shared/src/api'

export enum ScreenElementType {
  Root = 'ELEMENT_ROOT',
  Input = 'ELEMENT_INPUT',
  Display = 'ELEMENT_DISPLAY',
  Component = 'ELEMENT_COMPONENT',
}

export enum ScreenElementComponent {
  Root = 'Root',
  Input = 'Input',
  Selection = 'Selection',
  Selection2 = 'Selection2',
  PickList = 'PickList',
  //
  DisplayText = 'DisplayText',
  //
  InputList = 'InputList',
  FileDropzone = 'FileDropzone',
  Withdrawal = 'Withdrawal',

  AmountInput = 'AmountInput',
  Grid = 'Grid',
  DataLookup = 'DataLookup',

  ReactComponent = 'ReactComponent',
}

export type ExtendInputProps = InputProps & Record<string, any>
export type ExtendSelectionProps = SelectionProps & { defaultSelectValue?: string[] } & Record<string, any>
export type ExtendPickListProps = PickListProps & { defaultSelectValue?: string[] } & Record<string, any>

export type ScreenElement = Partial<Omit<BaseScreenFIComponent, 'props'>> & {
  component: ScreenElementComponent
  label: string
  props: Partial<RootProps & ExtendInputProps & ExtendSelectionProps & TextProps & ExtendPickListProps>
  children: string[]
}

export type ScreenWidgetItemType = {
  id?: string
  icon: JSX.Element
  label: string
  type: ScreenElementType
  component: ScreenElementComponent
  props: RootProps | ExtendInputProps | ExtendSelectionProps | TextProps | ExtendPickListProps | Record<string, any>
  children: string[]
}

export type DraggableElement = {
  elementId?: string
  item?: Omit<ScreenElement, 'id'> & { id?: string }
  parentId: string
  index: number
}
