import { FC, useState } from 'react'
import styled, { useTheme } from 'styled-components/macro'
import { BaseScreenFIComponent, ScreenFlowItemResponse } from '../../../../../../shared/src/api'
import { color } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NModal } from '../../../../components/NModal/NModal'
import { SelectOption } from '../../../../components/NSelect/model'
import { NToast } from '../../../../components/NToast'
import { FlowIcons } from '../../components/Icons'
import { FlowIconContainerClassName } from '../../utils/constants'
import { CommonWidgetDataType, FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { LeftSidebar } from './components/LeftSidebar'
import { RightSidebar } from './components/RightSidebar'
import { Root } from './components/ZoneItems/Root'
import { ScreenBuilderProvider, useScreenBuilder } from './contexts/ScreenBuilderContext'
import { ROOT_ELEMENT } from './utils/builder'
import { ScreenElement, ScreenElementComponent } from './utils/models'

const ScreenBuilderContainer = styled.div`
  display: flex;
  height: calc(100vh - 152px * 2);
  background-color: ${color('Neutral200')};
`

export const ScreenBuilder: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  return (
    <ScreenBuilderProvider defaultValue={transformFlowItemToScreenBuilder(nodeModal.data as ScreenFlowItemResponse)}>
      <ScreenBuilderModal nodeModal={nodeModal} isSubmitting={isSubmitting} onCancel={onCancel} onSubmit={onSubmit} />
    </ScreenBuilderProvider>
  )
}

const ScreenBuilderModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  const theme = useTheme()
  const { elements } = useScreenBuilder()

  const [flowName, setFlowName] = useState(nodeModal.data?.name || '')
  const [flowDisplayName, setFlowDisplayName] = useState(nodeModal.data?.displayName || '')
  const [flowDescription, setFlowDescription] = useState(nodeModal.data?.description || '')

  const handleSubmit = () => {
    const data = {
      name: flowName,
      displayName: flowDisplayName,
      description: flowDescription,
      script: elements,
    }

    const script = data.script && transformScreenScript(data.script)

    if (!script) {
      return
    }

    onSubmit({
      name: data.name,
      displayName: data.displayName,
      description: data.description,
      type: nodeModal.type,
      attributes: {
        layoutScript: script,
      },
      nodeAttributes: {
        position: nodeModal.position,
      },
    })
  }

  return (
    <>
      <NModal.Header title={`${nodeModal.nodeId ? 'Edit' : 'New'} Screen`} onClose={onCancel} />
      <NModal.Body>
        <ScreenBuilderContainer>
          <LeftSidebar />
          <Root
            isEditing={!!nodeModal.nodeId}
            flowName={flowName}
            flowDisplayName={flowDisplayName}
            flowDescription={flowDescription}
            setFlowName={setFlowName}
            setFlowDisplayName={setFlowDisplayName}
            setFlowDescription={setFlowDescription}
          />
          <RightSidebar />
        </ScreenBuilderContainer>
      </NModal.Body>
      <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit} />
    </>
  )
}

export const screenBuilderWidget: WidgetItemType = {
  leftIcon: <FlowIcons.Screen />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName}>
      <FlowIcons.Screen />
    </FlowIconContainer>
  ),
  label: 'Screen',
  type: NodeType.Screen,
  data: {
    name: '',
    displayName: '',
    description: '',
  },
}

const transformFlowItemToScreenBuilder = (
  flowItem: ScreenFlowItemResponse,
): CommonWidgetDataType & { script?: Record<string, ScreenElement> } => {
  return {
    name: flowItem.name,
    displayName: flowItem.displayName,
    description: flowItem.description,
    //@ts-ignore index type
    script: flowItem.attributes?.layoutScript && addRootLabel(flowItem.attributes?.layoutScript, flowItem),
  }
}

function addRootLabel(script: Record<string, BaseScreenFIComponent>, flowItem: ScreenFlowItemResponse) {
  const { displayName } = flowItem
  const root = script[ROOT_ELEMENT]
  const editScript = {
    ...script,
    [ROOT_ELEMENT]: {
      ...root,
      props: {
        ...root.props,
        label: displayName,
      },
    },
  }
  return editScript
}

function transformScreenScript(script: Record<string, ScreenElement>) {
  const elementIds = Object.keys(script)

  return elementIds.reduce((prev, curId, _, array) => {
    const element = script[curId]
    const { id, name, type, component, props, children } = element

    if (!id || !name || !type || !component || !props || !children) {
      NToast.error({ title: 'Script is not in correct format' })
      array.slice(1)
      return undefined
    }

    let defaultIndexes: number[] | undefined = undefined
    if (element.component === ScreenElementComponent.Selection) {
      const { options, defaultSelectValue } = element.props

      if (defaultSelectValue) {
        defaultIndexes = (defaultSelectValue as string[]).reduce((prev, defaultVal) => {
          const foundIndex = ((options || []) as SelectOption[]).findIndex(op => {
            return op.value === defaultVal
          })

          if (foundIndex < 0) {
            return prev
          }

          return [...prev, foundIndex]
        }, [] as number[])
      }
    }

    const transformed: BaseScreenFIComponent = {
      id,
      name,
      type,
      component,
      children,
      //@ts-ignore
      props: {
        ...props,
        defaultIndexes,
      },
    }

    return {
      ...prev,
      [id]: transformed,
    }
  }, {} as Record<string, BaseScreenFIComponent> | undefined)
}
