import { ZoneProps } from '../../../../LayoutBuilder/utils/models'
import { UnderConstruction } from '../components/UnderConstruction'
import { ZONE_ELEMENTS, ZONE_ELEMENTS_INPUTS, ZONE_ELEMENTS_SELECTIONS } from '../components/ZoneItems/zoneItems'
import { useScreenBuilder } from '../contexts/ScreenBuilderContext'

export function useElementRenderer(
  elementId: string,
  options?: Pick<ZoneProps, 'canDrag' | 'canDrop'>,
): React.ReactNodeArray {
  const { elements } = useScreenBuilder()
  if (!elementId) {
    return []
  }
  const element = elements[elementId]

  return (element.children || []).map((id, index) => {
    if (!elements[id]) {
      return null
    }
    const { component, props } = elements[id]

    let Component: ((props: ZoneProps) => JSX.Element) | undefined
    if (props.inputType) {
      Component = ZONE_ELEMENTS_INPUTS[props.inputType]
    } else if (props.selectionType) {
      if (props.isRadio) {
        Component = ZONE_ELEMENTS_SELECTIONS['Radio']
      } else {
        Component = ZONE_ELEMENTS_SELECTIONS[component === 'PickList' ? 'PickList' : 'PickList2']
      }
    } else {
      Component = ZONE_ELEMENTS[component]
    }

    if (Component) {
      return <Component key={id} elementId={id} parentId={elementId} index={index} {...options} />
    }

    return <UnderConstruction key={id}>{element.label}</UnderConstruction>
  })
}
