import React, { FC, useState } from 'react'
import styled from 'styled-components/macro'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { Icons } from '../../../../../components/Icons'
import { NDivider } from '../../../../../components/NDivider'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { RequiredIndicator, typography } from '../../../../../components/NTypography'

const Wrapper = styled('div')``

const WidgetHeading = styled.p`
  display: flex;
  align-items: center;
  padding: ${spacing('xs')} 0;
  ${typography('h300')};
  text-transform: none;
`

const WidgetContent = styled('div')<{ hasTitle: boolean }>`
  display: flex;
  flex-direction: column;
  padding: ${spacing('sm')} 0;
  padding-top: ${props => (props.hasTitle ? 0 : spacing('sm'))};
`

type WidgetProps = {
  required?: boolean
  title?: string
  rightElement?: React.ReactNode
  children: React.ReactNode
}

export function Widget({ required, title, children, rightElement }: WidgetProps) {
  return (
    <Wrapper>
      <NRow justify="space-between">
        {title && (
          <WidgetHeading>
            {required && <RequiredIndicator>*</RequiredIndicator>}
            {title}
          </WidgetHeading>
        )}
        {rightElement}
      </NRow>
      <WidgetContent hasTitle={title !== undefined}>{children}</WidgetContent>
    </Wrapper>
  )
}

////////////////////////////////////////////
const AccordionWidgetContent = styled('div')<{ addPadContent?: boolean }>`
  display: flex;
  flex-direction: column;
  padding: 0 ${props => (props.addPadContent ? spacing('md') : 0)};

  background: ${color('white')};
  .drop-placeholder {
    display: none;
    visibility: hidden;
  }
`

const AccordionWidgetHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  ${typography('overline')};

  border-top: 1px solid ${color('Neutral200')};
  height: 32px;

  color: ${color('Neutral400')};
  cursor: pointer;

  &:hover {
    background-color: ${color('Neutral200')};
  }
  transition: background-color 0.25s;
`

const AccordionWidgetIconContainer = styled.div`
  display: flex;
  margin: 0 ${spacing('sm')};
  width: 16px;
  height: 16px;
  justify-content: center;
  align-items: center;

  &:hover {
    background-color: ${props => (props.onClick ? color('white') : color('transparent'))};
  }
  transition: background-color 0.25s;
`

const AccordionWidgetTitleContent = styled.div`
  display: flex;
  align-items: center;
`

const AccordionWidgetWrapper = styled('div')``

type AccordionWidgetProps = {
  title: string
  addPadContent?: boolean
  onBack?: () => void
}

export const AccordionWidget: FC<AccordionWidgetProps> = ({ title, addPadContent, onBack, children }) => {
  const [isOpen, setIsOpen] = useState(true)
  return (
    <AccordionWidgetWrapper>
      <AccordionWidgetHeader onClick={() => setIsOpen(prev => !prev)}>
        <AccordionWidgetTitleContent>
          {onBack ? (
            <AccordionWidgetIconContainer onClick={onBack}>
              <Icons.Left />
            </AccordionWidgetIconContainer>
          ) : (
            <NDivider vertical size="md" />
          )}
          {title}
        </AccordionWidgetTitleContent>
        <AccordionWidgetIconContainer>{isOpen ? <Icons.Down /> : <Icons.Right />}</AccordionWidgetIconContainer>
      </AccordionWidgetHeader>
      {isOpen && <AccordionWidgetContent addPadContent={addPadContent}>{children}</AccordionWidgetContent>}
    </AccordionWidgetWrapper>
  )
}
