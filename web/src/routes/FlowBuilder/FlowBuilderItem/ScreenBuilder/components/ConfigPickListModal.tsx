import { FC } from 'react'
import styled from 'styled-components/macro'
import { PickListResponse } from '../../../../../../../shared/src/api'
import { spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NModal } from '../../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../../components/NPerfectScrollbar'
import { PickListItemsConfig } from '../../../../Dashboard/routes/Organization/PickLists/components/PickListItemsConfig'

const Wrapper = styled.div`
  padding: ${spacing('md')};
  max-height: 60vh;
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  data: PickListResponse
}

export const ConfigPickListModal: FC<Props> = ({ visible, setVisible, data }) => {
  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header onClose={() => setVisible(false)} title={`Config ${data.displayName}`} />
      <NModal.Body>
        <NPerfectScrollbar>
          <Wrapper>
            <PickListItemsConfig data={data} />
            <NDivider size="md" />
          </Wrapper>
        </NPerfectScrollbar>
      </NModal.Body>
    </NModal>
  )
}
