import { ReactComponent as Checkbox } from '../assets/checkbox-icon.svg'
import { ReactComponent as DateTime } from '../assets/datetime-icon.svg'
import { ReactComponent as PickList } from '../assets/picklist-icon.svg'
import { ReactComponent as Radio } from '../assets/radio-icon.svg'
import { ReactComponent as TextInput } from '../assets/text-input-icon.svg'

export const ScreenFlowItemIcons = {
  TextInput,
  PickList,
  DateTime,
  Checkbox,
  Radio,
}
