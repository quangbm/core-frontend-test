import * as React from 'react'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { classnames } from '../../../../../../../common/classnames'
import { MarkdownViewer } from '../../../../../../../components/MarkdownViewer'
import { NCodeEditor } from '../../../../../../../components/NCodeEditor'
import { Suggestion as BaseSuggestion } from '../../../../../../../components/NCodeEditor/Suggestion'
import { useFlowVariables } from '../../../../../../../hooks/useFlowVariables'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

const Suggestion = styled(BaseSuggestion)`
  span {
    display: block;
    font-size: 12px;
  }
`

export function Paragraph({ elementId, parentId, index, canDrag, canDrop }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const element = elements[elementId]
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrag, canDrop })
  const isSelected = selectedElement === elementId

  const { flowName } = useParams<{ flowName: string }>()

  const [filter, setFilter] = React.useState('')
  const suggestion = useFlowVariables(flowName, filter)

  return (
    <ZoneItemLayout
      shouldRenderSidebar={selectedElement === elementId}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Display Text"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            <Widget title="Content">
              <NCodeEditor
                style={{ maxHeight: 300, minHeight: 200 }}
                language="markdown"
                trigger="{{"
                replacer={v => `{${v}}`}
                value={element.props.content?.value || ''}
                onSearchChange={setFilter}
                onChange={e => {
                  configElement(elementId, {
                    content: { type: 'value', value: e.target.value },
                  })
                }}>
                {suggestion
                  .filter(v => v.name.toLowerCase().includes(filter.toLowerCase()))
                  .map(variable => (
                    <Suggestion value={variable.name} key={variable.name}>
                      {variable.displayName}
                    </Suggestion>
                  ))}
              </NCodeEditor>
            </Widget>
          </AccordionWidget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        style={{ flexDirection: 'column' }}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <MarkdownViewer>{element.props.content?.value || ''}</MarkdownViewer>
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetParagraph: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.TextInput />,
  label: 'Display Text',
  type: ScreenElementType.Display,
  component: ScreenElementComponent.DisplayText,
  props: {
    content: {
      type: 'value',
      value: '',
    },
  },
  children: [],
}
