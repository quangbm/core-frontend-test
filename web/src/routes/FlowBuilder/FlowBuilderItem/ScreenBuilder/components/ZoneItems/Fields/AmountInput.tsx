import * as React from 'react'
import { classnames } from '../../../../../../../common/classnames'
import { NTextInput } from '../../../../../../../components/NTextInput/NTextInput'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { FlowVariablePicker } from '../../../../../FlowBuilderVariable/FlowVariablePicker'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

export const AmountInput = React.memo(function Node({ elementId, parentId, index, canDrag, canDrop }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const element = elements[elementId]
  const isSelected = selectedElement === elementId
  const [{ isOver, isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrop, canDrag })

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Amount Input"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            <Widget required title="Default Value">
              <FlowVariablePicker
                selectedVariable={element.props.defaultValue}
                onVariableSelected={defaultValue => {
                  configElement(elementId, { defaultValue })
                }}
              />
            </Widget>
            <Widget required title="Denomination Options">
              <FlowVariablePicker
                label="Denom"
                selectedVariable={element.props.denom}
                onVariableSelected={denom => {
                  configElement(elementId, { denom })
                }}
              />
            </Widget>
            <Widget required title="Denomination Till">
              <FlowVariablePicker
                label="Denom Notes"
                selectedVariable={element.props.denomTill}
                onVariableSelected={denomTill => {
                  configElement(elementId, { denomTill })
                }}
              />
            </Widget>
            <Widget required title="Total Balance">
              <FlowVariablePicker
                label="Total Balance"
                selectedVariable={element.props.totalBalance}
                onVariableSelected={totalBalance => {
                  configElement(elementId, { totalBalance })
                }}
              />
            </Widget>
            <Widget required title="Currency Code">
              <FlowVariablePicker
                label="Currency Code"
                selectedVariable={element.props.currencyCode}
                onVariableSelected={currencyCode => {
                  configElement(elementId, { currencyCode })
                }}
              />
            </Widget>
            <Widget required title="Should validate balance">
              <FlowVariablePicker
                selectedVariable={element.props.shouldValidateBalance}
                onVariableSelected={shouldValidateBalance => {
                  configElement(elementId, { shouldValidateBalance })
                }}
              />
            </Widget>
            <Widget required title="Should Auto Fill">
              <FlowVariablePicker
                selectedVariable={element.props.shouldAutoFill}
                onVariableSelected={shouldAutoFill => {
                  configElement(elementId, { shouldAutoFill })
                }}
              />
            </Widget>
          </AccordionWidget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isOver && 'is-over',
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <NTextInput disabled label={element.props.label} required={element.props.isRequired} />
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
})

AmountInput.displayName = 'AmountInput'

export const widgetAmountInput: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'Money Input',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.Input,
  props: {
    inputType: 'object',
  },
  children: [],
}
