import { nanoid } from 'nanoid'
import { isValidElement, useEffect } from 'react'
import styled from 'styled-components/macro'
import { classnames } from '../../../../../../../common/classnames'
import { spacing } from '../../../../../../../components/GlobalStyle'
import { NSortableList } from '../../../../../../../components/NSortableList'
import { NTextInput } from '../../../../../../../components/NTextInput/NTextInput'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { useElementRenderer } from '../../../hooks/useElementRenderer'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { LayoutWrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

const StyledLayoutWrapper = styled(LayoutWrapper)`
  flex-direction: column;
`

const StyledSortableList = styled(NSortableList)`
  padding: ${spacing('sm')};
  width: 100%;
  display: grid;
  grid-template-rows: auto;
  grid-gap: ${spacing('sm')};
  align-items: start;
`

export function Grid({ elementId, parentId, index, canDrop, canDrag }: ZoneProps) {
  const [{ isOver, isMouseOver, mousePosition }, ref] = useElement({
    elementId,
    parentId,
    index,
    canDrop,
    canDrag,
  })

  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, configElementChildren, setElementName } = useScreenAction()
  const isSelected = selectedElement === elementId

  const element = elements[elementId]

  const children = useElementRenderer(elementId, { canDrag: false, canDrop: false })

  useEffect(() => {
    if (!element.name) {
      setElementName(elementId, nanoid())
    }
  }, [element.name, elementId, setElementName])

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <AccordionWidget
          title="Section"
          addPadContent
          onBack={() => {
            selectElement(ROOT_ELEMENT)
          }}>
          <Widget title="Columns">
            <NTextInput
              type="number"
              value={`${element.props.columns}`}
              onChange={e => {
                if (e.target.value && !Number.isNaN(e.target.value)) {
                  configElement(elementId, { columns: +e.target.value })
                } else {
                  configElement(elementId, { columns: '' })
                }
              }}
              onBlur={e => {
                if (!e.target.value) {
                  configElement(elementId, { columns: 2 })
                }
              }}
            />
          </Widget>
        </AccordionWidget>
      }>
      <StyledLayoutWrapper
        ref={ref}
        className={classnames([
          isOver && 'is-over',
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        {element.children.length > 0 && (
          <StyledSortableList
            onSort={(fromIndex, toIndex) => {
              const next = Array.from(element.children)
              ;[next[toIndex], next[fromIndex]] = [next[fromIndex], next[toIndex]]
              configElementChildren(elementId, next)
            }}
            style={{
              gridTemplateColumns: `repeat(${element.props.columns || 1}, minmax(0, 1fr))`,
            }}>
            {children.filter(isValidElement).map(child => {
              return <div key={child?.key}>{child}</div>
            })}
          </StyledSortableList>
        )}
        {element.children.length <= 0 && (
          <div
            style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            Drop item(s) here!
          </div>
        )}
        <Placeholder mousePosition={mousePosition} />
      </StyledLayoutWrapper>
    </ZoneItemLayout>
  )
}

export const widgetGrid: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'Grid',
  type: ScreenElementType.Component,
  component: ScreenElementComponent.Grid,
  props: {
    columns: 2,
  },
  children: [],
}
