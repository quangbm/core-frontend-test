import { useQuery } from 'react-query'
import { FieldSchema } from '../../../../../../../../../shared/src/api'
import { classnames } from '../../../../../../../common/classnames'
import { NButton } from '../../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../../components/NDivider'
import { NFilterBuilder } from '../../../../../../../components/NFilterBuilder'
import { SelectOption } from '../../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../../components/NSelect/NSingleSelect'
import { useSearchText } from '../../../../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../../../../services/api'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { FlowVariablePicker } from '../../../../../FlowBuilderVariable/FlowVariablePicker'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

export function DataLookup({ elementId, parentId, index, canDrag, canDrop }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrag, canDrop })
  const element = elements[elementId]
  const isSelected = selectedElement === elementId

  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')

  // get all objects
  const { data: objectsData = [], isFetching: isLoadingObjects } = useQuery(
    [QueryKeys.Builder.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  const {
    data: { fieldsByName, fieldNameOptions } = { fieldsByName: {}, fieldNameOptions: [] },
    isLoading: isLoadingFields,
  } = useQuery(
    [QueryKeys.Builder.getObjectSchema, { objName: element.props.objName }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: Boolean(element.props.objName),
      select(response) {
        const fieldsByName = {} as Record<string, FieldSchema>
        const fieldNameOptions = [] as Array<SelectOption>
        for (const field of response.data.fields || []) {
          fieldsByName[field.name] = field
          fieldNameOptions.push({ value: field.name, label: field.displayName })
        }
        return { fieldsByName, fieldNameOptions }
      },
    },
  )

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <AccordionWidget
          title="Data Lookup"
          addPadContent
          onBack={() => {
            selectElement(ROOT_ELEMENT)
          }}>
          <BaseRightWidget
            elementId={elementId}
            element={element}
            configElement={configElement}
            setElementName={setElementName}
          />
          <Widget title="Object Name">
            <NSingleSelect
              options={objectsData}
              value={element.props.objName}
              onValueChange={objName => configElement(elementId, { objName, filters: undefined })}
              isLoading={isLoadingObjects}
              isSearchable
              searchValue={searchValue}
              onSearchValueChange={handleSearchChange}
            />
            <NDivider />
            <NFilterBuilder
              filters={element.props.filters}
              fieldsByName={fieldsByName}
              fieldNameOptions={fieldNameOptions}
              onSubmit={filters => configElement(elementId, { filters })}>
              <NButton
                disabled={!element.props.objName || isLoadingFields}
                size="small"
                type="primary"
                block
                loading={isLoadingFields}>
                Show filter control
              </NButton>
            </NFilterBuilder>
          </Widget>
          <Widget title="Default Value">
            <FlowVariablePicker
              selectedVariable={element.props.defaultValue}
              onVariableSelected={varOption => {
                configElement(elementId, { defaultValue: varOption })
              }}
            />
          </Widget>
        </AccordionWidget>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <NSingleSelect label={element.props.label} value={element.props.defaultValue?.value} options={[]} disabled />
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetDataLookup: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'Data Lookup',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.DataLookup,
  props: {},
  children: [],
}
