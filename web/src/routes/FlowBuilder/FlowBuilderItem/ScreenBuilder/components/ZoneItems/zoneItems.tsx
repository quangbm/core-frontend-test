import { ZoneProps } from '../../../../../LayoutBuilder/utils/models'
import { ScreenWidgetItemType } from '../../utils/models'
import { FileDropzone, widgetFileDropzone } from './Components/FileDropzone'
import { Grid, widgetGrid } from './Components/Grid'
import { ReactComponent, widgetReactComponent } from './Components/ReactComponent'
import { AmountInput, widgetAmountInput } from './Fields/AmountInput'
import { Checkbox, widgetCheckbox } from './Fields/Checkbox'
import { CSVInput, widgetCSVInput } from './Fields/CSVInput'
import { DataLookup, widgetDataLookup } from './Fields/DataLookup'
import { DateTime, widgetDateTime } from './Fields/DateTime'
import { Input, widgetInput } from './Fields/Input'
import { Paragraph, widgetParagraph } from './Fields/Paragraph'
import { PickList, widgetPickList } from './Fields/PickList'
import { PickList2, widgetPickList2 } from './Fields/PickList2'
import { RadioGroup, widgetRadioGroup } from './Fields/RadioGroup'

export const WIDGET_ITEMS = [
  widgetInput,
  widgetPickList,
  widgetPickList2,
  widgetDateTime,
  widgetCheckbox,
  widgetRadioGroup,
  widgetFileDropzone,
  //
  widgetParagraph,
  widgetAmountInput,
  widgetCSVInput,
  widgetGrid,
  widgetDataLookup,

  widgetReactComponent,
]

//
export const ZONE_ITEMS_INPUTS = {
  string: widgetInput,
  float: widgetInput,
  integer: widgetInput,
  date: widgetDateTime,
  time: widgetDateTime,
  'date-time': widgetDateTime,
  boolean: widgetCheckbox,
  object: widgetAmountInput,
  array: widgetCSVInput,
} as Record<string, ScreenWidgetItemType>

export const ZONE_ITEMS_SELECTIONS = {
  PickList: widgetPickList,
  PickList2: widgetPickList2,
  Radio: widgetRadioGroup,
} as Record<string, ScreenWidgetItemType>

export const ZONE_ITEMS = {
  DisplayText: widgetParagraph,
  FileDropzone: widgetFileDropzone,
  AmountInput: widgetAmountInput,
  Grid: widgetGrid,
  DataLookup: widgetDataLookup,
  ReactComponent: widgetReactComponent,
} as Record<string, ScreenWidgetItemType>

//
type RendererType = (props: ZoneProps) => JSX.Element
export const ZONE_ELEMENTS_INPUTS = {
  string: Input,
  float: Input,
  integer: Input,
  date: DateTime,
  time: DateTime,
  'date-time': DateTime,
  boolean: Checkbox,
  object: AmountInput,
  array: CSVInput,
} as Record<string, RendererType>
export const ZONE_ELEMENTS_SELECTIONS = {
  PickList,
  PickList2,
  Radio: RadioGroup,
} as Record<string, RendererType>
export const ZONE_ELEMENTS = {
  // temporary
  Root: Input,
  DisplayText: Paragraph,
  // InputList,
  FileDropzone,
  Grid,
  DataLookup,
  ReactComponent,
} as Record<string, RendererType>
