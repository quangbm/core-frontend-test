import * as React from 'react'
import { classnames } from '../../../../../../../common/classnames'
import { SelectOption } from '../../../../../../../components/NSelect/model'
import { NMultiSelect } from '../../../../../../../components/NSelect/NMultiSelect'
import { NSingleSelect } from '../../../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../../../components/NTextInput/NTextInput'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { FlowVariablePicker } from '../../../../../FlowBuilderVariable/FlowVariablePicker'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

const PICKLIST_TYPE_OPTIONS: SelectOption[] = [
  { label: 'Single', value: 'single' },
  { label: 'Multiple', value: 'multi' },
]

export function PickList2({ elementId, parentId, index, canDrag, canDrop }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrag, canDrop })

  const element = elements[elementId]
  const isSelected = selectedElement === elementId
  const { defaultValue: _, selectionType, isRequired, ...restProps } = element.props

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Pick list"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            <Widget title="Type">
              <NSingleSelect
                options={PICKLIST_TYPE_OPTIONS}
                value={element.props.selectionType}
                onValueChange={newVal => {
                  configElement(elementId, { selectionType: newVal, defaultIndex: undefined })
                }}
              />
            </Widget>
            <Widget title="Caption">
              <NTextInput
                value={element.props.caption}
                onChange={e => {
                  configElement(elementId, { caption: e.target.value })
                }}
              />
            </Widget>
            <Widget title="Default Value">
              <FlowVariablePicker
                selectedVariable={element.props.defaultValue}
                onVariableSelected={defaultValue => {
                  configElement(elementId, { defaultValue })
                }}
              />
            </Widget>
            <Widget title="Data">
              <FlowVariablePicker
                selectedVariable={element.props.data}
                onVariableSelected={data => {
                  configElement(elementId, { data })
                }}
              />
            </Widget>
            <Widget title="Label Key">
              <NTextInput
                value={element.props.labelKey}
                onChange={e => {
                  configElement(elementId, { labelKey: e.target.value })
                }}
              />
            </Widget>
          </AccordionWidget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        {selectionType === 'multi' ? (
          <NMultiSelect
            required={isRequired}
            disabled
            values={[]}
            placeholder="Multiple Select"
            options={[]}
            {...restProps}
          />
        ) : (
          <NSingleSelect required={isRequired} disabled options={[]} placeholder="Single Select" {...restProps} />
        )}
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetPickList2: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'Data Pick List',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.Selection2,
  props: {
    label: 'Data Pick List',
    selectionType: 'single',
    // FIXME: Don't know why I need to do this.
    options: ['hacked'],
  },
  children: [],
}
