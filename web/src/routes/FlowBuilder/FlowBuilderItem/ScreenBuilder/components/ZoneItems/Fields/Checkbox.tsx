import * as React from 'react'
import { classnames } from '../../../../../../../common/classnames'
import { NCheckbox } from '../../../../../../../components/NCheckbox/NCheckbox'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { FlowVariablePicker } from '../../../../../FlowBuilderVariable/FlowVariablePicker'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

export function Checkbox({ elementId, parentId, index, canDrag, canDrop }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrag, canDrop })
  const element = elements[elementId]
  const isSelected = selectedElement === elementId

  const { label, inputType: type, defaultValue } = element.props
  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Date & Time"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
              noRequired
            />
            <Widget title="Default Value">
              <FlowVariablePicker
                inputType={type}
                selectedVariable={defaultValue}
                onVariableSelected={varOption => {
                  configElement(elementId, { defaultValue: varOption })
                }}
              />
            </Widget>
          </AccordionWidget>
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <NCheckbox title={label} checked={false} onChange={() => {}} type={type} />
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetCheckbox: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.Checkbox />,
  label: 'Checkbox',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.Input,
  props: {
    label: 'Checkbox',
    inputType: 'boolean',
  },
  children: [],
}
