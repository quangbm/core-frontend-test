import React, { useState } from 'react'
import { useQuery } from 'react-query'
import styled from 'styled-components/macro'
import { ActionMetadataResponse } from '../../../../../../../../../shared/src/api'
import { classnames } from '../../../../../../../common/classnames'
import { ActionMetadataMapInputModal } from '../../../../../../../components/Builder/ActionMetadataMapInputModal'
import { spacing } from '../../../../../../../components/GlobalStyle'
import { NButton } from '../../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../../components/NDivider'
import { SelectOption } from '../../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../../components/NSelect/NSingleSelect'
import { typography } from '../../../../../../../components/NTypography'
import { useSearchText } from '../../../../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../../../../services/api'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { LayoutWrapper } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

export const StyledWrapper = styled(LayoutWrapper)`
  flex-direction: column;
  padding: ${spacing('sm')};
`

export const Header = styled.div`
  display: flex;
  align-items: center;
`

export const HeaderTxt = styled.p`
  ${typography('s600')}
`

export const PlaceholderTxt = styled.p`
  ${typography('x-small-ui-text')}
`

export function FileDropzone({ elementId, parentId, index, canDrop, canDrag }: ZoneProps) {
  const [{ isOver, isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrop, canDrag })

  const [{ searchText, searchValue }, handleSearchChange] = useSearchText()
  // get action metadata data
  const { data, isFetching: isLoadingActionMetadata } = useQuery(
    [QueryKeys.Integrations.getAllActionMetadata, { searchText }],
    APIServices.Integrations.getAllActionMetadata,
    {
      keepPreviousData: true,
      select: data => {
        const { selectOptions, getDataById } = data.data.data.reduce(
          (prev, cur) => {
            return {
              selectOptions: [...prev.selectOptions, { value: cur.name, label: cur.displayName }],
              getDataById: { ...prev.getDataById, [cur.name]: cur },
            }
          },
          {
            selectOptions: [] as SelectOption[],
            getDataById: {} as Record<string, ActionMetadataResponse>,
          },
        )

        return {
          actionMetadataOptions: selectOptions,
          actionMetadataById: getDataById,
        }
      },
    },
  )

  //
  const [showInputMap, setShowInputMap] = useState(false)

  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const isSelected = selectedElement === elementId

  const element = elements[elementId]

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <AccordionWidget
          title="File Dropzone"
          addPadContent
          onBack={() => {
            selectElement(ROOT_ELEMENT)
          }}>
          <BaseRightWidget
            elementId={elementId}
            element={element}
            configElement={configElement}
            setElementName={setElementName}
          />
          <Widget title="Action metadata">
            <NSingleSelect
              options={data?.actionMetadataOptions || []}
              value={element.props.actionMetadata}
              onValueChange={newValue => {
                configElement(elementId, { actionMetadata: newValue, inputMap: {} })
              }}
              fullWidth
              isSearchable
              searchValue={searchValue}
              onSearchValueChange={handleSearchChange}
              isLoading={isLoadingActionMetadata}
            />
            <NDivider size="md" />
            <NButton
              disabled={!element.props.actionMetadata}
              size="small"
              type="outline-3"
              block
              onClick={() => setShowInputMap(true)}>
              Map input
            </NButton>
            <ActionMetadataMapInputModal
              visible={showInputMap}
              setVisible={setShowInputMap}
              actionMetadata={
                element.props.actionMetadata ? data?.actionMetadataById[element.props.actionMetadata] : undefined
              }
              defaultInputMap={element.props.inputMap || {}}
              onSubmit={newInputMap => {
                configElement(elementId, { inputMap: newInputMap })
                setShowInputMap(false)
              }}
            />
          </Widget>
        </AccordionWidget>
      }>
      <StyledWrapper
        ref={ref}
        className={classnames([
          isOver && 'is-over',
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <Header>
          <ScreenFlowItemIcons.PickList />
          <NDivider vertical size="md" />
          <div>
            <HeaderTxt>File Dropzone component:</HeaderTxt>
          </div>
        </Header>
        <NDivider size="md" />
        <PlaceholderTxt>This is a placeholder. File Dropzone don't run in builder.</PlaceholderTxt>
      </StyledWrapper>
    </ZoneItemLayout>
  )
}

export const widgetFileDropzone: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'Media Uploader',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.FileDropzone,
  props: {
    label: 'File',
    inputMap: {},
  },
  children: [],
}
