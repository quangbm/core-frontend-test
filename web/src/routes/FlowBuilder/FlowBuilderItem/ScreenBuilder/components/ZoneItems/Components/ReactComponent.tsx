import { classnames } from '../../../../../../../common/classnames'
import { NCodeEditor } from '../../../../../../../components/NCodeEditor'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { LayoutWrapper } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

export function ReactComponent({ elementId, parentId, index, canDrop, canDrag }: ZoneProps) {
  const [{ isOver, isMouseOver, mousePosition }, ref] = useElement({
    elementId,
    parentId,
    index,
    canDrop,
    canDrag,
  })

  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const isSelected = selectedElement === elementId

  const element = elements[elementId]

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <AccordionWidget
          title="React Component"
          addPadContent
          onBack={() => {
            selectElement(ROOT_ELEMENT)
          }}>
          <BaseRightWidget
            elementId={elementId}
            element={element}
            configElement={configElement}
            setElementName={setElementName}
            noRequired
          />
          {/* <Widget required title="Initial Props">
              <FlowVariablePicker
                selectedVariable={element.props.initialProps}
                onVariableSelected={initialProps => {
                  configElement(elementId, {initialProps})
                }}
              />
            </Widget> */}
          <Widget title="React Component">
            <NCodeEditor
              value={element.props.value}
              onChange={e => configElement(elementId, { value: e.target.value })}
            />
          </Widget>
        </AccordionWidget>
      }>
      <LayoutWrapper
        ref={ref}
        className={classnames([
          isOver && 'is-over',
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        &lt;ReactComponent /&gt; can not preview in builder
      </LayoutWrapper>
    </ZoneItemLayout>
  )
}

export const widgetReactComponent: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'React Component',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.ReactComponent,
  props: {
    value: '',
    inputType: 'object',
  },
  children: [],
}
