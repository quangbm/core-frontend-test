import styled from 'styled-components/macro'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { MousePosition } from '../../../../../LayoutBuilder/utils/models'

export const Placeholder = styled('div').attrs((props: { mousePosition: MousePosition | null }) => {
  switch (props.mousePosition) {
    case MousePosition.Top:
      return { className: 'element-placeholder', style: { top: 0, height: 2, left: 0, right: 0 } }
    case MousePosition.Bottom:
      return { className: 'element-placeholder', style: { bottom: 0, height: 2, left: 0, right: 0 } }
    default:
      return { className: 'element-placeholder', style: {} }
  }
})<{ mousePosition: MousePosition | null }>`
  position: absolute;
  background: ${color('Primary700')};
  display: none;
  z-index: 9999;
  pointer-events: none;
`
export const LayoutWrapper = styled('div')`
  display: flex;
  align-items: flex-start;
  background: ${color('white')};
  position: relative;
  min-height: 88px;
  &.is-over:not(.is-sort) {
    background: ${color('Primary200')};
  }
  &:after {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    pointer-events: none;
    border: 1px solid ${color('Neutral200')};
  }
  &.is-selected:after,
  &.is-mouse-over:after {
    border-style: dashed;
    border-color: ${color('Primary700')};
  }
  &.is-sort {
    ${Placeholder} {
      display: block;
    }
  }
  & > *:not(.element-placeholder):not(.element-actions) {
    flex: 1;
  }
`

export const FieldWrapper = styled(LayoutWrapper)`
  min-height: unset;
  width: 100%;
  padding: ${spacing('sm')};
`
