import * as React from 'react'
import { classnames } from '../../../../../../../common/classnames'
import { NDivider } from '../../../../../../../components/NDivider'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget } from '../../Sidebar'
import { Header, HeaderTxt, PlaceholderTxt, StyledWrapper } from '../Components/FileDropzone'
import { Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

export function CSVInput({ elementId, parentId, index, canDrag, canDrop }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrag, canDrop })

  const element = elements[elementId]
  const isSelected = selectedElement === elementId
  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Pick list"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            {/* <Widget title="Object">
              <NSingleSelect
                required
                options={[]}
                value={element.props.objectName}
                onValueChange={objectName => configElement(elementId, { objectName })}
              />
            </Widget> */}
          </AccordionWidget>
        </React.Fragment>
      }>
      <StyledWrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <Header>
          <ScreenFlowItemIcons.PickList />
          <NDivider vertical size="md" />
          <div>
            <HeaderTxt>CSV Input component:</HeaderTxt>
          </div>
        </Header>
        <NDivider size="md" />
        <PlaceholderTxt>This is a placeholder. CSV Input don't run in builder.</PlaceholderTxt>
        <Placeholder mousePosition={mousePosition} />
      </StyledWrapper>
    </ZoneItemLayout>
  )
}

export const widgetCSVInput: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'CSV Input',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.Input,
  props: {
    label: 'CSV',
    inputType: 'array',
  },
  children: [],
}
