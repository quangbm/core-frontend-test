import * as React from 'react'
import { useQuery } from 'react-query'
import { PickListResponse } from '../../../../../../../../../shared/src/api'
import { classnames } from '../../../../../../../common/classnames'
import { NButton } from '../../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../../components/NDivider'
import { SelectOption } from '../../../../../../../components/NSelect/model'
import { NMultiSelect } from '../../../../../../../components/NSelect/NMultiSelect'
import { NSingleSelect } from '../../../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../../../components/NTextInput/NTextInput'
import { useSearchText } from '../../../../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../../../../services/api'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ConfigPickListModal } from '../../ConfigPickListModal'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget, Widget } from '../../Sidebar'
import { FieldWrapper as Wrapper, Placeholder } from '../StyledElements'
import { ZoneItemLayout } from '../ZoneItemLayout'

const PICKLIST_TYPE_OPTIONS: SelectOption[] = [
  { label: 'Single', value: 'single' },
  { label: 'Multiple', value: 'multi' },
]

export function PickList({ elementId, parentId, index, canDrag, canDrop }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const [{ isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrag, canDrop })
  const [showConfig, setShowConfig] = React.useState(false)

  const element = elements[elementId]
  const isSelected = selectedElement === elementId
  const { defaultSelectValue, defaultValue: _, selectionType, isRequired, pickListId, ...restProps } = element.props

  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  const { data: pickListsData, isLoading: isLoadingPickList } = useQuery(
    [QueryKeys.PickLists.getAllPickLists, { searchText, status: 'ACTIVE' }],
    APIServices.PickLists.getAllPickLists,
    {
      keepPreviousData: true,
      select: resp =>
        resp.data.data.reduce(
          (a, pickList) => {
            a.pickListOptions.push({
              label: pickList.displayName || pickList.name,
              value: `${pickList.id}`,
            })
            a.pickListItemsById[pickList.id] = pickList
            return a
          },
          { pickListOptions: [] as SelectOption[], pickListItemsById: {} as Record<string, PickListResponse> },
        ),
    },
  )

  const options = pickListId
    ? pickListsData?.pickListItemsById[pickListId]?.items
        .filter(item => !item.isDeprecated)
        .map(item => ({ label: item.displayName, value: item.name })) || []
    : []

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Pick list"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            <Widget title="Type">
              <NSingleSelect
                options={PICKLIST_TYPE_OPTIONS}
                value={element.props.selectionType}
                onValueChange={newVal => {
                  configElement(elementId, { selectionType: newVal, defaultSelectValue: undefined })
                }}
              />
            </Widget>
            <Widget title="Caption">
              <NTextInput
                value={element.props.caption}
                onChange={e => {
                  configElement(elementId, { caption: e.target.value })
                }}
              />
            </Widget>
            <Widget title="Pick list">
              <NSingleSelect
                required
                isSearchable
                searchValue={searchValue}
                onSearchValueChange={handleSearchChange}
                fullWidth
                value={pickListId ? pickListId.toString() : ''}
                onValueChange={newVal => {
                  configElement(elementId, { pickListId: Number(newVal) })
                }}
                placeholder="Select pick list"
                isLoading={isLoadingPickList}
                options={pickListsData?.pickListOptions || []}
              />
              <NDivider size="md" />
              <NButton disabled={!pickListId} onClick={() => setShowConfig(true)}>
                Config
              </NButton>
            </Widget>
            <Widget title="Default Value">
              {selectionType === 'multi' ? (
                <NMultiSelect
                  fullWidth
                  values={defaultSelectValue ? defaultSelectValue : []}
                  options={options}
                  onValuesChange={newVal => {
                    configElement(elementId, { defaultSelectValue: newVal })
                  }}
                />
              ) : (
                <NSingleSelect
                  value={defaultSelectValue ? defaultSelectValue[0] : undefined}
                  options={options}
                  onValueChange={newVal => {
                    configElement(elementId, { defaultSelectValue: [newVal] })
                  }}
                />
              )}
            </Widget>
          </AccordionWidget>
          {showConfig && pickListId && pickListsData?.pickListItemsById[pickListId] && (
            <ConfigPickListModal
              visible={showConfig}
              setVisible={setShowConfig}
              data={pickListsData?.pickListItemsById[pickListId]}
            />
          )}
        </React.Fragment>
      }>
      <Wrapper
        ref={ref}
        className={classnames([
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        {selectionType === 'multi' ? (
          <NMultiSelect
            required={isRequired}
            disabled
            values={defaultSelectValue ? defaultSelectValue : []}
            options={options}
            {...restProps}
          />
        ) : (
          <NSingleSelect
            required={isRequired}
            disabled
            value={defaultSelectValue ? defaultSelectValue[0] : undefined}
            options={options}
            {...restProps}
          />
        )}
        <Placeholder mousePosition={mousePosition} />
      </Wrapper>
    </ZoneItemLayout>
  )
}

export const widgetPickList: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'Pick List',
  type: ScreenElementType.Input,
  component: ScreenElementComponent.PickList,
  props: {
    label: 'Pick List',
    selectionType: 'single',
  },
  children: [],
}
