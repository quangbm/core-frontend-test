import * as React from 'react'
import { classnames } from '../../../../../../../common/classnames'
import { NCheckbox } from '../../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../../components/NDivider'
import { ZoneProps } from '../../../../../../LayoutBuilder/utils/models'
import { useScreenAction, useScreenBuilder } from '../../../contexts/ScreenBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { ROOT_ELEMENT } from '../../../utils/builder'
import { ScreenElementComponent, ScreenElementType, ScreenWidgetItemType } from '../../../utils/models'
import { BaseRightWidget } from '../../BaseRightWidget'
import { ScreenFlowItemIcons } from '../../ScreenFlowItemIcons'
import { AccordionWidget } from '../../Sidebar'
import { ZoneItemLayout } from '../ZoneItemLayout'
import { Header, HeaderTxt, PlaceholderTxt, StyledWrapper } from './FileDropzone'

export const Withdrawal = React.memo(function Node({ elementId, parentId, index, canDrag, canDrop }: ZoneProps) {
  const { elements, selectedElement } = useScreenBuilder()
  const { selectElement, configElement, setElementName } = useScreenAction()
  const element = elements[elementId]
  const isSelected = selectedElement === elementId
  const [{ isOver, isMouseOver, mousePosition }, ref] = useElement({ elementId, parentId, index, canDrop, canDrag })

  return (
    <ZoneItemLayout
      shouldRenderSidebar={isSelected}
      sidebar={
        <React.Fragment>
          <AccordionWidget
            title="Withdrawal"
            addPadContent
            onBack={() => {
              selectElement(ROOT_ELEMENT)
            }}>
            <BaseRightWidget
              elementId={elementId}
              element={element}
              configElement={configElement}
              setElementName={setElementName}
            />
            <NDivider />
            <NCheckbox
              title="Is withdrawal?"
              checked={element.props.isWithdrawal}
              onChange={e => {
                configElement(elementId, { isWithdrawal: e.target.checked })
              }}
            />
          </AccordionWidget>
        </React.Fragment>
      }>
      <StyledWrapper
        ref={ref}
        className={classnames([
          isOver && 'is-over',
          isMouseOver && 'is-mouse-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementId)
        }}>
        <Header>
          <ScreenFlowItemIcons.PickList />
          <NDivider vertical size="md" />
          <div>
            <HeaderTxt>Withdrawl Component</HeaderTxt>
          </div>
        </Header>
        <NDivider size="md" />
        <PlaceholderTxt>This is a placeholder. Withdrawl don't run in builder.</PlaceholderTxt>
      </StyledWrapper>
    </ZoneItemLayout>
  )
})

export const widgetWithdrawal: ScreenWidgetItemType = {
  icon: <ScreenFlowItemIcons.PickList />,
  label: 'Money Input',
  type: ScreenElementType.Component,
  component: ScreenElementComponent.Withdrawal,
  props: {
    isWithdrawal: true,
  },
  children: [],
}
