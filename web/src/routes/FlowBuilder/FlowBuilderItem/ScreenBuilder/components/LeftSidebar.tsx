import React, { useMemo, useState } from 'react'
import { useDrag } from 'react-dnd'
import styled from 'styled-components/macro'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { GlobalIcons, Icons } from '../../../../../components/Icons'
import { elevation } from '../../../../../components/NElevation'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { typography } from '../../../../../components/NTypography'
import { useSearchText } from '../../../../../hooks/useSearchText'
import { LEFT_SIDEBAR } from '../utils/builder'
import { DraggableElement, ScreenElementType, ScreenWidgetItemType } from '../utils/models'
import { WIDGET_ITEMS } from './ZoneItems/zoneItems'

const Wrapper = styled('div')`
  width: 251px;
  background: ${color('white')};
  overflow-y: auto;

  .search-container {
    padding: ${spacing('md')};
  }
`

type LeftSidebarProps = {}

export function LeftSidebar({}: LeftSidebarProps) {
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  const { inputItems, componentItems, displayItems } = useMemo(() => {
    const filteredItems = WIDGET_ITEMS.filter(item =>
      item.label.toLowerCase().includes((searchText || '').toLowerCase()),
    )

    return filteredItems.reduce(
      (a, item) => {
        if (item.type === ScreenElementType.Input) {
          return {
            ...a,
            inputItems: [...a.inputItems, item],
          }
        }
        if (item.type === ScreenElementType.Component) {
          return {
            ...a,
            componentItems: [...a.componentItems, item],
          }
        }
        return {
          ...a,
          displayItems: [...a.displayItems, item],
        }
      },
      {
        inputItems: [] as ScreenWidgetItemType[],
        componentItems: [] as ScreenWidgetItemType[],
        displayItems: [] as ScreenWidgetItemType[],
      },
    )
  }, [searchText])

  return (
    <Wrapper>
      <NTextInput
        className="search-container"
        left={<GlobalIcons.Search />}
        placeholder="Search"
        value={searchValue}
        onChange={e => {
          const newVal = e.target.value
          handleSearchChange(newVal)
        }}
      />
      <Widget items={inputItems} title="Input" />
      <Widget items={componentItems} title="Component" />
      <Widget items={displayItems} title="Display" />
    </Wrapper>
  )
}

///////////////////////////////////
///////////////////////////////////

const WidgetContent = styled('div')`
  display: flex;
  flex-direction: column;

  background: ${color('Neutral200')};
  .drop-placeholder {
    display: none;
    visibility: hidden;
  }
`

const WidgetTitle = styled.div`
  ${typography('overline')};
  display: flex;
  justify-content: space-between;
  padding: ${spacing('sm')} ${spacing('md')};
  border-top: 1px solid ${color('Neutral200')};

  color: ${color('Neutral400')};
  text-transform: capitalize;

  cursor: pointer;

  &:hover {
    background-color: ${color('Neutral200')};
  }
`

const WidgetIconContainer = styled.div`
  display: flex;
  width: 16px;
  height: 16px;
  justify-content: center;
  align-items: center;
`

const WidgetWrapper = styled('div')``

type WidgetProps = {
  items: ScreenWidgetItemType[]
  title: string
}

function Widget({ items, title }: WidgetProps) {
  const [isOpen, setIsOpen] = useState(true)
  return (
    <WidgetWrapper>
      <WidgetTitle onClick={() => setIsOpen(prev => !prev)}>
        <span>{title}</span>
        <WidgetIconContainer>{isOpen ? <Icons.Down /> : <Icons.Right />}</WidgetIconContainer>
      </WidgetTitle>
      {isOpen && (
        <WidgetContent>
          {items.map((item, itemIndex) => (
            <DraggableItem data={item} key={`${item.label}`} index={itemIndex} />
          ))}
        </WidgetContent>
      )}
    </WidgetWrapper>
  )
}

///////////////////////////////////
///////////////////////////////////

const Item = styled('div')`
  background: ${color('white')};
  cursor: move;
  user-select: none;

  display: flex;
  align-items: center;
  height: 40px;
  padding: 0 ${spacing('md')};

  &.is-dragging {
    background-color: ${color('white')};
    ${elevation('Elevation400')};
  }

  &:hover {
    background-color: ${color('Neutral200')};
  }
`

const ItemLabel = styled('div')`
  margin-left: ${spacing('sm')};
  ${typography('small-bold-text')}
`

type DraggableItemProps = {
  data: ScreenWidgetItemType
  index: number
}

function DraggableItem({ data, index }: DraggableItemProps) {
  const { icon, ...item } = data
  const [, ref] = useDrag<DraggableElement, any, any>({
    type: item.type,
    item: {
      item: {
        label: item.label,
        component: item.component,
        // ignore for now becuz conflict defaultValue type with new PickList (string[]) while other contentValue[]
        //@ts-ignore
        props: item.props,
        type: item.type,
        children: item.children,
      },
      parentId: LEFT_SIDEBAR,
      index,
    },
    options: { dropEffect: 'copy' },
  })
  return (
    <Item ref={ref}>
      {icon}
      <ItemLabel>{item.label}</ItemLabel>
    </Item>
  )
}
