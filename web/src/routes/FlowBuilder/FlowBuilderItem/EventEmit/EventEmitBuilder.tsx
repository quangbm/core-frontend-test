import { FC } from 'react'
import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled from 'styled-components/macro'
import { EmitEventFlowItemResponse } from '../../../../../../shared/src/api/models'
import { spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../components/NPerfectScrollbar'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { useSearchText } from '../../../../hooks/useSearchText'
import { useValidateString } from '../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../services/api'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { FlowIcons } from '../../components/Icons'
import { DataFormType } from '../../models'
import { FlowIconContainerClassName } from '../../utils/constants'
import { parseConfigArrayToConfigObject, parseConfigObjectToConfigArray } from '../../utils/functions'
import { FlowNodeModalProps, NodeType, WidgetItemType } from '../../utils/types'
import { FlowIconContainer } from '../CommonStyledComponents'
import { AssignValueForm } from '../FlowItemData/components/AssignValueForm'

const ScrollWrapper = styled(NPerfectScrollbar)`
  max-height: 70vh;
`
const FormWrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`

export const eventEmitWidget: WidgetItemType = {
  leftIcon: <FlowIcons.EventEmit />,
  flowIcon: (
    <FlowIconContainer className={FlowIconContainerClassName} color="Tertiary900">
      <FlowIcons.EventEmit />
    </FlowIconContainer>
  ),
  label: 'Emit Event',
  type: NodeType.EmitEvent,
  data: {
    name: '',
    displayName: '',
    attributes: {},
  },
}

export const EventEmitModal: FC<FlowNodeModalProps> = ({ nodeModal, isSubmitting, onCancel, onSubmit }) => {
  const { validateFunction } = useValidateString()
  const formMethods = useForm<DataFormType>({
    defaultValues: nodeModal.data ? transformFlowItemToEventEmit(nodeModal.data as EmitEventFlowItemResponse) : {},
  })
  const { register, getValues, setValue, formState, handleSubmit, control, watch } = formMethods
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  const watchEventName = watch('eventName')
  const fieldArray = useFieldArray({
    control,
    name: 'payload',
  })

  const { data: eventOptions = [], isLoading: isLoadingObjects } = useQuery(
    [QueryKeys.Events.getEventList, { searchText }],
    APIServices.Events.getEventList,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  const onFormSubmit = (data: any) => {
    onSubmit({
      name: data.name,
      displayName: data.displayName,
      description: data.description,
      type: nodeModal.type,
      attributes: {
        eventName: data.eventName,
        payload: data.payload && parseConfigArrayToConfigObject(data.payload),
      },
      nodeAttributes: {
        position: nodeModal.position,
      },
    })
  }

  return (
    <>
      <NModal.Header onClose={onCancel} title={`${nodeModal.nodeId ? 'Edit' : 'New'} Event Emit`} />
      <NModal.Body>
        <ScrollWrapper>
          <FormWrapper>
            <NRow>
              <NColumn flex={1}>
                <NTextInput
                  {...register('displayName', { validate: validateFunction })}
                  required
                  error={formState.errors.displayName?.message || formState.errors.displayName?.type}
                  placeholder="Display name"
                  label="Display Name"
                  onBlur={e => {
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>

              <NDivider vertical size="xl" />
              <NColumn flex={1}>
                <NTextInput
                  disabled={!!nodeModal.nodeId}
                  {...register('name', { validate: !nodeModal.nodeId ? validateFunction : undefined })}
                  required
                  error={formState.errors.name?.message || formState.errors.name?.type}
                  placeholder="API Name"
                  label="API Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={1}>
                <NTextArea rows={4} {...register('description')} label="Description" />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn flex={0.5}>
                <Controller
                  name="eventName"
                  defaultValue={''}
                  rules={{
                    required: {
                      value: true,
                      message: 'Required',
                    },
                  }}
                  control={control}
                  render={({ field, fieldState }) => (
                    <NSingleSelect
                      required
                      options={eventOptions}
                      searchValue={searchValue}
                      onSearchValueChange={handleSearchChange}
                      isLoading={isLoadingObjects}
                      error={fieldState.error?.message}
                      isSearchable
                      fullWidth
                      label="Event"
                      placeholder="Select event"
                      value={field.value}
                      onValueChange={value => {
                        if (value !== watchEventName) {
                          setValue('payload', [])
                          fieldArray.append({ fieldName: '' })
                        }
                        field.onChange(value)
                      }}
                    />
                  )}
                />
              </NColumn>
            </NRow>
            <NDivider size="md" />
            {watchEventName && (
              <>
                <AssignValueForm fieldArray={fieldArray} formMethods={formMethods} objectName={watchEventName} />
              </>
            )}
          </FormWrapper>
        </ScrollWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={onCancel} onFinish={handleSubmit(onFormSubmit)} />
    </>
  )
}

const transformFlowItemToEventEmit = (flowItem: EmitEventFlowItemResponse) => {
  return {
    name: flowItem.name,
    displayName: flowItem.displayName,
    description: flowItem.description,
    payload: parseConfigObjectToConfigArray(flowItem.attributes?.payload),
    eventName: flowItem.attributes?.eventName,
  }
}
