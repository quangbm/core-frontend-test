import { useDrag } from 'react-dnd'
import styled from 'styled-components/macro'
import { FlowType } from '../../../../../shared/src/api/endpoints/flows'
import { Handle } from '../components/Node'
import { useBuilderState } from '../redux/hooks/useBuilderState'
import { TRIGGER_NAME, TRIGGER_POSITION } from '../utils/constants'
import { DroppableType } from '../utils/types'
import { BasicFlowTrigger } from './BasicFlowTrigger'
import { FLOW_TRIGGER } from './FlowTrigger'

type WrapperProps = { top: number; left: number; isDragging: boolean; isSelected: boolean }

const Wrapper = styled('div').attrs(({ top, left, isDragging }: WrapperProps) => ({
  style: {
    transform: `translate(calc(${left}px - 50%), calc(${top}px - 50%))`,
    opacity: isDragging ? 0 : 1,
  },
}))<WrapperProps>`
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  position: absolute;
`

type FlowTriggerNodeProps = {
  flowType: FlowType
}

const TRIGGER_FLOW_NODE_ID = 'TRIGGER_FLOW_NODE'

export function FlowTriggerNode({ flowType }: FlowTriggerNodeProps) {
  const { nodeConnections } = useBuilderState()

  const connection = nodeConnections[TRIGGER_NAME]

  const canConnect = (connection && connection.maxConnections > 0) || false

  const [, handleRef] = useDrag({
    type: DroppableType.ConnectNodes,
    item: {
      id: TRIGGER_FLOW_NODE_ID,
      name: TRIGGER_NAME,
    },
  })

  const triggerContent = FLOW_TRIGGER[flowType]

  return (
    <Wrapper top={TRIGGER_POSITION} left={TRIGGER_POSITION} isSelected={false} isDragging={false}>
      {triggerContent || <BasicFlowTrigger type={flowType} />}
      {canConnect && <Handle ref={handleRef} />}
    </Wrapper>
  )
}
