import React from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components/macro'
import { DataFlowResponse } from '../../../../../shared/src/api'
import { color, spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NDivider } from '../../../components/NDivider'
import { NRow } from '../../../components/NGrid/NGrid'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { NToast } from '../../../components/NToast'
import { typography } from '../../../components/NTypography'
import { APIServices, QueryKeys } from '../../../services/api'
import { ReactComponent as ConditionIcon } from '../assets/icons/condition.svg'
import { TriggerIcons } from '../components/Icons'
import { DataFlowTriggerModal } from '../FlowConfigModal/DataFlowTriggerModal'
import { FlowSelectObjectModal } from '../FlowConfigModal/FlowSelectObjectModal'
import { useFlowModifyConfirm } from '../hooks/useFlowModifyConfirm'
import { useBuilderState } from '../redux/hooks/useBuilderState'

const Wrapper = styled.div`
  background: ${color('Neutral900')};
  width: 220px;
  min-height: 140px;
  padding: ${spacing('md')};
  display: flex;
  flex-direction: column;
  border-radius: ${spacing('xs')};
`
const IconWrapper = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 24px;
  background: ${color('Primary700')};
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: ${spacing('xs')};
`
const TextHeader = styled.div`
  ${typography('x-small-ui-text')}
  color:${color('Neutral500')};
`

const SelectObjectBtn = styled(NButton)`
  justify-content: flex-start;
`
const DetailText = styled.p`
  color: ${color('white')};
  ${typography('button')}
  margin-left: ${spacing('xs')};
`

const LoadingWrapper = styled.div`
  padding: ${spacing('xl')};
  display: flex;
  justify-content: center;
`

export const DataFlowTrigger: React.FC = () => {
  const theme = useTheme()
  const [modalObject, setModalObject] = React.useState(false)
  const [modalTrigger, setModalTrigger] = React.useState(false)
  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  const { status } = useBuilderState()
  const warnFlowChange = useFlowModifyConfirm()

  const { data: flowData, isLoading: isLoadingFlow } = useQuery(
    [QueryKeys.Flows.getFlowTemplateVersion, { flowName, version }],
    APIServices.Flows.getFlowTemplateVersion,
    {
      //@ts-ignore
      select: res => res.data as DataFlowResponse,
    },
  )

  const { data: objectData, isLoading: isLoadingObjectData } = useQuery(
    [QueryKeys.Builder.getObjectSchema, { objName: flowData!.attributes.objectName! }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: !!flowData?.attributes?.objectName,
    },
  )

  const selectObject = () => {
    if (status !== 'draft') {
      warnFlowChange()
      return
    }
    if (flowData?.attributes.action) {
      setModalObject(true)
      return
    }
    NToast.warning({ title: 'Please select flow action first!' })
  }

  return (
    <Wrapper>
      <NRow justify="space-between" align="center">
        <NRow align="center">
          <IconWrapper>
            <TriggerIcons.ScheduleTrigger />
          </IconWrapper>
          <TextHeader>Data Trigger</TextHeader>
        </NRow>
        <NButton
          size="small"
          type="link"
          onClick={() => {
            if (status !== 'draft') {
              warnFlowChange()
              return
            }
            setModalTrigger(true)
          }}>
          EDIT
        </NButton>
      </NRow>
      <NDivider size="sm" lineSize={1} lineColor={color('Neutral700')({ theme })} />
      {isLoadingFlow ? (
        <LoadingWrapper>
          <NSpinner size={22} strokeWidth={2} color={color('Primary700')({ theme })} />
        </LoadingWrapper>
      ) : (
        <>
          {flowData && (
            <NRow align="center">
              <ConditionIcon />
              <DetailText>A record is {flowData.attributes?.action}</DetailText>
            </NRow>
          )}
          <NDivider size="sm" />
          {/* <NRow align="center">
        <RunIcon />
        <DetailText>Before a record is saved</DetailText>
      </NRow> */}

          <SelectObjectBtn
            icon={<TriggerIcons.ScheduleTriggerObject />}
            size="small"
            type="link"
            loading={isLoadingObjectData}
            onClick={selectObject}>
            {objectData?.data.displayName || 'Select Object'}
          </SelectObjectBtn>
        </>
      )}

      {modalObject && (
        <FlowSelectObjectModal flowData={flowData} showModal={modalObject} setShowModal={setModalObject} />
      )}
      {modalTrigger && (
        <DataFlowTriggerModal flowData={flowData} showModal={modalTrigger} setShowModal={setModalTrigger} />
      )}
    </Wrapper>
  )
}
