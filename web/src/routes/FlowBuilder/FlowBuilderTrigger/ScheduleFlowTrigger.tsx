import { format, parse } from 'date-fns'
import React from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { ScheduleFlowResponse } from '../../../../../shared/src/api'
import { color, spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NRow } from '../../../components/NGrid/NGrid'
import { typography } from '../../../components/NTypography'
import { APIServices, QueryKeys } from '../../../services/api'
import { TriggerIcons } from '../components/Icons'
import { FlowSetScheduleModal } from '../FlowConfigModal/FlowSetScheduleModal'
import { useFlowModifyConfirm } from '../hooks/useFlowModifyConfirm'
import { useBuilderState } from '../redux/hooks/useBuilderState'

const Wrapper = styled.div`
  background: ${color('Neutral900')};
  width: 220px;
  padding: ${spacing('md')};
  display: flex;
  flex-direction: column;
  border-radius: ${spacing('xs')};
`
const IconWrapper = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 24px;
  background: ${color('Primary700')};
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: ${spacing('xs')};
`
const TextHeader = styled.div`
  ${typography('x-small-ui-text')}
  color:${color('Neutral500')};
`

const TimeSection = styled.div`
  padding: ${spacing('xs')} 0;
  color: ${color('white')};
  display: flex;
  flex-direction: column;
`
const Element = styled.div`
  ${typography('x-small-ui-text')};
  display: flex;
  align-items: center;
  padding: ${spacing('xs')} 0;
`
const Icon = styled.div`
  margin-right: ${spacing('xs')};
  display: flex;
  align-items: center;
`

const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const ScheduleFlowTrigger: React.FC = () => {
  const [modalSchedule, setModalSchedule] = React.useState(false)
  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  const { status } = useBuilderState()
  const warnFlowChange = useFlowModifyConfirm()
  const { data: flowData } = useQuery(
    [QueryKeys.Flows.getFlowTemplateVersion, { flowName, version }],
    APIServices.Flows.getFlowTemplateVersion,
    {
      //@ts-ignore
      select: res => res.data as ScheduleFlowResponse,
    },
  )
  return (
    <Wrapper>
      <HeaderWrapper>
        <NRow align="center">
          <IconWrapper>
            <TriggerIcons.ScheduleTrigger />
          </IconWrapper>
          <TextHeader>Schedule Trigger</TextHeader>
        </NRow>
        <NButton
          size="small"
          type="link"
          onClick={() => {
            if (status !== 'draft') {
              warnFlowChange()
              return
            }
            setModalSchedule(true)
          }}>
          EDIT
        </NButton>
      </HeaderWrapper>
      <TimeSection>
        <Element>
          <Icon>
            <TriggerIcons.ScheduleTriggerDate />
          </Icon>
          {flowData?.attributes?.startDate &&
            format(parse(flowData?.attributes?.startDate, 'yyyy-MM-dd', new Date()), 'MMM dd, yyyy')}
        </Element>
        <Element>
          <Icon>
            <TriggerIcons.ScheduleTriggerTime />
          </Icon>
          {flowData?.attributes?.startTime}
        </Element>
      </TimeSection>
      {modalSchedule && (
        <FlowSetScheduleModal flowData={flowData} showModal={modalSchedule} setShowModal={setModalSchedule} />
      )}
    </Wrapper>
  )
}
