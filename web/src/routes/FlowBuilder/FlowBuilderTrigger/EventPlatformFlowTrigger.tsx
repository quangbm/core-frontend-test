import { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components/macro'
import { EventFlowResponse } from '../../../../../shared/src/api'
import { color, spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NDivider } from '../../../components/NDivider'
import { NRow } from '../../../components/NGrid/NGrid'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { typography } from '../../../components/NTypography'
import { APIServices, QueryKeys } from '../../../services/api'
import { TriggerIcons } from '../components/Icons'
import { EventSelectModal } from '../FlowConfigModal/EventSelectModal'
import { useFlowModifyConfirm } from '../hooks/useFlowModifyConfirm'
import { useBuilderState } from '../redux/hooks/useBuilderState'

const Wrapper = styled.div`
  background: ${color('Neutral900')};
  width: 220px;
  min-height: 100px;
  padding: ${spacing('md')};
  display: flex;
  flex-direction: column;
  border-radius: ${spacing('xs')};
`
const IconWrapper = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 24px;
  background: ${color('Primary700')};
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: ${spacing('xs')};
`
const TextHeader = styled.div`
  ${typography('x-small-ui-text')}
  color:${color('Neutral500')};
`

const SelectObjectBtn = styled(NButton)`
  justify-content: flex-start;
`

const LoadingWrapper = styled.div`
  padding: ${spacing('xl')};
  display: flex;
  justify-content: center;
`

export const EventPlatformFlowTrigger: FC = () => {
  const theme = useTheme()
  const [showEventModal, setShowEventModal] = useState(false)
  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  const { status } = useBuilderState()
  const warnFlowChange = useFlowModifyConfirm()

  const { data: flowData, isLoading: isLoadingFlow } = useQuery(
    [QueryKeys.Flows.getFlowTemplateVersion, { flowName, version }],
    APIServices.Flows.getFlowTemplateVersion,
    {
      //@ts-ignore
      select: res => res.data as EventFlowResponse,
    },
  )

  const { data: objectData, isLoading: isLoadingObjectData } = useQuery(
    [QueryKeys.Builder.getObjectSchema, { objName: flowData!.attributes.eventName! }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: !!flowData?.attributes?.eventName,
    },
  )

  return (
    <Wrapper>
      <NRow align="center">
        <IconWrapper>
          <TriggerIcons.ScheduleTrigger />
        </IconWrapper>
        <TextHeader>Event Trigger</TextHeader>
      </NRow>

      <NDivider size="sm" lineSize={1} lineColor={color('Neutral700')({ theme })} />
      {isLoadingFlow ? (
        <LoadingWrapper>
          <NSpinner size={22} strokeWidth={2} color={color('Primary700')({ theme })} />
        </LoadingWrapper>
      ) : (
        <SelectObjectBtn
          icon={<TriggerIcons.ScheduleTriggerObject />}
          size="small"
          type="link"
          loading={isLoadingObjectData}
          onClick={() => {
            if (status !== 'draft') {
              warnFlowChange()
              return
            }
            setShowEventModal(true)
          }}>
          {objectData?.data.displayName || 'Select Event'}
        </SelectObjectBtn>
      )}
      {showEventModal && flowData && (
        <EventSelectModal showModal={showEventModal} setShowModal={setShowEventModal} flowData={flowData} />
      )}
    </Wrapper>
  )
}
