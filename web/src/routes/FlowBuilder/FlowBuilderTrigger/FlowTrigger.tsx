import { DataFlowTrigger } from './DataFlowTrigger'
import { EventPlatformFlowTrigger } from './EventPlatformFlowTrigger'
import { ScheduleFlowTrigger } from './ScheduleFlowTrigger'

export const FLOW_TRIGGER: Record<string, JSX.Element | undefined> = {
  data: <DataFlowTrigger />,
  'time-based': <ScheduleFlowTrigger />,
  event: <EventPlatformFlowTrigger />,
}
