import { createContext, ReactNode, useCallback, useContext, useRef, useState } from 'react'
import { useParams } from 'react-router-dom'
import { FlowTemplateCloneDto } from '../../../../../shared/src/api'
import { NConfirmModal } from '../../../components/NModal/NConfirmModal'
import { FlowTemplateCloneModal } from '../../Dashboard/routes/Flow/components/FlowTemplateCloneModal'

type Confirm = (onConfirm?: () => void, onCancel?: () => void) => void

const FlowModifyConfirmContext = createContext<string | Confirm>(
  'useFlowModifyConfirm should be used inside FlowModifyConfirmProvider',
)

export function FlowModifyConfirmProvider({
  children,
  defaultValues,
}: {
  children: ReactNode
  defaultValues: FlowTemplateCloneDto
}) {
  const [showModal, setShowModal] = useState(false)
  const confirmRef = useRef<VoidFunction | undefined>(() => {})
  const cancelRef = useRef<VoidFunction | undefined>(() => {})
  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  const [showCloneModal, setShowCloneModal] = useState(false)

  const confirm = useCallback((onConfirm?: VoidFunction, onCancel?: VoidFunction) => {
    confirmRef.current = onConfirm
    cancelRef.current = onCancel
    setShowModal(true)
  }, [])

  return (
    <FlowModifyConfirmContext.Provider value={confirm}>
      {children}
      {showModal && (
        <NConfirmModal
          title="Cannot modify flow"
          finishText="Clone"
          visible={showModal}
          setVisible={setShowModal}
          onCancel={() => {
            if (cancelRef.current) {
              cancelRef.current()
            }
            setShowModal(false)
          }}
          onConfirm={() => {
            if (confirmRef.current) {
              confirmRef.current()
            }
            setShowCloneModal(true)
            setShowModal(false)
          }}>
          Flow can only be modified in draft mode. Please clone a new version to modify.
        </NConfirmModal>
      )}
      {showCloneModal && (
        <FlowTemplateCloneModal
          flowName={flowName}
          version={version}
          visible={showCloneModal}
          setVisible={setShowCloneModal}
          defaultValues={defaultValues}
        />
      )}
    </FlowModifyConfirmContext.Provider>
  )
}

export function useFlowModifyConfirm() {
  const c = useContext(FlowModifyConfirmContext)
  if (typeof c === 'string') {
    throw c
  }
  return c
}
