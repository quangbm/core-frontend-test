import React, { FC } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router'
import { CreateFlowItemConnectionDto } from '../../../../../shared/src/api'
import { NToast } from '../../../components/NToast'
import { APIServices, QueryKeys } from '../../../services/api'
import { useFlowModifyConfirm } from '../hooks/useFlowModifyConfirm'
import { builderActions } from '../redux/builderSlice'
import { useBuilderState } from '../redux/hooks/useBuilderState'
import { LinkModalType, NodeType } from '../utils/types'
import { LoopOutcomeSelectModal } from './LoopOutcomeSelectModal'
import { OutcomeSelectModal } from './OutcomeSelectModal'

type Props = {
  openedLinkModal: LinkModalType
}

export const LinkModal: FC<Props> = ({ openedLinkModal }) => {
  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  const dispatch = useDispatch()
  const { status } = useBuilderState()
  const warnFlowChange = useFlowModifyConfirm()

  const queryClient = useQueryClient()

  const { mutate: createConnection, isLoading: isCreating } = useMutation(
    APIServices.Flows.createFlowTemplateConnection,
    {
      onSuccess(data) {
        dispatch(
          builderActions.connectNodes({
            sourceName: data.data.srcItemName,
            targetName: data.data.trgItemName,
            sourceOutcomeName: data.data.srcOutcomeName,
            id: data.data.id,
            isFault: data.data.isFault || false,
          }),
        )
        queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateItems, { flowName, version }])
        queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateConnections, { flowName, version }])
        dispatch(builderActions.closeLinkModal())
      },
      onError: error => {
        NToast.error({
          title: 'Create connection unsuccessful',
          subtitle: error.response?.data.message,
        })
      },
    },
  )

  const handleCreateConnection = (args: { flowName: string; version: string } & CreateFlowItemConnectionDto) => {
    if (status !== 'draft') {
      warnFlowChange()
      return
    }
    createConnection(args)
  }

  const { srcNode, targetNode } = openedLinkModal

  if (srcNode.nodeType === NodeType.Decision) {
    return (
      <OutcomeSelectModal
        srcNodeName={srcNode.name}
        targetNode={targetNode}
        createConnection={handleCreateConnection}
        isCreating={isCreating}
      />
    )
  }

  if (srcNode.nodeType === NodeType.Loop) {
    return (
      <LoopOutcomeSelectModal
        srcNode={srcNode}
        targetNode={targetNode}
        createConnection={handleCreateConnection}
        isCreating={isCreating}
      />
    )
  }

  return <div>Not supported yet</div>
}
