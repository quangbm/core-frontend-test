import * as React from 'react'

export const flowBuilderPortalRef = React.createRef<HTMLDivElement>()

type FlowBuilderPortalProps = {
  children?: React.ReactNode
}

export function FlowBuilderPortal({}: FlowBuilderPortalProps) {
  return <div ref={flowBuilderPortalRef} />
}
