import * as React from 'react'
import { useDragLayer } from 'react-dnd'
import { FlowType } from '../../../../../shared/src/api/endpoints/flows'
import { Portal } from '../../../components/Portal'
import { useBuilderState } from '../redux/hooks/useBuilderState'
import { getSourceTargetXY } from '../utils/builder'
import { BIG_TRIGGER_HEIGHT, CELL_HEIGHT, TRIGGER_HEIGHT, TRIGGER_NAME, TRIGGER_POSITION } from '../utils/constants'
import { DroppableType } from '../utils/types'
import { Link } from './Link'
import { zoomGroupRef } from './PanAndZoom'

type DragLayerProps = {
  flowType?: FlowType
}

export function DragLayer({ flowType }: DragLayerProps) {
  const { isDragging, offsetX, offsetY, dragItem, dragItemType } = useDragLayer(monitor => {
    const { x: offsetX, y: offsetY } = monitor.getDifferenceFromInitialOffset() || { x: 0, y: 0 }
    return {
      isDragging: monitor.isDragging(),
      offsetX,
      offsetY,
      dragItem: monitor.getItem(),
      dragItemType: monitor.getItemType(),
    }
  })

  if (!isDragging || !dragItem) {
    return null
  }

  return (
    <Portal mountNode={zoomGroupRef}>
      {dragItemType === DroppableType.UpdateNode && (
        <LinkForDraggingNode offsetX={offsetX} offsetY={offsetY} nodeName={dragItem.name} />
      )}
      {dragItemType === DroppableType.ConnectNodes && (
        <LinkForConnectNodes offsetX={offsetX} offsetY={offsetY} fromNodeName={dragItem.name} flowType={flowType} />
      )}
    </Portal>
  )
}

const LinkForDraggingNode = React.memo(
  ({ offsetX, offsetY, nodeName }: { offsetX: number; offsetY: number; nodeName: string }) => {
    const { nodes, links } = useBuilderState()
    const renderLinks = React.useMemo(
      () => links.filter(link => link.source === nodeName || link.target === nodeName),
      [links, nodeName],
    )
    return (
      <>
        {renderLinks.map(link => {
          const { sourceX, sourceY, targetX, targetY } = getSourceTargetXY(nodes, {
            targetName: link.target,
            sourceName: link.source,
          })
          if (link.source === nodeName) {
            return (
              <Link
                key={link.id}
                sourceX={sourceX + offsetX}
                sourceY={sourceY + offsetY}
                targetX={targetX}
                targetY={targetY}
              />
            )
          }
          return (
            <Link
              key={link.id}
              sourceX={sourceX}
              sourceY={sourceY}
              targetX={targetX + offsetX}
              targetY={targetY + offsetY}
            />
          )
        })}
      </>
    )
  },
)

const LinkForConnectNodes = React.memo(
  ({
    offsetX,
    offsetY,
    fromNodeName,
    flowType,
  }: {
    offsetX: number
    offsetY: number
    fromNodeName: string
    flowType?: FlowType
  }) => {
    const { nodeConnections, nodesByName } = useBuilderState()
    const connection = nodeConnections[fromNodeName]

    const isFault = connection?.hasFaultConnection && connection?.maxConnections === 1

    const node = nodesByName[fromNodeName]

    const isFromTriggerNode = fromNodeName === TRIGGER_NAME

    const sourceX = node ? node.position.x : isFromTriggerNode ? TRIGGER_POSITION : 0 // trigger position: ;
    const sourceY = node ? node.position.y : isFromTriggerNode ? TRIGGER_POSITION : 0 // trigger position: ;

    const triggerOffset = getTriggerOffsetByType(flowType)

    const targetX = isFromTriggerNode ? sourceX + offsetX + triggerOffset.x : sourceX + offsetX
    const targetY = isFromTriggerNode ? sourceY + offsetY + triggerOffset.y : sourceY + offsetY + CELL_HEIGHT

    return (
      <Link
        isFault={isFault}
        sourceX={sourceX}
        sourceY={sourceY}
        targetX={targetX}
        targetY={targetY}
        offsetX={0}
        offsetY={0}
      />
    )
  },
)

function getTriggerOffsetByType(type?: FlowType) {
  switch (type) {
    case 'time-based':
    case 'data':
      return {
        x: 0,
        y: BIG_TRIGGER_HEIGHT / 2,
      }
    default:
      return {
        x: 0,
        y: TRIGGER_HEIGHT / 2,
      }
  }
}
