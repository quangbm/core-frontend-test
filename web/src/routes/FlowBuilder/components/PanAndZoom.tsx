import * as React from 'react'
import { useDispatch } from 'react-redux'
import styled from 'styled-components/macro'
import { builderActions } from '../redux/builderSlice'
import { useBuilderState } from '../redux/hooks/useBuilderState'

export const SvgWrapper = React.memo(function Node(props: React.SVGAttributes<SVGSVGElement>) {
  const dispatch = useDispatch()

  const ref = React.useRef<SVGSVGElement>(null)
  const mouseStateRef = React.useRef(false)

  React.useEffect(() => {
    const el = ref.current
    if (el) {
      const handleMouseDown = () => {
        el.style.cursor = 'grabbing'
        mouseStateRef.current = true
      }
      const handleMouseMove = (event: MouseEvent) => {
        if (!mouseStateRef.current) {
          return
        }
        event.preventDefault()
        // not support zoom yet
        dispatch(builderActions.panAndZoom({ translate: [event.movementX, event.movementY], scale: 1 }))
      }
      const handleMouseUp = () => {
        el.style.cursor = 'grab'
        mouseStateRef.current = false
      }
      el.addEventListener('mousedown', handleMouseDown)
      el.addEventListener('mousemove', handleMouseMove)
      el.addEventListener('mouseup', handleMouseUp)
      return () => {
        el.removeEventListener('mousedown', handleMouseDown)
        el.removeEventListener('mousemove', handleMouseMove)
        el.removeEventListener('mouseup', handleMouseUp)
      }
    }
  }, [dispatch])
  return (
    <svg
      ref={ref}
      {...props}
      style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, cursor: 'grab', userSelect: 'auto' }}
    />
  )
})

SvgWrapper.displayName = 'PanAndZoomDragLayer'

export const PanAndZoomWrapper = React.memo(function Node({ children }: { children: React.ReactNode }) {
  const { configs } = useBuilderState()
  const [translateX, translateY] = configs.translate
  const scale = configs.scale
  return (
    <div
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        transform: `translate(${translateX}px, ${translateY}px) scale(${scale})`,
      }}>
      {children}
    </div>
  )
})

PanAndZoomWrapper.displayName = 'PanAndZoom'

const G = styled('g')`
  :focus {
    outline: 0;
  }
`

export const zoomGroupRef = React.createRef<SVGGElement>()

export const PanAndZoomGroup = React.memo(function Node({ children }: { children: React.ReactNode }) {
  const { configs } = useBuilderState()
  const [translateX, translateY] = configs.translate
  const scale = configs.scale
  return (
    <G transform={`translate(${translateX} ${translateY}) scale(${scale})`} ref={zoomGroupRef}>
      {children}
    </G>
  )
})

PanAndZoomGroup.displayName = 'PanAndZoomGroup'
