import React, { FC } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components/macro'
import { color, spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NCodeEditor } from '../../../components/NCodeEditor'
import { NModal } from '../../../components/NModal/NModal'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { APIServices, QueryKeys } from '../../../services/api'

const BodyWrapper = styled(NModal.Body)`
  position: relative;
  margin: ${spacing('xl')};
  display: flex;
  flex-direction: column;
`
const LoadingContainer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  min-height: 60px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  background-color: ${color('Neutral700')}80;
`

type FlowDebugModalProps = {
  visible: boolean
  setVisible: (newState: boolean) => void
}

export const FlowDebugModal: FC<FlowDebugModalProps> = ({ visible, setVisible }) => {
  const theme = useTheme()
  const { flowName } = useParams<{ flowName: string }>()
  const { data, isFetching, refetch } = useQuery(
    [QueryKeys.Flows.debugFlow, { flowName }],
    APIServices.Flows.debugFlow,
    {
      cacheTime: 0,
    },
  )
  return (
    <NModal visible={visible} setVisible={setVisible} size="x-large">
      <NModal.Header title="Flow debug logs" />
      <NModal.Body>
        <BodyWrapper>
          <NCodeEditor
            language="json"
            value={JSON.stringify(data?.data, null, 2) || 'No recent data'}
            disabled
            style={{ maxWidth: '100%', maxHeight: '60vh' }}
          />
          {isFetching && (
            <LoadingContainer>
              <NSpinner size={50} strokeWidth={6} color={color('white')({ theme })} />
            </LoadingContainer>
          )}
        </BodyWrapper>
      </NModal.Body>
      <NModal.Footer>
        <NButton
          type="ghost"
          size="small"
          onClick={() => {
            setVisible(false)
          }}>
          Close
        </NButton>
        <NButton
          type="primary"
          size="small"
          onClick={() => {
            refetch()
          }}>
          Reset
        </NButton>
      </NModal.Footer>
    </NModal>
  )
}
