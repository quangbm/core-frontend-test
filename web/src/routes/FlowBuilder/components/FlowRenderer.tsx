import { omit } from 'lodash'
import * as React from 'react'
import { useDrop } from 'react-dnd'
import { useMutation, useQueryClient } from 'react-query'
import { useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components/macro'
import { FlowItemConnectionResponse } from '../../../../../shared/src/api'
import { BaseCreateFlowItemDto, BaseUpdateFlowItemDto, FlowType } from '../../../../../shared/src/api/endpoints/flows'
import { color, getAlpha } from '../../../components/GlobalStyle'
import { NConfirmModal } from '../../../components/NModal/NConfirmModal'
import { NModal } from '../../../components/NModal/NModal'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { NToast } from '../../../components/NToast'
import { APIServices, QueryKeys } from '../../../services/api'
import { FLOW_ITEM_BUILDER } from '../FlowBuilderItem/FlowItem'
import { LinkModal } from '../FlowBuilderOutcomeSelect/LinkModal'
import { FlowTriggerNode } from '../FlowBuilderTrigger/FlowTriggerNode'
import { useFlowModifyConfirm } from '../hooks/useFlowModifyConfirm'
import { builderActions } from '../redux/builderSlice'
import { useBuilderState } from '../redux/hooks/useBuilderState'
import { getSourceTargetXY, snapToGrid } from '../utils/builder'
import { TRIGGER_NAME, TRIGGER_POSITION } from '../utils/constants'
import { fromFlowItemToFlowNode } from '../utils/functions'
import { CommonWidgetDataType, DroppableType, NodeType, WidgetItemType, XY } from '../utils/types'
import { DragLayer } from './DragLayer'
import { FlowBuilderPortal } from './FlowBuilderPortal'
import { Link } from './Link'
import { Node } from './Node'
import { PanAndZoomGroup, PanAndZoomWrapper, SvgWrapper } from './PanAndZoom'
import { SvgDefs } from './SvgDefs'

const LoadingWrapper = styled('div')`
  position: absolute;
  top: 0;
  display: flex;
  justify-content: center;
  align-items: center;

  width: 100%;
  height: 100%;
  background-color: ${color('Neutral700')}${getAlpha(50)};

  cursor: progress;
`

const Wrapper = styled('div')`
  position: relative;
  overflow: hidden;
`

type Props = { flowType?: FlowType; isLoading?: boolean }

export function FlowRenderer({ flowType, isLoading }: Props) {
  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  const theme = useTheme()
  const {
    nodes,
    links,
    isDragging,
    draggingNodeName,
    selectedNodeId,
    openedNodeModal,
    openedLinkModal,
    selectedLinkId,
    configs,
    status,
  } = useBuilderState()
  const dispatch = useDispatch()
  const queryClient = useQueryClient()

  const wrapper = React.useRef<HTMLDivElement>(null)

  const [translateX, translateY] = configs.translate

  const [{ width, height, offsetX, offsetY }, setSize] = React.useState({ width: 0, height: 0, offsetX: 0, offsetY: 0 })
  const [confirmModalVisible, setConfirmModalVisible] = React.useState(false)
  const warnFlowChange = useFlowModifyConfirm()

  // create flow item mutation
  const { mutate: deleteConnection } = useMutation(APIServices.Flows.deleteFlowTemplateConnection, {
    async onMutate(args) {
      dispatch(builderActions.removeLink(args.id))

      await queryClient.cancelQueries([QueryKeys.Flows.getFlowTemplateConnections, { flowName }])
      const previous = queryClient.getQueryData<FlowItemConnectionResponse[]>([
        QueryKeys.Flows.getFlowTemplateConnections,
        { flowName },
      ])
      return { previous }
    },
    onError: (error, _, context: any) => {
      NToast.error({ title: 'Delete unsuccessful', subtitle: error.response?.data.message })
      queryClient.setQueryData([QueryKeys.Flows.getFlowTemplateConnections, { flowName }], context.previous)
    },
    onSettled() {
      queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateItems, { flowName }])
      queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateConnections, { flowName }])
    },
  })

  // for delete link
  const handleUserKeyPress = React.useCallback(
    event => {
      const { key } = event
      if (key === 'Delete' || key === 'Backspace') {
        if (selectedLinkId) {
          if (status !== 'draft') {
            warnFlowChange()
            return
          }
          deleteConnection({ id: selectedLinkId, flowName, version })
        }
      }
    },
    [selectedLinkId, deleteConnection, flowName, status],
  )

  React.useEffect(() => {
    window.addEventListener('keydown', handleUserKeyPress)

    return () => {
      window.removeEventListener('keydown', handleUserKeyPress)
    }
  }, [handleUserKeyPress])

  React.useLayoutEffect(() => {
    if (wrapper.current) {
      const size = wrapper.current.getBoundingClientRect()
      setSize({ width: size.width, height: size.height, offsetX: size.left, offsetY: size.top })
    }
  }, [])

  const renderLinks = React.useMemo(() => {
    if (isDragging && draggingNodeName) {
      return links.filter(link => link.source !== draggingNodeName && link.target !== draggingNodeName)
    }
    return links
  }, [links, isDragging, draggingNodeName])

  // create flow item mutation
  const { mutate: createFlowItem, isLoading: isFlowItemCreating } = useMutation(
    APIServices.Flows.createFlowTemplateItem,
    {
      onSuccess: ({ data }) => {
        const flowNode = fromFlowItemToFlowNode(data)
        if (!flowNode) {
          NToast.error({
            title: 'Create Item unsuccessful',
            subtitle: `Item type: "${data.type}" is not supported yet`,
          })
          return
        }
        dispatch(builderActions.addNode(flowNode))
        dispatch(builderActions.closeModal())
        queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateItems, { flowName, version }])
        queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateVariables, { flowName, version }])
      },
      onError: error => {
        NToast.error({
          title: 'Create Item unsuccessful',
          subtitle: error.response?.data.message,
        })
      },
    },
  )

  // create flow item mutation
  const { mutate: updateFlowItem, isLoading: isFlowItemUpdating } = useMutation(
    APIServices.Flows.updateFlowTemplateItem,
    {
      onSuccess: ({ data }) => {
        const flowNode = fromFlowItemToFlowNode(data)
        if (!flowNode) {
          NToast.error({
            title: 'Update Item unsuccessful',
            subtitle: `Item type: "${data.type}" is not supported yet`,
          })
          return
        }
        dispatch(builderActions.setNodeData({ nodeId: flowNode.id, data: flowNode.data }))
        dispatch(builderActions.closeModal())
        queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateItems, { flowName, version }])
        queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateConnections, { flowName, version }])
        queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateVariables, { flowName, version }])
      },
      onError: error => {
        NToast.error({
          title: 'Update Item unsuccessful',
          subtitle: error.response?.data.message,
        })
      },
    },
  )

  // submit handler
  const handleSubmitNodeData = (params: {
    nodeName?: string
    position: XY
    type: NodeType
    data: CommonWidgetDataType
  }) => {
    const { nodeName, data } = params

    if (nodeName) {
      updateFlowItem({
        flowItemName: nodeName,
        flowName,
        version,
        data: omit(data, ['type', 'name']) as BaseUpdateFlowItemDto,
      })
      return
    }

    createFlowItem({
      flowName,
      version,
      data: data as BaseCreateFlowItemDto,
    })
    return
  }
  //

  const ModalContent = openedNodeModal ? FLOW_ITEM_BUILDER[openedNodeModal.type] : undefined

  const [, drop] = useDrop({
    accept: [DroppableType.AddNode, DroppableType.UpdateNode],
    drop(item: { id?: string; widget?: WidgetItemType }, monitor) {
      if (status !== 'draft') {
        warnFlowChange()
        return
      }
      const clientOffset = monitor.getClientOffset() || { x: offsetX, y: offsetY }
      const position = snapToGrid({
        x: clientOffset.x - offsetX - translateX,
        y: clientOffset.y - offsetY - translateY,
      })
      const itemType = monitor.getItemType()
      if (itemType === DroppableType.UpdateNode && item.id) {
        const node = nodes.find(i => i.id === item.id)

        dispatch(
          builderActions.setNodePosition({
            nodeId: item.id,
            position,
          }),
        )

        if (node) {
          const updateArgs = {
            flowItemName: node.name,
            version,
            flowName,
            data: {
              type: node.type,
              displayName: node.data.displayName,
              nodeAttributes: {
                description: node.data.description,
                position: position,
              },
            } as BaseUpdateFlowItemDto,
          }

          if (!updateArgs) {
            console.error(`Flow item type: "${node.type}" is not supported yet`)
            return
          }

          updateFlowItem(updateArgs, {
            onError() {
              // mode node back to previous position if error
              dispatch(
                builderActions.setNodePosition({
                  nodeId: node.id,
                  position: node.position,
                }),
              )
            },
          })
        }
        return
      }
      if (itemType === DroppableType.AddNode && item.widget) {
        dispatch(builderActions.openModal({ position, type: item.widget.type, data: item.widget.data }))
      }
    },
  })

  drop(wrapper)

  return (
    <Wrapper
      ref={wrapper}
      onClick={() => {
        dispatch(builderActions.deselectNode())
        dispatch(builderActions.unselectLink())
      }}>
      <DragLayer flowType={flowType} />
      <SvgWrapper width={width} height={height} viewBox={`0 0 ${width} ${height}`}>
        <SvgDefs />
        <rect x="0" y="0" width="100%" height="100%" fill="url(#background-pattern)" />
        <PanAndZoomGroup>
          {renderLinks.map(link => {
            let sourceTargetXY = { sourceX: 0, sourceY: 0, targetX: 0, targetY: 0 }
            if (link.target === TRIGGER_NAME) {
              const sourceXY = getSourceTargetXY(nodes, { sourceName: link.source })
              sourceTargetXY = { targetX: TRIGGER_POSITION, targetY: TRIGGER_POSITION, ...sourceXY }
            } else if (link.source === TRIGGER_NAME) {
              const targetXY = getSourceTargetXY(nodes, { targetName: link.target })
              sourceTargetXY = { sourceX: TRIGGER_POSITION, sourceY: TRIGGER_POSITION, ...targetXY }
            } else {
              sourceTargetXY = getSourceTargetXY(nodes, { targetName: link.target, sourceName: link.source })
            }

            return (
              <Link
                key={link.id}
                {...sourceTargetXY}
                linkId={link.id}
                outcomeName={link.outcomeName}
                srcName={link.source}
                isFault={link.isFault}
              />
            )
          })}
          {selectedLinkId && <use href={`#${selectedLinkId}`} />}
        </PanAndZoomGroup>
      </SvgWrapper>
      <PanAndZoomWrapper>
        {flowType && <FlowTriggerNode flowType={flowType} />}
        {nodes.map(node => {
          return <Node key={node.id} node={node} selectedNodeId={selectedNodeId} />
        })}
      </PanAndZoomWrapper>
      {/* ///////////////////////// */}
      {openedLinkModal && (
        <NModal
          top="middle"
          visible
          setVisible={() => {
            dispatch(builderActions.closeLinkModal())
          }}>
          <LinkModal openedLinkModal={openedLinkModal} />
        </NModal>
      )}
      {/* ///////////////////////// */}
      {openedNodeModal && (
        <NModal
          visible
          setVisible={() => {
            setConfirmModalVisible(true)
          }}
          size="x-large">
          {ModalContent && (
            <ModalContent
              isSubmitting={isFlowItemCreating || isFlowItemUpdating}
              nodeModal={openedNodeModal}
              onCancel={() => {
                dispatch(builderActions.closeModal())
              }}
              onSubmit={data => {
                if (status !== 'draft') {
                  warnFlowChange()
                  return
                }
                handleSubmitNodeData({
                  ...openedNodeModal,
                  nodeName: openedNodeModal.data?.name,
                  data,
                })
              }}
            />
          )}
        </NModal>
      )}
      {/* ///////////////////////// */}
      {isLoading && (
        <LoadingWrapper>
          <NSpinner size={50} strokeWidth={5} color={color('white')({ theme })} />
        </LoadingWrapper>
      )}
      {/* Confirm close modal */}
      {confirmModalVisible && (
        <NConfirmModal
          visible={confirmModalVisible}
          setVisible={setConfirmModalVisible}
          onCancel={() => {
            setConfirmModalVisible(false)
          }}
          onConfirm={() => {
            setConfirmModalVisible(false)
            dispatch(builderActions.closeModal())
          }}>
          Do you want to close the modal?
        </NConfirmModal>
      )}

      <FlowBuilderPortal />
    </Wrapper>
  )
}
