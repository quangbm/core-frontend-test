import * as React from 'react'
import { Provider } from 'react-redux'
import { FlowItemConnectionResponse, FlowTemplateWithDataResponse } from '../../../../../shared/src/api'
import { FlowItem } from '../../../../../shared/src/api/endpoints/flows'
import { builderActions, NodeConnections } from '../redux/builderSlice'
import { store } from '../redux/store'
import { TRIGGER_CONNECTION, TRIGGER_NAME } from '../utils/constants'
import { fromFlowItemToFlowNode } from '../utils/functions'
import { FlowNode, LinkType } from '../utils/types'

type FlowBuilderProviderProps = {
  children: React.ReactNode
  nodes: FlowItem[]
  links: FlowItemConnectionResponse[]
  flowData?: FlowTemplateWithDataResponse
}

export function FlowBuilderProvider({ children, nodes, links, flowData }: FlowBuilderProviderProps) {
  React.useEffect(() => {
    // transform FIs to Nodes
    const { transformNodes, nodesByName } = nodes.reduce<{
      transformNodes: FlowNode[]
      nodesByName: Record<string, FlowNode>
    }>(
      (prev, flowItem) => {
        const transformed = fromFlowItemToFlowNode(flowItem)
        if (!transformed) {
          console.error(`Flow item type: "${flowItem.type}" is not supported yet`)
          return prev
        }

        return {
          transformNodes: [...prev.transformNodes, transformed],
          nodesByName: {
            ...prev.nodesByName,
            [flowItem.name]: transformed,
          },
        }
      },
      { transformNodes: [], nodesByName: {} },
    )

    store.dispatch(builderActions.setNodes({ nodes: transformNodes, nodesByName }))

    // transform FI Connections to Links
    const transformedLinks = links.map(connect => ({
      id: connect.id,
      target: connect.trgItemName,
      source: connect.srcItemName,
      type: LinkType.Success,
      data: {},
      outcomeName: connect.srcOutcomeName,
      isFault: connect.isFault || false,
    }))

    store.dispatch(builderActions.setLinks({ links: transformedLinks }))

    // calculate remaining connections can drag
    if (nodes && links) {
      // extract maxConnections and hasFaultConnection out of the nodes
      const flowItems = nodes.reduce<NodeConnections>(
        (a, { maxConnections, hasFaultConnection, name }) => {
          return {
            ...a,
            [name]: {
              maxConnections: hasFaultConnection ? maxConnections + 1 : maxConnections,
              hasFaultConnection,
            },
          }
        },
        {
          [TRIGGER_NAME]: TRIGGER_CONNECTION,
        },
      )

      // remaining connections
      const nodeConnections = links.reduce((a, { srcItemName, isFault }) => {
        const node = a[srcItemName]
        if (node) {
          return {
            ...a,
            [srcItemName]: {
              // If fault connection existed, not allow to drag fault connection
              hasFaultConnection: isFault ? false : node.hasFaultConnection,
              maxConnections: node.maxConnections - 1,
            },
          }
        }
        return a
      }, flowItems)

      store.dispatch(builderActions.setNodeConnections(nodeConnections))
    }
  }, [nodes, links])

  React.useEffect(() => {
    if (flowData) {
      store.dispatch(builderActions.setFlowStatus({ status: flowData.status }))
    }
  }, [flowData])

  return <Provider store={store}>{children}</Provider>
}
