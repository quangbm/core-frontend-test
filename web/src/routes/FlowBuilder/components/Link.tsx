import * as React from 'react'
import { useDispatch } from 'react-redux'
import styled, { useTheme } from 'styled-components/macro'
import { DecisionFlowItemResponse } from '../../../../../shared/src/api'
import { color, spacing } from '../../../components/GlobalStyle'
import { typography } from '../../../components/NTypography'
import { Portal } from '../../../components/Portal'
import { builderActions } from '../redux/builderSlice'
import { useBuilderState } from '../redux/hooks/useBuilderState'
import { CELL_HEIGHT, CELL_WIDTH } from '../utils/constants'
import { getEdgeLine } from '../utils/link'
import { NodeType, SourceXY, TargetXY } from '../utils/types'
import { flowBuilderPortalRef } from './FlowBuilderPortal'

type PositionProps = { top: number; left: number }

const OutcomeDisplayName = styled('div').attrs(({ top, left }: PositionProps) => ({
  style: {
    transform: `translate(calc(${left}px - 50%), calc(${top}px - 50%))`,
  },
}))<PositionProps>`
  position: absolute;
  top: 0;
  left: 0;
  padding: ${spacing('xxs')} ${spacing('xs')};
  border-radius: 4px;
  background: rgba(237, 238, 240, 0.6);
  cursor: pointer;
  ${typography('x-small-ui-text')};
`

type LinkProps = {
  linkId?: string
  outcomeName?: string
  srcName?: string
  offsetX?: number
  offsetY?: number
  isFault?: boolean
} & SourceXY &
  TargetXY

export const Link = React.memo(function Link({
  sourceX,
  sourceY,
  targetX,
  targetY,
  linkId,
  outcomeName,
  srcName,
  offsetX,
  offsetY,
  isFault = false,
}: LinkProps) {
  const theme = useTheme()
  const dispatch = useDispatch()
  const { selectedLinkId, nodesByName, configs } = useBuilderState()

  const [translateX, translateY] = configs.translate

  const node = srcName && nodesByName[srcName]

  const outcomeDisplayName = React.useMemo<string | undefined>(() => {
    if (!node) {
      return
    }

    if (node.type === NodeType.Decision) {
      const decisionAttribute = (node.data as DecisionFlowItemResponse)?.attributes
      return (
        (outcomeName && decisionAttribute.outcomes[outcomeName]?.displayName) || decisionAttribute.defaultOutcomeLabel
      )
    }

    if (node.type === NodeType.Loop) {
      if (outcomeName === 'Each') {
        return 'For each'
      }
      if (!outcomeName) {
        return 'After all'
      }
    }

    return
  }, [node, outcomeName])

  const d = React.useMemo(() => {
    const oX = offsetX ?? CELL_WIDTH + 8
    const oY = offsetY ?? CELL_HEIGHT + 8
    const dx = Math.abs(targetX - sourceX)
    const dy = Math.abs(targetY - sourceY)
    return getEdgeLine(
      { x: sourceX, y: sourceY },
      { x: targetX, y: targetY },
      { x: dx > oX * 2 ? oX : 0, y: dy > oY * 2 ? oY : 0 },
    )
  }, [sourceX, sourceY, targetX, targetY, offsetX, offsetY])

  const isSelected = selectedLinkId && selectedLinkId === linkId

  const stroke = isSelected
    ? isFault
      ? color('Red700')({ theme })
      : color('Primary700')({ theme })
    : color('Neutral300')({ theme })

  const strokeDasharray = isFault ? '4' : ''

  return (
    <React.Fragment>
      <g
        onClick={e => {
          e.stopPropagation()
          linkId && dispatch(builderActions.selectLink(linkId))
        }}
        id={linkId}>
        <path
          stroke={stroke}
          strokeWidth="2"
          fill="none"
          d={d}
          markerStart={`url(#dot${isSelected ? (isFault ? '-fault' : '-selected') : ''})`}
          markerEnd={`url(#arrow${isSelected ? (isFault ? '-fault' : '-selected') : ''})`}
          strokeDasharray={strokeDasharray}
        />
        <path cursor="pointer" strokeWidth="20" fill="none" d={d} stroke={stroke} strokeOpacity="0.0" />
      </g>
      {(outcomeDisplayName || isFault) && (
        <Portal mountNode={flowBuilderPortalRef}>
          <OutcomeDisplayName
            top={sourceY + (targetY - sourceY) / 2 + translateY}
            left={sourceX + (targetX - sourceX) / 2 + translateX}
            onClick={e => {
              e.stopPropagation()
              linkId && dispatch(builderActions.selectLink(linkId))
            }}>
            {outcomeDisplayName || 'Fault'}
          </OutcomeDisplayName>
        </Portal>
      )}
    </React.Fragment>
  )
})
