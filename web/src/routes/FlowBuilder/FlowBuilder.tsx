import * as React from 'react'
import { useState } from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useHistory, useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { DASHBOARD_ROUTE } from '../../common/constants'
import { BuilderHeader } from '../../components/Builder/BuilderHeader'
import { color } from '../../components/GlobalStyle'
import { SidebarIcons } from '../../components/Icons'
import { NBreadcrumbs } from '../../components/NBreadcrumbs/NBreadcrumbs'
import { NButton } from '../../components/NButton/NButton'
import { NDivider } from '../../components/NDivider'
import { NConfirmModal } from '../../components/NModal/NConfirmModal'
import { NToast } from '../../components/NToast'
import { APIServices, QueryKeys } from '../../services/api'
import { FlowTemplateCloneModal } from '../Dashboard/routes/Flow/components/FlowTemplateCloneModal'
import { FlowBuilderProvider } from './components/FlowBuilderProvider'
import { FlowDebugModal } from './components/FlowDebugModal'
import { FlowRenderer } from './components/FlowRenderer'
import { Sidebar } from './components/Sidebar'
import { FlowBuilderVariable } from './FlowBuilderVariable/FlowBuilderVariable'
import { FlowModifyConfirmProvider } from './hooks/useFlowModifyConfirm'

const Wrapper = styled('div')`
  display: grid;
  grid-template-rows: auto 1fr;
  grid-template-columns: 251px 1fr;
  grid-template-areas:
    'header header'
    'sidebar droppable-zone';
  width: 100vw;
  height: 100vh;
  background: ${color('Neutral200')};
  overflow: hidden;
`

const HeaderBtn = styled(NButton)`
  min-width: 120px;
`

export function FlowBuilder() {
  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  const queryClient = useQueryClient()
  const history = useHistory()
  const [showCloneModal, setShowCloneModal] = useState(false)
  const [showConfirmActivate, setShowConfirmActivate] = useState(false)

  // get flow data
  const { data: flowData, isFetching: isLoadingFlowData } = useQuery(
    [QueryKeys.Flows.getFlowTemplateVersion, { flowName, version }],
    APIServices.Flows.getFlowTemplateVersion,
    {
      onError: () => {
        history.push(`${DASHBOARD_ROUTE}/flow`)
      },
    },
  )

  // get flow items
  const { data: flowItems, isLoading: isLoadingFlowItems } = useQuery(
    [QueryKeys.Flows.getFlowTemplateItems, { flowName, version }],
    APIServices.Flows.getFlowTemplateItems,
    { keepPreviousData: true },
  )
  // get flow connection
  const { data: flowConnections, isLoading: isLoadingFlowConnections } = useQuery(
    [QueryKeys.Flows.getFlowTemplateConnections, { flowName, version }],
    APIServices.Flows.getFlowTemplateConnections,
    {
      keepPreviousData: true,
    },
  )

  // activate flow
  const { mutate: activateFlow, isLoading: isActivating } = useMutation(APIServices.Flows.activateFlowTemplate, {
    onSuccess: () => {
      queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateVersion, { flowName }])
    },
    onError: error => {
      NToast.error({
        title: 'Activate unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })
  // deactivate flow
  const { mutate: deactivateFlow, isLoading: isDeActivating } = useMutation(APIServices.Flows.deactivateFlowTemplate, {
    onSuccess: () => {
      queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateVersion, { flowName }])
    },
    onError: error => {
      NToast.error({
        title: 'Deactivate unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const [showVariableManager, setShowVariableManager] = React.useState(false)
  const [showDebug, setShowDebug] = React.useState(false)

  const handleShowVariableManager = () => {
    setShowVariableManager(true)
  }

  const updateFlowStatus = () => {
    if (flowData) {
      if (flowData.data.status === 'active') {
        deactivateFlow({ flowName, version })
        return
      }
      setShowConfirmActivate(true)
    }
  }

  return (
    <Wrapper>
      <BuilderHeader
        onBack={() => history.push(`${DASHBOARD_ROUTE}/flow`)}
        title="Flow Builder"
        icon={<SidebarIcons.AutomationOutlined />}
        style={{ gridArea: 'header' }}
        rightElement={
          <React.Fragment>
            <HeaderBtn size="small" type="outline" loading={isLoadingFlowData} onClick={handleShowVariableManager}>
              Variable Manager
            </HeaderBtn>
            <NDivider vertical size="xs" />
            <HeaderBtn
              disabled={isLoadingFlowData}
              size="small"
              type="outline"
              onClick={() => {
                setShowDebug(true)
              }}>
              Debug
            </HeaderBtn>
            <NDivider vertical size="xs" />
            <HeaderBtn
              disabled={isLoadingFlowData}
              size="small"
              type="outline"
              onClick={() => {
                setShowCloneModal(true)
              }}>
              Clone
            </HeaderBtn>
            <NDivider vertical size="xs" />
            <HeaderBtn
              size="small"
              type={flowData?.data.status === 'active' ? 'red' : 'primary'}
              loading={isLoadingFlowData || isActivating || isDeActivating}
              onClick={updateFlowStatus}>
              {flowData?.data.status === 'active' ? 'Deactivate' : 'Activate'}
            </HeaderBtn>
          </React.Fragment>
        }>
        <NBreadcrumbs
          breadcrumbs={[
            { label: 'Flow', path: `${DASHBOARD_ROUTE}/flow` },
            {
              label: flowData?.data
                ? `${flowData.data.displayName} ${flowData.data.status === 'draft' ? '(draft)' : ''}`
                : '',
            },
          ]}
        />
      </BuilderHeader>
      <FlowBuilderProvider nodes={flowItems?.data || []} links={flowConnections?.data || []} flowData={flowData?.data}>
        <FlowModifyConfirmProvider
          defaultValues={{ displayName: flowData?.data.displayName, description: flowData?.data.description }}>
          <Sidebar flowType={flowData?.data.type} />
          <FlowRenderer flowType={flowData?.data.type} isLoading={isLoadingFlowItems || isLoadingFlowConnections} />
          {showVariableManager && (
            <FlowBuilderVariable visible={showVariableManager} setVisible={setShowVariableManager} />
          )}
        </FlowModifyConfirmProvider>
      </FlowBuilderProvider>
      {showDebug && <FlowDebugModal visible={showDebug} setVisible={setShowDebug} />}
      {showCloneModal && (
        <FlowTemplateCloneModal
          flowName={flowName}
          version={version}
          visible={showCloneModal}
          setVisible={setShowCloneModal}
          defaultValues={{
            displayName: flowData?.data.displayName,
            description: flowData?.data.description,
          }}
        />
      )}
      {showConfirmActivate && (
        <NConfirmModal
          title="Confirm activate flow"
          visible={showConfirmActivate}
          setVisible={setShowConfirmActivate}
          onCancel={() => {
            setShowConfirmActivate(false)
          }}
          onConfirm={() => {
            activateFlow({ flowName, version })
            setShowConfirmActivate(false)
          }}>
          Current active version will be override.{' '}
          {flowData?.data.status === 'draft' ? 'Flow version cannot be modified after activation' : ''}
        </NConfirmModal>
      )}
    </Wrapper>
  )
}
