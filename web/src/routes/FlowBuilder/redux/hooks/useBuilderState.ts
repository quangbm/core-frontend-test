import { useSelector } from 'react-redux'
import { BuilderState } from '../builderSlice'

export function useBuilderState() {
  return useSelector((state: BuilderState) => state)
}
