import React, { Fragment } from 'react'
import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components/macro'
import { ConditionType, DataFlowCondition, EventFlowResponse, UpdateEventFlowDto } from '../../../../../shared/src/api'
import { color, spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'
import { NDivider } from '../../../components/NDivider'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { NModal } from '../../../components/NModal/NModal'
import { SelectOption } from '../../../components/NSelect/model'
import { NSingleSelect } from '../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../components/NTextInput/NTextInput'
import { NToast } from '../../../components/NToast'
import { typography } from '../../../components/NTypography'
import { useSearchText } from '../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../services/api'
import { AddBtn } from '../../Dashboard/routes/Object/components/AddBtn'
import { LogicOperator } from '../../Dashboard/routes/Object/components/LogicOperator'
import { DeleteBtn } from '../FlowBuilderItem/FlowItemData/components/CommonComponents'
import { ContentValuePicker } from '../FlowBuilderVariable/ContentValuePicker'
import { useFlowModifyConfirm } from '../hooks/useFlowModifyConfirm'
import { useBuilderState } from '../redux/hooks/useBuilderState'

const ScrollWrapper = styled.div`
  max-height: 70vh;
  overflow-y: auto;
`

const Wrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`
const TextHeader = styled.div`
  color: ${color('Neutral500')};
`

const IndexTxt = styled.p`
  ${typography('small-bold-text')}
  min-width: 15px;
`

const AddBtnWrapper = styled(AddBtn)`
  margin-left: ${spacing('sm')};
`

const CONDITIONS = [
  { label: 'NONE', value: 'none' },
  { label: 'All conditions are met (AND)', value: 'and' },
  { label: 'Any condition is met (OR)', value: 'or' },
  { label: 'Custom condition logic is met', value: 'custom' },
]

const SIGNS = [
  '===',
  '!==',
  '<=',
  '>=',
  '<',
  ' >',
  'isNull',
  'isNotNull',
  'startsWith',
  'endsWith',
  'contains',
  'notContains',
  'wasSet',
]

type EventFormType = {
  eventName: string
  conditionLogic: DataFlowCondition['conditionLogic']
  customConditionLogic: string
  conditionClauses: ConditionType[]
}

type Props = {
  showModal: boolean
  setShowModal: (val: boolean) => void
  flowData?: EventFlowResponse
}
export const EventSelectModal: React.FC<Props> = ({ showModal, setShowModal, flowData }) => {
  const { watch, control, handleSubmit, register } = useForm<EventFormType>({
    shouldUnregister: false,
    defaultValues: {
      eventName: undefined,
      conditionLogic: flowData?.attributes?.conditions?.conditionLogic || 'none',
      customConditionLogic: flowData?.attributes?.conditions?.customConditionLogic,
      conditionClauses: flowData?.attributes?.conditions?.conditionClauses || [
        {
          field: '',
          sign: '===',
        },
      ],
    },
  })
  const { fields, append, remove } = useFieldArray({
    control,
    name: 'conditionClauses',
  })

  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  const theme = useTheme()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const queryClient = useQueryClient()
  const watchEventName = watch('eventName', flowData?.attributes?.eventName)
  const watchConditionLogic = watch('conditionLogic', flowData?.attributes?.conditions?.conditionLogic)
  const { status } = useBuilderState()
  const warnFlowChange = useFlowModifyConfirm()

  // get all objects
  const { data: allEvents, isLoading: isLoadingEvents } = useQuery(
    [QueryKeys.Events.getEventList, { searchText }],
    APIServices.Events.getEventList,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          return {
            value: option.name,
            label: option.displayName,
          }
        }) as SelectOption[]
      },
    },
  )
  //
  const { mutate: updateFlow, isLoading: isUpdating } = useMutation(APIServices.Flows.updateFlowTemplate, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateVersion, { flowName, version }])
      setShowModal(false)
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const { data: fieldData, isLoading: isLoadingFields } = useQuery(
    [QueryKeys.Builder.getObjectSchema, { objName: watchEventName! }],

    APIServices.Builder.getObjectSchema,
    {
      enabled: !!watchEventName,
    },
  )

  const onSubmit = ({ eventName, conditionClauses, conditionLogic, customConditionLogic }: EventFormType) => {
    const updateData = {
      attributes: {
        eventName,
        conditions: {
          conditionLogic,
          conditionClauses:
            conditionLogic === 'none'
              ? []
              : conditionClauses.map((clause, index) => ({ ...clause, conditionNumber: index + 1 })),
          customConditionLogic,
        },
      },
    } as UpdateEventFlowDto
    if (status !== 'draft') {
      warnFlowChange()
      return
    }
    updateFlow({
      flowName,
      version,
      data: updateData,
    })
  }

  return (
    <NModal size="large" setVisible={setShowModal} visible={showModal}>
      <NModal.Header onClose={() => setShowModal(false)} title="Choose Event" />
      <NModal.Body>
        <ScrollWrapper>
          <form>
            <Wrapper>
              <TextHeader>
                The flow subscribes to the specified platform event. When a platform event message is received, the flow
                is triggered to run.
              </TextHeader>
              <NDivider size="md" />
              <NRow>
                <NColumn flex={1}>
                  <Controller
                    defaultValue={flowData?.attributes?.eventName || ''}
                    control={control}
                    name="eventName"
                    render={({ field }) => (
                      <NSingleSelect
                        isLoading={isLoadingEvents}
                        isSearchable
                        searchValue={searchValue}
                        onSearchValueChange={handleSearchChange}
                        options={allEvents || []}
                        placeholder="Search events"
                        label="Platform Event"
                        value={field.value}
                        onValueChange={value => {
                          field.onChange(value)
                        }}
                      />
                    )}
                  />
                </NColumn>
              </NRow>
            </Wrapper>

            {watchEventName && (
              <Wrapper>
                <NDivider size={1} lineSize={1} lineColor={color('Neutral300')({ theme })} />
                <NDivider size="md" />
                <NRow>
                  <NColumn flex={1}>
                    <Controller
                      name="conditionLogic"
                      control={control}
                      render={({ field }) => (
                        <NSingleSelect
                          options={CONDITIONS}
                          placeholder="Select Condition"
                          value={field.value}
                          label="Condition Requirements to Execute Outcome"
                          onValueChange={field.onChange}
                          className="conditionSelect"
                        />
                      )}
                    />
                  </NColumn>
                </NRow>
                {watchConditionLogic !== 'none' && (
                  <>
                    <NDivider size="md" />
                    {watchConditionLogic === 'custom' && (
                      <NTextInput
                        placeholder="Enter custom logic"
                        label="Condition logic"
                        {...register('customConditionLogic')}
                      />
                    )}
                    <NDivider size="md" />
                    {fields.map((field, fieldIndex) => (
                      <Fragment key={field.id}>
                        <NRow align="center">
                          {watchConditionLogic === 'custom' && (
                            <>
                              <IndexTxt>{fieldIndex + 1}</IndexTxt>
                              <NDivider size="md" vertical />
                            </>
                          )}
                          <NColumn>
                            <Controller
                              name={`conditionClauses.${fieldIndex}.field`}
                              defaultValue={field.field || ''}
                              control={control}
                              render={({ field }) => {
                                return (
                                  <NSingleSelect
                                    fullWidth
                                    isLoading={isLoadingFields}
                                    options={
                                      fieldData?.data.fields.map(field => ({
                                        value: field.name,
                                        label: field.displayName,
                                      })) || []
                                    }
                                    placeholder="Select field"
                                    value={field.value}
                                    onValueChange={field.onChange}
                                  />
                                )
                              }}
                            />
                          </NColumn>

                          <NDivider size="xs" vertical />
                          <Controller
                            name={`conditionClauses.${fieldIndex}.sign`}
                            defaultValue={field.sign || '==='}
                            control={control}
                            render={({ field }) => {
                              return (
                                <NSingleSelect
                                  options={SIGNS.map(sign => ({ value: sign }))}
                                  placeholder="Select operator"
                                  value={field.value}
                                  onValueChange={field.onChange}
                                />
                              )
                            }}
                          />
                          <NDivider size="xs" vertical />
                          <NColumn>
                            <Controller
                              name={`conditionClauses.${fieldIndex}.value`}
                              defaultValue={field.value}
                              control={control}
                              render={({ field }) => (
                                <ContentValuePicker
                                  selectedValue={field.value}
                                  onSelectValue={field.onChange}
                                  allowTypes={{ variable: true, value: true }}
                                />
                              )}
                            />
                          </NColumn>
                          <DeleteBtn>
                            <GlobalIcons.Trash onClick={() => remove(fieldIndex)} />
                          </DeleteBtn>
                        </NRow>
                        {fieldIndex !== fields.length - 1 && (
                          <>
                            {watchConditionLogic === 'or' ? (
                              <LogicOperator>OR</LogicOperator>
                            ) : watchConditionLogic === 'and' ? (
                              <LogicOperator>AND</LogicOperator>
                            ) : null}
                          </>
                        )}
                      </Fragment>
                    ))}
                    <AddBtnWrapper
                      onClick={() =>
                        append({
                          field: '',
                          sign: '===',
                        })
                      }
                    />
                  </>
                )}
              </Wrapper>
            )}
          </form>
        </ScrollWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isUpdating} onCancel={() => setShowModal(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
