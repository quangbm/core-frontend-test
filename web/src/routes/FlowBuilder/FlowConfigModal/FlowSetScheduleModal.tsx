import { format, parse, roundToNearestMinutes } from 'date-fns'
import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { ScheduleFlowAttributesDto, ScheduleFlowResponse, UpdateScheduleFlowDto } from '../../../../../shared/src/api'
import { NDatePicker } from '../../../components/DateTime/NDatePicker'
import { NTimePicker } from '../../../components/DateTime/NTimePicker'
import { spacing } from '../../../components/GlobalStyle'
import { NDivider } from '../../../components/NDivider'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { NModal } from '../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../components/NPerfectScrollbar'
import { NSingleSelect } from '../../../components/NSelect/NSingleSelect'
import { NToast } from '../../../components/NToast'
import { APIServices, QueryKeys } from '../../../services/api'
import { useFlowModifyConfirm } from '../hooks/useFlowModifyConfirm'
import { useBuilderState } from '../redux/hooks/useBuilderState'

const FREQUENCY_OPTIONS = [
  { label: 'Once', value: 'once' },
  { label: 'Daily', value: 'daily' },
  { label: 'Weekly', value: 'weekly' },
]

const Wrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`
type ScheduleFormType = {
  startDate: Date
  startTime: Date
  frequency: ScheduleFlowAttributesDto['frequency']
}

function getInitialValue() {
  return roundToNearestMinutes(new Date(), { nearestTo: 15 })
}

type Props = {
  showModal: boolean
  setShowModal: (val: boolean) => void
  flowData?: ScheduleFlowResponse
}

export const FlowSetScheduleModal: React.FC<Props> = ({ showModal, setShowModal, flowData }) => {
  const { handleSubmit, control } = useForm()
  const queryClient = useQueryClient()
  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  const { status } = useBuilderState()
  const warnFlowChange = useFlowModifyConfirm()

  const { mutate: updateFlow, isLoading: isUpdating } = useMutation(APIServices.Flows.updateFlowTemplate, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateVersion, { flowName, version }])
      setShowModal(false)
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const onSubmit = (data: ScheduleFormType) => {
    const updateData = {
      attributes: {
        frequency: data.frequency,
        startDate: format(data.startDate, 'yyyy-MM-dd'),
        startTime: format(data.startTime, 'HH:mm'),
      },
    } as UpdateScheduleFlowDto
    if (status !== 'draft') {
      warnFlowChange()
      return
    }
    updateFlow({
      flowName,
      version,
      data: updateData,
    })
  }

  return (
    <NModal size="large" setVisible={setShowModal} visible={showModal}>
      <NModal.Header onClose={() => setShowModal(false)} title="Set a schedule" />
      <NModal.Body>
        <NPerfectScrollbar style={{ maxHeight: '60vh' }}>
          <form>
            <Wrapper>
              <NRow>
                <NColumn flex={1}>
                  <Controller
                    defaultValue={
                      flowData?.attributes?.startDate
                        ? parse(flowData.attributes?.startDate, 'yyyy-MM-dd', getInitialValue())
                        : getInitialValue()
                    }
                    control={control}
                    name="startDate"
                    render={({ field }) => (
                      <NDatePicker
                        placeholder="Set Date"
                        label="Date"
                        value={field.value}
                        onChangeValue={value => {
                          field.onChange(value)
                        }}
                      />
                    )}
                  />
                </NColumn>
                <NDivider vertical size="md" />
                <NColumn flex={1}>
                  <Controller
                    defaultValue={
                      flowData?.attributes?.startTime
                        ? parse(flowData.attributes?.startTime, 'HH:mm', getInitialValue())
                        : getInitialValue()
                    }
                    control={control}
                    name="startTime"
                    render={({ field }) => (
                      <NTimePicker
                        secondPicker={false}
                        placeholder="Set Time"
                        label="Time"
                        value={field.value}
                        minuteInterval={15}
                        onChangeValue={value => {
                          field.onChange(value)
                        }}
                      />
                    )}
                  />
                </NColumn>
              </NRow>
              <NDivider size="md" />
              <NRow>
                <NColumn flex={1}>
                  <Controller
                    defaultValue={flowData?.attributes?.frequency || FREQUENCY_OPTIONS[0].value}
                    control={control}
                    name="frequency"
                    render={({ field }) => (
                      <NSingleSelect
                        options={FREQUENCY_OPTIONS}
                        placeholder="Frequency"
                        label="Frequency"
                        value={field.value}
                        onValueChange={value => {
                          field.onChange(value)
                        }}
                      />
                    )}
                  />
                </NColumn>
              </NRow>
            </Wrapper>
          </form>
        </NPerfectScrollbar>
      </NModal.Body>
      <NModal.Footer isLoading={isUpdating} onCancel={() => setShowModal(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
