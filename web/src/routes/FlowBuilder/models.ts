import {
  AndLogicRule,
  AssignmentVariableDto,
  ContentValue,
  FieldSchema,
  RecordMediaUpdate,
} from '../../../../shared/src/api'
import { SelectOption } from '../../components/NSelect/model'
import { PartialDecisionOutcomeData } from './FlowBuilderItem/FlowItemDecision/DecisionOutcomeMap'
import { CommonWidgetDataType, DataFlowItemConfigType } from './utils/types'

export type DecisionFormType = CommonWidgetDataType & {
  defaultOutcomeLabel: string
  outcomes: PartialDecisionOutcomeData[]
}

export type OutcomeDetail = {
  script: AndLogicRule[]
  displayName: string
  name: string
  condition: string
}

export type AssignmentFormType = CommonWidgetDataType & {
  assignmentVariables: Partial<AssignmentVariableDto>[]
}

export type LoopFormType = CommonWidgetDataType & {
  collectionVariableName: string
  loopOrder: 'ASC' | 'DESC'
}

export type SubFlowFormType = CommonWidgetDataType & {
  referencedFlow?: string
  inputMap: Record<string, any>
}

export type ActionFormType = CommonWidgetDataType & {
  actionMetadataId?: string
  inputMap: Record<string, any>
}

export type DataCreateFormType = CommonWidgetDataType & {
  objectName: string
  payload: { fieldName: string; value: string }[]
}

export type DataGetFormType = CommonWidgetDataType & {
  objectName: string
  query: { fieldName: string; value: string; sign: string }[]
  sortOrder: 'DESC' | 'ASC'
}

export type DataUpdateFormType = CommonWidgetDataType & {
  objectName: string
  query: { fieldName: string; value: string; sign: string }[]
}

export type DataDeleteFormType = CommonWidgetDataType & {
  objectName: string
  query: { fieldName: string; value: string; sign: string }[]
}

export type DataFormType = CommonWidgetDataType & {
  objectName: string
  query: { fieldName: string; value?: ContentValue; sign: string }[]
  payload: DataFlowItemConfigType[]
  mediaUpdate: RecordMediaUpdate[]
  mediaRemove: { mediaId: ContentValue }[]
  sortOrder: 'DESC' | 'ASC'
}

export type ObjectSchemaType = { fieldOptions: SelectOption[]; fieldsByName: Record<string, FieldSchema> }

export type AttributeType = 'config' | 'variable'
