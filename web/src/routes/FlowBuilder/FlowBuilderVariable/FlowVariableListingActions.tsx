import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router'
import { FlowVariableResponse } from '../../../../../shared/src/api'
import { NTableActions } from '../../../components/NTable/NTableActions'
import { NToast } from '../../../components/NToast'
import { APIServices, QueryKeys } from '../../../services/api'

type Props = {
  value: string
  data: FlowVariableResponse
  onEditClick?: (data: FlowVariableResponse) => void
}

export const FlowVariableListingActions: FC<Props> = ({ value, data, onEditClick }) => {
  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: deleteFlowVars, isLoading } = useMutation(APIServices.Flows.deleteFlowVariables)

  const handleEdit = () => {
    onEditClick && onEditClick(data)
    setShowDropList(false)
  }

  const handleDelete = () => {
    deleteFlowVars(
      { flowName, varName: data.name, version },
      {
        onSuccess: () => {
          queryClient.invalidateQueries(QueryKeys.Flows.getFlowTemplateVariables)
          setShowDropList(false)
        },
        onError: err => {
          NToast.error({ title: 'Delete object error', subtitle: `Name: ${value}\n${err.response?.data.message}` })
        },
      },
    )
  }

  return (
    <NTableActions
      showDropList={showDropList}
      setShowDropList={setShowDropList}
      options={[
        { title: 'Edit', onClick: handleEdit },
        { title: 'Delete', isLoading, onClick: handleDelete },
      ]}
    />
  )
}
