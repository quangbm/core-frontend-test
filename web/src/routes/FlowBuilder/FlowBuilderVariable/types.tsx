export enum FlowBuilderVariableState {
  Listing = 'Listing',
  Form = 'Form',
  Schema = 'Schema',
}
