import React, { FC } from 'react'
import { Controller, UseFormReturn } from 'react-hook-form'
import { useQuery } from 'react-query'
import { FlowVariableResponse } from '../../../../../../shared/src/api'
import { NDivider } from '../../../../components/NDivider'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../services/api'

const TYPE_OPTIONS: SelectOption[] = [
  { value: 'string', label: 'String' },
  { value: 'boolean', label: 'Boolean' },
  { value: 'integer', label: 'Integer' },
  { value: 'float', label: 'Float' },
  { value: 'date', label: 'Date' },
  { value: 'record', label: 'Record' },
]

type Props = {
  formMethods: UseFormReturn<FlowVariableResponse>
}
export const FlowVariableCollectionInput: FC<Props> = ({ formMethods }) => {
  const { control, watch, setValue } = formMethods
  const watchCollectionType = watch('attributes.itemType')

  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  React.useEffect(() => {
    formMethods.unregister('attributes.defaultValue')
  }, [])

  // get all objects
  const { data = [], isLoading } = useQuery(
    [QueryKeys.Builder.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  return (
    <>
      <Controller
        name="attributes.itemType"
        control={control}
        rules={{ required: { value: true, message: 'Required' } }}
        render={({ field, fieldState }) => {
          return (
            <NSingleSelect
              fullWidth
              required
              label="Collection Type"
              error={fieldState.error?.message}
              options={TYPE_OPTIONS}
              value={field.value}
              onValueChange={newType => {
                field.onChange(newType)
                setValue('attributes.recordObjectName', undefined as never)
              }}
            />
          )
        }}
      />
      {watchCollectionType === 'record' && (
        <>
          <NDivider size="md" />
          <Controller
            name="attributes.recordObjectName"
            control={control}
            rules={{ required: { value: true, message: 'Required' } }}
            render={({ field, fieldState }) => {
              return (
                <NSingleSelect
                  fullWidth
                  required
                  label="Item Record Type"
                  error={fieldState.error?.message}
                  options={data}
                  value={field.value}
                  onValueChange={newType => {
                    field.onChange(newType)
                  }}
                  isLoading={isLoading}
                  isSearchable
                  searchValue={searchValue}
                  onSearchValueChange={handleSearchChange}
                />
              )
            }}
          />
        </>
      )}
    </>
  )
}
