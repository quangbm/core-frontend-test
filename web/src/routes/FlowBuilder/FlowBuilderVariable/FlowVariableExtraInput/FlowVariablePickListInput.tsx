import React, { FC, useState } from 'react'
import { Controller, UseFormReturn } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled from 'styled-components/macro'
import {
  FlowPickListVariableAttributes,
  FlowVariableResponse,
  PickListResponse,
} from '../../../../../../shared/src/api'
import { spacing } from '../../../../components/GlobalStyle'
import { NButton } from '../../../../components/NButton/NButton'
import { NCheckbox } from '../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../components/NDivider'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../components/NPerfectScrollbar'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../services/api'
import { PickListItemsConfig } from '../../../Dashboard/routes/Organization/PickLists/components/PickListItemsConfig'

const ModalBody = styled(NModal.Body)`
  padding: ${spacing('xl')};
`

type Props = {
  formMethods: UseFormReturn<FlowVariableResponse>
  defaultValue?: FlowVariableResponse
}
export const FlowVariablePickListInput: FC<Props> = ({ formMethods, defaultValue }) => {
  const { control, watch } = formMethods
  const [showConfig, setShowConfig] = useState(false)
  const defaultPickListId =
    defaultValue && (defaultValue.attributes as FlowPickListVariableAttributes).pickListId.toString()

  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const watchPickListId = watch('attributes.pickListId') || defaultPickListId

  React.useEffect(() => {
    formMethods.unregister('attributes.defaultValue')
  }, [])

  const { data: pickListsData, isLoading: isLoadingPickList } = useQuery(
    [QueryKeys.PickLists.getAllPickLists, { searchText, status: 'ACTIVE' }],
    APIServices.PickLists.getAllPickLists,
    {
      keepPreviousData: true,
      select: resp =>
        resp.data.data.reduce(
          (a, pickList) => {
            a.pickListOptions.push({
              label: pickList.displayName || pickList.name,
              value: pickList.id.toString(),
            })
            a.pickListItemsById[pickList.id] = pickList
            return a
          },
          { pickListOptions: [] as SelectOption[], pickListItemsById: {} as Record<string, PickListResponse> },
        ),
    },
  )

  return (
    <>
      <Controller
        name="attributes.pickListId"
        control={control}
        rules={{ required: { value: true, message: 'Required' } }}
        render={({ field, fieldState }) => {
          return (
            <NSingleSelect
              fullWidth
              required
              label="Pick list"
              error={fieldState.error?.message}
              options={pickListsData?.pickListOptions || []}
              value={field.value}
              onValueChange={newType => {
                field.onChange(newType)
              }}
              isLoading={isLoadingPickList}
              isSearchable
              searchValue={searchValue}
              onSearchValueChange={handleSearchChange}
            />
          )
        }}
      />
      <NDivider size="md" />
      <NRow align="center" justify="space-between">
        <Controller
          name="attributes.isMulti"
          control={control}
          render={({ field }) => {
            return (
              <NCheckbox
                title="Is Multiple"
                checked={Boolean(field.value)}
                onChange={e => {
                  field.onChange(e.target.checked)
                }}
              />
            )
          }}
        />
        <NButton size="small" disabled={!watchPickListId} onClick={() => setShowConfig(true)}>
          Config
        </NButton>
      </NRow>
      <NModal visible={showConfig} setVisible={setShowConfig} size="x-large">
        <NModal.Header
          title="Pick list config"
          onClose={() => setShowConfig(false)}
          onBack={() => setShowConfig(false)}
        />
        <ModalBody>
          <NPerfectScrollbar style={{ maxHeight: '60vh' }}>
            {pickListsData?.pickListItemsById[watchPickListId] && (
              <PickListItemsConfig data={pickListsData?.pickListItemsById[watchPickListId]} />
            )}
          </NPerfectScrollbar>
        </ModalBody>
      </NModal>
    </>
  )
}
