import React, { FC } from 'react'
import { Controller, UseFormReturn } from 'react-hook-form'
import { useQuery } from 'react-query'
import { FlowVariableResponse } from '../../../../../../shared/src/api'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../services/api'

type Props = {
  formMethods: UseFormReturn<FlowVariableResponse>
}
export const FlowVariableRecordInput: FC<Props> = ({ formMethods }) => {
  const { control } = formMethods

  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  React.useEffect(() => {
    formMethods.unregister('attributes.defaultValue')
  }, [])

  // get all objects
  const { data = [], isLoading } = useQuery(
    [QueryKeys.Builder.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  return (
    <>
      <Controller
        name="attributes.recordObjectName"
        control={control}
        rules={{ required: { value: true, message: 'Required' } }}
        render={({ field, fieldState }) => {
          return (
            <NSingleSelect
              fullWidth
              required
              label="Record Type"
              error={fieldState.error?.message}
              options={data}
              value={field.value}
              onValueChange={newType => {
                field.onChange(newType)
              }}
              isLoading={isLoading}
              isSearchable
              searchValue={searchValue}
              onSearchValueChange={handleSearchChange}
            />
          )
        }}
      />
    </>
  )
}
