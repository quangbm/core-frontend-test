import { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router'
import styled, { useTheme } from 'styled-components/macro'
import { FlowVariableResponse, UpdateFlowVariableDto } from '../../../../../shared/src/api'
import { NDateTimePicker } from '../../../components/DateTime/NDateTimePicker'
import { color, spacing } from '../../../components/GlobalStyle'
import { NCheckbox } from '../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../components/NDivider'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { NModal } from '../../../components/NModal/NModal'
import { SelectOption } from '../../../components/NSelect/model'
import { NSingleSelect } from '../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../components/NTextInput/NTextInput'
import { NToast } from '../../../components/NToast'
import { useValidateString } from '../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../services/api'
import { fieldNameRegex } from '../../../utils/regex'
import { getDateFromIsoString, getNameFromDisplayName } from '../../../utils/utils'
import { useFlowModifyConfirm } from '../hooks/useFlowModifyConfirm'
import { useBuilderState } from '../redux/hooks/useBuilderState'
import { FlowVariableCollectionInput } from './FlowVariableExtraInput/FlowVariableCollectionInput'
import { FlowVariablePickListInput } from './FlowVariableExtraInput/FlowVariablePickListInput'
import { FlowVariableRecordInput } from './FlowVariableExtraInput/FlowVariableRecordInput'

const TYPE_OPTIONS: SelectOption[] = [
  { value: 'string', label: 'String' },
  { value: 'boolean', label: 'Boolean' },
  { value: 'integer', label: 'Integer' },
  { value: 'float', label: 'Float' },
  { value: 'date', label: 'Date' },
  { value: 'date-time', label: 'Date Time' },
  { value: 'record', label: 'Record' },
  { value: 'collection', label: 'Collection' },
  { value: 'pick-list', label: 'Pick List' },
]

const ObjectForm = styled.form`
  padding: ${spacing('xl')};
`

const ValueContainer = styled.div<{ isConstant?: boolean }>`
  display: flex;
  flex-direction: column;
  .constant {
    display: ${props => (props.isConstant ? 'flex' : 'none')};
  }
  .default {
    display: ${props => (props.isConstant ? 'none' : 'flex')};
  }
`

type Props = {
  defaultValue?: FlowVariableResponse
  onGoBack: () => void
}

export const FlowVariableForm: FC<Props> = ({ defaultValue, onGoBack }) => {
  const { flowName, version } = useParams<{ flowName: string; version: string }>()
  // post object mutation
  const theme = useTheme()
  const queryClient = useQueryClient()
  const { validateFunction } = useValidateString()
  const { status } = useBuilderState()

  const warnFlowChange = useFlowModifyConfirm()

  // create mode
  const { mutate: createFlowVariables, isLoading: isCreating } = useMutation(APIServices.Flows.createFlowVariables, {
    onSuccess: () => {
      queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateVersion, { flowName, version }])
      queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateVariables, { flowName, version }])
      onGoBack()
    },
    onError: err => {
      NToast.error({ title: 'Create variable error!', subtitle: `${err.response?.data.message}` })
    },
  })

  // update mode
  const { mutate: updateFlowVariables, isLoading: isUpdating } = useMutation(APIServices.Flows.updateFlowVariables, {
    onSuccess: () => {
      queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateVersion, { flowName, version }])
      queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplateVariables, { flowName, version }])
      onGoBack()
    },
    onError: err => {
      NToast.error({ title: 'Update variable error!', subtitle: `${err.response?.data.message}` })
    },
  })

  const transformedDefaultValue =
    defaultValue && defaultValue.type === 'pick-list'
      ? {
          ...defaultValue,
          //@ts-ignore
          attributes: { ...defaultValue.attributes, pickListId: defaultValue.attributes.pickListId.toString() },
        }
      : defaultValue

  const formMethods = useForm<FlowVariableResponse>({
    defaultValues: transformedDefaultValue || {
      name: '',
      type: 'string',
      isForInput: false,
      isForOutput: false,
    },
  })
  const { handleSubmit, register, control, formState, watch, setValue, getValues } = formMethods

  const watchType = watch('type')

  const handleApiSubmit = (data: FlowVariableResponse) => {
    if (defaultValue?.name) {
      updateFlowVariables({
        flowName,
        version,
        varName: defaultValue.name,
        body: {
          type: data.type as UpdateFlowVariableDto['type'],
          displayName: data.displayName,
          attributes: data.attributes,
          isForInput: data.isForInput,
          isForOutput: data.isForOutput,
        },
      })
      return
    }

    // TODO: fix for different type TS type
    const { type, ...restData } = data
    if (type === 'object') {
      return
    }

    createFlowVariables({
      flowName,
      version,
      body: {
        type,
        ...restData,
      },
    })
  }

  const onSubmit = (data: FlowVariableResponse) => {
    if (status !== 'draft') {
      warnFlowChange()
      return
    }
    handleApiSubmit(data)
  }

  const isEdit = !!defaultValue?.name

  return (
    <>
      <ObjectForm>
        <NRow>
          <NColumn>
            <NTextInput
              {...register('displayName', {
                validate: validateFunction,
              })}
              label="Display name"
              error={formState.errors.displayName?.message}
              required
              onBlur={e => {
                if (!!!defaultValue) {
                  const formatName = getNameFromDisplayName(e.target.value)
                  !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                }
              }}
            />
          </NColumn>
          <NDivider vertical size="md" />
          <NColumn>
            <NTextInput
              {...register('name', {
                validate: isEdit ? undefined : validateFunction,
                pattern: isEdit
                  ? undefined
                  : {
                      value: fieldNameRegex,
                      message: 'Invalid pattern',
                    },
              })}
              disabled={isEdit}
              required
              label="Name"
              error={formState.errors.name?.message}
            />
          </NColumn>
        </NRow>
        <NDivider size="md" />
        <NRow>
          <NColumn>
            <Controller
              name="type"
              control={control}
              rules={{ required: { value: true, message: 'Required' } }}
              render={({ field, fieldState }) => {
                return (
                  <NSingleSelect
                    required
                    disabled={isEdit}
                    label="Type"
                    error={fieldState.error?.message}
                    options={TYPE_OPTIONS}
                    value={field.value}
                    onValueChange={newType => {
                      field.onChange(newType)
                      setValue('attributes.itemType', undefined as never)
                      setValue('attributes.recordObjectName', undefined as never)
                      setValue('attributes.isMulti', undefined as never)
                      setValue('attributes.pickListId', undefined as never)
                      if (newType === 'boolean') {
                        setValue('attributes.defaultValue', '' as never)
                        return
                      }
                      setValue('attributes.defaultValue', undefined as never)
                    }}
                  />
                )
              }}
            />
          </NColumn>
          <NDivider size="md" vertical />
          <NColumn>
            {watchType === 'string' && (
              <>
                <ValueContainer>
                  <NTextInput
                    {...register('attributes.defaultValue')}
                    className="default"
                    disabled={isEdit}
                    label="Default value"
                  />
                </ValueContainer>
              </>
            )}
            {watchType === 'boolean' && (
              <>
                <NDivider size="md" />
                <ValueContainer>
                  <Controller
                    name="attributes.defaultValue"
                    control={control}
                    render={({ field }) => {
                      return (
                        <NCheckbox
                          disabled={isEdit}
                          className="default"
                          title="Default value"
                          checked={Boolean(field.value)}
                          onChange={e => {
                            field.onChange(e.target.checked)
                          }}
                        />
                      )
                    }}
                  />
                </ValueContainer>
              </>
            )}
            {(watchType === 'integer' || watchType === 'float') && (
              <>
                <ValueContainer>
                  <NTextInput
                    {...register('attributes.defaultValue')}
                    className="default"
                    disabled={isEdit}
                    type="number"
                    label="Default value"
                  />
                </ValueContainer>
              </>
            )}
            {(watchType === 'date' || watchType === 'date-time') && (
              <>
                <ValueContainer>
                  <Controller
                    name="attributes.defaultValue"
                    control={control}
                    render={({ field }) => {
                      // translate ios string to date
                      const valueDate = field.value ? getDateFromIsoString(field.value, 'datetime') : undefined
                      // translate date to iso string
                      const handleOnChange = (newDate: Date) => {
                        const iosString = newDate.toISOString()

                        field.onChange(iosString)
                      }
                      return (
                        <NDateTimePicker
                          disabled={isEdit}
                          className="default"
                          label="Default value"
                          value={valueDate}
                          onValueChange={handleOnChange}
                        />
                      )
                    }}
                  />
                </ValueContainer>
              </>
            )}
            {watchType === 'record' && <FlowVariableRecordInput formMethods={formMethods} />}
            {watchType === 'collection' && <FlowVariableCollectionInput formMethods={formMethods} />}
            {watchType === 'pick-list' && <FlowVariablePickListInput formMethods={formMethods} />}
          </NColumn>
        </NRow>

        <NDivider size={32} lineSize={1} lineColor={color('Neutral200')({ theme })} />
        <Controller
          name="isForInput"
          control={control}
          render={({ field }) => {
            return (
              <NCheckbox
                title="Available for input"
                checked={Boolean(field.value)}
                onChange={e => {
                  field.onChange(e.target.checked)
                }}
              />
            )
          }}
        />

        <NDivider size="md" />
        <Controller
          name="isForOutput"
          control={control}
          render={({ field }) => {
            return (
              <NCheckbox
                title="Available for output"
                checked={field.value}
                onChange={e => {
                  field.onChange(e.target.checked)
                }}
              />
            )
          }}
        />
      </ObjectForm>
      <NModal.Footer isLoading={isCreating || isUpdating} onCancel={onGoBack} onFinish={handleSubmit(onSubmit)} />
    </>
  )
}
