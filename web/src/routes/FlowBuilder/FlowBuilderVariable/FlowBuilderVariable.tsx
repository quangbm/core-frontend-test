import React, { FC, useState } from 'react'
import { FlowVariableResponse } from '../../../../../shared/src/api'
import { NModal } from '../../../components/NModal/NModal'
import { useFlowModifyConfirm } from '../hooks/useFlowModifyConfirm'
import { useBuilderState } from '../redux/hooks/useBuilderState'
import { FlowVariableForm } from './FlowVariableForm'
import { FlowVariableListing } from './FlowVariableListing'
import { FlowVariableSchema } from './FlowVariableSchema'
import { FlowBuilderVariableState } from './types'

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
}

export const FlowBuilderVariable: FC<Props> = ({ visible, setVisible }) => {
  const [modalState, setModalState] = useState(FlowBuilderVariableState.Listing)
  const [selectedVar, setSelectedVar] = useState<FlowVariableResponse | undefined>()
  const warnFlowChange = useFlowModifyConfirm()
  const { status } = useBuilderState()

  const changeStateToListing = () => {
    setSelectedVar(undefined)
    setModalState(FlowBuilderVariableState.Listing)
  }

  const showEditForm = (data: FlowVariableResponse) => {
    if (status !== 'draft') {
      warnFlowChange()
      return
    }
    setSelectedVar(data)
    setModalState(FlowBuilderVariableState.Form)
  }

  return (
    <NModal visible={visible} setVisible={setVisible} size="x-large">
      <NModal.Header
        onBack={modalState !== FlowBuilderVariableState.Listing ? changeStateToListing : undefined}
        onClose={() => {
          setVisible(false)
        }}>
        Variable Manager
      </NModal.Header>
      {modalState === FlowBuilderVariableState.Listing && (
        <FlowVariableListing
          onNewPressed={() => {
            if (status !== 'draft') {
              warnFlowChange()
              return
            }
            setModalState(FlowBuilderVariableState.Form)
          }}
          onEditClick={showEditForm}
          onRowSelect={flowVar => {
            const isVarWithSchema = flowVar.type === 'record' || flowVar.type === 'object'
            if (isVarWithSchema) {
              setModalState(FlowBuilderVariableState.Schema)
              setSelectedVar(flowVar)
            }
          }}
        />
      )}
      {modalState === FlowBuilderVariableState.Form && (
        <FlowVariableForm defaultValue={selectedVar} onGoBack={changeStateToListing} />
      )}
      {modalState === FlowBuilderVariableState.Schema && <FlowVariableSchema selectedVariable={selectedVar} />}
    </NModal>
  )
}
