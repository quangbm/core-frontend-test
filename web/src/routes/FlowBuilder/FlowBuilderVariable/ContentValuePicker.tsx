import { FC, useRef, useState } from 'react'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { ContentValue, FlowVariableResponse } from '../../../../../shared/src/api'
import { classnames } from '../../../common/classnames'
import { color, spacing } from '../../../components/GlobalStyle'
import { Icons } from '../../../components/Icons'
import { NCodeEditor } from '../../../components/NCodeEditor'
import { Suggestion as BaseSuggestion } from '../../../components/NCodeEditor/Suggestion'
import { NModal } from '../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../components/NPerfectScrollbar'
import { NPopover } from '../../../components/NPopover'
import { OptionsContainer, SelectDropdown, SelectLabel } from '../../../components/NSelect/CommonStyledSelect'
import { NTypography, RequiredIndicator } from '../../../components/NTypography'
import { useClickOutside } from '../../../hooks/useClickOutside'
import { useFlowVariables } from '../../../hooks/useFlowVariables'
import { renderVariableValue } from '../utils/functions'
import { FlowVariableForm } from './FlowVariableForm'
import { FlowVariableListing } from './FlowVariableListing'
import { FlowVariableSchema } from './FlowVariableSchema'
import { FlowBuilderVariableState } from './types'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const InputWrapper = styled.div`
  display: flex;
  align-items: center;
  height: 40px;
  padding: 0 ${spacing('xs')};

  cursor: pointer;
  transition: border-color 0.3s ease-in-out;
  border-radius: 4px;
  border: 1px solid ${color('Neutral200')};
  &:hover {
    border-color: ${color('Neutral400')};
  }
  &:focus-within {
    border-color: ${color('Primary700')};
  }

  &.is-disabled {
    cursor: not-allowed;
    background: ${color('Neutral200')};
    &:hover {
      border-color: ${color('Neutral200')};
    }
  }
`

const Input = styled('input')`
  border: none;
  font-size: 14px;
  line-height: 18px;
  padding: 0px 8px;
  flex: 1;
  width: 100%;
  color: ${color('Neutral900')};

  &:focus {
    outline: 0;
  }

  &.disabled {
    cursor: not-allowed;
  }

  :disabled {
    background-color: transparent;
  }
  ::placeholder {
    font-size: inherit;
    color: ${color('Neutral500')};
  }
`

const Caption = styled(NTypography.InputCaption)<{ isError?: boolean }>`
  margin-top: ${spacing('xxs')};
  color: ${props => (props.isError ? color('Red700') : color('Neutral400'))};
`

const ModalContentWrapper = styled.div`
  padding: ${spacing('md')};
  max-height: 70vh;
`

const Suggestion = styled(BaseSuggestion)`
  span {
    display: block;
    font-size: 12px;
  }
`

const StyledFlowListing = styled(FlowVariableListing)`
  padding: 0;
  overflow: visible;
`

const Option = styled.div`
  height: 40px;
  cursor: pointer;
  box-sizing: border-box;
  transition: all 0.3s ease-in-out;
  &.selected,
  &:hover {
    background: ${color('Neutral200')};
  }
`

const CloseIconContainer = styled.div`
  align-items: center;
  justify-content: center;
  display: flex;
  color: ${color('Neutral500')};
  width: 20px;
  height: 20px;
  border-radius: 50%;
  &.disabled {
    opacity: 0.6;
  }
  &.hide {
    display: none;
    visibility: hidden;
  }
  &:hover {
    background: ${color('Neutral200')};
  }
  transition: background 0.3s ease-in-out;
`

const TextLabel = styled.div`
  padding: ${spacing('md')};
  padding-top: ${spacing(11)};
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  color: ${color('Neutral700')};
`
type ValueType = 'variable' | 'value' | 'script'

type Props = {
  className?: string
  label?: string
  placeholder?: string
  caption?: string
  disabled?: boolean
  error?: string
  required?: boolean
  selectedValue?: ContentValue
  onClear?: () => void
  onSelectValue: (args?: ContentValue, flowVariable?: { name: string; type: string }) => void
  allowTypes?: Partial<Record<ValueType, boolean>>
} & React.HTMLAttributes<HTMLDivElement>

export const ContentValuePicker: FC<Props> = ({
  label,
  placeholder,
  caption,
  disabled,
  selectedValue,
  onSelectValue,
  required,
  allowTypes = {
    variable: true,
    value: true,
  },
  error,
  onClear,
  ...restProps
}) => {
  const { flowName } = useParams<{ flowName: string }>()
  const [valueType, setValueType] = useState<ValueType>(selectedValue?.type || 'value')
  const [showPicker, setShowPicker] = useState(false)
  const [modalState, setModalState] = useState(FlowBuilderVariableState.Listing)
  const [selectedVar, setSelectedVar] = useState<FlowVariableResponse | undefined>()
  const dropdownRef = useRef<HTMLDivElement>(null)
  const containerRef = useRef<HTMLDivElement>(null)
  const [showDropdown, setShowDropdown] = useState(false)

  const renderValue =
    selectedValue?.type === 'value' && selectedValue?.value ? selectedValue?.value : renderVariableValue(selectedValue)

  const [filter, setFilter] = useState('')
  const suggestion = useFlowVariables(flowName, filter)

  const closePicker = () => {
    setModalState(FlowBuilderVariableState.Listing)
    setShowPicker(false)
  }

  const onClearPress = () => {
    onSelectValue()
    onClear && onClear()
    closePicker()
  }

  const onRowSelect = (data: FlowVariableResponse) => {
    //
    const isVarWithSchema = data.type === 'record' || data.type === 'object'
    if (isVarWithSchema) {
      setModalState(FlowBuilderVariableState.Schema)
      setSelectedVar(data)
      return
    }
    onSelectValue({ value: data.name, type: 'variable' }, data)
    closePicker()
  }

  const onSelectFlowField = (contentValue: ContentValue, flowVar: { name: string; type: string }) => {
    onSelectValue(contentValue, flowVar)
    closePicker()
  }

  const changeStateToListing = () => {
    setSelectedVar(undefined)
    setModalState(FlowBuilderVariableState.Listing)
  }

  const showVariablePicker = () => {
    setValueType('variable')
    setShowDropdown(false)
    setShowPicker(true)
  }

  const showScriptEditor = () => {
    setValueType('script')
    setShowDropdown(false)
    setShowPicker(true)
  }

  useClickOutside(() => setShowDropdown(false), dropdownRef, containerRef.current ? [containerRef.current] : undefined)

  return (
    <>
      <Wrapper {...restProps}>
        <div
          ref={containerRef}
          onClick={() => {
            if (disabled) {
              return
            }
            setShowDropdown(true)
          }}>
          {label && (
            <SelectLabel>
              {required && <RequiredIndicator>*</RequiredIndicator>}
              {label}
            </SelectLabel>
          )}
          <InputWrapper>
            <Input
              disabled={disabled || !allowTypes.value}
              value={renderValue}
              onChange={e => {
                if (showDropdown) {
                  setShowDropdown(false)
                }
                if (!e.target.value) {
                  onSelectValue(undefined)
                  return
                }
                onSelectValue({ type: 'value', value: e.target.value })
              }}
              className={classnames([disabled && 'disabled'])}
              placeholder={placeholder || 'Search variable'}
            />
            <CloseIconContainer
              onClick={e => {
                e.stopPropagation()
                onClear && onClear()
                onSelectValue(undefined)
              }}
              className={classnames([disabled && 'disabled', !selectedValue && 'hide'])}>
              <Icons.Close8 />
            </CloseIconContainer>
          </InputWrapper>
        </div>
        {(error || caption) && <Caption isError={!!error}>{error || caption}</Caption>}
      </Wrapper>
      {showDropdown && (!selectedValue || (selectedValue && selectedValue.type !== 'value')) && (
        <NPopover targetRef={containerRef} ref={dropdownRef} placement={'bottom'} offsetX={0} offsetY={4} fullWidth>
          <SelectDropdown>
            <OptionsContainer>
              {(() => {
                switch (selectedValue?.type) {
                  case undefined:
                    return (
                      <>
                        {allowTypes.variable && (
                          <Option onClick={showVariablePicker}>
                            <TextLabel>New variable</TextLabel>
                          </Option>
                        )}
                        {allowTypes.script && (
                          <Option>
                            <TextLabel onClick={showScriptEditor}>New script</TextLabel>
                          </Option>
                        )}
                      </>
                    )
                  case 'script':
                    return (
                      <Option>
                        <TextLabel onClick={showScriptEditor}>Edit script</TextLabel>
                      </Option>
                    )
                  case 'variable':
                    return (
                      <Option onClick={showVariablePicker}>
                        <TextLabel>Edit variable</TextLabel>
                      </Option>
                    )
                }
              })()}
            </OptionsContainer>
          </SelectDropdown>
        </NPopover>
      )}

      {showPicker && (
        <NModal
          size="large"
          visible={showPicker}
          setVisible={(newVisible: boolean) => {
            if (newVisible) {
              setShowPicker(true)
              return
            }
            setShowPicker(false)
          }}>
          <NModal.Header
            title={label}
            onBack={
              valueType !== 'variable' || modalState === FlowBuilderVariableState.Listing
                ? undefined
                : changeStateToListing
            }
            onClose={() => setShowPicker(false)}
          />
          <NPerfectScrollbar>
            {(() => {
              switch (valueType) {
                case 'script':
                  return (
                    <ModalContentWrapper>
                      <NCodeEditor
                        placeholder="Enter script"
                        label="Script"
                        trigger="{{"
                        replacer={v => `{\\$${v}}`}
                        value={selectedValue?.value || ''}
                        onChange={e => onSelectValue({ type: 'script', value: e.target.value })}
                        onSearchChange={setFilter}
                        language="javascript">
                        {suggestion
                          .filter(v => v.name.toLowerCase().includes(filter.toLowerCase()))
                          .map(variable => (
                            <Suggestion value={variable.name} key={variable.name}>
                              {variable.displayName}
                            </Suggestion>
                          ))}
                      </NCodeEditor>
                    </ModalContentWrapper>
                  )
                case 'variable':
                  return (
                    <>
                      {modalState === FlowBuilderVariableState.Listing && (
                        <ModalContentWrapper>
                          <StyledFlowListing
                            selectMode
                            searchPlaceholder="Search variable"
                            searchDefaultValue={selectedValue?.value}
                            onNewPressed={() => {
                              setModalState(FlowBuilderVariableState.Form)
                            }}
                            onRowSelect={onRowSelect}
                            onClearSelect={onClearPress}
                          />
                        </ModalContentWrapper>
                      )}
                      {modalState === FlowBuilderVariableState.Form && (
                        <FlowVariableForm onGoBack={changeStateToListing} />
                      )}
                      {modalState === FlowBuilderVariableState.Schema && (
                        <FlowVariableSchema selectedVariable={selectedVar} onSelectField={onSelectFlowField} />
                      )}
                    </>
                  )
              }
            })()}
          </NPerfectScrollbar>
        </NModal>
      )}
    </>
  )
}
