import { FC, KeyboardEventHandler, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router'
import { SortingRule } from 'react-table'
import styled from 'styled-components/macro'
import { FlowVariableAttributes, FlowVariableResponse } from '../../../../../shared/src/api'
import { classnames } from '../../../common/classnames'
import { spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NDivider } from '../../../components/NDivider'
import { NPagination } from '../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../components/NTable/NTableStyledContainer'
import { NTextInput } from '../../../components/NTextInput/NTextInput'
import { NToast } from '../../../components/NToast'
import { useSearchText } from '../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../services/api'
import { FlowVariableListingActions } from './FlowVariableListingActions'

const LIMIT = 10

const Wrapper = styled.div`
  padding: ${spacing('xl')};
  overflow-y: scroll;
  max-height: 70vh;
`

const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: ${spacing('xl')};

  button:not(:last-child) {
    margin-right: ${spacing('sm')};
  }
`

type Props = {
  className?: string
  searchPlaceholder?: string
  searchDefaultValue?: string
  searchError?: string
  selectMode?: boolean
  onNewPressed?: () => void
  onEditClick?: (data: FlowVariableResponse) => void
  onRowSelect?: (data: FlowVariableResponse) => void
  onClearSelect?: () => void
  onSearchKeyDown?: KeyboardEventHandler<HTMLInputElement>
}

export const FlowVariableListing: FC<Props> = ({
  className,
  searchPlaceholder = 'Search',
  searchDefaultValue,
  searchError,
  selectMode,
  onNewPressed,
  onEditClick,
  onRowSelect,
  onClearSelect,
  onSearchKeyDown,
}) => {
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText(searchDefaultValue)

  const [page, setPage] = useState(1)
  const [sortConfig, setSortConfig] = useState<SortingRule<FlowVariableResponse>[]>([])

  const { flowName, version } = useParams<{ flowName: string; version: string }>()

  // get flow item variables
  const { data, isFetching } = useQuery(
    [QueryKeys.Flows.getFlowTemplateVariables, { flowName, version }],
    APIServices.Flows.getFlowTemplateVariables,
    {
      enabled: !!flowName,
      onError: error => {
        NToast.error({ title: 'Get flow variable error', subtitle: error.response?.data.message })
      },
    },
  )

  const columns = useMemo<NTableColumnType<FlowVariableResponse>>(() => {
    function handleEditObject(data: FlowVariableResponse) {
      onEditClick && onEditClick(data)
    }

    const result: NTableColumnType<FlowVariableResponse> = [
      {
        Header: 'Display name',
        accessor: 'displayName',
        defaultCanSort: true,
      },
      {
        Header: 'Name',
        accessor: 'name',
      },
      {
        Header: 'Type',
        accessor: 'type',
      },
      {
        Header: 'Default value',
        // React-Tablet's type suck!
        // @ts-ignore
        accessor: 'attributes.defaultValue',
      },
    ]

    if (!selectMode) {
      result.push({
        accessor: 'name',
        Cell: ({ value, row }) => {
          return <FlowVariableListingActions value={value} data={row.original} onEditClick={handleEditObject} />
        },
        id: classnames([NTableCollapsedCellClassName]),
      })
    }

    return result
  }, [selectMode, onEditClick])

  const flowVars = useMemo(() => {
    if (!data) {
      return []
    }
    let flowVarsData =
      sortConfig.length > 0
        ? [...data.data].sort((a, b) =>
            (sortConfig[0].desc ? a.displayName < b.displayName : a.displayName > b.displayName) ? 1 : -1,
          )
        : data.data

    if (searchText) {
      setPage(1)
      flowVarsData = flowVarsData.filter(
        flowVar =>
          flowVar.name.includes(searchText) ||
          flowVar.displayName.includes(searchText) ||
          JSON.stringify((flowVar.attributes as FlowVariableAttributes)?.defaultValue)?.includes(searchText),
      )
    }
    return flowVarsData
  }, [data, searchText, sortConfig])

  return (
    <Wrapper className={className}>
      <ButtonContainer>
        {selectMode && onClearSelect && <NButton onClick={onClearSelect}>Deselect</NButton>}
        {onNewPressed && (
          <NButton type="primary" onClick={onNewPressed}>
            New variable
          </NButton>
        )}
      </ButtonContainer>
      <NTextInput
        className="input"
        placeholder={searchPlaceholder}
        error={searchError}
        value={searchValue || ''}
        onChange={e => {
          handleSearchChange(e.target.value)
        }}
        onKeyDown={onSearchKeyDown}
      />
      <NDivider size="xl" />

      <NTable
        isLoading={isFetching}
        columns={columns}
        data={flowVars.slice((page - 1) * LIMIT, (page - 1) * LIMIT + LIMIT)}
        onClickRow={onRowSelect}
        pageSize={LIMIT}
        onChangeSort={config => setSortConfig(config)}
        pagination={
          <NPagination total={Math.ceil(flowVars.length / LIMIT)} current={page} onChangePage={page => setPage(page)} />
        }
      />
    </Wrapper>
  )
}
