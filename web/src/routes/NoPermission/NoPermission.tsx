import * as React from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components/macro'
import { NButton } from '../../components/NButton/NButton'

const Wrapper = styled('div')`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  min-height: 100vh;
`

export const NoPermission: React.FC = () => {
  const { push } = useHistory()
  return (
    <Wrapper>
      <h1>403: You don't have permission to view this page</h1>
      <div>
        <NButton type="link" onClick={() => push('/')}>
          Go to Home
        </NButton>
      </div>
    </Wrapper>
  )
}
