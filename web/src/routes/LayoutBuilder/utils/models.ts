import { DataResponse, ObjectSchemaResponse } from '../../../../../shared/src/api'

export enum LayoutActionType {
  AddElement = 'LAYOUT_ADD_ELEMENT',
  UpdateElement = 'LAYOUT_UPDATE_ELEMENT',
  RemoveElement = 'LAYOUT_REMOVE_ELEMENT',
  SortElement = 'LAYOUT_SORT_ELEMENT',

  SelectElement = 'LAYOUT_SELECT_ELEMENT',
  MoveElement = 'LAYOUT_MOVE_ELEMENT',
}

export enum ElementType {
  Layout = 'ELEMENT_LAYOUT',
  Field = 'ELEMENT_FIELD',
  Component = 'ELEMENT_COMPONENT',
}

export type Element<T = any> = {
  id: string
  label: string
  type: ElementType
  component: string
  props: T
  children: string[]
  // contentFieldName?: string
  schema?: ObjectSchemaResponse
}

export type DraggableElement = {
  elementId?: string
  type: ElementType
  item?: Omit<Element, 'id'> & { id?: string }
  parentId: string
  index: number
}

export type ZoneProps = {
  elementId: string
  parentId: string
  index: number
  canDrag?: boolean | ((item: DraggableElement) => boolean)
  canDrop?: boolean | ((item: DraggableElement) => boolean)
  canDuplicate?: boolean
  canRemove?: boolean
  onRemove?: () => void
}

export type SpacingUnit = 'px' | 'em' | 'rem' | '%'

// Use Element
export enum MousePosition {
  Top = 'move-top',
  Right = 'move-right',
  Bottom = 'move-bottom',
  Left = 'move-left',
}

export enum Direction {
  Vertical,
  Horizontal,
}

export type InputMap = {
  curUser?: Record<string, any>
  curRecord?: DataResponse
  now: string
}
