import * as React from 'react'
import { useForm, useFormState } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { Link, useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components/macro'
import { ApplicationResponse, ComponentResponse, LayoutResponse, UpdateLayoutDto } from '../../../../shared/src/api'
import { BuilderHeader } from '../../components/Builder/BuilderHeader'
import { color } from '../../components/GlobalStyle'
import { SidebarIcons } from '../../components/Icons'
import { NButton } from '../../components/NButton/NButton'
import { NDivider } from '../../components/NDivider'
import { NSpinner } from '../../components/NSpinner/NSpinner'
import { NTextArea } from '../../components/NTextArea/NTextArea'
import { NTextInput } from '../../components/NTextInput/NTextInput'
import { NToast } from '../../components/NToast'
import { typography } from '../../components/NTypography'
import { Portal } from '../../components/Portal'
import { APIServices, QueryKeys } from '../../services/api/api'
import { AskForPublishModal } from './components/AskForPublishModal'
import { DroppableZone } from './components/DroppableZone'
import { NavIcons } from './components/Icons'
import { LeftSidebar } from './components/LeftSidebar'
import { PublishAppPageModal } from './components/PublishAppPageModal'
import { PublishRecordPageModal } from './components/PublishRecordPageModal'
import { RecordPageAppConfirmModal } from './components/RecordPageAppConfirmModal'
import { RightSidebar, sidebarPortalRef } from './components/RightSidebar'
import { Widget } from './components/Sidebar'
import { ActionsUndoRedo, useLayoutAction, useLayoutBuilder } from './contexts/LayoutBuilderContext'
import { ROOT_ELEMENT } from './utils/builder'

const Wrapper = styled('div')`
  display: grid;
  grid-template-rows: auto 1fr;
  grid-template-columns: 251px 1fr 250px;
  grid-template-areas:
    'header header header'
    'left-sidebar droppable-zone right-sidebar';
  background: ${color('Neutral200')};
  width: 100vw;
  height: 100vh;
  overflow: hidden;
`

const Breadcrumbs = styled('ul')`
  display: flex;
  height: 100%;
  li {
    position: relative;
    display: inline-flex;
    align-items: center;
    height: 100%;
    ${typography('button')};
    a {
      color: ${color('Neutral400')};
      &:after {
        content: '/';
        margin: 0 4px;
        color: ${color('Neutral400')};
      }
      &:hover {
        color: ${color('Neutral900')};
      }
    }
  }
`

const SaveBtn = styled(NButton)`
  width: 80px;
`

type LayoutBuilderProps = {
  children?: React.ReactNode
}

export function LayoutBuilder({}: LayoutBuilderProps) {
  const theme = useTheme()
  const queryClient = useQueryClient()

  const params = useParams<{ layoutId: string }>()

  const { undoRedoElement } = useLayoutAction()
  const { layoutDetails: layoutData, elements, indexHistory, elementHistory, selectedElement } = useLayoutBuilder()
  const { selectElement } = useLayoutAction()
  const [selectedAppsForRecord, setSelectedAppsForRecord] = React.useState<ApplicationResponse[]>([])

  const layoutDetails = (layoutData || {}) as LayoutResponse
  const { register, formState, handleSubmit, control } = useForm<UpdateLayoutDto>()
  const { isDirty } = useFormState({
    control,
  })

  const [showAskPublish, setShowAskPublish] = React.useState(false)
  const [showPublishAppPage, setShowPublishAppPage] = React.useState(false)
  const [showPublishRecordPage, setShowPublishRecordPage] = React.useState(false)
  const [showRecordAppConfirm, setShowRecordAppConfirm] = React.useState(false)

  const handlePublish = () => {
    const layoutType = layoutDetails.type
    if (!layoutType) {
      return
    }
    if (layoutType === 'app-page') {
      setShowPublishAppPage(true)
      return
    }
    setShowPublishRecordPage(true)
  }

  const { mutate: updateLayoutScript, isLoading: isUpdatingScript } = useMutation(APIServices.Builder.putLayoutScript, {
    onSuccess: (_data, variables) => {
      queryClient.invalidateQueries([QueryKeys.Builder.getLayoutScript, { id: variables.id }])
      queryClient.invalidateQueries([QueryKeys.App.getAppScript, { layoutId: variables.id }])
      NToast.success({ title: 'Layout Builder', subtitle: 'Save script successful' })
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const { mutate: updateLayout, isLoading: isUpdatingLayout } = useMutation(APIServices.Builder.putLayout, {
    onSuccess: () => {
      queryClient.invalidateQueries([QueryKeys.Builder.getLayouts])
      queryClient.invalidateQueries([QueryKeys.Builder.getLayout, { id: layoutDetails.id }])
      NToast.success({ title: 'Layout Builder', subtitle: 'Update metadata successful' })
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const handleSave = (data: UpdateLayoutDto) => {
    selectElement(ROOT_ELEMENT)
    if (!params.layoutId) {
      setShowAskPublish(true)
    }
    updateLayoutScript({ id: params.layoutId, script: elements as Record<string, ComponentResponse> })
    isDirty && updateLayout({ id: layoutDetails.id, ...data })
  }

  const handleAssignAppDefault = (apps: ApplicationResponse[]) => {
    setSelectedAppsForRecord(apps)
    //for record modal
    setShowPublishRecordPage(false)
    setShowRecordAppConfirm(true)
  }

  const handleBackAppDefault = () => {
    setShowPublishRecordPage(true)
    setShowRecordAppConfirm(false)
  }

  return (
    <Wrapper>
      <BuilderHeader
        icon={<SidebarIcons.LayoutOutlined />}
        title="Layout Builder"
        rightElement={
          <React.Fragment>
            <NButton
              type="link"
              icon={<NavIcons.Undo />}
              size="small"
              disabled={indexHistory === 0}
              onClick={() => undoRedoElement(ActionsUndoRedo.Undo)}
            />
            <NButton
              type="link"
              icon={<NavIcons.Redo />}
              size="small"
              disabled={indexHistory === elementHistory.length - 1}
              onClick={() => undoRedoElement(ActionsUndoRedo.Redo)}
            />
            <NDivider vertical size="md" lineSize={1} lineColor={color('Neutral200')({ theme })} />
            <NDivider vertical />
            <NButton type="outline" size="small" onClick={handlePublish}>
              Publish
            </NButton>
            <NDivider vertical />
            <SaveBtn
              type="primary"
              size="small"
              loading={isUpdatingScript || isUpdatingLayout}
              onClick={handleSubmit(handleSave)}>
              Save
            </SaveBtn>
          </React.Fragment>
        }
        style={{ gridArea: 'header' }}>
        <Breadcrumbs>
          <li>
            <Link to="/setup/layout">Layout</Link>
          </li>
          <li>{layoutDetails?.name || <NSpinner size={8} strokeWidth={2} />}</li>
        </Breadcrumbs>
      </BuilderHeader>
      <LeftSidebar />
      <DroppableZone />
      <RightSidebar />
      <Portal mountNode={sidebarPortalRef}>
        {(selectedElement === ROOT_ELEMENT || !selectedElement) && layoutData && (
          <div
            onClick={e => {
              e.stopPropagation()
            }}>
            <Widget title="Layout's metadata">
              <NTextInput
                defaultValue={layoutDetails.name || ''}
                placeholder="Layout's name"
                label="Name"
                error={formState.errors.name?.message}
                {...register('name', {
                  required: {
                    value: true,
                    message: 'Required',
                  },
                })}
                required
              />
              <NDivider size="md" />
              <NTextArea
                defaultValue={layoutDetails.description || ''}
                label="Description"
                rows={4}
                {...register('description')}
              />
            </Widget>
          </div>
        )}
      </Portal>
      {showAskPublish && (
        <AskForPublishModal visible={showAskPublish} setVisible={setShowAskPublish} name={layoutDetails!.name}>
          <NButton
            size="small"
            type="ghost"
            onClick={() => {
              setShowAskPublish(false)
            }}>
            Not yet
          </NButton>
          <NButton
            size="small"
            type="primary"
            onClick={() => {
              setShowAskPublish(false)
              handlePublish()
            }}>
            Publish
          </NButton>
        </AskForPublishModal>
      )}
      {showPublishAppPage && (
        <PublishAppPageModal
          visible={showPublishAppPage}
          setVisible={setShowPublishAppPage}
          name={layoutDetails!.name}
        />
      )}

      {showPublishRecordPage && (
        <PublishRecordPageModal
          visible={showPublishRecordPage}
          setVisible={setShowPublishRecordPage}
          name={layoutDetails!.name}
          onAssignToApp={handleAssignAppDefault}
        />
      )}

      {showRecordAppConfirm && (
        <RecordPageAppConfirmModal
          onBack={handleBackAppDefault}
          visible={showRecordAppConfirm}
          setVisible={setShowRecordAppConfirm}
          selectedApps={selectedAppsForRecord}
        />
      )}
    </Wrapper>
  )
}
