import React from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { LayoutResponse } from '../../../../../shared/src/api'
import { color, spacing } from '../../../components/GlobalStyle'
import { APIServices, QueryKeys } from '../../../services/api'
import { ROOT_ELEMENT } from '../utils/builder'
import { Element, ElementType } from '../utils/models'

const ErrorWrapper = styled('div')`
  display: flex;
  justify-content: center;
  align-items: center;
`

const ErrorMessage = styled('div')`
  color: ${color('danger')};

  &:not(:last-child) {
    margin-bottom: ${spacing('xs')};
  }
`

function appendElementAt<T = string>(element: T, target: T[], position: number): T[] {
  return [...target.slice(0, position), element, ...target.slice(position)]
}

type LayoutBuilderContextType = {
  elementHistory: Record<string, Element>[]
  elements: Record<string, Element>
  selectedElement?: string
  indexHistory: number
  layoutDetails?: LayoutResponse
  isLoading: boolean
}

const LayoutBuilderContext = React.createContext<LayoutBuilderContextType | string>(
  'useLayoutBuilder should be used inside LayoutBuilderProvider',
)

const initialState = {
  isLoading: true,
  elementHistory: [
    {
      [ROOT_ELEMENT]: {
        id: ROOT_ELEMENT,
        label: 'Body',
        type: ElementType.Layout,
        component: '',
        props: {} as Record<string, any>,
        children: [] as string[],
      },
    },
  ],
  indexHistory: 0,
  elements: {
    [ROOT_ELEMENT]: {
      id: ROOT_ELEMENT,
      label: 'Body',
      type: ElementType.Layout,
      component: '',
      props: {} as Record<string, any>,
      children: [] as string[],
    },
  },
} as LayoutBuilderContextType

export enum Actions {
  AddElement,
  AddSubElement,
  MoveElement,
  SelectElement,
  ConfigElement,
  RemoveElement,
  RemoveSubElement,
  UndoRedoElement,
  UpdateHistory,
  Internal,
}

export enum ActionsUndoRedo {
  Redo,
  Undo,
}

type Action =
  | {
      type: Actions.AddElement
      payload: {
        element: Element
        source: { id: string }
        target: { id: string; index?: number }
        selectElementId?: string
      }
    }
  | {
      type: Actions.AddSubElement
      payload: {
        element: Element
        selectElementId?: string
      }
    }
  | {
      type: Actions.MoveElement
      payload: {
        elementId: string
        source: { id: string }
        target: { id: string; index?: number }
      }
    }
  | {
      type: Actions.SelectElement
      payload: string
    }
  | {
      type: Actions.ConfigElement
      payload: { elementId: string; props: Record<string, any>; children?: string[] }
    }
  | {
      type: Actions.RemoveElement
      payload: { elementId: string; selectElementId?: string }
    }
  | {
      type: Actions.RemoveSubElement
      payload: { elementId: string; selectElementId?: string }
    }
  | {
      type: Actions.UndoRedoElement
      payload: ActionsUndoRedo
    }
  | {
      type: Actions.UpdateHistory
    }
  | { type: Actions.Internal; payload: Partial<LayoutBuilderContextType> }

function reducer(store: LayoutBuilderContextType, action: Action) {
  switch (action.type) {
    case Actions.Internal: {
      return {
        ...store,
        ...action.payload,
      }
    }
    case Actions.AddElement: {
      const {
        element,
        target: { id: targetId, index },
        selectElementId,
      } = action.payload

      const target = store.elements[targetId]
      const children = target.children || []

      const newElement = {
        ...store.elements,
        [targetId]: {
          ...target,
          children: index === undefined ? children.concat(element.id) : appendElementAt(element.id, children, index),
        },
        [element.id]: element,
      }

      return {
        ...store,
        elements: newElement,
        selectedElement: selectElementId || element.id,
      }
    }

    case Actions.AddSubElement: {
      const { element, selectElementId } = action.payload
      const newElement = {
        ...store.elements,
        [element.id]: element,
      }

      return {
        ...store,
        elements: newElement,
        selectedElement: selectElementId || element.id,
      }
    }

    case Actions.MoveElement: {
      const {
        elementId,
        source: { id: sourceId },
        target: { id: targetId, index },
      } = action.payload

      const source = store.elements[sourceId]
      const target = store.elements[targetId]

      if (sourceId === targetId) {
        const children = (target.children || []).filter(i => i !== elementId)
        const newElement = {
          ...store.elements,
          [targetId]: {
            ...target,
            children: index === undefined ? children.concat(elementId) : appendElementAt(elementId, children, index),
          },
        }
        return {
          ...store,
          elements: newElement,
        }
      }
      const children = target.children || []

      const tempElement = {
        ...store.elements,
        [targetId]: {
          ...target,
          children: index === undefined ? children.concat(elementId) : appendElementAt(elementId, children, index),
        },
        [sourceId]: {
          ...source,
          children: (source.children || []).filter(i => i !== elementId),
        },
      }

      return {
        ...store,
        elements: tempElement,
      }
    }
    case Actions.SelectElement: {
      if (action.payload === store.selectedElement) {
        return store
      }
      return {
        ...store,
        selectedElement: action.payload,
      }
    }

    case Actions.ConfigElement: {
      const { props, elementId, children } = action.payload
      const element = store.elements[elementId]
      return {
        ...store,
        elements: {
          ...store.elements,
          [element.id]: {
            ...element,
            children: children || element.children,
            props: {
              ...element.props,
              ...props,
            },
          },
        },
      }
    }

    case Actions.RemoveElement: {
      const { elementId, selectElementId } = action.payload
      const listDeleteElement: string[] = []

      const recusiveGetChildren = (id: string) => {
        const eachElement = store.elements[id].children
        if (eachElement && eachElement.length > 0) {
          eachElement.map(childId => recusiveGetChildren(childId))
        }
        listDeleteElement.push(id)
      }

      recusiveGetChildren(elementId)

      const tempStore =
        store.elements &&
        Object.keys(store.elements)
          .filter(item => !listDeleteElement.includes(item))
          .reduce((acc, key) => {
            const item = store.elements[key]
            if (item.children) {
              const listChildren = item.children.filter(i => !listDeleteElement.includes(i))
              return {
                ...acc,
                [key]: { ...store.elements[key], children: listChildren.length > 0 ? listChildren : [] },
              }
            }
            return { ...acc, [key]: store.elements[key] }
          }, {})

      return {
        ...store,
        elements: tempStore,
        selectedElement: selectElementId || ROOT_ELEMENT,
      }
    }

    case Actions.RemoveSubElement: {
      const { elementId, selectElementId } = action.payload

      const tempStore = { ...store.elements }
      delete tempStore[elementId]

      return {
        ...store,
        elements: tempStore,
        selectedElement: selectElementId || ROOT_ELEMENT,
      }
    }

    case Actions.UndoRedoElement: {
      switch (action.payload) {
        case ActionsUndoRedo.Undo:
          return {
            ...store,
            elements: store.elementHistory[store.indexHistory - 1],
            indexHistory: store.indexHistory - 1,
            selectedElement: ROOT_ELEMENT,
          }
        case ActionsUndoRedo.Redo:
          return {
            ...store,
            elements: store.elementHistory[store.indexHistory + 1],
            indexHistory: store.indexHistory + 1,
            selectedElement: ROOT_ELEMENT,
          }
        default:
          return store
      }
    }

    case Actions.UpdateHistory: {
      return {
        ...store,
        elementHistory: [
          ...store.elementHistory.slice(store.elementHistory.length === 10 ? 1 : 0, store.indexHistory + 1),
          store.elements,
        ],
        indexHistory: store.indexHistory === 9 ? 9 : store.indexHistory + 1,
      }
    }

    default:
      return store
  }
}

const LayoutBuilderActionContext = React.createContext<React.Dispatch<Action> | string>(
  'useLayoutAction should be used inside LayoutBuilderProvider',
)

type LayoutBuilderContextProps = {}

export const LayoutBuilderProvider: React.FC<LayoutBuilderContextProps> = React.memo(({ children }) => {
  const params = useParams<{ layoutId: string }>()
  const [value, dispatch] = React.useReducer(reducer, initialState)

  const form = useForm()

  const {
    data: layoutData,
    isError: isGetLayoutError,
    error: layoutError,
  } = useQuery([QueryKeys.Builder.getLayout, { id: params.layoutId }], APIServices.Builder.getLayout)

  const {
    data: scriptData,
    isError: isGetScriptError,
    error: scriptError,
  } = useQuery([QueryKeys.Builder.getLayoutScript, { id: params.layoutId }], APIServices.Builder.getLayoutScript)

  React.useEffect(() => {
    if (!layoutData || !scriptData) {
      return
    }
    const elements = scriptData.data as Record<string, Element>
    const elementHistory = [elements]
    dispatch({
      type: Actions.Internal,
      payload: { isLoading: false, elements, elementHistory, layoutDetails: layoutData.data },
    })
  }, [layoutData, scriptData])

  const isError = isGetLayoutError || isGetScriptError

  return (
    <LayoutBuilderContext.Provider value={value}>
      <LayoutBuilderActionContext.Provider value={dispatch}>
        <FormProvider {...form}>
          {isError && (
            <ErrorWrapper>
              {isGetLayoutError && <ErrorMessage>{layoutError?.response?.data.message}</ErrorMessage>}
              {isGetScriptError && <ErrorMessage>{scriptError?.response?.data.message}</ErrorMessage>}
            </ErrorWrapper>
          )}
          {children}
        </FormProvider>
      </LayoutBuilderActionContext.Provider>
    </LayoutBuilderContext.Provider>
  )
})

export const useLayoutBuilder = () => {
  const context = React.useContext(LayoutBuilderContext)
  if (typeof context === 'string') {
    throw new Error(context)
  }
  return context
}

export const useLayoutAction = () => {
  const dispatch = React.useContext(LayoutBuilderActionContext)
  if (typeof dispatch === 'string') {
    throw new Error(dispatch)
  }

  const addElement = React.useCallback(
    (payload: {
      element: Element
      source: { id: string }
      target: { id: string; index?: number }
      selectElementId?: string
    }) => {
      dispatch({ type: Actions.AddElement, payload })
      dispatch({ type: Actions.UpdateHistory })
    },
    [dispatch],
  )

  const addSubElement = React.useCallback(
    (payload: { element: Element; selectElementId?: string }) => {
      dispatch({ type: Actions.AddSubElement, payload })
    },
    [dispatch],
  )

  const moveElement = React.useCallback(
    (payload: { elementId: string; source: { id: string }; target: { id: string; index?: number } }) => {
      dispatch({ type: Actions.MoveElement, payload })
      dispatch({ type: Actions.UpdateHistory })
    },
    [dispatch],
  )

  const removeElement = React.useCallback(
    (payload: { elementId: string; selectElementId?: string }) => {
      dispatch({ type: Actions.RemoveElement, payload })
      dispatch({ type: Actions.UpdateHistory })
    },
    [dispatch],
  )

  const removeSubElement = React.useCallback(
    (payload: { elementId: string; selectElementId?: string }) => {
      dispatch({ type: Actions.RemoveSubElement, payload })
    },
    [dispatch],
  )

  const undoRedoElement = React.useCallback(
    (payload: ActionsUndoRedo) => {
      dispatch({ type: Actions.UndoRedoElement, payload })
    },
    [dispatch],
  )

  const selectElement = React.useCallback(
    (payload: string) => {
      dispatch({ type: Actions.SelectElement, payload })
    },
    [dispatch],
  )

  const configElement = React.useCallback(
    (elementId: string, props: Record<string, any>, children?: string[]) => {
      dispatch({ type: Actions.ConfigElement, payload: { elementId, props, children } })
    },
    [dispatch],
  )

  return {
    addElement,
    addSubElement,
    moveElement,
    selectElement,
    configElement,
    undoRedoElement,
    removeElement,
    removeSubElement,
    dispatch,
  }
}
