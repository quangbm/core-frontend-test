import React from 'react'
import styled from 'styled-components/macro'
import { NFile } from '../../../../../components/NFileDropzone/NFile'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { useLayoutBuilderData } from '../../../contexts/LayoutBuilderDataContext'
import { ElementType, ZoneProps } from '../../../utils/models'
import { ComponentIcons } from '../../Icons'
import { TextInput } from '../../Inputs/TextInput'
import { Widget } from '../../Sidebar'
import ElementLayout from '../ElementLayout'
import { EmptyMessage as BaseEmptyMessage } from '../StyledElements'

const LoadingWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
`

const EmptyMessage = styled(BaseEmptyMessage)`
  border-radius: 3px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px dashed #999;
  flex: 1;
`

const AttachmentsWrapper = styled.div`
  display: flex;
  min-height: 210px;
  flex-wrap: wrap;
`

export function Attachments({ elementId, parentId, index, ...restProps }: ZoneProps) {
  const { isLoading, previewData } = useLayoutBuilderData()

  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()
  const element = elements[elementId]

  return (
    <ElementLayout
      {...restProps}
      label="Attachments"
      elementId={elementId}
      parentId={parentId}
      index={index}
      sidebar={
        <>
          <Widget title="Title">
            <TextInput
              value={element.props.title}
              onChange={e => configElement(elementId, { title: e.target.value })}
            />
          </Widget>
          <Widget title="Description">
            <TextInput
              value={element.props.description}
              onChange={e => configElement(elementId, { description: e.target.value })}
            />
          </Widget>
        </>
      }>
      <AttachmentsWrapper>
        {(() => {
          if (isLoading) {
            return (
              <LoadingWrapper>
                <NSpinner size={40} />
              </LoadingWrapper>
            )
          }

          // check media
          const mediaKeys = !!previewData?.$metadata?.media ? Object.keys(previewData.$metadata.media) : []
          if (mediaKeys.length === 0) {
            return <EmptyMessage>No attachments to review</EmptyMessage>
          }

          // display media
          return mediaKeys.map(key => {
            if (!previewData?.$metadata?.media) {
              return <></>
            }
            const media = previewData.$metadata.media[key]
            return <NFile key={`media_${media.id}`} name={key} media={media} review />
          })
        })()}
      </AttachmentsWrapper>
    </ElementLayout>
  )
}

export const widgetAttachments = {
  icon: <ComponentIcons.RecordDetails />,
  label: 'Attachments',
  type: ElementType.Field,
  component: 'Attachments',
  props: {
    title: '',
    description: '',
  },
  children: [],
}
