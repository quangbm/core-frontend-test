import * as React from 'react'
import styled from 'styled-components/macro'
import { FieldSchema } from '../../../../../../../../shared/src/api'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NCodeEditor } from '../../../../../../components/NCodeEditor'
import { CurrencyData } from '../../../../../../components/NCurrency/CurrencyData'
import { NTypography, typography } from '../../../../../../components/NTypography'
import { getDateFromIsoString, getStringFromDate, noop } from '../../../../../../utils/utils'

const Wrapper = styled.div`
  padding: ${spacing('xs')};
  &.is-dragging {
    opacity: 0.2;
  }
`
const Label = styled(NTypography.InputLabel)`
  margin-bottom: ${spacing('xs')};
`
const Value = styled('div')`
  display: flex;
  height: 42px;
  border-bottom: 1px solid ${color('Neutral400')};
  margin-bottom: ${spacing('xxs')};
  align-items: center;
  overflow: auto;

  ${typography('small-bold-text')};
  &.is-error {
    color: ${color('Red700')};
  }
`

const Tag = styled.div`
  background: ${color('Neutral200')};
  border-radius: 3px;
  display: inline-flex;
  align-items: center;
  min-height: 32px;
  max-width: 100%;

  margin-right: ${spacing('xxs')};
  padding-left: ${spacing('sm')};
  padding-right: ${spacing('sm')};

  color: ${color('Neutral700')};
  ${typography('button')}
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`

type FieldItemProps = {
  fieldSchema: FieldSchema
  value: any
  field: string
  extraData?: any
} & React.HTMLAttributes<HTMLDivElement>

export const FieldItem = React.forwardRef<HTMLDivElement, FieldItemProps>(function FieldItem(
  { field, fieldSchema, value, children, extraData, ...divAttributes },
  ref,
) {
  const renderer = () => {
    if (!fieldSchema) {
      return (
        <React.Fragment>
          <Label>{field}</Label>
          <Value className="is-error">This field may get deleted!</Value>
        </React.Fragment>
      )
    }
    switch (fieldSchema.typeName) {
      case 'text':
      case 'numeric':
      case 'relation':
      case 'externalRelation':
      case 'indirectRelation':
        return (
          <React.Fragment>
            <Label>{fieldSchema.displayName}</Label>
            <Value>{value || fieldSchema.defaultValue}</Value>
          </React.Fragment>
        )
      case 'generated':
        const rawValue = value || fieldSchema.defaultValue
        let generatedValue = rawValue
        if (fieldSchema.subType === 'date-time') {
          generatedValue = rawValue && new Date(rawValue).toLocaleString()
        }
        if (fieldSchema.subType === 'timestamp') {
          generatedValue = rawValue && new Date(rawValue * 1000).toLocaleString()
        }
        return (
          <React.Fragment>
            <Label>{fieldSchema.displayName}</Label>
            <Value>{generatedValue}</Value>
          </React.Fragment>
        )
      case 'json':
        return (
          <NCodeEditor
            style={{ maxHeight: 200 }}
            language="json"
            value={JSON.stringify(value || fieldSchema.defaultValue, null, 2)}
            disabled
            label={fieldSchema.displayName}
          />
        )
      case 'dateTime':
        let displayValue = ''
        if (value || fieldSchema.defaultValue) {
          const date = getDateFromIsoString(value || fieldSchema.defaultValue, fieldSchema.subType)
          displayValue = getStringFromDate({
            date,
            type: fieldSchema.subType,
            dateFormat: 'dd/MM/yyyy',
            timeFormat: 'HH:mm:ss',
          })
        }
        return (
          <React.Fragment>
            <Label>{fieldSchema.displayName}</Label>
            <Value>{displayValue}</Value>
          </React.Fragment>
        )
      case 'boolean':
        return (
          <Value>
            <NCheckbox
              checked={value || JSON.parse(fieldSchema.defaultValue || 'false')}
              onChange={noop}
              title={fieldSchema.displayName}
            />
          </Value>
        )
      case 'pickList': {
        if (fieldSchema.subType === 'multi') {
          let data: any[]
          try {
            data = value ? value : fieldSchema.defaultValue ? JSON.parse(fieldSchema.defaultValue) : []
          } catch {
            data = []
          }
          return (
            <React.Fragment>
              <Label>{fieldSchema.displayName}</Label>
              <Value>
                {data.map(tag => (
                  <Tag key={tag}>{tag}</Tag>
                ))}
              </Value>
            </React.Fragment>
          )
        }
        return (
          <React.Fragment>
            <Label>{fieldSchema.displayName}</Label>
            <Value>{value || fieldSchema.defaultValue}</Value>
          </React.Fragment>
        )
      }
      case 'currency': {
        const rawValue = value || fieldSchema.defaultValue || 0
        return (
          <React.Fragment>
            <Label>{fieldSchema.displayName}</Label>
            <Value>
              {value && extraData.currencyCode ? (
                <CurrencyData dataCurrencyCode={extraData.currencyCode} value={rawValue} />
              ) : (
                rawValue
              )}
            </Value>
          </React.Fragment>
        )
      }
      default: {
        return `Unsupported field type { ${fieldSchema.typeName} }`
      }
    }
  }
  return (
    <Wrapper ref={ref} {...divAttributes}>
      {renderer()}
      {children}
    </Wrapper>
  )
})
