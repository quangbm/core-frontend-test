import { NCodeEditor } from '../../../../../components/NCodeEditor'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { ElementType, ZoneProps } from '../../../utils/models'
import { FieldIcons } from '../../Icons'
import { Widget } from '../../Sidebar'
import ElementLayout from '../ElementLayout'

export function ReactComponent({ elementId, parentId, index, ...restProps }: ZoneProps) {
  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()

  const element = elements[elementId]
  const children = element.children || []

  const canDrop =
    element.props.columns && element.props.columns.length > 1 ? children.length < element.props.columns.length : true

  return (
    <ElementLayout
      {...restProps}
      label="React Component"
      elementId={elementId}
      parentId={parentId}
      index={index}
      canDrop={canDrop}
      sidebar={
        <>
          <Widget title="Component">
            <NCodeEditor
              value={element.props.value}
              onChange={e => configElement(elementId, { value: e.target.value })}
            />
          </Widget>
        </>
      }>
      <div style={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        &lt;ReactComponent /&gt; can not preview in builder
      </div>
    </ElementLayout>
  )
}

export const widgetReactComponent = {
  icon: <FieldIcons.Button />,
  label: 'React Component',
  type: ElementType.Field,
  component: 'ReactComponent',
  props: {
    value: '',
  },
  children: [],
}
