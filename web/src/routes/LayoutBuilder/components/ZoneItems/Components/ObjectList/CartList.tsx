import React from 'react'
import styled from 'styled-components/macro'
import { FieldSchema } from '../../../../../../../../shared/src/api'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { NFieldItem } from '../../../../../../components/NFieldItem/NFieldItem'
import { NSpinner } from '../../../../../../components/NSpinner/NSpinner'
import { NTableColumnType } from '../../../../../../components/NTable/NTable'
import { NTypography, typography } from '../../../../../../components/NTypography'

const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  overflow-y: auto;
  padding-top: ${spacing('sm')};
  padding-bottom: ${spacing('sm')};
`
const Card = styled.div<{ onClick?: () => void }>`
  background: ${color('white')};
  padding: ${spacing('sm')};
  min-width: 240px;
  box-shadow: 0px 4px 16px rgb(0 0 0 / 5%), 0px 1px 2px rgb(0 0 0 / 10%);
  margin: 0 10px;
  border-radius: 4px;
  cursor: ${props => (props.onClick ? 'pointer' : 'normal')};

  &:hover {
    box-shadow: 0px 4px 16px rgb(0 0 0 / 15%), 0px 1px 2px rgb(0 0 0 / 20%);
  }
`
const Title = styled(NTypography.Headline)`
  ${typography('h500')};
  margin-bottom: 10px;
`
const Content = styled.div`
  display: flex;
  flex-direction: column;
  padding: 4px 0;

  & > div {
    flex-direction: row;
    justify-content: space-between;
  }

  & > div > div {
    margin-bottom: 0;
    padding-bottom: 0;
  }
`

type CardListProps = {
  data: any[]
  columns: NTableColumnType<any>
  isLoading?: boolean
  onClick?: (data: any) => void
  fields?: FieldSchema[]
}
export const CardList: React.FC<CardListProps> = ({ columns, data, onClick, fields, isLoading }) => {
  return (
    <Wrapper className="card-list">
      {isLoading && <NSpinner size={32} />}
      {data.map(item => (
        <Card
          className="card-list-item"
          onClick={() => {
            onClick && onClick(item)
          }}>
          {columns.length && (
            <>
              <Title className={columns[0].accessor as string}>{item[columns[0].accessor as string]}</Title>
              {fields &&
                columns
                  .filter(c => c.Header)
                  .slice(1)
                  .map(col => (
                    <Content className={col.accessor as string}>
                      <NFieldItem field={fields.find(f => f.name === col.accessor)!} contents={item} />
                    </Content>
                  ))}
            </>
          )}
        </Card>
      ))}
    </Wrapper>
  )
}
