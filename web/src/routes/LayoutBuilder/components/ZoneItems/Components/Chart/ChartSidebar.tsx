import React, { useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import styled from 'styled-components/macro'
import { FieldSchema, ObjectSchemaResponse } from '../../../../../../../../shared/src/api'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../components/NDivider'
import { NFilterBuilder } from '../../../../../../components/NFilterBuilder'
import { SelectOption } from '../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { useSearchText } from '../../../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../../../services/api'
import { Element } from '../../../../utils/models'
import { TextArea } from '../../../Inputs/TextArea'
import { TextInput } from '../../../Inputs/TextInput'
import { Widget } from '../../../Sidebar'

const AdvancedWrapper = styled.div<{ visible: boolean }>`
  display: ${props => (props.visible ? 'block' : 'none')};
  margin: 1em;
`
const SORT_ORDER_OPTIONS: SelectOption[] = [{ value: 'ASC' }, { value: 'DESC' }]

type ChartSidebarProps = {
  element: Element
  configElement(elementId: string, props: Record<string, any>): void
  fieldsData?: ObjectSchemaResponse
}

type Serie = {
  fieldName?: string
  type: string
}

export const ChartSidebar: React.FC<ChartSidebarProps> = ({ element, configElement, fieldsData }) => {
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')
  const [xAdvanced, setXAdvanced] = useState(false)
  const { data: objectsData, isFetching: isLoadingObjects } = useQuery(
    [QueryKeys.Builder.getAllObjects, { searchText }],
    APIServices.Builder.getAllObjects,
    {
      keepPreviousData: true,
      select: data => {
        return data.data.data.map(option => {
          const typeName = option.isExternal ? ' (external)' : ' (internal)'
          return {
            value: option.name,
            label: option.displayName + typeName,
          }
        }) as SelectOption[]
      },
    },
  )

  const fieldsByName = useMemo<{ [key: string]: FieldSchema }>(() => {
    return (
      fieldsData?.fields.reduce((a, f) => {
        return { ...a, [f.name]: f }
      }, {}) || {}
    )
  }, [fieldsData])

  const fieldOptions = useMemo<SelectOption[]>(() => {
    return (
      fieldsData?.fields
        .map<SelectOption>(value => ({
          value: value.name,
          label: value.displayName,
        }))
        .sort((a, b) => (a.value > b.value ? 1 : -1)) || []
    )
  }, [fieldsData])

  const sortByOptions = useMemo<SelectOption[]>(() => {
    return (
      fieldsData?.fields
        .filter(v => v.isSortable)
        .map<SelectOption>(value => ({
          value: value.name,
          label: value.displayName,
        }))
        .sort((a, b) => (a.value > b.value ? 1 : -1)) || []
    )
  }, [fieldsData])

  return (
    <>
      <Widget title="Title">
        <TextInput
          value={element.props.title || ''}
          onChange={e => configElement(element.id, { title: e.target.value })}
        />
      </Widget>
      <Widget title="Description">
        <TextArea
          value={element.props.description || ''}
          onChange={e => configElement(element.id, { description: e.target.value })}
        />
      </Widget>
      <Widget title="Data Source">
        <NSingleSelect
          label="Object"
          options={objectsData || []}
          value={element.props.objName}
          onValueChange={newValue => {
            configElement(element.id, {
              objName: newValue,
              limit: element.props.limit || 10,
              sortBy: undefined,
              series: [{ type: 'bar' }],
              filters: undefined,
            })
          }}
          fullWidth
          isSearchable
          searchValue={searchValue}
          onSearchValueChange={handleSearchChange}
          isLoading={isLoadingObjects}
        />
      </Widget>

      {element.props.series && (
        <>
          <Widget title="Data Options">
            <TextInput
              label="Num. Records"
              type="number"
              value={`${element.props.limit}`}
              onChange={e => configElement(element.id, { limit: +e.target.value })}
            />
            <Widget title="Filters">
              <NFilterBuilder
                filters={element.props.filters}
                fieldNameOptions={fieldOptions}
                fieldsByName={fieldsByName}
                onSubmit={filters => {
                  configElement(element.id, { filters })
                }}>
                <NButton size="small" type="outline-3" block>
                  Show filter control
                </NButton>
              </NFilterBuilder>
            </Widget>
            <NSingleSelect
              label="Sort by"
              options={sortByOptions || []}
              value={element.props.sortBy}
              onValueChange={newValue => {
                if (newValue === '') {
                  configElement(element.id, { sortBy: undefined })
                  return
                }
                configElement(element.id, { sortBy: newValue, sortOrder: 'ASC' })
              }}
              fullWidth
              isLoading={isLoadingObjects}
            />
            {!!element.props.sortBy && (
              <>
                <NDivider size="sm" />
                <NSingleSelect
                  label="Sort order"
                  options={SORT_ORDER_OPTIONS}
                  value={element.props.sortOrder}
                  onValueChange={newValue => {
                    configElement(element.id, { sortOrder: newValue })
                  }}
                  fullWidth
                />
              </>
            )}
          </Widget>
          <Widget title="X Axis">
            <NSingleSelect
              label="Data Field"
              options={fieldOptions}
              onValueChange={newValue => {
                configElement(element.id, { xAxis: newValue })
              }}
              value={element.props.xAxis}
            />
            <AdvancedWrapper visible={xAdvanced}>
              <TextInput
                type="number"
                label="Ticks"
                value={element.props.xAxisTicks || 5}
                onChange={event => configElement(element.id, { xAxisTicks: event.target.value })}
              />
            </AdvancedWrapper>
            <NButton type={'link'} onClick={() => setXAdvanced(!xAdvanced)}>
              {xAdvanced ? 'Hide' : 'Show'} Advanced
            </NButton>
          </Widget>
          {element.props.series?.map((series: Serie, index: number) => (
            <Widget title={`Serie ${index + 1}`}>
              <NSingleSelect
                label="Data Field"
                options={fieldOptions}
                value={series.fieldName}
                onValueChange={value => {
                  series.fieldName = value
                  configElement(element.id, { series: element.props.series })
                }}
              />
              <NSingleSelect
                label={'Type'}
                options={[
                  {
                    value: 'line',
                    label: 'Line',
                  },
                  {
                    value: 'bar',
                    label: 'Bar',
                  },
                  {
                    value: 'area',
                    label: 'Area',
                  },
                ]}
                value={series.type}
                onValueChange={value => {
                  series.type = value || 'bar'
                  configElement(element.id, { series: element.props.series })
                }}
              />
            </Widget>
          ))}
          <Widget>
            <NButton
              type="primary"
              onClick={() => {
                configElement(element.id, { series: [...element.props.series, { type: 'bar' }] })
              }}>
              Add Another Serie
            </NButton>
          </Widget>
        </>
      )}
    </>
  )
}
