import * as React from 'react'
import { useQuery } from 'react-query'
import {
  Area,
  Bar,
  CartesianGrid,
  ComposedChart,
  Legend,
  Line,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'
import { APIServices, QueryKeys } from '../../../../../../services/api'
import { chartPalette } from '../../../../../../utils/chart-palette'
import { useLayoutAction, useLayoutBuilder } from '../../../../contexts/LayoutBuilderContext'
import { useLayoutBuilderData } from '../../../../contexts/LayoutBuilderDataContext'
import { resolveFilterVariable } from '../../../../utils/builder'
import { ElementType, ZoneProps } from '../../../../utils/models'
import { FieldIcons } from '../../../Icons'
import ElementLayout from '../../ElementLayout'
import { ItemDescription, ItemHeader, ItemName } from '../ObjectList/ObjectList'
import { ChartSidebar } from './ChartSidebar'

export const SERIE_TYPE = ['line', 'bar']

export const Chart = React.memo(function Node({ elementId, ...restProps }: ZoneProps) {
  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()
  const { inputMap } = useLayoutBuilderData()
  const element = elements[elementId]

  const isDisabled = !element.props.objName
  const isFiltered = element.props.filters && element.props.filters.length > 0

  const { data: fieldsData } = useQuery(
    [QueryKeys.Builder.getObjectSchema, { objName: element.props.objName }],
    APIServices.Builder.getObjectSchema,
    {
      enabled: !isDisabled,
    },
  )

  const mapFilters = resolveFilterVariable(element.props.filters || [], inputMap)

  // get content list
  const { data: objectData } = useQuery(
    [
      QueryKeys.Data.searchObjectData,
      {
        name: element.props.objName,
        limit: element.props.limit,
        filters: mapFilters,
        sortBy: element.props.sortBy,
        sortOrder: element.props.sortBy ? element.props.sortOrder : undefined,
      },
    ],
    APIServices.Data.searchObjectData,
    {
      keepPreviousData: true,
      enabled: !!element.props.objName && !isFiltered,
    },
  )

  return (
    <ElementLayout
      label="Chart"
      elementId={elementId}
      {...restProps}
      sidebar={<ChartSidebar element={element} configElement={configElement} fieldsData={fieldsData?.data} />}>
      <ItemHeader>
        <ItemName>{element.props.title || `${fieldsData?.data.displayName}`}</ItemName>
        <ItemDescription>{element.props.description}</ItemDescription>
      </ItemHeader>
      <ResponsiveContainer width={'100%'} height={350}>
        <ComposedChart data={objectData?.data.data || []}>
          <XAxis dataKey={element.props.xAxis} />
          <YAxis />
          {element.props.series?.map((series: any, index: number) => {
            switch (series.type) {
              case 'line':
                return <Line dataKey={series.fieldName} fill={chartPalette(index)} />
              case 'area':
                return <Area dataKey={series.fieldName} fill={chartPalette(index)} />
              default:
                return <Bar dataKey={series.fieldName} fill={chartPalette(index)} />
            }
          })}
          <Tooltip />
          <Legend />
          <CartesianGrid strokeDasharray="3 3" />
        </ComposedChart>
      </ResponsiveContainer>
    </ElementLayout>
  )
})

export const widgetChart = {
  icon: <FieldIcons.Chart />,
  label: 'Chart',
  type: ElementType.Field,
  component: 'Chart',
  props: {
    limit: 10,
    objName: '',
    sortBy: undefined,
    sortOrder: 'ASC',
  },
  children: [],
}
