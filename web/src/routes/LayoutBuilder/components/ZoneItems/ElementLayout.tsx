import * as React from 'react'
import styled from 'styled-components/macro'
import { classnames } from '../../../../common/classnames'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NCodeEditor } from '../../../../components/NCodeEditor'
import { NTabs as BaseNTabs } from '../../../../components/NTabs/NTabs'
import { typography } from '../../../../components/NTypography'
import { Portal } from '../../../../components/Portal'
import { useCombinedRefs } from '../../../../hooks/useCombinedRefs'
import { ReactComponent as Icon } from '../../assets/icons/Element.svg'
import { useLayoutAction, useLayoutBuilder } from '../../contexts/LayoutBuilderContext'
import { useElement } from '../../hooks/useElement'
import { ZoneProps } from '../../utils/models'
import { sidebarPortalRef } from '../RightSidebar'
import { Placeholder } from './StyledElements'

const Actions = styled('div')`
  position: absolute;
  right: -1px;
  bottom: 100%;
  background: ${color('Primary700')};
  color: ${color('white')};
  padding: ${spacing('xs')};
  border-radius: 1px 1px 0 0;
  display: none;
  align-items: center;
  span {
    margin: 0 ${spacing('xs')};
    ${typography('button')};
    color: ${color('white')};
  }
  div {
    ${typography('small-overline')}
    cursor: pointer;
    &:not(:last-child) {
      margin-right: ${spacing('xs')};
    }
  }
`

export const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
  background: ${color('white')};
  position: relative;
  min-height: 200px;
  padding: ${spacing('md')};
  border-radius: 3px;
  border: 1px solid transparent;
  &.is-over:not(.is-sort) {
    background: ${color('Primary200')};
  }
  &.is-over,
  &.is-selected {
    border-top-right-radius: 0;
    border-color: ${color('Primary700')};
    > ${Actions} {
      display: flex;
    }
  }
  &.is-sort {
    ${Placeholder} {
      display: block;
    }
  }
`

type ElementLayoutProps = {
  children: React.ReactNode
  sidebar: React.ReactNode
  label: string
  className?: string
  onClick?(e: React.MouseEvent<HTMLDivElement>): void
} & ZoneProps

const ElementLayout = React.forwardRef<HTMLDivElement, ElementLayoutProps>(function ElementLayout(
  { className, children, sidebar, label, onClick, canRemove = true, onRemove, ...elementProps },
  forwardRef,
) {
  const { selectedElement } = useLayoutBuilder()
  const { selectElement, removeElement } = useLayoutAction()
  const [{ isOver, mousePosition }, ref] = useElement(elementProps)
  const isSelected = selectedElement === elementProps.elementId
  const combinedRef = useCombinedRefs([ref, forwardRef])
  return (
    <React.Fragment>
      <Wrapper
        ref={combinedRef}
        className={classnames([
          className,
          isOver && 'is-over',
          mousePosition && `is-sort ${mousePosition}`,
          isSelected && 'is-selected',
        ])}
        onClick={e => {
          e.stopPropagation()
          selectElement(elementProps.elementId)
          if (onClick) {
            onClick(e)
          }
        }}>
        {children}
        <Placeholder mousePosition={mousePosition} />
        <Actions>
          <Icon />
          <span>{label}</span>
          {canRemove && (
            <div
              onClick={() => {
                if (onRemove) {
                  onRemove()
                  return
                }
                removeElement({ elementId: elementProps.elementId })
              }}>
              Delete
            </div>
          )}
        </Actions>
      </Wrapper>
      <Portal mountNode={sidebarPortalRef}>
        {isSelected && (
          <div
            onClick={e => {
              e.stopPropagation()
            }}>
            <Sidebar elementId={elementProps.elementId}>{sidebar}</Sidebar>
          </div>
        )}
      </Portal>
    </React.Fragment>
  )
})

const NTabs = styled(BaseNTabs)`
  .tab-list li {
    height: unset;
    padding: 16px;
    margin: 0;
  }
`

function Sidebar({ children, elementId }: { children: React.ReactNode; elementId: string }) {
  const { elements } = useLayoutBuilder()
  const { configElement } = useLayoutAction()
  const element = elements[elementId]
  return (
    <React.Fragment>
      <NTabs showUnderlineBg>
        <NTabs.Tab title="Properties">{children}</NTabs.Tab>
        <NTabs.Tab title="Custom CSS">
          <div style={{ padding: 16 }}>
            <NCodeEditor
              autoFocus
              style={{ maxHeight: 300 }}
              language="css"
              value={element.props.customCss || ''}
              onChange={e => configElement(elementId, { customCss: e.target.value })}
            />
          </div>
        </NTabs.Tab>
      </NTabs>
    </React.Fragment>
  )
}

export default ElementLayout
