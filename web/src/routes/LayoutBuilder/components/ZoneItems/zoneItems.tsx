import { LayoutResponse } from '../../../../../../shared/src/api'
import { Header, widgetHeader } from '../../demo/Header'
import { ElementType, ZoneProps } from '../../utils/models'
import { Attachments, widgetAttachments } from './Components/Attachments'
import { Button, widgetButton } from './Components/Button'
import { Chart, widgetChart } from './Components/Chart/Chart'
import { ObjectList, widgetObjectList } from './Components/ObjectList/ObjectList'
import { RelatedList, widgetRelatedList } from './Components/ObjectList/RelatedList'
import { ReactComponent, widgetReactComponent } from './Components/ReactComponent'
import { RecordDetails as RecordDetail, widgetRecordDetails } from './Components/RecordDetails/RecordDetails'
import { Checkbox, widgetCheckbox } from './Fields/Checkbox'
import { Heading, widgetHeading } from './Fields/Heading'
import { Input, widgetInput } from './Fields/Input'
import { Link, widgetLink } from './Fields/Link'
import { Paragraph, widgetParagraph } from './Fields/Paragraph'
import { Radio, widgetRadio } from './Fields/Radio'
import { Accordion, widgetAccordion } from './Layouts/Accordion'
import { Section, widgetSection } from './Layouts/Section'
import { Tabs, widgetTabs } from './Layouts/Tabs'

type Widget = {
  icon: JSX.Element
  label: string
  component: string
  type: ElementType
  props: Record<string, any>
  children?: string[]
}

export const GROUPS = ['Layout', 'Basic', 'Component'] as const
export type GroupName = typeof GROUPS[number]

export function getWidgetByGroups(layoutType?: LayoutResponse['type']): Record<GroupName, Widget[]> {
  if (!layoutType) {
    return {
      Layout: [],
      Basic: [],
      Component: [],
    }
  }
  if (layoutType === 'app-page') {
    return {
      Layout: [widgetSection, widgetTabs],
      Basic: [widgetHeading, widgetParagraph],
      Component: [widgetObjectList, widgetButton, widgetHeader, widgetChart, widgetReactComponent],
    }
  }
  return {
    Layout: [widgetSection, widgetTabs],
    Basic: [widgetHeading, widgetParagraph],
    Component: [
      widgetObjectList,
      widgetRecordDetails,
      widgetRelatedList,
      widgetButton,
      widgetHeader,
      widgetAttachments,
      widgetChart,
      widgetReactComponent,
    ],
  }
}

export const ZONE_ITEMS = {
  Section: widgetSection,
  Tabs: widgetTabs,
  Accordion: widgetAccordion,
  Input: widgetInput,
  Heading: widgetHeading,
  Paragraph: widgetParagraph,
  Checkbox: widgetCheckbox,
  Link: widgetLink,
  Radio: widgetRadio,
  ObjectList: widgetObjectList,
  RecordDetail: widgetRecordDetails,
  RelatedList: widgetRelatedList,
  Button: widgetButton,
  DemoHeader: widgetHeader,
  Attachments: widgetAttachments,
  Chart: widgetChart,
  ReactComponent: widgetReactComponent,
} as Record<string, any>

export const ZONE_ELEMENTS = {
  Section,
  Accordion,
  Tabs,
  Input,
  Heading,
  Paragraph,
  Button,
  Checkbox,
  Link,
  Radio,
  ObjectList,
  RecordDetail,
  RelatedList,
  DemoHeader: Header,
  Attachments,
  Chart,
  ReactComponent,
} as Record<string, (props: ZoneProps) => JSX.Element>
