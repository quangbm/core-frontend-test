import { nanoid } from 'nanoid'
import * as React from 'react'
import styled from 'styled-components/macro'
import { classnames } from '../../../../../common/classnames'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { GlobalIcons, Icons } from '../../../../../components/Icons'
import { NCheckbox } from '../../../../../components/NCheckbox/NCheckbox'
import { SelectOption } from '../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { NSortableList } from '../../../../../components/NSortableList'
import { TabType } from '../../../../../components/NTabs/model'
import { NTabs } from '../../../../../components/NTabs/NTabs'
import { typography } from '../../../../../components/NTypography'
import { useLayoutAction, useLayoutBuilder } from '../../../contexts/LayoutBuilderContext'
import { useElement } from '../../../hooks/useElement'
import { useElementRenderer } from '../../../hooks/useElementRenderer'
import { ElementType, ZoneProps } from '../../../utils/models'
import { LayoutIcons } from '../../Icons'
import { TextInput } from '../../Inputs/TextInput'
import { Widget } from '../../Sidebar'
import ElementLayout, { Wrapper } from '../ElementLayout'
import { EmptyMessage as BaseEmptyMessage } from '../StyledElements'
import { widgetSection } from './Section'

// tab type options
const TabTypeOptions: SelectOption[] = Object.keys(TabType).map(type => {
  //@ts-ignore
  const value = TabType[type] as string
  return {
    value,
    label: type,
  }
})

const TabListItem = styled('div')`
  display: flex;
  align-items: center;
  position: relative;
  cursor: move;
  &:not(:last-child) {
    margin-bottom: 2px;
  }
  & > svg {
    position: absolute;
    right: 100%;
    display: none;
  }
  &:hover > svg {
    display: block;
  }
`

const TabListItemAction = styled('div')`
  ${typography('small-overline')};
  color: ${color('Primary700')};
  cursor: pointer;
  margin-left: ${spacing('xs')};
`

const EmptyMessage = styled(BaseEmptyMessage)`
  border-radius: 3px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px dashed #999;
  flex: 1;
`

function getNewTab(index?: number) {
  const { icon: _icon, ...tab } = widgetSection
  return {
    id: nanoid(),
    ...tab,
    props: {
      ...tab.props,
      title: `Tab ${index || 0}`,
    },
  }
}

export function Tabs({ elementId, parentId, index, ...restProps }: ZoneProps) {
  const { elements } = useLayoutBuilder()
  const { selectElement, addElement, configElement, removeElement } = useLayoutAction()

  const element = elements[elementId]

  const elementChildren = element.children || []

  const [active, setActive] = React.useState(0)

  const tabId = elementChildren[active]

  const handleAddNewTab = () => {
    const newTab = getNewTab(elementChildren.length + 1)
    addElement({ element: newTab, source: { id: elementId }, target: { id: elementId }, selectElementId: elementId })
  }

  const handleSort = (fromIndex: number, toIndex: number) => {
    const newChildren = [...elementChildren]
    ;[newChildren[toIndex], newChildren[fromIndex]] = [newChildren[fromIndex], newChildren[toIndex]]
    configElement(elementId, {}, newChildren)
  }

  return (
    <ElementLayout
      {...restProps}
      label="Tabs"
      elementId={elementId}
      parentId={parentId}
      index={index}
      canDrop={false}
      sidebar={
        <React.Fragment>
          <Widget title="Tabs" rightElement={<GlobalIcons.Plus onClick={handleAddNewTab} />}>
            <NSortableList onSort={handleSort}>
              {elementChildren.map(id => {
                const tab = elements[id]
                const handleChangeTitle = (e: React.ChangeEvent<HTMLInputElement>) => {
                  configElement(id, { title: e.target.value })
                }
                const handleRemove = (e: React.MouseEvent<HTMLDivElement>) => {
                  e.stopPropagation()
                  removeElement({ elementId: id, selectElementId: elementId })
                }
                return (
                  <TabListItem key={id}>
                    <Icons.Reorder />
                    <TextInput type="text" value={tab.props.title} onChange={handleChangeTitle} />
                    {elementChildren.length > 1 && <TabListItemAction onClick={handleRemove}>Delete</TabListItemAction>}
                  </TabListItem>
                )
              })}
            </NSortableList>
          </Widget>
          <Widget title="Tab Type">
            <NSingleSelect
              options={TabTypeOptions}
              value={element.props.type}
              onValueChange={newValue => {
                configElement(element.id, { type: newValue })
              }}
            />
          </Widget>
          <Widget>
            <NCheckbox
              title="Show underline"
              checked={element.props.showUnderlineBg}
              onChange={e => {
                configElement(elementId, { showUnderlineBg: e.target.checked })
              }}
            />
          </Widget>
        </React.Fragment>
      }>
      <NTabs
        active={active}
        onChangeTab={setActive}
        type={element.props.type}
        showUnderlineBg={element.props.showUnderlineBg}>
        {elementChildren.map((id, idx) => {
          const tab = elements[id]
          return (
            <NTabs.Tab title={tab.props.title} key={`tab-${idx}`}>
              <Tab
                elementId={tabId}
                parentId={elementId}
                index={active}
                canDrag={elementChildren.length > 1}
                onClick={() => selectElement(tabId)}
              />
            </NTabs.Tab>
          )
        })}
      </NTabs>
    </ElementLayout>
  )
}

const TabWrapper = styled(Wrapper)`
  flex-direction: column;
  border-color: ${color('Primary700')};
  border-top-left-radius: 0;
  & > *:not(:last-child) {
    margin-bottom: ${spacing('md')};
  }
`

function Tab({ elementId, parentId, index, canDrag = true, canDrop = true, onClick }: ZoneProps & { onClick(): void }) {
  const [{ isOver }, ref] = useElement({ elementId, parentId, index, canDrag, canDrop })
  const children = useElementRenderer(elementId, { canDrop })
  return (
    <TabWrapper className={classnames([isOver && 'is-over'])} ref={ref} onClick={onClick}>
      {children || <EmptyMessage>Drop here to start</EmptyMessage>}
    </TabWrapper>
  )
}

export const widgetTabs = {
  icon: <LayoutIcons.Tab />,
  label: 'Tab',
  type: ElementType.Layout,
  component: 'Tabs',
  props: {
    type: TabType.Default,
    showUnderlineBg: false,
  },
  children: [],
}
