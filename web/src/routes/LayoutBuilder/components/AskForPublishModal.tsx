import * as React from 'react'
import styled from 'styled-components/macro'
import { color, spacing } from '../../../components/GlobalStyle'
import { NDivider } from '../../../components/NDivider'
import { NModal, NModalProps } from '../../../components/NModal/NModal'
import { typography } from '../../../components/NTypography'

const BodyWrapper = styled('div')`
  padding: ${spacing('xl')};
`

const Title = styled('div')`
  ${typography('h400')}
`

const Description = styled('div')`
  ${typography('small-bold-text')}
  color: ${color('Neutral500')}
`

type AskForPublishModalProps = { name: string; children: React.ReactNode } & NModalProps

export function AskForPublishModal({ visible, setVisible, name, children }: AskForPublishModalProps) {
  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header title={`Publish: ${name}`} onClose={() => setVisible && setVisible(false)} />
      <NModal.Body>
        <BodyWrapper>
          <Title>Publish this page to visible to use</Title>
          <NDivider size="xl" />
          <Description>
            Publish the page now, or do it later using the Publish button in the App Builder toolbar
          </Description>
        </BodyWrapper>
      </NModal.Body>
      <NModal.Footer>{children}</NModal.Footer>
    </NModal>
  )
}
