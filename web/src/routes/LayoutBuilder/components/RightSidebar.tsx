import * as React from 'react'
import styled from 'styled-components/macro'
import { color } from '../../../components/GlobalStyle'

export const sidebarPortalRef = React.createRef<HTMLDivElement>()
const Wrapper = styled.div`
  width: 250px;
  height: 100%;
  background: ${color('white')};
  overflow-y: auto;
`

type RightSidebarProps = {
  children?: React.ReactNode
}

export function RightSidebar({}: RightSidebarProps) {
  return <Wrapper ref={sidebarPortalRef} />
}
