import React, { FC, useEffect, useState } from 'react'
import { Controller, FormProvider, useForm, useFormContext } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components/macro'
import { ApplicationResponse, AppProfileDto } from '../../../../../shared/src/api'
import { color, spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'
import { NButton } from '../../../components/NButton/NButton'
import { NCheckbox } from '../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../components/NDivider'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { NPerfectScrollbar } from '../../../components/NPerfectScrollbar'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { NTextInput } from '../../../components/NTextInput/NTextInput'
import { NToast } from '../../../components/NToast'
import { typography } from '../../../components/NTypography'
import { useSearchText } from '../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../services/api'
import { noop } from '../../../utils/utils'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`

const AppWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  cursor: pointer;
  border-radius: 6px;
  padding: 0 ${spacing('sm')};
  border: 1px solid ${color('Neutral700')};

  &:hover {
    background-color: ${color('Neutral200')};
  }
`

const AppIcon = styled.div<{ isExpanded?: boolean }>`
  width: 40px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: rotate(${props => (props.isExpanded ? '90deg' : '0deg')});
`

const ProfilesWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 6px;
  border: 1px solid ${color('Primary700')};
  padding: ${spacing('sm')};
`

const PlaceholderWrapper = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: ${spacing('xl')};
`

const ProfileItem = styled('div')`
  display: flex;
  margin-top: ${spacing('xxs')};
  padding: ${spacing('sm')} ${spacing('md')};
  cursor: pointer;
  &:hover {
    background: ${color('Neutral100')};
  }
`

const ProfileItemName = styled('div')`
  flex: 1;
  margin-left: ${spacing('md')};
  ${typography('small-bold-text')}
`

type AppProfileSelectItemProps = {
  isExpanded: boolean
  setSelectedAppId: (appId?: string) => void
  app: ApplicationResponse
  defaultValues?: Record<string, boolean>
}
const AppProfileSelectItem: FC<AppProfileSelectItemProps> = ({ app, isExpanded, setSelectedAppId, defaultValues }) => {
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const theme = useTheme()
  const { control, watch } = useFormContext()

  const { data: profilesData, isFetching: isLoadingProfiles } = useQuery(
    [
      QueryKeys.Profiles.getProfiles,
      {
        searchText: searchText,
      },
    ],
    APIServices.Profiles.getProfiles,
  )

  const watchProfilesData: ObjAppsProfiles | undefined = watch('profilesData')
  const totalSelectedProfiles = profilesData
    ? profilesData.data.data.reduce((prev, cur) => {
        if (!watchProfilesData || !watchProfilesData[app.id]) {
          return prev
        }

        if (watchProfilesData[app.id][cur.id]) {
          return prev + 1
        }

        return prev
      }, 0)
    : 0

  return (
    <Wrapper>
      <AppWrapper
        onClick={() => {
          setSelectedAppId(isExpanded ? undefined : app.id)
        }}>
        <AppIcon isExpanded={isExpanded}>
          <GlobalIcons.MoreRight />
        </AppIcon>
        <div>{app.name}</div>
        <div style={{ flex: 1 }} />
        {isLoadingProfiles ? (
          <NSpinner size={20} strokeWidth={2} color={color('Primary900')({ theme })} />
        ) : (
          <div>({totalSelectedProfiles} profiles)</div>
        )}
      </AppWrapper>
      {isExpanded && (
        <ProfilesWrapper>
          <NTextInput
            left={<GlobalIcons.Search />}
            placeholder="Search for profile"
            value={searchValue}
            onChange={e => {
              handleSearchChange(e.target.value)
            }}
          />
          {isLoadingProfiles ? (
            <PlaceholderWrapper>
              <NSpinner size={20} strokeWidth={2} color={color('Primary900')({ theme })} />
            </PlaceholderWrapper>
          ) : (
            <>
              {profilesData?.data.data && profilesData?.data.data.length > 0 ? (
                profilesData.data.data.map(profile => (
                  <Controller
                    name={`profilesData.${app.id}.${profile.id}`}
                    control={control}
                    defaultValue={defaultValues ? defaultValues[profile.id] : undefined}
                    render={({ field: { value, onChange } }) => {
                      return (
                        <ProfileItem
                          key={profile.id}
                          onClick={() => {
                            onChange(!value)
                          }}>
                          <NCheckbox
                            value={profile.id}
                            checked={!!value}
                            onChange={noop}
                            onClick={e => e.stopPropagation()}
                          />
                          <ProfileItemName>{profile.name}</ProfileItemName>
                        </ProfileItem>
                      )
                    }}
                  />
                ))
              ) : (
                <PlaceholderWrapper>No profile found</PlaceholderWrapper>
              )}
            </>
          )}
        </ProfilesWrapper>
      )}
    </Wrapper>
  )
}

/////////////////////////////////////

const StyledButton = styled(NButton)`
  width: 100%;
`

type ObjAppsProfiles = Record<string, Record<string, boolean>>

type AppProfileSelectProps = {}
export const AppProfileSelect: FC<AppProfileSelectProps> = () => {
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  const params = useParams<{ layoutId: string }>()
  const theme = useTheme()
  const [selectedAppId, setSelectedAppId] = useState<string | undefined>(undefined)
  const queryClient = useQueryClient()

  const formMethods = useForm({ shouldUnregister: false })
  const {
    formState: { isDirty },
    reset,
    handleSubmit,
  } = formMethods

  const { data: appsData, isLoading: isLoadingAppsData } = useQuery(
    [
      QueryKeys.Builder.getApps,
      {
        searchText: searchText,
      },
    ],
    APIServices.Builder.getApps,
  )

  const { data: layoutAppsProfiles, isLoading: isLoadingAppsProfiles } = useQuery(
    [QueryKeys.Builder.getLayoutAppsProfiles, { id: params.layoutId }],
    APIServices.Builder.getLayoutAppsProfiles,
    {
      select: data => {
        return data.data.reduce((prev, cur) => {
          const newPrev = { ...prev }
          newPrev[cur.application.id] = newPrev[cur.application.id] || {}
          newPrev[cur.application.id][cur.profile.id] = true
          return newPrev
        }, {} as ObjAppsProfiles)
      },
    },
  )

  const { mutate: assignLayoutAppsProfiles, isLoading: isAssigning } = useMutation(
    APIServices.Builder.assignLayoutAppsProfiles,
    {
      onSuccess: () => {
        queryClient.invalidateQueries([QueryKeys.Builder.getLayoutAppsProfiles, { id: params.layoutId }])
      },
      onError: error => {
        NToast.error({
          title: 'Assign unsuccessful',
          subtitle: error.response?.data.message,
        })
      },
    },
  )

  useEffect(() => {
    if (!layoutAppsProfiles) {
      return
    }

    reset({ profilesData: layoutAppsProfiles })
  }, [layoutAppsProfiles, reset])

  const onSubmit = (data: { profilesData: ObjAppsProfiles }) => {
    const newAssignedAppsProfiles: AppProfileDto[] = []
    const { profilesData } = data
    const appIds = Object.keys(profilesData)
    appIds.map(appId => {
      const profileIds = Object.keys(profilesData[appId])
      profileIds.map(profileId => {
        if (profilesData[appId][profileId]) {
          newAssignedAppsProfiles.push({
            appId,
            profileId,
          })
        }
        return null
      })
      return null
    })
    assignLayoutAppsProfiles({ id: params.layoutId, input: newAssignedAppsProfiles })
  }

  const onReset = () => {
    reset({
      profilesData: layoutAppsProfiles,
    })
  }

  const isLoading = isLoadingAppsData || isLoadingAppsProfiles

  return (
    <>
      <FormProvider {...formMethods}>
        <NPerfectScrollbar style={{ maxHeight: 400 }}>
          <NTextInput
            left={<GlobalIcons.Search />}
            placeholder="Search for application"
            value={searchValue}
            onChange={e => {
              handleSearchChange(e.target.value)
            }}
          />
          {isLoading && (
            <PlaceholderWrapper>
              <NSpinner size={20} strokeWidth={2} color={color('Primary900')({ theme })} />
            </PlaceholderWrapper>
          )}
          {appsData?.data &&
            appsData.data.data.map(app => {
              return (
                <>
                  <NDivider size="xs" />
                  <AppProfileSelectItem
                    key={app.id}
                    app={app}
                    isExpanded={app.id === selectedAppId}
                    setSelectedAppId={setSelectedAppId}
                    defaultValues={layoutAppsProfiles ? layoutAppsProfiles[app.id] : undefined}
                  />
                </>
              )
            })}
        </NPerfectScrollbar>
        <NDivider size="xs" />
        <NRow gutter={16}>
          <NColumn flex={1}>
            <StyledButton type="primary" loading={isAssigning} disabled={!isDirty} onClick={handleSubmit(onSubmit)}>
              Update
            </StyledButton>
          </NColumn>
          <NColumn flex={1}>
            <StyledButton
              loading={isAssigning}
              disabled={!isDirty}
              onClick={() => {
                onReset()
              }}>
              Reset
            </StyledButton>
          </NColumn>
        </NRow>
      </FormProvider>
    </>
  )
}
