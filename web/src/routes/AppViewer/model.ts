import { SortOrder } from '../../../../shared/src/api'

export type CustomCss = { customCss?: string }

export type SortType = {
  sortBy: string
  sortOrder: SortOrder
}

export type SchemaFieldtype = {
  displayName: string
  name: string
  description?: string
  isRequired: boolean
  value?: any
  defaultValue?: string
  isSortable?: boolean
  isSearchable?: boolean
  typeName: string
  subType?: string
}

export enum LayoutType {
  Tab = 'TabLayout',
  Detail = 'DetailLayout',
  Create = 'CreateLayout',
  Edit = 'EditLayout',
}

export const ComponentContainerClassName = 'component-container'
