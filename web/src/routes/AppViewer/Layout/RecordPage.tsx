import React from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { useSession } from '../../../hooks/useSession'
import { APIServices, QueryKeys } from '../../../services/api'
import { Element } from '../../LayoutBuilder/utils/models'
import { ErrorContainer, ErrorSubtitle, ErrorTitle } from '../components/ErrorComponents'
import { LayoutViewerProvider } from '../contexts/LayoutViewerContext'
import { LayoutViewerRoot } from './LayoutViewerRoot'

export enum RecordPageType {
  Detail = 'RECORD_DETAIL_PAGE',
  Form = 'RECORD_FORM_PAGE',
}

export const RecordPage = () => {
  const { objectName } = useParams<{ id: string | undefined; objectName: string }>()
  const [{ data: sessionData }] = useSession()

  // const queryFunction =
  //   mode === RecordPageType.Detail ? APIServices.Layout.getObjectLayout : APIServices.Layout.getObjectMutationLayout
  // const queryKey =
  //   mode === RecordPageType.Detail ? LayoutQueryKeys.getObjectLayout : LayoutQueryKeys.getObjectMutationLayout
  // get template
  const {
    data: templateData,
    isLoading: isLoadingTemplate,
    error: getLayoutError,
  } = useQuery(
    [QueryKeys.Layout.getObjectLayout, { objName: objectName }, `${sessionData?.lastUsedAppId}`],
    APIServices.Layout.getObjectLayout,
  )

  if (getLayoutError && getLayoutError.response) {
    return (
      <ErrorContainer>
        <ErrorTitle>Error get form layout</ErrorTitle>
        <ErrorSubtitle>Code {getLayoutError.response.data.statusCode}</ErrorSubtitle>
        <ErrorSubtitle>{getLayoutError.response.data.message}</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  if (isLoadingTemplate) {
    return (
      <ErrorContainer>
        <NSpinner size={16} strokeWidth={2} />
      </ErrorContainer>
    )
  }

  if (!templateData) {
    return (
      <ErrorContainer>
        <ErrorTitle>Error get form layout</ErrorTitle>
        <ErrorSubtitle>Missing layout script</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  return (
    <LayoutViewerProvider
      elements={templateData.data.script as Record<string, Element>}
      layoutId={templateData.data.id}
      layoutType={RecordPageType.Detail}
      objectName={objectName}>
      <LayoutViewerRoot />
    </LayoutViewerProvider>
  )
}
