import * as currency from '@dinero.js/currencies'
import { invert } from 'lodash'
import * as React from 'react'
import { useMemo } from 'react'
import { FormProvider, useForm, useFormContext } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useHistory, useLocation, useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { FieldSchema, ObjectLayoutResponse, SessionResponse } from '../../../../../shared/src/api'
import { NButton } from '../../../components/NButton/NButton'
import { NFileType } from '../../../components/NFileDropzone/NFileType'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { NToast } from '../../../components/NToast'
import { useSession } from '../../../hooks/useSession'
import { APIServices, QueryKeys } from '../../../services/api'
import { dineroFromFloat } from '../../../utils/currency-utils'
import { Element } from '../../LayoutBuilder/utils/models'
import { ErrorContainer, ErrorSubtitle, ErrorTitle } from '../components/ErrorComponents'
import { LayoutViewerProvider, useLayoutViewer } from '../contexts/LayoutViewerContext'
import { LayoutType } from '../model'
import { LayoutViewerRoot } from './LayoutViewerRoot'

const ActionsWrapper = styled('div')`
  display: flex;
  justify-content: flex-end;
  margin-top: 8px;
  background: white;
  padding: 8px;
  border-radius: 4px;
  & > :not(:last-child) {
    margin-right: 8px;
  }
`

type RecordPageMutationProps = {
  children?: React.ReactNode
}

export function RecordPageMutation({}: RecordPageMutationProps) {
  const params = useParams<{ id?: string; objectName: string }>()
  const location = useLocation()
  const urlSearch = React.useMemo(() => new URLSearchParams(location.search), [location.search])

  // required fields
  const layoutId = urlSearch.get('layoutId')
  const componentId = urlSearch.get('componentId')

  // related list fields
  const relatedId = urlSearch.get('relatedId')
  const relatedField = urlSearch.get('relatedField')

  const guid = params.id
  const objectName = params.objectName

  // Calculate default relation values if exist
  const defaultValues = React.useMemo(() => {
    if (relatedField && relatedId) {
      const paths = relatedField.split('.')
      return {
        // Assumed relatedField follow format `objectName.relatedFieldName`
        [paths[paths.length - 1]]: relatedId,
      }
    }
    const result = {} as Record<string, any>
    urlSearch.forEach((value, key) => {
      // skip required fields and related fields
      if (key === 'layoutId' || key === 'componentId' || key === 'relatedId' || key === 'relatedField') {
        return
      }
      result[key] = value
    })

    return result
  }, [relatedField, relatedId, urlSearch])

  const formMethods = useForm({ defaultValues })

  const { mutate: getLayoutScript, data, isLoading, error } = useMutation(APIServices.Layout.triggers)

  React.useEffect(() => {
    if (layoutId && componentId) {
      if (guid) {
        getLayoutScript({
          layoutId,
          componentId,
          guid,
        })
        return
      }
      getLayoutScript({
        layoutId,
        componentId,
      })
    }
  }, [layoutId, componentId, getLayoutScript, guid])

  if (!layoutId || !componentId) {
    return (
      <ErrorContainer>
        <ErrorTitle>Error get form layout</ErrorTitle>
        <ErrorSubtitle>Missing required field!</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  if (isLoading) {
    return (
      <ErrorContainer>
        <NSpinner size={16} strokeWidth={2} />
      </ErrorContainer>
    )
  }

  if (error && error.response) {
    return (
      <ErrorContainer>
        <ErrorTitle>Error get form layout</ErrorTitle>
        <ErrorSubtitle>Code {error.response.data.statusCode}</ErrorSubtitle>
        <ErrorSubtitle>{error.response.data.message}</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  if (!data?.data) {
    return (
      <ErrorContainer>
        <ErrorTitle>Error get form layout</ErrorTitle>
        <ErrorSubtitle>Missing layout script</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  const layoutType = guid ? LayoutType.Edit : LayoutType.Create
  const elements = (data.data as ObjectLayoutResponse).script as Record<string, Element>

  return (
    <FormProvider {...formMethods}>
      <LayoutViewerProvider elements={elements} layoutId={layoutId} layoutType={layoutType} objectName={objectName}>
        <RecordMutationForm />
      </LayoutViewerProvider>
    </FormProvider>
  )
}

const RecordMutationForm: React.FC = () => {
  const params = useParams<{ id?: string; objectName: string }>()
  const formMethods = useFormContext()
  const history = useHistory()
  const queryClient = useQueryClient()
  const { contents, elements } = useLayoutViewer()
  const [{ data: { defaultCurrencyCode } = {} as SessionResponse }] = useSession()

  const currencyFields = useMemo(() => {
    const layoutKeys = Object.keys(elements)
    let recordElement
    for (const key of layoutKeys) {
      if (elements[key].component === 'RecordDetail' && elements[key].props.isPrimary) {
        recordElement = elements[key]
      }
    }
    if (recordElement) {
      return recordElement.schema?.fields.reduce((a, i) => {
        if (i.typeName === 'currency') {
          a[i.name] = i
        }
        return a
      }, {} as Record<string, FieldSchema>)
    }
    return {}
  }, [elements])

  const guid = params.id
  const objectName = params.objectName
  const { mutate: createObject, isLoading: isCreateLoading } = useMutation(APIServices.Data.createObjectData, {
    onSuccess(data) {
      NToast.success({
        title: 'Created successful',
      })
      queryClient.invalidateQueries([QueryKeys.Data.searchObjectData, { name: objectName }])
      history.replace(`/${objectName}/${data.data.guid}`)
    },
    onError(e) {
      NToast.error({
        title: `Create unsuccessful`,
        subtitle: e.response?.data.message,
      })
    },
  })
  const { mutate: updateObject, isLoading: isUpdateLoading } = useMutation(APIServices.Data.updateObjectData, {
    onSuccess() {
      NToast.success({
        title: 'Updated successful',
      })
      queryClient.invalidateQueries([QueryKeys.Data.getSingleObjectData, { name: objectName, guid: guid }])
      queryClient.invalidateQueries([QueryKeys.Data.searchObjectData, { name: objectName }])
      history.replace(`/${objectName}/${guid}`)
    },
    onError(e) {
      NToast.error({
        title: `Update unsuccessful`,
        subtitle: e.response?.data.message,
      })
    },
  })

  const formSubmit = (body: any) => {
    // metadata media map
    const media: Record<string, NFileType | undefined> = body?.$metadata?.media || {}
    const mediaKeys = Object.keys(media)
    const transformMedia = mediaKeys.reduce((prev, cur) => {
      const mediaFile = media[cur]
      if (!mediaFile) {
        return prev
      }
      return { ...prev, [mediaFile.id]: cur }
    }, {} as Record<string, string>)

    // transform to { tagName: mediaId } format to decide which one is delete/newly create
    const { oldMediaByNameKey, deleted } = Object.keys(contents.$metadata?.media || {}).reduce(
      (prev, cur) => {
        const mediaFile = contents.$metadata?.media?.[cur]
        if (mediaFile) {
          prev.oldMediaByNameKey[mediaFile.tagName || mediaFile.id] = mediaFile.id
          prev.deleted[mediaFile.tagName || mediaFile.id] = mediaFile.id

          return prev
        }
        return prev
      },
      { oldMediaByNameKey: {} as Record<string, string>, deleted: {} as Record<string, string> },
    )
    const newMediaByNameKey = invert(transformMedia)

    const save = Object.keys(newMediaByNameKey).reduce((a, name) => {
      if (!oldMediaByNameKey[name]) {
        // name not exist => save new
        a[newMediaByNameKey[name]] = name
        return a
      }

      if (oldMediaByNameKey[name] && oldMediaByNameKey[name] !== newMediaByNameKey[name]) {
        // name exist but different media => save new, keep delete value to delete old one
        a[newMediaByNameKey[name]] = name
        return a
      }
      // nothing change => remove name from delete values
      delete deleted[name]
      return a
    }, {} as Record<string, string>)

    for (const key of Object.keys(body)) {
      if (body[key] && currencyFields?.[key]) {
        //@ts-ignore key type
        if (currency[body.currencyCode || defaultCurrencyCode]) {
          body[key] = dineroFromFloat({
            float: body[key],
            //@ts-ignore key type
            currency: currency[body.currencyCode || defaultCurrencyCode],
          }).toJSON().amount
        }
      }
    }

    const transformBody = {
      ...body,
      $metadata: {
        ...body.$metadata,
        media: { del: Object.keys(deleted), save },
      },
    }

    if (guid) {
      updateObject({
        name: objectName,
        guid,
        body: transformBody,
      })
      return
    }
    createObject({
      name: objectName,
      body: transformBody,
    })
  }

  return (
    <form onSubmit={formMethods.handleSubmit(formSubmit)}>
      <LayoutViewerRoot />
      <ActionsWrapper>
        <NButton
          onClick={() => {
            if (history.length > 1) {
              history.goBack()
              return
            }
            if (guid) {
              history.push(`/${objectName}/${guid}`)
              return
            }
            history.push(`/${objectName}`)
          }}>
          Cancel
        </NButton>
        <NButton htmlType="submit" type="primary" loading={isCreateLoading || isUpdateLoading}>
          {!guid ? 'Create new Record' : 'Update record'}
        </NButton>
      </ActionsWrapper>
    </form>
  )
}
