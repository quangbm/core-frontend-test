import React from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import { APIServices, QueryKeys } from '../../../services/api'
import { Element } from '../../LayoutBuilder/utils/models'
import { ErrorContainer, ErrorSubtitle, ErrorTitle } from '../components/ErrorComponents'
import { LoadingView } from '../components/LoadingView'
import { LayoutViewerProvider } from '../contexts/LayoutViewerContext'
import { LayoutType } from '../model'
import { LayoutViewerRoot } from './LayoutViewerRoot'

export const TabPage = () => {
  const { layoutId } = useParams<{ layoutId: string }>()
  // get tab page script
  const { data, isLoading } = useQuery([QueryKeys.App.getAppScript, { layoutId }], APIServices.App.getAppScript, {
    enabled: !!layoutId,
  })

  if (isLoading) {
    return <LoadingView />
  }

  if (!data || !data.data) {
    return (
      <ErrorContainer>
        <ErrorTitle>Get layout error!</ErrorTitle>
        <ErrorSubtitle>No script</ErrorSubtitle>
      </ErrorContainer>
    )
  }

  return (
    <LayoutViewerProvider
      elements={data.data as Record<string, Element>}
      layoutType={LayoutType.Tab}
      layoutId={layoutId}>
      <LayoutViewerRoot />
    </LayoutViewerProvider>
  )
}
