import { FC, useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled from 'styled-components/macro'
import { color, spacing } from '../../../components/GlobalStyle'
import { NButton } from '../../../components/NButton/NButton'
import { NDivider } from '../../../components/NDivider'
import { FieldItemMode, NFieldItem, ViewItem } from '../../../components/NFieldItem/NFieldItem'
import { NRow } from '../../../components/NGrid/NGrid'
import { NToast } from '../../../components/NToast'
import { typography } from '../../../components/NTypography'
import { SettingBoard } from '../../../components/SettingBoard'
import { useMe } from '../../../hooks/useMe'
import { APIServices, QueryKeys } from '../../../services/api'

const Wrapper = styled.div`
  background: ${color('white')};
  flex: 1;
  padding: ${spacing('xl')};
`

const DetailWrapper = styled.div`
  padding: ${spacing('xl')};
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: ${spacing('xl')};
  grid-row-gap: ${spacing('md')};
`

export const AccordionTitle = styled('div')`
  flex: 1;
  color: ${color('Neutral700')};
  ${typography('overline')}
  text-transform: uppercase;
`

const SaveBtn = styled(NButton)`
  width: 80px;
`

export const TableWrapper = styled.div`
  padding: ${spacing('xl')};
`

export const MePage: FC = () => {
  const { data: meData } = useMe()
  const [isEdit, setIsEdit] = useState(false)
  const queryClient = useQueryClient()

  const formMethods = useForm()
  const { handleSubmit, reset } = formMethods

  const { mutate: updateMedia, isLoading } = useMutation(APIServices.Me.updateMe, {
    onSuccess: () => {
      queryClient.invalidateQueries(QueryKeys.Me.getMe)
      setIsEdit(false)
      NToast.success({
        title: 'User profile updated',
      })
    },
    onError: error => {
      NToast.error({
        title: 'Update user profile unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const onSubmit = (data: any) => {
    updateMedia(data)
  }

  return (
    <Wrapper>
      <SettingBoard
        title="My profile"
        action={
          isEdit ? (
            <NRow>
              <NButton
                size="small"
                onClick={() => {
                  setIsEdit(false)
                  reset({})
                }}>
                Cancel
              </NButton>
              <NDivider size="md" />
              <SaveBtn size="small" type="primary" onClick={handleSubmit(onSubmit)} loading={isLoading}>
                Save
              </SaveBtn>
            </NRow>
          ) : (
            <NButton size="small" onClick={() => setIsEdit(true)}>
              Edit
            </NButton>
          )
        }>
        <DetailWrapper>
          <FormProvider {...formMethods}>
            {meData?.userInfo?.schema.fields.map(field =>
              meData.userInfo.data.hasOwnProperty(field.name) ? (
                <NFieldItem
                  key={field.name}
                  mode={isEdit ? FieldItemMode.Form : FieldItemMode.View}
                  field={field}
                  contents={meData.userInfo.data || {}}
                  pickList={meData.userInfo.schema.pickList}
                />
              ) : null,
            )}
          </FormProvider>
          <ViewItem title="Profile" value={meData?.profile.name || ''} />
          <ViewItem title="Profile" value={meData?.role.displayName || ''} />
        </DetailWrapper>
      </SettingBoard>
    </Wrapper>
  )
}
