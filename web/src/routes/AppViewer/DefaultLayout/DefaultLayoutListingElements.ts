import { nanoid } from 'nanoid'
import { ROOT_ELEMENT } from '../../LayoutBuilder/utils/builder'
import { Element, ElementType } from '../../LayoutBuilder/utils/models'
import { SchemaFieldtype } from '../model'

export const generateDefaultLayoutElements = (objectName: string, fields: SchemaFieldtype[]) => {
  const ListViewElement: Element = {
    id: nanoid(),
    label: 'List View',
    type: ElementType.Field,
    component: 'ListView',
    props: {
      objectName,
      fieldNames: fields.map(f => f.name),
      actions: ['new'],
      limit: 10,
    },
    children: [],
  }

  const SECTION_ELEMENT = nanoid()
  const FLOW_ELEMENT = nanoid()

  const result: Record<string, Element> = {
    [ROOT_ELEMENT]: {
      id: ROOT_ELEMENT,
      label: 'Body',
      type: ElementType.Layout,
      component: '',
      props: {},
      children: [SECTION_ELEMENT],
    },
    [SECTION_ELEMENT]: {
      id: SECTION_ELEMENT,
      label: 'Section',
      type: ElementType.Layout,
      component: 'Section',
      props: {
        columns: [1], // [2, 1],
      },
      children: [ListViewElement.id], // [ListViewElement.id, FLOW_ELEMENT],
    },
    [ListViewElement.id]: ListViewElement,
    [FLOW_ELEMENT]: {
      id: FLOW_ELEMENT,
      label: 'Flow',
      type: ElementType.Field,
      component: 'Flow',
      props: {
        flowId: '00a_001iuo8BDUqik7ztabP7Wc8ae',
      },
      children: [],
    },
  }
  return result
}
