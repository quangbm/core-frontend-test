import React from 'react'
import { useQuery } from 'react-query'
import { Link, Route, Switch, useHistory, useLocation, useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { DASHBOARD_ROUTE } from '../../common/constants'
import { AppHeader } from '../../components/AppHeader/AppHeader'
import { color, spacing } from '../../components/GlobalStyle'
import { NAvatar } from '../../components/NAvatar/NAvatar'
import { NDivider } from '../../components/NDivider'
import { NSpinner } from '../../components/NSpinner/NSpinner'
import { typography } from '../../components/NTypography'
import { useSession } from '../../hooks/useSession'
import { APIServices, QueryKeys } from '../../services/api'
import { Can } from '../../services/casl/Can'
import { FooterScript } from './components/FooterScript'
import { HeaderScript } from './components/HeaderScript'
import { LoadingView } from './components/LoadingView'
import { PageListing } from './components/PageListing'
import { ActionDataProvider } from './contexts/ActionDataContext'
import { MePage } from './Layout/MePage'
import { RecordPage } from './Layout/RecordPage'
import { RecordPageMutation } from './Layout/RecordPageMutation'
import { TabPage } from './Layout/TabPage'

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
  height: 100vh;
`

const AppNameWrapper = styled.div`
  display: flex;
  align-items: center;
`

const AppName = styled('div')`
  ${typography('h500')}
  color:  ${color('Neutral700')};
`

const DashboardButton = styled('div')`
  display: flex;
  align-items: center;
  height: 32px;

  border: 1px solid ${color('Primary700')};
  color: ${color('Neutral700')};
  background: transparent;
  border-radius: 3px;
  &:hover:enabled {
    background: ${color('Primary100')};
  }
  &:active {
    background: transparent;
    color: ${color('Neutral700')};
    box-shadow: 0 0 0 4px ${color('Primary200')};
  }
  a {
    padding: 0 ${spacing('xl')};
    display: block;
    color: ${color('Primary700')};
  }
`

const AppWrapper = styled('div')`
  position: relative;
  display: flex;
  flex-direction: column;
  flex: 1;
  position: relative;
  overflow: auto;
  background: ${color('Neutral100')};
  padding: ${spacing('md')};
`

type AppViewerProps = {
  children?: React.ReactNode
}

export function AppViewer({}: AppViewerProps) {
  const { id } = useParams<{ objectName?: string; layoutId?: string; id?: string }>()

  const history = useHistory()
  const location = useLocation()
  const [{ data: sessionData }, updateSession] = useSession()
  const lastUsedApp = sessionData?.lastUsedAppId

  const { isLoading: isAppsListLoading } = useQuery([QueryKeys.App.getApps, { limit: 1 }], APIServices.App.getApps, {
    enabled: !lastUsedApp,
    onSuccess(response) {
      if (!lastUsedApp && response.data.data[0]) {
        updateSession({
          lastUsedAppId: response.data.data[0].id,
        })
      }
    },
  })

  const { data: appData, isLoading: isAppLoading } = useQuery(
    [QueryKeys.App.getApp, { id: lastUsedApp! }],
    APIServices.App.getApp,
    {
      enabled: !!lastUsedApp,
      select(response) {
        return response.data
      },
      keepPreviousData: false,
    },
  )

  const { data: appPages, isLoading: isAppPagesLoading } = useQuery(
    [QueryKeys.App.getAppPages, { id: lastUsedApp! }],
    APIServices.App.getAppPages,
    {
      enabled: !!lastUsedApp,
      select(response) {
        return response.data
      },
    },
  )

  // Always navigate to first tab when app changed
  React.useEffect(() => {
    if (appPages?.length) {
      history.push(`/${appPages[0].id}`)
    }
  }, [lastUsedApp, appPages, history])

  // Update lastUrl to server
  React.useEffect(() => {
    if (location.pathname !== '/') {
      updateSession({ lastUrl: `${location.pathname}${location.search || ''}` })
    }
  }, [location.pathname, updateSession, location.search])

  React.useEffect(() => {
    if (location.pathname === '/' && sessionData?.lastUrl) {
      history.push(sessionData?.lastUrl)
    }
  }, [])

  const isLoading = isAppLoading || isAppPagesLoading || isAppsListLoading

  return (
    <React.Fragment>
      {appData?.customProps?.headerScript && <HeaderScript scripts={appData.customProps.headerScript} />}
      {appData?.customProps?.footerScript && <FooterScript>{appData.customProps.footerScript}</FooterScript>}
      <Wrapper>
        <AppHeader
          left={
            (isAppLoading && <NSpinner size={18} style={{ marginLeft: 8 }} />) || (
              <AppNameWrapper>
                <NDivider vertical size="xs" />
                {appData?.name && <NAvatar size={24} variant="circle" name={appData?.name} />}
                <NDivider vertical size="xs" />
                <AppName>{appData?.name}</AppName>
              </AppNameWrapper>
            )
          }
          right={
            <Can I="read" a="setup">
              <DashboardButton>
                <Link to={DASHBOARD_ROUTE}>Go to Setup</Link>
              </DashboardButton>
            </Can>
          }>
          <PageListing pages={appPages} isLoading={isAppPagesLoading} />
        </AppHeader>
        {(() => {
          if (isLoading) {
            return <LoadingView />
          }
          if (!isLoading && !appData) {
            return <div>No App Found</div>
          }
          if (!isLoading && appData && !appPages?.length) {
            return (
              <Switch>
                <Route path="/me" exact>
                  <MePage />
                </Route>
                <Route path="/">
                  <div>No tab found</div>
                </Route>
              </Switch>
            )
          }
          return (
            <AppWrapper>
              <ActionDataProvider
                id={id}
                postDelete={() => {
                  history.goBack()
                }}>
                <Switch>
                  <Route path="/me" exact>
                    <MePage />
                  </Route>
                  <Route path="/:layoutId" exact>
                    <TabPage />
                  </Route>
                  <Route path={['/:objectName/create', '/:objectName/:id/edit']}>
                    <RecordPageMutation />
                  </Route>
                  <Route path="/:objectName/:id">
                    <RecordPage />
                  </Route>
                </Switch>
              </ActionDataProvider>
            </AppWrapper>
          )
        })()}
      </Wrapper>
    </React.Fragment>
  )
}
