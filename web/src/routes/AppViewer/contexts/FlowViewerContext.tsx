import React, { createContext, FC } from 'react'
import { ScreenElement } from '../../FlowBuilder/FlowBuilderItem/ScreenBuilder/utils/models'

export type FlowViewerContext = {
  elements: Record<string, ScreenElement>
  content?: Record<string, any>
  context?: Record<string, any>
  layoutType?: string
  //
  objectName?: string
  screenName: string
}

export const FlowViewerContext = createContext<FlowViewerContext | string>(
  'useFlowViewer should be used inside FlowViewerProvider',
)

export type FlowViewerContextProps = {
  elements: Record<string, ScreenElement>
  content?: Record<string, any>
  context?: Record<string, any>
  screenName?: string
}

export const FlowViewerProvider: FC<FlowViewerContextProps> = ({
  elements,
  context,
  content,
  screenName = '',
  children,
}) => {
  const value = {
    elements,
    content,
    context,
    screenName,
  }
  return <FlowViewerContext.Provider value={value}>{children}</FlowViewerContext.Provider>
}

export const useFlowViewer = () => {
  const context = React.useContext(FlowViewerContext)
  if (typeof context === 'string') {
    throw new Error(context)
  }
  return context
}
