import * as React from 'react'

type HackValue = {}

const HackContext = React.createContext<HackValue | string>('useHack should be used inside HackProvider')

type HackContextProps = {
  children: React.ReactNode
  onClose(): void
  onSubmit(e?: any): void
}

export default function HackProvider({ children, onClose, onSubmit }: HackContextProps) {
  const value: HackValue = {
    onClose,
    onSubmit,
  }

  return <HackContext.Provider {...{ value, children }} />
}

export const useHack = () => {
  const context = React.useContext(HackContext)
  if (typeof context === 'string') {
    throw new Error(context)
  }
  return context
}
