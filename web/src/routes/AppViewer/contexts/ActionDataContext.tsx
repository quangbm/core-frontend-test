import React, { createContext, FC } from 'react'

export type ActionDataContextType = {
  id?: string
  postDelete?: () => void
  postClose?: () => void
}

export const ActionDataContext = createContext<ActionDataContextType | string>(
  'useActionData should be used inside ActionDataProvider',
)

export type ActionDataContextProps = {
  id?: string
  postDelete?: () => void
  postClose?: () => void
}

export const ActionDataProvider: FC<ActionDataContextProps> = ({ children, ...props }) => {
  const value = props
  return <ActionDataContext.Provider value={value}>{children}</ActionDataContext.Provider>
}

export const useActionData = () => {
  const context = React.useContext(ActionDataContext)
  if (typeof context === 'string') {
    throw new Error(context)
  }
  return context
}
