import React, { createContext, Dispatch, FC, useReducer } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router'
import { DataResponse } from '../../../../../shared/src/api'
import { NToast } from '../../../components/NToast'
import { useAccessToken } from '../../../hooks/useAuth'
import { getCurUserFromMe, useMe } from '../../../hooks/useMe'
import { useSession } from '../../../hooks/useSession'
import { APIServices, QueryKeys } from '../../../services/api'
import { Element } from '../../LayoutBuilder/utils/models'

export type InputMap = {
  curUser?: Record<string, any>
  curRecord?: DataResponse
  now: string
}

export type LayoutViewerContextValue = {
  elements: Record<string, Element>
  isLoadingContents?: boolean
  layoutType: string
  layoutId: string
  contents: Record<string, any> & DataResponse
  //
  objectName?: string
  inputMap: InputMap
  setLayoutVar?: Dispatch<{ [key: string]: any }> // Allow components to create custom vars into inputMap
}

export const LayoutViewerContext = createContext<LayoutViewerContextValue | string>(
  'useLayoutViewer should be used inside LayoutViewerProvider',
)

export type LayoutViewerContextProps = {
  elements: Record<string, Element>
  layoutType: string
  //
  objectName?: string
  layoutId: string
}

const varMapReducer = (state: { [key: string]: any }, action: { [key: string]: any }) => ({
  ...state,
  ...action,
})
export const LayoutViewerProvider: FC<LayoutViewerContextProps> = ({
  layoutId,
  elements,
  layoutType,
  objectName,
  children,
}) => {
  const { id: guid } = useParams<{ id?: string }>()
  const { data: accessToken } = useAccessToken()
  const [{ data: sessionData }] = useSession()
  const [variableMap, dispatch] = useReducer(varMapReducer, {})
  const { data: meData } = useMe()

  // get default value
  const {
    data: contents = {
      createdBy: (meData?.userInfo.data as Record<string, any>)['name'],
      $metadata: {
        creatorId: sessionData?.userId,
      },
    } as Record<string, any> & DataResponse,
    isLoading: isLoadingContents,
  } = useQuery(
    [QueryKeys.Data.getSingleObjectData, { name: objectName!, guid: guid! }],
    APIServices.Data.getSingleObjectData,
    {
      enabled: !!objectName && !!guid,
      // TODO: Handle error on UI
      onError: error => {
        NToast.error({ title: 'Get contents error', subtitle: error.response?.data.message })
      },
      select(res) {
        return res.data
      },
    },
  )

  const inputMap = React.useMemo(
    () => ({
      curUser: { ...getCurUserFromMe(meData), accessToken },
      curRecord: contents,
      now: new Date().toISOString(),
      ...variableMap,
    }),
    [contents, variableMap, meData],
  )

  const value = {
    layoutId,
    elements,
    layoutType,
    isLoadingContents,
    contents,
    //
    objectName,
    inputMap,
    setLayoutVar: dispatch,
  }
  return <LayoutViewerContext.Provider value={value}>{children}</LayoutViewerContext.Provider>
}

export const useLayoutViewer = () => {
  const context = React.useContext(LayoutViewerContext)
  if (typeof context === 'string') {
    throw new Error(context)
  }
  return context
}
