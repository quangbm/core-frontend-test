import React, { FC } from 'react'
import styled from 'styled-components/macro'
import { color } from '../../../components/GlobalStyle'
import { typography } from '../../../components/NTypography'
import { VIEW_ITEMS } from '../ViewItem/ViewItems'
import { useLayoutViewer } from './LayoutViewerContext'

const UnderConstructionContainer = styled.div`
  color: ${color('Neutral400')};
  ${typography('button')}
`

type UnderConstructionProps = {
  children?: React.ReactNode
}

export const ViewUnderConstruction: FC<UnderConstructionProps> = () => {
  return <UnderConstructionContainer>Need more 🍕 and ☕️ to build this component</UnderConstructionContainer>
}

export function useRenderView(elementId: string) {
  const { elements } = useLayoutViewer()
  const element = elements[elementId]

  return (
    element.children &&
    element.children.map((id, idx) => {
      const Component = VIEW_ITEMS[elements[id]?.component]
      if (!Component) {
        return <ViewUnderConstruction key={id} />
      }
      return <Component key={id} elementId={id} parentId={elementId} index={idx} />
    })
  )
}
