import React, { FC } from 'react'
import styled from 'styled-components/macro'
import { spacing } from '../../../components/GlobalStyle'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'
import { CustomCss } from '../model'
import { getCustomCss, resolveTextVariable } from '../utils'

const Text = styled('div')<CustomCss>`
  padding: ${spacing('md')};
  ${getCustomCss}
`

export const Paragraph: FC<ZoneProps> = ({ elementId }) => {
  const { elements, inputMap } = useLayoutViewer()
  const element = elements[elementId]

  return <Text customCss={element.props.customCss}>{resolveTextVariable(element.props.children, inputMap)}</Text>
}
