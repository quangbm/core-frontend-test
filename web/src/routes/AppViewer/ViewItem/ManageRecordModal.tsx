import React, { FC, useEffect, useState } from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import styled from 'styled-components/macro'
import { spacing } from '../../../components/GlobalStyle'
import { NModal } from '../../../components/NModal/NModal'
import { SelectOption } from '../../../components/NSelect/model'
import { NSingleSelect } from '../../../components/NSelect/NSingleSelect'
import { NToast } from '../../../components/NToast'
import { useSearchText } from '../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../services/api'

const ModalBody = styled(NModal.Body)`
  padding: ${spacing('xl')};
`

type Props = {
  objName: string
  recordId: string
  visible: boolean
  setVisible: (newState: boolean) => void
}
export const ManageRecordModal: FC<Props> = ({ objName, recordId, visible, setVisible }) => {
  const [manager, setManager] = useState<string | undefined>()

  const queryClient = useQueryClient()
  const [{ searchText, searchValue }, onSearchChange] = useSearchText()

  const { mutate: updateManager, isLoading: isUpdating } = useMutation(APIServices.Data.setManagedBy, {
    onSuccess: () => {
      queryClient.invalidateQueries([QueryKeys.Data.searchObjectData, { name: objName }])
      queryClient.invalidateQueries([QueryKeys.Data.getSingleObjectData, { name: objName, guid: recordId }])
      NToast.success({ title: 'Assign manager successful' })
      setVisible(false)
    },
    onError: e => {
      NToast.error({ title: 'Assign manager error', subtitle: e })
    },
  })

  const { data: recordData, isLoading: isRecordLoading } = useQuery(
    [QueryKeys.Data.getSingleObjectData, { name: objName, guid: recordId }],
    APIServices.Data.getSingleObjectData,
  )

  const { data: userOptions, isLoading: isOptionsLoading } = useQuery(
    [QueryKeys.Users.getUsers, { searchText }],
    APIServices.Users.getUsers,
    {
      select: data => {
        return data.data.data.map(user => {
          return {
            value: user.id,
            label: (user.userInfo as Record<string, any>)['name'],
          } as SelectOption
        })
      },
    },
  )

  useEffect(() => {
    if (!recordData?.data || manager) {
      return
    }

    setManager(recordData.data['$metadata']?.managerId)
  }, [recordData?.data])

  const onSubmit = () => {
    if (!manager) {
      return
    }
    updateManager({ objName, guids: [recordId], managedBy: manager })
  }

  const isLoading = isOptionsLoading || isRecordLoading
  return (
    <NModal visible={visible} setVisible={setVisible} zIndex={1100}>
      <NModal.Header title="Manage Record" />
      <ModalBody>
        <NSingleSelect
          isLoading={isLoading}
          label="Managed by"
          options={userOptions || []}
          value={manager}
          onValueChange={newValue => {
            newValue && setManager(newValue)
          }}
          isSearchable
          searchValue={searchValue}
          onSearchValueChange={onSearchChange}
        />
      </ModalBody>
      <NModal.Footer
        isLoading={isUpdating || isLoading}
        onCancel={() => {
          setVisible(false)
        }}
        onFinish={() => {
          onSubmit()
        }}
      />
    </NModal>
  )
}
