import get from 'lodash/get'
import * as React from 'react'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components/macro'
import { spacing } from '../../../../components/GlobalStyle'
import { Icons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NSpinner } from '../../../../components/NSpinner/NSpinner'
import { ZoneProps } from '../../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../../contexts/LayoutViewerContext'
import { useRenderView } from '../../contexts/useRenderView'

const Wrapper = styled('div')`
  background: white;
  border-radius: 4px;
  padding: 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const Title = styled('div')`
  font-size: 16px;
  line-height: 16px;
  font-weight: bold;
  color: #0d1826;
`
const Subtitle = styled('div')`
  font-size: 11px;
  line-height: 11px;
  color: #768493;
  margin-top: 8px;
`

const Actions = styled('div')`
  display: flex;
  & > *:not(:last-child) {
    margin-right: 8px;
  }
`
const GoBackButton = styled(NButton)`
  padding-right: ${spacing('md')};
  padding-left: ${spacing('md')};
`
export function Header({ elementId }: ZoneProps) {
  const { elements, contents, isLoadingContents } = useLayoutViewer()
  const element = elements[elementId]
  const children = useRenderView(elementId)
  const history = useHistory()

  const title = React.useMemo(() => {
    if (element.props.title.includes('$')) {
      const key = element.props.title.replace('$', '')
      return contents && get(contents, key, '')
    }
    return element.props.title
  }, [contents, element.props.title])

  return (
    <Wrapper>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        {element.props.hasBack && (
          <GoBackButton
            type="ghost"
            size="small"
            onClick={() => {
              history.goBack()
            }}>
            <Icons.Left />
          </GoBackButton>
        )}
        {isLoadingContents && <NSpinner />}
        {!isLoadingContents && (
          <div>
            <Title>{title}</Title>
            {element.props.description && <Subtitle>{element.props.description}</Subtitle>}
          </div>
        )}
      </div>
      <Actions>{children}</Actions>
    </Wrapper>
  )
}
