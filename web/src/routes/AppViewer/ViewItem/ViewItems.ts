import { ScreenElement } from '../../FlowBuilder/FlowBuilderItem/ScreenBuilder/utils/models'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { Attachments } from './Attachments'
import { Button } from './Button'
import { Header } from './demo/Header'
import { Divider } from './Divider'
import { AmountInput } from './Flow/AmountInput'
import { Checkbox } from './Flow/Checkbox'
import { CSVInput } from './Flow/CSVInput'
import { DataLookup } from './Flow/DataLookup'
import { DateTime } from './Flow/DateTime'
import { DisplayText } from './Flow/DisplayText'
import { FileDropzone } from './Flow/FileDropzone'
import { Grid } from './Flow/Grid'
import { Input } from './Flow/Input'
import { RendererPropsType } from './Flow/model'
import { PickList } from './Flow/PickList'
import { PickList2 } from './Flow/PickList2'
import { RadioGroup } from './Flow/RadioGroup'
import { ReactComponent as FlowReactComponent } from './Flow/ReactComponent'
import { Withdrawal } from './Flow/Withdrawal'
import { Heading } from './Heading'
import { ListView } from './Listview/ListView'
import { RelatedList } from './Listview/RelatedList'
import { Paragraph } from './Paragraph'
import { ReactComponent } from './ReactComponent'
import { RecordDetail } from './RecordDetail/RecordDetail'
import { Section } from './Section'
import { Tabs } from './Tabs'

export const VIEW_ITEMS = {
  Section,
  Tabs,
  RecordDetail,
  Heading,
  Paragraph,
  Divider,
  Button,
  ObjectList: ListView,
  RelatedList,
  DemoHeader: Header,
  Attachments,
  ReactComponent,
} as Record<string, (props: ZoneProps) => JSX.Element>

export type RendererType = (props: RendererPropsType) => JSX.Element

export const SCREEN_VIEW_ITEMS_INPUTS = {
  string: Input,
  float: Input,
  integer: Input,
  date: DateTime,
  time: DateTime,
  'date-time': DateTime,
  boolean: Checkbox,
  object: AmountInput,
  array: CSVInput,
} as Record<string, RendererType>
export const SCREEN_VIEW_ITEMS_SELECTIONS = {
  PickList,
  PickList2,
  Radio: RadioGroup,
} as Record<string, RendererType>
export const SCREEN_VIEW_ITEMS = {
  FileDropzone,
  DisplayText,
  Withdrawal,
  Grid,
  DataLookup,
  ReactComponent: FlowReactComponent,
} as Record<string, RendererType>

export const getScreenViewItemFromElement = (element: ScreenElement) => {
  const { props, component } = element
  if (props.inputType) {
    return SCREEN_VIEW_ITEMS_INPUTS[props.inputType]
  }
  if (props.selectionType) {
    if (props.isRadio) {
      return SCREEN_VIEW_ITEMS_SELECTIONS['Radio']
    }
    return SCREEN_VIEW_ITEMS_SELECTIONS[component === 'PickList' ? 'PickList' : 'PickList2']
  }
  return SCREEN_VIEW_ITEMS[component]
}
