import * as React from 'react'
import { useQuery } from 'react-query'
import {
  Area,
  Bar,
  CartesianGrid,
  ComposedChart,
  Legend,
  Line,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'
import styled, { useTheme } from 'styled-components/macro'
import { color, spacing } from '../../../components/GlobalStyle'
import { NDivider } from '../../../components/NDivider'
import { NRow } from '../../../components/NGrid/NGrid'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { NToast } from '../../../components/NToast'
import { typography } from '../../../components/NTypography'
import { APIServices, QueryKeys } from '../../../services/api'
import { chartPalette } from '../../../utils/chart-palette'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'
import { resolveFilterVariable } from '../utils'

const Wrapper = styled.div`
  border-radius: 4px;
  padding: ${spacing('md')};
  background-color: ${color('white')};
`

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${spacing('md')};
`
const Title = styled.div`
  ${typography('h500')};
`
const Description = styled.div`
  ${typography('ui-text')};
  color: ${color('Neutral500')};
  margin-bottom: ${spacing('md')};
`
export const Chart = React.memo(function Node({ elementId }: ZoneProps) {
  const { elements, inputMap } = useLayoutViewer()
  const element = elements[elementId]
  const theme = useTheme()

  const mapFilters = resolveFilterVariable(element.props.filters || [], inputMap)
  // get content list
  const { data: objectData, isFetching: isLoadingObjectsData } = useQuery(
    [
      QueryKeys.Data.searchObjectData,
      {
        name: element.props.objName,
        limit: element.props.limit,
        filters: mapFilters,
        sortBy: element.props.sortBy,
        sortOrder: element.props.sortBy ? element.props.sortOrder : undefined,
      },
    ],
    APIServices.Data.searchObjectData,
    {
      keepPreviousData: true,
      onError: error => {
        NToast.error({ title: 'Get object data error!', subtitle: error.response?.data.message })
      },
      enabled: !!element.props.objName,
    },
  )

  return (
    <Wrapper>
      <HeaderContainer>
        <Title>
          {element.props.title || element.schema?.displayName || (
            <NRow align="center">
              Loading
              <NDivider vertical size="md" />
              <NSpinner size={20} strokeWidth={4} />
            </NRow>
          )}
        </Title>
        {isLoadingObjectsData && <NSpinner size={24} strokeWidth={2} color={color('Primary900')({ theme })} />}
      </HeaderContainer>
      <Description>{element.props.description}</Description>
      <ResponsiveContainer width={'100%'} height={350}>
        <ComposedChart data={objectData?.data.data || []}>
          <XAxis dataKey={element.props.xAxis} />
          <YAxis />
          {element.props.series?.map((series: any, index: number) => {
            switch (series.type) {
              case 'line':
                return <Line dataKey={series.fieldName} fill={chartPalette(index)} />
              case 'area':
                return <Area dataKey={series.fieldName} fill={chartPalette(index)} />
              default:
                return <Bar dataKey={series.fieldName} fill={chartPalette(index)} />
            }
          })}
          <Tooltip />
          <Legend />
          <CartesianGrid strokeDasharray="3 3" />
        </ComposedChart>
      </ResponsiveContainer>
    </Wrapper>
  )
})

Chart.displayName = 'Chart'
