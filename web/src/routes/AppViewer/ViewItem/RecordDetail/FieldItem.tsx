import React, { FC } from 'react'
import { FieldSchema, PickListItemResponse } from '../../../../../../shared/src/api'
import { FieldItemMode, NFieldItem } from '../../../../components/NFieldItem/NFieldItem'
import { ErrorText } from '../../components/ErrorText'

type FieldItemProps = {
  fieldName: string
  fields: FieldSchema[]
  mode: FieldItemMode
  contents: Record<string, any>
  pickList?: Record<string, PickListItemResponse[]>
}

export const FieldItem: FC<FieldItemProps> = ({ fieldName, fields, mode, contents, pickList }) => {
  // props
  const field = fields.find(f => f.name === fieldName)

  if (!field) {
    return <ErrorText>Field {fieldName} doesn't exist</ErrorText>
  }
  return <NFieldItem mode={mode} field={field} contents={contents} pickList={pickList} />
}
