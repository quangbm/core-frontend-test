import React, { FC, useState } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { useMutation } from 'react-query'
import styled from 'styled-components/macro'
import { MediaResponse } from '../../../../../../shared/src/api'
import { NFileDropzone } from '../../../../components/NFileDropzone/NFileDropZone'
import { NToast } from '../../../../components/NToast'
import { APIServices } from '../../../../services/api'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

const Wrapper = styled.div`
  display: inline-flex;
  flex-direction: column;
  width: 100%;
`

export const FileDropzone: FC<RendererPropsType> = ({ elementId }) => {
  const { elements } = useFlowViewer()
  const formMethods = useFormContext()
  const { control, setValue } = formMethods

  const element = elements[elementId]
  const { actionMetadata } = element.props

  const [file, setFile] = useState<MediaResponse>()
  const [progress, setProgress] = React.useState(0)

  const { mutate: uploadFile, isLoading } = useMutation(
    ({ name, formData }: { name: string; formData: FormData }) =>
      APIServices.UploadFile.uploadFile(
        { name, formData },
        {
          onUploadProgress(e) {
            setProgress(Math.round((e.loaded / e.total + Number.EPSILON) * 10) / 10)
          },
        },
      ),
    {
      onSuccess() {
        setProgress(0)
      },
      onError: error => {
        setProgress(0)
        NToast.error({ title: 'Upload file error', subtitle: error.response?.data.message })
      },
    },
  )

  return (
    <Wrapper>
      <Controller
        control={control}
        name={element.name!}
        render={({ field: { onChange }, fieldState }) => {
          return (
            <NFileDropzone
              label={element.props.label}
              error={fieldState.error?.message}
              file={file}
              isUploading={isLoading}
              progress={progress}
              onDropAccepted={(newFiles: File[]) => {
                if (!newFiles || newFiles.length < 1) {
                  return
                }
                const formData = new FormData()
                formData.append('file', newFiles[0])
                uploadFile(
                  {
                    name: actionMetadata,
                    formData,
                  },
                  {
                    onSuccess: data => {
                      onChange(data.data.id)
                      setFile(data.data)
                    },
                  },
                )
              }}
              onRemove={() => {
                setValue(element.name!, undefined)
                setFile(undefined)
              }}
            />
          )
        }}
      />
    </Wrapper>
  )
}
