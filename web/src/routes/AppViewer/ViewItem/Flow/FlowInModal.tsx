import { yupResolver } from '@hookform/resolvers/yup'
import * as React from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import styled from 'styled-components/macro'
import * as yup from 'yup'
import { CtxFlowResponse } from '../../../../../../shared/src/api'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NConfirmModal } from '../../../../components/NModal/NConfirmModal'
import { NModal, NModalProps } from '../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../components/NPerfectScrollbar'
import { NSpinner } from '../../../../components/NSpinner/NSpinner'
import { NToast } from '../../../../components/NToast'
import { NTypography } from '../../../../components/NTypography'
import { APIServices, getScreenFlowSSEUrl, QueryKeys } from '../../../../services/api'
import { ROOT_ELEMENT } from '../../../FlowBuilder/FlowBuilderItem/ScreenBuilder/utils/builder'
import { ScreenElement } from '../../../FlowBuilder/FlowBuilderItem/ScreenBuilder/utils/models'
import { FlowViewerProvider } from '../../contexts/FlowViewerContext'
import HackProvider from '../../contexts/HackContext'
import { getCtxId, removeCtxId, saveCtxId } from '../../utils'
import { ScreenFlowLayoutRoot } from './ScreenFlowLayoutRoot'

const Wrapper = styled('div')`
  height: 20vh;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

const HackedForDemo = styled('div')`
  .g {
    display: grid;
    grid-template-rows: minmax(0, 1fr);
    grid-gap: 16px;
  }
  .h {
    font-size: 14px;
    text-transform: uppercase;
    color: #9fa9b3;
  }
  .g-3,
  .notes-row {
    grid-template-columns: repeat(3, minmax(0, 1fr));
  }
  .br {
    padding-bottom: 8px;
    margin-bottom: 8px;
    border-bottom: 1px solid #eceef0;
  }
  .g-2 {
    grid-template-columns: repeat(2, minmax(0, 1fr));
  }
  .t {
    font-weight: bold;
    font-size: 12px;
    color: #525c66;
    margin-bottom: 8px;
  }
  .v {
    font-size: 14px;
    color: #525c66;
    font-weight: thin;
  }
`

function getResolver(script: Record<string, ScreenElement> = {}) {
  const formSchema = {} as Record<string, any>
  const screenName = script?.[ROOT_ELEMENT]?.name || ''

  for (const field of Object.entries(script)) {
    const [fieldId, fieldSchema] = field
    const { name } = fieldSchema
    if (fieldId === ROOT_ELEMENT || !name) {
      continue
    }
    const { inputType, isRequired = false, label } = fieldSchema.props
    const displayName = label || name
    if (fieldSchema.component !== 'Input') {
      if (inputType === 'string') {
        const stringSchema = yup.string().typeError(`${displayName} is not a string`)
        const requiredSchema = isRequired ? stringSchema.required('Required!') : stringSchema

        formSchema[name] = requiredSchema.test('stringNotEmpty', 'Empty string is not allowed!', (value = '') => {
          if (!isRequired && !value) {
            return true
          }
          return Boolean(value?.trim())
        })
        continue
      }
      if (inputType === 'integer') {
        const numberSchema = yup
          .number()
          .typeError(`${displayName} is not a number`)
          .integer(`${displayName} must be an integer`)

        formSchema[name] = isRequired ? numberSchema.required('Required!') : numberSchema
        continue
      }
      if (inputType === 'float') {
        const numberSchema = yup.number().typeError(`${displayName} is not a number`)
        formSchema[name] = isRequired ? numberSchema.required('Required!') : numberSchema
        continue
      }
    }

    if (fieldSchema.component === 'Selection2' || fieldSchema.component === 'PickList') {
      const arraySchema = yup.array().typeError(`${displayName} is not an array`)
      formSchema[name] = isRequired
        ? arraySchema.required('Required!').test('arrayNotEmpty', 'Required!', value => {
            if (!isRequired && !value) {
              return true
            }
            return Array.isArray(value) && value.length > 0
          })
        : arraySchema
      continue
    }

    if (isRequired) {
      formSchema[name] = yup.string().required('Required!')
    }
  }

  return yupResolver(
    yup.object({
      [screenName]: yup.object().shape(formSchema),
    }),
  )
}

type FlowInModalProps = {
  children?: React.ReactNode
  layoutId: string
  componentId: string
  inputMap?: Record<string, string>
  actionId: string
} & NModalProps

export const FlowInModal = React.memo(function Node({
  layoutId,
  componentId,
  visible,
  setVisible,
  inputMap,
  actionId,
}: FlowInModalProps) {
  const [confirm, setConfirm] = React.useState(false)
  const [ctxId, setCtxId] = React.useState(getCtxId(actionId))
  const [isError, setIsError] = React.useState(false)

  const queryClient = useQueryClient()

  const handleCloseModal = () => {
    if (setVisible) {
      setVisible(false)
    }
    toggleSSE(false)
  }

  const { mutate: getTriggers, isLoading: isGettingCtxId } = useMutation(APIServices.Layout.triggers, {
    onSuccess(res) {
      const data = res.data as CtxFlowResponse
      if (!data.ctxId) {
        NToast.error({
          title: 'Invalid component',
          subtitle: 'Something went wrong!',
        })
        handleCloseModal()
        return
      }
      saveCtxId(actionId, data.ctxId)
      setCtxId(data.ctxId)
    },
    onError(error) {
      NToast.error({
        title: 'Get Context Id unsuccessful',
        subtitle: error.response?.data.message || error.message,
      })
      handleCloseModal()
    },
  })

  React.useEffect(() => {
    if (visible && layoutId && componentId) {
      if (!ctxId) {
        getTriggers({
          layoutId,
          componentId,
          ...inputMap,
        })
      }
    }
  }, [componentId, ctxId, getTriggers, inputMap, layoutId, visible])

  // get current screen flow
  const {
    data: screenLayout,
    isLoading: isLoadingScreenLayout,
    refetch: refetchScreenLayout,
  } = useQuery([QueryKeys.Flows.getScreenFlowLayout, { ctxId }], APIServices.Flows.getScreenFlowLayout, {
    cacheTime: 0, // disable cache
    retry: false,
    enabled: false, // for manual control
    refetchOnWindowFocus: false,
    onSuccess: res => {
      //close SSE when got layout script
      if (res?.data?.script) {
        return
      }
      // (No data || no script) === Always need to open SSE
      toggleSSE(true)
    },
    onError: error => {
      // status code that indicates flow finished
      if (error.response?.data.statusCode === 410) {
        // FIXME: Should remove ctxId when the id stored in session === current ctxId only
        removeCtxId()
        if (visible) {
          NToast.success({
            title: 'Screen flow finished!',
          })
          queryClient.invalidateQueries({ refetchActive: true })
          handleCloseModal()
        }
      } else {
        if (visible) {
          NToast.error({
            title: 'Get screen flow unsuccessful',
            subtitle: error.response?.data.message,
          })
        }
      }
    },
  })

  const formMethods = useForm({
    shouldUnregister: true,
    reValidateMode: 'onChange',
    resolver: getResolver(screenLayout?.data.script as Record<string, ScreenElement>),
  })

  const eventSourceRef = React.useRef<EventSource>()
  const toggleSSE = React.useCallback(
    (open: boolean) => {
      eventSourceRef.current?.close()
      eventSourceRef.current = undefined
      if (!ctxId) {
        return
      }
      if (open) {
        const es = new EventSource(getScreenFlowSSEUrl(ctxId))
        es.onmessage = ev => {
          if (ev.data === 'RELOAD') {
            toggleSSE(false)
            refetchScreenLayout()
          }
        }
        es.onerror = () => {
          setIsError(true)
        }
        eventSourceRef.current = es
      }
    },
    [ctxId, refetchScreenLayout],
  )

  // submit current flow screen data
  const { mutate: submitFormData, isLoading: isSubmitting } = useMutation(APIServices.Flows.submitScreenFlowData, {
    onSuccess: () => {
      console.info('Finish submit')
      refetchScreenLayout()
    },
    onError: error => {
      NToast.error({
        title: 'Submit unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  React.useEffect(() => {
    if (ctxId) {
      toggleSSE(false)
      refetchScreenLayout()
    }
  }, [ctxId, refetchScreenLayout, toggleSSE])

  const handleRetry = () => {
    setIsError(false)
    toggleSSE(false)
    refetchScreenLayout()
  }

  const isSSEConnecting = eventSourceRef.current?.readyState === EventSource.CONNECTING
  const isLoading =
    isLoadingScreenLayout || isGettingCtxId || !screenLayout || !screenLayout.data.script || isSSEConnecting

  const script = screenLayout?.data.script as Record<string, ScreenElement>
  const content = screenLayout?.data.content as Record<string, any>
  const context = screenLayout?.data.ctx as Record<string, any>
  const { displayName } = screenLayout?.data || {}
  const rootElement = script?.[ROOT_ELEMENT] || {}
  const screenName = rootElement.name || ''
  const { showFooter, size } = rootElement.props || {}

  const submitForm = formMethods.handleSubmit(body => {
    console.info({ ctxId, body })
    if (ctxId && screenName) {
      submitFormData({ ctxId, body: body[screenName] })
    }
  })

  return (
    <HackProvider onClose={() => setConfirm(true)} onSubmit={submitForm}>
      <NModal visible={visible} closeOnOverlayClick={false} size={size}>
        <NModal.Header
          title={isLoading ? 'Processing ...' : displayName}
          onClose={() => {
            setConfirm(true)
          }}
        />
        <NModal.Body>
          {isLoading && !isError && (
            <Wrapper>
              <NSpinner size={24} strokeWidth={4} />
              <NDivider size="sm" />
              <NTypography.Headline>Loading ...</NTypography.Headline>
            </Wrapper>
          )}
          {isError && (
            <Wrapper>
              <NTypography.Headline>Something went wrong!</NTypography.Headline>
              <NDivider size="sm" />
              <NButton onClick={handleRetry} type="primary">
                Retry flow
              </NButton>
            </Wrapper>
          )}
          {!isLoading && !isError && screenLayout && screenName && (
            <NPerfectScrollbar style={{ maxHeight: '70vh' }}>
              <HackedForDemo>
                <FlowViewerProvider screenName={screenName} elements={script} content={content} context={context}>
                  <FormProvider {...formMethods}>
                    <ScreenFlowLayoutRoot />
                  </FormProvider>
                </FlowViewerProvider>
              </HackedForDemo>
            </NPerfectScrollbar>
          )}
        </NModal.Body>
        {showFooter && (
          <NModal.Footer>
            <NButton
              onClick={() => {
                setConfirm(true)
              }}
              type="default"
              size="small">
              Cancel
            </NButton>
            <NRow>
              {/* {root?.props?.showPrintBtn && (
            <NButton type="outline" size="small">
              Print
            </NButton>
          )} */}
              <NDivider size="md" vertical />
              <NButton
                type="primary"
                size="small"
                onClick={submitForm}
                disabled={isLoading || isError}
                loading={isSubmitting}>
                Next
              </NButton>
            </NRow>
          </NModal.Footer>
        )}
      </NModal>
      <NConfirmModal
        visible={confirm}
        setVisible={setConfirm}
        onCancel={() => {
          setConfirm(false)
        }}
        onConfirm={() => {
          setConfirm(false)
          handleCloseModal()
        }}>
        Do you want to close the modal?
      </NConfirmModal>
    </HackProvider>
  )
})
