import React, { FC } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { NMultiSelect } from '../../../../components/NSelect/NMultiSelect'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

export const PickList: FC<RendererPropsType> = ({ elementId }) => {
  const { elements, screenName } = useFlowViewer()
  const formMethods = useFormContext()

  const element = elements[elementId]
  const { defaultValue: _dV, label, selectionType: type, isRequired, defaultSelectValue, ...restProps } = element.props

  const options = element.props.pickListItems
    ? element.props.pickListItems.map(item => ({ label: item.displayName, value: item.name }))
    : []

  return (
    <Controller
      control={formMethods.control}
      name={`${screenName}.${element.name!}`}
      defaultValue={defaultSelectValue || []}
      render={({ field: { value, onChange }, fieldState }) => {
        if (type === 'multi') {
          return (
            <NMultiSelect
              fullWidth
              values={value || []}
              options={options}
              error={fieldState.error?.message}
              required={isRequired}
              onValuesChange={newVals => {
                onChange(newVals)
              }}
              label={label}
              {...restProps}
            />
          )
        }
        return (
          <NSingleSelect
            fullWidth
            value={value && value[0]}
            error={fieldState.error?.message}
            options={options}
            required={isRequired}
            onValueChange={newVal => {
              onChange([newVal])
            }}
            label={label}
            {...restProps}
          />
        )
      }}
    />
  )
}
