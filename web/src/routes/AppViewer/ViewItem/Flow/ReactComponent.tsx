// @ts-ignore
import * as babel from '@babel/standalone'
import * as React from 'react'
import { useFormContext } from 'react-hook-form'
// @ts-ignore
import Sass from 'sass.js'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { useHack } from '../../contexts/HackContext'

function useSass(value: string) {
  const [status, setStatus] = React.useState('idle')
  const [css, setCSS] = React.useState('')
  React.useEffect(() => {
    async function getCSS(value: string) {
      return new Promise<string>(resolve => {
        Sass.compile(value, (result: any) => {
          resolve(result.text)
        })
      })
    }
    setStatus('transforming')
    getCSS(value)
      .then(setCSS)
      .finally(() => setStatus('idle'))
  }, [value])
  return {
    status,
    css,
  }
}

function plugin({ types: t }: any) {
  return {
    visitor: {
      ExportDefaultDeclaration(path: any) {
        path.replaceWith(t.returnStatement(path.node.declaration))
      },
    },
  }
}

babel.registerPlugin('plugin', plugin)

function parseCode(codeString: string) {
  try {
    const { code } = babel.transform(codeString, {
      presets: ['react', 'env'],
      plugins: ['plugin'],
    })
    return code
  } catch (err) {
    return null
  }
}

export function ReactComponent({ elementId }: { elementId: string }) {
  const { elements } = useFlowViewer()
  const element = elements[elementId]
  const Component = React.useMemo(() => {
    // eslint-disable-next-line no-new-func
    const renderer = new Function(
      'React',
      'useSass',
      'useHack',
      'useFlowViewer',
      'useFormContext',
      parseCode(element.props.value),
    )
    return renderer(React, useSass, useHack, useFlowViewer, useFormContext)
  }, [element.props.value])
  if (!Component) {
    return <div style={{ color: 'red' }}>Something went wrong!</div>
  }
  return <Component />
}
