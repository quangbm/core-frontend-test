import React, { FC } from 'react'
import styled from 'styled-components/macro'
import { spacing } from '../../../../components/GlobalStyle'
import { ROOT_ELEMENT } from '../../../FlowBuilder/FlowBuilderItem/ScreenBuilder/utils/builder'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { ViewUnderConstruction } from '../../contexts/useRenderView'
import { getScreenViewItemFromElement, RendererType } from '../ViewItems'

const ScreenBody = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: ${spacing('md')};

  & > div {
    margin-top: ${spacing('xs')};
    margin-bottom: ${spacing('xs')};
  }
`
export const ScreenFlowLayoutRoot: FC = () => {
  const children = useElementRenderer()
  return <ScreenBody>{children}</ScreenBody>
}

export function useElementRenderer(elementId: string = ROOT_ELEMENT) {
  const { elements } = useFlowViewer()
  const element = elements[elementId]

  return (
    element.children &&
    element.children.map((id, idx) => {
      const Component: RendererType | undefined = getScreenViewItemFromElement(elements[id])

      if (!Component) {
        return <ViewUnderConstruction key={id} />
      }

      return <Component key={id} elementId={id} parentId={elementId} index={idx} />
    })
  )
}
