import { get } from 'lodash'
import * as React from 'react'
import { Controller, useFieldArray, useFormContext } from 'react-hook-form'
import styled from 'styled-components/macro'
import { color } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NCurrencyInput } from '../../../../components/NCurrencyInput'
import { NDivider } from '../../../../components/NDivider'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { integerRegex } from '../../../../utils/regex'
import { numberFormat } from '../../../../utils/utils'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'
import { Denomination, DenominationTill, inText, Notes, suggestBillAmount } from './utils'

const RowWrapper = styled('div')`
  overflow: auto;
`

const AmountRowWrapper = styled('div')`
  display: flex;
  & > :first-child {
    flex: 2;
  }
`

const Row = styled('div')`
  display: flex;
  align-items: flex-end;
  & > :not(:last-child) {
    margin-right: 8px;
  }
  & > :nth-child(2) {
    flex: 1;
    width: 100%;
  }
  & > :last-child {
    padding: 0 8px;
  }
  &:not(:last-child) {
    margin-bottom: 8px;
  }
`

const Error = styled('div')`
  color: ${color('danger')};
`

// type ElementProps = {
//   defaultValue: ContentValue
//   denom: ContentValue
//   denomTill: ContentValue
//   totalBalance: ContentValue
//   currencyCode: ContentValue
//   isRequired: boolean
//   shouldValidateBalance: ContentValue
//   shouldAutoFill: ContentValue
// }

export const AmountInput = React.memo(function Node({ elementId }: RendererPropsType) {
  const { context, elements, screenName } = useFlowViewer()
  const element = elements[elementId]

  const {
    shouldValidateBalance: { value: shouldValidateBalanceKey } = { value: '' },
    shouldAutoFill: { value: shouldAutoFillKey } = { value: '' },
    defaultValue: { value: defaultValueKey } = { value: '' },
    denom: { value: denomKey } = { value: '' },
    denomTill: { value: denomTillKey } = { value: '' },
    currencyCode: { value: currencyCodeKey } = { value: '' },
    totalBalance: { value: totalBalanceKey } = { value: '' },
    isRequired,
    label,
  } = element.props

  const {
    setValue,
    watch,
    setError,
    clearErrors,
    control,
    formState: { errors },
  } = useFormContext()
  const notesName = `${screenName}.${element.name}.notes`
  const { fields, append, remove } = useFieldArray({ control, name: notesName })

  const totalName = `${screenName}.${element.name}.total`

  const amount = watch(totalName)
  const values = watch(notesName)

  const [
    {
      denomByIds = {},
      noteOptions = [],
      notesValue = {},
      notesCount = {},
      defaultValue,
      totalBalance = 0,
      currencyCode = '',
      shouldValidateBalance,
      shouldAutoFill,
    },
    errorMsg,
  ] = React.useMemo<
    [
      Partial<{
        defaultValue: number
        noteOptions: Array<SelectOption>
        notesValue: Notes
        notesCount: Notes
        totalBalance: number
        currencyCode: string
        denomByIds: Record<string, Denomination>
        shouldValidateBalance: boolean
        shouldAutoFill: boolean
      }>,
      string,
    ]
  >(() => {
    if (context) {
      try {
        let denom = (get(context, denomKey) || '[]') as string | Array<Denomination>
        if (typeof denom === 'string') {
          denom = JSON.parse(denom) as Array<Denomination>
        }
        const noteOptions: SelectOption[] = []
        const denomByIds: Record<string, Denomination> = {}
        const notesValue: Record<string, number> = {}
        for (const item of denom) {
          const { name, guid, value } = item
          noteOptions.push({ label: name, value: guid })
          denomByIds[guid] = item
          notesValue[guid] = value
        }
        let denomNotes = (get(context, denomTillKey) || '[]') as string | Array<DenominationTill>
        if (typeof denomNotes === 'string') {
          denomNotes = JSON.parse(denomNotes) as Array<DenominationTill>
        }

        const notesCount: Record<string, number> = {}
        if (denomNotes.length > 0) {
          for (const item of denomNotes) {
            const { bankNote, amount } = item
            notesCount[bankNote] = amount
          }
        } else {
          for (const item of denom) {
            const { guid } = item
            notesCount[guid] = Number.MAX_SAFE_INTEGER
          }
        }
        const defaultValue = get(context, defaultValueKey)
        const totalBalance = get(context, totalBalanceKey, 0)
        const currencyCode = get(context, currencyCodeKey, '')
        const shouldValidateBalance = Boolean(get(context, shouldValidateBalanceKey, true))
        const shouldAutoFill = Boolean(get(context, shouldAutoFillKey, true))
        return [
          {
            defaultValue,
            denom,
            denomNotes,
            denomByIds,
            noteOptions,
            notesValue,
            notesCount,
            totalBalance,
            currencyCode,
            shouldValidateBalance,
            shouldAutoFill,
          },
          '',
        ]
      } catch (e) {
        console.error('AmountInput', e.message)
        return [{}, `Cannot parse value of ${denomKey} or ${denomTillKey}!`]
      }
    }
    return [{}, 'Context not found!']
  }, [
    context,
    denomKey,
    denomTillKey,
    defaultValueKey,
    totalBalanceKey,
    currencyCodeKey,
    shouldValidateBalanceKey,
    shouldAutoFillKey,
  ])

  const notesAmount = React.useMemo(() => {
    return (values || []).reduce((sum: number, i: { bankNote: string; amount: number }) => {
      return sum + notesValue[i.bankNote] * i.amount
    }, 0)
  }, [values, notesValue])

  // Show error for notes fields
  React.useEffect(() => {
    if (Number(amount) !== notesAmount && Number(amount) > 0) {
      setError('money', { message: 'Invalid count!' })
      return
    }
    clearErrors('money')
  }, [amount, clearErrors, notesAmount, setError])

  const handleSuggestBill = React.useCallback(() => {
    try {
      const suggestedBill = suggestBillAmount(Number(amount), notesCount, notesValue)
      const suggestedFields = [] as { bankNote: string; amount: number }[]
      for (const [bankNote, amount] of Object.entries(suggestedBill)) {
        if (amount > 0) {
          suggestedFields.push({ bankNote, amount })
        }
      }
      setValue(notesName, suggestedFields)
    } catch (e) {}
  }, [amount, notesCount, notesName, notesValue, setValue])

  React.useEffect(() => {
    if (typeof defaultValue === 'number') {
      handleSuggestBill()
    }
  }, [defaultValue, handleSuggestBill])

  return (
    <React.Fragment>
      <AmountRowWrapper>
        <Controller
          control={control}
          name={totalName}
          defaultValue={defaultValue || ''}
          rules={{
            required:
              (isRequired && {
                value: true,
                message: 'Required!',
              }) ||
              undefined,
            max:
              (shouldValidateBalance && {
                value: totalBalance,
                message: 'Not enough money',
              }) ||
              undefined,
            pattern: {
              value: integerRegex,
              message: 'Invalid value',
            },
          }}
          render={({ field, fieldState }) => (
            <NCurrencyInput
              label={label}
              value={field.value}
              disabled={Boolean(errorMsg)}
              min={0}
              required={isRequired}
              formatter={value => `${numberFormat((value || '').toString())}`}
              onChange={v => {
                field.onChange(+v)
              }}
              onBlur={() => {
                field.onBlur()
                if (shouldAutoFill) {
                  handleSuggestBill()
                  return
                }
              }}
              error={fieldState.error?.message}
            />
          )}
        />
        <NDivider vertical />
        <div style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
          <div className="t">Currency</div>
          <div className="v" style={{ display: 'flex', flex: 1, alignItems: 'center' }}>
            {currencyCode}
          </div>
        </div>
      </AmountRowWrapper>
      <RowWrapper>
        {fields.map((field, fieldIndex) => {
          const fieldValue = watch(`${notesName}[${fieldIndex}]`) || {}
          return (
            <Row key={field.id}>
              <Controller
                control={control}
                name={`${notesName}[${fieldIndex}].bankNote`}
                defaultValue={fieldValue.bankNote || ''}
                render={({ field: { value, onChange } }) => {
                  return (
                    <NSingleSelect
                      label={fieldIndex === 0 ? 'Money' : undefined}
                      options={noteOptions}
                      value={value}
                      onValueChange={bankNote => {
                        onChange(bankNote)
                        setValue(`${notesName}[${fieldIndex}].name`, (bankNote && denomByIds[bankNote]?.name) || '')
                      }}
                    />
                  )
                }}
              />
              <Controller
                control={control}
                name={`${notesName}[${fieldIndex}].amount`}
                defaultValue={fieldValue.amount || 0}
                render={({ field: { value, onChange } }) => {
                  return (
                    <NTextInput
                      label={fieldIndex === 0 ? 'Note' : undefined}
                      disabled={!fieldValue.bankNote}
                      value={value}
                      onChange={e => onChange(e.target.value)}
                    />
                  )
                }}
              />
              {fields.length > 1 && (
                <NButton
                  htmlType="button"
                  type="ghost"
                  onClick={() => {
                    remove(fieldIndex)
                  }}>
                  <GlobalIcons.Trash />
                </NButton>
              )}
            </Row>
          )
        })}
        <NButton
          type="primary"
          size="small"
          onClick={() => append({ bankNote: '', amount: 0, currencyCode, name: '' })}>
          +
        </NButton>
      </RowWrapper>
      {(errors['money'] || errorMsg) && <Error>{errorMsg || errors['money']?.message}</Error>}
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 1 }}>
          <div className="t">Total</div>
          <div className="v">
            {numberFormat(notesAmount)} {currencyCode}
          </div>
        </div>
        <div style={{ flex: 2 }}>
          <div className="t">In Text</div>
          <div className="v">
            {Number.isNaN(Number(amount)) ? '' : inText(Number(amount), currencyCode)}{' '}
            {(currencyCode === 'USD' && `dollar${(Number(amount) > 1 && 's') || ''}`) || 'Dong'}
          </div>
        </div>
      </div>
    </React.Fragment>
  )
})

AmountInput.displayName = 'AmountInput'
