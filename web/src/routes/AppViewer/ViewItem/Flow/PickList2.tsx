import get from 'lodash/get'
import React, { FC } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { SelectOption } from '../../../../components/NSelect/model'
import { NMultiSelect } from '../../../../components/NSelect/NMultiSelect'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

export const PickList2: FC<RendererPropsType> = ({ elementId }) => {
  const { elements, context, screenName } = useFlowViewer()
  const formMethods = useFormContext()

  const [filter, setFilter] = React.useState('')

  const element = elements[elementId]
  const {
    defaultValue: { value: defaultValueKey } = { value: '' },
    data: { value: dataKey } = { value: '' },
    labelKey,
    options: _o,
    label,
    selectionType: type,
    isRequired,
    ...restProps
  } = element.props

  const { defaultValue, options } = React.useMemo(() => {
    if (context) {
      let data = (get(context, dataKey) || '[]') as Array<any>
      if (typeof data === 'string') {
        data = JSON.parse(data) as Array<any>
      }
      const options: Array<SelectOption> = data.map((data, dataIndex) => ({
        value: data.guid,
        label: get(data, labelKey, data.name || `Option ${dataIndex}`),
      }))
      const defaultValue = [get(context, defaultValueKey)]
      return {
        options,
        defaultValue,
      }
    }
    return {
      options: [],
    }
  }, [defaultValueKey, dataKey, labelKey, context])

  const filteredOptions = filter
    ? options.filter(i => typeof i.label === 'string' && i.label.toLowerCase().includes(filter.toLowerCase()))
    : options

  return (
    <Controller
      control={formMethods.control}
      name={`${screenName}.${element.name!}`}
      defaultValue={defaultValue}
      render={({ field: { value, onChange }, fieldState }) => {
        if (type === 'multi') {
          return (
            <NMultiSelect
              fullWidth
              values={value || []}
              error={fieldState.error?.message}
              options={filteredOptions}
              required={isRequired}
              onValuesChange={newVals => {
                onChange(newVals)
              }}
              label={label}
              {...restProps}
            />
          )
        }
        return (
          <NSingleSelect
            fullWidth
            value={value && value[0]}
            error={fieldState.error?.message}
            options={filteredOptions}
            required={isRequired}
            onValueChange={newVal => {
              onChange([newVal])
            }}
            isSearchable
            searchValue={filter}
            onSearchValueChange={setFilter}
            label={label}
            {...restProps}
          />
        )
      }}
    />
  )
}
