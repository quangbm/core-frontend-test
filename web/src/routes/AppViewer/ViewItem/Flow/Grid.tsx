import styled from 'styled-components/macro'
import { spacing } from '../../../../components/GlobalStyle'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'
import { useElementRenderer } from './ScreenFlowLayoutRoot'

const Wrapper = styled('div')`
  display: grid;
  grid-template-rows: minmax(0, 1fr);
  align-items: start;
  grid-gap: ${spacing('sm')};
`

export function Grid({ elementId }: RendererPropsType) {
  const { elements } = useFlowViewer()
  const element = elements[elementId]

  const children = useElementRenderer(elementId)

  return (
    <Wrapper style={{ gridTemplateColumns: `repeat(${element.props.columns}, minmax(0, 1fr))` }}>{children}</Wrapper>
  )
}
