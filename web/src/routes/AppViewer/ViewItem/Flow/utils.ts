import numberInUSWords from 'num-words'

export type Denomination = {
  guid: string
  value: number
  name: string
}

export type DenominationTill = {
  bankNote: string
  amount: number
}
export type Notes = Record<string, number>

function getCountedNote(amount: number, notesCount: Notes, notesValue: Notes) {
  const notes = Object.keys(notesCount)
  const totalNotesValue = notes.reduce((a, i) => a + notesValue[i] * notesCount[i], 0)
  if (totalNotesValue < amount) {
    throw Error('getCountedNote: Not Enough Money')
  }
  let restAmount = amount
  const result = {} as Notes
  for (const note of notes) {
    const noteCount = Math.min(notesCount[note], Math.floor(restAmount / notesValue[note]))
    restAmount = restAmount - noteCount * notesValue[note]
    result[note] = noteCount
  }
  if (restAmount > 0) {
    throw Error('getCountedNote: Not Enough Count')
  }
  return result
}

export function suggestBillAmount(amount: number, notesCount: Notes, notesValue: Notes) {
  const notes = Object.keys(notesCount)
  const availableNotes = notes.filter(note => notesCount[note] > 0).sort((a, b) => notesValue[b] - notesValue[a])

  if (availableNotes.length < 1) {
    throw Error('Not enough money')
  }

  const limits = {} as Notes
  const halfLimits = {} as Notes
  for (const [index, note] of availableNotes.entries()) {
    limits[note] = notesCount[note]
    halfLimits[note] = index === 0 || index === 1 ? Math.floor(notesCount[note] / 2) : notesCount[note]
  }

  try {
    return getCountedNote(amount, halfLimits, notesValue)
  } catch (e) {
    return getCountedNote(amount, limits, notesValue)
  }
}

export function numberInVNWords(raw: number) {
  const postfix = ['Ty', 'Trieu', 'Nghin', 'Dong']
  const numbers = ['Khong', 'Mot', 'Hai', 'Ba', 'Bon', 'Nam', 'Sau', 'Bay', 'Tam', 'Chin']
  const padded = raw.toString().padStart(12, '0')
  const groupped = padded.match(/.{1,3}/g)
  function getBlock(input: string) {
    const value = Number(input)
    if (Number.isNaN(value) || value < 1) {
      return
    }
    const hundress = Math.floor(value / 100)
    const tens = Math.floor((value % 100) / 10)
    const ones = value % 10
    const result = []
    if (hundress > 0) {
      result.push(`${numbers[hundress]} tram`)
    }
    if (tens > 1) {
      result.push(`${numbers[tens]} muoi`)
    }
    if (hundress > 0 && tens === 1) {
      result.push(`Muoi ${numbers[tens].toLocaleLowerCase()}`)
    }
    if (hundress < 1 && tens === 1) {
      result.push(`Muoi`)
    }
    if (ones > 0) {
      result.push(`${numbers[ones]}`)
    }
    return result.join(' ')
  }
  if (groupped) {
    return groupped?.reduce((a, i, idx) => {
      const inText = getBlock(i)
      if (inText) {
        return `${a} ${inText} ${postfix[idx]}`
      }
      return a
    }, '')
  }
  return `${raw}`
}

export function inText(value: number, currencyCode: string) {
  const fns: Record<string, (v: number) => string> = {
    USD: numberInUSWords,
    VND: numberInVNWords,
  }
  if (fns[currencyCode]) {
    return fns[currencyCode](value)
  }
  return fns['USD'](value)
}
