import React, { FC } from 'react'
import { useFormContext } from 'react-hook-form'
import styled from 'styled-components/macro'
import { InputProps } from '../../../../../../shared/src/api'
import { GlobalIcons } from '../../../../components/Icons'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { renderVariableValue } from '../../../FlowBuilder/utils/functions'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

const Icons = styled('div')`
  cursor: pointer;
`

const resolveHtmlType = (inputType: InputProps['inputType'] | undefined) => {
  switch (inputType) {
    case 'string':
      return 'text'
    case 'integer':
    case 'float':
      return 'number'
  }
}

export const Input: FC<RendererPropsType> = ({ elementId }) => {
  const { elements, content, screenName } = useFlowViewer()

  const {
    register,
    formState: { errors },
  } = useFormContext()

  const element = elements[elementId]
  const { name } = element
  const { defaultValue, inputType, isRequired, isHidden, isSensitivity, ...restProps } = element.props

  const [hidden, setHidden] = React.useState(true)

  if (isHidden || !name) {
    return null
  }

  return (
    <NTextInput
      {...register(`${screenName}.${name}`)}
      defaultValue={renderVariableValue(defaultValue, content)}
      error={errors[screenName]?.[name]?.message}
      required={isRequired}
      right={
        isSensitivity && (
          <Icons onClick={() => setHidden(prev => !prev)}>
            {hidden ? <GlobalIcons.CloseEye /> : <GlobalIcons.Eye />}
          </Icons>
        )
      }
      type={isSensitivity && hidden ? 'password' : resolveHtmlType(inputType)}
      {...restProps}
    />
  )
}
