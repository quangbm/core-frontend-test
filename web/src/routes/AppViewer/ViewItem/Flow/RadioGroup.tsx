import React, { FC } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import styled from 'styled-components/macro'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NRadio } from '../../../../components/NRadio/NRadio'
import { SelectOption } from '../../../../components/NSelect/model'
import { NTypography, RequiredIndicator } from '../../../../components/NTypography'
import { renderVariableValue } from '../../../FlowBuilder/utils/functions'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

const RadioContainer = styled.div`
  display: flex;
  flex-direction: column;

  .radio {
    margin-top: ${spacing('md')};
  }
`

const ErrorMsg = styled(NTypography.InputCaption)`
  color: ${color('Red700')};
`

export const RadioGroup: FC<RendererPropsType> = ({ elementId }) => {
  const { elements, content, screenName } = useFlowViewer()
  const formMethods = useFormContext()

  const element = elements[elementId]
  const { label, defaultValue: _, defaultIndexes, options: _o, isRequired } = element.props

  const transformOptions: SelectOption[] = (element.props.options || []).map(op => {
    const label = renderVariableValue(op, content)
    const value = (content && content[op.value]) || op.value
    return { label, value }
  })

  const defaultValue =
    transformOptions && defaultIndexes && defaultIndexes[0] ? transformOptions[defaultIndexes[0]].value : undefined

  return (
    <Controller
      control={formMethods.control}
      name={`${screenName}.${element.name!}`}
      defaultValue={defaultValue}
      render={({ field: { value, onChange }, fieldState }) => {
        return (
          <RadioContainer>
            <NRow justify="space-between">
              <NTypography.InputLabel>
                {isRequired && <RequiredIndicator>*</RequiredIndicator>}
                {label}
              </NTypography.InputLabel>
            </NRow>
            {fieldState.error?.message && <ErrorMsg>{fieldState.error?.message}</ErrorMsg>}
            {(transformOptions || []).map((op: SelectOption) => {
              return (
                <NRadio
                  className="radio"
                  key={`${elementId}_radio_group_${op.value}`}
                  title={op.value}
                  name={element.name!}
                  value={op.value}
                  checked={value === op.value}
                  onChange={onChange}
                />
              )
            })}
          </RadioContainer>
        )
      }}
    />
  )
}
