import { nanoid } from 'nanoid'
import * as React from 'react'
import { useFormContext } from 'react-hook-form'
import styled from 'styled-components/macro'
import { GlobalIcons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { ZoneProps } from '../../../LayoutBuilder/utils/models'
import { useFlowViewer } from '../../contexts/FlowViewerContext'

const RowWrapper = styled('div')`
  overflow: auto;
`
const Row = styled('div')`
  display: flex;
  align-items: flex-end;
  & > :not(:last-child) {
    margin-right: 8px;
  }
  & > :nth-child(2) {
    flex: 1;
    width: 100%;
  }
  & > :last-child {
    padding: 0 8px;
  }
  &:not(:last-child) {
    margin-bottom: 8px;
  }
`

const NOTES = ['b500', 'b200', 'b100', 'b50', 'b20', 'b10'] as const
type Notes = typeof NOTES[number]
const NOTES_VALUE: Record<Notes, number> = {
  b500: 500000,
  b200: 200000,
  b100: 100000,
  b50: 50000,
  b20: 20000,
  b10: 10000,
}
const NOTES_LABEL: Record<Notes, string> = {
  b500: '500.000',
  b200: '200.000',
  b100: '100.000',
  b50: '50.000',
  b20: '20.000',
  b10: '10.000',
}

// function numberInText(raw: string | number) {
//   const postfix = ['Ty', 'Trieu', 'Nghin', 'Viet Nam Dong']
//   const numbers = ['Khong', 'Mot', 'Hai', 'Ba', 'Bon', 'Nam', 'Sau', 'Bay', 'Tam', 'Chin']
//   const padded = raw.toString().padStart(12, '0')
//   const groupped = padded.match(/.{1,3}/g)
//   function getBlock(input: string) {
//     const value = Number(input)
//     if (Number.isNaN(value) || value < 1) {
//       return
//     }
//     const tram = Math.floor(value / 100)
//     const chuc = Math.floor((value % 100) / 10)
//     const donVi = value % 10
//     return [
//       (tram > 0 && `${numbers[tram]} tram`) || '',
//       (chuc > 0 && `${numbers[chuc]} muoi`) || '',
//       donVi > 0 && `${numbers[donVi]}`,
//     ].join(' ')
//   }
//   if (groupped) {
//     return groupped?.reduce((a, i, idx) => {
//       const inText = getBlock(i)
//       if (inText) {
//         return `${a} ${inText} ${postfix[idx]}`
//       }
//       return a
//     }, '')
//   }
//   return raw
// }

function getTotal(fields: Array<{ id: string; note?: Notes; value: string }>) {
  return fields.reduce((sum, i) => {
    const v = Number(i.value)
    if (!v || !i.note) {
      return sum
    }
    return sum + NOTES_VALUE[i.note] * v
  }, 0)
}

export const Withdrawal = React.memo(function Node({ elementId }: ZoneProps) {
  const { context, elements } = useFlowViewer()
  const element = elements[elementId]

  const isWithdrawal = element.props.isWithdrawal

  const { setValue, watch, setError, clearErrors } = useFormContext()
  const [localError, setLocalError] = React.useState('')
  const [fields, setFields] = React.useState<Array<{ id: string; note?: Notes; value: string }>>([
    { id: nanoid(), note: undefined, value: '0' },
  ])

  const amount = watch('amount')

  const noteValues = React.useMemo(
    () => ({
      balance: Number(context?.getSpace.balances) || 0,
      b500: Number(context?.getTill.b500) || 0,
      b200: Number(context?.getTill.b200) || 0,
      b100: Number(context?.getTill.b100) || 0,
      b50: Number(context?.getTill.b50) || 0,
      b20: Number(context?.getTill.b20) || 0,
      b10: Number(context?.getTill.b10) || 0,
    }),
    [context],
  )

  const total = React.useMemo(() => getTotal(fields), [fields])

  React.useEffect(() => {
    if (noteValues.balance < amount && isWithdrawal) {
      setLocalError('Not enough balance!')
      setError('error', { message: 'Error' })
      return
    }
    if (Number(amount) !== total && Number(amount) > 0) {
      setLocalError('Invalid count!')
      setError('error', { message: 'Error' })
      return
    }
    setLocalError('')
    clearErrors('error')
  }, [noteValues.balance, amount, total, clearErrors, setError, isWithdrawal])

  React.useEffect(() => {
    if (isWithdrawal) {
      setValue(`updatedBalance`, `${noteValues.balance - total}`)
    } else {
      setValue(`updatedBalance`, `${noteValues.balance + total}`)
    }
  }, [noteValues, total, setValue, isWithdrawal])

  React.useEffect(() => {
    NOTES.map(note => {
      setValue(`updated${note}`, `${noteValues[note]}`)
    })
    setValue(`updatedBalance`, `${noteValues.balance}`)
  }, [noteValues, setValue])

  return (
    <React.Fragment>
      <RowWrapper>
        {fields.map((field, fieldIndex) => {
          return (
            <Row key={field.id}>
              <NSingleSelect
                label={fieldIndex === 0 ? 'Money' : undefined}
                options={NOTES.map(i => ({
                  value: i,
                  label: NOTES_LABEL[i],
                }))}
                value={field.note || ''}
                onValueChange={newNote => {
                  setFields(prev =>
                    prev.map(p => {
                      if (p.id === field.id) {
                        return {
                          ...p,
                          note: newNote as Notes,
                          value: '0',
                        }
                      }
                      return p
                    }),
                  )
                }}
              />
              <NTextInput
                label={fieldIndex === 0 ? 'Note' : undefined}
                disabled={!field.note}
                value={field.value}
                onChange={e => {
                  setFields(prev =>
                    prev.map(p => {
                      if (p.id === field.id) {
                        return {
                          ...p,
                          value: e.target.value,
                        }
                      }
                      return p
                    }),
                  )
                  if (e.target.value) {
                    const value = Number(e.target.value)
                    const noteValue = noteValues[field.note!]
                    if (Number.isNaN(value) || noteValue - value < 0) {
                      return
                    }
                    setValue(field.note!, `${value}`)
                    if (isWithdrawal) {
                      setValue(`updated${field.note}`, `${noteValue - value}`)
                    } else {
                      setValue(`updated${field.note}`, `${noteValue + value}`)
                    }
                  }
                }}
              />
              {fields.length > 1 && (
                <NButton
                  htmlType="button"
                  type="ghost"
                  onClick={() => {
                    setFields(prev => prev.filter(i => i.id !== field.id))
                  }}>
                  <GlobalIcons.Trash />
                </NButton>
              )}
            </Row>
          )
        })}
        <NButton
          type="primary"
          size="small"
          onClick={() => setFields(prev => [...prev, { id: nanoid(), note: undefined, value: '' }])}>
          +
        </NButton>
      </RowWrapper>
      {localError && <div style={{ color: 'red' }}>{localError}</div>}
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 1 }}>
          <div className="t">Total</div>
          <div className="v">{total}</div>
        </div>
        <div style={{ flex: 1 }}>
          <div className="t">In Text</div>
          <div className="v">{total}</div>
        </div>
      </div>
    </React.Fragment>
  )
})
