import Papaparse from 'papaparse'
import * as React from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

export function CSVInput({ elementId }: RendererPropsType) {
  const { elements, screenName } = useFlowViewer()
  const { control } = useFormContext()
  const element = elements[elementId]

  const { label } = element.props

  return (
    <React.Fragment>
      <Controller
        name={`${screenName}.${element.name!}`}
        control={control}
        render={({ field: { onChange }, fieldState }) => (
          <NTextInput
            type="file"
            label={label}
            accept="text/csv"
            onChange={e => {
              if (e.target.files) {
                Papaparse.parse<any>(e.target.files[0], {
                  header: true,
                  skipEmptyLines: true,
                  complete(result) {
                    onChange(result.data)
                  },
                })
              }
            }}
            error={fieldState.error?.message}
          />
        )}
      />
    </React.Fragment>
  )
}
