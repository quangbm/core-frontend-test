import React, { FC } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { InputProps } from '../../../../../../shared/src/api'
import { NCheckbox } from '../../../../components/NCheckbox/NCheckbox'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

export const Checkbox: FC<RendererPropsType> = ({ elementId }) => {
  const { elements, screenName } = useFlowViewer()
  const formMethods = useFormContext()

  const element = elements[elementId]
  const { label, defaultValue, isRequired: _, ...restProps } = element.props as InputProps

  return (
    <Controller
      control={formMethods.control}
      name={`${screenName}.${element.name!}`}
      defaultValue={typeof defaultValue === 'boolean' ? defaultValue : false}
      render={({ field: { value, onChange } }) => {
        return (
          <NCheckbox
            checked={value}
            value={value}
            onChange={event => {
              onChange(event.target.checked)
            }}
            title={label}
            {...restProps}
          />
        )
      }}
    />
  )
}
