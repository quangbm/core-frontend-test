import { Controller, useFormContext } from 'react-hook-form'
import { useQuery } from 'react-query'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../services/api'
import { useFlowViewer } from '../../contexts/FlowViewerContext'
import { RendererPropsType } from './model'

export function DataLookup({ elementId }: RendererPropsType) {
  const { elements, screenName } = useFlowViewer()
  const { control } = useFormContext()
  const element = elements[elementId]
  const { label, objName, filters, isRequired } = element.props

  const [{ searchValue, searchText }, handleSearchChange] = useSearchText('')

  const { data = [], isLoading } = useQuery(
    [
      QueryKeys.Data.searchObjectData,
      {
        name: objName!,
        limit: 10,
        searchText,
        filters,
      },
    ],
    APIServices.Data.searchObjectData,
    {
      select(response) {
        const options = [] as Array<SelectOption>
        for (const option of response.data.data || []) {
          options.push({
            // name is not included in DataResponse type
            // @ts-ignore
            label: option.name,
            value: option.guid,
          })
        }
        return options
      },
    },
  )

  return (
    <Controller
      name={`${screenName}.${element.name!}`}
      control={control}
      render={({ field, fieldState }) => (
        <NSingleSelect
          required={isRequired}
          options={data}
          isLoading={isLoading}
          value={field.value}
          onValueChange={field.onChange}
          isSearchable
          searchValue={searchValue}
          onSearchValueChange={handleSearchChange}
          label={label}
          error={fieldState.error?.message}
        />
      )}
    />
  )
}
