import React, { FC, useEffect, useMemo } from 'react'
import { useQuery } from 'react-query'
import { NavLink, useHistory } from 'react-router-dom'
import { Column } from 'react-table'
import styled from 'styled-components/macro'
import { FieldSchema } from '../../../../../../shared/src/api'
import { color, spacing } from '../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../components/Icons'
import { NButton } from '../../../../components/NButton/NButton'
import { FilterItem } from '../../../../components/NConditionBuilder'
import { CurrencyData } from '../../../../components/NCurrency/CurrencyData'
import { NDivider } from '../../../../components/NDivider'
import { NFilterBuilder } from '../../../../components/NFilterBuilder'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NHyperlinkOrText } from '../../../../components/NHyperlinkOrText'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../components/NPagination'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSpinner } from '../../../../components/NSpinner/NSpinner'
import { NTable, NTableColumnType } from '../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName, NTableTagItem } from '../../../../components/NTable/NTableStyledContainer'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../components/NToast'
import { typography } from '../../../../components/NTypography'
import { useLocalTableConfigs } from '../../../../hooks/useLocalTableConfigs'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../services/api'
import { getTableData } from '../../../../utils/table-utils'
import { capitalize, getDateFromIsoString, getStringFromDate } from '../../../../utils/utils'
import { CardList } from '../../../LayoutBuilder/components/ZoneItems/Components/ObjectList/CartList'
import { ZoneProps } from '../../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../../contexts/LayoutViewerContext'
import { useRenderView } from '../../contexts/useRenderView'
import { ComponentContainerClassName, CustomCss } from '../../model'
import { getCustomCss, resolveFilterVariable, resolveTextVariable } from '../../utils'
import { JsonPreview } from './JsonPreview'
import { ListViewTableActions } from './ListViewTableActions'

export const SearchWrapper = styled('div')`
  display: flex;
  & > *:first-child {
    flex: 1;
  }
`

const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${spacing('md')};
`
const Title = styled.div`
  ${typography('h500')};
`
const Description = styled.div`
  ${typography('ui-text')};
  color: ${color('Neutral500')};
  margin-bottom: ${spacing('md')};
`

const ErrorText = styled.p`
  color: ${color('Red700')};
  ${typography('overline')}
`

const ActionWrapper = styled.div`
  display: flex;

  & > button {
    margin-left: ${spacing('xs')};
    margin-right: ${spacing('xs')};
  }

  & > button:first-of-type {
    margin-left: 0;
  }

  & > button:last-of-type {
    margin-right: 0;
  }
`

export const ListViewWrapper = styled('div')<CustomCss>`
  display: flex;
  flex-direction: column;
  padding: ${spacing('md')};
  background-color: ${color('white')};
  border-radius: 4px;
  ${getCustomCss}
`

type ElementProps = {
  objName?: string
  limit?: number
  fields: string[]
  filters?: FilterItem[][]
  rowActions?: string[]
} & CustomCss

export const ListView: FC<ZoneProps> = ({ elementId }) => {
  const { elements, inputMap } = useLayoutViewer()
  const history = useHistory()

  const element = elements[elementId]
  const actions = useRenderView(elementId)

  const { objName, limit, fields, filters, rowActions, customCss } = (element?.props || {
    objName: undefined,
    limit: 10,
  }) as ElementProps

  const [{ searchText, searchValue }, onChangeSearch] = useSearchText()
  const [{ sort, currentPage }, { onChangeSort, onChangePage }] = useLocalTableConfigs()

  const [localFilter, setLocalFilter] = React.useState<FilterItem[][]>()
  const { isSearchable, fieldNameOptions, fieldsByName } = transformSchema(element.schema?.fields)

  const mapFilter = resolveFilterVariable(
    // Because server's type suck :()
    // @ts-ignore
    filters || [],
    inputMap,
  ).concat(
    // Because server's type suck :()
    // @ts-ignore
    localFilter || [],
  )

  // get content list
  const { data: filteredData, isLoading: isLoadingFiltered } = useQuery(
    [
      QueryKeys.Data.searchObjectData,
      {
        name: objName!,
        limit,
        offset: limit ? (currentPage - 1) * limit : 0,
        searchText,
        filters: mapFilter,
        sortBy: sort?.sortBy || element.props.sortBy,
        sortOrder: sort?.sortOrder || (element.props.sortBy ? element.props.sortOrder : undefined),
      },
    ],
    APIServices.Data.searchObjectData,
    {
      onError: error => {
        NToast.error({ title: 'Get object data error!', subtitle: error.response?.data.message })
      },
      enabled: Boolean(objName),
    },
  )

  useEffect(() => {
    onChangePage(1)
  }, [searchText])

  // this is neccessary because of react table :(
  const { pageData, pageInfo, totalPage } = useMemo(() => {
    return getTableData(filteredData)
  }, [filteredData])

  const columns = React.useMemo(() => {
    return getTableColumns(elementId, fields, fieldsByName, rowActions)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [elementId, fields, rowActions]) as NTableColumnType<any>

  if (!element) {
    return <ErrorText>Cannot find element</ErrorText>
  }

  if (!element.schema) {
    return <ErrorText>Script missing fields schema</ErrorText>
  }

  const titleText = resolveTextVariable(element.props.title, inputMap) || element.schema?.displayName
  const descriptionText =
    resolveTextVariable(element.props.description, inputMap) || `${pageInfo.total} ${element.schema?.displayName}`
  return (
    <ListViewWrapper className={ComponentContainerClassName} customCss={customCss}>
      <HeaderContainer>
        <Title>
          {titleText || (
            <NRow align="center">
              Loading
              <NDivider vertical size="md" />
              <NSpinner size={20} strokeWidth={4} />
            </NRow>
          )}
        </Title>
        <ActionWrapper>{actions}</ActionWrapper>
      </HeaderContainer>
      <Description>{descriptionText}</Description>
      <NContainer.Content>
        {isSearchable && element.props.showSearch && (
          <SearchWrapper>
            <NTextInput
              left={<GlobalIcons.Search />}
              placeholder="Search"
              value={searchValue || ''}
              onChange={e => {
                onChangeSearch(e.target.value)
              }}
            />
            <NDivider size="md" vertical />
            <NFilterBuilder
              filters={localFilter}
              onSubmit={setLocalFilter}
              fieldNameOptions={fieldNameOptions}
              fieldsByName={fieldsByName}>
              <NButton type={localFilter && localFilter.length > 0 ? 'primary' : 'default'}>Filter by Column</NButton>
            </NFilterBuilder>
          </SearchWrapper>
        )}
        <NDivider size="md" />
        {element.props.displayMode === 'card' ? (
          <CardList
            data={pageData}
            columns={columns}
            isLoading={isLoadingFiltered}
            fields={element?.schema?.fields}
            onClick={data => {
              const id = data.guid || data.externalId
              if (!id) {
                NToast.error({ title: "Missing 'id' in the row data" })
                return
              }
              history.push(`/${objName}/${id}`)
            }}
          />
        ) : (
          <NTable
            isLoading={isLoadingFiltered}
            columns={columns as any}
            data={pageData}
            defaultSortBy={sort?.sortBy}
            defaultSortOrder={sort?.sortOrder}
            onChangeSort={onChangeSort}
            pageSize={limit}
            pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
            onClickRow={data => {
              const id = data.guid || data.externalId
              if (!id) {
                NToast.error({ title: "Missing 'id' in the row data" })
                return
              }
              history.push(`/${objName}/${id}`)
            }}
          />
        )}
      </NContainer.Content>
    </ListViewWrapper>
  )
}

const StyledNavLink = styled(NavLink)`
  text-decoration: underline;
  border-radius: 2px;
  padding: ${spacing('xxs')};
  margin: -${spacing('xxs')};
  color: ${color('Neutral700')};

  &:hover {
    background-color: ${color('Neutral300')};
  }
`

export function transformSchema(fields: Array<FieldSchema> = []) {
  const fieldsByName = {} as Record<string, FieldSchema>
  const fieldNameOptions = [] as SelectOption[]
  let isSearchable = false

  for (const field of fields) {
    fieldsByName[field.name] = field
    fieldNameOptions.push({ label: field.displayName, value: field.name })
    if (!isSearchable) {
      isSearchable = field.isSearchable || false
    }
  }

  return {
    fieldsByName,
    fieldNameOptions,
    isSearchable,
  }
}

const dateFormat = 'dd/MM/yyyy'
const timeFormat = 'HH:mm:ss'

export function getTableColumns(
  elementId: string,
  fields: string[],
  fieldsByName: Record<string, FieldSchema>,
  rowActions?: string[],
): Column<any>[] {
  const tableActions: Column<any> = {
    accessor: 'objName',
    Cell: ({ row }) => {
      return <ListViewTableActions parentId={elementId} data={row.original} rowActions={rowActions || []} />
    },
    id: NTableCollapsedCellClassName,
  }

  const columns = fields.map<Column<any>>(field => {
    const fieldData = fieldsByName[field]
    if (!fieldData) {
      return {
        Header: capitalize(field),
        Cell: () => `{Missing field}`,
      }
    }
    const { displayName: Header, isSortable: defaultCanSort, typeName, subType } = fieldData
    const accessor = field

    if (typeName === 'boolean') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value }) => (value ? <GlobalIcons.Check /> : null),
      }
    }
    if (typeName === 'dateTime') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = getDateFromIsoString(value, subType)
          return getStringFromDate({ date, type: subType, dateFormat, timeFormat })
        },
      }
    }
    if (typeName === 'pickList' && subType === 'multi') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value, row }) => {
          return (
            <NTableTagItem.Container data-type={`${typeName}|${subType}`}>
              {value?.map((val: string, index: number) => {
                return (
                  <NTableTagItem.Tag data-value={val} key={`${field}_${row.original.guid}_${index}`}>
                    {val}
                  </NTableTagItem.Tag>
                )
              })}
              <NTableTagItem.Blur />
            </NTableTagItem.Container>
          )
        },
      }
    }
    if (typeName === 'relation') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value, row }) => {
          const relation = row.original['$metadata']?.relations
          const link = `/${fieldData.value}/${relation[accessor]}`
          return (
            <StyledNavLink data-type={typeName} to={link} onClick={e => e.stopPropagation()}>
              {value}
            </StyledNavLink>
          )
        },
      }
    }
    if (typeName === 'currency') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value, row }) => {
          return row.original.currencyCode ? (
            <CurrencyData
              data-type={typeName}
              data-value={value}
              dataCurrencyCode={row.original.currencyCode}
              value={value}
            />
          ) : (
            value
          )
        },
      }
    }
    if (typeName === 'externalRelation' || typeName === 'indirectRelation') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value }) => {
          const link = `/${fieldData.value}/${value}`
          return (
            <StyledNavLink data-type={typeName} to={link} onClick={e => e.stopPropagation()}>
              {value}
            </StyledNavLink>
          )
        },
      }
    }
    if (typeName === 'json') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value }) => {
          return <JsonPreview value={value} field={fieldData.displayName} />
        },
      }
    }

    if (typeName === 'generated') {
      return {
        Header,
        accessor,
        defaultCanSort,
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          if (subType === 'date-time') {
            return value ? new Date(value).toLocaleString() : null
          }
          if (subType === 'timestamp') {
            return value ? new Date(value * 1000).toLocaleString() : null
          }
          return value
        },
      }
    }

    return {
      Header,
      accessor,
      defaultCanSort,
      Cell: ({ value }) => {
        if (typeof value === 'object') {
          return 'N/A'
        }
        return (
          <NHyperlinkOrText data-type={typeName} data-value={value}>
            {value ?? null}
          </NHyperlinkOrText>
        )
      },
    }
  })

  return (rowActions || []).length ? columns.concat(tableActions) : columns
}
