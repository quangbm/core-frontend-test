import { FC, useState } from 'react'
import { NButton } from '../../../../components/NButton/NButton'
import { NCodeEditor } from '../../../../components/NCodeEditor'
import { NModal } from '../../../../components/NModal/NModal'

type Props = {
  value: any
  field: string
}

export const JsonPreview: FC<Props> = ({ value, field }) => {
  const [togglePreview, setTogglePreview] = useState(false)

  return (
    <div
      onClick={e => {
        e.stopPropagation()
      }}>
      <NButton
        type="link"
        size="small"
        onClick={e => {
          e.stopPropagation()
          setTogglePreview(prev => !prev)
        }}>
        View JSON
      </NButton>
      {togglePreview && (
        <NModal visible={togglePreview} setVisible={setTogglePreview}>
          <NModal.Header onClose={() => setTogglePreview(false)} title={`JSON field: ${field}`} />
          <NModal.Body>
            <NCodeEditor
              style={{ maxHeight: '60vh' }}
              language="json"
              disabled
              value={JSON.stringify(value, null, 2)}
            />
          </NModal.Body>
        </NModal>
      )}
    </div>
  )
}
