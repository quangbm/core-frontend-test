import * as React from 'react'
import styled from 'styled-components/macro'
import { spacing } from '../../../components/GlobalStyle'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'
import { CustomCss } from '../model'
import { getCustomCss, resolveTextVariable } from '../utils'

const H = styled('h1')<CustomCss>`
  padding: ${spacing('md')};
  ${getCustomCss}
`

export const Heading: React.FC<ZoneProps> = ({ elementId }) => {
  const { elements, inputMap } = useLayoutViewer()
  const element = elements[elementId]
  return (
    <H as={element.props.level} customCss={element.props.customCss}>
      {resolveTextVariable(element.props.children, inputMap)}
    </H>
  )
}
