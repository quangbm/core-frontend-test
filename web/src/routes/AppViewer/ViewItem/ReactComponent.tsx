// @ts-ignore
import * as babel from '@babel/standalone'
import * as React from 'react'
// @ts-ignore
import Sass from 'sass.js'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'

function plugin({ types: t }: any) {
  return {
    visitor: {
      ExportDefaultDeclaration(path: any) {
        path.replaceWith(t.returnStatement(path.node.declaration))
      },
    },
  }
}

babel.registerPlugin('plugin', plugin)

function parseCode(codeString: string) {
  try {
    const { code } = babel.transform(codeString, {
      presets: ['react', 'env'],
      plugins: ['plugin'],
    })
    return code
  } catch (err) {
    return null
  }
}

function useSass(value: string) {
  const [status, setStatus] = React.useState('idle')
  const [css, setCSS] = React.useState('')
  React.useEffect(() => {
    async function getCSS(value: string) {
      return new Promise<string>(resolve => {
        Sass.compile(value, (result: any) => {
          resolve(result.text)
        })
      })
    }
    setStatus('transforming')
    getCSS(value)
      .then(setCSS)
      .finally(() => setStatus('idle'))
  }, [value])
  return {
    status,
    css,
  }
}

export function ReactComponent({ elementId }: { elementId: string }) {
  const { elements } = useLayoutViewer()
  const element = elements[elementId]
  const Component = React.useMemo(() => {
    const renderer = new Function('React', 'useSass', parseCode(element.props.value))
    return renderer(React, useSass)
  }, [element.props.value])
  return (
    <>
      <Component />
    </>
  )
}
