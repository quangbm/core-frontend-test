import React, { FC } from 'react'
import styled, { css } from 'styled-components/macro'
import { color } from '../../../components/GlobalStyle'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { ZoneProps } from '../../LayoutBuilder/utils/models'
import { useLayoutViewer } from '../contexts/LayoutViewerContext'
import { useRenderView } from '../contexts/useRenderView'
import { ComponentContainerClassName } from '../model'
import { getCustomCss } from '../utils'

const StyledRow = styled(NRow)<{ showBackground?: boolean; customCss?: string }>`
  ${props => {
    if (props.showBackground) {
      return css`
        background-color: ${color('white')};
        border-radius: 4px;
      `
    }
  }}
  ${props =>
    Boolean(props.customCss) &&
    css`
      ${props.customCss}
    `}
`

const SingleColumnWrapper = styled.div<{ showBackground?: boolean; customCss?: string }>`
  display: flex;
  flex-direction: column;
  ${props => {
    if (props.showBackground) {
      return css`
        background-color: ${color('white')};
        border-radius: 4px;
      `
    }
  }}
  ${getCustomCss}
`

export const Section: FC<ZoneProps> = ({ elementId }) => {
  const { elements } = useLayoutViewer()
  const children = useRenderView(elementId)
  const element = elements[elementId]
  const { columns, showBackground, customCss } = element.props as {
    columns: number[]
    showBackground?: boolean
    customCss?: string
  }

  if (columns.length > 1) {
    return (
      <StyledRow
        className={ComponentContainerClassName}
        gutter={16}
        align="stretch"
        showBackground={showBackground}
        customCss={customCss}>
        {columns.map((flex, index) => {
          return (
            <NColumn key={`section_${elementId}_${index}`} flex={flex}>
              {children && children[index]}
            </NColumn>
          )
        })}
      </StyledRow>
    )
  }

  return (
    <SingleColumnWrapper className={ComponentContainerClassName} showBackground={showBackground} customCss={customCss}>
      {children}
    </SingleColumnWrapper>
  )
}
