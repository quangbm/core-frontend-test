import * as React from 'react'

type FooterScriptProps = {
  children: string
}

export const FooterScript = React.memo<FooterScriptProps>(function Node({ children }) {
  React.useEffect(() => {
    if (children) {
      const element = document.createElement('div')
      const range = document.createRange()
      element.appendChild(range.createContextualFragment(children))

      document.body.appendChild(element)

      return () => {
        document.body.removeChild(element)
      }
    }
  }, [children])
  return null
})

FooterScript.displayName = 'FooterScript'
