import * as React from 'react'

type HeaderScriptProps = {
  scripts?: Array<{ src: string }>
}

export const HeaderScript = React.memo<HeaderScriptProps>(function Node({ scripts = [] }) {
  React.useEffect(() => {
    const scriptsElement = [] as Array<HTMLScriptElement>
    for (const script of scripts) {
      const scriptElement = document.createElement('script')
      scriptElement.async = true
      scriptElement.src = script.src
      scriptsElement.push(scriptElement)
    }
    scriptsElement.forEach(element => document.head.appendChild(element))
    return () => {
      scriptsElement.forEach(element => document.head.removeChild(element))
    }
  }, [scripts])
  return null
})

HeaderScript.displayName = 'HeaderScript'
