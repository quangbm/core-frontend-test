import { FC } from 'react'
import styled from 'styled-components/macro'
import { color, spacing } from '../../../components/GlobalStyle'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { typography } from '../../../components/NTypography'

const Wrapper = styled.div`
  display: flex;
  width: 100%;
  height: 40px;
  padding: ${spacing('xxs')} ${spacing('md')};
  background: ${color('white')};
  box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.05), 0px 1px 2px rgba(0, 0, 0, 0.1);
  overflow-x: auto;
  position: fixed;
  top: 56px;
  z-index: 10;
`

const TabItem = styled.div<{ isActive: boolean }>`
  height: 32px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  background: transparent;
  color: ${props => (props.isActive ? color('Primary900') : color('Neutral700'))};
  &:hover {
    background: ${color('Neutral200')};
  }
  background: ${props => props.isActive && color('Primary200')};
  padding-left: ${spacing('sm')};
  padding-right: ${spacing('sm')};
  border-radius: 6px;
  transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
  ${typography('small-bold-text')}
  font-weight: bold;
`
export type TabData = {
  value: string
  displayName: string
  isLoading?: boolean
}

type Props = {
  value?: string
  tabs: TabData[]
  onTabChange: (value: string) => void
}

export const AppPanelTabBar: FC<Props> = ({ tabs, value, onTabChange }) => {
  return (
    <Wrapper>
      {tabs.map((tab, index) => (
        <TabItem
          key={tab.value}
          isActive={value === undefined ? index === 0 : value === tab.value}
          onClick={() => onTabChange(tab.value)}>
          {tab.isLoading ? <NSpinner size={12} /> : tab.displayName}
        </TabItem>
      ))}
    </Wrapper>
  )
}
