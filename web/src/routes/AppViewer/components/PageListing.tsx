import * as React from 'react'
import { useQuery } from 'react-query'
import { Link, useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { LayoutResponse } from '../../../../../shared/src/api'
import { color, spacing } from '../../../components/GlobalStyle'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { typography } from '../../../components/NTypography'
import { APIServices, QueryKeys } from '../../../services/api'

const PagesWrapper = styled('div')`
  display: flex;
  overflow-x: auto;
  min-height: 40px;
  & > *:not(:last-child) {
    margin-right: 2px;
  }
`

const PageItem = styled('div')`
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${color('Neutral700')};
  padding-left: ${spacing('sm')};
  padding-right: ${spacing('sm')};
  border-radius: 6px;
  ${typography('small-bold-text')}
  font-weight: bold;
  &.is-selected {
    color: ${color('Primary900')};
    background: ${color('Primary200')};
    transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
  }
  &:not(.is-selected):hover {
    background: ${color('Neutral200')};
    transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
  }
`

type PageListingProps = {
  pages?: Array<LayoutResponse>
  isLoading?: boolean
}

export const PageListing = React.memo<PageListingProps>(function Node({ pages, isLoading = false }) {
  const { layoutId, objectName, id } = useParams<{ layoutId?: string; objectName?: string; id?: string }>()

  const { data: objectData, isLoading: isObjectDataLoading } = useQuery(
    [QueryKeys.Data.getSingleObjectData, { name: objectName!, guid: id! }],
    APIServices.Data.getSingleObjectData,
    {
      enabled: Boolean(objectName && id),
      select(response) {
        return response.data
      },
    },
  )
  if (isLoading) {
    return (
      <PagesWrapper>
        <PageItem>
          <NSpinner />
        </PageItem>
      </PagesWrapper>
    )
  }

  return (
    <PagesWrapper>
      {pages?.map(page => {
        return (
          <Link to={`/${page.id}`} key={page.id}>
            <PageItem className={(layoutId === page.id && 'is-selected') || undefined}>{page.name}</PageItem>
          </Link>
        )
      })}
      {objectName && id && (
        <PageItem className="is-selected">
          {(isObjectDataLoading && <NSpinner />) ||
            `${
              // @ts-ignore
              objectData?.name || objectName
            }`}
        </PageItem>
      )}
      {objectName && !id && <PageItem className="is-selected">New {objectName} Record</PageItem>}
      {layoutId === 'me' && <PageItem className="is-selected">My profile</PageItem>}
    </PagesWrapper>
  )
})

PageListing.displayName = 'PageListing'
