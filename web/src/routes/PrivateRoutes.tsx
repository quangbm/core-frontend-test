import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { Redirect, Route, Switch, useLocation } from 'react-router-dom'
import styled, { useTheme } from 'styled-components/macro'
import { ProfileAccessResponse } from '../../../shared/src/api'
import { DASHBOARD_ROUTE } from '../common/constants'
import { color } from '../components/GlobalStyle'
import { NSpinner } from '../components/NSpinner/NSpinner'
import { useAccessToken } from '../hooks/useAuth'
import { useMe } from '../hooks/useMe'
import { useRQRefetchOnWindowFocus } from '../hooks/useRQRefetchOnWindowFocus'
import { useSession } from '../hooks/useSession'
import { AppBuilder } from '../module/AppEngine'
import { getAbility } from '../services/casl/ability'
import { AbilityContext, Can } from '../services/casl/Can'
import { AppViewer } from './AppViewer/AppViewer'
import { Dashboard } from './Dashboard/Dashboard'
import { FlowBuilder } from './FlowBuilder/FlowBuilder'
import { LayoutBuilderProvider } from './LayoutBuilder/contexts/LayoutBuilderContext'
import { LayoutBuilderDataProvider } from './LayoutBuilder/contexts/LayoutBuilderDataContext'
import { LayoutBuilder } from './LayoutBuilder/LayoutBuilder'
import { NoPermission } from './NoPermission/NoPermission'
import { NotFound } from './NotFound/NotFound'

const Overlay = styled('div')`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: ${color('white')};
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`

export function PrivateRoutes() {
  const { pathname, state } = useLocation<{ redirect: string }>()
  const { data: accessToken, isLoading: isAccessTokenLoading } = useAccessToken()
  const [{ isLoading }] = useSession({ enabled: !!accessToken })
  const { data: meData, isLoading: isLoadingMe } = useMe({ enabled: !!accessToken })
  useRQRefetchOnWindowFocus(
    !pathname.includes('layout-builder') && !pathname.includes('flow-builder') && !pathname.includes('setup'),
  )

  const theme = useTheme()

  if (isLoading || isAccessTokenLoading || isLoadingMe) {
    return (
      <Overlay>
        <NSpinner size={30} strokeWidth={2} color={color('Primary900')({ theme })} />
      </Overlay>
    )
  }

  if (!accessToken) {
    return <Redirect to={{ pathname: '/auth', state: { redirect: pathname } }} />
  }

  const hasRedirect = state?.redirect !== undefined
  const redirectToReference = hasRedirect && state!.redirect !== '/'

  if (redirectToReference) {
    return <Redirect to={state!.redirect} />
  }

  return (
    <DndProvider backend={HTML5Backend}>
      <AbilityContext.Provider
        value={getAbility(meData?.profile.featuresAccess as Record<string, ProfileAccessResponse>)}>
        <Switch>
          <Route path={`${DASHBOARD_ROUTE}`}>
            <Can I="read" a="setup">
              <Dashboard />
            </Can>
            <Can not I="read" a="setup">
              <NoPermission />
            </Can>
          </Route>
          <Route path="/layout-builder/new">
            <AppBuilder />
          </Route>
          <Route path={['/layout-builder/:layoutId']}>
            <Can I="read" a="application-layout">
              <LayoutBuilderProvider>
                <LayoutBuilderDataProvider>
                  <LayoutBuilder />
                </LayoutBuilderDataProvider>
              </LayoutBuilderProvider>
            </Can>
            <Can not I="read" a="application-layout">
              <NoPermission />
            </Can>
          </Route>
          <Route path="/flow-builder/:flowName/templates/:version">
            <Can I="read" a="flow">
              <FlowBuilder />
            </Can>
            <Can not I="read" a="flow">
              <NoPermission />
            </Can>
          </Route>
          <Route path={['/:objectName/create', '/:objectName/:id', '/:layoutId', '/']}>
            <AppViewer />
          </Route>
          <Route>
            <NotFound />
          </Route>
        </Switch>
      </AbilityContext.Provider>
    </DndProvider>
  )
}
