import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Redirect, useLocation } from 'react-router-dom'
import { down } from 'styled-breakpoints'
import styled from 'styled-components/macro'
import { ReactComponent as NuclentLogo } from '../../assets/nuclent.svg'
import { color, spacing } from '../../components/GlobalStyle'
import { NButton } from '../../components/NButton/NButton'
import { NTextInput } from '../../components/NTextInput/NTextInput'
import { useAccessToken, useSignIn } from '../../hooks/useAuth'

const Wrapper = styled('div')`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
`
const Content = styled.div`
  flex: 1;
  flex-direction: row;
  display: flex;
  justify-content: space-between;
`

const Footer = styled.div`
  background: ${color('Neutral200')};
  height: 64px;
  width: 100%;
  flex-shrink: 0;
`

const LoginContainer = styled.div`
  width: 320px;
  ${down('md')} {
    margin-left: ${spacing('xl')};
    margin-right: ${spacing('xl')};
  }
  margin-left: ${spacing(173)};
  margin-top: ${spacing(61)};
`

const LoginHeader = styled.p`
  font-weight: bold;
  font-size: 32px;
  line-height: 41px;
  color: ${color('Neutral900')};
  margin-top: ${spacing(76)};
  margin-bottom: ${spacing(18)};
`
const LoginDescription = styled.p`
  font-weight: normal;
  font-size: 16px;
  line-height: 20px;
  color: ${color('Neutral500')};
  margin-bottom: 32px;
`

const SideDecorator = styled.div`
  width: 346px;
  height: 100%;
  background: ${color('Primary700')};
`
const LoginForm = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
`

const StyledNTextInput = styled(NTextInput)`
  margin-bottom: ${spacing('xl')};
`

const ForgotPasswordBtn = styled(NButton)`
  margin-top: ${spacing('xl')};
`

const ErrorMessage = styled('div')`
  margin-bottom: ${spacing('xl')};
  color: ${color('danger')};
`

export function SignIn() {
  const { state, search, hash } = useLocation()
  const { handleSubmit, formState, register } = useForm({
    defaultValues: {
      username: '',
      password: '',
    },
  })
  const { t } = useTranslation()

  const { data: accessToken } = useAccessToken()
  const { mutate: login, isLoading, error } = useSignIn()

  if (accessToken) {
    //Redirect to app and handle session there
    return <Redirect to={{ pathname: '/', state, search, hash }} />
  }

  return (
    <Wrapper>
      <Content style={{ flexDirection: 'row' }}>
        <LoginContainer>
          <NuclentLogo />
          <LoginHeader>{t('auth.loginHeader')}</LoginHeader>
          <LoginDescription>{t('auth.loginDesc')}</LoginDescription>
          {error && <ErrorMessage>{error.response?.data?.message || error.message}</ErrorMessage>}
          <LoginForm
            onSubmit={handleSubmit(data => {
              login(data)
            })}>
            <StyledNTextInput
              placeholder={t('auth.enterUsername')}
              label={t('auth.username')}
              error={formState.errors.username?.message}
              {...register('username', {
                required: {
                  value: true,
                  message: t('common.error.required'),
                },
              })}
            />
            <StyledNTextInput
              type="password"
              placeholder={t('auth.enterPassword')}
              label={t('auth.password')}
              error={formState.errors.password?.message}
              {...register('password', {
                required: {
                  value: true,
                  message: t('common.error.required'),
                },
              })}
            />
            <NButton loading={isLoading} type="primary" htmlType="submit" block>
              {t('auth.loginBtn')}
            </NButton>
            <ForgotPasswordBtn type="link">{t('auth.forgotPassword')}</ForgotPasswordBtn>
          </LoginForm>
        </LoginContainer>
        <SideDecorator />
      </Content>
      <Footer />
    </Wrapper>
  )
}
