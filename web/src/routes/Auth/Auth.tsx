import { Redirect, Route, Switch, useLocation } from 'react-router-dom'
import i18n from '../../services/i18n/i18n'
import en from './locales/en'
import vi from './locales/vi'
import { SignIn } from './SignIn'

i18n.addResourceBundle('en', 'translation', { auth: en })
i18n.addResourceBundle('vi', 'translation', { auth: vi })

type AuthProps = {}

export function Auth({}: AuthProps) {
  const { state, search, hash } = useLocation()
  return (
    <Switch>
      <Route path="/auth/sign-in">
        <SignIn />
      </Route>
      <Redirect to={{ pathname: '/auth/sign-in', state, search, hash }} />
    </Switch>
  )
}
