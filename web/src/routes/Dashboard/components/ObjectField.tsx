import { FC } from 'react'
import styled from 'styled-components/macro'
import { ExtFieldResponse, FieldResponse } from '../../../../../shared/src/api'
import { color, spacing } from '../../../components/GlobalStyle'
import { GlobalIcons } from '../../../components/Icons'
import { NDivider } from '../../../components/NDivider'
import { NColumn, NRow } from '../../../components/NGrid/NGrid'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { FieldDecoration } from './FieldDecoration'

const FieldCell = styled.div`
  height: 48px;
  padding-left: ${spacing('xl')};
  padding-right: ${spacing('xl')};
  &:hover {
    background: ${color('Neutral200')};
  }
  cursor: pointer;
`
const FieldRow = styled(NRow)`
  align-items: center;
  height: 100%;
`

const FieldName = styled.div`
  display: flex;
  align-items: center;
`

const DataType = styled.p`
  color: ${color('Neutral500')};
  font-size: 14px;
`
const FieldRule = styled.p`
  color: ${color('Primary700')};
`

type ObjectFieldProps = {
  field: FieldResponse | ExtFieldResponse
  onClick?: () => void
  className?: string
  onDelete: (fieldName: string) => void
  isDeleting?: boolean
}

export const ObjectField: FC<ObjectFieldProps> = ({ field, onClick, className, onDelete, isDeleting }) => {
  const subType = field.attributes.subType && `(${field.attributes.subType})`

  return (
    <FieldCell className={className} onClick={onClick}>
      <FieldRow>
        <NColumn flex={1}>
          <FieldName>
            <FieldDecoration type={field.dataType.name} />
            <NDivider vertical size="md" />
            {field.displayName}
          </FieldName>
        </NColumn>
        <NColumn flex={1}>
          <DataType>
            {field.dataType.displayName} {subType}
          </DataType>
        </NColumn>
        <NColumn flex={1}>{field.isRequired && <FieldRule>is required</FieldRule>}</NColumn>
        <NColumn flex={1}>{field.isSystemDefault && <FieldRule>is system</FieldRule>}</NColumn>
        {field.isSystemDefault ? (
          <></>
        ) : isDeleting ? (
          <NSpinner size={16} strokeWidth={2} />
        ) : (
          <GlobalIcons.Trash
            onClick={e => {
              e.stopPropagation()
              onDelete(field.name)
            }}
          />
        )}
      </FieldRow>
    </FieldCell>
  )
}
