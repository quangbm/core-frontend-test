import { FC } from 'react'
import styled from 'styled-components/macro'
import { color } from '../../../components/GlobalStyle'
import { DatatypeIcons } from '../../../components/Icons'

const Wrapper = styled.div`
  background: ${color('Neutral400')};
  min-width: 48px;
  height: 32px;
  border-radius: 3px;
  color: ${color('white')};
  display: flex;
  justify-content: center;
  align-items: center;
`
const iconResolve = (type: string) => {
  switch (type) {
    case 'text':
      return <DatatypeIcons.Text />
    case 'boolean':
      return <DatatypeIcons.Checkbox />
    case 'numeric':
      return <DatatypeIcons.Numeric />
    case 'pickList':
      return <DatatypeIcons.Picklist />
    case 'dateTime':
      return <DatatypeIcons.Datetime />
    case 'relation':
      return <DatatypeIcons.Relation />
    case 'generated':
      return <DatatypeIcons.Generated />
    case 'json':
      return <DatatypeIcons.JSON />
    case 'currency':
      return <DatatypeIcons.Currency />
    case 'externalRelation':
      return <DatatypeIcons.ExternalRelation />
    case 'indirectRelation':
      return <DatatypeIcons.Relation />
    default:
      return null
  }
}

type Props = {
  type: string
}

export const FieldDecoration: FC<Props> = ({ type }) => {
  return <Wrapper className="FieldDecoration">{iconResolve(type)}</Wrapper>
}
