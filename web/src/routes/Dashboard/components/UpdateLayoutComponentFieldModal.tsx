import React, { FC, useEffect, useReducer } from 'react'
import { useQuery } from 'react-query'
import styled, { useTheme } from 'styled-components/macro'
import { LayoutComponent } from '../../../../../shared/src/api'
import { color, spacing } from '../../../components/GlobalStyle'
import { NCheckbox } from '../../../components/NCheckbox/NCheckbox'
import { NModal } from '../../../components/NModal/NModal'
import { NSpinner } from '../../../components/NSpinner/NSpinner'
import { typography } from '../../../components/NTypography'
import { APIServices, QueryKeys } from '../../../services/api'

const ModalWrapper = styled.div<{ visible?: boolean }>`
  display: ${props => (!!props.visible ? 'initial' : 'none')};
`

const LoadingContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: ${spacing('xl')};
`

const Wrapper = styled.div`
  padding: ${spacing('xl')};
`

const LayoutWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid ${color('Neutral200')};
  margin-bottom: ${spacing('md')};

  &:last-child {
    margin-bottom: 0;
  }
`

const LayoutName = styled.div`
  ${typography('h400')};
  padding: ${spacing('md')};
`

const ComponentWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  border-top: 1px solid ${color('Neutral200')};
  padding: ${spacing('md')};
`

/////////////////
enum ActionType {
  Init = 'Init',
  Update = 'Update',
  Clean = 'Clean',
}

type RecducerAction =
  | {
      type: ActionType.Init
      payload: {
        updateLayouts: LayoutRecord
      }
    }
  | {
      type: ActionType.Update
      payload: {
        layoutId: string
        componentId: string
        checked: boolean
      }
    }
  | {
      type: ActionType.Clean
    }

function reducer(state: LayoutRecord, action: RecducerAction) {
  switch (action.type) {
    case ActionType.Init:
      return action.payload.updateLayouts
    case ActionType.Update:
      const { layoutId, componentId, checked } = action.payload
      state[layoutId].components[componentId] = checked
      const layout = state[layoutId]
      const { components } = layout
      return {
        ...state,
        [layoutId]: {
          ...layout,
          components: {
            ...components,
            [componentId]: checked,
          },
        },
      }
    case ActionType.Clean:
      return {}
    default:
      throw new Error()
  }
}
/////////////////
type LayoutRecord = Record<string, { name: string; components: Record<string, boolean> }>

type Props = {
  objName: string
  fieldName?: string
  isRequired?: boolean

  visible: boolean
  setVisible: (visible: boolean) => void
  onBack?: () => void

  isSubmitting?: boolean
  onSubmit: (newUpdateLayouts: LayoutComponent[]) => void
}

export const UpdateLayoutComponentFieldModal: FC<Props> = ({
  visible,
  setVisible,
  onBack,

  objName,
  fieldName,
  isRequired,

  isSubmitting,
  onSubmit,
}) => {
  const theme = useTheme()
  const [updateLayouts, dispatch] = useReducer(reducer, {} as LayoutRecord)

  const { data: layouts, isFetching } = useQuery(
    [QueryKeys.ObjectRecordPages.getObjectRecordPages, { objName, fieldName: fieldName! }],
    APIServices.ObjectRecordPages.getObjectRecordPages,
    {
      enabled: !!fieldName && visible,
    },
  )

  // get default value when fetching layouts or open, close modal
  useEffect(() => {
    if (!visible || !layouts) {
      dispatch({ type: ActionType.Clean })
      return
    }

    const transformedUpdateLayouts = layouts.data.reduce((prevLayout, cur) => {
      const { id, name, components } = cur
      const layoutComponents = components.reduce((prev, cur) => {
        return { ...prev, [cur.id]: (cur.isPrimary && isRequired) || cur.fieldExists }
      }, {} as Record<string, boolean>)

      return { ...prevLayout, [id]: { name, components: layoutComponents } }
    }, {} as LayoutRecord)

    dispatch({ type: ActionType.Init, payload: { updateLayouts: transformedUpdateLayouts } })
  }, [visible, layouts, isRequired])

  const handleClose = () => {
    setVisible(false)
  }

  const handleSubmit = () => {
    const layoutKeys = Object.keys(updateLayouts)

    const parsedUpdateLayouts: LayoutComponent[] = layoutKeys.reduce((prevLayoutUpdates, key) => {
      const componentKeys = Object.keys(updateLayouts[key].components)
      const componentIds = componentKeys.reduce((prev, cur) => {
        const comp = updateLayouts[key].components[cur]
        if (comp) {
          return [...prev, cur]
        }

        return prev
      }, [] as string[])

      if (componentIds.length === 0) {
        return prevLayoutUpdates
      }

      return [
        ...prevLayoutUpdates,
        {
          layoutId: key,
          componentIds,
        },
      ]
    }, [] as LayoutComponent[])

    onSubmit(parsedUpdateLayouts)
  }

  return (
    <ModalWrapper visible={visible}>
      <NModal.Header title="Update Layout Component fields" onBack={onBack} onClose={handleClose}></NModal.Header>
      <NModal.Body>
        {isFetching ? (
          <LoadingContainer>
            <NSpinner size={30} strokeWidth={3} color={color('Primary700')({ theme })} />
          </LoadingContainer>
        ) : (
          <Wrapper>
            {layouts?.data.map(layout => {
              return (
                <LayoutWrapper key={`layout_${layout.id}`}>
                  <LayoutName>{layout.name}</LayoutName>
                  <ComponentWrapper>
                    {layout.components.map(component => {
                      const checked = !!updateLayouts[layout.id]?.components[component.id]

                      return (
                        <NCheckbox
                          key={`component_${component.id}`}
                          disabled={component.isPrimary && isRequired}
                          title={`${component.label}${component.isPrimary ? ' (primary)' : ''}`}
                          checked={checked}
                          onChange={e => {
                            dispatch({
                              type: ActionType.Update,
                              payload: {
                                layoutId: layout.id,
                                componentId: component.id,
                                checked: e.target.checked,
                              },
                            })
                          }}
                        />
                      )
                    })}
                  </ComponentWrapper>
                </LayoutWrapper>
              )
            })}
          </Wrapper>
        )}
      </NModal.Body>
      <NModal.Footer isLoading={isSubmitting} onCancel={handleClose} onFinish={handleSubmit} />
    </ModalWrapper>
  )
}
