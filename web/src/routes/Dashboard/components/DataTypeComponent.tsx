import { FC } from 'react'
import styled from 'styled-components/macro'
import { DataTypeResponse } from '../../../../../shared/src/api'
import { color, spacing } from '../../../components/GlobalStyle'
import { NDivider } from '../../../components/NDivider'
import { NTypography } from '../../../components/NTypography'
import { FieldDecoration } from './FieldDecoration'

const DataTypeContainer = styled.div`
  background: transparent;
  display: flex;
  padding: ${spacing('sm')};
  cursor: pointer;
  border-radius: 4px;
  &:hover {
    background: ${color('Primary100')};
  }
  &:hover .FieldDecoration {
    background: ${color('Primary700')};
  }
  transition: background 0.2s ease-in-out;
`

const TextContainer = styled.div`
  width: 100%;
`

const DataTypeName = styled.p`
  color: ${color('Neutral900')};
  font-size: 14px;
  margin-bottom: ${spacing('xxs')};
`

type DataTypeComponentProps = {
  type: DataTypeResponse
  onClick?: (type: DataTypeResponse) => void
}

export const DataTypeComponent: FC<DataTypeComponentProps> = ({ type, onClick }) => {
  return (
    <DataTypeContainer
      onClick={() => {
        onClick && onClick(type)
      }}>
      <FieldDecoration type={type.name} />
      <NDivider vertical size="sm" />
      <TextContainer>
        <DataTypeName>{type.displayName}</DataTypeName>
        <NTypography.InputCaption>{type.description}</NTypography.InputCaption>
      </TextContainer>
    </DataTypeContainer>
  )
}
