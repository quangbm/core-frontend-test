import { FC } from 'react'
import { Controller } from 'react-hook-form'
import { NCodeEditor } from '../../../../components/NCodeEditor'
import { NDivider } from '../../../../components/NDivider'
import { DataTypeFormProps } from './DataTypeForm'
import { NameInputRow } from './NameInputRow'

export const JsonForm: FC<DataTypeFormProps> = ({ formMethods, dataType, fieldData, isExternal }) => {
  const { control } = formMethods

  return (
    <>
      <NameInputRow formMethods={formMethods} dataType={dataType} fieldData={fieldData} />
      <NDivider size="xl" />
      <Controller
        control={control}
        name="defaultValue"
        defaultValue={
          fieldData?.attributes.defaultValue
            ? JSON.stringify(JSON.parse(fieldData?.attributes.defaultValue), null, 2)
            : ''
        }
        render={({ field: { value, onChange } }) => {
          return (
            <NCodeEditor
              value={value}
              onChange={onChange}
              language="json"
              style={{ minHeight: 100, maxHeight: 500 }}
              defaultValue={fieldData?.attributes.defaultValue}
              label="Default value"
              disabled={isExternal}
            />
          )
        }}
      />
    </>
  )
}
