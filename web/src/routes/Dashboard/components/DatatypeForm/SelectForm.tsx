import { useState } from 'react'
import { Controller } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled from 'styled-components/macro'
import { PickListResponse } from '../../../../../../shared/src/api'
import { spacing } from '../../../../components/GlobalStyle'
import { NButton } from '../../../../components/NButton/NButton'
import { NDivider } from '../../../../components/NDivider'
import { NColumn, NRow } from '../../../../components/NGrid/NGrid'
import { NModal } from '../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../components/NPerfectScrollbar'
import { SelectOption } from '../../../../components/NSelect/model'
import { NMultiSelect } from '../../../../components/NSelect/NMultiSelect'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../services/api'
import { PickListItemsConfig } from '../../routes/Organization/PickLists/components/PickListItemsConfig'
import { DataTypeFormProps } from './DataTypeForm'
import { NameInputRow } from './NameInputRow'

const ModalBody = styled(NModal.Body)`
  padding: ${spacing('xl')};
`

export const SelectForm = ({ formMethods, dataType, fieldData }: DataTypeFormProps) => {
  const { control, watch, setValue } = formMethods

  const watchAttributes = watch('attributes') || fieldData?.attributes
  const watchPickListId = watch('pickListId', fieldData?.pickListId?.toString())

  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  const { data: pickListsData, isLoading: isLoadingPickList } = useQuery(
    [QueryKeys.PickLists.getAllPickLists, { searchText, status: 'ACTIVE' }],
    APIServices.PickLists.getAllPickLists,
    {
      keepPreviousData: true,
      select: resp =>
        resp.data.data.reduce(
          (a, pickList) => {
            a.pickListOptions.push({
              label: pickList.displayName || pickList.name,
              value: pickList.id.toString(),
            })
            a.pickListItemsById[pickList.id] = pickList
            return a
          },
          { pickListOptions: [] as SelectOption[], pickListItemsById: {} as Record<string, PickListResponse> },
        ),
    },
  )

  const [showConfig, setShowConfig] = useState(false)

  return (
    <>
      <NameInputRow formMethods={formMethods} dataType={dataType} fieldData={fieldData} />
      <NDivider size="xl" />
      <NRow>
        <NColumn flex={1}>
          <NRow>
            <Controller
              control={control}
              name="attributes.subType"
              defaultValue={fieldData?.attributes.subType || dataType.attributes.subTypes[0]}
              render={({ field }) => (
                <NSingleSelect
                  fullWidth
                  value={field.value}
                  onValueChange={newType => {
                    field.onChange(newType)
                    setValue('defaultValue', '')
                  }}
                  label="Type"
                  options={dataType.attributes.subTypes.map(type => {
                    return {
                      value: type,
                    }
                  })}
                />
              )}
            />
          </NRow>
        </NColumn>
        <NDivider vertical size="xl" />
        <NColumn flex={1}>
          <Controller
            control={control}
            name="pickListId"
            defaultValue={fieldData?.pickListId?.toString() || ''}
            render={({ field }) => (
              <NSingleSelect
                required
                isSearchable
                searchValue={searchValue}
                onSearchValueChange={handleSearchChange}
                fullWidth
                value={field.value}
                onValueChange={newType => {
                  field.onChange(newType)
                  setValue('defaultValue', '')
                }}
                placeholder="Select pick list"
                label="Pick list"
                isLoading={isLoadingPickList}
                options={pickListsData?.pickListOptions || []}
              />
            )}
          />
        </NColumn>
      </NRow>
      <NDivider size="xl" />

      <NButton
        type="outline"
        style={{ width: '100%' }}
        disabled={!pickListsData?.pickListItemsById[watchPickListId]}
        onClick={() => {
          setShowConfig(true)
        }}>
        Config pick list
      </NButton>
      <NDivider size="xl" />

      <Controller
        control={control}
        name="defaultValue"
        defaultValue={fieldData?.attributes.defaultValue}
        render={({ field: { value, onChange } }) => {
          const options =
            pickListsData?.pickListItemsById[watchPickListId]?.items
              .filter(item => !item.isDeprecated)
              .map(item => ({ label: item.displayName, value: item.name })) || []

          if (watchAttributes?.subType === 'multi') {
            const mulValues: string[] = JSON.parse(value || '[]')
            return (
              <NMultiSelect
                label="Default value"
                placeholder="Multiple pick"
                fullWidth
                values={mulValues}
                options={options}
                onValuesChange={values => {
                  onChange(JSON.stringify(values))
                }}
              />
            )
          }
          return (
            <NSingleSelect
              label="Default value"
              placeholder="Single pick"
              fullWidth
              value={value}
              options={options}
              onValueChange={onChange}
            />
          )
        }}
      />
      <NModal visible={showConfig} setVisible={setShowConfig} size="large">
        <NModal.Header title="Pick list config" />
        <ModalBody>
          <NPerfectScrollbar style={{ maxHeight: '60vh' }}>
            {pickListsData?.pickListItemsById[watchPickListId] && (
              <PickListItemsConfig data={pickListsData?.pickListItemsById[watchPickListId]} />
            )}
          </NPerfectScrollbar>
        </ModalBody>
        <NModal.Footer>
          <NButton
            onClick={() => {
              setShowConfig(false)
            }}>
            Close
          </NButton>
        </NModal.Footer>
      </NModal>
    </>
  )
}
