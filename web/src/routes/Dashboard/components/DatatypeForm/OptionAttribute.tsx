import { FC } from 'react'
import { Control, Controller } from 'react-hook-form'
import { NRow } from '../../../../components/NGrid/NGrid'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { IFieldForm } from '../../routes/Object/models'

type Props = {
  control: Control<IFieldForm>
  options: SelectOption[]
  name: keyof IFieldForm
  disabled?: boolean
  defaultValue?: string
  label: string
}

export const OptionAttribute: FC<Props> = ({ control, options, name, disabled, defaultValue, label }) => {
  return (
    <NRow>
      <Controller
        defaultValue={defaultValue}
        control={control}
        name={name}
        render={({ field }) => (
          <NSingleSelect
            fullWidth
            value={typeof field.value === 'string' ? field.value : ''}
            onValueChange={field.onChange}
            label={label}
            options={options}
            disabled={disabled}
          />
        )}
      />
    </NRow>
  )
}
