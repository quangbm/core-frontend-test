import { FC } from 'react'
import { Controller } from 'react-hook-form'
import { NDivider } from '../../../../components/NDivider'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { DataTypeFormProps } from './DataTypeForm'
import { NameInputRow } from './NameInputRow'

export const GeneratedForm: FC<DataTypeFormProps> = ({ formMethods, dataType, fieldData }) => {
  const { control } = formMethods
  return (
    <>
      <NameInputRow formMethods={formMethods} dataType={dataType} fieldData={fieldData} />
      <NDivider size="xl" />
      <NRow>
        <Controller
          defaultValue={fieldData?.attributes.subType || dataType.attributes.subTypes[0]}
          control={control}
          name="attributes.subType"
          render={({ field }) => (
            <NSingleSelect
              fullWidth
              value={field.value}
              onValueChange={field.onChange}
              label="Type"
              options={dataType.attributes.subTypes.map(type => {
                return {
                  value: type,
                }
              })}
            />
          )}
        />
      </NRow>
    </>
  )
}
