import React, { FC, useEffect, useMemo, useState } from 'react'
import { Controller, FormProvider, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useQuery } from 'react-query'
import styled from 'styled-components/macro'
import { FieldSchema } from '../../../../../../shared/src/api'
import { color, fontSize, spacing } from '../../../../components/GlobalStyle'
import { NButton } from '../../../../components/NButton/NButton'
import {
  FilterItem,
  NConditionBuilder,
  transformToFormValue,
  transformToPropsValue,
} from '../../../../components/NConditionBuilder'
import { NDivider } from '../../../../components/NDivider'
import { elevation } from '../../../../components/NElevation'
import { NModal } from '../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../components/NPerfectScrollbar'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../services/api'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { ReactComponent as ManyToOne } from '../icons/many-to-one.svg'
import { DataTypeFormProps } from './DataTypeForm'

const Wrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  .SelectTargetObjectContainer {
    height: 40px;
    overflow: visible;
  }
  .SelectTargetObject {
    min-height: 40px;
    border-color: ${color('Primary700')};
  }
`

const NameTextInput = styled(NTextInput)`
  width: 100%;
`
const NameInputWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: ${spacing('md')};
  flex-direction: column;
`

const ObjectContainer = styled.div`
  width: 212px;
  border: 1px solid ${color('Primary700')};
  ${elevation('Elevation100')}
  border-radius: 3px;
`

const ObjectName = styled.div`
  height: 40px;
  box-sizing: border-box;
  border-bottom: 1px solid ${color('Neutral200')};
  display: flex;
  justify-content: center;
  align-items: center;
`

const Connector = styled.div`
  background: ${color('Primary700')};
  height: 1px;
  width: 472px;
`

const ConnectDecorator = styled.div`
  width: 16px;
  height: 16px;
  background: ${color('Primary700')};
  border-radius: 0px 8px 8px 0px;
  &.right {
    transform: rotate(-180deg);
  }
`

const RelationshipItem = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);

  display: flex;
  justify-content: center;
  align-items: center;
  height: 32px;
  width: 32px;
  box-sizing: border-box;
  border: 1px solid ${color('Neutral200')};
  border-radius: 3px;

  background: ${color('Primary700')};
  color: ${color('white')};

  cursor: pointer;
`

const RelationshipDescription = styled.p`
  font-size: ${fontSize('xs')};
  position: absolute;
  left: 50%;
  top: 70%;
  transform: translate(-50%, -70%);
  background: ${color('transparent')};
`

const RelationshipHighlight = styled.span`
  color: ${color('Primary700')};
`

const ModalBody = styled(NModal.Body)`
  padding: ${spacing('sm')};
`

export const RelationshipForm: FC<DataTypeFormProps> = ({ formMethods, fieldData, object }) => {
  const { register, formState, control, setValue, getValues, watch } = formMethods
  const { t } = useTranslation()
  const [targetObjectName, setTargetObjectName] = useState<undefined | string>(fieldData?.value)
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()

  // show/hide filter config
  const [showFilter, setShowFilter] = useState(false)

  // watch objName value
  const watchObjName = getValues('value')

  const { data: objectsData } = useQuery(
    [
      QueryKeys.Metadata.getObjects,
      {
        searchText: searchText,
      },
    ],
    APIServices.Metadata.getObjects,
    { keepPreviousData: true },
  )

  const objectOptions: SelectOption[] = useMemo(() => {
    if (objectsData?.data) {
      return objectsData?.data.data.map(obj => {
        return {
          label: obj.displayName,
          value: obj.name,
        }
      })
    }
    return []
  }, [objectsData?.data])

  useEffect(() => {
    setValue('attributes.filters', fieldData?.attributes.filters)
  }, [fieldData])

  //@ts-ignore
  const watchFilters: FilterItem[][] = watch('attributes.filters')

  return (
    <>
      <Wrapper>
        <ObjectContainer>
          <ObjectName>{object?.displayName}</ObjectName>
          <NameInputWrapper>
            <NameTextInput
              defaultValue={fieldData?.displayName || ''}
              placeholder="Display name"
              label="Display name"
              error={formState.errors.displayName?.message}
              {...register('displayName', {
                required: {
                  value: true,
                  message: t('common.error.required'),
                },
              })}
              required
              onBlur={e => {
                if (!!!fieldData) {
                  const formatName = getNameFromDisplayName(e.target.value)
                  !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                }
              }}
            />
            <NDivider size="xl" />
            <NameTextInput
              required
              defaultValue={fieldData?.name || ''}
              placeholder="Name"
              label="Field Name"
              error={formState.errors.name?.message}
              {...register('name', {
                required: {
                  value: true,
                  message: t('common.error.required'),
                },
                pattern: {
                  value: fieldNameRegex,
                  message: 'Invalid pattern',
                },
              })}
              disabled={!!fieldData}
            />
          </NameInputWrapper>
        </ObjectContainer>
        <ConnectDecorator />

        <Connector />
        <RelationshipItem>
          <ManyToOne />
        </RelationshipItem>
        {targetObjectName && (
          <RelationshipDescription>
            {object?.displayName} <RelationshipHighlight>many to one</RelationshipHighlight> {targetObjectName}
          </RelationshipDescription>
        )}

        <ConnectDecorator className="right" />
        <Controller
          defaultValue={fieldData?.value}
          control={control}
          rules={{ required: { value: true, message: 'Required' } }}
          name="value"
          render={({ field, fieldState }) => (
            <NSingleSelect
              isSearchable
              containerClassName="SelectTargetObjectContainer"
              className="SelectTargetObject"
              options={objectOptions}
              placeholder="Select object"
              error={fieldState.error?.message}
              value={field.value}
              searchValue={searchValue}
              onSearchValueChange={handleSearchChange}
              onValueChange={value => {
                field.onChange(value)
                //TODO: Optimize this
                setTargetObjectName(objectsData?.data.data.find(obj => obj.name === value)?.displayName)
              }}
            />
          )}
        />
      </Wrapper>
      <NDivider size="md" />
      <NButton
        type="outline"
        style={{ width: '100%' }}
        onClick={() => {
          setShowFilter(true)
        }}>
        Edit Filters
      </NButton>
      <FiltersModal
        filters={watchFilters}
        showFilter={showFilter}
        setShowFilter={setShowFilter}
        objName={watchObjName}
        onSubmit={filters => {
          //@ts-ignore
          setValue('attributes.filters', filters)
        }}
      />
    </>
  )
}

//////////////////////////

type FiltersModalProps = {
  filters?: FilterItem[][]
  showFilter: boolean
  setShowFilter: (newState: boolean) => void
  objName: string
  onSubmit(filters?: FilterItem[][]): void
}
const FiltersModal: FC<FiltersModalProps> = ({ filters, showFilter, setShowFilter, objName, onSubmit }) => {
  // get operator
  const { data: operators = {}, isLoading: isOperatorsLoading } = useQuery(
    [QueryKeys.Builder.getFilterOperators],
    APIServices.Builder.getFilterOperators,
    {
      select(res) {
        return Object.keys(res.data).reduce((a, k) => {
          return {
            ...a,
            [k]: res.data[k].map(i => ({ value: i.value, label: i.displayName })),
          }
        }, {} as Record<string, SelectOption[]>)
      },
    },
  )
  // get list of fields of the selected field
  const {
    data: { fieldNameOptions, fieldsByName } = { fieldNameOptions: [], fieldsByName: {} } as {
      fieldNameOptions: SelectOption[]
      fieldsByName: Record<string, FieldSchema>
    },
  } = useQuery([QueryKeys.Builder.getObjectSchema, { objName }], APIServices.Builder.getObjectSchema, {
    enabled: !!objName,
    select: data => {
      return data.data.fields.reduce(
        (a, f) => {
          return {
            fieldNameOptions: [...a.fieldNameOptions, { label: f.displayName, value: f.name }],
            fieldsByName: {
              ...a.fieldsByName,
              [f.name]: f,
            },
          }
        },
        {
          fieldNameOptions: [],
          fieldsByName: {},
        } as {
          fieldNameOptions: SelectOption[]
          fieldsByName: Record<string, FieldSchema>
        },
      )
    },
  })
  // form
  const formMethods = useForm({
    defaultValues: {
      filters: (filters && transformToFormValue(filters)) || [{ rules: [{ fieldName: '', operator: '', value: '' }] }],
    },
    shouldUnregister: false,
  })

  useEffect(() => {
    if (showFilter) {
      formMethods.reset({
        filters: filters ? transformToFormValue(filters) : [{ rules: [{ fieldName: '', operator: '', value: '' }] }],
      })
    }
  }, [showFilter])

  return (
    <NModal visible={showFilter} setVisible={setShowFilter} zIndex={1000} size="x-large">
      <NModal.Header title="Edit Filters"></NModal.Header>
      <ModalBody>
        <NPerfectScrollbar style={{ maxHeight: '60vh' }}>
          <FormProvider {...formMethods}>
            <NConditionBuilder
              name="filters"
              fieldNames={fieldNameOptions}
              fields={fieldsByName}
              operators={operators}
              isOperatorsLoading={isOperatorsLoading}
            />
          </FormProvider>
        </NPerfectScrollbar>
      </ModalBody>
      <NModal.Footer
        onCancel={() => {
          formMethods.reset({
            filters: filters
              ? transformToFormValue(filters)
              : [{ rules: [{ fieldName: '', operator: '', value: '' }] }],
          })
          setShowFilter(false)
        }}
        onFinish={formMethods.handleSubmit(v => {
          onSubmit(transformToPropsValue(v.filters))
          setShowFilter(false)
        })}
      />
    </NModal>
  )
}
