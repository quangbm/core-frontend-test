import { omit, pick } from 'lodash'
import { FC } from 'react'
import { UseFormReturn } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled, { useTheme } from 'styled-components/macro'
import {
  DataTypeResponse,
  ExtDataTypeResponse,
  ExtFieldResponse,
  FieldResponse,
  ObjectResponse,
} from '../../../../../../shared/src/api'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { NSpinner } from '../../../../components/NSpinner/NSpinner'
import { APIServices, QueryKeys } from '../../../../services/api'
import { IFieldForm } from '../../routes/Object/models'
import { FormInputs } from './FormInputs'
import { OptionAttribute } from './OptionAttribute'
import { ToggleAttributes } from './ToggleAttributes'

const LoadingWrapper = styled.div`
  padding: ${spacing('xl')};
  display: flex;
  justify-content: center;
`

export type DataTypeFormProps = {
  dataType: DataTypeResponse | ExtDataTypeResponse
  fieldData?: FieldResponse & ExtFieldResponse
  formMethods: UseFormReturn<IFieldForm>
  object?: ObjectResponse
  isExternal?: boolean
}

export const DataTypeForm: FC<DataTypeFormProps> = props => {
  const theme = useTheme()
  const watchTextSubType = props.formMethods.watch('attributes.subType', props.fieldData?.attributes.subType || 'short')

  const { data: dataTypes, isLoading: dataTypesLoading } = useQuery(
    [QueryKeys.Builder.getObjectDataTypes, { objName: props.object?.name || '' }], // TODO: Fix type? objName should not empty
    APIServices.Builder.getObjectDataTypes,
  )

  if (dataTypesLoading) {
    return (
      <LoadingWrapper>
        <NSpinner size={22} strokeWidth={2} color={color('Primary700')({ theme })} />
      </LoadingWrapper>
    )
  }

  // filter attribute using toggle and dropdown
  const availableAttributes = dataTypes?.data.find(type => type.name === props.dataType?.name)?.availableAttributes
  const attrKeys: string[] = availableAttributes
    ? Object.keys(
        props.isExternal
          ? omit(availableAttributes, ['defaultValue', 'subType', 'sensitivity', 'filters'])
          : omit(availableAttributes, ['defaultValue', 'subType', 'filters']),
      )
    : []
  const optionAttrKeys: string[] = []
  const toggleAttrKeys: string[] = []

  attrKeys.forEach(attrKey => {
    availableAttributes &&
      (availableAttributes[attrKey]?.options ? optionAttrKeys.push(attrKey) : toggleAttrKeys.push(attrKey))
  })

  const toggleAttributes = availableAttributes && pick(availableAttributes, toggleAttrKeys)
  const optionAttributes = availableAttributes && pick(availableAttributes, optionAttrKeys)

  return (
    <>
      <FormInputs {...props} />
      <NDivider size="md" />
      {toggleAttributes && (
        <ToggleAttributes formMethods={props.formMethods} attributes={toggleAttributes} fieldData={props.fieldData} />
      )}
      {optionAttributes &&
        optionAttrKeys.map(attrKey => (
          <div key={attrKey}>
            <NDivider size="md" />
            <OptionAttribute
              options={optionAttributes[attrKey].options?.map(opt => ({ value: opt })) || []}
              control={props.formMethods.control}
              name={`attributes.${attrKey}` as keyof IFieldForm}
              label={optionAttributes[attrKey].displayName}
              //@ts-ignore
              defaultValue={props.fieldData?.attributes?.[attrKey] || optionAttributes[attrKey].options?.[0]}
              disabled={watchTextSubType === 'long'}
            />
          </div>
        ))}
    </>
  )
}
