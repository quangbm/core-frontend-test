import React, { FC, useState } from 'react'
import { Controller } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useQuery } from 'react-query'
import styled from 'styled-components/macro'
import { color, spacing } from '../../../../components/GlobalStyle'
import { NDivider } from '../../../../components/NDivider'
import { elevation } from '../../../../components/NElevation'
import { SelectOption } from '../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../components/NTextInput/NTextInput'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../services/api'
import { fieldNameRegex } from '../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../utils/utils'
import { ReactComponent as ManyToOne } from '../icons/many-to-one.svg'
import { DataTypeFormProps } from './DataTypeForm'

const Wrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
`
const InputWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: ${spacing('md')};
  flex-direction: column;
`

const NameTextInput = styled(NTextInput)`
  width: 100%;
`

const ObjectContainer = styled.div`
  min-width: 200px;
  border: 1px solid ${color('Primary700')};
  ${elevation('Elevation100')}
  border-radius: 3px;
`

const ObjectName = styled.div`
  height: 40px;
  box-sizing: border-box;
  border-bottom: 1px solid ${color('Neutral200')};
  display: flex;
  justify-content: center;
  align-items: center;
`

const ConnectDecorator = styled.div`
  width: 16px;
  height: 16px;
  background: ${color('Primary700')};
  border-radius: 0px 8px 8px 0px;
  &.right {
    transform: rotate(-180deg);
  }
`
const Connector = styled.div`
  background: ${color('Primary700')};
  height: 1px;
  width: 100%;
`

const ExternalRelationshipItem = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);

  display: flex;
  justify-content: center;
  align-items: center;
  height: 32px;
  width: 32px;
  box-sizing: border-box;
  border: 1px solid ${color('Neutral200')};
  border-radius: 3px;

  background: ${color('Primary700')};
  color: ${color('white')};
`

type CustomOptions = {
  objectOptions: SelectOption[]
  fieldOptionsByObjectName: Record<string, SelectOption[]>
}

export const IndirectRelationForm: FC<DataTypeFormProps> = ({ formMethods, fieldData, object }) => {
  const { register, formState, setValue, control, getValues } = formMethods
  const { t } = useTranslation()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const [selectedObjectName, setSelectedObjectName] = useState<string | undefined>(fieldData?.value?.split('.')[0])

  const { data: indirectData } = useQuery(
    [QueryKeys.Integrations.getIndirectRelations, { searchText }],
    APIServices.Integrations.getIndirectRelations,
    {
      select: data => {
        const fieldOptionsByObjectName: CustomOptions['fieldOptionsByObjectName'] = {}
        const objectOptions = data.data.map(obj => {
          fieldOptionsByObjectName[obj.name] = obj.fields.map(field => ({
            label: field.displayName,
            value: `${obj.name}.${field.name}`,
          }))
          return { label: obj.displayName, value: obj.name }
        })
        return {
          fieldOptionsByObjectName,
          objectOptions,
        }
      },
    },
  )

  return (
    <Wrapper>
      <ObjectContainer>
        <ObjectName>{object?.displayName}</ObjectName>
        <InputWrapper>
          <NameTextInput
            defaultValue={fieldData?.displayName || ''}
            placeholder="Display name"
            label="Display name"
            error={formState.errors.displayName?.message}
            required
            {...register('displayName', {
              required: {
                value: true,
                message: t('common.error.required'),
              },
            })}
            onBlur={e => {
              if (!!!fieldData) {
                const formatName = getNameFromDisplayName(e.target.value)
                !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
              }
            }}
          />
          <NDivider size="xl" />
          <NameTextInput
            defaultValue={fieldData?.name || ''}
            required
            placeholder="Name"
            label="Field Name"
            error={formState.errors.name?.message}
            {...register('name', {
              required: {
                value: true,
                message: t('common.error.required'),
              },
              pattern: {
                value: fieldNameRegex,
                message: 'Invalid pattern',
              },
            })}
            disabled={!!fieldData}
          />
        </InputWrapper>
      </ObjectContainer>
      <ConnectDecorator />
      <Connector />
      <ExternalRelationshipItem>
        <ManyToOne />
      </ExternalRelationshipItem>
      <ConnectDecorator className="right" />
      <ObjectContainer>
        <InputWrapper>
          <NSingleSelect
            placeholder="Select object"
            fullWidth
            options={indirectData?.objectOptions || []}
            value={selectedObjectName}
            onValueChange={value => {
              if (value !== selectedObjectName) {
                setValue('value', '')
                setSelectedObjectName(value)
              }
            }}
            searchValue={searchValue}
            onSearchValueChange={handleSearchChange}
            isSearchable
            label="Object"
          />
          <NDivider size="md" />
          <Controller
            defaultValue={fieldData?.value}
            control={control}
            name="value"
            rules={{ required: { value: true, message: 'Required' } }}
            render={({ field, fieldState }) => (
              <NSingleSelect
                fullWidth
                options={selectedObjectName ? indirectData?.fieldOptionsByObjectName?.[selectedObjectName] || [] : []}
                disabled={!!!selectedObjectName}
                placeholder="Select field"
                value={field.value}
                onValueChange={value => {
                  field.onChange(value)
                }}
                error={fieldState.error?.message}
                label="Field"
              />
            )}
          />
        </InputWrapper>
      </ObjectContainer>
    </Wrapper>
  )
}
