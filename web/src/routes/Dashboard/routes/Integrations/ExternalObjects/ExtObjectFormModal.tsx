import { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled from 'styled-components/macro'
import { ObjectResponse, PostAction } from '../../../../../../../shared/src/api'
import { spacing } from '../../../../../components/GlobalStyle'
import { NModal } from '../../../../../components/NModal/NModal'
import { NToast } from '../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { ObjectForm, ObjectFormType } from '../../../components/ObjectForm'

const FormWrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  objectData?: ObjectResponse
}

export const ExtObjectFormModal: FC<Props> = ({ visible, setVisible, objectData }) => {
  const formMethods = useForm({
    defaultValues: {
      name: objectData?.name || '',
      recordName: objectData?.recordName.label || '',
      description: objectData?.description || '',
      displayName: objectData?.displayName || '',
      owd: objectData?.owd || 'Private',
    },
  })
  const queryClient = useQueryClient()

  const { mutate: postObject, isLoading } = useMutation(APIServices.Integrations.postExtObject, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(QueryKeys.Integrations.getExtObjectList)
      await queryClient.invalidateQueries([QueryKeys.Integrations.getExtObject, { objName: objectData?.name }])

      setVisible(false)
    },
    onError: error => {
      NToast.error({
        title: `${objectData ? 'Update' : 'Create'} external object unsuccessful`,
        subtitle: error.response?.data.message,
      })
    },
  })

  const onSubmit = (data: ObjectFormType) => {
    postObject({
      data: {
        displayName: data.displayName,
        recordName: {
          label: data.recordName,
          type: 'text',
        },
        name: objectData ? undefined : data.name,
        description: data.description,
        owd: data.owd,
      },
      action: objectData ? PostAction.Update : PostAction.Create,
      name: objectData?.name,
    })
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header title={`${objectData ? 'Update' : 'Create'} external object`} onClose={() => setVisible(false)} />
      <NModal.Body>
        <FormWrapper>
          <ObjectForm objectData={objectData} formMethods={formMethods} />
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer
        isLoading={isLoading}
        onFinish={formMethods.handleSubmit(onSubmit)}
        onCancel={() => setVisible(false)}></NModal.Footer>
    </NModal>
  )
}
