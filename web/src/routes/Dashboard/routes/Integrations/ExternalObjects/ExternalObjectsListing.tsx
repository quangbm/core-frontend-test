import { FC, useEffect, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory, useRouteMatch } from 'react-router-dom'
import { ObjectResponse } from '../../../../../../../shared/src/api'
import { classnames } from '../../../../../common/classnames'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../../hooks/useTableConfigs'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { getTableData } from '../../../../../utils/table-utils'
import { ExtObjectTableActions } from './components/ExtObjectTableActions'
import { ExtObjectFormModal } from './ExtObjectFormModal'

const PAGE_SIZE = 10

export const ExternalObjectsListing: FC = () => {
  const [showExtFormModal, setShowExtFormModal] = useState(false)
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const [editingExtObject, setEditingExtObject] = useState<ObjectResponse>()
  const history = useHistory()
  const { url } = useRouteMatch()

  // remove edit object data when close create modal
  useEffect(() => {
    if (!showExtFormModal) {
      setEditingExtObject(undefined)
    }
  }, [showExtFormModal])

  const { data: externalObjectsData, isLoading: isLoadingObjectsData } = useQuery(
    [
      QueryKeys.Integrations.getExtObjectList,
      { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE, searchText, sortBy, sortOrder },
    ],
    APIServices.Integrations.getExtObjectList,
  )

  const { pageData, pageInfo, totalPage } = useMemo(() => {
    return getTableData(externalObjectsData)
  }, [externalObjectsData])

  const handleEdit = (extObj: ObjectResponse) => {
    setEditingExtObject(extObj)
    setShowExtFormModal(true)
  }

  const columns = useMemo<NTableColumnType<ObjectResponse>>(
    () => [
      { Header: 'Display name', accessor: 'displayName', defaultCanSort: true },
      { Header: 'API name', accessor: 'name', defaultCanSort: true },
      { Header: 'Description', accessor: 'description' },
      {
        Header: 'Record name',
        accessor: 'recordName',
        Cell: ({ value }) => {
          return value.label || null
        },
      },
      {
        Header: 'Created at',
        accessor: 'createdAt',
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = new Date(value)
          return date.toLocaleString('vi')
        },
        defaultCanSort: true,
      },
      {
        Header: 'Last modified',
        accessor: 'updatedAt',
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = new Date(value)
          return date.toLocaleString('vi')
        },
        defaultCanSort: true,
      },
      {
        Header: 'Is System',
        accessor: 'isSystemDefault',
        defaultCanSort: true,
        Cell: ({ value }) => {
          return value ? <GlobalIcons.Check /> : null
        },
      },
      {
        accessor: 'name',
        Cell: ({ value, row }) => {
          if (row.original.isSystemDefault) {
            return <></>
          }
          return <ExtObjectTableActions value={value} data={row.original} onEdit={handleEdit} />
        },
        id: classnames([NTableCollapsedCellClassName]),
      },
    ],
    [],
  )

  return (
    <NContainer>
      <NRow justify="space-between">
        <NContainer.Title>External Objects</NContainer.Title>
        <NButton onClick={() => setShowExtFormModal(true)} type="primary">
          New External Object
        </NButton>
      </NRow>
      <NContainer.Description>{pageInfo.total} objects</NContainer.Description>
      <NTableToolbar
        searchConfig={{
          value: searchValue,
          onChange: e => {
            onChangeSearch(e.target.value)
          },
        }}
      />
      <NTable
        columns={columns}
        isLoading={isLoadingObjectsData}
        data={pageData}
        pageSize={PAGE_SIZE}
        defaultSortBy={sortBy}
        defaultSortOrder={sortOrder}
        onChangeSort={onChangeSort}
        pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
        onClickRow={data => {
          history.push(`${url}/${data.name}`)
        }}
      />
      {showExtFormModal && (
        <ExtObjectFormModal objectData={editingExtObject} visible={showExtFormModal} setVisible={setShowExtFormModal} />
      )}
    </NContainer>
  )
}
