import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { ObjectResponse, PostAction } from '../../../../../../../../shared/src/api'
import { NTableActions } from '../../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../../services/api'

type Props = {
  value: string
  data: ObjectResponse
  onEdit: (data: ObjectResponse) => void
}

export const ExtObjectTableActions: FC<Props> = ({ value, data, onEdit }) => {
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: postObject, isLoading: isDeleting } = useMutation(APIServices.Integrations.postExtObject, {
    onSuccess: async () => {
      await queryClient.invalidateQueries(QueryKeys.Integrations.getExtObjectList)
      setShowDropList(false)
    },
    onError: error => {
      NToast.error({
        title: 'Delete unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const handleEdit = () => {
    onEdit(data)
    setShowDropList(false)
  }

  const handleDelete = () => {
    postObject({
      action: PostAction.Delete,
      name: value,
    })
  }

  return (
    <NTableActions
      showDropList={showDropList}
      setShowDropList={setShowDropList}
      options={[
        { title: 'Edit', onClick: handleEdit },
        { title: 'Delete', isLoading: isDeleting, onClick: handleDelete },
      ]}
    />
  )
}
