import { FC } from 'react'
import { Control, Controller } from 'react-hook-form'
import styled from 'styled-components/macro'
import { RequestVarType } from '../../../../../../../../shared/src/api'
import { color } from '../../../../../../components/GlobalStyle'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { typography } from '../../../../../../components/NTypography'
import { REQUEST_INPUT_TYPE_OPTIONS } from '../../model'

const InputLabel = styled.p`
  ${typography('h400')}
  overflow: hidden;
  text-overflow: ellipsis;
`

const ErrorMessage = styled('div')`
  color: ${color('danger')};
`

const Wrapper = styled.div<{ visible?: boolean }>`
  display: ${props => !props.visible && 'none'};
`

type Props = {
  input: string
  control: Control<Record<string, any>>
  requestVars?: RequestVarType[]
  name: string
  visible?: boolean
  defaultValue?: any
  isDeprecated?: boolean
  onRemove?: (name: string) => void
}

export const InputMappingFormItem: FC<Props> = ({
  input,
  control,
  requestVars = [],
  name,
  visible,
  defaultValue,
  isDeprecated,
  onRemove,
}) => {
  return (
    <Controller
      defaultValue={defaultValue ? { type: 'value', value: defaultValue } : {}}
      name={name}
      control={control}
      render={({ field: { value, onChange } }) => {
        return (
          <Wrapper visible={visible}>
            <NRow align="center">
              <NColumn flex={0.5}>
                <InputLabel>{input}</InputLabel>
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={2}>
                <NSingleSelect
                  value={value?.type}
                  fullWidth
                  label="Type"
                  options={REQUEST_INPUT_TYPE_OPTIONS}
                  onValueChange={newVal => {
                    onChange({
                      type: newVal,
                    })
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn flex={2}>
                {value?.type === 'value' && (
                  <NTextInput
                    label="Value"
                    value={value?.value}
                    onChange={e => {
                      onChange({
                        ...value,
                        value: e.target.value,
                      })
                    }}
                  />
                )}
                {value?.type === 'variable' && (
                  <NSingleSelect
                    value={value?.value}
                    fullWidth
                    label="Value"
                    onValueChange={newVal => {
                      onChange({
                        ...value,
                        value: newVal,
                      })
                    }}
                    options={requestVars.map(requestVar => ({
                      value: requestVar.variable,
                    }))}
                  />
                )}
                {value?.type === 'template' && (
                  <NTextArea
                    rows={4}
                    label="Value"
                    value={value?.value}
                    onChange={e => {
                      onChange({
                        ...value,
                        value: e.target.value,
                      })
                    }}
                  />
                )}
              </NColumn>
            </NRow>
            {isDeprecated && (
              <>
                <NDivider size="xs" />
                <NRow align="center">
                  <ErrorMessage>{`* This config (${input}) is deprecated.`}</ErrorMessage>
                  <NDivider vertical size="xs" />
                  <NButton onClick={() => onRemove && onRemove(name)} type="link">
                    Remove now
                  </NButton>
                </NRow>
              </>
            )}
            <NDivider size="md" />
          </Wrapper>
        )
      }}
    />
  )
}
