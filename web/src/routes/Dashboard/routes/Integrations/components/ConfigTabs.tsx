import styled, { css } from 'styled-components/macro'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { typography } from '../../../../../components/NTypography'

const TabStyle = styled.div<{ active?: boolean }>`
  display: flex;
  align-items: center;
  height: 20px;
  border-radius: 4px;
  padding: 0 ${spacing('xs')};
  ${typography('small-overline')}
  cursor: pointer;

  ${props => {
    if (props.active) {
      return css`
        background-color: ${color('white')};
        color: ${color('Neutral700')};
        border-right: none;
      `
    }

    return css`
      background-color: ${color('transparent')};
      color: ${color('Neutral400')};

      &:hover {
        background-color: ${color('Neutral300')};
      }
    `
  }}
  transition: background-color 0.2s ease-in-out, color 0.2s ease-in-out;
`

const TabContainer = styled.div`
  display: flex;
  align-items: center;
  margin-left: ${spacing('xs')};
  height: 24px;
  border-radius: 6px;
  padding: 0 2px;
  background-color: ${color('Neutral200')};
  ${TabStyle} {
    border-right: 1px solid ${color('white')};
  }
  ${TabStyle}:last-child {
    border-right: none;
  }
`

type Props<T> = {
  tabOptions: { label: string; value: T }[]
  selectedTab: T
  onSelectTab: (newOption: T) => void
}

export function ConfigTabs<T>({ selectedTab, onSelectTab, tabOptions }: Props<T>) {
  return (
    <TabContainer>
      {tabOptions.map(op => {
        return (
          <TabStyle
            key={`tab-key-${op.value}`}
            active={op.value === selectedTab}
            onClick={e => {
              e.stopPropagation()
              onSelectTab(op.value)
            }}>
            {op.label}
          </TabStyle>
        )
      })}
    </TabContainer>
  )
}
