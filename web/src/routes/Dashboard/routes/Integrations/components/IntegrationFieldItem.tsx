import { FC } from 'react'
import styled from 'styled-components/macro'
import { color, fontSize, spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NHyperlinkOrText } from '../../../../../components/NHyperlinkOrText'
import { typography } from '../../../../../components/NTypography'

const FieldWrapper = styled.div`
  padding: ${spacing('sm')} ${spacing('md')};
`

const FieldLabel = styled.p`
  color: ${color('Neutral700')};
  font-size: ${fontSize('xs')};
  line-height: ${fontSize('xs')};
  font-weight: bold;
`

const FieldValue = styled.p`
  ${typography('small-bold-text')}
  color: ${color('Neutral400')};
`

type FieldItemProps = {
  title: string
  value?: string | number
}
export const IntegrationFieldItem: FC<FieldItemProps> = ({ title, value }) => {
  return (
    <FieldWrapper>
      <FieldLabel>{title}</FieldLabel>
      <NDivider size="xs" />
      <FieldValue>
        <NHyperlinkOrText>{value}</NHyperlinkOrText>
      </FieldValue>
    </FieldWrapper>
  )
}
