import React, { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { ActionMetadataResponse } from '../../../../../../../shared/src/api'
import { NTableActions } from '../../../../../components/NTable/NTableActions'
import { APIServices, QueryKeys } from '../../../../../services/api'

type ActionMetadataTableActionProps = {
  data: ActionMetadataResponse
}
export const ActionMetadataTableAction: FC<ActionMetadataTableActionProps> = ({ data }) => {
  const [showDropList, setShowDropList] = useState(false)
  // post object mutation
  const [isLoading, setIsLoading] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: deleteActionMetadata } = useMutation(APIServices.Integrations.deleteActionMetadata, {
    onSuccess: () => {
      queryClient.invalidateQueries(QueryKeys.Integrations.getAllActionMetadata)
    },
  })

  const handleDelete = () => {
    setIsLoading(true)
    deleteActionMetadata({ name: data.name })
  }
  return (
    <NTableActions
      showDropList={showDropList}
      setShowDropList={setShowDropList}
      options={[{ title: 'Delete', isLoading, onClick: handleDelete }]}
    />
  )
}
