import * as React from 'react'
import { Controller, useFieldArray, useFormContext } from 'react-hook-form'
import { GlobalIcons } from '../../../../../../components/Icons'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NTextArea } from '../../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { ActionMetadataFieldValues, RULES } from '../constants'
import { InputItemWrapper } from './StyledCommon'

export const HeaderInput = React.memo(function Node() {
  const { control } = useFormContext<ActionMetadataFieldValues>()
  const name = 'inputs.header'
  const { fields, remove, append } = useFieldArray({ control, name })
  return (
    <React.Fragment>
      {fields.map((field, fieldIndex) => {
        return (
          <InputItemWrapper key={field.id}>
            <NRow align="flex-end">
              <NColumn>
                <Controller
                  name={`${name}.${fieldIndex}.name`}
                  control={control}
                  render={({ field: { value, onChange }, fieldState }) => (
                    <NTextInput
                      value={value}
                      onChange={e => onChange(e.target.value)}
                      label="Name"
                      required
                      error={fieldState.error?.message}
                    />
                  )}
                  rules={RULES}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <div className="delete-btn">
                <NButton
                  className="delete-btn"
                  type="ghost"
                  icon={<GlobalIcons.Trash />}
                  onClick={() => {
                    remove(fieldIndex)
                  }}
                />
              </div>
            </NRow>
            <NDivider size="xl" />
            <Controller
              control={control}
              name={`${name}.${fieldIndex}.description`}
              render={({ field: { value, onChange } }) => (
                <NTextArea label="Description" value={value} onChange={e => onChange(e.target.value)} />
              )}
            />
            <NDivider size="xl" />
            <Controller
              control={control}
              name={`${name}.${fieldIndex}.isRequired`}
              render={({ field: { value, onChange } }) => (
                <NCheckbox title="Required" checked={Boolean(value)} onChange={e => onChange(e.target.checked)} />
              )}
            />
            <NDivider size="xl" />
            <Controller
              name={`${name}.${fieldIndex}.default`}
              control={control}
              render={({ field: { value, onChange } }) => {
                return <NTextInput label="Default Value" value={value} onChange={e => onChange(e.target.value)} />
              }}
            />
          </InputItemWrapper>
        )
      })}
      <NButton type="primary" size="small" onClick={() => append({ name: '', isRequired: false })}>
        <GlobalIcons.Plus />
      </NButton>
    </React.Fragment>
  )
})

HeaderInput.displayName = 'QueryInput'
