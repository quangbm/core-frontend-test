import { omit } from 'lodash'
import { FC, useMemo, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import styled from 'styled-components/macro'
import { ActionMetadataResponse, InputMetadata } from '../../../../../../../../shared/src/api'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NCodeEditor } from '../../../../../../components/NCodeEditor'
import { NDivider } from '../../../../../../components/NDivider'
import { NRow } from '../../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { typography } from '../../../../../../components/NTypography'
import { SettingBoard } from '../../../../../../components/SettingBoard'
import { APIServices } from '../../../../../../services/api'

const ContentWrapper = styled.div`
  padding: ${spacing('md')};
  overflow-x: auto;
`
const Label = styled.p`
  ${typography('small-bold-text')}
`
const Wrapper = styled.div`
  max-height: 60vh;
  overflow: auto;
`

const FullWidthInput = styled(NTextInput)`
  width: 100%;
`

const StyledRow = styled(NRow)`
  margin-bottom: ${spacing('xs')};
`

const Error = styled.p`
  margin-top: ${spacing('xs')};
  color: ${color('danger')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  objectName?: string
  actionMetadata: ActionMetadataResponse
}

export const TestMetadataModal: FC<Props> = ({ visible, setVisible, actionMetadata }) => {
  const { register, handleSubmit } = useForm({ shouldUnregister: false })
  const [excludedFields, setExcludedFields] = useState<string[]>([])

  const {
    mutate: testMetadata,
    isLoading,
    data: testResult,
  } = useMutation(APIServices.Integrations.testActionMetadata, {
    onError: error => {
      NToast.error({ title: 'Fail to test', subtitle: error.response?.data.message })
    },
  })

  const actionInputs: string[] = useMemo(() => {
    if (actionMetadata.inputs) {
      return constructFormKeys(actionMetadata.inputs)
    }
    return []
  }, [actionMetadata])

  const onSubmit = (data: Record<string, any>) => {
    testMetadata({ data: omit(constructSubmitData(data), excludedFields), name: actionMetadata.name })
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header
        title={`Test metadata`}
        onClose={() => {
          setVisible(false)
        }}
      />
      <NModal.Body>
        <Wrapper>
          <ContentWrapper>
            <SettingBoard
              action={
                <NButton size="small" type="primary" onClick={handleSubmit(onSubmit)} loading={isLoading}>
                  Submit
                </NButton>
              }
              title="Request">
              <ContentWrapper>
                {actionInputs.map(input => {
                  // NOTE: Excluded fields => reverse checked
                  const isChecked = !excludedFields.some(field => field === input)
                  return (
                    <StyledRow align="center" key={input}>
                      <NCheckbox
                        checked={isChecked}
                        value={input}
                        onChange={e => {
                          if (!isChecked) {
                            setExcludedFields(prev => prev.filter(field => field !== e.target.value))
                          } else {
                            setExcludedFields(prev => [...prev, e.target.value])
                          }
                        }}
                      />
                      <NDivider vertical size="md" />
                      <FullWidthInput key={`${input}`} {...register(input)} label={`${input}`} />
                    </StyledRow>
                  )
                })}
              </ContentWrapper>
            </SettingBoard>
            {testResult?.data && (
              <SettingBoard title="Response">
                <ContentWrapper>
                  {testResult?.data.url && (
                    <>
                      <Label>URL: {testResult?.data.url}</Label>
                      <NDivider size="md" />
                    </>
                  )}

                  {testResult?.data.status && (
                    <>
                      <Label>Status: {testResult?.data.status} </Label>
                      <NDivider size="md" />
                    </>
                  )}
                  {testResult?.data.headers && (
                    <SettingBoard title="Header">
                      <ContentWrapper>
                        <NCodeEditor
                          language="json"
                          disabled
                          value={JSON.stringify(testResult?.data.headers, null, 2) || ''}
                        />
                      </ContentWrapper>
                    </SettingBoard>
                  )}
                  <SettingBoard title={testResult?.data.data ? 'Data' : 'Error'}>
                    <ContentWrapper>
                      <NCodeEditor
                        language="json"
                        disabled
                        value={JSON.stringify(testResult?.data.data, null, 2) || ''}
                      />
                      {testResult?.data.error && <Error>{testResult?.data.error}</Error>}
                    </ContentWrapper>
                  </SettingBoard>
                </ContentWrapper>
              </SettingBoard>
            )}
          </ContentWrapper>
        </Wrapper>
      </NModal.Body>
    </NModal>
  )
}

function constructFormKeys(inputs: InputMetadata): string[] {
  const formKeys = []

  if (inputs.path && inputs.path.length > 0) {
    inputs.path.forEach(pathKey => {
      formKeys.push(`path.${pathKey}`)
    })
  }
  if (inputs.query) {
    Object.keys(inputs.query).forEach(queryKey => {
      formKeys.push(`query.${queryKey}`)
    })
  }
  if (inputs.header) {
    Object.keys(inputs.header).forEach(headerKey => {
      formKeys.push(`header.${headerKey}`)
    })
  }
  if (inputs.body) {
    formKeys.push('body')
  }
  return formKeys
}

function constructSubmitData(data: Record<string, any>): Record<string, any> {
  const inputMap: Record<string, any> = {}
  data.path &&
    Object.keys(data.path).forEach(input => {
      inputMap[`path.${input}`] = data.path[input]
    })
  data.header &&
    Object.keys(data.header).forEach(input => {
      inputMap[`header.${input}`] = data.header[input]
    })
  data.query &&
    Object.keys(data.query).forEach(input => {
      inputMap[`query.${input}`] = data.query[input]
    })
  inputMap[`body`] = data.body
  return inputMap
}
