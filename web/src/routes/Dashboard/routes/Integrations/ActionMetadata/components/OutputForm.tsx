import * as React from 'react'
import { Controller, useFieldArray, useFormContext } from 'react-hook-form'
import { GlobalIcons } from '../../../../../../components/Icons'
import { NAccordion } from '../../../../../../components/NAccordion/NAccordion'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { ActionMetadataFieldValues, DATA_TYPE_OPTIONS, RULES } from '../constants'
import { AccordionContent, AccordionTitle, InputItemWrapper } from './StyledCommon'

type OutputFormProps = {}

export const OutputForm = React.memo(function Node({}: OutputFormProps) {
  const { control } = useFormContext<ActionMetadataFieldValues>()
  const name = 'outputs'
  const { fields, append, remove } = useFieldArray({ control, name })
  return (
    <NAccordion title={<AccordionTitle>Output</AccordionTitle>}>
      <AccordionContent>
        {fields.map((field, fieldIndex) => {
          return (
            <InputItemWrapper key={field.id}>
              <NRow align="flex-end">
                <NColumn>
                  <Controller
                    name={`${name}.${fieldIndex}.name`}
                    control={control}
                    render={({ field, fieldState }) => (
                      <NTextInput
                        value={field.value || ''}
                        onChange={e => field.onChange(e.target.value)}
                        label="HTTP Code"
                        required
                        error={fieldState.error?.message}
                      />
                    )}
                    rules={RULES}
                  />
                </NColumn>
                <NDivider vertical size="xl" />
                <NColumn>
                  <Controller
                    name={`${name}.${fieldIndex}.type`}
                    control={control}
                    render={({ field }) => (
                      <NSingleSelect
                        label="Type"
                        options={DATA_TYPE_OPTIONS}
                        value={field.value || ''}
                        onValueChange={field.onChange}
                      />
                    )}
                    rules={RULES}
                  />
                </NColumn>
                <NDivider vertical size="xl" />
                <div className="delete-btn">
                  <NButton
                    className="delete-btn"
                    type="ghost"
                    icon={<GlobalIcons.Trash />}
                    onClick={() => {
                      remove(fieldIndex)
                    }}
                  />
                </div>
              </NRow>
              <NDivider size="xl" />
              <Controller
                control={control}
                name={`${name}.${fieldIndex}.description`}
                render={({ field }) => (
                  <NTextArea
                    label="Description"
                    value={field.value || ''}
                    onChange={e => field.onChange(e.target.value)}
                  />
                )}
              />
            </InputItemWrapper>
          )
        })}
        <NButton type="primary" size="small" onClick={() => append({})}>
          <GlobalIcons.Plus />
        </NButton>
      </AccordionContent>
    </NAccordion>
  )
})

OutputForm.displayName = 'OutputForm'
