import { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { color, fontSize, spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NHyperlinkOrText } from '../../../../../components/NHyperlinkOrText'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { typography } from '../../../../../components/NTypography'
import { SettingBoard } from '../../../../../components/SettingBoard'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { ChangeAuthenModal } from './components/ChangeAuthenModal'
import { UpdateCredentialModal } from './components/UpdateCredentialModal'

const FieldWrapper = styled.div`
  padding: ${spacing(14)} ${spacing('md')};
`

const FieldLabel = styled.p`
  color: ${color('Neutral700')};
  font-size: ${fontSize('xs')};
  line-height: ${fontSize('xs')};
  font-weight: bold;
`

const FieldValue = styled.p`
  ${typography('small-bold-text')}
  color: ${color('Neutral400')};
`

const BoardWrapper = styled.div`
  display: grid;
  padding: ${spacing('xl')} ${spacing('xs')};
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${spacing('xl')};
`

type CredentialFieldItemProps = {
  title: string
  value?: string
}

const CredentialFieldItem: FC<CredentialFieldItemProps> = ({ title, value }) => {
  return (
    <FieldWrapper>
      <FieldLabel>{title}</FieldLabel>
      <NDivider size="xs" />
      <FieldValue>
        <NHyperlinkOrText>{value}</NHyperlinkOrText>
      </FieldValue>
    </FieldWrapper>
  )
}

export const CredentialDetail: FC = () => {
  const { credentialName } = useParams<{ credentialName: string }>()
  const [showChangeAuthen, setShowChangeAuthen] = useState(false)
  const [showUpdateCredential, setShowUpdateCredential] = useState(false)

  const { data: credentialData } = useQuery(
    [QueryKeys.Integrations.getCredential, { name: credentialName }],
    APIServices.Integrations.getCredential,
  )

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[
          { label: 'Intergrations', path: `${DASHBOARD_ROUTE}/integrations` },
          { label: 'Credentials', path: `${DASHBOARD_ROUTE}/integrations/credentials` },
          { label: credentialData?.data.displayName || '' },
        ]}
      />
      <NContainer.Title>{credentialData?.data.displayName}</NContainer.Title>
      {credentialData?.data.description && (
        <NContainer.Description>{credentialData?.data.description}</NContainer.Description>
      )}
      <SettingBoard
        title="Credential"
        action={
          <NButton size="small" onClick={() => setShowUpdateCredential(true)}>
            Edit
          </NButton>
        }>
        <BoardWrapper>
          <CredentialFieldItem title="Display name" value={credentialData?.data.displayName} />
          <CredentialFieldItem title="API name" value={credentialData?.data.name} />
          <CredentialFieldItem title="URL" value={credentialData?.data.url} />
        </BoardWrapper>
      </SettingBoard>
      <SettingBoard
        title="Authentication"
        action={
          <NButton size="small" onClick={() => setShowChangeAuthen(true)}>
            Change
          </NButton>
        }>
        <BoardWrapper>
          <CredentialFieldItem title="Identity type" value={credentialData?.data.identityType} />
          <CredentialFieldItem title="Auth protocol" value={credentialData?.data.authProtocol} />
        </BoardWrapper>
      </SettingBoard>
      {showChangeAuthen && credentialData?.data && (
        <ChangeAuthenModal visible={showChangeAuthen} setVisible={setShowChangeAuthen} data={credentialData?.data} />
      )}
      {showUpdateCredential && (
        <UpdateCredentialModal
          credential={credentialData?.data}
          visible={showUpdateCredential}
          setVisible={setShowUpdateCredential}
        />
      )}
    </NContainer>
  )
}
