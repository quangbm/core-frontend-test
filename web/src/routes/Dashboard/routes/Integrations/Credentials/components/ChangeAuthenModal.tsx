import { FC } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { CredentialResponse } from '../../../../../../../../shared/src/api'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../../../components/NPerfectScrollbar'
import { NToast } from '../../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../../services/api'
import { AuthProtocolFormItems } from './AuthProtocolFormItems'

const CredentialForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
  max-height: 70vh;
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  data: CredentialResponse
}

export const ChangeAuthenModal: FC<Props> = ({ visible, setVisible, data }) => {
  const formMethods = useForm({
    defaultValues: {
      identityType: data.identityType,
      authProtocol: data.authProtocol,
    },
  })
  const { handleSubmit } = formMethods
  const queryClient = useQueryClient()
  const { credentialName } = useParams<{ credentialName: string }>()

  const { mutate: updateCredential, isLoading: isUpdating } = useMutation(APIServices.Integrations.updateCredential, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([QueryKeys.Integrations.getCredentials])
      await queryClient.invalidateQueries([QueryKeys.Integrations.getCredential, { name: credentialName }])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Update credential', subtitle: error.response?.data.message })
    },
  })
  const onSubmit = (data: any) => {
    const submitData = data
    if (data.authProtocol === 'oauth' && data?.additionalConfig) {
      submitData.credential.additionalConfig = data.additionalConfig.reduce(
        (a: Record<string, string>, item: { key: string; value: string }) => {
          a[item.key] = item.value
          return a
        },
        {},
      )
    }
    submitData.additionalConfig = undefined
    updateCredential({
      name: credentialName,
      ...submitData,
    })
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header title="Change authentication" onClose={() => setVisible(false)} />
      <NModal.Body>
        <NPerfectScrollbar>
          <CredentialForm>
            <FormProvider {...formMethods}>
              <AuthProtocolFormItems />
            </FormProvider>
          </CredentialForm>
        </NPerfectScrollbar>
      </NModal.Body>
      <NModal.Footer isLoading={isUpdating} onFinish={handleSubmit(onSubmit)} onCancel={() => setVisible(false)} />
    </NModal>
  )
}
