import React, { FC } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled from 'styled-components/macro'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../../../components/NPerfectScrollbar'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { useValidateString } from '../../../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../../../services/api'
import { fieldNameRegex } from '../../../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../../../utils/utils'
import { AuthProtocolFormItems } from './AuthProtocolFormItems'

const CredentialForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
  max-height: 70vh;
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
}

export const CreateCredentialModal: FC<Props> = ({ visible, setVisible }) => {
  const formMethods = useForm()
  const { register, setValue, handleSubmit, formState, getValues } = formMethods
  const queryClient = useQueryClient()
  const { validateFunction } = useValidateString()

  const { mutate: createCredential, isLoading: isCreating } = useMutation(APIServices.Integrations.createCredential, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([QueryKeys.Integrations.getCredentials])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Create credential', subtitle: error.response?.data.message })
    },
  })

  const onSubmit = (data: any) => {
    const submitData = data
    if (data.authProtocol === 'oauth' && data?.additionalConfig) {
      submitData.credential.additionalConfig = data.additionalConfig.reduce(
        (a: Record<string, string>, item: { key: string; value: string }) => {
          a[item.key] = item.value
          return a
        },
        {},
      )
    }
    //clear temp fieldArray
    submitData.additionalConfig = undefined
    createCredential(submitData)
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header title="Credential" onClose={() => setVisible(false)} />
      <NModal.Body>
        <NPerfectScrollbar>
          <CredentialForm>
            <NRow>
              <NColumn>
                <NTextInput
                  required
                  {...register('displayName', {
                    validate: validateFunction,
                  })}
                  label="Display name"
                  error={formState.errors?.displayName?.message}
                  onBlur={e => {
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }}
                />
              </NColumn>
              <NDivider vertical size="xl" />
              <NColumn>
                <NTextInput
                  required
                  label="API name"
                  error={formState.errors?.name?.message}
                  {...register('name', {
                    validate: validateFunction,
                    pattern: {
                      value: fieldNameRegex,
                      message: 'Invalid pattern',
                    },
                  })}
                  caption="The API name is used to generate the API routes and databases tables/collections"
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <NRow>
              <NColumn>
                <NTextInput
                  required
                  {...register('url', {
                    validate: validateFunction,
                  })}
                  label="URL"
                  error={formState.errors?.url?.message}
                />
              </NColumn>
            </NRow>
            <NDivider size="xl" />
            <FormProvider {...formMethods}>
              <AuthProtocolFormItems />
            </FormProvider>
          </CredentialForm>
        </NPerfectScrollbar>
      </NModal.Body>
      <NModal.Footer isLoading={isCreating} onFinish={handleSubmit(onSubmit)} onCancel={() => setVisible(false)} />
    </NModal>
  )
}
