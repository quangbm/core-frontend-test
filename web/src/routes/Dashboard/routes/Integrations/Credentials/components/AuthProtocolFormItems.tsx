import { Controller, useFieldArray, useFormContext } from 'react-hook-form'
import styled from 'styled-components/macro'
import { spacing } from '../../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../../components/Icons'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NTypography } from '../../../../../../components/NTypography'
import { useValidateString } from '../../../../../../hooks/useValidateString'
import { AUTH_PROTOCOLS, IDENTITY_TYPES } from '../../model'

const TrashBtn = styled(NButton)`
  min-width: 40px;
`

const ConfigRow = styled(NRow)`
  margin-bottom: ${spacing('xl')};
`

export const AuthProtocolFormItems = () => {
  const { register, setValue, control, formState, watch } = useFormContext()
  const watchIdentityType = watch('identityType')
  const watchAuthProtocol = watch('authProtocol')
  const { validateFunction } = useValidateString()
  const {
    fields: configs,
    append,
    remove,
  } = useFieldArray({
    control,
    name: 'additionalConfig',
  })

  return (
    <>
      <NRow>
        <NColumn>
          <Controller
            control={control}
            name="identityType"
            render={({ field }) => (
              <NSingleSelect
                fullWidth
                options={IDENTITY_TYPES.map(type => ({ value: type }))}
                label="Identity type"
                value={field.value}
                onValueChange={value => {
                  field.onChange(value)
                  if (value === 'anon') {
                    setValue('authProtocol', undefined)
                    setValue('credential', undefined)
                    return
                  }
                  setValue('credential', {})
                }}
              />
            )}
          />
        </NColumn>
        <NDivider vertical size="xl" />
        <NColumn>
          {watchIdentityType !== 'anon' && (
            <Controller
              control={control}
              name="authProtocol"
              render={({ field }) => (
                <NSingleSelect
                  fullWidth
                  options={AUTH_PROTOCOLS.map(item => ({ value: item as string }))}
                  label="Auth protocol"
                  value={field.value}
                  onValueChange={field.onChange}
                />
              )}
            />
          )}
        </NColumn>
      </NRow>
      {watchIdentityType !== 'anon' && watchAuthProtocol && (
        <>
          <NDivider size="xl" />
          {(() => {
            switch (watchAuthProtocol) {
              case 'password':
                return (
                  <NRow>
                    <NColumn>
                      <NTextInput
                        required
                        {...register('credential.username', {
                          validate: validateFunction,
                        })}
                        label="Username"
                        error={formState.errors.credential?.username?.message}
                      />
                    </NColumn>
                    <NDivider vertical size="xl" />
                    <NColumn>
                      <NTextInput label="Password" type="password" {...register('credential.password')} />
                    </NColumn>
                  </NRow>
                )
              case 'oauth':
                return (
                  <>
                    <NTextInput
                      required
                      {...register('credential.endpoint', {
                        validate: validateFunction,
                      })}
                      label="Endpoint"
                      error={formState.errors.credential?.endpoint?.message}
                    />
                    <NDivider size="xl" />
                    <NRow>
                      <NColumn>
                        <NTextInput
                          required
                          {...register('credential.clientId', {
                            validate: validateFunction,
                          })}
                          label="Client id"
                          error={formState.errors.credential?.clientId?.message}
                        />
                      </NColumn>
                      <NDivider vertical size="xl" />
                      <NColumn>
                        <NTextInput
                          required
                          label="Client secret"
                          {...register('credential.clientSecret', {
                            validate: validateFunction,
                          })}
                        />
                      </NColumn>
                    </NRow>
                    <NDivider size="xl" />
                    <NTypography.InputLabel>Additional config</NTypography.InputLabel>
                    <NDivider size="xl" />
                    {configs.map((config, index) => (
                      <ConfigRow key={config.id} align="flex-end">
                        <NColumn>
                          <NTextInput
                            required
                            label="Key"
                            {...register(`additionalConfig.${index}.key`, {
                              validate: validateFunction,
                            })}
                          />
                        </NColumn>
                        <NDivider vertical size="xl" />
                        <NColumn>
                          <NTextInput
                            required
                            label="Value"
                            {...register(`additionalConfig.${index}.value`, {
                              validate: validateFunction,
                            })}
                          />
                        </NColumn>
                        <NDivider vertical size="xl" />
                        <TrashBtn onClick={() => remove(index)} icon={<GlobalIcons.Trash />} />
                      </ConfigRow>
                    ))}
                    <NButton
                      onClick={() => append({ key: '', value: '' })}
                      type="primary"
                      icon={<GlobalIcons.Plus />}
                    />
                  </>
                )
            }
          })()}
        </>
      )}
    </>
  )
}
