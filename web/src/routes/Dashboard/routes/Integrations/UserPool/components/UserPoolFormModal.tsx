import { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled from 'styled-components/macro'
import { CreateUserPoolDto, UserPoolResponse } from '../../../../../../../../shared/src/api'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NSwitch } from '../../../../../../components/NSwitch/NSwitch'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { useValidateString } from '../../../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../../../services/api'
import { emailRegex } from '../../../../../../utils/regex'

const FormWrapper = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  userData?: UserPoolResponse
}

export const UserPoolFormModal: FC<Props> = ({ visible, setVisible, userData }) => {
  const { handleSubmit, register, formState, control } = useForm<CreateUserPoolDto>()
  const { validateFunction } = useValidateString()
  const queryClient = useQueryClient()

  const { mutate: createUser, isLoading: isCreating } = useMutation(APIServices.Integrations.createUser, {
    onSuccess: () => {
      queryClient.invalidateQueries(QueryKeys.Integrations.getUserPool)
      setVisible(false)
    },
    onError: error => {
      NToast.error({
        title: 'Create user unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const { mutate: updateUser, isLoading: isUpdating } = useMutation(APIServices.Integrations.updateUser, {
    onSuccess: () => {
      queryClient.invalidateQueries(QueryKeys.Integrations.getUserPool)
      queryClient.invalidateQueries([QueryKeys.Integrations.getUser, { id: userData?.id }])

      setVisible(false)
    },
    onError: error => {
      NToast.error({
        title: 'Create user unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const onSubmit = (data: CreateUserPoolDto) => {
    if (userData) {
      updateUser({
        ...data,
        id: userData.id,
        email: data.email ? data.email : undefined,
      })
      return
    }
    createUser({
      ...data,
      email: data.email ? data.email : undefined,
    })
  }

  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header title={userData ? 'Update user' : 'Create user'} onClose={() => setVisible(false)} />
      <NModal.Body>
        <FormWrapper>
          <NRow>
            <NColumn flex={1}>
              <NTextInput
                {...register('username', {
                  validate: userData ? undefined : validateFunction,
                })}
                defaultValue={userData?.username || ''}
                disabled={!!userData}
                required
                error={formState.errors.username?.message}
                placeholder="Username"
                label="Username"
              />
            </NColumn>
          </NRow>
          <NDivider size="md" />
          <NRow>
            <NColumn flex={1}>
              <NTextInput
                {...register('email', {
                  pattern: {
                    value: emailRegex,
                    message: 'Invalid email',
                  },
                })}
                defaultValue={userData?.email || ''}
                error={formState.errors.email?.message}
                placeholder="Email"
                label="Email"
              />
            </NColumn>
          </NRow>
          <NDivider size="md" />
          <NRow>
            <NColumn>
              <NTextInput
                {...register('firstName')}
                defaultValue={userData?.firstName || ''}
                error={formState.errors.firstName?.message}
                placeholder="First name"
                label="First name"
              />
            </NColumn>
            <NDivider size="md" vertical />
            <NColumn>
              <NTextInput
                {...register('lastName')}
                defaultValue={userData?.lastName || ''}
                error={formState.errors.lastName?.message}
                placeholder="Last name"
                label="Last name"
              />
            </NColumn>
          </NRow>
          <NDivider size="md" />
          <Controller
            defaultValue={userData?.enabled || true}
            control={control}
            name="enabled"
            render={({ field }) => <NSwitch title="Enabled" checked={Boolean(field.value)} onChange={field.onChange} />}
          />
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer
        isLoading={isCreating || isUpdating}
        onCancel={() => setVisible(false)}
        onFinish={handleSubmit(onSubmit)}
      />
    </NModal>
  )
}
