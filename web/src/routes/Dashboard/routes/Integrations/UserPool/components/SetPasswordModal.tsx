import { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { SetUserPwdDto } from '../../../../../../../../shared/src/api'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { useValidateString } from '../../../../../../hooks/useValidateString'
import { APIServices } from '../../../../../../services/api'

const FormWrapper = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
}

export const SetPasswordModal: FC<Props> = ({ visible, setVisible }) => {
  const { handleSubmit, register, formState } = useForm<SetUserPwdDto>()
  const { validateFunction } = useValidateString()
  const { userId } = useParams<{ userId: string }>()

  const { mutate: setPassword, isLoading: isUpdating } = useMutation(APIServices.Integrations.setUserPassword, {
    onSuccess: () => {
      NToast.success({ title: 'Password updated' })
      setVisible(false)
    },
    onError: error => {
      NToast.error({
        title: 'Set password unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const onSubmit = (data: SetUserPwdDto) => {
    setPassword({
      ...data,
      id: userId,
    })
  }

  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header title="Set password" onClose={() => setVisible(false)} />
      <NModal.Body>
        <FormWrapper>
          <NTextInput
            {...register('password', {
              validate: validateFunction,
            })}
            type="password"
            defaultValue={''}
            required
            error={formState.errors.password?.message}
            placeholder="Password"
            label="Password"
          />
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer isLoading={isUpdating} onCancel={() => setVisible(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
