import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { NTableActions } from '../../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../../services/api'

type Props = {
  userId: string
}

export const UserPoolTableAction: FC<Props> = ({ userId }) => {
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: deleteUser, isLoading: isDeleting } = useMutation(APIServices.Integrations.deleteUser, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([QueryKeys.Integrations.getUserPool])
    },
    onError: error => {
      NToast.error({ title: 'Delete user', subtitle: error.response?.data.message })
    },
  })

  return (
    <NTableActions
      showDropList={showDropList}
      setShowDropList={setShowDropList}
      options={[{ title: 'Delete', isLoading: isDeleting, onClick: () => deleteUser({ id: userId }) }]}
    />
  )
}
