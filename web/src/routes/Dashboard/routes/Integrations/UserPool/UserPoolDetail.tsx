import { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { SettingBoard } from '../../../../../components/SettingBoard'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { IntegrationFieldItem } from '../components/IntegrationFieldItem'
import { SetPasswordModal } from '../UserPool/components/SetPasswordModal'
import { UserPoolFormModal } from './components/UserPoolFormModal'

const BoardWrapper = styled.div`
  display: grid;
  padding: ${spacing('xl')} ${spacing('xs')};
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${spacing('xl')};
`

const ErrorMessage = styled.p`
  color: ${color('danger')};
  margin-bottom: ${spacing('sm')};
`

export const UserPoolDetail: FC = () => {
  const { userId } = useParams<{ userId: string }>()
  const { data: userData, error } = useQuery(
    [QueryKeys.Integrations.getUser, { id: userId }],
    APIServices.Integrations.getUser,
  )
  const [showEdit, setShowEdit] = useState(false)
  const [showUpdatePwd, setShowUpdatePwd] = useState(false)

  return (
    <NContainer>
      {showEdit && userData?.data && (
        <UserPoolFormModal visible={showEdit} setVisible={setShowEdit} userData={userData?.data} />
      )}
      {showUpdatePwd && <SetPasswordModal visible={showUpdatePwd} setVisible={setShowUpdatePwd} />}
      <NDetailHeader
        breadcrumbs={[
          { label: 'Integrations', path: `${DASHBOARD_ROUTE}/integrations` },
          { label: 'User Pool', path: `${DASHBOARD_ROUTE}/integrations/user-pool` },
          { label: userData?.data.username || '' },
        ]}
      />
      <NContainer.Title>{userData?.data.username}</NContainer.Title>
      <SettingBoard
        title="User detail"
        action={
          <NRow>
            <NButton size="small" onClick={() => setShowUpdatePwd(true)} type="outline">
              Set password
            </NButton>
            <NDivider vertical size="md" />
            <NButton size="small" onClick={() => setShowEdit(true)} type="outline">
              Edit
            </NButton>
          </NRow>
        }>
        {error && <ErrorMessage>{error.response?.data.message}</ErrorMessage>}
        <BoardWrapper>
          <IntegrationFieldItem title="Username" value={userData?.data.username} />
          <IntegrationFieldItem title="Email" value={userData?.data.email} />
          <IntegrationFieldItem title="First name" value={userData?.data.firstName} />
          <IntegrationFieldItem title="Last name" value={userData?.data.lastName} />
        </BoardWrapper>
      </SettingBoard>
    </NContainer>
  )
}
