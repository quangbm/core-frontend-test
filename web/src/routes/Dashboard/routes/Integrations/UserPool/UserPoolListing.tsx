import { FC, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { UserPoolResponse } from '../../../../../../../shared/src/api'
import { classnames } from '../../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../../hooks/useTableConfigs'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { getTableData } from '../../../../../utils/table-utils'
import { UserPoolFormModal } from './components/UserPoolFormModal'
import { UserPoolTableAction } from './components/UserPoolTableAction'

const PAGE_SIZE = 10

const COLUMNS: NTableColumnType<UserPoolResponse> = [
  { Header: 'Username', accessor: 'username' },
  { Header: 'First name', accessor: 'firstName' },
  { Header: 'Last name', accessor: 'lastName' },
  { Header: 'Email', accessor: 'email' },
  {
    Header: 'Enabled',
    accessor: 'enabled',
    Cell: ({ value }) => {
      return value ? <GlobalIcons.Check /> : null
    },
  },
  {
    Header: 'Created at',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
  },
  {
    accessor: 'id',
    Cell: ({ value }) => {
      return <UserPoolTableAction userId={value} />
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

export const UserPoolListing: FC = () => {
  const [showCreateUser, setShowCreateUser] = useState(false)
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const history = useHistory()

  const { data: userPoolData, isLoading } = useQuery(
    [
      QueryKeys.Integrations.getUserPool,
      {
        searchText: searchText,
        limit: PAGE_SIZE,
        offset: (currentPage - 1) * PAGE_SIZE,
        sortBy,
        sortOrder,
      },
    ],
    APIServices.Integrations.getUserPool,
  )

  const { pageData, totalPage, pageInfo } = useMemo(() => {
    return getTableData(userPoolData)
  }, [userPoolData])

  return (
    <NContainer>
      <NRow justify="space-between">
        <NContainer.Title>User Pool</NContainer.Title>
        <NButton type="primary" onClick={() => setShowCreateUser(true)}>
          Create user
        </NButton>
      </NRow>
      <NContainer.Description>{pageInfo.total} users</NContainer.Description>
      <NTableToolbar
        searchConfig={{
          value: searchValue,
          onChange: e => {
            onChangeSearch(e.target.value)
          },
        }}
      />
      <NTable
        isLoading={isLoading}
        columns={COLUMNS}
        data={pageData}
        pageSize={PAGE_SIZE}
        pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
        onClickRow={data => {
          history.push(`${DASHBOARD_ROUTE}/integrations/user-pool/${data.id}`)
        }}
        defaultSortBy={sortBy}
        defaultSortOrder={sortOrder}
        onChangeSort={onChangeSort}
      />
      {showCreateUser && <UserPoolFormModal visible={showCreateUser} setVisible={setShowCreateUser} />}
    </NContainer>
  )
}
