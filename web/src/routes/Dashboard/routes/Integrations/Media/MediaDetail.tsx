import { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NToast } from '../../../../../components/NToast'
import { SettingBoard } from '../../../../../components/SettingBoard'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { IntegrationFieldItem } from '../components/IntegrationFieldItem'
import { MediaEditModal } from './MediaEditModal'

const BoardWrapper = styled.div`
  display: grid;
  padding: ${spacing('xl')} ${spacing('xs')};
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${spacing('xl')};
`

export const MediaDetail: FC = () => {
  const { mediaId } = useParams<{ mediaId: string }>()
  const { data: mediaData } = useQuery([QueryKeys.Media.getMedia, { id: mediaId }], APIServices.Media.getMedia, {
    onError: error => {
      NToast.error({ title: 'Fail to get media detail', subtitle: error.response?.data.message })
    },
  })
  const [showEdit, setShowEdit] = useState(false)

  return (
    <NContainer>
      {showEdit && mediaData?.data && (
        <MediaEditModal visible={showEdit} setVisible={setShowEdit} mediaData={mediaData?.data} />
      )}
      <NDetailHeader
        breadcrumbs={[
          { label: 'Integrations', path: `${DASHBOARD_ROUTE}/integrations` },
          { label: 'Media', path: `${DASHBOARD_ROUTE}/integrations/media` },
          { label: mediaData?.data.displayName || mediaData?.data.name || '' },
        ]}
      />
      <NContainer.Title>{mediaData?.data.displayName || mediaData?.data.name}</NContainer.Title>
      {mediaData?.data.description && <NContainer.Description>{mediaData?.data.description}</NContainer.Description>}
      <SettingBoard
        title="Media detail"
        action={
          <NButton size="small" onClick={() => setShowEdit(true)} type="outline">
            Edit
          </NButton>
        }>
        <BoardWrapper>
          <IntegrationFieldItem title="Name" value={mediaData?.data.name} />
          <IntegrationFieldItem title="Display name" value={mediaData?.data.displayName} />
          <IntegrationFieldItem title="Size" value={mediaData?.data.size} />
          <IntegrationFieldItem title="MIME type" value={mediaData?.data.mimeType} />
          <IntegrationFieldItem title="URL" value={mediaData?.data.url} />
          <IntegrationFieldItem title="Reference key" value={mediaData?.data.referenceKey} />
          <IntegrationFieldItem title="Created at" value={mediaData?.data.createdAt} />
          <IntegrationFieldItem title="Updated at" value={mediaData?.data.updatedAt} />
          <IntegrationFieldItem title="Created by" value={mediaData?.data.createdBy} />
        </BoardWrapper>
      </SettingBoard>
      <SettingBoard title="Action metadata">
        <BoardWrapper>
          <IntegrationFieldItem title="Name" value={mediaData?.data.actionMetadata.name} />
          <IntegrationFieldItem title="Display name" value={mediaData?.data.actionMetadata.displayName} />
        </BoardWrapper>
      </SettingBoard>
    </NContainer>
  )
}
