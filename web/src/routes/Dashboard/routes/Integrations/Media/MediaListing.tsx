import { FC, useMemo } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { MediaResponse } from '../../../../../../../shared/src/api'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableToolbar } from '../../../../../components/NTable/NTableToolbar'
import { NToast } from '../../../../../components/NToast'
import { useTableConfigs } from '../../../../../hooks/useTableConfigs'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { getTableData } from '../../../../../utils/table-utils'

const COLUMNS: NTableColumnType<MediaResponse> = [
  {
    Header: 'Name',
    accessor: 'name',
    defaultCanSort: true,
  },
  {
    Header: 'Display name',
    accessor: 'displayName',
    defaultCanSort: true,
  },
  {
    Header: 'Description',
    accessor: 'description',
  },
  {
    Header: 'MIME type',
    accessor: 'mimeType',
    defaultCanSort: true,
  },
  {
    Header: 'Created at',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
]

const PAGE_SIZE = 10

export const MediaListing: FC = () => {
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const history = useHistory()

  const { data, isLoading } = useQuery(
    [
      QueryKeys.Media.getAllMedia,
      { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE, searchText, sortBy, sortOrder },
    ],
    APIServices.Media.getAllMedia,
    {
      keepPreviousData: true,
      onError: error => {
        NToast.error({ title: 'Fail to get media listing', subtitle: error.response?.data.message })
      },
    },
  )

  const { pageData, pageInfo, totalPage } = useMemo(() => {
    return getTableData(data)
  }, [data])

  return (
    <NContainer>
      <NContainer.Title>Media Listing</NContainer.Title>
      <NContainer.Description>{pageInfo.total} media items</NContainer.Description>
      <NContainer.Content>
        <NTableToolbar
          searchConfig={{
            value: searchValue,
            onChange: e => {
              onChangeSearch(e.target.value)
            },
          }}
        />
        <NTable
          isLoading={isLoading}
          columns={COLUMNS}
          data={pageData}
          defaultSortBy={sortBy}
          defaultSortOrder={sortOrder}
          onChangeSort={onChangeSort}
          pageSize={PAGE_SIZE}
          pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
          onClickRow={value => {
            history.push(`${DASHBOARD_ROUTE}/integrations/media/${value.id}`)
          }}
        />
      </NContainer.Content>
    </NContainer>
  )
}
