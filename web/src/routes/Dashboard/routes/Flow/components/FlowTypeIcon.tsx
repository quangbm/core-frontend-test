import { ReactComponent as Action } from './icons/action.svg'
import { ReactComponent as Data } from './icons/data.svg'
import { ReactComponent as PlatformEvent } from './icons/platform-event.svg'
import { ReactComponent as Screen } from './icons/screen.svg'
import { ReactComponent as Time } from './icons/time.svg'

export const FlowTypeIcon = {
  Action,
  Data,
  Time,
  Screen,
  PlatformEvent,
}
