import { FC, useCallback, useMemo, useState } from 'react'
import { useHistory, useRouteMatch } from 'react-router-dom'
import { FlowResponse } from '../../../../../../../shared/src/api/models'
import { NTableActions } from '../../../../../components/NTable/NTableActions'

type FlowTableActionsProps = {
  value: string
  data: FlowResponse
  onEdit: () => void
}

export const FlowTableActions: FC<FlowTableActionsProps> = ({ onEdit, data }) => {
  const [showDropList, setShowDropList] = useState(false)
  const history = useHistory()
  const { url } = useRouteMatch()

  const handleEdit = useCallback(() => {
    onEdit()
    setShowDropList(false)
  }, [onEdit])

  const handleViewVersions = useCallback(() => {
    history.push(`${url}/${data.name}/templates`)
  }, [data.name, history, url])

  const actions = useMemo(() => {
    if (data.status === 'draft') {
      return [
        { title: 'Edit', onClick: handleEdit },
        { title: 'View versions', onClick: handleViewVersions },
      ]
    }
    return [{ title: 'View versions', onClick: handleViewVersions }]
  }, [data.status, handleEdit, handleViewVersions])

  return <NTableActions showDropList={showDropList} setShowDropList={setShowDropList} options={actions} />
}
