import { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useQuery } from 'react-query'
import styled from 'styled-components/macro'
import { FlowCreateDto, FlowResponse } from '../../../../../../../shared/src/api'
import { spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../components/NModal/NModal'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { useSearchText } from '../../../../../hooks/useSearchText'
import { useValidateString } from '../../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { fieldNameRegex } from '../../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../../utils/utils'

export type FlowDetailForm = FlowCreateDto

const DetailForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type FlowDetailModalProps = {
  visible: boolean
  setVisible: (visible: boolean) => void
  onSubmit: (data: FlowDetailForm) => void
  data?: FlowResponse
  onBack: () => void
  isLoading: boolean
}

export const FlowDetailModal: FC<FlowDetailModalProps> = ({
  visible,
  setVisible,
  data,
  onBack,
  onSubmit,
  isLoading,
}) => {
  const { register, setValue, formState, handleSubmit, getValues, control } = useForm()
  const { t } = useTranslation()
  const { validateFunction } = useValidateString()
  const [{ searchText, searchValue }, onChangeSearch] = useSearchText()

  const { data: credentialOptions, isLoading: isLoadingCredentials } = useQuery(
    [QueryKeys.Integrations.getCredentials, { searchText }],
    APIServices.Integrations.getCredentials,
    {
      select: data => {
        return data.data.data.map(credential => ({ value: credential.name, label: credential.displayName }))
      },
      onError: error => {
        NToast.error({ title: 'Get credentials error!', subtitle: error.response?.data.message })
      },
    },
  )

  const submitForm = (data: FlowDetailForm) => {
    onSubmit(data)
  }

  const onClose = () => {
    setVisible(false)
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header
        onBack={data ? undefined : onBack}
        onClose={onClose}
        title={data ? 'Edit flow' : 'Create new flow'}
      />
      <NModal.Body>
        <DetailForm>
          <NRow>
            <NColumn flex={1}>
              <NTextInput
                required
                defaultValue={data?.displayName || ''}
                {...register('displayName', {
                  validate: validateFunction,
                })}
                name="displayName"
                error={formState.errors.displayName?.message}
                placeholder={t('dashboard.objectListing.createObjectModal.displayNameField')}
                label="Display name"
                onBlur={e => {
                  if (!!!data) {
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }
                }}
              />
            </NColumn>
            <NDivider vertical size="xl" />
            <NColumn flex={1}>
              <NTextInput
                defaultValue={data?.name || ''}
                {...register('name', {
                  validate: !data ? validateFunction : undefined,
                  pattern: {
                    value: fieldNameRegex,
                    message: 'Invalid pattern',
                  },
                })}
                required
                disabled={!!data}
                error={formState.errors.name?.message}
                placeholder={t('dashboard.objectListing.createObjectModal.nameField')}
                label="Name"
              />
            </NColumn>
          </NRow>
          <NDivider size="md" />
          <Controller
            name="credentialName"
            defaultValue={data?.credentialName}
            control={control}
            render={({ field, fieldState }) => {
              return (
                <NSingleSelect
                  fullWidth
                  label="Credential"
                  options={credentialOptions || []}
                  isSearchable
                  searchValue={searchValue}
                  onSearchValueChange={onChangeSearch}
                  value={field.value}
                  error={fieldState.error?.message}
                  onValueChange={field.onChange}
                  isLoading={isLoadingCredentials}
                  allowClear
                  onClear={() => setValue('credentialName', null)}
                />
              )
            }}
          />
          <NDivider size="md" />
          <NRow>
            <NColumn flex={1}>
              <NTextArea
                defaultValue={data?.description || ''}
                {...register('description')}
                placeholder="Flow description"
                label="Description"
              />
            </NColumn>
          </NRow>
        </DetailForm>
      </NModal.Body>
      <NModal.Footer isLoading={isLoading} onFinish={handleSubmit(submitForm)} onCancel={onClose} />
    </NModal>
  )
}
