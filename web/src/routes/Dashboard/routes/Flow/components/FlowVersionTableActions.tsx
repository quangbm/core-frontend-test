import { FC, useMemo, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import { FlowResponse, FlowTemplateResponse } from '../../../../../../../shared/src/api'
import { NTableActions } from '../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { FlowDetailForm, FlowDetailModal } from './FlowDetailModal'
import { FlowTemplateCloneModal } from './FlowTemplateCloneModal'

type Props = {
  version: string
  flowData: FlowTemplateResponse
}

export const FlowVersionTableActions: FC<Props> = ({ version, flowData }) => {
  const [showDropList, setShowDropList] = useState(false)
  const [showCloneModal, setShowCloneModal] = useState(false)
  const [showEditModal, setShowEditModal] = useState(false)
  const { flowName } = useParams<{ flowName: string }>()
  const queryClient = useQueryClient()

  const { mutate: updateFlow, isLoading: isUpdating } = useMutation(APIServices.Flows.updateFlowTemplate, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([QueryKeys.Flows.getFlowsList])
      setShowEditModal(false)
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const onSubmitEdit = (data: FlowDetailForm) => {
    updateFlow({
      flowName,
      version: flowData.version,
      data: {
        displayName: data.displayName,
        description: data.description,
        credentialName: data.credentialName,
      },
    })
  }

  const actions = useMemo(() => {
    if (flowData.status === 'draft') {
      return [
        {
          title: 'Clone',
          onClick: () => {
            setShowDropList(false)
            setShowCloneModal(true)
          },
        },
        {
          title: 'Edit',
          onClick: () => {
            setShowDropList(false)
            setShowEditModal(true)
          },
        },
      ]
    }
    return [
      {
        title: 'Clone',
        onClick: () => {
          setShowDropList(false)
          setShowCloneModal(true)
        },
      },
    ]
  }, [flowData.status])

  return (
    <>
      {showCloneModal && (
        <FlowTemplateCloneModal
          flowName={flowName}
          version={version}
          visible={showCloneModal}
          setVisible={setShowCloneModal}
          defaultValues={{
            displayName: flowData.displayName,
            description: flowData.description,
          }}
        />
      )}
      {showEditModal && (
        <FlowDetailModal
          onBack={() => {}}
          data={{ ...flowData, name: flowName } as FlowResponse}
          visible={showEditModal}
          setVisible={setShowEditModal}
          onSubmit={onSubmitEdit}
          isLoading={isUpdating}
        />
      )}
      <NTableActions showDropList={showDropList} setShowDropList={setShowDropList} options={actions} />
    </>
  )
}
