import { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components/macro'
import { FlowTemplateCloneDto } from '../../../../../../../shared/src/api'
import { spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../components/NDivider'
import { NModal } from '../../../../../components/NModal/NModal'
import { NTextArea } from '../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { typography } from '../../../../../components/NTypography'
import { APIServices, QueryKeys } from '../../../../../services/api'

const FormWrapper = styled.div`
  padding: ${spacing('lg')} ${spacing('xl')};
`

const Caption = styled.div`
  ${typography('caption')}
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  version: string
  flowName: string
  defaultValues?: FlowTemplateCloneDto
}

export const FlowTemplateCloneModal: FC<Props> = ({ visible, setVisible, version, flowName, defaultValues }) => {
  const { register, handleSubmit } = useForm<FlowTemplateCloneDto>({
    defaultValues,
  })
  const queryClient = useQueryClient()
  const history = useHistory()

  const { mutate: cloneTemplate, isLoading } = useMutation(APIServices.Flows.cloneFlowTemplate, {
    onSuccess: res => {
      queryClient.invalidateQueries([QueryKeys.Flows.getFlowTemplates, { flowName }])
      setVisible(false)
      NToast.success({
        title: 'Clone successful',
      })
      history.push(`/flow-builder/${flowName}/templates/${res.data.version}`)
    },
    onError: error => {
      NToast.error({
        title: 'Clone unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const onSubmit = (data: FlowTemplateCloneDto) => {
    cloneTemplate({
      version,
      flowName,
      data,
    })
  }

  return (
    <NModal setVisible={setVisible} visible={visible}>
      <NModal.Header title="Clone template" onClose={() => setVisible(false)} />
      <NModal.Body>
        <FormWrapper>
          <NTextInput label="Display name" {...register('displayName')} />
          <NDivider size="md" />
          <NTextArea label="Description" {...register('description')} />
          <NDivider size="md" />
          <Caption>Clone new template with new information or reuse old information.</Caption>
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer>
        <NButton
          type="ghost"
          size="small"
          onClick={() => {
            setVisible(false)
          }}>
          Close
        </NButton>
        <NButton loading={isLoading} type="primary" size="small" onClick={handleSubmit(onSubmit)}>
          Clone
        </NButton>
      </NModal.Footer>
    </NModal>
  )
}
