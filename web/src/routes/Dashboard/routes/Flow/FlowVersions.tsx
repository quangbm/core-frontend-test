import { useQuery } from 'react-query'
import { useHistory, useParams } from 'react-router-dom'
import { FlowTemplateResponse } from '../../../../../../shared/src/api'
import { classnames } from '../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { NDetailHeader } from '../../../../components/NDetailHeader'
import { NDivider } from '../../../../components/NDivider'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NTable, NTableColumnType } from '../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../components/NTable/NTableStyledContainer'
import { APIServices, QueryKeys } from '../../../../services/api'
import { FlowVersionTableActions } from './components/FlowVersionTableActions'

const COLUMNS: NTableColumnType<FlowTemplateResponse> = [
  {
    Header: 'Version',
    accessor: 'version',
  },
  { Header: 'Display name', accessor: 'displayName' },
  { Header: 'Description', accessor: 'description' },

  { Header: 'Status', accessor: 'status' },
  {
    Header: 'Created At',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
  },
  {
    accessor: 'version',
    Cell: ({ value, row }) => {
      return <FlowVersionTableActions version={value} flowData={row.original} />
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

export const FlowVersions = () => {
  const { flowName } = useParams<{ flowName: string }>()
  const history = useHistory()

  const { data: flowVersions = [], isLoading: isLoadingVersions } = useQuery(
    [QueryKeys.Flows.getFlowTemplates, { flowName }],
    APIServices.Flows.getFlowTemplates,
    { select: resp => resp.data },
  )

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[
          { path: DASHBOARD_ROUTE, label: 'Dashboard' },
          { path: `${DASHBOARD_ROUTE}/flow`, label: 'Flows' },
          { label: flowName },
        ]}
      />
      <NContainer.Title>Flow versions</NContainer.Title>
      <NContainer.Description>{flowVersions.length} versions(s)</NContainer.Description>

      <NContainer.Content>
        <NTable
          columns={COLUMNS}
          data={flowVersions}
          isLoading={isLoadingVersions}
          onClickRow={data => {
            history.push(`/flow-builder/${flowName}/templates/${data.version}`)
          }}
        />

        <NDivider size="md" />
      </NContainer.Content>
      <NDivider size="xl" />
    </NContainer>
  )
}
