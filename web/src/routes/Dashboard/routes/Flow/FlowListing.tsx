import { FC, useMemo, useState } from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useHistory } from 'react-router'
import { Link } from 'react-router-dom'
import { FlowResponse } from '../../../../../../shared/src/api'
import { FlowType } from '../../../../../../shared/src/api/endpoints/flows'
import { classnames } from '../../../../common/classnames'
import { NButton } from '../../../../components/NButton/NButton'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../components/NTable/NTableToolbar'
import { getRequestError, NToast } from '../../../../components/NToast'
import { useTableConfigs } from '../../../../hooks/useTableConfigs'
import { APIServices, QueryKeys } from '../../../../services/api'
import { getTableData } from '../../../../utils/table-utils'
import { FlowDetailForm, FlowDetailModal } from './components/FlowDetailModal'
import { FlowTableActions } from './components/FlowTableActions'
import { FlowTypesModal } from './components/FlowTypesModal'

const PAGE_SIZE = 10

export const FlowListing: FC = () => {
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const [showFlowTypesModal, setShowFlowTypesModal] = useState(false)
  const [selectedFlowType, setSelectedFlowType] = useState<FlowType>('screen')
  const [showFlowDetailModal, setShowFlowDetailModal] = useState(false)
  const queryClient = useQueryClient()
  const [editingFlow, setEditingFlow] = useState<FlowResponse | undefined>()
  const history = useHistory()

  const { data: flowData, isFetching } = useQuery(
    [
      QueryKeys.Flows.getFlowsList,
      { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE, searchText, sortBy, sortOrder },
    ],
    APIServices.Flows.getFlowsList,
    {
      keepPreviousData: true,
      onError: error => {
        NToast.error(getRequestError('Get Flow', error))
      },
    },
  )

  const { mutate: createFlow, isLoading: isCreating } = useMutation(APIServices.Flows.createFlow, {
    onSuccess: async res => {
      await queryClient.invalidateQueries([
        QueryKeys.Flows.getFlowsList,
        { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE },
      ])
      setShowFlowDetailModal(false)
      if (res) {
        console.log({ data: res })
        history.push(`/flow-builder/${res?.data.name}/templates/${res.data.version}`)
      }
    },
    onError: error => {
      NToast.error({ title: 'Create unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const { mutate: updateFlow, isLoading: isUpdating } = useMutation(APIServices.Flows.updateFlowTemplate, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([
        QueryKeys.Flows.getFlowsList,
        { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE },
      ])
      setEditingFlow(undefined)
      setShowFlowDetailModal(false)
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const { pageData, pageInfo, totalPage } = useMemo(() => {
    return getTableData(flowData)
  }, [flowData])

  const onSubmitForm = (data: FlowDetailForm) => {
    if (editingFlow) {
      updateFlow({
        flowName: editingFlow.name,
        version: editingFlow.version,
        data: {
          displayName: data.displayName,
          description: data.description,
          credentialName: data.credentialName,
        },
      })
    } else {
      createFlow({ ...data, type: selectedFlowType })
    }
  }

  const columns = useMemo<NTableColumnType<FlowResponse>>(
    () => [
      {
        Header: 'Label',
        accessor: 'displayName',
        defaultCanSort: true,
        Cell({ value, row }) {
          return (
            <Link
              to={`/flow-builder/${row.original.name}/templates/${row.original.version}`}
              onClick={e => e.stopPropagation()}>
              {value}
            </Link>
          )
        },
      },
      { Header: 'Description', accessor: 'description' },
      { Header: 'Version', accessor: 'version' },
      { Header: 'Status', accessor: 'status', defaultCanSort: true },
      { Header: 'Type', accessor: 'type', defaultCanSort: true },
      { Header: 'Credential', accessor: 'credentialName' },
      {
        Header: 'Created at',
        accessor: 'createdAt',
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = new Date(value)
          return date.toLocaleString('vi')
        },
        defaultCanSort: true,
      },
      {
        Header: 'Last modified',
        accessor: 'updatedAt',
        Cell: ({ value }) => {
          if (!value) {
            return null
          }
          const date = new Date(value)
          return date.toLocaleString('vi')
        },
        defaultCanSort: true,
      },
      {
        accessor: 'name',
        Cell: ({ value, row }) => {
          return (
            <FlowTableActions
              onEdit={() => {
                setShowFlowDetailModal(true)
                setEditingFlow(row.original)
              }}
              value={value}
              data={row.original}
            />
          )
        },
        id: classnames([NTableCollapsedCellClassName]),
      },
    ],
    [],
  )

  return (
    <NContainer>
      <FlowTypesModal
        selectedType={selectedFlowType}
        setSelectedType={setSelectedFlowType}
        visible={showFlowTypesModal}
        setVisible={setShowFlowTypesModal}
        onFinish={() => {
          setShowFlowTypesModal(false)
          setShowFlowDetailModal(true)
        }}
      />
      {showFlowDetailModal && (
        <FlowDetailModal
          data={editingFlow}
          visible={showFlowDetailModal}
          setVisible={setShowFlowDetailModal}
          onBack={() => {
            setShowFlowDetailModal(false)
            setShowFlowTypesModal(true)
          }}
          onSubmit={onSubmitForm}
          isLoading={isCreating || isUpdating}
        />
      )}

      <NRow justify="space-between">
        <NContainer.Title>Flow Listing</NContainer.Title>
        <NButton
          type="primary"
          onClick={() => {
            setShowFlowTypesModal(true)
            setEditingFlow(undefined)
          }}>
          Create Flow
        </NButton>
      </NRow>
      <NContainer.Description>{pageInfo.total} Flows</NContainer.Description>
      <NTableToolbar
        searchConfig={{
          value: searchValue,
          onChange: e => {
            onChangeSearch(e.target.value)
          },
        }}
      />
      <NTable
        columns={columns}
        isLoading={isFetching}
        data={pageData}
        defaultSortBy={sortBy}
        defaultSortOrder={sortOrder}
        onChangeSort={onChangeSort}
        pageSize={PAGE_SIZE}
        pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
        onClickRow={data => {
          history.push(`/flow-builder/${data.name}/templates/${data.version}`)
        }}
      />
    </NContainer>
  )
}
