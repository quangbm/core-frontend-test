import React, { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components/macro'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { color, fontSize, spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { typography } from '../../../../../components/NTypography'
import { SettingBoard } from '../../../../../components/SettingBoard'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { CurrencyModal } from '../components/CurrencyModal'
import { OrgCurrencyRateHistory } from '../components/OrgCurrencyRateHistory'

const BoardWrapper = styled.div`
  display: grid;
  padding: ${spacing('xl')} ${spacing('xs')};
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${spacing('xl')};
`

const FieldWrapper = styled.div`
  padding: ${spacing(14)} ${spacing('md')};
`

const FieldLabel = styled.p`
  color: ${color('Neutral700')};
  font-size: ${fontSize('xs')};
  line-height: ${fontSize('xs')};
  font-weight: bold;
`

const FieldValue = styled.p`
  ${typography('small-bold-text')}
  color: ${color('Neutral400')};
`

const LoadingWrapper = styled.div`
  padding: ${spacing('xl')};
  display: flex;
  justify-content: center;
`

type FieldItemProps = {
  label: string
  value?: string | number
}

const FieldItem: FC<FieldItemProps> = ({ label, value }) => {
  return (
    <FieldWrapper>
      <FieldLabel>{label}</FieldLabel>
      <NDivider size="xs" />
      <FieldValue>{value}</FieldValue>
    </FieldWrapper>
  )
}

export const OrgCurrencyDetail: FC = () => {
  const theme = useTheme()
  const { currencyCode } = useParams<{ currencyCode: string }>()
  const [showUpdateModal, setShowUpdateModal] = useState(false)

  const { data: currency, isLoading: isLoadingCurrency } = useQuery(
    [QueryKeys.Currencies.getCurrency, { currencyCode }],
    APIServices.Currencies.getCurrency,
  )

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[
          { label: 'Organization', path: `${DASHBOARD_ROUTE}/organization` },
          { label: 'Currency', path: `${DASHBOARD_ROUTE}/organization/currency` },
          { label: currency?.data.currencyCode || '' },
        ]}
      />
      <NContainer.Title>{currency?.data.displayName}</NContainer.Title>
      <NRow>
        <NColumn flex={4} style={{ padding: '1px' }}>
          <SettingBoard
            title={currency?.data.displayName}
            action={
              <NButton size="small" onClick={() => setShowUpdateModal(true)}>
                Edit
              </NButton>
            }>
            {isLoadingCurrency ? (
              <LoadingWrapper>
                <NSpinner size={22} strokeWidth={2} color={color('Primary700')({ theme })} />
              </LoadingWrapper>
            ) : (
              <BoardWrapper>
                <FieldItem label="Display name" value={currency?.data.displayName} />
                <FieldItem label="Code" value={currency?.data.currencyCode} />
                <FieldItem label="Amount" value={currency?.data.amount} />
                <FieldItem label="Scale" value={currency?.data.scale} />
                <FieldItem label="Status" value={currency?.data.isActive ? 'Active' : 'Inactive'} />
                <FieldItem label="Created" value={currency?.data.createdAt} />
                <FieldItem label="Modified" value={currency?.data.updatedAt} />
              </BoardWrapper>
            )}
          </SettingBoard>

          {showUpdateModal && currency?.data && (
            <CurrencyModal visible={showUpdateModal} setVisible={setShowUpdateModal} currency={currency?.data} />
          )}
        </NColumn>
        <NColumn flex={1}>
          <OrgCurrencyRateHistory currencyCode={currencyCode} />
        </NColumn>
      </NRow>
    </NContainer>
  )
}
