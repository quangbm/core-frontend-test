import React, { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { OrganizationCurrencyResponse } from '../../../../../../../shared/src/api'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableToolbar } from '../../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../../hooks/useTableConfigs'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { getTableData } from '../../../../../utils/table-utils'
import { CurrencyModal } from '../components/CurrencyModal'

const PAGE_SIZE = 10

const COLUMNS: NTableColumnType<OrganizationCurrencyResponse> = [
  { Header: 'Currency', accessor: 'displayName', defaultCanSort: true },
  { Header: 'Code', accessor: 'currencyCode', defaultCanSort: true },
  { Header: 'Amount', accessor: 'amount' },
  { Header: 'Scale', accessor: 'scale' },
  {
    Header: 'Active',
    accessor: 'isActive',
    defaultCanSort: true,
    Cell: ({ value }) => {
      return value ? <GlobalIcons.Check /> : null
    },
  },

  {
    Header: 'Created at',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
]

export const OrgCurrency: FC = () => {
  const history = useHistory()
  const [showCreateCurrency, setShowCreateCurrency] = useState(false)

  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()

  const { data: currenciesData, isLoading: isLoadingCurrencies } = useQuery(
    [
      QueryKeys.Currencies.getCurrenciesList,
      { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE, searchText, sortBy, sortOrder },
    ],
    APIServices.Currencies.getCurrenciesList,
    { keepPreviousData: true },
  )

  const { pageData, totalPage } = React.useMemo(() => {
    return getTableData(currenciesData)
  }, [currenciesData])

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[{ label: 'Organization', path: `${DASHBOARD_ROUTE}/organization` }, { label: 'Currency' }]}>
        <NButton type="primary" onClick={() => setShowCreateCurrency(true)}>
          Add currency
        </NButton>
      </NDetailHeader>
      <NContainer.Title>Currency</NContainer.Title>
      <NContainer.Description>
        Use this page to define all the currencies used by your organization. Corporate Currency should be set to the
        currency in which your corporate headquarters reports revenue. If you designate a different currency as
        corporate, all conversion rates will be modified to reflect the change.
      </NContainer.Description>
      <NTableToolbar
        searchConfig={{
          value: searchValue,
          onChange: e => {
            onChangeSearch(e.target.value)
          },
        }}
      />
      <NTable
        isLoading={isLoadingCurrencies}
        columns={COLUMNS}
        data={pageData}
        defaultSortBy={sortBy}
        defaultSortOrder={sortOrder}
        onChangeSort={onChangeSort}
        pageSize={PAGE_SIZE}
        pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
        onClickRow={data => history.push(`${DASHBOARD_ROUTE}/organization/currency/${data.currencyCode}`)}
      />
      {showCreateCurrency && <CurrencyModal visible={showCreateCurrency} setVisible={setShowCreateCurrency} />}
    </NContainer>
  )
}
