import { FC, useMemo } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import styled from 'styled-components/macro'
import { CreateOrgCurrencyDto, OrganizationCurrencyResponse } from '../../../../../../../shared/src/api'
import { spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../components/NModal/NModal'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { NSwitch } from '../../../../../components/NSwitch/NSwitch'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { useSearchText } from '../../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../../services/api'

const FormWrapper = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  currency?: OrganizationCurrencyResponse
}

export const CurrencyModal: FC<Props> = ({ visible, setVisible, currency }) => {
  const queryClient = useQueryClient()
  const [{ searchValue: searchCurrencyValue, searchText: searchCurrencyText }, handleSearchCurrency] = useSearchText('')

  const { control, register, handleSubmit, formState } = useForm<CreateOrgCurrencyDto>({
    defaultValues: currency
      ? {
          currencyCode: currency.currencyCode,
          amount: currency.amount,
          scale: currency.scale,
          isActive: currency.isActive,
        }
      : {},
  })

  const { mutate: updateCurrency, isLoading: isUpdating } = useMutation(APIServices.Currencies.updateCurrency, {
    onSuccess: () => {
      queryClient.invalidateQueries([QueryKeys.Currencies.getCurrenciesList])
      queryClient.invalidateQueries([QueryKeys.Currencies.getCurrency, { currencyCode: currency?.currencyCode }])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Update Currency', subtitle: error.response?.data.message })
    },
  })

  const { mutate: createCurrency, isLoading: isCreating } = useMutation(APIServices.Currencies.createCurrency, {
    onSuccess: () => {
      queryClient.invalidateQueries([QueryKeys.Currencies.getCurrenciesList])
      queryClient.invalidateQueries([QueryKeys.Currencies.getCurrency, { currencyCode: currency?.currencyCode }])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Update Currency', subtitle: error.response?.data.message })
    },
  })

  const onSubmit = (data: CreateOrgCurrencyDto) => {
    if (currency) {
      updateCurrency(data)
      return
    }
    createCurrency(data)
  }

  const { data: availableCurrencies = [], isLoading: isLoadingCurrencies } = useQuery(
    [QueryKeys.Currencies.getAvailableCurrencies],
    APIServices.Currencies.getAvailableCurrencies,
    {
      select: res => {
        return res.data.map(currency => ({
          value: currency.code,
          label: currency.displayName,
        }))
      },
    },
  )

  const filteredCurrencies = useMemo(() => {
    return searchCurrencyText
      ? availableCurrencies.filter(
          locale =>
            (locale.label as string).toLocaleLowerCase().includes(searchCurrencyText.toLocaleLowerCase()) ||
            locale.value.toLocaleLowerCase().includes(searchCurrencyText.toLocaleLowerCase()),
        )
      : availableCurrencies
  }, [searchCurrencyText, availableCurrencies])

  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header
        title={currency ? `Update ${currency?.displayName}` : 'Create currency'}
        onClose={() => setVisible(false)}
      />
      <NModal.Body>
        <FormWrapper>
          <Controller
            control={control}
            name="currencyCode"
            render={({ field, fieldState }) => (
              <NSingleSelect
                disabled={!!currency}
                required
                value={field.value}
                options={filteredCurrencies}
                isLoading={isLoadingCurrencies}
                onValueChange={field.onChange}
                error={fieldState.error?.message}
                label="Currency"
                placeholder="Select currency"
                fullWidth
                onSearchValueChange={handleSearchCurrency}
                searchValue={searchCurrencyValue}
                isSearchable
              />
            )}
          />
          <NDivider size="xl" />
          <NRow>
            <NColumn>
              <NTextInput
                required
                label="Amount"
                type="number"
                {...register('amount', {
                  required: { value: true, message: 'Required!' },
                })}
                error={formState.errors.amount?.message}
              />
            </NColumn>
            <NDivider size="xl" vertical />
            <NColumn>
              <NTextInput
                required
                label="Scale"
                type="number"
                {...register('scale', {
                  required: { value: true, message: 'Required!' },
                })}
                error={formState.errors.scale?.message}
              />
            </NColumn>
          </NRow>
          <NDivider size="xl" />
          <Controller
            control={control}
            name="isActive"
            render={({ field }) => <NSwitch title="Is active" checked={field.value} onChange={field.onChange} />}
          />
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer
        onCancel={() => setVisible(false)}
        onFinish={handleSubmit(onSubmit)}
        isLoading={isCreating || isUpdating}
      />
    </NModal>
  )
}
