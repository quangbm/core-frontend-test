import { FC, useState } from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { color, fontSize, spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NToast } from '../../../../../components/NToast'
import { typography } from '../../../../../components/NTypography'
import { SettingBoard } from '../../../../../components/SettingBoard'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { PickListItemsConfig } from './components/PickListItemsConfig'
import { PickListModal } from './components/PickListModal'

const FieldWrapper = styled.div`
  padding: ${spacing(14)} ${spacing('md')};
`

const FieldLabel = styled.p`
  color: ${color('Neutral700')};
  font-size: ${fontSize('xs')};
  line-height: ${fontSize('xs')};
  font-weight: bold;
`

const FieldValue = styled.p`
  ${typography('small-bold-text')}
  color: ${color('Neutral400')};
`

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${spacing('xl')};
`

const BoardWrapper = styled.div`
  padding: ${spacing('xl')} ${spacing('xs')};
`

export const PickListDetail: FC = () => {
  const { pickListName } = useParams<{ pickListName: string }>()
  const [showEdit, setShowEdit] = useState(false)
  const queryClient = useQueryClient()

  const { data: pickList } = useQuery(
    [QueryKeys.PickLists.getPickList, { name: pickListName }],
    APIServices.PickLists.getPickList,
  )

  const { mutate: activatePickList, isLoading: isActivating } = useMutation(APIServices.PickLists.activatePickList, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([QueryKeys.PickLists.getAllPickLists])
      await queryClient.invalidateQueries([QueryKeys.PickLists.getPickList, { name: pickListName }])
    },
    onError: err => {
      NToast.error({
        title: `Failed to activate pick list`,
        subtitle: err.response?.data.message,
      })
    },
  })

  const { mutate: deactivatePickList, isLoading: isDeactivating } = useMutation(
    APIServices.PickLists.deactivatePickList,
    {
      onSuccess: async () => {
        await queryClient.invalidateQueries([QueryKeys.PickLists.getAllPickLists])
        await queryClient.invalidateQueries([QueryKeys.PickLists.getPickList, { name: pickListName }])
      },
      onError: err => {
        NToast.error({
          title: `Failed to deactivate pick list`,
          subtitle: err.response?.data.message,
        })
      },
    },
  )

  return (
    <NContainer>
      {showEdit && pickList?.data && <PickListModal data={pickList.data} visible={showEdit} setVisible={setShowEdit} />}
      <NDetailHeader
        breadcrumbs={[
          { label: 'Organization', path: `${DASHBOARD_ROUTE}/organization` },
          { label: 'Pick lists', path: `${DASHBOARD_ROUTE}/organization/pick-lists` },
          { label: pickList?.data.displayName || '' },
        ]}
      />
      <NContainer.Title>{pickList?.data.displayName}</NContainer.Title>
      <NContainer.Content>
        <SettingBoard
          title="General"
          action={
            <NRow>
              <NButton
                size="small"
                loading={isActivating || isDeactivating}
                disabled={!pickList?.data}
                onClick={() =>
                  pickList?.data.isActive
                    ? deactivatePickList({ name: pickListName })
                    : activatePickList({ name: pickListName })
                }>
                {pickList?.data.isActive ? 'Deactivate' : 'Activate'}
              </NButton>
              <NDivider vertical size="xs" />
              <NButton size="small" disabled={!pickList?.data} onClick={() => setShowEdit(true)}>
                Edit
              </NButton>
            </NRow>
          }>
          <BoardWrapper>
            <Grid>
              <FieldWrapper>
                <FieldLabel>Display name</FieldLabel>
                <NDivider size="xs" />
                <FieldValue>{pickList?.data.displayName}</FieldValue>
              </FieldWrapper>
              <FieldWrapper>
                <FieldLabel>API name</FieldLabel>
                <NDivider size="xs" />
                <FieldValue>{pickList?.data.name}</FieldValue>
              </FieldWrapper>
            </Grid>
            <FieldWrapper>
              <FieldLabel>Description</FieldLabel>
              <NDivider size="xs" />
              <FieldValue>{pickList?.data.description}</FieldValue>
            </FieldWrapper>
          </BoardWrapper>
        </SettingBoard>
        <NDivider size="md" />
        {pickList?.data && <PickListItemsConfig data={pickList.data} />}
        <NDivider size="xl" />
      </NContainer.Content>
    </NContainer>
  )
}
