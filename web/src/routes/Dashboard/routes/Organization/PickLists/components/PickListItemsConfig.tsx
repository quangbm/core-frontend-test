import { FC, useMemo } from 'react'
import { Controller, useFieldArray, useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled, { useTheme } from 'styled-components/macro'
import { MutatePickListItemDto, PickListItemResponse, PickListResponse } from '../../../../../../../../shared/src/api'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../../components/Icons'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NSpinner } from '../../../../../../components/NSpinner/NSpinner'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { NTypography } from '../../../../../../components/NTypography'
import { SettingBoard } from '../../../../../../components/SettingBoard'
import { APIServices, QueryKeys } from '../../../../../../services/api'
import { getNameFromDisplayName } from '../../../../../../utils/utils'

const SettingActionWrapper = styled.div`
  display: flex;
`

const BoardWrapper = styled.div`
  padding: ${spacing('xl')} ${spacing('xl')};
  position: relative;
`

const StyledRow = styled(NRow)`
  margin-bottom: ${spacing('md')};
`

export const Label = styled(NTypography.InputLabel)`
  margin-bottom: ${spacing('xs')};
`

const LoadingMask = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  opacity: 0.8;
  background-color: ${color('white')};
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`

const TrashBtn = styled.div`
  cursor: pointer;
  margin-top: ${spacing('md')};
`

const ActionWrapper = styled.div`
  width: 100px;
  height: 100%;
`

type UpdatePickListItemsForm = {
  items: PickListItemResponse[]
}

type Props = {
  data: PickListResponse
}

export const PickListItemsConfig: FC<Props> = ({ data }) => {
  const { control, handleSubmit, reset, getValues, setValue, formState } = useForm<UpdatePickListItemsForm>({
    defaultValues: {
      items: data.items || [],
    },
  })

  const { fields, append, remove } = useFieldArray({ control, name: 'items' })
  const queryClient = useQueryClient()
  const theme = useTheme()

  const { mutate: updatePickList, isLoading: isUpdating } = useMutation(APIServices.PickLists.updatePickList, {
    onSuccess: res => {
      reset({ items: res.data.items })
      NToast.success({
        title: 'Pick list items updated',
      })
      queryClient.invalidateQueries(QueryKeys.PickLists.getAllPickLists)
      queryClient.invalidateQueries([QueryKeys.PickLists.getPickList, { name: data.name }])
    },
    onError: error => {
      NToast.error({
        title: 'Update pick list unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const existedItemMap: Record<string, PickListItemResponse> = useMemo(() => {
    if (data.items) {
      return data.items.reduce((a, item) => {
        a[item.name] = item
        return a
      }, {} as Record<string, PickListItemResponse>)
    }
    return {}
  }, [data])

  const onSubmit = (formData: UpdatePickListItemsForm) => {
    const itemsData = transformPayload(formData.items, data.items || [])
    itemsData.length > 0 && updatePickList({ name: data.name, items: itemsData })
  }

  return (
    <SettingBoard
      title="Pick list items"
      action={
        <SettingActionWrapper>
          <NButton
            disabled={isUpdating || !formState.isDirty}
            type="default"
            size="small"
            onClick={() => {
              reset({
                items: data.items || [],
              })
            }}>
            Reset
          </NButton>
          <NDivider size="md" />
          <NButton type="primary" disabled={isUpdating} size="small" onClick={handleSubmit(onSubmit)}>
            Save
          </NButton>
        </SettingActionWrapper>
      }>
      <BoardWrapper>
        {isUpdating && (
          <LoadingMask>
            <NSpinner size={30} strokeWidth={2} color={color('Primary900')({ theme })} />
          </LoadingMask>
        )}
        {fields.map((item, fieldIndex) => (
          <StyledRow key={item.id}>
            <NColumn>
              <Controller
                name={`items.${fieldIndex}.displayName`}
                control={control}
                render={({ field, fieldState }) => (
                  <NTextInput
                    value={field.value || ''}
                    onChange={e => {
                      field.onChange(e.target.value)
                    }}
                    onBlur={e => {
                      if (!getValues(`items.${fieldIndex}.name`)) {
                        setValue(`items.${fieldIndex}.name`, getNameFromDisplayName(e.target.value))
                      }
                    }}
                    label={`Display name`}
                    required
                    error={fieldState.error?.message}
                  />
                )}
                rules={{ required: { value: true, message: 'Required!' } }}
              />
            </NColumn>
            <NDivider vertical size="xs" />
            <NColumn>
              <Controller
                name={`items.${fieldIndex}.name`}
                control={control}
                render={({ field, fieldState }) => (
                  <NTextInput
                    value={field.value || ''}
                    onChange={e => field.onChange(e.target.value)}
                    label={`Name`}
                    required
                    disabled={!!existedItemMap[item.name]}
                    error={fieldState.error?.message}
                  />
                )}
                rules={{ required: { value: true, message: 'Required!' } }}
              />
            </NColumn>
            <NDivider vertical size="xs" />
            <ActionWrapper>
              {existedItemMap[item.name] ? (
                <Controller
                  control={control}
                  name={`items.${fieldIndex}.isDeprecated`}
                  render={({ field: { value, onChange } }) => (
                    <div>
                      <Label>Active</Label>
                      <NCheckbox checked={!Boolean(value)} onChange={e => onChange(!e.target.checked)} />
                    </div>
                  )}
                />
              ) : (
                <TrashBtn onClick={() => remove(fieldIndex)}>
                  <GlobalIcons.Trash />
                </TrashBtn>
              )}
            </ActionWrapper>
          </StyledRow>
        ))}
        <NButton icon={<GlobalIcons.Add />} type="primary" onClick={() => append({ displayName: '', name: '' })} />
      </BoardWrapper>
    </SettingBoard>
  )
}

const transformPayload = (newItems: PickListItemResponse[], originalItems: PickListItemResponse[]) => {
  // create hash object
  const original = originalItems.reduce((a, item) => {
    a[item.name] = item
    return a
  }, {} as Record<string, PickListItemResponse>)

  return newItems.reduce((a, newItem) => {
    if (!original[newItem.name]) {
      a.push({ action: 'CREATE', name: newItem.name, displayName: newItem.displayName })
      return a
    }
    if (original[newItem.name]) {
      if (original[newItem.name].displayName !== newItem.displayName) {
        a.push({ action: 'UPDATE', name: newItem.name, displayName: newItem.displayName })
      }
      if (original[newItem.name].isDeprecated && !newItem.isDeprecated) {
        a.push({ action: 'RESTORE', name: newItem.name })
      }
      if (!original[newItem.name].isDeprecated && newItem.isDeprecated) {
        a.push({ action: 'DELETE', name: newItem.name })
      }
    }
    return a
  }, [] as MutatePickListItemDto[])
}
