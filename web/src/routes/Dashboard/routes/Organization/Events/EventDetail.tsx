import { formatDistanceToNow } from 'date-fns'
import { FC, useEffect, useState } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { EventFieldResponse } from '../../../../../../../shared/src/api'
import { classnames } from '../../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { FieldDecoration } from '../../../components/FieldDecoration'
import { EventFieldFormModal } from './components/EventFieldFormModal'
import { EventTableActions } from './components/EventFieldsTableAction'
import { EventFormModal } from './components/EventFormModal'

const DisplayName = styled.div`
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`

const COLUMNS: NTableColumnType<EventFieldResponse> = [
  {
    Header: 'Display name',
    accessor: 'displayName',
    Cell: ({ value, row }) => {
      return (
        <NRow align="center">
          <FieldDecoration type={row.original.dataType.name} />
          <NDivider vertical size="md" />
          <DisplayName>{value}</DisplayName>
        </NRow>
      )
    },
  },
  { Header: 'API name', accessor: 'name' },
  {
    Header: 'Type',
    accessor: 'dataType',
    Cell: ({ value, row }) => {
      const subType = row.original.attributes.subType ? `(${row.original.attributes.subType})` : ''
      return ` ${value.displayName} ${subType}`
    },
  },
  {
    Header: 'Is system',
    accessor: 'isSystemDefault',
    Cell: ({ value }) => {
      return value ? <GlobalIcons.Check /> : null
    },
  },
  {
    Header: 'Created at',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
  },
  {
    accessor: 'id',
    Cell: ({ row }) => {
      return row.original.isSystemDefault ? null : <EventTableActions eventFieldName={row.original.name} />
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

export const EventDetail: FC = () => {
  const { eventName } = useParams<{ eventName: string }>()
  const [showEditModal, setShowEditModal] = useState(false)
  const [showFieldModal, setShowFieldModal] = useState(false)
  const [selectedField, setSelectedField] = useState<EventFieldResponse>()

  const { data: eventData } = useQuery([QueryKeys.Events.getEvent, { name: eventName }], APIServices.Events.getEvent)

  const { data: eventFields, isLoading: isLoadingFields } = useQuery(
    [QueryKeys.Events.getEventFields, { eventName }],
    APIServices.Events.getEventFields,
  )

  useEffect(() => {
    if (!showFieldModal) {
      setSelectedField(undefined)
    }
  }, [showFieldModal])

  return (
    <NContainer>
      {showFieldModal && eventData?.data && (
        <EventFieldFormModal
          visible={showFieldModal}
          setVisible={setShowFieldModal}
          eventData={eventData.data}
          selectedField={selectedField}
        />
      )}
      {showEditModal && eventData?.data && (
        <EventFormModal eventData={eventData.data} visible={showEditModal} setVisible={setShowEditModal} />
      )}
      <NDetailHeader
        label={
          eventData?.data.updatedAt &&
          `Last updated ${formatDistanceToNow(new Date(eventData?.data.updatedAt), { addSuffix: true })}`
        }
        breadcrumbs={[
          { label: 'Organization', path: `${DASHBOARD_ROUTE}/organization` },
          { label: 'Events', path: `${DASHBOARD_ROUTE}/organization/events` },
          { label: eventData?.data.displayName || '' },
        ]}>
        {!eventData?.data.isSystemDefault && (
          <NButton icon={<GlobalIcons.Edit />} onClick={() => setShowEditModal(true)}>
            Edit
          </NButton>
        )}
        <NDivider size="sm" vertical />
        <NButton type="primary" onClick={() => setShowFieldModal(true)}>
          Add Field
        </NButton>
      </NDetailHeader>
      <NContainer.Title>{eventData?.data.displayName}</NContainer.Title>
      {eventData?.data.description && <NContainer.Description>{eventData?.data.description}</NContainer.Description>}
      <NTable
        isLoading={isLoadingFields}
        columns={COLUMNS}
        data={eventFields?.data || []}
        onClickRow={data => {
          if (!data.isSystemDefault) {
            setSelectedField(data)
            setShowFieldModal(true)
          }
        }}
      />
    </NContainer>
  )
}
