import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { EventResponse, PostAction } from '../../../../../../../../shared/src/api'
import { NTableActions } from '../../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../../services/api'
import { EventFormModal } from './EventFormModal'

type Props = {
  eventData: EventResponse
}

export const EventTableActions: FC<Props> = ({ eventData }) => {
  const [showDropList, setShowDropList] = useState(false)
  const [showEditModal, setShowEditModal] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: postEvent, isLoading: isPosting } = useMutation(APIServices.Events.postEvent, {
    onSuccess: () => {
      NToast.success({ title: 'Event deleted' })
      queryClient.invalidateQueries(QueryKeys.Events.getEventList)
      queryClient.invalidateQueries([QueryKeys.Events.getEvent, { name: eventData.name }])
      setShowDropList(false)
    },
    onError: error => {
      NToast.error({
        title: 'Delete unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  return (
    <>
      {showEditModal && <EventFormModal eventData={eventData} visible={showEditModal} setVisible={setShowEditModal} />}
      <NTableActions
        options={[
          {
            title: 'Edit',
            onClick: () => {
              setShowEditModal(true)
              setShowDropList(false)
            },
          },
          {
            title: 'Delete',
            onClick: () => {
              postEvent({ name: eventData.name, action: PostAction.Delete })
            },
            isLoading: isPosting,
          },
        ]}
        showDropList={showDropList}
        setShowDropList={setShowDropList}
      />
    </>
  )
}
