import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import { PostAction } from '../../../../../../../../shared/src/api'
import { NTableActions } from '../../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../../services/api'

type Props = {
  eventFieldName: string
}

export const EventTableActions: FC<Props> = ({ eventFieldName }) => {
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()
  const { eventName } = useParams<{ eventName: string }>()

  const { mutate: postEvent, isLoading: isPosting } = useMutation(APIServices.Events.postEventField, {
    onSuccess: () => {
      NToast.success({ title: 'Event deleted' })
      queryClient.invalidateQueries(QueryKeys.Events.getEventFields)
      setShowDropList(false)
    },
    onError: error => {
      NToast.error({
        title: 'Delete unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  return (
    <>
      <NTableActions
        options={[
          {
            title: 'Delete',
            onClick: () => {
              postEvent({
                eventName,
                body: {
                  name: eventFieldName,
                  action: PostAction.Delete,
                },
              })
            },
            isLoading: isPosting,
          },
        ]}
        showDropList={showDropList}
        setShowDropList={setShowDropList}
      />
    </>
  )
}
