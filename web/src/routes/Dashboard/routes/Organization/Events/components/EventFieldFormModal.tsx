import { omit } from 'lodash'
import { FC, useEffect, useReducer, useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components/macro'
import { LayoutComponent, PostAction } from '../../../../../../../../shared/src/api'
import { DataTypeResponse, EventFieldResponse, EventResponse } from '../../../../../../../../shared/src/api/models'
import { color, spacing } from '../../../../../../components/GlobalStyle'
import { NButton } from '../../../../../../components/NButton/NButton'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../components/NDivider'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../../../components/NPerfectScrollbar'
import { NSpinner } from '../../../../../../components/NSpinner/NSpinner'
import { NToast } from '../../../../../../components/NToast'
import { NTypography, typography } from '../../../../../../components/NTypography'
import { APIServices, QueryKeys } from '../../../../../../services/api'
import { getValueFromDate } from '../../../../../../utils/utils'
import { DataTypeForm } from '../../../../components/DatatypeForm/DataTypeForm'
import { FieldDecoration } from '../../../../components/FieldDecoration'

//FIXME: Refactor properly T.T, also add back actions =.=

const DataTypeWrapper = styled('div')`
  display: grid;
  grid-template-columns: repeat(2, minmax(0, 1fr));
  grid-column-gap: ${spacing('xl')};
  padding: ${spacing('xl')};
`

const DataTypeItem = styled('div')`
  background: transparent;
  display: flex;
  padding: ${spacing('sm')};
  cursor: pointer;
  border-radius: 4px;
  &:hover {
    background: ${color('Primary100')};
  }
  &:hover .FieldDecoration {
    background: ${color('Primary700')};
  }
  transition: background 0.2s ease-in-out;
`

const ModalBodyWrapper = styled.div`
  padding: ${spacing('xl')};
`

const DataTypeName = styled('div')`
  color: ${color('Neutral900')};
  font-size: 14px;
  margin-bottom: ${spacing('xxs')};
`

const LayoutWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid ${color('Neutral200')};
  margin-bottom: ${spacing('md')};

  &:last-child {
    margin-bottom: 0;
  }
`

const LoadingContainer = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  padding: ${spacing('xl')};
`

const LayoutName = styled.div`
  ${typography('h400')};
  padding: ${spacing('md')};
`

const ComponentWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  border-top: 1px solid ${color('Neutral200')};
  padding: ${spacing('md')};
`

/////////////////
enum ActionType {
  Init = 'Init',
  Update = 'Update',
  Clean = 'Clean',
}

type ReducerAction =
  | {
      type: ActionType.Init
      payload: {
        updateLayouts: LayoutRecord
      }
    }
  | {
      type: ActionType.Update
      payload: {
        layoutId: string
        componentId: string
        checked: boolean
      }
    }
  | {
      type: ActionType.Clean
    }

function reducer(state: LayoutRecord, action: ReducerAction) {
  switch (action.type) {
    case ActionType.Init:
      return action.payload.updateLayouts
    case ActionType.Update:
      const { layoutId, componentId, checked } = action.payload
      state[layoutId].components[componentId] = checked
      const layout = state[layoutId]
      const { components } = layout
      return {
        ...state,
        [layoutId]: {
          ...layout,
          components: {
            ...components,
            [componentId]: checked,
          },
        },
      }
    case ActionType.Clean:
      return {}
    default:
      throw new Error()
  }
}
/////////////////
type LayoutRecord = Record<string, { name: string; components: Record<string, boolean> }>

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  eventData: EventResponse
  selectedField?: EventFieldResponse
}

export const EventFieldFormModal: FC<Props> = ({ visible, setVisible, eventData, selectedField }) => {
  const formMethods = useForm({
    shouldUnregister: false,
  })
  const { eventName } = useParams<{ eventName: string }>()
  const [selectedDataType, setSelectedDataType] = useState<DataTypeResponse | undefined>(selectedField?.dataType)
  const [updateLayouts, dispatch] = useReducer(reducer, {} as LayoutRecord)
  const theme = useTheme()
  const [formStep, setFormStep] = useState<'datatype' | 'field' | 'layout' | undefined>(
    selectedField ? 'field' : 'datatype',
  )
  const queryClient = useQueryClient()

  const watchName = selectedField?.name || formMethods.watch('name')
  const watchIsRequired = selectedField?.isRequired || (formMethods.watch('isRequired') as boolean)

  const { data: layouts, isLoading: isLoadingLayout } = useQuery(
    [QueryKeys.ObjectRecordPages.getObjectRecordPages, { objName: eventName, fieldName: watchName! }],
    APIServices.ObjectRecordPages.getObjectRecordPages,
    {
      enabled: !!watchName && visible,
    },
  )
  const { data: dataTypes = [], isLoading: isDataTypeLoading } = useQuery(
    [QueryKeys.Builder.getObjectDataTypes, { objName: eventName }],
    APIServices.Builder.getObjectDataTypes,
    {
      select(response) {
        return response.data
      },
    },
  )

  const { mutate: postEventField, isLoading: isPostingField } = useMutation(APIServices.Events.postEventField, {
    onSuccess: async () => {
      const invalidateQueries = [
        queryClient.invalidateQueries(QueryKeys.Events.getEventFields),
        queryClient.invalidateQueries(QueryKeys.ObjectRecordPages.getObjectRecordPages),
        queryClient.invalidateQueries(QueryKeys.Builder.getLayoutScript),
      ]
      await Promise.all(invalidateQueries)
      formMethods.unregister()
      setVisible(false)
    },
    onError: error => {
      NToast.error({
        title: `${selectedField ? 'Update' : 'Create'} field unsuccessful`,
        subtitle: error.response?.data.message,
      })
    },
  })

  // get default value when fetching layouts or open, close modal
  useEffect(() => {
    if (!visible || !layouts) {
      dispatch({ type: ActionType.Clean })
      return
    }

    const transformedUpdateLayouts = layouts.data.reduce((prevLayout, cur) => {
      const { id, name, components } = cur
      const layoutComponents = components.reduce((prev, cur) => {
        return { ...prev, [cur.id]: (cur.isPrimary && watchIsRequired) || cur.fieldExists }
      }, {} as Record<string, boolean>)

      return { ...prevLayout, [id]: { name, components: layoutComponents } }
    }, {} as LayoutRecord)

    dispatch({ type: ActionType.Init, payload: { updateLayouts: transformedUpdateLayouts } })
  }, [visible, layouts, watchIsRequired])

  const handleSubmitField = async () => {
    const isValid = await formMethods.trigger()
    if (isValid) {
      setFormStep('layout')
    }
  }

  const handleSubmitLayout = () => {
    const layoutKeys = Object.keys(updateLayouts)
    const parsedUpdateLayouts: LayoutComponent[] = layoutKeys.reduce((prevLayoutUpdates, key) => {
      const componentKeys = Object.keys(updateLayouts[key].components)
      const componentIds = componentKeys.reduce((prev, cur) => {
        const comp = updateLayouts[key].components[cur]
        if (comp) {
          return [...prev, cur]
        }

        return prev
      }, [] as string[])

      if (componentIds.length === 0) {
        return prevLayoutUpdates
      }

      return [
        ...prevLayoutUpdates,
        {
          layoutId: key,
          componentIds,
        },
      ]
    }, [] as LayoutComponent[])

    formMethods.handleSubmit(data => {
      onSubmit(data, parsedUpdateLayouts)
    })()
  }

  const onSubmit = (data: any, updateLayouts: LayoutComponent[]) => {
    let defaultValue = data.defaultValue || undefined
    // transform default value if it is datetime
    if (selectedDataType?.name === 'dateTime' && defaultValue) {
      const date = new Date(defaultValue)
      defaultValue = getValueFromDate(date, data.attributes.subType)
    }

    const submitData = {
      name: selectedField ? undefined : data.name,
      displayName: data.displayName,
      typeName: selectedField ? undefined : selectedDataType?.name,
      isRequired: data.isRequired,
      isExternalId: data.isExternalId,
      pickListId: data.pickListId ? Number(data.pickListId) : undefined,
      attributes: {
        defaultValue,
        ...data.attributes,
      },
      value: data.value,
    }

    postEventField({
      body: {
        data: selectedField ? omit(submitData, ['name', 'typeName']) : submitData,
        action: selectedField ? PostAction.Update : PostAction.Create,
        name: selectedField && selectedField.name,
        updateLayouts,
      },
      eventName,
    })
  }

  return (
    <NModal
      size="large"
      visible={visible}
      setVisible={(visible: boolean) => {
        setVisible(visible)
        setFormStep(undefined)
      }}>
      <NModal.Header title={`${selectedField ? 'Update' : 'Create'} field`} />
      <NModal.Body>
        <FormProvider {...formMethods}>
          <NPerfectScrollbar style={{ maxHeight: '70vh' }}>
            {(() => {
              if (isDataTypeLoading) {
                return (
                  <div style={{ minHeight: 200, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    <NSpinner />
                  </div>
                )
              }
              if (formStep === 'datatype') {
                return (
                  <DataTypeWrapper>
                    {dataTypes.map(dataType => (
                      <DataTypeItem
                        key={dataType.name}
                        onClick={() => {
                          //@ts-ignore
                          setSelectedDataType(dataType)
                          setFormStep('field')
                        }}>
                        <FieldDecoration type={dataType.name} />
                        <NDivider vertical size="sm" />
                        <div style={{ flex: 1 }}>
                          <DataTypeName>{dataType.displayName}</DataTypeName>
                          <NTypography.InputCaption>{dataType.description}</NTypography.InputCaption>
                        </div>
                      </DataTypeItem>
                    ))}
                  </DataTypeWrapper>
                )
              }
              if (formStep === 'field' && selectedDataType) {
                return (
                  <ModalBodyWrapper>
                    <DataTypeForm
                      object={eventData}
                      //FIXME: Refactor Datatype form =.=
                      //@ts-ignore
                      fieldData={selectedField}
                      //@ts-ignore
                      formMethods={formMethods}
                      dataType={selectedDataType}
                    />
                  </ModalBodyWrapper>
                )
              }
              if (formStep === 'layout') {
                return isLoadingLayout ? (
                  <LoadingContainer>
                    <NSpinner size={30} strokeWidth={3} color={color('Primary700')({ theme })} />
                  </LoadingContainer>
                ) : (
                  <ModalBodyWrapper>
                    {layouts?.data.map(layout => {
                      return (
                        <LayoutWrapper key={`layout_${layout.id}`}>
                          <LayoutName>{layout.name}</LayoutName>
                          <ComponentWrapper>
                            {layout.components.map(component => {
                              const checked = !!updateLayouts[layout.id]?.components[component.id]

                              return (
                                <NCheckbox
                                  key={`component_${component.id}`}
                                  disabled={component.isPrimary && watchIsRequired}
                                  title={`${component.label}${component.isPrimary ? ' (primary)' : ''}`}
                                  checked={checked}
                                  onChange={e => {
                                    dispatch({
                                      type: ActionType.Update,
                                      payload: {
                                        layoutId: layout.id,
                                        componentId: component.id,
                                        checked: e.target.checked,
                                      },
                                    })
                                  }}
                                />
                              )
                            })}
                          </ComponentWrapper>
                        </LayoutWrapper>
                      )
                    })}
                  </ModalBodyWrapper>
                )
              }
            })()}
          </NPerfectScrollbar>
        </FormProvider>
      </NModal.Body>
      <NModal.Footer>
        <NButton
          size="small"
          onClick={() => {
            formMethods.reset({})
            setVisible(false)
            setFormStep(undefined)
          }}>
          Cancel
        </NButton>
        {formStep === 'field' && (
          <NButton size="small" type="primary" onClick={handleSubmitField}>
            Next
          </NButton>
        )}
        {formStep === 'layout' && (
          <NButton size="small" type="primary" onClick={handleSubmitLayout} loading={isPostingField}>
            Finish
          </NButton>
        )}
      </NModal.Footer>
    </NModal>
  )
}
