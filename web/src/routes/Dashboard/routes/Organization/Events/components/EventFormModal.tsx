import { FC } from 'react'
import { useForm } from 'react-hook-form'
import { useMutation, useQueryClient } from 'react-query'
import styled from 'styled-components/macro'
import { EventResponse, PostAction } from '../../../../../../../../shared/src/api'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NToast } from '../../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../../services/api'
import { ObjectForm, ObjectFormType } from '../../../../components/ObjectForm'

const FormWrapper = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  eventData?: EventResponse
}

export const EventFormModal: FC<Props> = ({ visible, setVisible, eventData }) => {
  const queryClient = useQueryClient()
  const formMethods = useForm({
    defaultValues: {
      name: eventData?.name || '',
      recordName: eventData?.recordName.label || '',
      description: eventData?.description || '',
      displayName: eventData?.displayName || '',
      owd: eventData?.owd || 'Private',
    },
  })

  const { mutate: postEvent, isLoading } = useMutation(APIServices.Events.postEvent, {
    onSuccess: async () => {
      NToast.success({ title: `Event ${eventData ? 'updated' : 'created'}!` })
      await queryClient.invalidateQueries(QueryKeys.Events.getEventList)
      await queryClient.invalidateQueries([QueryKeys.Events.getEvent, { name: eventData?.name }])
      setVisible(false)
    },
    onError: error => {
      NToast.error({
        title: `${eventData ? 'Update' : 'Create'} event unsuccessful`,
        subtitle: error.response?.data.message,
      })
    },
  })

  const onSubmit = (data: ObjectFormType) => {
    postEvent({
      data: {
        displayName: data.displayName,
        recordName: {
          label: data.recordName,
          type: 'text',
        },
        name: eventData ? undefined : data.name,
        description: data.description,
        owd: data.owd,
      },
      action: eventData ? PostAction.Update : PostAction.Create,
      name: eventData?.name,
    })
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header title={`${eventData ? 'Edit' : 'Create'} Event`} onClose={() => setVisible(false)} />
      <NModal.Body>
        <FormWrapper>
          <ObjectForm objectData={eventData} formMethods={formMethods} />
        </FormWrapper>
      </NModal.Body>
      <NModal.Footer
        isLoading={isLoading}
        onCancel={() => setVisible(false)}
        onFinish={formMethods.handleSubmit(onSubmit)}
      />
    </NModal>
  )
}
