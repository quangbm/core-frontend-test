import React, { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { EventResponse } from '../../../../../../../shared/src/api'
import { classnames } from '../../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { NTableToolbar } from '../../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../../hooks/useTableConfigs'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { getTableData } from '../../../../../utils/table-utils'
import { EventFormModal } from './components/EventFormModal'
import { EventTableActions } from './components/EventTableActions'

const PAGE_SIZE = 10

const COLUMNS: NTableColumnType<EventResponse> = [
  { Header: 'Display name', accessor: 'displayName', defaultCanSort: true },
  { Header: 'Name', accessor: 'name', defaultCanSort: true },
  { Header: 'Description', accessor: 'description' },
  {
    Header: 'Is system',
    accessor: 'isSystemDefault',
    defaultCanSort: true,
    Cell: ({ value }) => {
      return value ? <GlobalIcons.Check /> : null
    },
  },
  {
    Header: 'Created at',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    accessor: 'name',
    Cell: ({ row }) => {
      return row.original.isSystemDefault ? null : <EventTableActions eventData={row.original} />
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

export const EventListing: FC = () => {
  const history = useHistory()
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const [showModal, setShowModal] = useState(false)

  const { data: events, isLoading } = useQuery(
    [
      QueryKeys.Events.getEventList,
      { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE, searchText, sortBy, sortOrder },
    ],
    APIServices.Events.getEventList,
    { keepPreviousData: true },
  )

  const { pageData, totalPage } = React.useMemo(() => {
    return getTableData(events)
  }, [events])

  return (
    <NContainer>
      {showModal && <EventFormModal visible={showModal} setVisible={setShowModal} />}
      <NDetailHeader
        breadcrumbs={[{ label: 'Organization', path: `${DASHBOARD_ROUTE}/organization` }, { label: 'Platform Events' }]}
      />
      <NRow justify="space-between">
        <NContainer.Title>Platform Events</NContainer.Title>
        <NButton type="primary" onClick={() => setShowModal(true)}>
          Create Event
        </NButton>
      </NRow>
      <NTableToolbar
        searchConfig={{
          value: searchValue,
          onChange: e => {
            onChangeSearch(e.target.value)
          },
        }}
      />
      <NTable
        isLoading={isLoading}
        columns={COLUMNS}
        data={pageData}
        defaultSortBy={sortBy}
        defaultSortOrder={sortOrder}
        onChangeSort={onChangeSort}
        pageSize={PAGE_SIZE}
        pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
        onClickRow={data => history.push(`${DASHBOARD_ROUTE}/organization/events/${data.name}`)}
      />
    </NContainer>
  )
}
