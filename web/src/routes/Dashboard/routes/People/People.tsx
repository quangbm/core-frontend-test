import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { NLayout, NSideBar } from '../../../../components/NLayout'
import { NMenu } from '../../../../components/NMenu/NMenu'
import { ProfileListing } from './Profiles/ProfileListing'
import { Roles } from './Roles/Roles'
import { SharingRuleDetail } from './SharingRules/SharingRuleDetail'
import { SharingRulesListing } from './SharingRules/SharingRulesListing'
import { UserDetail } from './Users/UserDetail'
import { Users } from './Users/Users'

type PeopleProps = {}

export function People({}: PeopleProps) {
  return (
    <NLayout isRow>
      <NSideBar title="People">
        <NMenu type="primary">
          <NMenu.Item to={`${DASHBOARD_ROUTE}/people/users`}>Users</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/people/profiles`}>Profiles</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/people/roles`}>Roles</NMenu.Item>
          <NMenu.Item to={`${DASHBOARD_ROUTE}/people/sharing-rules`}>Sharing Rules</NMenu.Item>
        </NMenu>
      </NSideBar>
      <Switch>
        <Route path={`${DASHBOARD_ROUTE}/people/profiles`} component={ProfileListing} exact />
        <Route path={`${DASHBOARD_ROUTE}/people/roles`} component={Roles} />
        <Route path={`${DASHBOARD_ROUTE}/people/users/:userId`} component={UserDetail} />
        <Route path={`${DASHBOARD_ROUTE}/people/users`} component={Users} />
        <Route path={`${DASHBOARD_ROUTE}/people/sharing-rules/:sharingRuleName`} component={SharingRuleDetail} />
        <Route path={`${DASHBOARD_ROUTE}/people/sharing-rules`} component={SharingRulesListing} />
        <Redirect to={`${DASHBOARD_ROUTE}/people/users`} />
      </Switch>
    </NLayout>
  )
}
