import * as React from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { ProfileFeatureDto } from '../../../../../../../shared/src/api/models'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NToast } from '../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { PermissionTable } from './components/PermissionTable'

const SaveBtn = styled(NButton)`
  min-width: 80px;
`

type TableDatum = {
  name: string
  displayName: string
  read: boolean
  create: boolean
  delete: boolean
  update: boolean
}

export const ProfileFeatures: React.FC = () => {
  const { profileId } = useParams<{ profileId: string }>()

  const [tableData, setTableData] = React.useState<TableDatum[]>([])
  const queryClient = useQueryClient()

  const { isLoading: isLoadingFeatures, data: profileFeatures } = useQuery(
    [QueryKeys.Profiles.getProfileFeatures, { id: profileId }],
    APIServices.Profiles.getProfileFeatures,
  )

  const { data: profileData } = useQuery(
    [QueryKeys.Profiles.getProfile, { id: profileId }],
    APIServices.Profiles.getProfile,
  )

  React.useEffect(() => {
    if (profileFeatures?.data) {
      const tmpTableData = []
      for (const feature in profileFeatures?.data) {
        tmpTableData.push({
          name: feature,
          ...profileFeatures?.data[feature],
        })
      }
      setTableData(tmpTableData)
    }
  }, [profileFeatures])

  const { mutate: updateProfileFeatures, isLoading: isUpdating } = useMutation(
    APIServices.Profiles.updateProfileFeatures,
    {
      onSuccess() {
        queryClient.invalidateQueries([QueryKeys.Profiles.getProfileFeatures, { id: profileId }])
        NToast.success({ title: 'Update successful' })
      },
      onError: error => {
        NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
      },
    },
  )

  const submit = () => {
    updateProfileFeatures({
      id: profileId,
      items: tableData.map(item => {
        return {
          featureName: item.name as ProfileFeatureDto['featureName'],
          access: {
            read: item.read,
            create: item.create,
            update: item.update,
            delete: item.delete,
          },
        }
      }),
    })
  }

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[
          { path: DASHBOARD_ROUTE, label: 'Dashboard' },
          { path: `${DASHBOARD_ROUTE}/people`, label: 'People' },
          { path: `${DASHBOARD_ROUTE}/people/profiles`, label: 'Profiles' },
          { label: profileData?.data.name || '' },
        ]}>
        <SaveBtn loading={isUpdating} onClick={submit} type="primary">
          Save
        </SaveBtn>
      </NDetailHeader>
      <NContainer.Title>{profileData?.data.name}</NContainer.Title>
      <NContainer.Content>
        <PermissionTable
          tableData={tableData}
          setTableData={setTableData}
          isLoading={isLoadingFeatures}
          header="Permission"
        />
        <NDivider size="md" />
      </NContainer.Content>
      <NDivider size="xl" />
    </NContainer>
  )
}
