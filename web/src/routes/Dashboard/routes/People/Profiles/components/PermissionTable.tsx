import * as React from 'react'
import { FC } from 'react'
import { Row } from 'react-table'
import styled from 'styled-components/macro'
import { color } from '../../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../../components/Icons'
import { NCheckbox } from '../../../../../../components/NCheckbox/NCheckbox'
import { NDivider } from '../../../../../../components/NDivider'
import { NTable } from '../../../../../../components/NTable/NTable'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { typography } from '../../../../../../components/NTypography'

const HeaderLabel = styled.div`
  ${typography('overline')};
  color: ${color('Neutral400')};
  display: flex;
  align-items: center;
  cursor: pointer;
`

const Label = styled('label')`
  ${typography('small-bold-text')};
  color: ${color('Neutral900')};
  display: flex;
  align-items: center;
  cursor: pointer;
`

type TableDatum = {
  name: string
  displayName: string
  read: boolean
  create: boolean
  delete: boolean
  update: boolean
}

type Props = {
  tableData?: TableDatum[]
  setTableData?: (data: React.SetStateAction<TableDatum[]>) => void
  isLoading?: boolean
  disabled?: boolean
  header: string
}

export const PermissionTable: FC<Props> = ({ tableData = [], setTableData, isLoading, disabled = false, header }) => {
  const [searchTerm, setSearchTerm] = React.useState('')
  const [headerData, setHeaderData] = React.useState({
    read: false,
    create: false,
    update: false,
    delete: false,
  })

  const handleCheckboxChange = (row: Row<TableDatum>, key: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    setTableData &&
      !disabled &&
      setTableData(prev => {
        return [...prev.slice(0, row.index), { ...row.original, [key]: e.target.checked }, ...prev.slice(row.index + 1)]
      })
  }

  const handleCheckboxChangeAll = (key: string) => (e: React.ChangeEvent<HTMLInputElement>) => {
    setHeaderData(prev => ({ ...prev, [key]: e.target.checked }))
    setTableData && setTableData(prev => prev.map(i => ({ ...i, [key]: e.target.checked })))
  }

  const count = tableData.reduce(
    (a, i) => {
      return {
        read: i.read ? a.read + 1 : a.read,
        create: i.create ? a.create + 1 : a.create,
        delete: i.delete ? a.delete + 1 : a.delete,
        update: i.update ? a.update + 1 : a.update,
      }
    },
    { read: 0, create: 0, delete: 0, update: 0 },
  )

  const data = React.useMemo(() => {
    if (searchTerm) {
      return tableData.filter(i => i.name.includes(searchTerm.toLowerCase()))
    }
    return tableData
  }, [searchTerm, tableData])

  return (
    <>
      <NTextInput
        className="input"
        left={<GlobalIcons.Search />}
        placeholder="Search"
        value={searchTerm}
        onChange={e => setSearchTerm(e.target.value)}
      />
      <NDivider size="xl" />
      <NTable
        isLoading={isLoading}
        data={data}
        columns={[
          { Header: header, accessor: 'displayName' },
          {
            Header: () => {
              return (
                <HeaderLabel>
                  <NCheckbox
                    checked={headerData.read}
                    indeterminate={count.read > 0 && count.read < tableData.length}
                    onChange={handleCheckboxChangeAll('read')}
                    disabled={disabled}
                  />
                  <NDivider vertical size="md" />
                  Read
                </HeaderLabel>
              )
            },
            accessor: 'read',
            Cell: ({ value, row }: { value: boolean; row: Row<TableDatum> }) => {
              return (
                <Label>
                  <NCheckbox checked={value} onChange={handleCheckboxChange(row, 'read')} />
                  <NDivider vertical size="md" />
                  Read
                </Label>
              )
            },
          },
          {
            Header: () => {
              return (
                <HeaderLabel>
                  <NCheckbox
                    checked={headerData.create}
                    indeterminate={count.create > 0 && count.create < tableData.length}
                    onChange={handleCheckboxChangeAll('create')}
                    disabled={disabled}
                  />
                  <NDivider vertical size="md" />
                  Create
                </HeaderLabel>
              )
            },
            accessor: 'create',
            Cell: ({ value, row }: { value: boolean; row: Row<TableDatum> }) => {
              return (
                <Label>
                  <NCheckbox checked={value} onChange={handleCheckboxChange(row, 'create')} />
                  <NDivider vertical size="md" />
                  Create
                </Label>
              )
            },
          },
          {
            Header: () => {
              return (
                <HeaderLabel>
                  <NCheckbox
                    checked={headerData.update}
                    disabled={disabled}
                    indeterminate={count.update > 0 && count.update < tableData.length}
                    onChange={handleCheckboxChangeAll('update')}
                  />
                  <NDivider vertical size="md" />
                  Update
                </HeaderLabel>
              )
            },
            accessor: 'update',
            Cell: ({ value, row }: { value: boolean; row: Row<TableDatum> }) => {
              return (
                <Label>
                  <NCheckbox checked={value} onChange={handleCheckboxChange(row, 'update')} />
                  <NDivider vertical size="md" />
                  Update
                </Label>
              )
            },
          },
          {
            Header: () => {
              return (
                <HeaderLabel>
                  <NCheckbox
                    disabled={disabled}
                    checked={headerData.delete}
                    indeterminate={count.delete > 0 && count.delete < tableData.length}
                    onChange={handleCheckboxChangeAll('delete')}
                  />
                  <NDivider vertical size="md" />
                  Delete
                </HeaderLabel>
              )
            },
            accessor: 'delete',
            Cell: ({ value, row }: { value: boolean; row: Row<TableDatum> }) => {
              return (
                <Label>
                  <NCheckbox checked={value} onChange={handleCheckboxChange(row, 'delete')} />
                  <NDivider vertical size="md" />
                  Delete
                </Label>
              )
            },
          },
        ]}
      />
    </>
  )
}
