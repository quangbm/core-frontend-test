import * as React from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NToast } from '../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { PermissionTable } from './components/PermissionTable'

const SaveBtn = styled(NButton)`
  min-width: 80px;
`

type TableDatum = {
  name: string
  displayName: string
  read: boolean
  create: boolean
  delete: boolean
  update: boolean
}

type ProfileDetailsProps = {
  children?: React.ReactNode
}

export function ProfileObjects({}: ProfileDetailsProps) {
  const { profileId } = useParams<{ profileId: string }>()

  const [tableData, setTableData] = React.useState<TableDatum[]>([])
  const queryClient = useQueryClient()

  const { isLoading: isLoadingProfilesObjects, data: profileObjectsData } = useQuery(
    [QueryKeys.Profiles.getProfileObjects, { id: profileId }],
    APIServices.Profiles.getProfileObjects,
  )

  const { data: profileData } = useQuery(
    [QueryKeys.Profiles.getProfile, { id: profileId }],
    APIServices.Profiles.getProfile,
  )

  React.useEffect(() => {
    // useEffect of onSuccess becuz onSuccess won't be called when get data from cache
    if (profileObjectsData?.data) {
      const tmpTableData = []
      for (const objName in profileObjectsData?.data) {
        tmpTableData.push({
          name: objName,
          ...profileObjectsData?.data[objName],
          displayName: `${profileObjectsData?.data[objName].displayName} (${objName})`,
        })
      }
      setTableData(tmpTableData)
    }
  }, [profileObjectsData])

  const { mutate: updateProfileObjects, isLoading: isUpdateProfilesObjects } = useMutation(
    APIServices.Profiles.updateProfileObjects,
    {
      onSuccess() {
        queryClient.invalidateQueries([QueryKeys.Profiles.getProfileObjects, { id: profileId }])
        NToast.success({ title: 'Update successful' })
      },
      onError: error => {
        NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
      },
    },
  )

  const submit = () => {
    updateProfileObjects({
      id: profileId,
      items: tableData.map(item => {
        return {
          objName: item.name,
          access: {
            read: item.read,
            create: item.create,
            update: item.update,
            delete: item.delete,
          },
        }
      }),
    })
  }

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[
          { path: DASHBOARD_ROUTE, label: 'Dashboard' },
          { path: `${DASHBOARD_ROUTE}/people`, label: 'People' },
          { path: `${DASHBOARD_ROUTE}/people/profiles`, label: 'Profiles' },
          { label: profileData?.data.name || '' },
        ]}>
        <SaveBtn loading={isUpdateProfilesObjects} onClick={submit} type="primary">
          Save
        </SaveBtn>
      </NDetailHeader>
      <NContainer.Title>{profileData?.data.name}</NContainer.Title>
      <NContainer.Content>
        <PermissionTable
          tableData={tableData}
          setTableData={setTableData}
          isLoading={isLoadingProfilesObjects}
          header="Object"
        />
        <NDivider size="md" />
      </NContainer.Content>
      <NDivider size="xl" />
    </NContainer>
  )
}
