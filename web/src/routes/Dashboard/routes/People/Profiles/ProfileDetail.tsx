import { FC } from 'react'
import { useQuery } from 'react-query'
import { Redirect, Route, Switch, useHistory, useParams, useRouteMatch } from 'react-router-dom'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { NLayout, NSideBar } from '../../../../../components/NLayout'
import { NMenu } from '../../../../../components/NMenu/NMenu'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { SidebarHeader } from '../../../components/SidebarHeader'
import { ProfileFeatures } from './ProfileFeatures'
import { ProfileObjects } from './ProfileObjects'

export const ProfileDetail: FC = () => {
  const history = useHistory()
  const { profileId } = useParams<{ profileId: string }>()

  const { url, path } = useRouteMatch()
  const { data: profileData } = useQuery(
    [QueryKeys.Profiles.getProfile, { id: profileId }],
    APIServices.Profiles.getProfile,
  )

  return (
    <NLayout isRow>
      <NSideBar>
        <SidebarHeader
          onBack={() => history.push(`${DASHBOARD_ROUTE}/people/profile`)}
          backTitle="Back to Profiles"
          title={profileData?.data.name || ''}
        />
        <NMenu type="primary">
          <NMenu.Item to={`${url}/objects`}>Objects</NMenu.Item>
          <NMenu.Item to={`${url}/features`}>Features</NMenu.Item>
        </NMenu>
      </NSideBar>
      <Switch>
        <Route path={`${path}/objects`}>
          <ProfileObjects />
        </Route>
        <Route path={`${path}/features`}>
          <ProfileFeatures />
        </Route>
        <Redirect to={`${url}/objects`} />
      </Switch>
    </NLayout>
  )
}
