import { FC, useEffect, useState } from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import styled, { css } from 'styled-components/macro'
import { RoleResponse } from '../../../../../../../shared/src/api'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../components/NDivider'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NModal } from '../../../../../components/NModal/NModal'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { NToast } from '../../../../../components/NToast'
import { typography } from '../../../../../components/NTypography'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { NoPermission } from '../../../../NoPermission/NoPermission'
import { RoleFormModal } from './RoleFormModal'
import { RolesProvider, RoleTreeNodeType, useRoles } from './RolesContext'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 6px;
  border: 1px solid ${color('Neutral400')};
  padding: ${spacing('sm')};
`

export const RoleListing: FC = () => {
  const [showRoleModal, setShowRoleModal] = useState(false)
  const [selectedRole, setSelectedRole] = useState<RoleResponse>()

  // remove data from modal
  useEffect(() => {
    if (!showRoleModal) {
      setSelectedRole(undefined)
    }
  }, [showRoleModal])

  const {
    data: { rootNode, roleObject } = { rootNode: undefined, roleObject: {} as Record<string, RoleTreeNodeType> },
    isLoading,
    error,
  } = useQuery([QueryKeys.Roles.getRoles], APIServices.Roles.getRoles, {
    select: data => {
      const result = data.data.reduce((prev, cur) => {
        return { ...prev, [cur.id]: { ...cur, children: [] } as RoleTreeNodeType }
      }, {} as Record<string, RoleTreeNodeType>)

      data.data.map(role => {
        const parent = role.reportsToRole
        if (!parent) {
          return null
        }

        const parentObj = result[parent.id]
        parentObj.children = [...parentObj.children, role.id]
        return null
      })

      const root = data.data.find(role => !role.reportsToRole)

      return {
        roleObject: result,
        rootNode: root ? result[root.id] : undefined,
      }
    },
  })

  return (
    <NContainer>
      {error?.response?.data.statusCode === 403 ? (
        <NoPermission />
      ) : (
        <>
          <NRow justify="space-between">
            <NContainer.Title>Roles</NContainer.Title>
            <NButton onClick={() => setShowRoleModal(true)} type="primary">
              New Role
            </NButton>
          </NRow>
          <Wrapper>
            {isLoading && <NSpinner />}
            {rootNode && (
              <RolesProvider roleObject={roleObject}>
                <RoleTreeNode
                  node={rootNode}
                  onEdit={(editNode: RoleTreeNodeType) => {
                    setSelectedRole(editNode)
                    setShowRoleModal(true)
                  }}
                />
              </RolesProvider>
            )}
          </Wrapper>
          {showRoleModal && (
            <RoleFormModal visible={showRoleModal} setVisible={setShowRoleModal} roleData={selectedRole} />
          )}
        </>
      )}
    </NContainer>
  )
}

const RoleTreeNodeWrapper = styled.div`
  display: flex;
  flex-direction: column;

  &:not(:last-child) {
    margin-bottom: ${spacing('sm')};
  }
`

const NodeWrapper = styled.div<{ clickable: boolean }>`
  display: flex;
  flex-direction: row;
  align-items: center;
  border-radius: 6px;
  ${props => {
    if (!props.clickable) {
      return ''
    }
    return css`
      cursor: pointer;
      &:hover {
        background-color: ${color('Neutral200')};
      }
    `
  }}
`

const IconWrapper = styled.div<{ showChildren: boolean }>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 40px;
  height: 40px;
  transform: rotate(${props => (props.showChildren ? '90deg' : '0deg')});
`

const RootNameText = styled.div`
  margin-right: ${spacing('md')};
  text-decoration: underline;
  ${typography('ui-text')}
`

const ChildrenWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  margin-top: ${spacing('sm')};
  margin-left: 40px;
`

const VerticalLine = styled.div`
  position: absolute;
  height: 100%;
  width: 1px;
  background-color: ${color('Neutral900')};
  left: -20px;
`

const NodeDot = styled.div`
  position: absolute;
  width: 20px;
  height: 1px;
  background-color: ${color('Neutral900')};
  left: -20px;
`

const ModalBody = styled(NModal.Body)`
  padding: ${spacing('xl')};
`

type RoleTreeNodeProps = {
  node: RoleTreeNodeType
  onEdit: (editNode: RoleTreeNodeType) => void
}
const RoleTreeNode: FC<RoleTreeNodeProps> = ({ node, onEdit }) => {
  const [showChildren, setShowChildren] = useState(false)
  const [showDeleteConfirm, setShowDeleteConfirm] = useState(false)
  const { roleObject } = useRoles()
  const queryClient = useQueryClient()

  const { mutate: deleteRole, isLoading: isDeleting } = useMutation(APIServices.Roles.deleteRole, {
    onSuccess() {
      queryClient.invalidateQueries([QueryKeys.Roles.getRoles])
      setShowDeleteConfirm(false)
    },
    onError: error => {
      NToast.error({ title: 'Delete unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const hasChildren = !!node.children.length
  return (
    <RoleTreeNodeWrapper>
      <NodeWrapper
        clickable={hasChildren}
        onClick={e => {
          if (!hasChildren) {
            return
          }
          e.stopPropagation()
          setShowChildren(prev => !prev)
        }}>
        {node.reportsToRole && <NodeDot />}
        <IconWrapper showChildren={showChildren}>{hasChildren && <GlobalIcons.MoreRight />}</IconWrapper>
        <RootNameText>{node.displayName}</RootNameText>
        <NButton
          type="outline"
          size="small"
          onClick={e => {
            e.stopPropagation()
            onEdit(node)
          }}>
          Edit
        </NButton>
        <NDivider vertical size="md" />
        {node.reportsToRole && (
          <NButton
            type="red"
            size="small"
            onClick={e => {
              e.stopPropagation()
              setShowDeleteConfirm(true)
            }}>
            Delete
          </NButton>
        )}
      </NodeWrapper>
      {showChildren && (
        <ChildrenWrapper>
          <VerticalLine />
          {node.children.map(childId => {
            const child = roleObject[childId]
            return <RoleTreeNode key={child.id} node={child} onEdit={onEdit} />
          })}
        </ChildrenWrapper>
      )}

      <NModal visible={showDeleteConfirm} setVisible={setShowDeleteConfirm}>
        <NModal.Header title="Delete confirmation" />
        {hasChildren ? (
          <ModalBody>
            Cannot delete '{node.displayName}' role {'\n'}Delete all children first
          </ModalBody>
        ) : (
          <ModalBody>Are you sure you want to delete the '{node.displayName}' role</ModalBody>
        )}
        <NModal.Footer
          isLoading={isDeleting}
          onCancel={() => {
            setShowDeleteConfirm(false)
          }}
          onFinish={() => {
            deleteRole(node.name)
          }}>
          {hasChildren && (
            <NButton
              onClick={() => {
                setShowDeleteConfirm(false)
              }}>
              Close
            </NButton>
          )}
        </NModal.Footer>
      </NModal>
    </RoleTreeNodeWrapper>
  )
}
