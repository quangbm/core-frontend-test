import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { RoleListing } from './RoleListing'

type RolesProps = {}

export function Roles({}: RolesProps) {
  return (
    <NContainer>
      <Switch>
        <Route path={`${DASHBOARD_ROUTE}/people/roles`} exact>
          <RoleListing />
        </Route>
      </Switch>
    </NContainer>
  )
}
