import { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import styled from 'styled-components/macro'
import { CreateRoleDto, RoleResponse } from '../../../../../../../shared/src/api'
import { spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../components/NModal/NModal'
import { SelectOption } from '../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { useValidateString } from '../../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { getNameFromDisplayName } from '../../../../../utils/utils'

const RoleForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  roleData?: RoleResponse
}

export const RoleFormModal: FC<Props> = ({ visible, setVisible, roleData }) => {
  const { control, handleSubmit, register, formState, setValue, getValues } = useForm()
  const { validateFunction } = useValidateString()

  const queryClient = useQueryClient()

  const { mutate: createRole, isLoading: isCreating } = useMutation(APIServices.Roles.createRole, {
    onSuccess() {
      queryClient.invalidateQueries([QueryKeys.Roles.getRoles])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Create unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const { mutate: updateRole, isLoading: isUpdating } = useMutation(APIServices.Roles.updateRole, {
    onSuccess() {
      queryClient.invalidateQueries([QueryKeys.Roles.getRoles])
      setVisible(false)
    },
    onError: error => {
      NToast.error({ title: 'Update unsuccessful', subtitle: error.response?.data.message })
    },
  })

  const { data: roleOptions = [] as SelectOption[], isLoading } = useQuery(
    [QueryKeys.Roles.getRoles],
    APIServices.Roles.getRoles,
    {
      select: data => {
        return data.data
          .filter(role => role.id !== roleData?.id || '')
          .map(role => {
            return {
              label: role.displayName,
              value: role.id,
            } as SelectOption
          })
      },
    },
  )

  const onSubmit = (data: CreateRoleDto) => {
    const { name: _, ...restData } = data
    roleData ? updateRole({ roleName: roleData.name, ...restData }) : createRole(data)
  }

  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header title={`${roleData ? 'Edit' : 'Create'} Role`} onClose={() => setVisible(false)} />
      <NModal.Body>
        <RoleForm>
          <NRow>
            <NColumn>
              <NTextInput
                required
                defaultValue={roleData?.displayName || ''}
                {...register('displayName', {
                  validate: validateFunction,
                })}
                error={formState.errors.displayName?.message}
                label="Display name"
                onBlur={e => {
                  if (!!!roleData) {
                    const formatName = getNameFromDisplayName(e.target.value)
                    !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                  }
                }}
              />
            </NColumn>
            <NDivider vertical size="md" />
            <NColumn>
              <NTextInput
                required
                defaultValue={roleData?.name || ''}
                {...register('name', {
                  validate: roleData ? undefined : validateFunction,
                })}
                error={formState.errors.name?.message}
                disabled={!!roleData}
                label="Name"
              />
            </NColumn>
          </NRow>
          {!(roleData && !roleData.reportsToRole) && (
            <>
              <NDivider size="md" />
              <Controller
                name="reportsTo"
                control={control}
                defaultValue={roleData ? roleData.reportsToRole.id : undefined}
                rules={{ validate: validateFunction }}
                render={({ field: { value, onChange } }) => {
                  return (
                    <NSingleSelect
                      fullWidth
                      required
                      isLoading={isLoading}
                      options={roleOptions}
                      value={value}
                      onValueChange={onChange}
                      label="Reports to"
                      error={formState.errors.reportsTo?.message}
                    />
                  )
                }}
              />
            </>
          )}
        </RoleForm>
      </NModal.Body>
      <NModal.Footer
        isLoading={isCreating || isUpdating}
        onCancel={() => setVisible(false)}
        onFinish={handleSubmit(onSubmit)}
      />
    </NModal>
  )
}
