import React, { createContext, FC } from 'react'
import { RoleResponse } from '../../../../../../../shared/src/api'

export type RoleTreeNodeType = RoleResponse & {
  children: string[]
}

export type RolesContextType = {
  roleObject: Record<string, RoleTreeNodeType>
}

export const RolesContext = createContext<RolesContextType | string>('useRoles should be used inside RolesProvider')

export type RolesContextProps = {
  roleObject: Record<string, RoleTreeNodeType>
}

export const RolesProvider: FC<RolesContextProps> = ({ roleObject, children }) => {
  const value = {
    roleObject,
  }
  return <RolesContext.Provider value={value}>{children}</RolesContext.Provider>
}

export const useRoles = () => {
  const context = React.useContext(RolesContext)
  if (typeof context === 'string') {
    throw new Error(context)
  }
  return context
}
