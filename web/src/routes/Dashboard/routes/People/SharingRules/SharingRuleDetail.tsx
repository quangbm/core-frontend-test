import { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { OwnerAttributesResponse } from '../../../../../../../shared/src/api'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { NDivider } from '../../../../../components/NDivider'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { SettingBoard } from '../../../../../components/SettingBoard'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { IntegrationFieldItem } from '../../Integrations/components/IntegrationFieldItem'
import { SharingRuleFormModal } from './components/SharingRuleFormModal'

const BoardWrapper = styled.div`
  display: grid;
  padding: ${spacing('xl')} ${spacing('xs')};
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: ${spacing('xl')};
`

export const SharingRuleDetail: FC = () => {
  const { sharingRuleName } = useParams<{ sharingRuleName: string }>()
  const [showEdit, setShowEdit] = useState(false)

  const { data: ruleData } = useQuery(
    [QueryKeys.SharingRules.getSharingRule, { name: sharingRuleName }],
    APIServices.SharingRules.getSharingRule,
  )

  return (
    <NContainer>
      {showEdit && ruleData?.data && (
        <SharingRuleFormModal visible={showEdit} setVisible={setShowEdit} data={ruleData.data} />
      )}

      <NDetailHeader
        breadcrumbs={[
          { label: 'People', path: `${DASHBOARD_ROUTE}/people` },
          { label: 'Sharing Rules', path: `${DASHBOARD_ROUTE}/people/sharing-rules` },
          { label: ruleData?.data.displayName || '' },
        ]}
      />
      <NRow justify="space-between">
        <NContainer.Title>{ruleData?.data.displayName}</NContainer.Title>
        <NButton onClick={() => setShowEdit(true)} type="outline">
          Edit
        </NButton>
      </NRow>
      {ruleData?.data.description && <NContainer.Description>{ruleData?.data.description}</NContainer.Description>}
      <SettingBoard title="Sharing rule detail">
        <BoardWrapper>
          <IntegrationFieldItem title="Name" value={ruleData?.data.name} />
          <IntegrationFieldItem title="Display name" value={ruleData?.data.displayName} />
          <IntegrationFieldItem title="Object" value={ruleData?.data.object.displayName} />
          <IntegrationFieldItem title="Access Type" value={ruleData?.data.accessType} />
          <IntegrationFieldItem title="Created at" value={ruleData?.data.createdAt} />
          <IntegrationFieldItem title="Updated at" value={ruleData?.data.updatedAt} />
        </BoardWrapper>
      </SettingBoard>
      <NDivider size="md" />
      {ruleData?.data.type === 'record-owner' && (
        <SettingBoard title="Record owner attributes">
          <BoardWrapper>
            <IntegrationFieldItem
              title="Share from role"
              value={(ruleData.data.attributes as OwnerAttributesResponse).shareFromRole.displayName}
            />
            <IntegrationFieldItem
              title="Share from type"
              value={(ruleData.data.attributes as OwnerAttributesResponse).shareToType}
            />
            <IntegrationFieldItem
              title="Share to role"
              value={(ruleData.data.attributes as OwnerAttributesResponse).shareToRole.displayName}
            />
            <IntegrationFieldItem
              title="Share to role"
              value={(ruleData.data.attributes as OwnerAttributesResponse).shareToType}
            />
          </BoardWrapper>
        </SettingBoard>
      )}
      {ruleData?.data.type === 'criteria' && <SettingBoard title="Criteria attributes"></SettingBoard>}
      <NDivider size="xl" />
    </NContainer>
  )
}
