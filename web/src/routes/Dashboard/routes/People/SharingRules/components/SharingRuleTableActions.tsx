import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { NTableActions } from '../../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../../services/api'

type Props = {
  name: string
}

export const SharingRuleTableActions: FC<Props> = ({ name }) => {
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: deleteSharingRule, isLoading: isDeleting } = useMutation(APIServices.SharingRules.deleteSharingRule, {
    onSuccess() {
      queryClient.invalidateQueries([QueryKeys.SharingRules.getSharingRuleList])
      setShowDropList(false)
      NToast.success({
        title: 'Sharing rule deleted',
      })
    },
    onError: error => {
      NToast.error({ title: 'Delete unsuccessful', subtitle: error.response?.data.message })
    },
  })

  return (
    <NTableActions
      options={[
        {
          title: 'Delete',
          onClick: () => deleteSharingRule(name),
          isLoading: isDeleting,
        },
      ]}
      showDropList={showDropList}
      setShowDropList={setShowDropList}
    />
  )
}
