import { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import styled from 'styled-components/macro'
import {
  CreateSharingRuleDto,
  OwnerAttributesResponse,
  SharingRuleResponse,
} from '../../../../../../../../shared/src/api'
import { spacing } from '../../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../../components/NDivider'
import { NColumn, NRow } from '../../../../../../components/NGrid/NGrid'
import { NModal } from '../../../../../../components/NModal/NModal'
import { NPerfectScrollbar } from '../../../../../../components/NPerfectScrollbar'
import { SelectOption } from '../../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../../components/NSelect/NSingleSelect'
import { NTextArea } from '../../../../../../components/NTextArea/NTextArea'
import { NTextInput } from '../../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../../components/NToast'
import { useSearchText } from '../../../../../../hooks/useSearchText'
import { useValidateString } from '../../../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../../../services/api'
import { fieldNameRegex } from '../../../../../../utils/regex'
import { getNameFromDisplayName } from '../../../../../../utils/utils'

const ModalForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

const ACCESS_TYPE_OPTIONS = [{ value: 'read-write' }, { value: 'read-only' }]

const TYPE_OPTIONS = [
  {
    value: 'record-owner',
  },
  {
    value: 'criteria',
  },
]

const SHARE_TYPE_OPTIONS = [{ value: 'role' }, { value: 'role-and-subordinates' }]

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
  data?: SharingRuleResponse
}

export const SharingRuleFormModal: FC<Props> = ({ visible, setVisible, data }) => {
  const { register, control, formState, getValues, setValue, watch, handleSubmit } = useForm<CreateSharingRuleDto>({
    defaultValues: data
      ? {
          name: data.name,
          displayName: data.displayName,
          description: data.description,
          type: data.type,
          accessType: data.accessType,
          objectName: data.object.name,
          attributes:
            data.type === 'record-owner'
              ? {
                  shareFromRoleName: (data.attributes as OwnerAttributesResponse).shareFromRole.name,
                  shareFromType: (data.attributes as OwnerAttributesResponse).shareFromType,
                  shareToRoleName: (data.attributes as OwnerAttributesResponse).shareToRole.name,
                  shareToType: (data.attributes as OwnerAttributesResponse).shareToType,
                }
              : {},
        }
      : {
          displayName: '',
          name: '',
          description: '',
        },
  })
  const { validateFunction } = useValidateString()
  const [{ searchText, searchValue }, onChangeSearch] = useSearchText()
  const watchType = watch('type', data?.type)
  const queryClient = useQueryClient()

  const { data: objectOptions, isFetching: isLoadingObjects } = useQuery(
    [QueryKeys.Metadata.getObjects, { searchText }],
    APIServices.Metadata.getObjects,
    {
      select: res => {
        return res.data.data.map(
          object =>
            ({
              label: object.displayName,
              value: object.name,
            } as SelectOption),
        )
      },
    },
  )

  const { data: roleOptions, isFetching: isLoadingRoles } = useQuery(
    [QueryKeys.Roles.getRoles],
    APIServices.Roles.getRoles,
    {
      select: data => {
        return data.data.map(
          role =>
            ({
              label: role.displayName || role.name,
              value: role.name,
            } as SelectOption),
        )
      },
    },
  )

  const { mutate: createSharingRule, isLoading: isCreating } = useMutation(APIServices.SharingRules.createSharingRule, {
    onSuccess: async () => {
      NToast.success({
        title: 'Sharing rule created',
      })
      await queryClient.invalidateQueries([QueryKeys.SharingRules.getSharingRuleList])
      onClose()
    },
    onError: err => {
      NToast.error({
        title: `Failed to create sharing rule`,
        subtitle: err.response?.data.message,
      })
    },
  })

  const { mutate: updateSharingRule, isLoading: isUpdating } = useMutation(APIServices.SharingRules.updateSharingRule, {
    onSuccess: async () => {
      NToast.success({
        title: 'Sharing rule updated',
      })
      await queryClient.invalidateQueries([QueryKeys.SharingRules.getSharingRuleList])
      await queryClient.invalidateQueries([QueryKeys.SharingRules.getSharingRule, { name: data?.name }])

      onClose()
    },
    onError: err => {
      NToast.error({
        title: `Failed to update sharing rule`,
        subtitle: err.response?.data.message,
      })
    },
  })

  const onClose = () => {
    setVisible(false)
  }

  const onSubmit = (formData: CreateSharingRuleDto) => {
    console.log({ formData })
    if (data) {
      updateSharingRule(formData)
      return
    }
    createSharingRule(formData)
  }

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header title={`${data ? 'Update' : 'Create'} Sharing Rule`} onClose={onClose} />
      <NModal.Body>
        <NPerfectScrollbar>
          <ModalForm>
            <NRow>
              <NColumn>
                <NTextInput
                  required
                  {...register('displayName', {
                    validate: validateFunction,
                  })}
                  name="displayName"
                  error={formState.errors.displayName?.message}
                  placeholder={'Display name'}
                  label="Display name"
                  onBlur={e => {
                    if (!!!data) {
                      const formatName = getNameFromDisplayName(e.target.value)
                      !getValues('name') && setValue('name', formatName, { shouldDirty: true, shouldValidate: true })
                    }
                  }}
                />
              </NColumn>
              <NDivider vertical size="md" />
              <NColumn>
                <NTextInput
                  defaultValue={data?.name || ''}
                  {...register('name', {
                    validate: !data ? validateFunction : undefined,
                    pattern: {
                      value: fieldNameRegex,
                      message: 'Invalid pattern',
                    },
                  })}
                  required
                  disabled={!!data}
                  error={formState.errors.name?.message}
                  placeholder="Name"
                  label="Name"
                />
              </NColumn>
            </NRow>
            <NDivider size="md" />
            <NTextArea {...register('description')} label="Description" placeholder="Enter description" rows={3} />
            <NDivider size="md" />
            <NRow>
              <NColumn>
                <Controller
                  control={control}
                  rules={{ required: { value: true, message: 'Required!' } }}
                  name="objectName"
                  render={({ field }) => (
                    <NSingleSelect
                      fullWidth
                      required
                      isSearchable
                      searchValue={searchValue}
                      onSearchValueChange={onChangeSearch}
                      options={objectOptions || []}
                      isLoading={isLoadingObjects}
                      placeholder="Select object"
                      label="Object"
                      value={field.value}
                      onValueChange={value => {
                        field.onChange(value)
                      }}
                      error={formState.errors.objectName?.message}
                    />
                  )}
                />
              </NColumn>
              <NDivider vertical size="md" />
              <NColumn>
                <Controller
                  rules={{ required: { value: true, message: 'Required!' } }}
                  control={control}
                  name="accessType"
                  render={({ field }) => (
                    <NSingleSelect
                      fullWidth
                      required
                      options={ACCESS_TYPE_OPTIONS}
                      placeholder="Select access type"
                      label="Access type"
                      value={field.value}
                      onValueChange={value => {
                        field.onChange(value)
                      }}
                      error={formState.errors.accessType?.message}
                    />
                  )}
                />
              </NColumn>
            </NRow>
            <NDivider size="md" />
            <NRow>
              <NColumn flex={0.5}>
                <Controller
                  rules={{ required: { value: true, message: 'Required!' } }}
                  control={control}
                  name="type"
                  render={({ field }) => (
                    <NSingleSelect
                      fullWidth
                      required
                      options={TYPE_OPTIONS}
                      placeholder="Select type"
                      label="Type"
                      value={field.value}
                      onValueChange={value => {
                        setValue('attributes', {})
                        field.onChange(value)
                      }}
                      error={formState.errors.type?.message}
                    />
                  )}
                />
              </NColumn>
            </NRow>
            <NDivider size="md" />
            {watchType === 'record-owner' && (
              <>
                <NRow>
                  <NColumn>
                    <Controller
                      control={control}
                      rules={{ required: { value: true, message: 'Required!' } }}
                      name="attributes.shareFromRoleName"
                      render={({ field }) => (
                        <NSingleSelect
                          fullWidth
                          required
                          options={roleOptions || []}
                          isLoading={isLoadingRoles}
                          placeholder="Select role"
                          label="Share from role"
                          value={field.value}
                          onValueChange={value => {
                            field.onChange(value)
                          }}
                          //@ts-ignore
                          error={formState.errors.attributes?.shareFromRoleName?.message}
                        />
                      )}
                    />
                  </NColumn>
                  <NDivider vertical size="md" />
                  <NColumn>
                    <Controller
                      control={control}
                      name="attributes.shareFromType"
                      rules={{ required: { value: true, message: 'Required!' } }}
                      render={({ field }) => (
                        <NSingleSelect
                          fullWidth
                          required
                          placeholder="Select type"
                          options={SHARE_TYPE_OPTIONS}
                          isLoading={isLoadingRoles}
                          label="Share from type"
                          value={field.value}
                          onValueChange={value => {
                            field.onChange(value)
                          }}
                          //@ts-ignore
                          error={formState.errors.attributes?.shareFromType?.message}
                        />
                      )}
                    />
                  </NColumn>
                </NRow>
                <NDivider size="md" />
                <NRow>
                  <NColumn>
                    <Controller
                      control={control}
                      name="attributes.shareToRoleName"
                      rules={{ required: { value: true, message: 'Required!' } }}
                      render={({ field }) => (
                        <NSingleSelect
                          fullWidth
                          required
                          options={roleOptions || []}
                          isLoading={isLoadingRoles}
                          placeholder="Select role"
                          label="Share to role"
                          value={field.value}
                          onValueChange={value => {
                            field.onChange(value)
                          }}
                          //@ts-ignore
                          error={formState.errors.attributes?.shareToRoleName?.message}
                        />
                      )}
                    />
                  </NColumn>
                  <NDivider vertical size="md" />
                  <NColumn>
                    <Controller
                      control={control}
                      name="attributes.shareToType"
                      rules={{ required: { value: true, message: 'Required!' } }}
                      render={({ field }) => (
                        <NSingleSelect
                          fullWidth
                          required
                          placeholder="Select type"
                          options={SHARE_TYPE_OPTIONS}
                          isLoading={isLoadingRoles}
                          label="Share to type"
                          value={field.value}
                          onValueChange={value => {
                            field.onChange(value)
                          }}
                          //@ts-ignore
                          error={formState.errors.attributes?.shareToType?.message}
                        />
                      )}
                    />
                  </NColumn>
                </NRow>
              </>
            )}
            {watchType === 'criteria' && <div>Criteria type is not supported at the moment.</div>}
          </ModalForm>
        </NPerfectScrollbar>
      </NModal.Body>
      <NModal.Footer isLoading={isCreating || isUpdating} onFinish={handleSubmit(onSubmit)} onCancel={onClose} />
    </NModal>
  )
}
