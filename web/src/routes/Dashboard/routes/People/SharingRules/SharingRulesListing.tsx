import { FC, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { SharingRuleResponse } from '../../../../../../../shared/src/api'
import { classnames } from '../../../../../common/classnames'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../components/NDivider'
import { NRow } from '../../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { NTable, NTableColumnType } from '../../../../../components/NTable/NTable'
import { NTableCollapsedCellClassName } from '../../../../../components/NTable/NTableStyledContainer'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { SharingRuleFormModal } from './components/SharingRuleFormModal'
import { SharingRuleTableActions } from './components/SharingRuleTableActions'

const COLUMNS: NTableColumnType<SharingRuleResponse> = [
  { Header: 'Display name', accessor: 'displayName' },
  { Header: 'Name', accessor: 'name' },
  { Header: 'Description', accessor: 'description' },
  { Header: 'Type', accessor: 'type' },
  { Header: 'Access type', accessor: 'accessType' },
  {
    Header: 'Object',
    accessor: 'object',
    Cell: ({ value }) => {
      return value.displayName
    },
  },
  {
    Header: 'Created At',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
  },
  {
    accessor: 'name',
    Cell: ({ value }) => {
      return <SharingRuleTableActions name={value} />
    },
    id: classnames([NTableCollapsedCellClassName]),
  },
]

export const SharingRulesListing: FC = () => {
  const history = useHistory()
  const [showCreateModal, setShowCreateModal] = useState(false)
  const { data: sharingRules, isLoading: isLoadingRules } = useQuery(
    [QueryKeys.SharingRules.getSharingRuleList],
    APIServices.SharingRules.getSharingRuleList,
  )

  return (
    <NContainer>
      <NRow justify="space-between">
        <NContainer.Title>Sharing Rules</NContainer.Title>
        <NButton onClick={() => setShowCreateModal(true)} type="primary">
          Create Sharing Rule
        </NButton>
      </NRow>
      <NContainer.Description>{sharingRules?.data.length || 0} sharing rules</NContainer.Description>
      <NTable
        columns={COLUMNS}
        data={sharingRules?.data || []}
        isLoading={isLoadingRules}
        onClickRow={data => {
          history.push(`${DASHBOARD_ROUTE}/people/sharing-rules/${data.name}`)
        }}
      />
      <NDivider size="xl" />
      {showCreateModal && <SharingRuleFormModal visible={showCreateModal} setVisible={setShowCreateModal} />}
    </NContainer>
  )
}
