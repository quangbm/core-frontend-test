import { FC } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import styled from 'styled-components/macro'
import { InviteUserDto } from '../../../../../../../shared/src/api'
import { spacing } from '../../../../../components/GlobalStyle'
import { NDivider } from '../../../../../components/NDivider'
import { NModal } from '../../../../../components/NModal/NModal'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { useSearchText } from '../../../../../hooks/useSearchText'
import { useValidateString } from '../../../../../hooks/useValidateString'
import { APIServices, QueryKeys } from '../../../../../services/api'

const InviteForm = styled.form`
  padding: ${spacing('lg')} ${spacing('xl')};
`

type Props = {
  visible: boolean
  setVisible: (visible: boolean) => void
}

export const InviteUserModal: FC<Props> = ({ visible, setVisible }) => {
  const { handleSubmit, register, formState, control } = useForm()
  const [{ searchValue: searchProfileVal, searchText: searchProfileTxt }, handleSearchProfile] = useSearchText()
  const [{ searchValue: searchRoleVal, searchText: searchRoleTxt }, handleSearchRole] = useSearchText()
  const { validateFunction } = useValidateString()

  const queryClient = useQueryClient()

  const { mutate: inviteUser, isLoading } = useMutation(APIServices.Users.inviteUser, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([QueryKeys.Users.getUsers])
      NToast.success({ title: 'User invited' })
      setVisible(false)
    },
    onError: err => {
      NToast.error({
        title: `Failed to invite`,
        subtitle: err.response?.data.message,
      })
    },
  })

  const { data: profilesData, isFetching: isLoadingProfiles } = useQuery(
    [
      QueryKeys.Profiles.getProfiles,
      {
        searchText: searchProfileTxt,
      },
    ],
    APIServices.Profiles.getProfiles,
  )

  const { data: rolesData, isFetching: isLoadingRoles } = useQuery(
    [
      QueryKeys.Roles.getRoles,
      {
        searchText: searchRoleTxt,
      },
    ],
    APIServices.Roles.getRoles,
  )

  const onSubmit = (data: InviteUserDto) => {
    inviteUser(data)
  }

  return (
    <NModal visible={visible} setVisible={setVisible}>
      <NModal.Header title="Invite User" onClose={() => setVisible(false)} />
      <NModal.Body>
        <InviteForm>
          <NTextInput
            required
            defaultValue=""
            {...register('username', {
              validate: validateFunction,
            })}
            error={formState.errors.name?.message}
            label="Username"
            placeholder="Enter username"
          />
          <NDivider size="sm" />
          <Controller
            defaultValue=""
            control={control}
            name="profileName"
            render={({ field }) => (
              <NSingleSelect
                isLoading={isLoadingProfiles}
                fullWidth
                isSearchable
                searchValue={searchProfileVal}
                onSearchValueChange={handleSearchProfile}
                options={profilesData?.data.data.map(profile => ({ label: profile.name, value: profile.name })) || []}
                placeholder="Search for profile"
                label="Profile"
                value={field.value}
                onValueChange={value => {
                  field.onChange(value)
                }}
              />
            )}
          />
          <NDivider size="sm" />
          <Controller
            defaultValue=""
            control={control}
            name="roleName"
            render={({ field }) => (
              <NSingleSelect
                isLoading={isLoadingRoles}
                fullWidth
                isSearchable
                searchValue={searchRoleVal}
                onSearchValueChange={handleSearchRole}
                options={rolesData?.data.map(role => ({ label: role.name, value: role.name })) || []}
                placeholder="Search for role"
                label="Role"
                value={field.value}
                onValueChange={value => {
                  field.onChange(value)
                }}
              />
            )}
          />
        </InviteForm>
      </NModal.Body>
      <NModal.Footer isLoading={isLoading} onCancel={() => setVisible(false)} onFinish={handleSubmit(onSubmit)} />
    </NModal>
  )
}
