import { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { UserResponse } from '../../../../../../../shared/src/api'
import { NTableActions } from '../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../services/api'

type Props = {
  value: string
  data: UserResponse
}

export const UserTableActions: FC<Props> = ({ value, data }) => {
  const [showDropList, setShowDropList] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: activateUser, isLoading: isActivating } = useMutation(APIServices.Users.activateUser, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([QueryKeys.Users.getUsers])
      await queryClient.invalidateQueries([QueryKeys.Users.getUser, { userId: value }])
      setShowDropList(false)
    },
    onError: err => {
      NToast.error({
        title: `Failed to activate user`,
        subtitle: err.response?.data.message,
      })
    },
  })

  const { mutate: deactivateUser, isLoading: isDeactivating } = useMutation(APIServices.Users.deactivateUser, {
    onSuccess: async () => {
      await queryClient.invalidateQueries([QueryKeys.Users.getUsers])
      await queryClient.invalidateQueries([QueryKeys.Users.getUser, { userId: value }])
      setShowDropList(false)
    },
    onError: err => {
      NToast.error({
        title: `Failed to deactivate user`,
        subtitle: err.response?.data.message,
      })
    },
  })

  return (
    <NTableActions
      options={[
        {
          title: data.isActive ? 'Deactivate' : 'Activate',
          isLoading: isActivating || isDeactivating,
          onClick: () => (data.isActive ? deactivateUser({ userId: value }) : activateUser({ userId: value })),
        },
      ]}
      showDropList={showDropList}
      setShowDropList={setShowDropList}
    />
  )
}
