import { formatDistanceToNow } from 'date-fns'
import React, { FC } from 'react'
import { Controller, FormProvider, useForm } from 'react-hook-form'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled, { useTheme } from 'styled-components/macro'
import { UpdateUserDto } from '../../../../../../../shared/src/api'
import { DASHBOARD_ROUTE } from '../../../../../common/constants'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDetailHeader } from '../../../../../components/NDetailHeader'
import { FieldItemMode, NFieldItem } from '../../../../../components/NFieldItem/NFieldItem'
import { NContainer } from '../../../../../components/NLayout/NContainer'
import { SelectOption } from '../../../../../components/NSelect/model'
import { NSingleSelect } from '../../../../../components/NSelect/NSingleSelect'
import { NSpinner } from '../../../../../components/NSpinner/NSpinner'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NToast } from '../../../../../components/NToast'
import { SettingBoard } from '../../../../../components/SettingBoard'
import { useMe } from '../../../../../hooks/useMe'
import { useSearchText } from '../../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../../services/api'

const DetailForm = styled.form`
  padding: ${spacing('xl')};
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: ${spacing('xl')};
  grid-row-gap: ${spacing('md')};
`
const LoadingWrapper = styled.div`
  padding: ${spacing('xl')};
  display: flex;
  align-items: center;
  justify-content: center;
`

const SaveBtn = styled(NButton)`
  width: 80px;
`

export const UserDetail: FC = () => {
  const { userId } = useParams<{ userId: string }>()

  const formMethods = useForm()
  const { handleSubmit, control } = formMethods
  const theme = useTheme()

  const queryClient = useQueryClient()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const { data: meData } = useMe()

  const { data: user, isLoading: isLoadingUser } = useQuery(
    [
      QueryKeys.Users.getUser,
      {
        userId,
      },
    ],
    APIServices.Users.getUser,
    {
      select: resp => {
        return {
          ...resp.data,
          userInfo: resp.data.userInfo as Record<string, any>,
        }
      },
      onError: err => {
        NToast.error({
          title: `Failed to get user`,
          subtitle: err.response?.data.message,
        })
      },
    },
  )

  const { mutate: updateUser, isLoading: isUpdating } = useMutation(APIServices.Users.updateUser, {
    onSuccess: async () => {
      NToast.success({
        title: 'Updated successful',
      })
      await queryClient.invalidateQueries([QueryKeys.Users.getUsers])
      await queryClient.invalidateQueries([QueryKeys.Users.getUser, { userId }])
    },
    onError: err => {
      NToast.error({
        title: `Failed to update user`,
        subtitle: err.response?.data.message,
      })
    },
  })

  const { data: profilesData, isFetching: isLoadingProfiles } = useQuery(
    [
      QueryKeys.Profiles.getProfiles,
      {
        searchText: searchText,
      },
    ],
    APIServices.Profiles.getProfiles,
    {
      select: resp => {
        return resp.data.data.map(
          profile =>
            ({
              label: profile.name,
              value: profile.name,
            } as SelectOption),
        )
      },
    },
  )

  const { data: rolesData, isFetching: isLoadingRoles } = useQuery(
    [QueryKeys.Roles.getRoles],
    APIServices.Roles.getRoles,
    {
      select: data => {
        return data.data.map(
          role =>
            ({
              label: role.displayName || role.name,
              value: role.name,
            } as SelectOption),
        )
      },
    },
  )

  const onSubmit = ({ profileName, roleName, ...restData }: UpdateUserDto) => {
    updateUser({
      userId: userId,
      userInfo: restData,
      profileName,
      roleName,
    })
  }

  return (
    <NContainer>
      <NDetailHeader
        breadcrumbs={[
          { path: DASHBOARD_ROUTE, label: 'Dashboard' },
          { path: `${DASHBOARD_ROUTE}/people`, label: 'People' },
          { path: `${DASHBOARD_ROUTE}/people/users`, label: 'Users' },
          { label: ((user?.userInfo || {}) as Record<string, any>)['name'] || '' },
        ]}
        label={
          user?.updatedAt && `Last updated ${formatDistanceToNow(new Date(user?.updatedAt), { addSuffix: true })}`
        }>
        <SaveBtn loading={isUpdating} type="outline-2" onClick={handleSubmit(onSubmit)}>
          Save
        </SaveBtn>
      </NDetailHeader>
      <SettingBoard title="User Detail">
        {isLoadingUser && (
          <LoadingWrapper>
            <NSpinner size={30} strokeWidth={2} color={color('Primary900')({ theme })} />
          </LoadingWrapper>
        )}

        {user && meData && (
          <DetailForm>
            <FormProvider {...formMethods}>
              {meData.userInfo.schema.fields.map(field =>
                user.userInfo.hasOwnProperty(field.name) ? (
                  <NFieldItem
                    key={field.name}
                    mode={FieldItemMode.Form}
                    field={field}
                    contents={user.userInfo || {}}
                    pickList={meData.userInfo.schema.pickList}
                  />
                ) : null,
              )}
            </FormProvider>
            {user?.role && (
              <Controller
                defaultValue={user?.role.name}
                control={control}
                name="roleName"
                render={({ field }) => (
                  <NSingleSelect
                    fullWidth
                    required
                    options={rolesData || []}
                    isLoading={isLoadingRoles}
                    label="Role"
                    value={field.value}
                    onValueChange={value => {
                      field.onChange(value)
                    }}
                  />
                )}
              />
            )}
            {user?.profile && (
              <Controller
                defaultValue={user?.profile.name}
                control={control}
                name="profileName"
                render={({ field }) => (
                  <NSingleSelect
                    fullWidth
                    isSearchable
                    required
                    searchValue={searchValue}
                    onSearchValueChange={handleSearchChange}
                    options={profilesData || []}
                    isLoading={isLoadingProfiles}
                    placeholder="Search for profile"
                    label="Profile"
                    value={field.value}
                    onValueChange={value => {
                      field.onChange(value)
                    }}
                  />
                )}
              />
            )}
            <NTextInput defaultValue={user?.ssoId || ''} label="SSO Id" disabled />
          </DetailForm>
        )}
      </SettingBoard>
    </NContainer>
  )
}
