import { FC, useState } from 'react'
import { ApplicationResponse } from '../../../../../../../shared/src/api'
import { NTableActions } from '../../../../../components/NTable/NTableActions'

type AppTableActionsProps = {
  value: string
  data: ApplicationResponse
}

export const AppTableActions: FC<AppTableActionsProps> = () => {
  const [showDropList, setShowDropList] = useState(false)

  // TODO: Add delete cuz related to other BE task
  return <NTableActions options={[{ title: 'Delete' }]} showDropList={showDropList} setShowDropList={setShowDropList} />
}
