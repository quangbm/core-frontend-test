import * as React from 'react'
import { Controller, useFieldArray } from 'react-hook-form'
import styled from 'styled-components/macro'
import { spacing } from '../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'
import { NDivider } from '../../../../../components/NDivider'
import { NTextInput } from '../../../../../components/NTextInput/NTextInput'
import { NTypography } from '../../../../../components/NTypography'

const Wrapper = styled('div')`
  display: flex;
  & > * {
    &:first-child {
      flex: 1;
    }
    &:not(:last-child) {
      margin-right: ${spacing('xs')};
    }
  }
  &:not(:last-child) {
    margin-bottom: ${spacing('xs')};
  }
`

type ScriptBuilderProps = {
  control: any
  name: string
  label?: string
}

export const ScriptBuilder = React.memo<ScriptBuilderProps>(function Node({ name, control, label }) {
  const { fields, append, remove } = useFieldArray({ control, name })
  return (
    <React.Fragment>
      {label && (
        <>
          <NTypography.InputLabel>{label}</NTypography.InputLabel>
          <NDivider size="xs" />
        </>
      )}
      {fields.map((field, fieldIndex) => (
        <Wrapper key={field.id}>
          <Controller
            control={control}
            name={`${name}.${fieldIndex}.src`}
            render={({ field: { value, onChange }, fieldState }) => (
              <NTextInput value={value} onChange={e => onChange(e.target.value)} error={fieldState.error?.message} />
            )}
            rules={{ required: { value: true, message: 'Required!' } }}
          />
          <NButton type="ghost" onClick={() => remove(fieldIndex)}>
            <GlobalIcons.Trash />
          </NButton>
        </Wrapper>
      ))}
      <NButton type="primary" size="small" onClick={() => append({})}>
        <GlobalIcons.Plus />
      </NButton>
    </React.Fragment>
  )
})

ScriptBuilder.displayName = 'ScriptBuilder'
