import { FC, useEffect, useState } from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useParams } from 'react-router-dom'
import styled from 'styled-components/macro'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { spacing } from '../../../../components/GlobalStyle'
import { NBreadcrumbs } from '../../../../components/NBreadcrumbs/NBreadcrumbs'
import { NButton } from '../../../../components/NButton/NButton'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NToast } from '../../../../components/NToast'
import { PickColumn, PickListOption } from '../../../../components/PickColumn'
import { SettingBoard } from '../../../../components/SettingBoard'
import { useSearchText } from '../../../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../../../services/api'
import { ObjectHeadersComponent } from '../Object/components/ObjectHeaderComponent'

const Wrapper = styled.div`
  padding: ${spacing('xl')};
`

export const UserProfile: FC = () => {
  const [selectedProfiles, setSelectedProfile] = useState<PickListOption[]>([])
  const { id } = useParams<{ id: string }>()
  const [{ searchValue, searchText }, handleSearchChange] = useSearchText()
  const queryClient = useQueryClient()

  const { data: appData } = useQuery([QueryKeys.App.getApp, { id }], APIServices.App.getApp)

  const { data: profilesData, isFetching: isLoadingProfiles } = useQuery(
    [
      QueryKeys.Profiles.getProfiles,
      {
        searchText: searchText,
      },
    ],
    APIServices.Profiles.getProfiles,
  )

  const { data: selectedProfilesData, isFetching: isLoadingSelectedProfiles } = useQuery(
    [QueryKeys.Builder.getAppProfiles, { id }],
    APIServices.Builder.getAppProfiles,
  )

  const { mutate: updateAppProfiles, isLoading: isUpdateProfilesObjects } = useMutation(
    APIServices.Builder.updateAppProfiles,
    {
      onSuccess: () => {
        queryClient.invalidateQueries([QueryKeys.Builder.getAppProfiles, { id }])
        queryClient.invalidateQueries(QueryKeys.App.getApps)
      },
      onError: error => {
        NToast.error({ title: 'Update profiles unsuccessful', subtitle: error.response?.data.message })
      },
    },
  )

  useEffect(() => {
    if (selectedProfilesData?.data) {
      setSelectedProfile(selectedProfilesData?.data.map(prof => ({ value: prof.id, label: prof.name })))
    }
  }, [selectedProfilesData])

  const onSave = () => {
    updateAppProfiles({
      id,
      profileIds: selectedProfiles.map(profile => profile.value),
    })
  }

  return (
    <>
      <ObjectHeadersComponent.HeaderContainer>
        <NBreadcrumbs
          breadcrumbs={[
            { path: DASHBOARD_ROUTE, label: 'Dashboard' },
            {
              path: `${DASHBOARD_ROUTE}/app`,
              label: 'Applications',
            },
            {
              label: appData?.data.name || '',
            },
          ]}
        />
        <ObjectHeadersComponent.ButtonContainer>
          {/* Last updated <NDivider vertical size="xl" /> */}
          <NButton loading={isUpdateProfilesObjects} type="outline-2" onClick={onSave}>
            Save
          </NButton>
        </ObjectHeadersComponent.ButtonContainer>
      </ObjectHeadersComponent.HeaderContainer>
      <NContainer.Title>{appData?.data.name}</NContainer.Title>
      <SettingBoard title="User Profile">
        <Wrapper>
          <PickColumn
            options={profilesData?.data.data.map(profile => ({ label: profile.name, value: profile.id })) || []}
            onOptionsChange={setSelectedProfile}
            selectedOptions={selectedProfiles}
            leftHeader="Available profiles"
            rightHeader="Selected profiles"
            isLoading={isLoadingProfiles || isLoadingSelectedProfiles}
            searchValue={searchValue}
            onSearchValueChange={handleSearchChange}
          />
        </Wrapper>
      </SettingBoard>
    </>
  )
}
