import { FC, useMemo, useState } from 'react'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import { ApplicationResponse } from '../../../../../../shared/src/api'
import { DASHBOARD_ROUTE } from '../../../../common/constants'
import { NButton } from '../../../../components/NButton/NButton'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../components/NTable/NTable'
import { NTableToolbar } from '../../../../components/NTable/NTableToolbar'
import { useTableConfigs } from '../../../../hooks/useTableConfigs'
import { APIServices, QueryKeys } from '../../../../services/api'
import { getTableData } from '../../../../utils/table-utils'
import { AppCreateModal } from './AppCreateModal'

const PAGE_SIZE = 10

const COLUMNS: NTableColumnType<ApplicationResponse> = [
  { Header: 'Name', accessor: 'name', defaultCanSort: true },
  {
    Header: 'Description',
    accessor: 'description',
  },
  {
    Header: 'Created at',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  // {
  //   accessor: 'id',
  //   Cell: ({ value, row }) => {
  //     return <AppTableActions value={value} data={row.original} />
  //   },
  //   id: classnames([NTableCollapsedCellClassName]),
  // },
]

export const AppListing: FC = () => {
  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()
  const history = useHistory()
  const [showCreateApp, setShowCreateApp] = useState(false)

  const { data: appsData, isLoading: isLoadingAppsData } = useQuery(
    [
      QueryKeys.Builder.getApps,
      {
        searchText: searchText,
        limit: PAGE_SIZE,
        offset: (currentPage - 1) * PAGE_SIZE,
        sortBy,
        sortOrder,
      },
    ],
    APIServices.Builder.getApps,
  )

  const { pageData, pageInfo, totalPage } = useMemo(() => {
    return getTableData(appsData)
  }, [appsData])

  return (
    <>
      <NContainer>
        <NRow justify="space-between">
          <NContainer.Title>Applications</NContainer.Title>
          <NButton type="primary" onClick={() => setShowCreateApp(true)}>
            Create App
          </NButton>
        </NRow>
        <NContainer.Description>{pageInfo.total} apps</NContainer.Description>
        <NTableToolbar
          searchConfig={{
            value: searchValue,
            onChange: e => {
              onChangeSearch(e.target.value)
            },
          }}
        />
        <NTable
          isLoading={isLoadingAppsData}
          columns={COLUMNS}
          data={pageData}
          pageSize={PAGE_SIZE}
          pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
          onClickRow={data => {
            history.push(`${DASHBOARD_ROUTE}/app/${data.id}/edit`)
          }}
          defaultSortBy={sortBy}
          defaultSortOrder={sortOrder}
          onChangeSort={onChangeSort}
        />
      </NContainer>
      <AppCreateModal visible={showCreateApp} setVisible={setShowCreateApp} />
    </>
  )
}
