import React from 'react'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { Link, useHistory } from 'react-router-dom'
import { CreateLayoutDto, LayoutResponse } from '../../../../../../shared/src/api'
import { NButton } from '../../../../components/NButton/NButton'
import { NRow } from '../../../../components/NGrid/NGrid'
import { NContainer } from '../../../../components/NLayout/NContainer'
import { NPagination } from '../../../../components/NPagination'
import { NTable, NTableColumnType } from '../../../../components/NTable/NTable'
import { NTableToolbar } from '../../../../components/NTable/NTableToolbar'
import { NToast } from '../../../../components/NToast'
import { useTableConfigs } from '../../../../hooks/useTableConfigs'
import { APIServices, QueryKeys } from '../../../../services/api'
import { getTableData } from '../../../../utils/table-utils'
import { capitalize } from '../../../../utils/utils'
import { CreateLayoutModal } from './CreateLayoutModal'

type LayoutProps = {
  children?: React.ReactNode
}

const PAGE_SIZE = 10

const COLUMNS: NTableColumnType<LayoutResponse> = [
  {
    Header: 'Layout',
    accessor: 'name',
    defaultCanSort: true,
    Cell({ value, row }) {
      return (
        <Link to={`/layout-builder/${row.original.id}`} onClick={e => e.stopPropagation()}>
          {value}
        </Link>
      )
    },
  },
  {
    Header: 'Description',
    accessor: 'description',
  },
  {
    Header: 'Type',
    accessor: 'type',
    defaultCanSort: true,
    Cell({ value }) {
      return capitalize(value)
    },
  },
  {
    Header: 'Object',
    accessor: 'objectName',
    defaultCanSort: true,
  },
  {
    Header: 'Created at',
    accessor: 'createdAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
  {
    Header: 'Last modified',
    accessor: 'updatedAt',
    Cell: ({ value }) => {
      if (!value) {
        return null
      }
      const date = new Date(value)
      return date.toLocaleString('vi')
    },
    defaultCanSort: true,
  },
]

export function LayoutListing({}: LayoutProps) {
  const history = useHistory()
  const queryClient = useQueryClient()

  const [showModal, setShowModal] = React.useState(false)

  const [
    { sortBy, sortOrder, searchText, searchValue, page: currentPage },
    { onChangeSort, onChangePage, onChangeSearch },
  ] = useTableConfigs()

  const { data, isFetching } = useQuery(
    [
      QueryKeys.Builder.getLayouts,
      { limit: PAGE_SIZE, offset: (currentPage - 1) * PAGE_SIZE, searchText, sortBy, sortOrder },
    ],
    APIServices.Builder.getLayouts,
    { keepPreviousData: true },
  )

  const { pageData, pageInfo, totalPage } = React.useMemo(() => {
    return getTableData(data)
  }, [data])

  const { mutate: createLayout, isLoading } = useMutation(APIServices.Builder.postLayout, {
    onSuccess(layout) {
      queryClient.invalidateQueries([QueryKeys.Builder.getLayouts])
      history.push(`/layout-builder/${layout.data.id}`)
    },
    onError: error => {
      NToast.error({
        title: 'Create layout unsuccessful',
        subtitle: error.response?.data.message,
      })
    },
  })

  const handleSubmit = (data: CreateLayoutDto) => {
    createLayout(data)
  }

  return (
    <NContainer>
      <NRow justify="space-between">
        <NContainer.Title>Layout</NContainer.Title>
        <NButton loading={isLoading} type="primary" onClick={() => setShowModal(true)}>
          Create Layout
        </NButton>
      </NRow>
      <NContainer.Description>{pageInfo.total} Layouts</NContainer.Description>
      <NContainer.Content>
        <NTableToolbar
          searchConfig={{
            value: searchValue,
            onChange: e => {
              onChangeSearch(e.target.value)
            },
          }}
        />
        <NTable
          isLoading={isFetching}
          columns={COLUMNS}
          data={pageData}
          defaultSortBy={sortBy}
          defaultSortOrder={sortOrder}
          onChangeSort={onChangeSort}
          pageSize={PAGE_SIZE}
          pagination={<NPagination total={totalPage} current={currentPage} onChangePage={onChangePage} />}
          onClickRow={data => {
            history.push(`/layout-builder/${data.id}`, data)
          }}
        />
      </NContainer.Content>
      {showModal && <CreateLayoutModal visible={showModal} setVisible={setShowModal} onSubmit={handleSubmit} />}
    </NContainer>
  )
}
