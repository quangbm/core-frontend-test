import { AndLogicRule, FieldAttributesDto, ValidationRuleResponse } from '../../../../../../shared/src/api'

export interface IFieldForm {
  name: string
  displayName: string
  defaultValue: string
  isRequired: boolean
  isExternalId: boolean
  value: string
  attributes: FieldAttributesDto
  pickListId: string
}

export type RuleState = {
  isEditing?: boolean
  ruleData?: Partial<ValidationRuleResponse>
}

export type RuleScript = { script: AndLogicRule[]; errorMessage?: string }
