import { FC } from 'react'
import styled from 'styled-components/macro'
import { color, spacing } from '../../../../../components/GlobalStyle'
import { GlobalIcons } from '../../../../../components/Icons'
import { NButton } from '../../../../../components/NButton/NButton'

const Wrapper = styled.div`
  display: inline-flex;
  flex-direction: column;
  align-items: center;
`

const LinkLine = styled.div`
  margin-top: ${spacing(2)};
  margin-bottom: ${spacing(2)};
  width: 1px;
  height: 12px;
  background: ${color('Neutral200')};
`

type AddBtnProps = {
  className?: string
  onClick: () => void
}

export const AddBtn: FC<AddBtnProps> = ({ className, onClick }) => {
  return (
    <Wrapper className={className}>
      <LinkLine />
      <NButton size="small" type="primary" icon={<GlobalIcons.Add />} onClick={onClick} />
    </Wrapper>
  )
}
