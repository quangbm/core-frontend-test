import React, { FC, useState } from 'react'
import { useMutation, useQueryClient } from 'react-query'
import { ObjectResponse, PostAction } from '../../../../../../../shared/src/api'
import { NTableActions } from '../../../../../components/NTable/NTableActions'
import { NToast } from '../../../../../components/NToast'
import { APIServices, QueryKeys } from '../../../../../services/api'

type ObjectTableActionsProps = {
  value: string
  data: ObjectResponse
  onEditClick: (data: ObjectResponse) => void
}
export const ObjectTableActions: FC<ObjectTableActionsProps> = ({ value, data, onEditClick }) => {
  const [showDropList, setShowDropList] = useState(false)
  // post object mutation
  const [isLoading, setIsLoading] = useState(false)
  const queryClient = useQueryClient()

  const { mutate: postObjectApi } = useMutation(APIServices.Metadata.postObject)

  const handleEdit = () => {
    onEditClick(data)
    setShowDropList(false)
  }

  const handleDelete = () => {
    setIsLoading(true)

    postObjectApi(
      { action: PostAction.Delete, name: value },
      {
        onSettled: () => {
          setIsLoading(false)
        },
        onSuccess: () => {
          queryClient.invalidateQueries(QueryKeys.Metadata.getObjects)
          setShowDropList(false)
        },
        onError: error => {
          NToast.error({
            title: 'Delete unsuccessful',
            subtitle: error.response?.data.message,
          })
        },
      },
    )
  }
  return (
    <NTableActions
      showDropList={showDropList}
      setShowDropList={setShowDropList}
      options={[
        { title: 'Edit', onClick: handleEdit },
        { title: 'Delete', isLoading, onClick: handleDelete },
      ]}
    />
  )
}
