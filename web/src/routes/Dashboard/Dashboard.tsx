import * as React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import styled from 'styled-components/macro'
import { ProfileAccessResponse } from '../../../../shared/src/api'
import { ReactComponent as NuclentCoreIcon } from '../../assets/icons/nuclent-core.svg'
import { DASHBOARD_ROUTE } from '../../common/constants'
import { AppHeader } from '../../components/AppHeader/AppHeader'
import { color, spacing } from '../../components/GlobalStyle'
import { NDivider } from '../../components/NDivider'
import { NLayout } from '../../components/NLayout'
import { NNavBar } from '../../components/NNavBar/NNavBar'
import { typography } from '../../components/NTypography'
import { useMe } from '../../hooks/useMe'
import { Can } from '../../services/casl/Can'
import i18n from '../../services/i18n'
import { NoPermission } from '../NoPermission/NoPermission'
import { dashboardRoutes, navItems } from './dashboardNavInfo'
import en from './locales/en'
import vi from './locales/vi'

i18n.addResourceBundle('en', 'translation', { dashboard: en })
i18n.addResourceBundle('vi', 'translation', { dashboard: vi })

type DashboardProps = {
  children?: React.ReactNode
}

const Wrapper = styled.div`
  display: flex;
  flex: 1;
`

const NavSideBar = styled(NNavBar)`
  border-right: 0;
  width: 56px;
`

const NavItem = styled(NNavBar.Item)`
  margin-bottom: ${spacing('xs')};
`

const Title = styled('div')`
  margin-left: ${spacing('xs')};
  ${typography('h500')}
  color:  ${color('Neutral700')};
`
const TitleText = styled.span`
  color: ${color('Neutral500')};
`

export function Dashboard({}: DashboardProps) {
  const { data: meData } = useMe()

  return (
    <NLayout>
      <AppHeader
        left={
          <>
            <NDivider vertical size="xs" />
            <NuclentCoreIcon />
            <Title>
              core <TitleText>setup</TitleText>
            </Title>
          </>
        }
      />
      <Wrapper>
        <NavSideBar>
          {navItems.map(item => (
            <Can I="read" a={item.feature} key={item.to}>
              <NavItem {...item} />
            </Can>
          ))}
        </NavSideBar>
        <Switch>
          {dashboardRoutes.map(route => (
            <Route key={route.path} path={route.path}>
              <Can I="read" a={route.feature}>
                {route.component}
              </Can>
              <Can not I="read" a={route.feature}>
                <NoPermission />
              </Can>
            </Route>
          ))}
          <Route path={`${DASHBOARD_ROUTE}`}>
            <Redirect to={getFirstRoute(meData?.profile.featuresAccess as Record<string, ProfileAccessResponse>)} />
          </Route>
        </Switch>
      </Wrapper>
    </NLayout>
  )
}

const getFirstRoute = (feature?: Record<string, ProfileAccessResponse>) => {
  let firstRoute = '/'
  if (feature) {
    for (const featureKey of Object.keys(feature)) {
      if (feature[featureKey].read) {
        firstRoute = navItems.find(nav => nav.feature === featureKey)?.to || '/'
        break
      }
    }
  }

  return firstRoute
}
