import { DASHBOARD_ROUTE } from '../../common/constants'
import { SidebarIcons } from '../../components/Icons'
import { AppRoutes } from './routes/App/AppRoutes'
import { FlowListing } from './routes/Flow/FlowListing'
import { FlowVersions } from './routes/Flow/FlowVersions'
import { ExternalObjectsDetailRoutes } from './routes/Integrations/ExternalObjects/ExternalObjectsDetailRoutes'
import { IntegrationsRoutes } from './routes/Integrations/IntegrationsRoutes'
import { LayoutListing } from './routes/Layout/LayoutListings'
import { ObjectDetail } from './routes/Object/ObjectDetail'
import { ObjectListing } from './routes/Object/ObjectListing'
import { Organization } from './routes/Organization/Organization'
import { People } from './routes/People/People'
import { ProfileDetail } from './routes/People/Profiles/ProfileDetail'

export const navItems = [
  {
    title: 'Applications',
    to: `${DASHBOARD_ROUTE}/app`,
    icon: <SidebarIcons.AppOutlined />,
    activeIcon: <SidebarIcons.AppFilled />,
    feature: 'application-layout',
  },
  {
    title: 'Objects',
    to: `${DASHBOARD_ROUTE}/object`,
    icon: <SidebarIcons.ObjectOutlined />,
    activeIcon: <SidebarIcons.ObjectFilled />,
    feature: 'mo-integration',
  },
  {
    title: 'Layouts',
    to: `${DASHBOARD_ROUTE}/layout`,
    icon: <SidebarIcons.LayoutOutlined />,
    activeIcon: <SidebarIcons.LayoutFilled />,
    feature: 'application-layout',
  },
  {
    title: 'People',
    to: `${DASHBOARD_ROUTE}/people`,
    icon: <SidebarIcons.PeopleOutlined />,
    activeIcon: <SidebarIcons.PeopleFilled />,
    feature: 'user',
  },
  {
    title: 'Integrations',
    to: `${DASHBOARD_ROUTE}/integrations`,
    icon: <SidebarIcons.IntegrationOutlined />,
    activeIcon: <SidebarIcons.IntegrationFilled />,
    feature: 'mo-integration',
  },
  {
    title: 'Flows',
    to: `${DASHBOARD_ROUTE}/flow`,
    icon: <SidebarIcons.AutomationOutlined />,
    activeIcon: <SidebarIcons.AutomationFilled />,
    feature: 'flow',
  },
  {
    title: 'Organization',
    to: `${DASHBOARD_ROUTE}/organization`,
    icon: <SidebarIcons.OrganizationOutlined />,
    activeIcon: <SidebarIcons.OrganizationFilled />,
    feature: 'organization',
  },
]

export const dashboardRoutes = [
  {
    path: `${DASHBOARD_ROUTE}/app`,
    component: <AppRoutes />,
    feature: 'application-layout',
  },
  {
    path: `${DASHBOARD_ROUTE}/layout`,
    component: <LayoutListing />,
    feature: 'application-layout',
  },
  {
    path: `${DASHBOARD_ROUTE}/object/:objName`,
    component: <ObjectDetail />,
    feature: 'mo-integration',
  },
  {
    path: `${DASHBOARD_ROUTE}/object`,
    component: <ObjectListing />,
    feature: 'mo-integration',
  },
  {
    path: `${DASHBOARD_ROUTE}/integrations/external-objects/:objName`,
    component: <ExternalObjectsDetailRoutes />,
    feature: 'mo-integration',
  },
  {
    path: `${DASHBOARD_ROUTE}/integrations`,
    component: <IntegrationsRoutes />,
    feature: 'mo-integration',
  },
  {
    path: `${DASHBOARD_ROUTE}/flow/:flowName/templates`,
    component: <FlowVersions />,
    feature: 'flow',
  },
  {
    path: `${DASHBOARD_ROUTE}/flow`,
    component: <FlowListing />,
    feature: 'flow',
  },
  {
    path: `${DASHBOARD_ROUTE}/people/profiles/:profileId`,
    component: <ProfileDetail />,
    feature: 'user',
  },
  {
    path: `${DASHBOARD_ROUTE}/people`,
    component: <People />,
    feature: 'user',
  },

  {
    path: `${DASHBOARD_ROUTE}/organization`,
    component: <Organization />,
    feature: 'organization',
  },
]
