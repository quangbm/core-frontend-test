import styled, { css, keyframes } from 'styled-components/macro'
import { getAlpha } from '../GlobalStyle'

const rotate = keyframes`
  0% {
    transform: rotate(0);
  }
  100% {
    transform: rotate(360deg);
  }
`
export type NSpinnerProps = {
  size?: number
  strokeWidth?: number
  color?: string
  sizeUnit?: string
}

export const NSpinner = styled('div').attrs(() => ({ 'data-testid': 'NSpinner' }))<NSpinnerProps>`
  ${({ size = 12, sizeUnit = 'px', strokeWidth = 2, color = '#66BBB5' }) => css`
    width: ${size}${sizeUnit};
    height: ${size}${sizeUnit};
    border-width: ${strokeWidth}${sizeUnit};
    border-color: ${color};
    border-right-color: ${color}${getAlpha(10)};
  `}
  border-style: solid;
  border-radius: 50%;
  animation: ${rotate} 0.75s linear infinite;
`
