import { ReactComponent as ArrowDown } from '../../assets/icons/8/arrow-down.svg'
import { ReactComponent as ArrowUp } from '../../assets/icons/8/arrow-up.svg'
import { ReactComponent as Check } from '../../assets/icons/8/check.svg'
import { ReactComponent as Down } from '../../assets/icons/8/chevron-down.svg'
import { ReactComponent as Left } from '../../assets/icons/8/chevron-left.svg'
import { ReactComponent as Right } from '../../assets/icons/8/chevron-right.svg'
import { ReactComponent as Up } from '../../assets/icons/8/chevron-up.svg'
import { ReactComponent as Close8 } from '../../assets/icons/8/close.svg'
import { ReactComponent as FilledLeft } from '../../assets/icons/8/filled-chevron-left.svg'
import { ReactComponent as More } from '../../assets/icons/8/more-icon.svg'
import { ReactComponent as Sort } from '../../assets/icons/8/sort.svg'
import { ReactComponent as Checkbox } from '../../assets/icons/Datatype/checkbox.svg'
import { ReactComponent as Currency } from '../../assets/icons/Datatype/currency.svg'
import { ReactComponent as Datetime } from '../../assets/icons/Datatype/datetime.svg'
import { ReactComponent as ExternalRelation } from '../../assets/icons/Datatype/external-relation.svg'
import { ReactComponent as Formula } from '../../assets/icons/Datatype/formula.svg'
import { ReactComponent as Generated } from '../../assets/icons/Datatype/generated.svg'
import { ReactComponent as JSON } from '../../assets/icons/Datatype/json.svg'
import { ReactComponent as Numeric } from '../../assets/icons/Datatype/numeric.svg'
import { ReactComponent as Picklist } from '../../assets/icons/Datatype/picklist.svg'
import { ReactComponent as Relation } from '../../assets/icons/Datatype/relation.svg'
import { ReactComponent as Text } from '../../assets/icons/Datatype/text.svg'
import { ReactComponent as Add } from '../../assets/icons/Global/Add.svg'
import { ReactComponent as Calendar } from '../../assets/icons/Global/Calendar.svg'
import { ReactComponent as GlobalCheck } from '../../assets/icons/Global/Check.svg'
import { ReactComponent as LeftLarge } from '../../assets/icons/Global/CheveronLeftLarge.svg'
import { ReactComponent as Close } from '../../assets/icons/Global/Close.svg'
import { ReactComponent as CloseEye } from '../../assets/icons/Global/CloseEye.svg'
import { ReactComponent as DownArrow } from '../../assets/icons/Global/DownArrow.svg'
import { ReactComponent as Edit } from '../../assets/icons/Global/Edit.svg'
import { ReactComponent as Eye } from '../../assets/icons/Global/Eye.svg'
import { ReactComponent as GridMenu } from '../../assets/icons/Global/GridMenu.svg'
import { ReactComponent as Info } from '../../assets/icons/Global/Info.svg'
import { ReactComponent as Key } from '../../assets/icons/Global/Key.svg'
import { ReactComponent as LeftArrow } from '../../assets/icons/Global/LeftArrow.svg'
import { ReactComponent as Menu } from '../../assets/icons/Global/Menu.svg'
import { ReactComponent as Minus } from '../../assets/icons/Global/Minus.svg'
import { ReactComponent as MoreLeft } from '../../assets/icons/Global/MoreLeft.svg'
import { ReactComponent as MoreRight } from '../../assets/icons/Global/MoreRight.svg'
import { ReactComponent as Plus } from '../../assets/icons/Global/Plus.svg'
import { ReactComponent as Reorder } from '../../assets/icons/Global/Reorder.svg'
import { ReactComponent as Search } from '../../assets/icons/Global/Search.svg'
import { ReactComponent as Time } from '../../assets/icons/Global/Time.svg'
import { ReactComponent as Trash } from '../../assets/icons/Global/Trash.svg'
import { ReactComponent as LayoutOrder } from '../../assets/icons/Layout/LayoutOrder.svg'
import { ReactComponent as AppFilled } from '../../assets/icons/SideBar/AppFilled.svg'
import { ReactComponent as AppOutlined } from '../../assets/icons/SideBar/AppOutlined.svg'
import { ReactComponent as AutomationFilled } from '../../assets/icons/SideBar/AutomationFilled.svg'
import { ReactComponent as AutomationOutlined } from '../../assets/icons/SideBar/AutomationOutlined.svg'
import { ReactComponent as BuilderFilled } from '../../assets/icons/SideBar/BuilderFilled.svg'
import { ReactComponent as BuilderOutlined } from '../../assets/icons/SideBar/BuilderOutlined.svg'
import { ReactComponent as IntegrationFilled } from '../../assets/icons/SideBar/IntegrationFilled.svg'
import { ReactComponent as IntegrationOutlined } from '../../assets/icons/SideBar/IntegrationOutlined.svg'
import { ReactComponent as LargePlusOutlined } from '../../assets/icons/SideBar/LargePlusOutlined.svg'
import { ReactComponent as LayoutFilled } from '../../assets/icons/SideBar/LayoutFilled.svg'
import { ReactComponent as LayoutOutlined } from '../../assets/icons/SideBar/LayoutOutlined.svg'
import { ReactComponent as ObjectFilled } from '../../assets/icons/SideBar/ObjectFilled.svg'
import { ReactComponent as ObjectOutlined } from '../../assets/icons/SideBar/ObjectOutlined.svg'
import { ReactComponent as OrganizationFilled } from '../../assets/icons/SideBar/OrganizationFilled.svg'
import { ReactComponent as OrganizationOutlined } from '../../assets/icons/SideBar/OrganizationOutlined.svg'
import { ReactComponent as PeopleFilled } from '../../assets/icons/SideBar/PeopleFilled.svg'
import { ReactComponent as PeopleOutlined } from '../../assets/icons/SideBar/PeopleOutlined.svg'
import { ReactComponent as SearchFilled } from '../../assets/icons/SideBar/SearchFilled.svg'
import { ReactComponent as SearchOutlined } from '../../assets/icons/SideBar/SearchOutlined.svg'

export const DatatypeIcons = {
  Checkbox,
  Currency,
  Datetime,
  Formula,
  Generated,
  JSON,
  Numeric,
  Picklist,
  Relation,
  ExternalRelation,
  Text,
}

export const Icons = {
  Reorder,
  Down,
  Up,
  Right,
  Left,
  FilledLeft,
  Check,
  ArrowUp,
  ArrowDown,
  More,
  Close8,
  Sort,
  Key,
}

export const SidebarIcons = {
  AutomationFilled,
  AutomationOutlined,
  LargePlusOutlined,
  LayoutFilled,
  LayoutOutlined,
  ObjectFilled,
  ObjectOutlined,
  PeopleFilled,
  PeopleOutlined,
  SearchFilled,
  SearchOutlined,
  BuilderFilled,
  BuilderOutlined,
  AppOutlined,
  AppFilled,
  IntegrationFilled,
  IntegrationOutlined,
  OrganizationFilled,
  OrganizationOutlined,
}

export const GlobalLayoutIcons = {
  LayoutOrder,
}

export const GlobalIcons = {
  Calendar,
  Close,
  Check: GlobalCheck,
  LeftLarge,
  MoreLeft,
  MoreRight,
  Search,
  Trash,
  Add,
  Edit,
  Minus,
  Plus,
  Time,
  Menu,
  GridMenu,
  Info,
  LeftArrow,
  DownArrow,
  Eye,
  CloseEye,
}

export type IconProps = {
  size?: number
  filled?: boolean
  fillColor?: string
}
