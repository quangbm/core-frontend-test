import { FC, ReactNode } from 'react'
import styled from 'styled-components/macro'
import { color, spacing } from './GlobalStyle'
import { NDivider } from './NDivider'
import { elevation } from './NElevation'
import { typography } from './NTypography'

const Board = styled.div`
  ${elevation('Elevation100')}
  border-radius: 4px;
  background: ${color('white')};
  margin-bottom: ${spacing('xxl')};

  &:last-child {
    margin-bottom: 0;
  }
`

const BoardHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: ${spacing('xl')};
  padding-right: ${spacing('xl')};
  border-bottom: 1px solid ${color('Neutral200')};
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  height: 46px;
  button {
    min-width: 80px;
  }
`
const BoardContent = styled.div`
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
`

const BoardTitle = styled.p`
  ${typography('h500')}
`

const TitleWrapper = styled.div`
  display: flex;
  align-items: center;
`

type SettingBoardProps = {
  title?: string
  action?: ReactNode
  className?: string
  headerComponent?: ReactNode
}

export const SettingBoard: FC<SettingBoardProps> = ({ title, children, className, action, headerComponent }) => {
  return (
    <Board className={className}>
      <BoardHeader>
        <TitleWrapper>
          <BoardTitle>{title}</BoardTitle>
          <NDivider size="md" vertical />
          {headerComponent}
        </TitleWrapper>
        {action}
      </BoardHeader>
      <BoardContent>{children}</BoardContent>
    </Board>
  )
}
