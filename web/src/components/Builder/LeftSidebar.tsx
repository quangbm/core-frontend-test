import styled from 'styled-components/macro'
import { color, spacing } from '../GlobalStyle'
import { elevation } from '../NElevation'
import { typography } from '../NTypography'

export const Wrapper = styled('div')`
  width: 251px;
  background: ${color('white')};
  overflow-y: auto;
  border-right: solid 1px ${color('Neutral300')};
`

export const WidgetContent = styled('div')`
  display: grid;
  grid-template: 1fr / repeat(3, 83px);
  grid-gap: 1px;

  .drop-placeholder {
    display: none;
    visibility: hidden;
  }
`

export const WidgetTitle = styled.div`
  ${typography('overline')};
  padding: ${spacing('xs')} ${spacing('sm')};
  border: 1px solid ${color('Neutral200')};
  border-left: 0;
  border-right: 0;
`

export const WidgetWrapper = styled('div')`
  &:first-child {
    ${WidgetTitle} {
      border-top: none;
    }
  }

  &:last-child {
    border-bottom: 1px solid ${color('Neutral200')};
  }
`

export const Item = styled('div')`
  background: ${color('white')};
  color: ${color('Neutral700')};
  cursor: move;
  user-select: none;
  width: 83px;
  height: 83px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  &.is-dragging {
    ${elevation('Elevation400')};
  }
`

export const ItemLabel = styled('div')`
  color: ${color('Neutral500')};
  margin-top: ${spacing('xs')};
  text-align: center;
  ${typography('x-small-ui-text')}
`

type WidgetProps = {
  title: string
  children: React.ReactNode
}

export function Widget({ title, children }: WidgetProps) {
  const span = Array.isArray(children) ? 3 - (children.length % 3) : 2
  return (
    <WidgetWrapper>
      <WidgetTitle>{title}</WidgetTitle>
      <WidgetContent>
        {children}
        {span < 3 && <div style={{ gridColumn: `span ${span}`, background: 'white', height: 83 }} />}
      </WidgetContent>
    </WidgetWrapper>
  )
}
