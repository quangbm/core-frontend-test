import React, { useEffect, useMemo, useState } from 'react'
import styled from 'styled-components/macro'
import { ActionMetadataResponse, RequestInputType } from '../../../../shared/src/api'
import { color, spacing } from '../GlobalStyle'
import { NDivider } from '../NDivider'
import { NModal } from '../NModal/NModal'
import { NSwitch } from '../NSwitch/NSwitch'
import { NTextInput } from '../NTextInput/NTextInput'
import { NTypography } from '../NTypography'

const Wrapper = styled(NModal.Body)`
  padding: ${spacing('lg')};
  max-height: 60vh;
  overflow: auto;
`

const SetValueContainer = styled.div`
  display: flex;
  padding: ${spacing('md')} 0;
  justify-content: space-between;

  .variable_switch {
    height: fit-content;
  }

  &:not(:last-child) {
    border-bottom: 1px solid ${color('Neutral200')};
  }
`

const SetValueContainerLeft = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 40%;
`

const VariableNameText = styled(NTypography.Headline)``

type ExtendRequestInputType = RequestInputType & {
  name: string
  displayName: string
}

type Props = {
  visible: boolean
  setVisible: (val: boolean) => void
  actionMetadata?: ActionMetadataResponse
  defaultInputMap: Record<string, string>
  onSubmit(inputMap: Record<string, string>): void
}
export const ActionMetadataMapInputModal: React.FC<Props> = ({
  visible,
  setVisible,
  actionMetadata,
  defaultInputMap,
  onSubmit,
}) => {
  const [value, onChange] = useState<Record<string, string>>(defaultInputMap)

  useEffect(() => {
    visible && onChange(defaultInputMap)
  }, [defaultInputMap, visible])

  const actionInputs = useMemo(() => {
    if (!actionMetadata) {
      return []
    }
    const { inputs } = actionMetadata

    // get input from path
    let result: ExtendRequestInputType[] = inputs.path.map(inputName => {
      return {
        name: `path.${inputName}`,
        displayName: `${inputName} (path input)`,
        isRequired: true,
        type: 'string',
      }
    })

    const headerKeys = Object.keys(inputs.header)
    const queryKeys = Object.keys(inputs.query)

    headerKeys.map(key => {
      const input = {
        ...inputs.header[key],
        type: 'string',
        name: `header.${key}`,
        displayName: `${key} (header input)`,
      } as ExtendRequestInputType
      result = [...result, input]
      return input
    })

    queryKeys.map(key => {
      const input = {
        ...inputs.query[key],
        name: `query.${key}`,
        displayName: `${key} (query input)`,
      }
      result = [...result, input]
      return input
    })

    if (inputs.body) {
      result = [...result, { ...inputs.body, name: 'body', displayName: `body` }]
    }

    return result
  }, [actionMetadata])

  return (
    <NModal size="large" visible={visible} setVisible={setVisible}>
      <NModal.Header onClose={() => setVisible(false)} title="Column Selection" />
      <Wrapper>
        {actionInputs.map(input => {
          const isChecked = input.isRequired || (input.name in value && value[input.name] !== input.name)

          const toggleVariable = (newVal: boolean) => {
            const newInputMap = { ...value }
            if (newVal) {
              newInputMap[input.name] = ''
            } else {
              delete newInputMap[input.name]
            }
            onChange(newInputMap)
          }

          const setApiNameToInput = (apiName: string) => {
            const newInputMap = { ...value }
            newInputMap[input.name] = apiName
            onChange(newInputMap)
          }

          return (
            <SetValueContainer key={`inputMap_${input.name}`}>
              <SetValueContainerLeft>
                <VariableNameText>{input.displayName}</VariableNameText>
                {isChecked && (
                  <>
                    <NDivider size="sm" />
                    <NTextInput
                      value={value[input.name] || ''}
                      onChange={e => {
                        setApiNameToInput(e.target.value)
                      }}
                    />
                  </>
                )}
              </SetValueContainerLeft>
              {!input.isRequired && (
                <NSwitch className="variable_switch" checked={isChecked} onChange={toggleVariable} />
              )}
            </SetValueContainer>
          )
        })}
      </Wrapper>
      <NModal.Footer
        onCancel={() => setVisible(false)}
        onFinish={() => {
          onSubmit(value)
        }}
      />
    </NModal>
  )
}
