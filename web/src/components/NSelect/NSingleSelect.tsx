import React, { FC, useRef, useState } from 'react'
import styled, { useTheme } from 'styled-components/macro'
import { classnames } from '../../common/classnames'
import { useClickOutside } from '../../hooks/useClickOutside'
import { color, spacing } from '../GlobalStyle'
import { Icons } from '../Icons'
import { NPopover } from '../NPopover'
import { NSpinner } from '../NSpinner/NSpinner'
import { RequiredIndicator } from '../NTypography'
import {
  NoOption,
  OptionsContainer,
  SelectCaption,
  SelectContainer,
  SelectDropdown,
  SelectLabel,
} from './CommonStyledSelect'
import { CommonSelectProps, SelectOption } from './model'

const Wrapper = styled.div`
  display: inline-flex;
  flex-direction: column;
  &.fixed-width {
    min-width: 180px;
  }
  &.full-width {
    width: 100%;
  }
`

const SingleSelectContainer = styled(SelectContainer)`
  height: 40px;
  width: 100%;
`

const ValueContainer = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
`

const Value = styled.div`
  font-size: 14px;
  font-weight: 400;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  width: 100%;
`

const Option = styled.div`
  height: 40px;
  cursor: pointer;
  box-sizing: border-box;
  transition: all 0.3s ease-in-out;
  &.selected,
  &:hover {
    background: ${color('Neutral200')};
  }
`

const TextLabel = styled.div`
  padding: ${spacing('md')};
  padding-top: ${spacing(11)};
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  color: ${color('Neutral700')};
`

const IconContainer = styled.div`
  margin-left: ${spacing('md')};
  align-items: center;
  display: flex;
  color: ${color('Neutral500')};
  &.disabled {
    opacity: 0.6;
  }
`

const SearchInput = styled.input`
  box-sizing: border-box;
  width: 100%;
  border: 0;
`
const LoadingContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 168px;
`

export type NSingleSelectProps = CommonSelectProps & {
  containerClassName?: string
  auto?: boolean
  onValueChange?: (value?: string) => void
  value?: string
  options: SelectOption[]
  isSearchable?: boolean
  searchValue?: string
  onSearchValueChange?: (value: string) => void
  isLoading?: boolean
  onClose?: () => void
  onClear?: () => void
}

export const NSingleSelect: FC<NSingleSelectProps> = ({
  auto = false,
  disabled = false,
  required,
  options,
  onValueChange,
  emptyComponent,
  label,
  caption,
  value,
  placeholder = 'Select an option',
  containerClassName = '',
  className = '',
  error,
  fullWidth = false,
  isSearchable,
  searchValue,
  onSearchValueChange,
  isLoading,
  onClose,
  placement,
  allowClear = false,
  onClear,
  ...rest
}) => {
  const selectRef = useRef<HTMLDivElement>(null)
  const dropdownRef = useRef<HTMLDivElement>(null)
  const [showDropdown, setShowDropdown] = useState(false)
  const theme = useTheme()

  const handleClick = () => {
    if (showDropdown || disabled) {
      return
    }
    setShowDropdown(prev => !prev)
  }

  const handleClose = () => {
    setShowDropdown(false)
    onClose && onClose()
  }
  useClickOutside(handleClose, dropdownRef, selectRef.current ? [selectRef.current] : undefined)

  const selectedOption = options.find(i => i.value === value)

  return (
    <Wrapper className={classnames([fullWidth && 'full-width', !auto && 'fixed-width', containerClassName])}>
      {label && (
        <SelectLabel>
          {required && <RequiredIndicator>*</RequiredIndicator>}
          {label}
        </SelectLabel>
      )}
      <SingleSelectContainer
        {...rest}
        className={classnames([showDropdown && 'opened', disabled && 'disabled', className])}
        data-testid="NSingleSelect"
        ref={selectRef}
        onClick={handleClick}>
        <ValueContainer>
          {showDropdown && isSearchable ? (
            // Add size=1 here so this input won't have default size=20. use width 100% instead
            <SearchInput
              value={searchValue}
              autoFocus
              size={1}
              onChange={e => {
                onSearchValueChange && onSearchValueChange(e.target.value)
              }}
            />
          ) : (
            <Value>
              {(selectedOption && (selectedOption.selectedLabel || selectedOption.label)) || value || placeholder}
            </Value>
          )}
          {allowClear && value && (
            <IconContainer
              onClick={e => {
                if (allowClear && onValueChange) {
                  e.stopPropagation()
                  onValueChange(undefined)
                  onClear && onClear()
                }
              }}
              className={disabled ? 'disabled' : ''}>
              <Icons.Close8 />
            </IconContainer>
          )}

          <IconContainer className={disabled ? 'disabled' : ''}>
            <Icons.Down />
          </IconContainer>
        </ValueContainer>
        {showDropdown && (
          <NPopover targetRef={selectRef} ref={dropdownRef} placement={placement} offsetX={0} offsetY={4} fullWidth>
            <SelectDropdown>
              {isLoading ? (
                <LoadingContainer>
                  <NSpinner size={20} strokeWidth={2} color={color('Primary900')({ theme })} />
                </LoadingContainer>
              ) : (
                <OptionsContainer style={{ maxHeight: 168 }}>
                  {options.length <= 0 && (emptyComponent ? emptyComponent : <NoOption>No option</NoOption>)}
                  {options.length > 0 &&
                    options.map(opt => {
                      return (
                        <Option
                          className={opt.value === value ? 'selected' : ''}
                          onClick={e => {
                            e.stopPropagation()
                            onValueChange && onValueChange(opt.value)
                            handleClose()
                          }}
                          key={opt.value || 'deselect_option'}>
                          {opt.label ? (
                            typeof opt.label === 'string' ? (
                              <TextLabel>{opt.label}</TextLabel>
                            ) : (
                              opt.label
                            )
                          ) : (
                            <TextLabel>{opt.value}</TextLabel>
                          )}
                        </Option>
                      )
                    })}
                </OptionsContainer>
              )}
            </SelectDropdown>
          </NPopover>
        )}
      </SingleSelectContainer>
      {(error || caption) && (
        <SelectCaption className={classnames([!!error && 'is-error'])}>{error || caption}</SelectCaption>
      )}
    </Wrapper>
  )
}
