import { HTMLAttributes, ReactNode } from 'react'
import { NPopoverPlacement } from '../NPopover'

export type CommonSelectProps = {
  label?: string
  caption?: string
  disabled?: boolean
  required?: boolean
  error?: string
  fullWidth?: boolean
  emptyComponent?: ReactNode
  placement?: NPopoverPlacement
  placeholder?: string
  className?: string
  scrollYOffset?: number
  allowClear?: boolean
} & HTMLAttributes<HTMLDivElement>

export type SelectOption = {
  label?: string | ReactNode
  selectedLabel?: string | ReactNode
  value: string
  isLocal?: boolean
}
