import { FC, HTMLAttributes } from 'react'
import styled from 'styled-components/macro'
import { color, spacing } from '../GlobalStyle'
import { Icons } from '../Icons'
import { typography } from '../NTypography'
import { SelectOption } from './model'

const TagContainer = styled.div`
  background: ${color('Neutral200')};
  border-radius: 3px;
  display: inline-flex;
  align-items: center;
  min-height: 32px;
  padding-left: ${spacing('sm')};
  padding-right: ${spacing('sm')};
  max-width: 100%;
`

const CloseWrapper = styled.div`
  margin-left: ${spacing('sm')};
  cursor: pointer;
  color: ${color('Neutral500')};
  &:hover {
    color: ${color('Neutral900')};
  }
`

const TextLabel = styled.div`
  width: 100%;
  ${typography('button')}
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`

export type NOptionTagProps = {
  option: SelectOption
  closable?: boolean
  onClose?: () => void
  className?: string
} & HTMLAttributes<HTMLDivElement>

export const NOptionTag: FC<NOptionTagProps> = ({ option, closable, onClose, className, ...rest }) => {
  return (
    <TagContainer {...rest} className={className}>
      {option.label ? (
        typeof option.label === 'string' ? (
          <TextLabel>{option.label}</TextLabel>
        ) : (
          option.label
        )
      ) : (
        <TextLabel>{option.value}</TextLabel>
      )}
      {closable && (
        <CloseWrapper
          onClick={() => {
            onClose && onClose()
          }}>
          <Icons.Close8 />
        </CloseWrapper>
      )}
    </TagContainer>
  )
}
