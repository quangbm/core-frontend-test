import { FC } from 'react'
import styled from 'styled-components/macro'
import { spacing } from '../GlobalStyle'
import { NModal } from './NModal'

export type NConfirmModalProps = {
  visible: boolean
  setVisible: (visible: boolean) => void
  title?: string
  onCancel: () => void
  onConfirm: () => void
  isLoading?: boolean
  finishText?: string
}

const StyledBody = styled(NModal.Body)`
  padding: ${spacing('xl')};
`

export const NConfirmModal: FC<NConfirmModalProps> = ({
  children,
  visible,
  setVisible,
  title,
  onCancel,
  onConfirm,
  isLoading,
  finishText = 'Confirm',
}) => {
  return (
    <NModal visible={visible} top="middle" setVisible={setVisible}>
      <NModal.Header title={title || 'Confirmation'} />
      <StyledBody>{children}</StyledBody>
      <NModal.Footer onCancel={onCancel} onFinish={onConfirm} finishText={finishText} isLoading={isLoading} />
    </NModal>
  )
}
