import styled from 'styled-components/macro'
import { color, getAlpha, spacing } from '../GlobalStyle'
import { elevation } from '../NElevation'

export const checkboxClassName = '_selector'
export const sortSymbolClassName = '_sort_symbol'
export const NTableCollapsedCellClassName = '_collapsed'

export const NTableSize = styled.div<{ minHeight: number }>`
  display: flex;
  flex-direction: column;
  position: relative;

  ${elevation('Container100')}
  border-radius: 4px;

  min-height: ${props => spacing(props.minHeight + 32)};
`

const NTableTagContainer = styled.div`
  position: relative;
  display: flex;
  overflow: hidden;
`

const NTableTagBlur = styled.div`
  position: absolute;
  right: 0;
  height: 100%;
  width: 20%;
  background-image: linear-gradient(to right, ${color('white')}${getAlpha(0)}, ${color('white')}${getAlpha(100)});
`

const NTableTag = styled.div`
  background-color: ${color('Neutral200')};
  margin-right: ${spacing('xs')};
  padding: ${spacing('xs')};
  border-radius: 4px;
`

export const NTableTagItem = {
  Container: NTableTagContainer,
  Blur: NTableTagBlur,
  Tag: NTableTag,
}

export const NTableStyledContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  table {
    border-spacing: 0px;
    width: 100%;
    table-layout: fixed;

    thead {
      background-color: ${color('Neutral200')};

      color: ${color('Neutral400')};
      text-transform: uppercase;
      font-size: 12px;
      letter-spacing: 0.15em;

      tr {
        height: 32px;
      }
      th {
        vertical-align: center;
        text-align: left;
        text-overflow: ellipsis;
        padding-left: ${spacing('md')};
        padding-right: ${spacing('md')};

        font-weight: normal;
      }
    }

    tbody {
      tr {
        cursor: pointer;
        height: 40px;

        :hover {
          background-color: ${color('Neutral200')};
          ${NTableTagBlur} {
            background-image: linear-gradient(
              to right,
              ${color('Neutral200')}${getAlpha(0)},
              ${color('Neutral200')}${getAlpha(100)}
            );
          }
          ${NTableTag} {
            background-color: ${color('Neutral300')};
          }
        }
      }

      th,
      td {
        padding-left: ${spacing('md')};
        padding-right: ${spacing('md')};
        border-bottom: 1px solid ${color('Neutral200')};

        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        color: ${color('Neutral700')};
      }
      td a {
        color: inherit;
        display: block;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        &:hover {
          text-decoration: underline;
        }
      }
    }

    .${checkboxClassName} {
      width: 32px;
      padding: 0 8px;
    }
  }

  .${NTableCollapsedCellClassName} {
    width: 56px;
  }
`

export const PaginationWrapper = styled('div')`
  display: flex;
  width: 100%;
  margin-top: ${spacing('xxl')};
  justify-content: center;
`
export const NTableEmptyIcon = styled.svg`
  width: 8px;
  height: 8px;
`

export const NTableLoadingContainer = styled.div<{}>`
  position: absolute;
  bottom: 0;
  display: flex;
  width: 100%;
  height: calc(100% - 32px);
  flex: 1;
  background-color: ${color('white')}${getAlpha(50)};
  justify-content: center;
  align-items: center;
`

export const NTableHeaderCell = styled('div')`
  display: flex;
  align-items: center;

  .${sortSymbolClassName} {
    margin-left: ${spacing('xs')};
    justify-content: space-between;

    path {
      fill: ${color('Neutral400')};
    }
  }
`

export const NTableHeaderText = styled.div`
  flex: 1;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`
