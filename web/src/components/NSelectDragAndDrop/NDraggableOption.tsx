import React, { FC, ReactNode, useRef, useState } from 'react'
import { useDrag, useDrop } from 'react-dnd'
import styled, { css } from 'styled-components/macro'
import { color, spacing } from '../GlobalStyle'
import { GlobalLayoutIcons } from '../Icons'
import { NDivider } from '../NDivider'
import { NColumn } from '../NGrid/NGrid'
import { typography } from '../NTypography'

export const DraggableWrapperSelectedClassName = 'selected-draggable'

export enum OptionsType {
  Available = 'available',
  Selected = 'selected',
}

const Wrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;

  > div {
    width: 100%;
  }

  &.${DraggableWrapperSelectedClassName} {
    background: ${color('Primary200')};
  }
`

const AvailableOptionWrapper = styled.div<{ isDragging: boolean }>`
  display: flex;
  align-items: center;

  padding: ${spacing('sm')};
  ${typography('x-small-ui-text')}
  border-bottom: 1px solid ${color('Neutral200')};
  height: 48px;
  cursor: pointer;

  ${props => {
    const { isDragging } = props
    return css`
      opacity: ${isDragging ? 0.5 : 1};
    `
  }}

  transition: opacity 0.25s;
`

const SelectedOptionWrapper = styled.div<{ isDragging: boolean }>`
  display: flex;
  align-items: center;
  padding: ${spacing('sm')};
  font-size: 14px;
  ${typography('x-small-ui-text')}
  color:${color('Neutral700')};

  cursor: pointer;

  ${props => {
    const { isDragging } = props
    return css`
      opacity: ${isDragging ? 0 : 1};
    `
  }}
`

const PrimaryText = styled.p`
  ${typography('button')}
  color: ${color('Primary700')};
`

const Box = styled.div`
  display: flex;
  width: 24px;
  height: 24px;
  border-radius: 3px;
  background-color: ${color('Primary700')};
  margin-right: ${spacing('xs')};
`

const TopLine = styled.div`
  top: -1px;
  position: absolute;
  height: 2px;
  background-color: ${color('Primary700')};
`
const LabelText = styled.div`
  font-size: 14px;
  margin-left: 8px;
`

export type DndOptionType = {
  label: string
  value: string
  isRequired?: boolean
  icon?: ReactNode
}

export type DraggableItemProps = {
  index: number
} & DndOptionType

type DraggableWrapperProps = {
  className?: string
  type: OptionsType
  data: DndOptionType
  index: number
  insertOption: (option: DndOptionType, index?: number) => void
  onMoveCard: (from: number, to: number) => void
  onClickElement?: (val: string) => void
  isOneColumn?: boolean
}

export const DraggableWrapper: FC<DraggableWrapperProps> = ({
  className,
  data,
  type,
  index,
  onClickElement,
  insertOption,
  onMoveCard,
  isOneColumn,
}) => {
  const ref = useRef<HTMLDivElement>(null)
  const [isInsert, setIsInsert] = useState(false)

  const [{ isDragging }, drag] = useDrag({
    type,
    item: { index, ...data },
    collect: monitor => ({
      isDragging: !!monitor.isDragging(),
    }),
  })

  const [, drop] = useDrop({
    accept: [OptionsType.Available, OptionsType.Selected],
    drop: (item: DraggableItemProps, monitor) => {
      const itemType = monitor.getItemType()
      const { index: _i, ...option } = item

      if (type === OptionsType.Available || itemType === OptionsType.Selected) {
        return
      }
      insertOption(option, index)
    },
    hover: (item: DraggableItemProps, monitor) => {
      const itemType = monitor.getItemType()
      // not hovered if is available option
      if (itemType === OptionsType.Available || type === OptionsType.Available || !ref.current) {
        return
      }

      const dragIndex = item.index
      const hoverIndex = index

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return
      }

      onMoveCard(dragIndex, hoverIndex)
      item.index = hoverIndex
    },
    collect: monitor => {
      const itemType = monitor.getItemType()
      const isOver = monitor.isOver({ shallow: true })
      if (itemType === OptionsType.Available && type === OptionsType.Selected) {
        setIsInsert(isOver)
      }
    },
  })

  drag(drop(ref))
  return (
    <Wrapper className={className} ref={ref}>
      {isOneColumn ? (
        <>
          <SelectedOptionWrapper isDragging={isDragging} onClick={() => onClickElement && onClickElement(data.value)}>
            <NDivider vertical size="xs" />
            {data.icon}
            <LabelText>{data.label}</LabelText>
          </SelectedOptionWrapper>
        </>
      ) : (
        <>
          {isInsert && <TopLine />}
          {type === OptionsType.Available ? (
            <AvailableOptionWrapper isDragging={isDragging}>
              {data.icon || <Box />}
              {data.label}
              <NColumn flex={1} />
              {data.isRequired && <PrimaryText>Required</PrimaryText>}
            </AvailableOptionWrapper>
          ) : (
            <SelectedOptionWrapper isDragging={isDragging}>
              <GlobalLayoutIcons.LayoutOrder />
              <NDivider vertical size="xs" />
              {data.icon || <Box />}
              {data.label}
              <NColumn flex={1} />
              {data.isRequired && <PrimaryText>Required</PrimaryText>}
            </SelectedOptionWrapper>
          )}
        </>
      )}
    </Wrapper>
  )
}
