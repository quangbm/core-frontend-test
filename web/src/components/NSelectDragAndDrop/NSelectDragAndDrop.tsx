import React, { FC, useMemo } from 'react'
import styled, { css } from 'styled-components/macro'
import { classnames } from '../../common/classnames'
import { color, spacing } from '../GlobalStyle'
import { elevation } from '../NElevation'
import { NColumn } from '../NGrid/NGrid'
import { NToast } from '../NToast'
import { typography } from '../NTypography'
import { DndOptionType, DraggableWrapper, DraggableWrapperSelectedClassName, OptionsType } from './NDraggableOption'
import { AvailableOptionsContainer, SelectedOptionsContainer } from './NDroppableZone'

const Container = styled.div<{ isOneColumn?: boolean }>`
  display: flex;
  width: 100%;
  border-radius: 4px;
  ${props => {
    const { isOneColumn } = props
    if (!isOneColumn) {
      return css`
        ${elevation('Container100')}
      `
    }
  }}
`

const LeftContainer = styled(NColumn)`
  flex: 1;
  border-right: 1px solid ${color('Neutral200')};
`

const StyledTitle = styled.p`
  ${typography('overline')}
  padding: ${spacing('sm')};
  border-bottom: 1px solid ${color('Neutral200')};
`

export type NSelectDragAndDropProps = {
  options: DndOptionType[]
  values: DndOptionType[]
  onValuesChange: (values: DndOptionType[]) => void
  title?: string
  onClickElement?: (id: string) => void
  isOneColumn?: boolean
  activeElement?: string
}

export const NSelectDragAndDrop: FC<NSelectDragAndDropProps> = ({
  title,
  options,
  values,
  onValuesChange,
  onClickElement,
  isOneColumn,
  activeElement,
}) => {
  const availableOptions = useMemo<DndOptionType[]>(() => {
    return options.filter(op => {
      const foundVal = values.find(val => val.value === op.value)
      return !foundVal
    })
  }, [options, values])

  const selectValue = (selectedVal: DndOptionType, index?: number) => {
    let newValues: DndOptionType[] = []
    if (typeof index !== 'number') {
      newValues = [...values, selectedVal]
    } else {
      newValues = [...values.slice(0, index), selectedVal, ...values.slice(index)]
    }
    onValuesChange(newValues)
  }

  const deselectValue = (selectedVal: DndOptionType) => {
    if (selectedVal.isRequired) {
      NToast.error({ subtitle: 'Cannot remove required field!' })
      return
    }
    const newValues = values.filter(val => selectedVal.value !== val.value)
    onValuesChange(newValues)
  }

  const moveSelectedOption = (from: number, to: number) => {
    const newValues = values.slice()
    if (!values[from] || !values[to]) {
      return
    }
    const moveValue = values[from]
    newValues[from] = newValues[to]
    newValues[to] = moveValue
    onValuesChange(newValues)
  }

  return (
    <Container isOneColumn>
      {!isOneColumn && (
        <LeftContainer>
          {title && <StyledTitle>{title}</StyledTitle>}
          <AvailableOptionsContainer length={options.length} onDrop={deselectValue}>
            {availableOptions.map((op, index) => (
              <DraggableWrapper
                key={`available_op_${op.value}`}
                index={index}
                type={OptionsType.Available}
                data={op}
                insertOption={selectValue}
                onMoveCard={moveSelectedOption}
              />
            ))}
          </AvailableOptionsContainer>
        </LeftContainer>
      )}
      <SelectedOptionsContainer length={options.length} onDrop={selectValue} noPadding={isOneColumn}>
        {values.map((val, index) => {
          return (
            <DraggableWrapper
              className={classnames([val.value === activeElement && DraggableWrapperSelectedClassName])}
              onClickElement={onClickElement}
              key={`selected_op_${val.value}`}
              index={index}
              type={OptionsType.Selected}
              data={val}
              insertOption={selectValue}
              onMoveCard={moveSelectedOption}
              isOneColumn={isOneColumn}
            />
          )
        })}
      </SelectedOptionsContainer>
    </Container>
  )
}
