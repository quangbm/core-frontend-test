import '@testing-library/jest-dom'
import { cleanup, render } from '@testing-library/react'
import { ThemeProvider } from 'styled-components/macro'
import { theme } from '../GlobalStyle'
import { NTextInput } from './NTextInput'

describe('NTextInput', () => {
  afterEach(cleanup)

  test('NTextInput show label', () => {
    const TestLabel = 'Test Label'

    const { getAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <NTextInput label={TestLabel} />
      </ThemeProvider>,
    )

    const Label = getAllByTestId('NTextInput-Label')
    expect(Label.length === 1)
    expect(Label[0].nodeValue === TestLabel)
  })

  test('NTextInput show caption', () => {
    const TestLabel = 'Test Label'

    const { getAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <NTextInput caption={TestLabel} />
      </ThemeProvider>,
    )

    const Caption = getAllByTestId('NTextInput-Caption')
    expect(Caption.length === 1)
    expect(Caption[0].nodeValue === TestLabel)
  })

  test('NTextInput show error', () => {
    const TestLabel = 'Test Label'
    const TestError = 'Test error'

    const { getAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <NTextInput caption={TestLabel} error={TestError} />
      </ThemeProvider>,
    )

    const Caption = getAllByTestId('NTextInput-Caption')
    expect(Caption.length === 1)
    expect(Caption[0].nodeValue === TestError)
  })

  test('NTextInput render Left', () => {
    const LeftComp = <div data-testid="LeftComp" style={{ width: '10px', height: '10px', backgroundColor: 'red' }} />

    const { getAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <NTextInput left={LeftComp} />
      </ThemeProvider>,
    )

    const Caption = getAllByTestId('LeftComp')
    expect(Caption.length === 1)
  })

  test('NTextInput render right', () => {
    const RightComp = <div data-testid="RightComp" style={{ width: '10px', height: '10px', backgroundColor: 'red' }} />

    const { getAllByTestId } = render(
      <ThemeProvider theme={theme}>
        <NTextInput right={RightComp} />
      </ThemeProvider>,
    )

    const Caption = getAllByTestId('RightComp')
    expect(Caption.length === 1)
  })
})
