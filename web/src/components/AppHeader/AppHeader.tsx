import React, { ReactNode, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useQuery } from 'react-query'
import { useHistory } from 'react-router-dom'
import styled from 'styled-components/macro'
import { useSignOut } from '../../hooks/useAuth'
import { getCurUserFromMe, useMe } from '../../hooks/useMe'
import { useSession } from '../../hooks/useSession'
import { APIServices, QueryKeys } from '../../services/api'
import { color, spacing } from '../GlobalStyle'
import { NAvatar } from '../NAvatar/NAvatar'
import { NButton } from '../NButton/NButton'
import { NDivider } from '../NDivider'
import { NMenuPopover, NMenuPopoverItem } from '../NMenuPopover'
import { NSpinner } from '../NSpinner/NSpinner'
import { typography } from '../NTypography'
import { AppLauncher } from './AppLauncher'
import { ReactComponent as AppDrawerIcon } from './icons/app-drawer.svg'
import { ReactComponent as UnionIcon } from './icons/union-icon.svg'

const Wrapper = styled('div')`
  background-color: ${color('white')};
  box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.05), 0px 1px 2px rgba(0, 0, 0, 0.1);
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};
  z-index: 10;
`

const Row = styled('div')`
  display: flex;
  align-items: center;
  min-height: 56px;
`

const LeftElement = styled('div')`
  flex: 1;
  display: flex;
  align-items: center;
`

const RightElement = styled('div')`
  flex: 1;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin-right: ${spacing('sm')};
`

const MenuIcon = styled(AppDrawerIcon)`
  cursor: pointer;
`

const AppMenu = styled(NMenuPopover)`
  width: 300px;
  padding: ${spacing('md')};
  padding-top: ${spacing('xl')};
  padding-bottom: ${spacing('lg')};
  z-index: 10;
`

const UserMenu = styled(NMenuPopover)`
  width: 300px;
  padding: ${spacing('md')};
  z-index: 10;
`
const AvatarWithPointer = styled(NAvatar)`
  cursor: pointer;
`
const UserInfoWrapper = styled.div`
  height: 56px;
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};
  display: flex;
  align-items: center;
`
const UserInfo = styled.div`
  margin-left: ${spacing('sm')};
`

const ProfileTitle = styled.p`
  ${typography('button')}
  margin-bottom: ${spacing('xxs')};
`
const ProfileName = styled.p`
  ${typography('x-small-ui-text')}
`

const AppMenuHeader = styled.div`
  padding: ${spacing('xs')} ${spacing('md')};
  ${typography('overline')}
  color: ${color('Neutral500')};
`

const CustomMenuItem = styled(NMenuPopoverItem)`
  padding-left: ${spacing('md')};
  padding-right: ${spacing('md')};
  display: flex;
  ${typography('small-bold-text')};
  align-items: center;
  height: 48px;
`

const CenterElement = styled('div')`
  min-width: 40%;
`

const FooterContainer = styled.div`
  display: flex;
  align-items: center;
  padding-left: ${spacing('md')};
`

const CurrentApp = styled.div`
  display: flex;
  margin-left: ${spacing('md')};
  margin-right: ${spacing('md')};
  border-bottom: 1px solid ${color('Neutral200')};
  padding-bottom: ${spacing('sm')};
  margin-bottom: ${spacing('sm')};
`

const CurrentAppName = styled.p`
  ${typography('button')}
  font-weight: bold;
  color: ${color('Neutral700')};
  width: 180px;
`

const CurrentAppDesc = styled.p`
  ${typography('small-ui-text')}
  color: ${color('Neutral400')};
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  width: 180px;
`

const AppItemName = styled.div`
  width: 220px;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`

const APP_LIMIT = 5

type AppHeaderProps = {
  icon?: React.ReactNode
  left?: ReactNode
  right?: ReactNode
} & React.HTMLAttributes<HTMLDivElement>

export function AppHeader({ left, right, children, ...restProps }: AppHeaderProps) {
  const { t } = useTranslation()
  const history = useHistory()
  const [showAppLauncher, setShowAppLauncher] = useState(false)
  const { data: meData } = useMe()

  const [{ data: sessionData }, updateSession] = useSession()
  const { mutate: logout } = useSignOut()

  const lastUsedApp = sessionData?.lastUsedAppId

  const { data: appsData, isLoading } = useQuery(
    [QueryKeys.App.getApps, { limit: APP_LIMIT }],
    APIServices.App.getApps,
    {
      select(res) {
        return res.data
      },
    },
  )

  const { data: appPages } = useQuery([QueryKeys.App.getAppPages, { id: lastUsedApp! }], APIServices.App.getAppPages, {
    enabled: !!lastUsedApp,
    select(response) {
      return response.data
    },
  })

  const selectedApp = appsData?.data.find(app => app.id === sessionData?.lastUsedAppId)
  const currentUser = getCurUserFromMe(meData)

  const selectApp = (appId: string) => {
    if (sessionData?.lastUsedAppId !== appId) {
      updateSession({ lastUsedAppId: appId })
    }
    if (appPages?.length) {
      history.push(`/${appPages[0].id}`)
    }
  }

  return (
    <>
      <Wrapper {...restProps}>
        <Row>
          <LeftElement>
            <AppMenu
              offsetY={12}
              header={
                selectedApp && (
                  <CurrentApp>
                    <NAvatar name={selectedApp.name} size={40} variant="rounded" />
                    <NDivider vertical size="md" />
                    <div>
                      <CurrentAppName>{selectedApp.name}</CurrentAppName>
                      <NDivider size="xxs" />
                      <CurrentAppDesc>{selectedApp.description}</CurrentAppDesc>
                    </div>
                  </CurrentApp>
                )
              }
              disabled={isLoading}
              trigger={(isLoading && <NSpinner size={24} />) || <MenuIcon />}>
              <AppMenuHeader>SWITCH TO</AppMenuHeader>
              {(appsData?.data || []).map(app => {
                return (
                  <CustomMenuItem
                    onClick={() => {
                      selectApp(app.id)
                    }}
                    key={app.id}>
                    <NAvatar name={app.name} size={32} variant="rounded" />
                    <NDivider vertical size="sm" />
                    <AppItemName>{app.name}</AppItemName>
                  </CustomMenuItem>
                )
              })}
              <FooterContainer>
                <UnionIcon />
                <NDivider vertical size="sm" />
                <NButton
                  size="small"
                  type="link"
                  onClick={() => {
                    setShowAppLauncher(true)
                  }}>
                  All apps
                </NButton>
              </FooterContainer>
            </AppMenu>
            {left}
          </LeftElement>
          <CenterElement>
            {/* <NTextInput left={<GlobalIcons.Search />} placeholder="Search Nuclent Core" /> */}
          </CenterElement>
          <RightElement>
            {right}
            <NDivider size="md" vertical />
            <UserMenu
              offsetY={12}
              placement="bottomRight"
              header={
                <UserInfoWrapper>
                  <NAvatar name={currentUser['name'] || ''} variant="circle" size={32} />
                  <UserInfo>
                    <ProfileTitle>{t('dashboard.profile')}</ProfileTitle>
                    <ProfileName>{currentUser['name'] || ''}</ProfileName>
                  </UserInfo>
                </UserInfoWrapper>
              }
              trigger={<AvatarWithPointer name={currentUser['name'] || ''} variant="circle" size={32} />}>
              <NMenuPopoverItem onClick={() => history.push(`/me`)}>{t('dashboard.profile')}</NMenuPopoverItem>
              <NMenuPopoverItem onClick={() => logout()}>{t('dashboard.logout')}</NMenuPopoverItem>
            </UserMenu>
          </RightElement>
        </Row>
        {children}
      </Wrapper>
      {showAppLauncher && (
        <AppLauncher
          showAppLauncher={showAppLauncher}
          setShowAppLauncher={setShowAppLauncher}
          setSelectedApp={selectApp}
        />
      )}
    </>
  )
}
