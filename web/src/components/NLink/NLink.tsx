import * as React from 'react'
import { Link, LinkProps } from 'react-router-dom'

type NLinkProps = {
  to: string
} & LinkProps

export function NLink({ to, ...linkProps }: NLinkProps) {
  const regex = new RegExp('^https?://', 'g')
  if (regex.test(to)) {
    return <a href={to} {...linkProps} />
  }
  return <Link to={to} {...linkProps} />
}
