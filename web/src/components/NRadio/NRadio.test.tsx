import '@testing-library/jest-dom'
import { cleanup, render } from '../../utils/test-utils'
import { NRadio } from './NRadio'

describe('NRadio', () => {
  afterEach(cleanup)

  test('disabled NRadio should be disabled', () => {
    const mockOnChange = jest.fn()

    const { getByTestId } = render(<NRadio disabled {...{ title: 'Op 1', value: 'Op1' }} onChange={mockOnChange} />)
    getByTestId('NRadioContainer').click()
    expect(mockOnChange.mock.calls.length).toBe(0)
  })
})
