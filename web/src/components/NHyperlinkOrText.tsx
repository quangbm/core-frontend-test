import { FC, HTMLAttributes } from 'react'

type Props = HTMLAttributes<HTMLAnchorElement>

export const NHyperlinkOrText: FC<Props> = ({ children, onClick, ...restProps }) => {
  const hyperlinkRegex = new RegExp('^https?://', 'g')
  if (typeof children === 'string' && hyperlinkRegex.test(children)) {
    return (
      <a
        href={children}
        onClick={e => {
          e.stopPropagation()
          if (onClick) {
            onClick(e)
          }
        }}
        {...restProps}
        target="_blank"
        rel="noopener noreferrer">
        {children}
      </a>
    )
  }

  return (
    <span onClick={onClick} {...restProps}>
      {children}
    </span>
  )
}
