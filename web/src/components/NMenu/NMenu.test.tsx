import '@testing-library/jest-dom'
import { cleanup, render } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'
import { ThemeProvider } from 'styled-components/macro'
import { GlobalStyle, theme } from '../GlobalStyle'
import { NMenu } from './NMenu'

describe('NMenu', () => {
  afterEach(cleanup)
  test('render', () => {
    const values = ['1', '2', '3']

    const { getAllByTestId } = render(
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <GlobalStyle />
          <NMenu>
            {values.map(v => (
              <NMenu.Item key={`menu_item_${v}`} to={`/${v}`}>
                Action 1
              </NMenu.Item>
            ))}
          </NMenu>
        </ThemeProvider>
      </BrowserRouter>,
    )

    const MenuItems = getAllByTestId('MenuItem')
    expect(MenuItems).toHaveLength(3)
  })
})
