import * as React from 'react'
import styled from 'styled-components/macro'
import { color, spacing } from '../GlobalStyle'
import { Icons } from '../Icons'

const Wrapper = styled('div')`
  display: flex;
  flex-direction: column;
  position: relative;
  &:not(:first-child) {
    border-top: 1px solid ${color('Neutral200')};
  }
`

const IconWrapper = styled('div')`
  transition: transform 0.3s ease-in-out;
  transform: rotate(0deg);

  &.is-show-left {
    transform: rotate(90deg);
  }
  &.is-show-right {
    transform: rotate(90deg);
  }
`

const Header = styled('div')`
  display: flex;
  align-items: center;
  padding: ${spacing('sm')} 0;
  cursor: pointer;
`

const Content = styled.div<{ isShow?: boolean }>`
  position: relative;
  width: 100%;
  display: ${props => (props.isShow ? 'block' : 'none')};
  padding-bottom: ${spacing('xs')};
`

export type NAccordionProps = {
  className?: string
  title?: React.ReactNode
  children: React.ReactNode
  icon?: React.ReactNode
  iconPosition?: 'left' | 'right'
  initialOpen?: boolean
}

export const NAccordion = React.memo(function Node({
  icon = <Icons.FilledLeft />,
  iconPosition = 'left',
  children,
  title,
  className,
  initialOpen = true,
}: NAccordionProps) {
  const [show, setShow] = React.useState(initialOpen)
  return (
    <Wrapper
      onClick={e => {
        e.stopPropagation()
      }}
      className={className}>
      <Header
        onClick={e => {
          e.stopPropagation()
          setShow(prev => !prev)
        }}>
        {iconPosition === 'left' && <IconWrapper className={show ? 'is-show-left' : undefined}>{icon}</IconWrapper>}
        {title}
        {iconPosition === 'right' && <IconWrapper className={show ? 'is-show-right' : undefined}>{icon}</IconWrapper>}
      </Header>
      <Content isShow={show}>{children}</Content>
    </Wrapper>
  )
})

NAccordion.displayName = 'NAccordion'
