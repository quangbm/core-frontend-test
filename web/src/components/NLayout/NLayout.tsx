import React, { CSSProperties, FC } from 'react'
import styled from 'styled-components/macro'

type NLayoutBaseProps = {
  isRow?: boolean
}
const NLayoutBase = styled.div<NLayoutBaseProps>`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: ${props => (props.isRow ? 'row' : 'column')};
  overflow: hidden;
`

export type NLayoutProps = {
  isRow?: boolean
  className?: string
  style?: CSSProperties
}

export const NLayout: FC<NLayoutProps> = ({ isRow, className, style, children }) => {
  return (
    <NLayoutBase className={className} style={style} isRow={isRow}>
      {children}
    </NLayoutBase>
  )
}
