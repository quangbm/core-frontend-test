import React, { FC } from 'react'
import styled from 'styled-components/macro'
import { color, spacing } from '../GlobalStyle'
import { NDivider } from '../NDivider'
import { elevation } from '../NElevation'
import { NTypography, typography } from '../NTypography'
import { NFileType } from './NFileType'

const FileInfo = styled.div`
  position: absolute;
  bottom: 0;
  padding: ${spacing('md')};
  width: 100%;
  opacity: 0;

  transition: opacity 0.5s;
  background-color: ${color('white')};
`

const FileWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  min-width: 100px;
  width: auto;
  border-radius: 6px;
  margin: 0 ${spacing('sm')};
  justify-content: center;
  align-items: center;
  ${elevation('Container100')};

  &:hover {
    ${FileInfo} {
      opacity: 1;
    }
  }
`

const Img = styled.img`
  height: 200px;
  width: auto;
`
const FilePlaceholder = styled.div`
  display: flex;
  height: 200px;
  color: ${color('Neutral300')};
  justify-content: center;
  align-items: center;
  ${typography('small-bold-text')}
`

const FileName = styled.div`
  ${typography('x-small-bold-text')};
`

type Props = {
  name: string
  media?: NFileType
  review?: boolean
}
export const NFile: FC<Props> = ({ name, media }) => {
  const imageTypes = ['image/png', 'image/jpeg', 'image/gif']

  if (!media) {
    return (
      <FileWrapper>
        <NDivider size="sm" />
        <NTypography.InputLabel>{name}</NTypography.InputLabel>
        <FilePlaceholder>Missing file</FilePlaceholder>
      </FileWrapper>
    )
  }

  return (
    <FileWrapper>
      <NDivider size="sm" />
      <NTypography.InputLabel>{media.tagName || name}</NTypography.InputLabel>
      {imageTypes.includes(media.mimeType) && media.url ? (
        <Img src={media.url} alt="" />
      ) : (
        <FilePlaceholder>{media.mimeType}</FilePlaceholder>
      )}
      <FileInfo>
        <FileName>{media.name}</FileName>
      </FileInfo>
    </FileWrapper>
  )
}
