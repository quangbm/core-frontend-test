import * as React from 'react'
import styled from 'styled-components/macro'
import { classnames } from '../../common/classnames'
import { color } from '../GlobalStyle'
import { elevation } from '../NElevation'
import { NPerfectScrollbar } from '../NPerfectScrollbar'
import { Caption, Label } from '../NTextInput/NTextInput'
import { RequiredIndicator } from '../NTypography'
import { Portal } from '../Portal'
import { syntaxColor } from './syntax-color'
import {
  ARROW_CODES,
  CaretXY,
  COMPLETE_CODES,
  createTrigger,
  EXIT_CODES,
  getCaretXY,
  htmlEncode,
  processHTML,
  SELECT_CODES,
} from './utils'

const Wrapper = styled('div')`
  overflow-y: auto;
  overflow-x: hidden;
  transition: border-color 0.3s ease-in-out;
  border-radius: 4px;
  border: 1px solid ${color('Neutral200')};
  &:hover {
    border-color: ${color('Neutral400')};
  }
  &:focus-within {
    border-color: ${color('Primary700')};
  }
  &.is-error {
    border-color: ${color('Red700')};
    & ~ ${Caption} {
      color: ${color('Red700')};
    }
  }
  &.is-disabled {
    cursor: not-allowed;
    background: ${color('Neutral200')};
    &:hover {
      border-color: ${color('Neutral200')};
    }
  }
`

const CodeWrapper = styled('div')`
  min-height: 100%;
  position: relative;
  padding: 0;
  overflow: hidden;
  font-family: 'Space Mono', ui-monospace, Consolas, Menlo, monospace;
  /* background-color: ${color('Neutral200')}; */

  pre {
    margin: 0;
    padding: 0;
    white-space: inherit;
    font-family: inherit;
    font-size: inherit;
    code {
      font-family: inherit;
    }
  }

  textarea {
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    resize: none;
    color: inherit;
    overflow: hidden;
    -moz-font-smoothing: grayscale;
    -webkit-font-smoothing: antialiased;
    -webkit-text-fill-color: transparent;
  }

  .editor {
    margin: 0;
    border: 0;
    padding: 14px;
    background: transparent;
    box-sizing: inherit;
    display: inherit;
    font-family: inherit;
    font-size: inherit;
    font-style: inherit;
    font-variant-ligatures: none;
    font-weight: inherit;
    letter-spacing: inherit;
    line-height: inherit;
    tab-size: inherit;
    text-indent: inherit;
    text-rendering: inherit;
    text-transform: inherit;
    white-space: pre-wrap;
    word-break: keep-all;
    overflow-wrap: break-word;
    outline: 0;
  }

  ${syntaxColor}
`

const DropdownWrapper = styled('div')`
  background: ${color('white')};
  border-radius: 4px;
  overflow: hidden;
  position: fixed;
  top: 0;
  left: 0;
  ${elevation('Elevation200')}
`

type NCodeEditorProps = {
  required?: boolean
  label?: string
  caption?: string
  error?: string
  trigger?: string
  replacer?(variable: string): string
  value?: string
  children?: React.ReactNode
  language?: 'javascript' | 'markdown' | 'css' | 'json' | 'html'
  onSearchChange?(query: string): void
} & Pick<
  React.TextareaHTMLAttributes<HTMLTextAreaElement>,
  'onChange' | 'onKeyDown' | 'onKeyUp' | 'disabled' | 'autoFocus'
> &
  Omit<React.HTMLAttributes<HTMLDivElement>, 'onKeyDown' | 'onKeyUp' | 'onChange'>

type VariableQuery = {
  replace: string
  query: string
  extra: string
  start: number
  end: number
}

export const NCodeEditor = React.memo<NCodeEditorProps>(
  React.forwardRef(function Node(props, ref) {
    const {
      required,
      label,
      caption,
      error,
      trigger,
      replacer = (a: string) => a,
      language = 'javascript',
      children: propsChildren,
      onSearchChange,
      // input props
      disabled,
      value,
      onChange,
      onKeyDown,
      onKeyUp,
      autoFocus,
      // restprops
      ...divProps
    } = props

    const triggerRegex = React.useMemo(() => {
      if (!trigger) {
        return undefined
      }
      return createTrigger(trigger)
    }, [trigger])
    const replacerCallbackRef = React.useRef(replacer)
    replacerCallbackRef.current = replacer

    const inputRef = React.useRef<HTMLTextAreaElement>(null)

    const [caret, setCaret] = React.useState<CaretXY | undefined>()
    const [query, setQuery] = React.useState<VariableQuery | undefined>()
    const [focusIndex, setFocusIndex] = React.useState(0)

    const handleSelectSuggestion = React.useCallback(
      (selectedVariable = '') => {
        const element = inputRef.current
        if (query && element) {
          const variable =
            (replacerCallbackRef.current && replacerCallbackRef.current(selectedVariable)) || selectedVariable
          element.setRangeText(`${variable}${query.extra}`, query.start, query.end, 'end')
          element.dispatchEvent(new Event('change', { bubbles: true, cancelable: false }))
          element.focus()
          setQuery(undefined)
          setCaret(undefined)
          setFocusIndex(0)
        }
      },
      [query],
    )

    const handleCompleteSuggestion = (selectedVariable = '') => {
      const element = inputRef.current
      if (query && element) {
        element.setRangeText(`${selectedVariable}.`, query.start + (trigger || '').length, query.end, 'end')
        element.dispatchEvent(new Event('change', { bubbles: true, cancelable: false }))
        element.dispatchEvent(new KeyboardEvent('keyup', { key: '.' }))
        element.focus()
        setFocusIndex(0)
      }
    }

    const { options, children } = React.useMemo(() => {
      const options = [] as Array<string>
      const children = [] as Array<JSX.Element>
      for (const [childIndex, child] of React.Children.toArray(propsChildren).entries()) {
        if (React.isValidElement(child)) {
          options.push(child.props.value)
          children.push(
            React.cloneElement(child, {
              className: classnames([child.props.className, childIndex === focusIndex && 'focused']),
              onClick(e: React.MouseEvent<HTMLDivElement>) {
                e.stopPropagation()
                e.preventDefault()
                const props = (child as any).props as any
                if (props.onClick) {
                  props.onClick(e)
                }
                handleSelectSuggestion(props.value)
              },
            }),
          )
        }
      }
      return {
        options,
        children,
      }
    }, [handleSelectSuggestion, propsChildren, focusIndex])

    const handleKeyDown = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
      if (onKeyDown) {
        onKeyDown(e)
      }
      if (!triggerRegex) {
        return
      }
      if ([SELECT_CODES, COMPLETE_CODES, ...ARROW_CODES].includes(e.code) && caret) {
        e.stopPropagation()
        e.preventDefault()
      }
    }

    const handleKeyUp = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
      if (onKeyUp) {
        onKeyUp(e)
      }
      if (!triggerRegex) {
        return
      }
      const element = e.currentTarget
      if (ARROW_CODES.includes(e.code) && caret) {
        e.stopPropagation()
        e.preventDefault()
        if (e.code === ARROW_CODES[0]) {
          setFocusIndex(prev => (prev + 1 + options.length) % options.length)
        } else {
          setFocusIndex(prev => (prev - 1 + options.length) % options.length)
        }
      }

      if (COMPLETE_CODES === e.code && caret) {
        e.stopPropagation()
        e.preventDefault()
        if (children) {
          handleCompleteSuggestion(options[focusIndex])
        }
      }

      if (SELECT_CODES === e.code && caret) {
        e.stopPropagation()
        e.preventDefault()
        if (children) {
          handleSelectSuggestion(options[focusIndex])
        }
      }

      const matches = Array.from(
        element.value.substring(0, element.selectionStart || 0).matchAll(triggerRegex),
        match => ({
          replace: match[0] || '',
          query: match[1] || '',
          extra: match[2] || '',
          start: match.index || 0,
          end: (match.index || 0) + match[0].length,
        }),
      )

      if (!EXIT_CODES.includes(e.code) && matches.length > 0) {
        setQuery(matches[0])
        if (onSearchChange) {
          onSearchChange(matches[0].query)
        }
        setCaret(getCaretXY(element))
      } else {
        setQuery(undefined)
        setCaret(undefined)
      }
    }

    React.useImperativeHandle(ref, () => inputRef.current)

    const classname = classnames([divProps?.className, disabled && 'is-disabled', !!error && 'is-error'])

    return (
      <React.Fragment>
        {label && (
          <Label>
            {required && <RequiredIndicator>*</RequiredIndicator>}
            {label}
          </Label>
        )}
        <Wrapper {...divProps} className={classname}>
          <CodeWrapper>
            <textarea
              className="editor"
              value={value}
              onChange={onChange}
              onKeyDown={handleKeyDown}
              onKeyUp={handleKeyUp}
              ref={inputRef}
              disabled={disabled}
              autoFocus={autoFocus}
            />
            <div
              className="editor"
              dangerouslySetInnerHTML={{
                __html: processHTML(
                  `<pre aria-hidden=true><code class="language-${language}">${htmlEncode(
                    String(value || ' '),
                  )} </code></pre>`,
                ),
              }}
            />
          </CodeWrapper>
        </Wrapper>
        {(error || caption) && <Caption>{error || caption}</Caption>}
        {caret && (
          <Portal>
            <DropdownWrapper
              style={{
                zIndex: 10000,
                transform: `translate(${caret?.x || 0}px, calc(${caret?.y || 0}px + ${caret?.height || ''}))`,
              }}>
              <NPerfectScrollbar style={{ maxHeight: 200 }}>{children}</NPerfectScrollbar>
            </DropdownWrapper>
          </Portal>
        )}
      </React.Fragment>
    )
  }),
)

NCodeEditor.displayName = 'NCodeEditor'
