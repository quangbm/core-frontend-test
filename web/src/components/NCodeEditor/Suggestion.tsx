import * as React from 'react'
import styled from 'styled-components/macro'
import { color, spacing } from '../GlobalStyle'

const Wrapper = styled('div')`
  padding: ${spacing('xs')};
  cursor: pointer;
  &:not(:last-child) {
    margin-bottom: 1px;
  }
  &:hover,
  &.focused {
    background: ${color('Primary100')};
  }
`

export type SuggestionProps = {
  value: string
  children?: React.ReactNode
} & React.HTMLAttributes<HTMLDivElement>

export const Suggestion = React.memo(function Node(props: SuggestionProps) {
  return <Wrapper {...props} />
})

Suggestion.displayName = 'Suggestion'
