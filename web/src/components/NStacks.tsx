import styled from 'styled-components/macro'
import { spacing, theme } from './GlobalStyle'

type NStackProps = {
  size?: keyof typeof theme.spacing | number
}

export const VStack = styled.div<NStackProps>`
  display: flex;
  flex-direction: column;
  > *:not(:last-child) {
    margin-bottom: ${({ size = 'sm' }) => spacing(size)};
  }
`

export const HStack = styled.div<NStackProps>`
  display: flex;
  > *:not(:last-child) {
    margin-right: ${({ size = 'sm' }) => spacing(size)};
  }
`
