import { FC } from 'react'
import { useQuery } from 'react-query'
import { useSearchText } from '../../hooks/useSearchText'
import { APIServices, QueryKeys } from '../../services/api'
import { SelectOption } from '../NSelect/model'
import { NSingleSelect } from '../NSelect/NSingleSelect'

export type NCurrencyPickerProps = {
  currencyCode?: string
  onChange: (code?: string) => void
  label?: string
  caption?: string
  error?: string
}

export const NCurrencyPicker: FC<NCurrencyPickerProps> = ({ currencyCode, onChange, label, caption, error }) => {
  const [{ searchText, searchValue }, onChangeSearch] = useSearchText()

  const { data: currenciesData, isLoading: isLoadingCurrencies } = useQuery(
    [QueryKeys.Currencies.getCurrenciesList, { searchText }],
    APIServices.Currencies.getCurrenciesList,
    {
      keepPreviousData: true,
      select: response =>
        response.data.data.map(
          currency => ({ label: currency.displayName, value: currency.currencyCode } as SelectOption),
        ),
    },
  )

  return (
    <NSingleSelect
      fullWidth
      isSearchable
      onSearchValueChange={onChangeSearch}
      searchValue={searchValue}
      options={currenciesData || []}
      value={currencyCode}
      onValueChange={onChange}
      label={label}
      caption={caption}
      error={error}
      isLoading={isLoadingCurrencies}
    />
  )
}
