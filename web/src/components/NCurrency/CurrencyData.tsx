import * as currency from '@dinero.js/currencies'
import { dinero } from 'dinero.js'
import { FC } from 'react'
import { SessionResponse } from '../../../../shared/src/api'
import { useSession } from '../../hooks/useSession'
import { createIntlFormatter } from '../../utils/currency-utils'

type Props = {
  dataCurrencyCode: string
  value: number
}

export const CurrencyData: FC<Props> = ({ dataCurrencyCode, value }) => {
  const [{ data: { defaultLocale } = {} as SessionResponse }] = useSession()

  if (value) {
    //@ts-ignore index type
    const dineroCurrency = currency[dataCurrencyCode]
    const dineroValue = dinero({ amount: value, currency: dineroCurrency })

    const intlFormat = createIntlFormatter(defaultLocale)

    return <>{intlFormat(dineroValue)}</>
  }
  return null
}
