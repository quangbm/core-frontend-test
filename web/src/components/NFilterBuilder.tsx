import { cloneElement, isValidElement, useEffect, useState } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import styled from 'styled-components/macro'
import { FieldSchema } from '../../../shared/src/api'
import { APIServices, QueryKeys } from '../services/api'
import { spacing } from './GlobalStyle'
import { NButton } from './NButton/NButton'
import {
  FilterItem,
  Filters,
  NConditionBuilder,
  transformToFormValue,
  transformToPropsValue,
} from './NConditionBuilder'
import { NModal } from './NModal/NModal'
import { NPerfectScrollbar } from './NPerfectScrollbar'
import { SelectOption } from './NSelect/model'

const ModalBody = styled(NModal.Body)`
  max-height: 50vh;
`

const Wrapper = styled('div')`
  padding: ${spacing('xl')};
`

const DEFAULT_FILTERS = [{ rules: [{ fieldName: '' } as FilterItem] }] as Filters[]

type NFilterBuilderProps = {
  children: React.ReactNode
  filters?: FilterItem[][]
  onSubmit?(filters?: FilterItem[][]): void
  onCancel?(): void
  fieldsByName?: Record<string, FieldSchema>
  fieldNameOptions?: SelectOption[]
}

export function NFilterBuilder({
  children,
  filters,
  fieldsByName = {},
  fieldNameOptions = [],
  onSubmit,
  onCancel,
}: NFilterBuilderProps) {
  const [show, setShow] = useState(false)
  const formMethods = useForm({
    defaultValues: {
      filters: (filters && transformToFormValue(filters)) || DEFAULT_FILTERS,
    },
  })

  useEffect(() => {
    formMethods.reset({
      filters: (filters && transformToFormValue(filters)) || DEFAULT_FILTERS,
    })
  }, [filters, formMethods])

  const { data: operators = {}, isLoading } = useQuery(
    [QueryKeys.Builder.getFilterOperators],
    APIServices.Builder.getFilterOperators,
    {
      select(res) {
        return Object.keys(res.data).reduce((a, k) => {
          return {
            ...a,
            [k]: res.data[k].map(i => ({ value: i.value, label: i.displayName })),
          }
        }, {} as Record<string, SelectOption[]>)
      },
    },
  )

  const handleClose = () => {
    if (onCancel) {
      onCancel()
    }
    formMethods.reset({ filters: (filters && transformToFormValue(filters)) || DEFAULT_FILTERS })
    setShow(false)
  }

  const action = isValidElement(children) ? (
    cloneElement(children, {
      onClick() {
        setShow(true)
      },
    })
  ) : (
    <NButton onClick={() => setShow(true)}>{children}</NButton>
  )
  return (
    <>
      {action}
      <NModal size="large" visible={show} setVisible={setShow}>
        <NModal.Header onClose={handleClose} title="Filters" />
        <NPerfectScrollbar>
          <ModalBody>
            <Wrapper>
              <FormProvider {...formMethods}>
                <NConditionBuilder
                  name="filters"
                  fieldNames={fieldNameOptions}
                  fields={fieldsByName}
                  operators={operators}
                  isOperatorsLoading={isLoading}
                />
              </FormProvider>
            </Wrapper>
          </ModalBody>
        </NPerfectScrollbar>
        <NModal.Footer
          onCancel={handleClose}
          onFinish={formMethods.handleSubmit(v => {
            if (onSubmit) {
              onSubmit(transformToPropsValue(v.filters))
            }
            setShow(false)
          })}
        />
      </NModal>
    </>
  )
}
