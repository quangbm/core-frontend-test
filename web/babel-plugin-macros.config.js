module.exports = {
  styledComponents: {
    pure: true,
    displayName: process.env.NODE_ENV === 'development',
  },
}
