const path = require('path')

const customPackages = [path.resolve(__dirname, '../shared/src')]

module.exports = {
  plugins: [
    {
      plugin: {
        overrideWebpackConfig: ({ webpackConfig }) => {
          // add custom paths to ModuleScopePlugin
          webpackConfig.resolve.plugins.forEach(plugin => {
            if (plugin.constructor && plugin.constructor.name === 'ModuleScopePlugin') {
              plugin.appSrcs = [...plugin.appSrcs, ...customPackages]
            }
          })
          // support typescript
          const oneOfRule = webpackConfig.module.rules.find(rule => rule.oneOf)
          if (oneOfRule) {
            const tsxRule = oneOfRule.oneOf.find(rule => rule.test && rule.test.toString().includes('tsx'))

            if (tsxRule) {
              tsxRule.include = Array.isArray(tsxRule.include)
                ? [...tsxRule.include, ...customPackages]
                : [tsxRule.include, ...customPackages]
            }
          }
          return webpackConfig
        },
      },
    },
  ],
}
