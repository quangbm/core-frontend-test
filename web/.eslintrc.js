const based = require('../.eslintrc')

module.exports = {
  ...based,
  extends: [...based.extends, 'react-app', 'react-app/jest', 'plugin:react-hooks/recommended'],
  rules: {
    ...based.rules,

    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',

    'no-restricted-imports': [
      'warn',
      {
        paths: [
          {
            name: 'styled-components',
            message: 'Please import from styled-components/macro.',
          },
        ],
        patterns: ['!styled-components/macro'],
      },
    ],
  },
}
