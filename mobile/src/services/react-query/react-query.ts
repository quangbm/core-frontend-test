import { QueryCache, QueryClient } from 'react-query'

export const cacheTimes = {
  Minute: 1000 * 60,
  Hour: 1000 * 60 * 60,
  Day: 1000 * 60 * 60 * 24,
}
export const queryCache = new QueryCache()
export const queryClient = new QueryClient({
  queryCache,
  defaultOptions: {
    queries: {
      staleTime: cacheTimes.Minute,
      cacheTime: cacheTimes.Minute * 5,
    },
  },
})
