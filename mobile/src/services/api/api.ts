import AsyncStorage from '@react-native-async-storage/async-storage'
import { createApi, createRequest } from '../../../../shared/src/api'
import { API_HOST } from '../../env'
import { ACCESS_TOKEN_KEY } from '../../hooks/useAuth'
import { queryClient } from '../react-query'

const { request, setToken, clearToken } = createRequest(API_HOST, () => {
  AsyncStorage.removeItem(ACCESS_TOKEN_KEY)
  queryClient.invalidateQueries()
})

const { APIServices, QueryKeys } = createApi(request)

export { setToken, request, clearToken, APIServices, QueryKeys }
