import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import { en } from './locales/en'
import { vi } from './locales/vi'

export const SUPPORTED_LANGUAGES = ['en', 'vi']

const resources = {
  en,
  vi,
}

i18n.use(initReactI18next).init({ resources, fallbackLng: 'en', react: { useSuspense: false } })

export default i18n
