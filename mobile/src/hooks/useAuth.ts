import AsyncStorage from '@react-native-async-storage/async-storage'
import { useMutation, useQuery, useQueryClient } from 'react-query'
import { APIServices, clearToken, setToken } from '../services/api'
import { cacheTimes } from '../services/react-query'

export const ACCESS_TOKEN_KEY = '@nCore/ACCESS_TOKEN'

export function useAccessToken() {
  return useQuery([ACCESS_TOKEN_KEY], () => AsyncStorage.getItem(ACCESS_TOKEN_KEY), {
    onSuccess(data) {
      if (data) {
        setToken(data)
      }
    },
    cacheTime: cacheTimes.Day * 2,
  })
}

export function useSignIn() {
  const queryClient = useQueryClient()
  return useMutation(APIServices.Auth.login, {
    async onSuccess(response) {
      await AsyncStorage.setItem(ACCESS_TOKEN_KEY, response.data.accessToken)
      queryClient.invalidateQueries([ACCESS_TOKEN_KEY])
    },
  })
}

export function useSignOut() {
  const queryClient = useQueryClient()
  return useMutation(APIServices.Auth.logout, {
    async onSettled() {
      clearToken()
      await AsyncStorage.removeItem(ACCESS_TOKEN_KEY)
      queryClient.invalidateQueries()
    },
  })
}
