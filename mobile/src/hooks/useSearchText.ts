import debounce from 'lodash/debounce'
import * as React from 'react'

export function useSearchText(
  initialValue?: string,
  timeout = 300,
): [{ searchValue: string | undefined; searchText: string | undefined }, (newValue: string) => void] {
  const [searchValue, setSearchValue] = React.useState<string | undefined>(initialValue)
  const [searchText, setSearchText] = React.useState<string | undefined>(initialValue)

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleChangeSearchText = React.useCallback(debounce(setSearchText, timeout), [timeout])

  const handleChange = React.useCallback(
    (newValue: string) => {
      setSearchValue(newValue)
      handleChangeSearchText(newValue)
    },
    [handleChangeSearchText],
  )

  return [{ searchValue, searchText }, handleChange]
}
