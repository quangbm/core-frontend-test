import debounce from 'lodash/debounce'
import * as React from 'react'
import { SortOrder, SortParams } from '../../../shared/src/api'

export function useQueryParams({ limit = 10, timeout = 300 }: { limit?: number; timeout?: number } = {}): [
  {
    page: number
    limit: number
    offset: number
    searchText?: string
    searchValue?: string
    sortBy?: string
    sortOrder?: SortOrder
  },
  { onChangeSort(params?: SortParams): void; onChangePage(page: number): void; onChangeSearch(keyword: string): void },
] {
  const [sortParams, onChangeSort] = React.useState<SortParams | undefined>()
  const [page, onChangePage] = React.useState(1)
  const [searchValue, setSearchValue] = React.useState<string | undefined>()
  const [searchText, setSearchText] = React.useState<string | undefined>()

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const handleChangeSearchText = React.useCallback(
    debounce((keyword: string) => {
      setSearchText(keyword)
      onChangePage(prev => {
        if (prev > 1) {
          return 1
        }
        return prev
      })
    }, timeout),
    [timeout],
  )

  const onChangeSearch = React.useCallback(
    (newValue: string) => {
      setSearchValue(newValue)
      handleChangeSearchText(newValue)
    },
    [handleChangeSearchText],
  )

  return [
    {
      page,
      limit,
      offset: (page - 1) * limit,
      searchText,
      searchValue,
      ...(sortParams || {}),
    },
    {
      onChangeSearch,
      onChangeSort,
      onChangePage,
    },
  ]
}
