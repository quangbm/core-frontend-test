import { useIsFocused } from '@react-navigation/native'
import { AxiosResponse } from 'axios'
import { useEffect } from 'react'
import { useMutation, useQuery, useQueryClient, UseQueryOptions } from 'react-query'
import { ErrorResponse, SessionResponse } from '../../../shared/src/api'
import { APIServices, QueryKeys } from '../services/api'
import { cacheTimes } from '../services/react-query'

export function useSession(options?: UseQueryOptions<AxiosResponse<SessionResponse>, ErrorResponse, SessionResponse>) {
  const queryClient = useQueryClient()
  const state = useQuery([QueryKeys.Sessions.getSession], APIServices.Sessions.getSession, {
    ...options,
    cacheTime: cacheTimes.Day,
    staleTime: cacheTimes.Hour,
    select(response) {
      return response.data
    },
  })
  const { mutate } = useMutation(APIServices.Sessions.updateSession, {
    onMutate(dto) {
      queryClient.cancelQueries(QueryKeys.Sessions.getSession)

      const previousSession = queryClient.getQueryData(QueryKeys.Sessions.getSession)
      queryClient.setQueryData<SessionResponse>(QueryKeys.Sessions.getSession, old => {
        return {
          ...old!,
          ...dto,
        }
      })
      return previousSession
    },
    onError(_, __, previousSession) {
      queryClient.setQueryData(QueryKeys.Sessions.getSession, previousSession)
    },
    onSuccess(response) {
      queryClient.setQueryData(QueryKeys.Sessions.getSession, response)
    },
  })
  return [state, mutate] as [typeof state, typeof mutate]
}

export function useUpdateSession(options: { appId?: string; currentUrl?: string } = {}) {
  const isFocused = useIsFocused()
  const [{ data: sessionData }, updateSession] = useSession()

  const appId = options.appId || sessionData?.lastUsedAppId
  const url = options.currentUrl || sessionData?.lastUrl

  useEffect(() => {
    if (!isFocused) {
      return
    }
    if (appId !== sessionData?.lastUsedAppId || url !== sessionData?.lastUrl) {
      if (__DEV__) {
        console.info('New session', {
          appId,
          url,
        })
      }
      updateSession({
        lastUsedAppId: appId,
        lastUrl: url,
      })
    }
  }, [appId, url, updateSession, isFocused, sessionData?.lastUsedAppId, sessionData?.lastUrl])
}
