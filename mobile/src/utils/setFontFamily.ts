import { Text, TextInput, TextProps } from 'react-native'
import { colors } from '../components/GlobalStyles'

export function setFontFamily(fontFamily: string) {
  // @ts-ignore
  const TextRender = Text.render
  // @ts-ignore
  Text.render = function render(props: TextProps, ...args) {
    const oldProps = props
    props = { ...props, style: [{ fontFamily, color: colors.Neutral900 }, props.style] }
    try {
      return TextRender.apply(this, args)
    } finally {
      props = oldProps
    }
  }
  // @ts-ignore
  const TextInputRender = TextInput.render
  // @ts-ignore
  TextInput.render = function render(props: TextProps, ...args) {
    const oldProps = props
    props = { ...props, style: [{ fontFamily, color: colors.Neutral900 }, props.style] }
    try {
      return TextInputRender.apply(this, args)
    } finally {
      props = oldProps
    }
  }
}
