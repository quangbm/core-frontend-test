import { shouldPolyfill as intl } from '@formatjs/intl-getcanonicallocales/should-polyfill'
import { shouldPolyfill as locale } from '@formatjs/intl-locale/should-polyfill'
import { shouldPolyfill as nf } from '@formatjs/intl-numberformat/should-polyfill'
import { shouldPolyfill as plural } from '@formatjs/intl-pluralrules/should-polyfill'

if (intl()) {
  require('@formatjs/intl-getcanonicallocales/polyfill')
}

if (locale()) {
  require('@formatjs/intl-locale/polyfill')
}

if (nf()) {
  require('@formatjs/intl-numberformat/polyfill')
  require('@formatjs/intl-numberformat/locale-data/vi')
  require('@formatjs/intl-numberformat/locale-data/en')
}

if (plural()) {
  require('@formatjs/intl-pluralrules/polyfill')
  require('@formatjs/intl-pluralrules/locale-data/vi')
  require('@formatjs/intl-pluralrules/locale-data/en')
}
