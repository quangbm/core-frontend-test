export const colors = {
  white: '#FFFFFF',
  transparent: 'transparent',
  black: 'black',

  Neutral100: '#F9F9FA',
  Neutral200: '#EDEEF0',
  Neutral300: '#CED3D8',
  Neutral400: '#A3ABB5',
  Neutral500: '#828B94',
  Neutral700: '#525C66',
  Neutral900: '#0D1826',

  Primary100: '#F0F9F8',
  Primary200: '#E0F1F0',
  Primary300: '#B2DCD9',
  Primary400: '#84C8C3',
  Primary700: '#66BBB5',
  Primary900: '#35938B',

  Secondary900: '#408DA2',

  Tertiary900: '#233655',

  warning: '#FF9202',
  danger: '#E8442F',

  Red700: '#EC6140',
}

export const spacings = {
  none: 0 as const,
  xs: 4 as const,
  sm: 8 as const,
  md: 16 as const,
  lg: 24 as const,
  xl: 32 as const,
}

export const borderRadius = {
  none: 0 as const,
  sm: 2 as const,
  md: 4 as const,
  lg: 6 as const,
  xl: 8 as const,
  pill: 9999 as const,
}

export const boxShadows = {
  md: {
    shadowColor: colors.Neutral900,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
}
