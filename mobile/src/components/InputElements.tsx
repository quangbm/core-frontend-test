import * as React from 'react'
import { Text } from 'react-native'
import { colors, spacings } from './GlobalStyles'
import { NIfString } from './NIfString'
import { VStack } from './NStacks'

type InputLabelProps = {
  children?: React.ReactNode
  required?: boolean
}

export const InputLabel = React.memo<InputLabelProps>(function Node({ children, required }) {
  return (
    <NIfString
      component={
        <VStack spacing={spacings.xs}>
          {Boolean(required) && <Text style={{ color: colors.danger }}>*</Text>}
          <Text style={{ color: colors.Neutral500, fontWeight: 'bold' }}>{children}</Text>
        </VStack>
      }>
      {children}
    </NIfString>
  )
})

InputLabel.displayName = 'InputLabel'

type InputMessageProps = {
  children?: React.ReactNode
  isError?: boolean
}

export const InputMessage = React.memo<InputMessageProps>(function Node({ children, isError }) {
  return (
    <NIfString
      component={
        <Text style={{ color: isError ? colors.danger : colors.Neutral500, fontSize: 10, marginTop: 4 }}>
          {children}
        </Text>
      }>
      {children}
    </NIfString>
  )
})

InputMessage.displayName = 'InputMessage'
