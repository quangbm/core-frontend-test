import * as React from 'react'
import { Text, View, ViewStyle } from 'react-native'
import { borderRadius, colors, spacings } from './GlobalStyles'
import { NIcons } from './NIcons'
import { NIfString } from './NIfString'
import { HStack, VStack } from './NStacks'

const ICON_NAME_BY_TYPE = {
  error: 'x-circle',
  success: 'check-circle',
  warning: 'alert-circle',
}

const COLOR_BY_TYPE = {
  error: colors.danger,
  success: colors.Primary700,
  warning: colors.warning,
}

type NAlertProps = {
  style?: ViewStyle
  title?: React.ReactNode
  subtitle?: React.ReactNode
  children?: React.ReactNode
  type?: 'error' | 'success' | 'warning'
}

export const NAlert = React.memo<NAlertProps>(function Node({ title, subtitle, children, style, type = 'success' }) {
  return (
    <VStack spacing={spacings.sm} style={[{ borderRadius: borderRadius.md, backgroundColor: colors.white }, style]}>
      <View
        style={{
          aspectRatio: 1,
          backgroundColor: COLOR_BY_TYPE[type],
          justifyContent: 'center',
          alignItems: 'center',
          borderTopLeftRadius: 4,
          borderBottomLeftRadius: 4,
        }}>
        <NIcons name={ICON_NAME_BY_TYPE[type]} color={colors.white} size={24} />
      </View>
      {(title || subtitle) && !children && (
        <HStack spacing={spacings.none} style={{ flex: 1, padding: spacings.xs }}>
          <NIfString
            component={<Text style={{ color: colors.Neutral900, fontSize: 14, fontWeight: 'bold' }}>{title}</Text>}>
            {title}
          </NIfString>
          <NIfString component={<Text style={{ color: colors.Neutral900 }}>{subtitle}</Text>}>{subtitle}</NIfString>
        </HStack>
      )}
      {children}
    </VStack>
  )
})

NAlert.displayName = 'NAlert'
