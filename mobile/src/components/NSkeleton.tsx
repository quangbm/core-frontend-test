import MaskedView from '@react-native-community/masked-view'
import * as React from 'react'
import { View } from 'react-native'
import Animated, {
  Easing,
  interpolateColor,
  useAnimatedStyle,
  useSharedValue,
  withDelay,
  withRepeat,
  withTiming,
} from 'react-native-reanimated'
import { colors } from './GlobalStyles'

type NSkeletonProps = {
  start?: string
  end?: string
  children?: React.ReactNode
  delay?: number
  duration?: number
}

export const NSkeleton = React.memo<NSkeletonProps>(function Node({
  children,
  start = colors.Neutral300,
  end = colors.Neutral200,
  delay = 0,
  duration = 800,
}) {
  const [size, setSize] = React.useState<{ width: number; height: number } | undefined>()

  // Animation
  const progress = useSharedValue(0)

  const animatedStyle = useAnimatedStyle(() => {
    const backgroundColor = interpolateColor(progress.value, [0, 1], [start, end])
    return {
      backgroundColor,
    }
  })

  React.useEffect(() => {
    if (size) {
      progress.value = withDelay(delay, withRepeat(withTiming(1, { duration, easing: Easing.exp }), Infinity, true))
    }
  }, [size, progress, delay, duration])

  if (!size) {
    return (
      <View onLayout={e => setSize({ width: e.nativeEvent.layout.width, height: e.nativeEvent.layout.height })}>
        {children}
      </View>
    )
  }
  return (
    <MaskedView
      maskElement={<React.Fragment>{children}</React.Fragment>}
      style={{ width: size.width, height: size.height }}>
      <Animated.View style={[{ flex: 1 }, animatedStyle]} />
    </MaskedView>
  )
})

NSkeleton.displayName = 'NSkeleton'
