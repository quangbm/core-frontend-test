import * as React from 'react'
import { ActivityIndicator, Pressable, PressableProps, StyleSheet, Text, ViewStyle } from 'react-native'
import { borderRadius, colors, spacings } from './GlobalStyles'
import { NIfString } from './NIfString'

export type NButtonProps = {
  children?: React.ReactNode
  isLoading?: boolean
  type?: 'primary' | 'default' | 'danger' | 'clear' | 'outline'
  size?: 'small' | 'medium' | 'large'
  style?: ViewStyle
} & Omit<PressableProps, 'style'>

export const NButton = React.memo<NButtonProps>(function Node({
  type = 'primary',
  size = 'medium',
  isLoading,
  children,
  ...props
}) {
  return (
    <Pressable
      {...props}
      onPress={e => {
        if (!isLoading && props.onPress) {
          props.onPress(e)
        }
      }}
      // activeOpacity={isLoading ? 1 : activeOpacity}
      style={[props.style, styles.button, styles[type], styles[size], props.disabled && styles.disabled]}>
      {isLoading && <ActivityIndicator size={16} color="white" />}
      {!isLoading && (
        <NIfString
          component={
            <Text style={[textStyles[type], textStyles[size], props.disabled && textStyles.disabled]}>{children}</Text>
          }>
          {children}
        </NIfString>
      )}
    </Pressable>
  )
})

const styles = StyleSheet.create({
  button: {
    borderWidth: 1,
    borderColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: borderRadius.md,
  },
  primary: {
    backgroundColor: colors.Primary700,
  },
  default: {
    backgroundColor: colors.Neutral200,
  },
  danger: {
    backgroundColor: colors.danger,
  },
  outline: {
    borderColor: colors.Primary700,
  },
  clear: {
    backgroundColor: 'transparent',
  },
  disabled: {
    backgroundColor: colors.Neutral300,
  },
  small: { paddingHorizontal: spacings.sm, minHeight: 24 },
  medium: { paddingHorizontal: spacings.md, minHeight: 32 },
  large: { paddingHorizontal: spacings.lg, minHeight: 40 },
})

const textStyles = StyleSheet.create({
  text: {},
  primary: {
    color: colors.white,
  },
  default: {
    color: colors.Neutral900,
  },
  danger: {
    color: colors.white,
  },
  outline: {
    color: colors.Primary700,
  },
  clear: {
    color: colors.Neutral900,
  },
  disabled: {
    color: colors.Neutral400,
  },
  small: {
    fontSize: 12,
  },
  medium: {
    fontSize: 16,
  },
  large: {
    fontSize: 20,
  },
})

NButton.displayName = 'NButton'
