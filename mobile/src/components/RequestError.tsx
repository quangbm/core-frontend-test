import * as React from 'react'
import { Text, ViewStyle } from 'react-native'
import { ErrorResponse } from '../../../shared/src/api'
import { borderRadius, colors, spacings } from './GlobalStyles'
import { NIcons } from './NIcons'
import { HStack, VStack } from './NStacks'

type RequestErrorProps = {
  error?: ErrorResponse | null
  style?: ViewStyle
  children?: React.ReactNode
}

export function RequestError({ error, style, children }: RequestErrorProps) {
  if (!error) {
    return null
  }
  return (
    <VStack
      style={[{ backgroundColor: colors.white, padding: spacings.sm, borderRadius: borderRadius.md }, style]}
      spacing={spacings.md}>
      <NIcons name="alert-triangle" size={48} color={colors.danger} />
      <HStack spacing={spacings.none} style={{ flex: 1 }}>
        <Text style={{ fontSize: 24, color: colors.Neutral900, fontWeight: 'bold' }}>
          Code: {error?.response?.data.statusCode}
        </Text>
        <Text style={{ fontSize: 16, color: colors.Neutral900 }}>
          {error?.response?.data.message || error?.message || 'Something went wrong!'}
        </Text>
      </HStack>
      {children}
    </VStack>
  )
}

RequestError.displayName = 'RequestError'
