import * as React from 'react'
import { Pressable, Text, TouchableOpacity, View, ViewStyle } from 'react-native'
import Icons from 'react-native-vector-icons/Feather'
import { SortOrder, SortParams } from '../../../../shared/src/api'
import { colors, spacings } from '../GlobalStyles'
import { HStack, VStack } from '../NStacks'
import { NDataGridHeader } from './NDataGridHeader'
import { NDataGridProvider } from './NDataGridProvider'
import { NDataGridRow } from './NDataGridRow'

export type Column<T> = {
  Header?: React.ReactNode
  accessor: string
  render?: React.ReactNode
  cellRender?(cell: T): React.ReactNode
  isSortable?: boolean
  style?: ViewStyle
}

type NDataGridProps<T> = {
  data?: Array<T>
  columns: Array<Column<T>>
  onRowPress?(row: T, index: number): void
  onRowLongPress?(row: T, index: number): void
  onSortChange?(sortParams?: SortParams): void
  emptyMessage?: React.ReactNode
}

export function NDataGrid<T>({
  data = [],
  columns,
  onRowPress,
  onRowLongPress,
  onSortChange,
  emptyMessage,
}: NDataGridProps<T>) {
  const [sortParams, setSortParams] = React.useState<SortParams | undefined>()
  const sortPress = (accessor: string) => {
    setSortParams(prev => {
      if (prev && prev.sortBy !== accessor) {
        return {
          sortBy: accessor,
          sortOrder: SortOrder.Asc,
        }
      }
      switch (prev?.sortOrder || '') {
        case SortOrder.Asc:
          return {
            sortBy: accessor,
            sortOrder: SortOrder.Desc,
          }
        case SortOrder.Desc:
          return undefined
        default:
          return {
            sortBy: accessor,
            sortOrder: SortOrder.Asc,
          }
      }
    })
  }

  const onSortChangeCallback = React.useRef(onSortChange)
  onSortChangeCallback.current = onSortChange
  React.useEffect(() => {
    if (onSortChangeCallback.current) {
      onSortChangeCallback.current(sortParams)
    }
  }, [sortParams])

  return (
    <NDataGridProvider>
      <HStack style={{ position: 'relative' }}>
        <NDataGridHeader style={{ backgroundColor: colors.Neutral200 }}>
          {columns.map((column, columnIndex) => {
            return (
              <View style={[{ flex: 1, justifyContent: 'center' }, column.style]} key={`header-${columnIndex}`}>
                <TouchableOpacity
                  disabled={!column.isSortable}
                  activeOpacity={!column.isSortable ? 1 : 0.2}
                  onPress={() => sortPress(column.accessor)}>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-between',
                    }}>
                    {column.render || (
                      <Text style={{ padding: spacings.sm, textTransform: 'uppercase', color: colors.Neutral400 }}>
                        {column.Header}
                      </Text>
                    )}
                    <VStack style={{ flexDirection: 'row', alignItems: 'center' }}>
                      {sortParams && column.accessor === sortParams.sortBy && (
                        <Icons
                          name={sortParams.sortOrder === SortOrder.Asc ? 'arrow-up' : 'arrow-down'}
                          style={{ color: colors.Neutral400 }}
                        />
                      )}
                      {column.isSortable && <Icons name="align-left" style={{ color: colors.Neutral400 }} />}
                    </VStack>
                  </View>
                </TouchableOpacity>
              </View>
            )
          })}
        </NDataGridHeader>
        {data.map((row, rowIndex) => {
          return (
            <Pressable
              key={`row-${rowIndex}`}
              disabled={!onRowPress}
              onPress={() => {
                if (onRowPress) {
                  onRowPress(row, rowIndex)
                }
              }}
              onLongPress={() => {
                if (onRowLongPress) {
                  onRowLongPress(row, rowIndex)
                }
              }}>
              <NDataGridRow style={{ borderBottomColor: colors.Neutral200, borderBottomWidth: 1 }}>
                {columns.map((cell, cellIndex) => {
                  return (
                    <View key={`cell-${cellIndex}`}>
                      {cell.cellRender && cell.cellRender(row)}
                      {!cell.cellRender && (
                        <Text
                          style={{ paddingVertical: spacings.xs, paddingHorizontal: spacings.sm }}
                          numberOfLines={2}
                          ellipsizeMode="tail">
                          {row[cell.accessor as keyof T]}
                        </Text>
                      )}
                    </View>
                  )
                })}
              </NDataGridRow>
            </Pressable>
          )
        })}
        {data.length < 1 && emptyMessage}
      </HStack>
    </NDataGridProvider>
  )
}

NDataGrid.displayName = 'NDataGrid'
