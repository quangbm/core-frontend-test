import * as React from 'react'
import { TouchableOpacity, View } from 'react-native'
import Icons from 'react-native-vector-icons/Feather'
import { colors } from '../GlobalStyles'

type NDataGridActionProps = {
  children?: React.ReactNode
}

export const NDataGridAction = React.memo<NDataGridActionProps>(function Node({}) {
  // TODO: Show action in bottom sheet
  const [, setShow] = React.useState(false)
  return (
    <View pointerEvents="box-none" style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
      <TouchableOpacity onPress={() => setShow(true)} hitSlop={{ top: 16, left: 16, right: 16, bottom: 16 }}>
        <Icons name="more-horizontal" style={{ color: colors.Neutral900, fontSize: 16 }} />
      </TouchableOpacity>
    </View>
  )
})

NDataGridAction.displayName = 'NDataGridAction'
