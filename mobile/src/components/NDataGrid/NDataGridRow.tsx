import * as React from 'react'
import { VStack, VStackProps } from '../NStacks'
import { useNDataGrid } from './NDataGridProvider'

type NDataGridRowProps = {} & VStackProps

export const NDataGridRow = React.memo<NDataGridRowProps>(function Node({ children, ...restProps }) {
  const { sizes } = useNDataGrid()
  const cells = React.Children.map(children, (child, childIndex) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child, {
        ...child.props,
        style: [child.props.style, { width: sizes[childIndex] }],
      })
    }
  })
  return <VStack {...restProps}>{cells}</VStack>
})

NDataGridRow.displayName = 'NDataGridRow'
