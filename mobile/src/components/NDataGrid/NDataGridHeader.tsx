import * as React from 'react'
import { LayoutChangeEvent } from 'react-native'
import { VStack, VStackProps } from '../NStacks'
import { useNDataGrid } from './NDataGridProvider'

type NDataGridHeaderProps = {} & VStackProps

export const NDataGridHeader = React.memo<NDataGridHeaderProps>(function Node({ children, ...restProps }) {
  const { setSizes } = useNDataGrid()
  const cells = React.Children.map(children, (child, childIndex) => {
    if (React.isValidElement(child)) {
      return React.cloneElement(child, {
        ...child.props,
        onLayout(e: LayoutChangeEvent) {
          e.persist()
          setSizes(prev => {
            if (!prev[childIndex]) {
              const next = [...prev]
              next[childIndex] = e?.nativeEvent?.layout?.width || 0
              return next
            }
            return prev
          })
        },
      })
    }
  })
  return <VStack {...restProps}>{cells}</VStack>
})

NDataGridHeader.displayName = 'NDataGridHeader'
