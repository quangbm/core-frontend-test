import * as React from 'react'

type NDataGridValue = {
  sizes: Array<number>
  setSizes: React.Dispatch<React.SetStateAction<Array<number>>>
}

const NDataGridContext = React.createContext<NDataGridValue | string>(
  'useNDataGrid should be used inside NDataGridProvider',
)

type NDataGridContextProps = {
  children: React.ReactNode
}

export function NDataGridProvider({ children }: NDataGridContextProps) {
  const [sizes, setSizes] = React.useState<Array<number>>([])
  const value: NDataGridValue = { sizes, setSizes }

  return <NDataGridContext.Provider {...{ value, children }} />
}

export const useNDataGrid = () => {
  const context = React.useContext(NDataGridContext)
  if (typeof context === 'string') {
    throw new Error(context)
  }
  return context
}
