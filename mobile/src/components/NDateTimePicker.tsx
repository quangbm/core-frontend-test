import DateTimePicker from '@react-native-community/datetimepicker'
import { format as dateFormat } from 'date-fns'
import * as React from 'react'
import { Text, TouchableWithoutFeedback } from 'react-native'
import { InputLabel, InputMessage } from './InputElements'

type NDateTimePickerProps = {
  label?: React.ReactNode
  caption?: React.ReactNode
  error?: React.ReactNode
  required?: boolean
  value?: Date
  onChange?(date?: Date): void
  mode?: 'date' | 'time'
  format?: string
  placeholder?: string
}

export const NDateTimePicker = React.memo<NDateTimePickerProps>(function Node({
  label,
  caption,
  error,
  mode = 'date',
  value,
  onChange,
  format = 'YYYY/MM/dd',
  placeholder,
}) {
  const [show, setShow] = React.useState(false)
  const changeDate = (_: any, date?: Date) => {
    if (onChange) {
      onChange(date)
    }
    setShow(false)
  }
  return (
    <React.Fragment>
      {Boolean(label) && <InputLabel>{label}</InputLabel>}
      <TouchableWithoutFeedback onPress={() => setShow(true)}>
        <Text>{value ? dateFormat(value, format) : placeholder}</Text>
      </TouchableWithoutFeedback>
      {Boolean(error || caption) && <InputMessage>{error || caption}</InputMessage>}
      {show && <DateTimePicker mode={mode} value={value || new Date()} onChange={changeDate} />}
    </React.Fragment>
  )
})

NDateTimePicker.displayName = 'NDateTimePicker'
