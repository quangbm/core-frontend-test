import * as React from 'react'
import { KeyboardAvoidingView, KeyboardAvoidingViewProps, Platform } from 'react-native'

type NKeyboardAvoidingViewProps = {
  children?: React.ReactNode
} & KeyboardAvoidingViewProps

export const NKeyboardAvoidingView = React.memo<NKeyboardAvoidingViewProps>(function Node({
  children,
  style,
  ...viewProps
}) {
  return (
    <KeyboardAvoidingView
      {...viewProps}
      behavior={Platform.select({ ios: 'padding', android: undefined })}
      style={[{ flex: 1 }, style]}>
      {children}
    </KeyboardAvoidingView>
  )
})

NKeyboardAvoidingView.displayName = 'NKeyboardAvoidingView'
