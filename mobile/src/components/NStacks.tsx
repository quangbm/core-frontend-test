import * as React from 'react'
import { View, ViewProps } from 'react-native'
import { spacings } from './GlobalStyles'

type ViewDirection = 'row' | 'column'

function getSpacing(direction: ViewDirection, space: number) {
  const property = { row: 'width', column: 'height' }[direction]
  return {
    [property]: space,
  }
}

export type NSpacerProps = {
  spacing?: number
  direction?: ViewDirection
} & ViewProps

export const NSpacer = React.forwardRef<View, NSpacerProps>(function Node(
  { style, spacing = spacings.sm, direction = 'column', ...viewProps },
  ref,
) {
  return <View {...viewProps} style={[style, getSpacing(direction, spacing)]} ref={ref} />
})

type NStackProps = {
  children?: React.ReactNode
  direction?: ViewDirection
  spacing?: number
} & ViewProps

const NStack = React.forwardRef<View, NStackProps>(function NStack(
  { direction = 'column', spacing = spacings.sm, children, style, ...viewProps },
  ref,
) {
  const length = Array.isArray(children) ? children.length : [children].length
  return (
    <View style={[style, { flexDirection: direction }]} {...viewProps} ref={ref}>
      {spacing === 0 && children}
      {spacing > 0 &&
        React.Children.map(children, (child, childIndex) => {
          if (!child) {
            return null
          }
          return (
            <React.Fragment key={`item-${childIndex}`}>
              {child}
              {childIndex + 1 < length && <NSpacer direction={direction} spacing={spacing} />}
            </React.Fragment>
          )
        })}
    </View>
  )
})

NStack.displayName = 'NStack'

export type VStackProps = {
  children?: React.ReactNode
  spacing?: number
} & ViewProps

export const VStack = React.forwardRef<View, VStackProps>(function Node(props, ref) {
  return <NStack {...props} ref={ref} direction="row" />
})

export type HStackProps = {
  children?: React.ReactNode
  spacing?: number
} & ViewProps

export const HStack = React.forwardRef<View, HStackProps>(function Node(props, ref) {
  return <NStack {...props} ref={ref} direction="column" />
})
