import * as React from 'react'

type NIfStringProps = {
  component?: React.ReactNode
  children?: React.ReactNode
}

export const NIfString = React.memo<NIfStringProps>(function Node({ children, component }) {
  if (typeof children === 'string') {
    return <React.Fragment>{component}</React.Fragment>
  }
  return <React.Fragment>{children}</React.Fragment>
})

NIfString.displayName = 'NIfString'

/*
<NIfString placeholder={children}>{children}</NIfString>

*/
