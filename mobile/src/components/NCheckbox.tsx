import * as React from 'react'
import { Pressable } from 'react-native'
import { InputLabel, InputMessage } from './InputElements'
import { NIcons } from './NIcons'
import { VStack } from './NStacks'

type NCheckboxProps = {
  children?: React.ReactNode
  checked?: boolean
  onChange(checked?: boolean): void
  label?: React.ReactNode
  error?: React.ReactNode
  caption?: React.ReactNode
  required?: boolean
}

export const NCheckbox = React.memo<NCheckboxProps>(function Node({
  required = false,
  label,
  error,
  caption = ' ',
  checked,
  onChange,
}) {
  return (
    <Pressable
      onPress={() => {
        if (onChange) {
          onChange(!checked)
        }
      }}>
      <VStack>
        <NIcons name={checked ? 'check-square' : 'square'} />
        {Boolean(label) && <InputLabel required={required}>{label}</InputLabel>}
      </VStack>
      {Boolean(error || caption) && <InputMessage isError={Boolean(error)}>{error || caption}</InputMessage>}
    </Pressable>
  )
})

NCheckbox.displayName = 'NCheckbox'
