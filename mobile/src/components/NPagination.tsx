import * as React from 'react'
import { Text, View } from 'react-native'
import { colors, spacings } from './GlobalStyles'
import { NButton } from './NButton'
import { NIcons } from './NIcons'
import { VStack } from './NStacks'

const SEPARATOR = 'PAGE_SEPARATOR'
const MAGIC_NUMBER = 3

function calculatePage(total: number, current: number, padding = 1): (number | string)[] {
  const length = MAGIC_NUMBER + padding * 2
  if (total <= length + 2) {
    return Array.from({ length: total }, (_, i) => i + 1)
  }

  const isOnlyLeft = current < length
  if (isOnlyLeft) {
    return (Array.from({ length }, (_, i) => i + 1) as (string | number)[]).concat([SEPARATOR, total])
  }

  const isOnlyRight = current > total - length + 1
  if (isOnlyRight) {
    return [1, SEPARATOR].concat(Array.from({ length }, (_, i) => i + total - length + 1))
  }

  const center = Array.from({ length }, (_, i) => {
    if (i === 0 || i + 1 === length) {
      return SEPARATOR
    }
    return i + current - padding
  })

  return ([1] as (string | number)[]).concat(center).concat([total])
}

type NPaginationProps = {
  total: number
  current: number
  onChangePage(page: number): void
  padding?: number
  disabled?: boolean
}

export const NPagination = React.memo<NPaginationProps>(function Node({
  disabled,
  total,
  current,
  padding,
  onChangePage,
}) {
  const pages = calculatePage(total, current, padding)
  // const canPreviousPress = current - 1 > 0
  // const previousPress = () => {
  //   if (canPreviousPress) {
  //     onChangePage(current - 1)
  //   }
  // }

  // const canNextPress = current < total
  // const nextPress = () => {
  //   if (canNextPress) {
  //     onChangePage(current + 1)
  //   }
  // }
  if (total <= 1) {
    return null
  }
  return (
    <VStack spacing={spacings.sm} style={{ justifyContent: 'center' }}>
      {pages.map((page, pageIndex) => {
        const key = `${page}-${pageIndex}`
        if (typeof page === 'string') {
          return (
            <View style={{ alignItems: 'center', justifyContent: 'center' }} key={key}>
              <NIcons name="more-horizontal" />
            </View>
          )
        }
        const isSelected = current === page
        return (
          <NButton
            key={key}
            type={isSelected ? 'primary' : 'default'}
            size="small"
            hitSlop={{ top: spacings.md, bottom: spacings.md, left: spacings.xs, right: spacings.xs }}
            onPress={() => {
              if (!disabled) {
                onChangePage(page)
              }
            }}>
            <Text
              style={{
                width: 16,
                textAlign: 'center',
                fontSize: 12,
                color: isSelected ? colors.white : colors.Neutral900,
              }}>
              {page}
            </Text>
          </NButton>
        )
      })}
    </VStack>
  )
})

NPagination.displayName = 'NPagination'
