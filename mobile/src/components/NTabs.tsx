import * as React from 'react'
import { FlatList, Text, TouchableOpacity, View } from 'react-native'
import { colors, spacings } from './GlobalStyles'

type NTabsProps = {
  defaultActive?: number
  active?: number
  onChangeTab?(index: number): void
  children?: ReturnType<typeof NTab> | Array<ReturnType<typeof NTab>>
  left?: React.ReactNode
  right?: React.ReactNode
}

export function NTabs({ children, defaultActive = 0, active, onChangeTab }: NTabsProps) {
  const [uncontrolledActive, setUncontrolledActive] = React.useState(defaultActive)
  const tabs = Array.isArray(children) ? children : [children]

  const activeIndex = active ?? uncontrolledActive

  const items = React.Children.map(tabs, (child, childIndex) => {
    if (React.isValidElement(child)) {
      const { title, onPress } = child.props as NTabProps

      const tabPress = () => {
        if (onPress) {
          onPress()
        }
        if (childIndex === activeIndex) {
          return
        }
        if (onChangeTab) {
          onChangeTab(childIndex)
        }
        setUncontrolledActive(childIndex)
      }

      return {
        id: `${title}-${childIndex}`,
        children: typeof title === 'string' ? <Text>{title}</Text> : title,
        onPress: tabPress,
        isActive: childIndex === activeIndex,
      }
    }
  })
  return (
    <React.Fragment>
      <FlatList
        data={items}
        renderItem={({ item }) => (
          <TouchableOpacity
            disabled={item.isActive}
            activeOpacity={item.isActive ? 1 : 0.6}
            onPress={item.onPress}
            style={{
              paddingVertical: spacings.xs,
              borderBottomWidth: 2,
              borderBottomColor: item.isActive ? colors.Primary700 : 'transparent',
            }}>
            {item.children}
          </TouchableOpacity>
        )}
        keyExtractor={item => item.id}
        horizontal
        ItemSeparatorComponent={() => <View style={{ width: spacings.sm }} />}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{
          paddingHorizontal: spacings.sm,
          paddingVertical: spacings.xs,
        }}
      />
      {tabs[activeIndex]}
    </React.Fragment>
  )
}

NTabs.displayName = 'NTabs'

type NTabProps = {
  title: React.ReactNode
  onPress?(): void
  children?: React.ReactNode
}

export function NTab({ children }: NTabProps) {
  if (typeof children === 'string') {
    return <Text>{children}</Text>
  }
  return <React.Fragment>{children}</React.Fragment>
}

NTab.displayName = 'NTab'
