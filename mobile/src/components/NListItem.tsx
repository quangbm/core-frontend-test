import * as React from 'react'
import { View, ViewProps } from 'react-native'
import { spacings } from './GlobalStyles'
import { VStack } from './NStacks'

type NListItemProps = {
  children?: React.ReactNode
  left?: React.ReactNode
  right?: React.ReactNode
  spacing?: number
} & ViewProps

export const NListItem = React.memo(function Node({
  spacing = spacings.xs,
  left,
  children,
  right,
  style,
  ...divProps
}: NListItemProps) {
  return (
    <VStack spacing={spacing} style={[{ alignItems: 'center' }, style]} {...divProps}>
      {left}
      <View style={{ flex: 1 }}>{children}</View>
      {right}
    </VStack>
  )
})

NListItem.displayName = 'NListItem'
