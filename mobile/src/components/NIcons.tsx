import * as React from 'react'
import Icons from 'react-native-vector-icons/Feather'
import { IconProps } from 'react-native-vector-icons/Icon'
import { colors } from './GlobalStyles'

type NIconsProps = {} & IconProps

export const NIcons = React.memo<NIconsProps>(function Node({ name, size = 16, color = colors.Neutral900 }) {
  return <Icons name={name} size={size} color={color} />
})

NIcons.displayName = 'NIcons'
