import * as React from 'react'
import { TextInput, TextInputProps, ViewStyle } from 'react-native'
import { colors, spacings } from './GlobalStyles'
import { InputLabel, InputMessage } from './InputElements'
import { VStack } from './NStacks'

type NTextInputProps = {
  label?: React.ReactNode
  caption?: React.ReactNode
  error?: React.ReactNode
  left?: React.ReactNode
  right?: React.ReactNode
  containerStyle?: ViewStyle
  required?: boolean
} & TextInputProps

export const NTextInput = React.memo(
  React.forwardRef<TextInput | null, NTextInputProps>(function Node(
    {
      label,
      caption = ' ',
      error,
      left,
      right,
      placeholderTextColor = colors.Neutral300,
      containerStyle,
      required,
      ...inputProps
    },
    ref,
  ) {
    const [isFocus, setIsFocus] = React.useState(false)
    const borderColor = (error && colors.danger) || (isFocus && colors.Primary700) || colors.Neutral300

    const inputRef = React.useRef<TextInput>(null)

    React.useImperativeHandle<TextInput | null, TextInput | null>(ref, () => inputRef.current)

    return (
      <React.Fragment>
        {Boolean(label) && <InputLabel required={required}>{label}</InputLabel>}
        <VStack
          style={[
            {
              paddingVertical: spacings.sm,
              backgroundColor: 'white',
              borderColor,
              borderBottomWidth: 1,
            },
            containerStyle,
          ]}>
          {left}
          <TextInput
            ref={inputRef}
            {...inputProps}
            placeholderTextColor={placeholderTextColor}
            onFocus={e => {
              if (inputProps.onFocus) {
                inputProps.onFocus(e)
              }
              setIsFocus(true)
            }}
            onBlur={e => {
              if (inputProps.onBlur) {
                inputProps.onBlur(e)
              }
              setIsFocus(false)
            }}
            style={[{ width: '100%' }, inputProps.style, { paddingVertical: 0, color: colors.Neutral900 }]}
          />
          {right}
        </VStack>
        {Boolean(error || caption) && <InputMessage isError={Boolean(error)}>{error || caption}</InputMessage>}
      </React.Fragment>
    )
  }),
)

NTextInput.displayName = 'NTextInput'
