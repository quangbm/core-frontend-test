import ColorHash from 'color-hash'
import * as React from 'react'
import { Text, View, ViewProps } from 'react-native'
import { borderRadius } from './GlobalStyles'

type NAvatarProps = {
  children?: React.ReactNode
  name: string
  size?: number
} & ViewProps

export const NAvatar = React.memo<NAvatarProps>(function Node({ name, size = 24, style, ...viewProps }) {
  return (
    <View
      {...viewProps}
      style={[
        style,
        {
          justifyContent: 'center',
          alignItems: 'center',
          width: size,
          height: size,
          borderRadius: borderRadius.pill,
          backgroundColor: new ColorHash().hex(name),
        },
      ]}>
      <Text style={{ color: 'white' }}>{name[0].toUpperCase()}</Text>
    </View>
  )
})

NAvatar.displayName = 'NAvatar'
