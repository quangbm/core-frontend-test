import * as React from 'react'
import { createNativeStackNavigator } from 'react-native-screens/native-stack'
import { AppViewerStack } from './AppViewer/AppViewerStack'
import { PrivateParamList } from './PrivateParamList'

const { Navigator, Screen } = createNativeStackNavigator<PrivateParamList>()

export const PrivateStack = React.memo(function Node() {
  return (
    <Navigator screenOptions={{ headerShown: false }}>
      <Screen {...AppViewerStack} />
    </Navigator>
  )
})
