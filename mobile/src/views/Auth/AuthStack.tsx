import * as React from 'react'
import { createNativeStackNavigator } from 'react-native-screens/native-stack'
import { AuthParamList } from './AuthParamList'
import { SignIn } from './views/SignIn'

const { Navigator, Screen } = createNativeStackNavigator<AuthParamList>()

export function AuthStack() {
  return (
    <Navigator screenOptions={{ headerShown: false }}>
      <Screen name="SignIn" component={SignIn} />
    </Navigator>
  )
}
