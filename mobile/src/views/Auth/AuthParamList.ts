import { NativeStackNavigationProp } from 'react-native-screens/native-stack'

export type AuthParamList = {
  SignIn: undefined
}

export type AuthNavigation = NativeStackNavigationProp<AuthParamList>
