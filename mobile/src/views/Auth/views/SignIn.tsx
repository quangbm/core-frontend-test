import * as React from 'react'
import { Controller, useForm } from 'react-hook-form'
import { Text, TextInput, View } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
import { colors } from '../../../components/GlobalStyles'
import { NButton } from '../../../components/NButton'
import { NTextInput } from '../../../components/NTextInput'
import { useSignIn } from '../../../hooks/useAuth'

export function SignIn() {
  // const refs = React.useRef<(TextInput | null)[]>([])
  // refs.current = []
  // const addToRefs = (node: TextInput | null) => {
  //   if (node && !refs.current.includes(node)) {
  //     refs.current.push(node)
  //   }
  // }

  const passwordRef = React.useRef<TextInput>(null)

  const { control, handleSubmit } = useForm({
    defaultValues: {
      username: 'admin',
      password: 'Abcd1234!',
    },
  })
  const { mutate, isLoading } = useSignIn()
  const [showPassword, setShowPassword] = React.useState(true)

  const submit = handleSubmit(data => mutate(data))

  return (
    <View style={{ flex: 1, padding: 16, justifyContent: 'center', backgroundColor: colors.Neutral900 }}>
      <Text style={{ fontSize: 32, color: 'white', fontWeight: 'bold' }}>
        n<Text style={{ color: colors.Primary700 }}>C</Text>ore
      </Text>
      <View style={{ backgroundColor: 'white', padding: 16, borderRadius: 8 }}>
        <Controller
          control={control}
          name="username"
          render={({ field: { value, onChange }, fieldState }) => (
            <NTextInput
              label="Username"
              placeholder="Enter your username"
              error={fieldState.error?.message}
              value={value}
              onChangeText={onChange}
              onSubmitEditing={() => {
                passwordRef.current?.focus()
              }}
            />
          )}
          rules={{ required: { value: true, message: 'Required!' } }}
        />
        <Controller
          control={control}
          name="password"
          render={({ field: { value, onChange }, fieldState }) => (
            <NTextInput
              ref={passwordRef}
              label="Password"
              placeholder="Enter your password"
              error={fieldState.error?.message}
              right={
                <NButton type="clear" onPress={() => setShowPassword(prev => !prev)}>
                  <Icon name={showPassword ? 'eye' : 'eye-off'} color={colors.Neutral700} />
                </NButton>
              }
              value={value}
              onChangeText={onChange}
              secureTextEntry={showPassword}
              onSubmitEditing={submit}
            />
          )}
          rules={{ required: { value: true, message: 'Required!' } }}
        />
        <View style={{ flexDirection: 'row', width: '100%' }}>
          <View style={{ flex: 1 }} />
          <NButton onPress={submit} style={{ flex: 1 }} isLoading={isLoading}>
            <Text style={{ color: 'white' }}>Sign In</Text>
          </NButton>
        </View>
      </View>
    </View>
  )
}

SignIn.displayName = 'SignIn'
