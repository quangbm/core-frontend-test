import { NavigatorScreenParams } from '@react-navigation/native'
import { NativeStackNavigationProp } from 'react-native-screens/native-stack'
import { AppViewerParamList } from './AppViewer/AppViewerParamList'
import { SettingsParamList } from './Settings/SettingsParamList'

export type PrivateParamList = {
  Settings: NavigatorScreenParams<SettingsParamList>
  AppViewer: NavigatorScreenParams<AppViewerParamList>
}

export type PrivateNavigation = NativeStackNavigationProp<PrivateParamList>
