import { RouteProp } from '@react-navigation/native'
import * as React from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { ActivityIndicator, ScrollView, Text, View } from 'react-native'
import { NativeStackNavigationProp } from 'react-native-screens/native-stack'
import { useMutation, useQueryClient } from 'react-query'
import { ObjectLayoutResponse } from '../../../../../shared/src/api'
import { colors, spacings } from '../../../components/GlobalStyles'
import { NButton } from '../../../components/NButton'
import { NIcons } from '../../../components/NIcons'
import { RequestError } from '../../../components/RequestError'
import { useSession, useUpdateSession } from '../../../hooks/useSession'
import { APIServices, QueryKeys } from '../../../services/api'
import { AppViewerParamList } from '../AppViewerParamList'
import { LayoutHeader } from '../components/LayoutHeader'
import { RootElement } from '../components/RootElement'
import { LayoutScriptProvider } from '../contexts/LayoutScriptContext'

function getDefaultValues(params?: {
  layoutId: string
  componentId: string
  recordId?: string
  relatedField?: string
}) {
  if (!params) {
    return undefined
  }
  const { relatedField, recordId } = params
  if (relatedField && recordId) {
    const paths = relatedField?.split('.') || []
    return {
      [paths[paths.length - 1]]: recordId,
    }
  }
  // FIXME: Input mapping
  return {}
}

export const RecordMutationLayoutName = 'RecordMutationLayout' as const

export type RecordMutationLayoutParams = {
  [RecordMutationLayoutName]?: {
    layoutId: string
    componentId: string
    recordId?: string
    relatedField?: string
  }
}

type RecordMutationLayoutProps = {
  route: RouteProp<AppViewerParamList, 'RecordMutationLayout'>
  navigation: NativeStackNavigationProp<AppViewerParamList, 'RecordMutationLayout'>
}

function RecordMutationLayoutScreen({ route, navigation }: RecordMutationLayoutProps) {
  const { layoutId, componentId, recordId } = route.params || {}
  const [{ data: sessionData }] = useSession()
  const isUpdateRecord = Boolean(recordId)

  const formMethods = useForm({ defaultValues: getDefaultValues(route.params) })

  const queryClient = useQueryClient()

  const goBackOrPush = () => {
    if (navigation.canGoBack()) {
      navigation.goBack()
      return
    }
    if (sessionData?.lastUsedAppId) {
      navigation.push('AppLayout', { appId: sessionData.lastUsedAppId, layoutId })
      return
    }
    navigation.push('AppListing')
  }

  const { mutate: getLayoutScript, error, data, isLoading } = useMutation(APIServices.Layout.triggers)
  const {
    mutate: createObject,
    error: createObjectError,
    isLoading: isCreating,
  } = useMutation(APIServices.Data.createObjectData, {
    onSuccess() {
      formMethods.reset()
      queryClient.invalidateQueries([QueryKeys.Data.searchObjectData])
      goBackOrPush()
    },
    onError() {},
  })
  const {
    mutate: updateObject,
    error: updateObjectError,
    isLoading: isUpdating,
  } = useMutation(APIServices.Data.updateObjectData, {
    onSuccess() {
      queryClient.invalidateQueries([QueryKeys.Data.searchObjectData])
      goBackOrPush()
    },
    onError() {},
  })

  React.useEffect(() => {
    if (layoutId && componentId) {
      getLayoutScript({ layoutId, componentId, guid: recordId })
    }
  }, [componentId, getLayoutScript, layoutId, recordId])

  const layoutData = data?.data as ObjectLayoutResponse
  const elements = layoutData?.script
  const objectName = layoutData?.objectName

  const urlParams = `layoutId=${layoutId}&componentId=${componentId}`

  const currentUrl =
    (layoutData &&
      (isUpdateRecord ? `/${objectName}/${recordId}/edit?${urlParams}` : `/${objectName}/create?${urlParams}`)) ||
    undefined

  useUpdateSession({ currentUrl })

  const submitPress = formMethods.handleSubmit(formValues => {
    console.log('data', { formValues })
    if (recordId) {
      updateObject({ name: objectName!, guid: recordId, body: formValues })
      return
    }
    createObject({
      name: objectName!,
      body: formValues,
    })
  })

  const isError = Boolean(error || createObjectError || updateObjectError)

  return (
    <View style={{ flex: 1 }}>
      <LayoutHeader
        left={
          <NButton
            type="clear"
            onPress={() => {
              navigation.goBack()
            }}>
            <NIcons name="chevron-left" />
          </NButton>
        }
        title={<Text>{isUpdateRecord ? 'Update' : 'Create'}</Text>}
        right={<NButton type="primary">{isUpdateRecord ? 'Save' : 'Create'}</NButton>}
      />
      {isLoading && (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: spacings.sm }}>
          <ActivityIndicator color={colors.Primary700} />
        </View>
      )}
      {isError && (
        <View style={{ padding: spacings.sm }}>
          <RequestError error={error || createObjectError || updateObjectError} />
        </View>
      )}
      {!isError && (
        <ScrollView contentContainerStyle={{ padding: spacings.sm }}>
          <FormProvider {...formMethods}>
            <LayoutScriptProvider
              elements={elements}
              objectName={objectName}
              recordId={recordId}
              layoutId={layoutId!}
              layoutMode="form">
              <RootElement />
            </LayoutScriptProvider>
          </FormProvider>
          <NButton isLoading={isCreating || isUpdating} onPress={submitPress}>
            {isUpdateRecord ? 'Update' : 'Create'}
          </NButton>
        </ScrollView>
      )}
    </View>
  )
}

export const RecordMutationLayout = {
  name: RecordMutationLayoutName,
  component: RecordMutationLayoutScreen,
}
