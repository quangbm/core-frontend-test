import { RouteProp } from '@react-navigation/native'
import * as React from 'react'
import { ScrollView, View } from 'react-native'
import { NativeStackNavigationProp } from 'react-native-screens/native-stack'
import { useQuery } from 'react-query'
import { spacings } from '../../../components/GlobalStyles'
import { NButton } from '../../../components/NButton'
import { NIcons } from '../../../components/NIcons'
import { RequestError } from '../../../components/RequestError'
import { useSession, useUpdateSession } from '../../../hooks/useSession'
import { APIServices, QueryKeys } from '../../../services/api'
import { AppViewerParamList } from '../AppViewerParamList'
import { LayoutHeader } from '../components/LayoutHeader'
import { RootElement } from '../components/RootElement'
import { LayoutScriptProvider } from '../contexts/LayoutScriptContext'

export const RecordLayoutName = 'RecordLayout' as const

export type RecordLayoutParams = {
  [RecordLayoutName]?: {
    objectName: string
    recordId: string
  }
}

type RecordLayoutProps = {
  route: RouteProp<AppViewerParamList, 'RecordLayout'>
  navigation: NativeStackNavigationProp<AppViewerParamList, 'RecordLayout'>
}

function RecordLayoutScreen({ route, navigation }: RecordLayoutProps) {
  const { objectName, recordId } = route.params || {}
  const [{ data: sessionData }] = useSession()

  useUpdateSession({ currentUrl: `/${objectName}/${recordId}` })

  const { data, error, isError } = useQuery(
    [QueryKeys.Layout.getObjectLayout, { objName: objectName! }, sessionData!.lastUsedAppId] as const,
    APIServices.Layout.getObjectLayout,
    {
      enabled: Boolean(objectName),
      select(response) {
        return response.data
      },
    },
  )

  const { data: objectData } = useQuery(
    [QueryKeys.Data.getSingleObjectData, { name: objectName!, guid: recordId! }] as const,
    APIServices.Data.getSingleObjectData,
    {
      enabled: Boolean(objectName && recordId),
      select(response) {
        return response.data
      },
    },
  )
  return (
    <View style={{ flex: 1 }}>
      <LayoutHeader
        left={
          <NButton
            type="clear"
            onPress={() => {
              navigation.goBack()
            }}>
            <NIcons name="chevron-left" />
          </NButton>
        }
        title={
          // FIXME: Add Loading state, use better typography for title
          // Display record's name on title
          // @ts-ignore
          objectData?.name || data?.name || objectName
        }
      />
      {isError && <RequestError error={error!} style={{ margin: spacings.sm }} />}
      {!isError && data && (
        <ScrollView contentContainerStyle={{ padding: spacings.sm }}>
          <LayoutScriptProvider elements={data?.script} layoutId={data.id} recordId={recordId} objectName={objectName}>
            <RootElement />
          </LayoutScriptProvider>
        </ScrollView>
      )}
    </View>
  )
}

export const RecordLayout = {
  name: RecordLayoutName,
  component: RecordLayoutScreen,
}
