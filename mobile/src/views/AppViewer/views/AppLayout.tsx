import { RouteProp } from '@react-navigation/native'
import * as React from 'react'
import { ActivityIndicator, FlatList, ScrollView, Text, View } from 'react-native'
import { NativeStackNavigationProp } from 'react-native-screens/native-stack'
import { useQuery } from 'react-query'
import { LayoutResponse } from '../../../../../shared/src/api'
import { colors, spacings } from '../../../components/GlobalStyles'
import { NButton } from '../../../components/NButton'
import { NSpacer } from '../../../components/NStacks'
import { useUpdateSession } from '../../../hooks/useSession'
import { APIServices, QueryKeys } from '../../../services/api'
import { cacheTimes } from '../../../services/react-query'
import { AppViewerParamList } from '../AppViewerParamList'
import { LayoutHeader } from '../components/LayoutHeader'
import { RootElement } from '../components/RootElement'
import { LayoutScriptProvider } from '../contexts/LayoutScriptContext'

export const AppLayoutName = 'AppLayout' as const

export type AppLayoutParams = {
  [AppLayoutName]?: {
    appId: string
    layoutId?: string
  }
}

type AppLayoutProps = {
  route: RouteProp<AppViewerParamList, 'AppLayout'>
  navigation: NativeStackNavigationProp<AppViewerParamList, 'AppLayout'>
}

function AppLayoutScreen({ route, navigation }: AppLayoutProps) {
  const { appId, layoutId } = route.params || { appId: '' }

  useUpdateSession({ appId, currentUrl: `/${layoutId}` })

  const {
    data: appData,
    error: appDataError,
    isLoading: _isAppDataLoading,
  } = useQuery([QueryKeys.App.getApp, { id: appId! }] as const, APIServices.App.getApp, {
    enabled: Boolean(appId),
    select(response) {
      return response.data
    },
  })
  const {
    data: { appPages, pageById: _pageById } = { appPages: [], pageById: {} },
    error: appPagesError,
    isLoading: isAppPagesLoading,
  } = useQuery([QueryKeys.App.getAppPages, { id: appId! }] as const, APIServices.App.getAppPages, {
    enabled: Boolean(appId),
    select(response) {
      const pages = {} as Record<string, LayoutResponse>
      for (const page of response.data) {
        pages[page.id] = page
      }
      return {
        appPages: response.data,
        pageById: pages,
      }
    },
  })

  React.useEffect(() => {
    if (!layoutId && appPages[0]) {
      navigation.setParams({ appId, layoutId: appPages[0].id })
    }
  }, [appId, appPages, layoutId, navigation])

  const {
    data: elements = {},
    isLoading: isAppScriptLoading,
    error: appScriptError,
  } = useQuery([QueryKeys.App.getAppScript, { layoutId: layoutId! }, appId!] as const, APIServices.App.getAppScript, {
    enabled: Boolean(layoutId),
    select(response) {
      return response.data
    },
    staleTime: cacheTimes.Minute * 5,
    cacheTime: cacheTimes.Minute * 5,
  })

  const isError = Boolean(appDataError || appPagesError || appScriptError)

  if (isError) {
    return (
      <View>
        <Text>Something went wrong!</Text>
      </View>
    )
  }

  /*

          <VStack style={{ alignItems: 'center' }}>
            <View>
              <Text style={{ fontSize: 16 }} numberOfLines={1}>
                {isAppDataLoading ? 'Loading ...' : appData?.name}
              </Text>
              <Text style={{ fontSize: 12 }} numberOfLines={1}>
                {isAppPagesLoading ? 'Loading ...' : layoutId && pageById[layoutId]?.name}
              </Text>
            </View>
            {!isAppPagesLoading && appPages.length > 1 && <NIcons name={showPages ? 'chevron-up' : 'chevron-down'} />}
          </VStack>
  */

  return (
    <View style={{ flex: 1 }}>
      <LayoutHeader title={isAppPagesLoading ? 'Loading ...' : appData?.name}>
        <View>
          <FlatList
            style={{ backgroundColor: colors.Neutral100 }}
            horizontal
            data={appPages}
            contentContainerStyle={{ padding: spacings.sm }}
            renderItem={({ item }) => {
              const isActive = layoutId === item.id
              return (
                <NButton
                  size="small"
                  type={isActive ? 'primary' : 'clear'}
                  onPress={() => {
                    navigation.navigate('AppLayout', { appId, layoutId: item.id })
                  }}>
                  {item.name}
                </NButton>
              )
            }}
            keyExtractor={item => item.id}
            ItemSeparatorComponent={() => <NSpacer spacing={spacings.sm} direction="row" />}
            ListHeaderComponent={() =>
              (isAppPagesLoading && (
                <View>
                  <Text>Loading ...</Text>
                </View>
              )) ||
              null
            }
          />
        </View>
      </LayoutHeader>
      {isAppScriptLoading && (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator color={colors.Primary700} />
        </View>
      )}
      {!isAppScriptLoading && (
        <ScrollView contentContainerStyle={{ padding: spacings.sm }}>
          <LayoutScriptProvider elements={elements} layoutId={layoutId!}>
            <RootElement />
          </LayoutScriptProvider>
        </ScrollView>
      )}
    </View>
  )
}

export const AppLayout = {
  name: AppLayoutName,
  component: AppLayoutScreen,
}
