import { RouteProp } from '@react-navigation/native'
import * as React from 'react'
import { FlatList, Text, View } from 'react-native'
import { NativeStackNavigationProp } from 'react-native-screens/native-stack'
import { useInfiniteQuery } from 'react-query'
import { ApplicationResponse } from '../../../../../shared/src/api'
import { NButton } from '../../../components/NButton'
import { NSpacer } from '../../../components/NStacks'
import { useSearchText } from '../../../hooks/useSearchText'
import { useUpdateSession } from '../../../hooks/useSession'
import { APIServices, QueryKeys } from '../../../services/api'
import { AppViewerParamList } from '../AppViewerParamList'
import { LayoutHeader } from '../components/LayoutHeader'

export const AppListingName = 'AppListing' as const

export type AppListingParams = {
  [AppListingName]?: {}
}

type AppListingProps = {
  route: RouteProp<AppViewerParamList, 'AppListing'>
  navigation: NativeStackNavigationProp<AppViewerParamList, 'AppListing'>
}

function AppListingScreen({ navigation }: AppListingProps) {
  useUpdateSession({ currentUrl: '/' })
  const [{ searchText }] = useSearchText()
  const { data, hasNextPage, fetchNextPage } = useInfiniteQuery(
    [QueryKeys.App.getApps, { limit: 20, searchText }, 'Infinite'] as const,
    APIServices.App.getApps,
    {
      getNextPageParam(lastPage) {
        const offset = lastPage.data.pageInfo.offset + lastPage.data.pageInfo.limit
        if (offset > lastPage.data.pageInfo.total) {
          return
        }
        return { offset }
      },
    },
  )
  const listData = React.useMemo(() => {
    let result = [] as Array<ApplicationResponse>
    for (const page of data?.pages || []) {
      result = result.concat(page.data.data)
    }
    return result
  }, [data])

  return (
    <View style={{ flex: 1 }}>
      <LayoutHeader title="nCore" left={<View />} />
      <FlatList
        data={listData}
        renderItem={({ item }) => {
          return (
            <NButton
              type="clear"
              onPress={() => {
                navigation.push('AppLayout', { appId: item.id, layoutId: '' })
              }}>
              <View style={{ flex: 1 }}>
                <Text>{item.name}</Text>
              </View>
            </NButton>
          )
        }}
        keyExtractor={item => item.id}
        ItemSeparatorComponent={() => <NSpacer />}
        onEndReached={() => {
          if (hasNextPage) {
            fetchNextPage()
          }
        }}
      />
    </View>
  )
}

export const AppListing = {
  name: AppListingName,
  component: AppListingScreen,
}
