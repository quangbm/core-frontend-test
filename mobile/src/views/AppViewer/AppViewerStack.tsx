import * as React from 'react'
import { createNativeStackNavigator } from 'react-native-screens/native-stack'
import { AppViewerParamList } from './AppViewerParamList'
import { AppLayout } from './views/AppLayout'
import { AppListing } from './views/AppListing'
import { RecordLayout } from './views/RecordLayout'
import { RecordMutationLayout } from './views/RecordMutationLayout'

const { Navigator, Screen } = createNativeStackNavigator<AppViewerParamList>()

function Component() {
  return (
    <Navigator screenOptions={{ headerShown: false }}>
      <Screen {...AppListing} />
      <Screen {...AppLayout} />
      <Screen {...RecordLayout} />
      <Screen {...RecordMutationLayout} />
    </Navigator>
  )
}

export const AppViewerStack = {
  name: 'AppViewer' as const,
  component: Component,
}
