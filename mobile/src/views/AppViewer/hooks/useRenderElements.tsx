import * as React from 'react'
import { Text } from 'react-native'
import { VIEW_ELEMENTS } from '../components/ViewElements'
import { useLayoutScript } from '../contexts/LayoutScriptContext'

export function useRenderElements(elementId: string) {
  const { elements, layoutId } = useLayoutScript()
  const element = elements && elements[elementId]
  return (
    element &&
    element.children &&
    element.children.map(childId => {
      const componentName = elements[childId]?.component
      const Component = VIEW_ELEMENTS[componentName as keyof typeof VIEW_ELEMENTS]
      if (!Component) {
        return <Text key={childId}>{JSON.stringify({ componentName, layoutId, elementId }, null, 2)}</Text>
      }
      return <Component elementId={childId} key={childId} parentId={elementId} />
    })
  )
}
