import * as React from 'react'
import { useQuery } from 'react-query'
import { ComponentResponse, DataResponse } from '../../../../../shared/src/api'
import { APIServices, QueryKeys } from '../../../services/api'

export type CurrentUser = {}

export type InputMap = {
  curUser?: Record<string, any>
  curRecord?: DataResponse
  now: string
}

type LayoutScriptValue = {
  elements: Record<string, ComponentResponse>
  layoutId: string
  content?: DataResponse
  isLoading: boolean
  layoutMode: 'view' | 'form'
}

const LayoutScriptContext = React.createContext<LayoutScriptValue | string>(
  'useLayoutScript should be used inside LayoutScriptProvider',
)

type LayoutScriptContextProps = {
  children: React.ReactNode
  elements: Record<string, ComponentResponse>
  layoutId: string
  layoutMode?: 'view' | 'form'
  recordId?: string
  objectName?: string
}

export function LayoutScriptProvider({
  children,
  elements,
  layoutId,
  objectName,
  recordId,
  layoutMode = 'view',
}: LayoutScriptContextProps) {
  const { data: content, isLoading } = useQuery(
    [QueryKeys.Data.getSingleObjectData, { name: objectName!, guid: recordId! }] as const,
    APIServices.Data.getSingleObjectData,
    {
      enabled: Boolean(objectName && recordId),
      select(response) {
        return response.data
      },
    },
  )
  const value: LayoutScriptValue = { elements, layoutId, layoutMode, content, isLoading }

  return <LayoutScriptContext.Provider {...{ value, children }} />
}

export const useLayoutScript = () => {
  const context = React.useContext(LayoutScriptContext)
  if (typeof context === 'string') {
    throw new Error(context)
  }
  return context
}
