import * as React from 'react'
import { spacings } from '../../../components/GlobalStyles'
import { HStack } from '../../../components/NStacks'
import { ROOT_ELEMENT_ID } from '../constants'
import { useRenderElements } from '../hooks/useRenderElements'

export function RootElement() {
  const children = useRenderElements(ROOT_ELEMENT_ID)
  return <HStack spacing={spacings.sm}>{children || null}</HStack>
}

RootElement.displayName = 'RootElement'
