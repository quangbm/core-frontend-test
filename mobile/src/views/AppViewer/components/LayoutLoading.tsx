import * as React from 'react'
import { useWindowDimensions, View } from 'react-native'
import { colors } from '../../../components/GlobalStyles'
import { NSkeleton } from '../../../components/NSkeleton'
import { HStack, VStack } from '../../../components/NStacks'

export const LayoutLoading = React.memo(function Node() {
  const { width } = useWindowDimensions()
  return (
    <HStack>
      <NSkeleton>
        <VStack>
          <View style={{ height: 32, width: (width / 5) * 2, backgroundColor: colors.white }} />
          <View style={{ height: 32, width: (width / 5) * 1, backgroundColor: colors.white }} />
        </VStack>
      </NSkeleton>
      <NSkeleton delay={15}>
        <View style={{ height: 32, width: (width / 5) * 1, backgroundColor: colors.white }} />
      </NSkeleton>
    </HStack>
  )
})

LayoutLoading.displayName = 'LayoutLoading'
