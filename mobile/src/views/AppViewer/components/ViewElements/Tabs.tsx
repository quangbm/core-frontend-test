import * as React from 'react'
import { View } from 'react-native'
import { borderRadius, colors } from '../../../../components/GlobalStyles'
import { NTab, NTabs } from '../../../../components/NTabs'
import { useLayoutScript } from '../../contexts/LayoutScriptContext'
import { useRenderElements } from '../../hooks/useRenderElements'
import { ElementProps } from './types'

export function Tabs({ elementId }: ElementProps) {
  const { elements } = useLayoutScript()
  const element = elements[elementId]

  const [active, setActive] = React.useState(0)

  return (
    <View style={{ backgroundColor: colors.white, borderRadius: borderRadius.md }}>
      <NTabs active={active} onChangeTab={setActive}>
        {element.children?.map(child => {
          const tab = elements[child] as { props: { title: string } }
          return (
            <NTab title={tab.props.title} key={child}>
              <Tab elementId={child} parentId={elementId} />
            </NTab>
          )
        })}
      </NTabs>
    </View>
  )
}

function Tab({ elementId }: ElementProps) {
  const children = useRenderElements(elementId)
  return <React.Fragment>{children}</React.Fragment>
}
