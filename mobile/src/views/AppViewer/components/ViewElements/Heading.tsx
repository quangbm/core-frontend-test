import * as React from 'react'
import { StyleSheet, Text } from 'react-native'
import { useLayoutScript } from '../../contexts/LayoutScriptContext'
import { ElementProps } from './types'

type HeadingProps = {
  level: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6'
  children: string
}

export function Heading({ elementId }: ElementProps) {
  const { elements } = useLayoutScript()
  const element = elements[elementId]
  // conflict type with server
  // @ts-ignore
  const { level, children } = element.props as HeadingProps
  return <Text style={[styles.text, styles[level]]}>{children}</Text>
}

const styles = StyleSheet.create({
  text: {},
  h1: { fontSize: 24 },
  h2: { fontSize: 20 },
  h3: { fontSize: 16 },
  h4: { fontSize: 12 },
  h5: { fontSize: 10 },
  h6: { fontSize: 8 },
})
