import * as React from 'react'
import { Text, View } from 'react-native'
import { borderRadius, colors, spacings } from '../../../../components/GlobalStyles'
import { useLayoutScript } from '../../contexts/LayoutScriptContext'
import { ElementProps } from './types'

type ParagraphProps = {
  children: string
}

export function Paragraph({ elementId }: ElementProps) {
  const { elements } = useLayoutScript()
  const element = elements[elementId]
  // conflict type with server
  // @ts-ignore
  const { children } = element.props as ParagraphProps
  return (
    <View style={{ padding: spacings.sm, backgroundColor: colors.white, borderRadius: borderRadius.md }}>
      <Text>{children}</Text>
    </View>
  )
}
