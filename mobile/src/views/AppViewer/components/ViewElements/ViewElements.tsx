import { Button } from './Button'
import { Heading } from './Heading'
import { ObjectList, RelatedList } from './ObjectList'
import { Paragraph } from './Paragraph'
import { RecordDetail } from './RecordDetail'
import { Section } from './Section'
import { Tabs } from './Tabs'

export const VIEW_ELEMENTS = {
  Button,
  Heading,
  Paragraph,
  Section,
  Tabs,
  ObjectList,
  RelatedList,
  RecordDetail,
}
