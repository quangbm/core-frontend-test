import { useNavigation } from '@react-navigation/native'
import * as React from 'react'
import { ScrollView, Text, useWindowDimensions } from 'react-native'
import { useQuery } from 'react-query'
import {
  DataResponse,
  FieldSchema,
  FilterItem,
  ObjectSchemaResponse,
  SortOrder,
} from '../../../../../../../shared/src/api'
import { borderRadius, colors, spacings } from '../../../../../components/GlobalStyles'
import { Column, NDataGrid, NDataGridAction } from '../../../../../components/NDataGrid'
import { NPagination } from '../../../../../components/NPagination'
import { HStack, VStack } from '../../../../../components/NStacks'
import { useQueryParams } from '../../../../../hooks/useQueryParams'
import { useSession } from '../../../../../hooks/useSession'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { PrivateNavigation } from '../../../../PrivateParamList'
import { useLayoutScript } from '../../../contexts/LayoutScriptContext'
import { useRenderElements } from '../../../hooks/useRenderElements'
import { ElementProps } from '../types'
import { ListEmptyMessage } from './components/ListEmptyMessage'
import { ListHeader } from './components/ListHeader'
import { ListLoading } from './components/ListLoading'

type ObjectListProps = {
  fields: Array<string>
  limit: number
  objName: string
  rowActions: Array<string>
  sortOrder: SortOrder
  title: string
  description: string
  filters?: FilterItem[][]
}

export function ObjectList({ elementId }: ElementProps) {
  const navigation = useNavigation<PrivateNavigation>()
  const [{ data: sessionData }] = useSession()
  const { elements } = useLayoutScript()
  const dimension = useWindowDimensions()
  const width = Math.round((dimension.width / 5) * 2)

  const actions = useRenderElements(elementId)

  // Server type is outdated
  // @ts-ignore
  const element = elements[elementId] as {
    children: Array<string>
    props: ObjectListProps
    schema: ObjectSchemaResponse
  }

  const { objName, title, description } = element.props

  const [{ searchValue, page, ...queryParams }, { onChangeSort, onChangeSearch, onChangePage }] = useQueryParams({
    limit: element.props.limit,
  })

  const { data: { data, totalPage } = { data: [], totalPage: 1 }, isFetching: isLoading } = useQuery(
    [QueryKeys.Data.searchObjectData, { name: objName, ...(queryParams || {}) }, sessionData!.lastUsedAppId] as const,
    APIServices.Data.searchObjectData,
    {
      enabled: Boolean(objName),
      keepPreviousData: true,
      select(response) {
        return {
          data: response.data.data,
          totalPage: Math.ceil(response.data.pageInfo.total / response.data.pageInfo.limit) ?? 1,
        }
      },
    },
  )

  const { columns, isSearchable } = React.useMemo(() => {
    let searchable = false
    const transformedSchema = {} as Record<string, FieldSchema>
    for (const schema of element.schema?.fields || []) {
      transformedSchema[schema.name] = schema
    }
    const fields = [] as Array<Column<DataResponse>>
    for (const fieldName of element.props.fields || []) {
      const field = transformedSchema[fieldName]
      if (!field) {
        continue
      }
      searchable = Boolean(field.isSearchable || searchable)
      if (field.typeName === 'pickList') {
        fields.push({
          accessor: field.name,
          Header: field.displayName,
          isSortable: field.isSearchable,
          style: { width },
          cellRender(row: DataResponse) {
            const values = row[field.name as keyof DataResponse]
            if (!values) {
              return <Text style={{ paddingVertical: spacings.xs, paddingHorizontal: spacings.sm }}>N/A</Text>
            }
            return (
              <VStack spacing={spacings.sm} style={{ paddingTop: spacings.xs, flexWrap: 'wrap' }}>
                {(values as Array<string>).map(v => (
                  <Text
                    key={v}
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={{
                      borderRadius: borderRadius.md,
                      backgroundColor: colors.Neutral200,
                      paddingVertical: spacings.xs,
                      paddingHorizontal: spacings.sm,
                      marginBottom: spacings.xs,
                    }}>
                    {v}
                  </Text>
                ))}
              </VStack>
            )
          },
        })
        continue
      }
      if (field.typeName === 'json') {
        fields.push({
          accessor: field.name,
          Header: field.displayName,
          isSortable: field.isSortable,
          style: { width },
          cellRender(row) {
            return <Text numberOfLines={1}>{JSON.stringify(row[field.name as keyof DataResponse])}</Text>
          },
        })
        continue
      }
      fields.push({
        accessor: field.name,
        Header: field.displayName,
        isSortable: field.isSortable,
        style: { width },
      } as Column<DataResponse>)
    }
    return {
      isSearchable: searchable,
      columns: fields.concat([
        {
          accessor: 'action',
          style: { width: 50 },
          cellRender() {
            return <NDataGridAction />
          },
        } as Column<DataResponse>,
      ]),
    }
  }, [element.props.fields, element.schema?.fields, width])

  return (
    <HStack style={{ paddingBottom: spacings.sm, backgroundColor: colors.white, borderRadius: borderRadius.md }}>
      <ListHeader
        title={title || element.schema.displayName}
        description={description || element.schema.description}
        isSearchable={isSearchable}
        searchValue={searchValue}
        onChangeSearch={onChangeSearch}>
        {actions}
      </ListHeader>
      <HStack style={{ position: 'relative' }}>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingHorizontal: spacings.sm }}
          scrollEnabled={!isLoading}>
          <NDataGrid
            data-object={objName}
            data={data}
            columns={columns}
            onRowPress={row => {
              navigation.push('AppViewer', {
                screen: 'RecordLayout',
                params: { objectName: objName, recordId: row.guid, appId: '' },
              })
            }}
            // TODO: Show actions on long press
            // onRowLongPress={row => {
            //   Alert.alert('Long Pressed', row.objName)
            // }}
            onSortChange={onChangeSort}
            emptyMessage={<ListEmptyMessage />}
          />
        </ScrollView>
        <NPagination total={totalPage} current={page} onChangePage={onChangePage} disabled={isLoading} />
        {isLoading && <ListLoading />}
      </HStack>
    </HStack>
  )
}
