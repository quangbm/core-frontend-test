import { useNavigation } from '@react-navigation/native'
import * as React from 'react'
import { ScrollView, Text, useWindowDimensions } from 'react-native'
import { useQuery } from 'react-query'
import { DataResponse, FilterItem, ObjectSchemaResponse, SortOrder } from '../../../../../../../shared/src/api'
import { borderRadius, colors, spacings } from '../../../../../components/GlobalStyles'
import { NButton } from '../../../../../components/NButton'
import { Column, NDataGrid, NDataGridAction } from '../../../../../components/NDataGrid'
import { NPagination } from '../../../../../components/NPagination'
import { HStack, VStack } from '../../../../../components/NStacks'
import { useQueryParams } from '../../../../../hooks/useQueryParams'
import { useSession } from '../../../../../hooks/useSession'
import { APIServices, QueryKeys } from '../../../../../services/api'
import { PrivateNavigation } from '../../../../PrivateParamList'
import { useLayoutScript } from '../../../contexts/LayoutScriptContext'
import { ElementProps } from '../types'
import { ListEmptyMessage } from './components/ListEmptyMessage'
import { ListHeader } from './components/ListHeader'
import { ListLoading } from './components/ListLoading'

type RelatedListProps = {
  fields: Array<string>
  limit: number
  objName: string
  rowActions: Array<string>
  sortOrder: SortOrder
  title: string
  description: string
  relationField: string
  filters?: FilterItem[][]
}

// FIX ME
const GUID = ''

export function RelatedList({ elementId }: ElementProps) {
  const navigation = useNavigation<PrivateNavigation>()
  const [{ data: sessionData }] = useSession()
  const { elements } = useLayoutScript()
  const dimension = useWindowDimensions()
  const width = Math.round((dimension.width / 5) * 2)

  // Server type is outdated
  // @ts-ignore
  const element = elements[elementId] as {
    children: Array<string>
    props: RelatedListProps
    schema: ObjectSchemaResponse
  }

  const { objName, title, description, relationField } = element.props

  const [{ searchValue, page, ...queryParams }, { onChangeSort, onChangeSearch, onChangePage }] = useQueryParams({
    limit: element.props.limit,
  })

  const { data: { data, totalPage } = { data: [], totalPage: 1 }, isFetching: isLoading } = useQuery(
    [
      QueryKeys.Data.searchObjectRelatedData,
      { name: objName, relation: relationField, guid: GUID, ...(queryParams || {}) },
      sessionData!.lastUsedAppId,
    ] as const,
    APIServices.Data.searchObjectRelatedData,
    {
      enabled: Boolean(objName && relationField && GUID),
      keepPreviousData: true,
      select(response) {
        return {
          data: response.data.data,
          totalPage: Math.ceil(response.data.pageInfo.total / response.data.pageInfo.limit) ?? 1,
        }
      },
    },
  )

  const { columns, isSearchable } = React.useMemo(() => {
    let searchable = false
    const fields = element.schema.fields
      ?.map<Column<DataResponse>>(field => {
        searchable = Boolean(field.isSearchable || searchable)
        if (field.typeName === 'pickList') {
          return {
            accessor: field.name,
            Header: field.displayName,
            isSortable: field.isSearchable,
            style: { width },
            cellRender(row) {
              const values = row[field.name as keyof DataResponse]
              if (!values) {
                return <Text style={{ paddingVertical: spacings.xs, paddingHorizontal: spacings.sm }}>N/A</Text>
              }
              return (
                <VStack spacing={spacings.sm} style={{ paddingTop: spacings.xs, flexWrap: 'wrap' }}>
                  {(values as Array<string>).map(v => (
                    <Text
                      key={v}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                      style={{
                        borderRadius: borderRadius.md,
                        backgroundColor: colors.Neutral200,
                        paddingVertical: spacings.xs,
                        paddingHorizontal: spacings.sm,
                        marginBottom: spacings.xs,
                      }}>
                      {v}
                    </Text>
                  ))}
                </VStack>
              )
            },
          }
        }
        return {
          accessor: field.name,
          Header: field.displayName,
          isSortable: field.isSortable,
          style: { width },
        }
      })
      .concat([
        {
          accessor: 'action',
          style: { width: 50 },
          cellRender() {
            return <NDataGridAction />
          },
        },
      ])
    return {
      isSearchable: searchable,
      columns: fields,
    }
  }, [element.schema, width])

  return (
    <HStack style={{ paddingBottom: spacings.sm, backgroundColor: colors.white, borderRadius: borderRadius.md }}>
      <ListHeader
        title={title || element.schema.displayName}
        description={description || element.schema.description}
        isSearchable={isSearchable}
        searchValue={searchValue}
        onChangeSearch={onChangeSearch}>
        {element.children.map(child => (
          <NButton key={child}>
            <Text>{child.slice(0, 4)}</Text>
          </NButton>
        ))}
      </ListHeader>
      <HStack style={{ position: 'relative' }}>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingHorizontal: spacings.sm }}
          scrollEnabled={!isLoading}>
          <NDataGrid
            data={data}
            columns={columns}
            onRowPress={row => {
              navigation.push('AppViewer', {
                screen: 'RecordLayout',
                params: { objectName: objName, recordId: row.guid },
              })
            }}
            onSortChange={onChangeSort}
            emptyMessage={<ListEmptyMessage />}
          />
        </ScrollView>
        <NPagination total={totalPage} current={page} onChangePage={onChangePage} disabled={isLoading} />
        {isLoading && <ListLoading />}
      </HStack>
    </HStack>
  )
}
