import * as React from 'react'
import { Text, useWindowDimensions, View, ViewProps } from 'react-native'
import { spacings } from '../../../../../../components/GlobalStyles'
import { NIfString } from '../../../../../../components/NIfString'

type ListEmptyMessageProps = {
  children?: React.ReactNode
} & ViewProps

export const ListEmptyMessage = React.memo<ListEmptyMessageProps>(function Node({
  children = 'No Data',
  style,
  ...viewProps
}) {
  const dimension = useWindowDimensions()
  return (
    <View {...viewProps} style={[{ width: dimension.width - spacings.sm * 2, alignItems: 'center' }, style]}>
      <NIfString
        component={<Text style={{ paddingVertical: spacings.xs, paddingHorizontal: spacings.sm }}>{children}</Text>}>
        {children}
      </NIfString>
    </View>
  )
})

ListEmptyMessage.displayName = 'ListEmptyMessage'
