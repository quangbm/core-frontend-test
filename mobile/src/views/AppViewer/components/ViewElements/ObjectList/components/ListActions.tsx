import * as React from 'react'
import { Text } from 'react-native'
import { useRenderElements } from '../../../../hooks/useRenderElements'

type ListActionsProps = {
  elementId: string
}

export const ListActions = React.memo<ListActionsProps>(function Node({ elementId }) {
  const _actions = useRenderElements(elementId)
  return <Text>List Actions</Text>
})

ListActions.displayName = 'ListActions'
