import * as React from 'react'
import { Text, View } from 'react-native'
import { borderRadius, colors, spacings } from '../../../../../../components/GlobalStyles'
import { NIcons } from '../../../../../../components/NIcons'
import { HStack, VStack } from '../../../../../../components/NStacks'
import { NTextInput } from '../../../../../../components/NTextInput'

type ListHeaderProps = {
  children?: React.ReactNode
  isSearchable?: boolean
  searchValue?: string
  onChangeSearch?(keyword: string): void
  title?: string
  description?: string
}

export const ListHeader = React.memo<ListHeaderProps>(function Node({
  title,
  description,
  isSearchable,
  searchValue,
  onChangeSearch,
  children,
}) {
  return (
    <HStack spacing={spacings.sm}>
      <VStack style={{ paddingHorizontal: spacings.sm, paddingTop: spacings.sm }} spacing={spacings.sm}>
        <View style={{ flex: 1, minHeight: 38 }}>
          {Boolean(title) && <Text style={{ fontSize: 16, fontWeight: 'bold' }}>{title}</Text>}
          {Boolean(description) && <Text style={{ fontSize: 12, color: colors.Neutral400 }}>{description}</Text>}
        </View>
        <VStack style={{ alignItems: 'center' }}>{children}</VStack>
      </VStack>
      {isSearchable && (
        <View style={{ paddingHorizontal: spacings.sm }}>
          <NTextInput
            left={<NIcons name="search" />}
            value={searchValue}
            onChangeText={onChangeSearch}
            caption={<View />}
            placeholder="Search ..."
            containerStyle={{
              borderWidth: 1,
              borderColor: colors.Neutral400,
              borderRadius: borderRadius.md,
              padding: spacings.xs,
              marginBottom: spacings.none,
            }}
          />
        </View>
      )}
    </HStack>
  )
})

ListHeader.displayName = 'ListHeader'
