import * as React from 'react'
import { ActivityIndicator, Text, useWindowDimensions, View, ViewProps } from 'react-native'
import { borderRadius, colors, spacings } from '../../../../../../components/GlobalStyles'
import { NIfString } from '../../../../../../components/NIfString'
import { VStack } from '../../../../../../components/NStacks'

type ListLoadingProps = {
  children?: React.ReactNode
} & ViewProps

export const ListLoading = React.memo<ListLoadingProps>(function Node({
  children = 'Loading ...',
  style,
  ...viewProps
}) {
  const dimension = useWindowDimensions()
  return (
    <View
      style={[
        { width: dimension.width - spacings.sm * 2, justifyContent: 'center', alignItems: 'center' },
        style,
        {
          position: 'absolute',
          top: 0,
          left: 0,
          bottom: 0,
        },
      ]}
      {...viewProps}>
      <NIfString
        component={
          <VStack
            style={{
              padding: spacings.sm,
              backgroundColor: colors.white,
              borderRadius: borderRadius.md,
              elevation: 1,
            }}>
            <ActivityIndicator size={16} color={colors.Primary700} />
            <Text style={{ color: colors.Primary700 }}>{children}</Text>
          </VStack>
        }>
        {children}
      </NIfString>
    </View>
  )
})

ListLoading.displayName = 'ListLoading'
