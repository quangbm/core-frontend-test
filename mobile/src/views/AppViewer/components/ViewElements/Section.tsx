import * as React from 'react'
import { View } from 'react-native'
import { borderRadius, colors, spacings } from '../../../../components/GlobalStyles'
import { HStack, VStack } from '../../../../components/NStacks'
import { useLayoutScript } from '../../contexts/LayoutScriptContext'
import { useRenderElements } from '../../hooks/useRenderElements'
import { chunk } from '../../utils'
import { ElementProps } from './types'

const MAXIMUM_COLUMNS = 2

type SectionProps = {
  columns: Array<number>
}

export function Section({ elementId }: ElementProps) {
  const { elements } = useLayoutScript()
  const element = elements[elementId] as { children: Array<string>; props: SectionProps }
  const { columns } = element.props
  const children = useRenderElements(elementId)
  return (
    <HStack
      spacing={spacings.sm}
      style={{ padding: spacings.sm, backgroundColor: colors.white, borderRadius: borderRadius.md }}>
      {chunk(children, MAXIMUM_COLUMNS).map((row, rowIndex) => (
        <VStack
          spacing={spacings.sm}
          key={rowIndex}
          style={{
            alignItems: 'flex-start',
          }}>
          {row.map((child, childIndex) => {
            const idx = rowIndex * 2 + childIndex
            return (
              <View key={idx} style={{ flex: columns[idx] }}>
                {child}
              </View>
            )
          })}
        </VStack>
      ))}
    </HStack>
  )
}
