import * as React from 'react'
import { Text } from 'react-native'

type FlowTrigger = {
  elementId: string
  parentId: string
}

export const FlowTrigger = React.memo<FlowTrigger>(function Node({ elementId, parentId }) {
  return <Text>{JSON.stringify({ elementId, parentId }, null, 2)}</Text>
})

FlowTrigger.displayName = 'FlowTrigger'
