import * as React from 'react'
import { Text } from 'react-native'
import { useLayoutScript } from '../../../contexts/LayoutScriptContext'
import { ElementProps } from '../types'
import { FlowTrigger } from './FlowTrigger'
import { ObjectAction } from './ObjectAction'
import { ButtonProps } from './types'

export function Button({ elementId, parentId }: ElementProps) {
  const { elements } = useLayoutScript()
  // Server type is outdated
  // @ts-ignore
  const element = elements[elementId] as { props: ButtonProps }

  if (element.props.type === 'flow' && element.props.flowId) {
    return <FlowTrigger elementId={elementId} parentId={parentId} />
  }

  if (element.props.type === 'object') {
    return <ObjectAction elementId={elementId} parentId={parentId} />
  }

  return <Text>Not supported Button type: {element.props.type}</Text>
}
