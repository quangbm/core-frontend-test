export type ButtonProps = {
  actionId: string
  title: string
  buttonType: string
  buttonSize: string
  inputMap: Record<string, string>
  objName: string
  action: string
  type: 'object' | 'flow'
  displayName: string
  flowId: string
}
