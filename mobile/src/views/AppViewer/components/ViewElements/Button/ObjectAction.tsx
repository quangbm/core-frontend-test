import { RouteProp, useNavigation, useRoute } from '@react-navigation/native'
import * as React from 'react'
import { NButton, NButtonProps } from '../../../../../components/NButton'
import { AppViewerNavigation, AppViewerParamList } from '../../../AppViewerParamList'
import { useLayoutScript } from '../../../contexts/LayoutScriptContext'
import { ButtonProps } from './types'

type ObjectActionProps = {
  children?: React.ReactNode
  elementId: string
  parentId: string
}

export const ObjectAction = React.memo<ObjectActionProps>(function Node({ elementId, parentId }) {
  const navigation = useNavigation<AppViewerNavigation>()
  const route = useRoute<RouteProp<AppViewerParamList, 'RecordLayout'>>()
  const { elements, layoutId } = useLayoutScript()
  // Server type is outdated
  // @ts-ignore
  const element = elements[elementId] as { props: ButtonProps }
  const parent = elements[parentId]

  const { buttonSize, buttonType, displayName, action } = element.props
  const size = buttonSize === 'default' ? 'medium' : 'small'

  const buttonPress = () => {
    if (parent?.component === 'RelatedList') {
      if (action === 'new') {
        navigation.push('RecordMutationLayout', {
          layoutId,
          componentId: elementId,
          recordId: route.params?.recordId,
          relatedField: (parent?.props as { relationField: string }).relationField,
        })
        return
      }
    }
    if (action === 'edit') {
      navigation.push('RecordMutationLayout', {
        layoutId,
        componentId: elementId,
        recordId: route.params?.recordId,
      })
      return
    }
    if (action === 'new') {
      navigation.push('RecordMutationLayout', {
        layoutId,
        componentId: elementId,
        // FIXME: inputs mapping
      })
      return
    }
    console.error('ObjectAction', `Not supported action: ${layoutId} - ${elementId}`)
  }
  return (
    <NButton onPress={buttonPress} type={buttonType as NButtonProps['type']} size={size}>
      {displayName}
    </NButton>
  )
})

ObjectAction.displayName = 'ObjectAction'
