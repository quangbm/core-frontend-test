import * as React from 'react'
import { Text, View } from 'react-native'
import { FieldSchema } from '../../../../../../../../shared/src/api'

type FieldProps = {
  schema: FieldSchema
  value: unknown
}

export const Field = React.memo<FieldProps>(function Node({ schema, value }) {
  if (schema.typeName === 'text' || schema.typeName === 'generated') {
    return (
      <View>
        <Text>{schema.displayName}</Text>
        <Text>{value as string}</Text>
      </View>
    )
  }
  return (
    <View>
      <Text>{schema.displayName}</Text>
      <Text>{JSON.stringify({ value })}</Text>
    </View>
  )
})

Field.displayName = 'Field'
