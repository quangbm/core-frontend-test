import * as React from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { Text } from 'react-native'
import { FieldSchema } from '../../../../../../../../shared/src/api'
import { colors } from '../../../../../../components/GlobalStyles'
import { NCheckbox } from '../../../../../../components/NCheckbox'
import { NTextInput } from '../../../../../../components/NTextInput'

function isEmptyString(raw?: string) {
  return !(raw && raw.trim())
}

type InputProps = {
  schema: FieldSchema
  value: unknown
}

export const Input = React.memo<InputProps>(function Node({ schema, value }) {
  const { control } = useFormContext()
  const { t } = useTranslation()
  const rules = {
    validate(raw?: string) {
      return schema.isRequired && isEmptyString(raw) ? (t('common.error.required') as string) || 'Required!' : true
    },
  }
  /*
    // | 'numeric'
    // | 'text'
    | 'dateTime'
    // | 'boolean'
    | 'pickList'
    | 'json'
    // | 'generated'
    | 'currency'
    | 'externalRelation'
    | 'indirectRelation'
    | 'relation'
  */
  if (schema.typeName === 'generated') {
    return null
  }
  if (schema.typeName === 'currency') {
    return <Text style={{ color: colors.Neutral900 }}>Currency Input</Text>
  }
  if (schema.typeName === 'text' || schema.typeName === 'numeric') {
    const isNumberic = schema.typeName === 'numeric'
    return (
      <Controller
        control={control}
        name={schema.name}
        defaultValue={value || schema.defaultValue || ''}
        render={({ field, fieldState }) => (
          <NTextInput
            label={schema.displayName}
            error={fieldState.error?.message}
            value={field.value}
            keyboardType={isNumberic ? 'numeric' : 'default'}
            required={schema.isRequired}
            onChangeText={v => {
              if (!v) {
                field.onChange('')
                return
              }
              if (isNumberic) {
                field.onChange(Number(v))
                return
              }
              field.onChange(v)
            }}
          />
        )}
        rules={rules}
      />
    )
  }
  if (schema.typeName === 'boolean') {
    return (
      <Controller
        control={control}
        name={schema.name}
        defaultValue={value || schema.defaultValue || false}
        render={({ field, fieldState }) => (
          <NCheckbox
            checked={field.value}
            onChange={field.onChange}
            label={schema.displayName}
            error={fieldState.error?.message}
          />
        )}
        rules={rules}
      />
    )
  }
  return <Text style={{ color: colors.Neutral900 }}>Not supported type: {schema.typeName}</Text>
})

Input.displayName = 'Input'
