import * as React from 'react'
import { Text, View } from 'react-native'
import { DataResponse, FieldSchema, ObjectSchemaResponse } from '../../../../../../../shared/src/api'
import { borderRadius, colors, spacings } from '../../../../../components/GlobalStyles'
import { HStack } from '../../../../../components/NStacks'
import { useLayoutScript } from '../../../contexts/LayoutScriptContext'
import { useRenderElements } from '../../../hooks/useRenderElements'
import { ElementProps } from '../types'
import { Field } from './components/Field'
import { Input } from './components/Input'

type RecordDetailProps = {
  objName: string
  isPrimary: boolean
  numberOfCols: number
  fields: Array<string>
  showFileDropzones: boolean
  actionMetadata: string
  inputMap: Record<string, string>
  fileNames: Array<string>
}

export function RecordDetail({ elementId }: ElementProps) {
  const { elements, content, isLoading, layoutMode } = useLayoutScript()

  // Server type is outdated
  // @ts-ignore
  const element = elements[elementId] as {
    children: Array<string>
    props: RecordDetailProps
    schema: ObjectSchemaResponse
  }

  const { fields } = React.useMemo(() => {
    const transformedSchema = {} as Record<string, FieldSchema>
    for (const schema of element.schema?.fields || []) {
      transformedSchema[schema.name] = schema
    }
    const transformedFields = [] as Array<{
      schema: FieldSchema
      value: unknown
    }>
    for (const fieldName of element.props.fields) {
      const fieldSchema = transformedSchema[fieldName]
      transformedFields.push({
        schema: fieldSchema,
        value: content ? content[fieldName as keyof DataResponse] : fieldSchema.defaultValue,
      })
    }
    return {
      fields: transformedFields,
    }
  }, [content, element.schema.fields, element.props.fields])

  const actions = useRenderElements(elementId)

  if (isLoading) {
    return (
      <View>
        <Text>Loading ...</Text>
      </View>
    )
  }

  return (
    <React.Fragment>
      {layoutMode !== 'form' && actions}
      <HStack style={{ backgroundColor: colors.white, padding: spacings.sm, borderRadius: borderRadius.md }}>
        {fields.map(field => {
          if (layoutMode === 'form') {
            if (field.schema.typeName === 'generated') {
              return null
            }
            return <Input schema={field.schema} value={field.value} key={field.schema.name} />
          }
          return <Field schema={field.schema} value={field.value} key={field.schema.name} />
        })}
      </HStack>
    </React.Fragment>
  )
}
