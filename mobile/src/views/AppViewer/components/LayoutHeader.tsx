import { useNavigation } from '@react-navigation/native'
import * as React from 'react'
import { Text, View } from 'react-native'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import { boxShadows, colors, spacings } from '../../../components/GlobalStyles'
import { NAvatar } from '../../../components/NAvatar'
import { NButton } from '../../../components/NButton'
import { NIcons } from '../../../components/NIcons'
import { NIfString } from '../../../components/NIfString'
import { VStack } from '../../../components/NStacks'
import { AppViewerNavigation } from '../AppViewerParamList'

type LayoutHeaderProps = {
  children?: React.ReactNode
  left?: React.ReactNode
  right?: React.ReactNode
  title?: React.ReactNode
}

export const LayoutHeader = React.memo<LayoutHeaderProps>(function Node({ left, right, title, children }) {
  const { top } = useSafeAreaInsets()
  const navigation = useNavigation<AppViewerNavigation>()
  return (
    <View
      style={[
        {
          paddingTop: top,
          backgroundColor: colors.white,
        },
        boxShadows.md,
      ]}>
      <VStack style={{ padding: spacings.sm }}>
        {left ?? (
          <NButton
            type="clear"
            onPress={() => {
              if (navigation.canGoBack()) {
                navigation.goBack()
                return
              }
              navigation.push('AppListing')
            }}>
            <NIcons name="list" size={20} />
          </NButton>
        )}
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
          <NIfString
            component={
              <React.Fragment>
                <NAvatar name={title as string} />
                <View style={{ width: 8 }} />
                <Text>{title}</Text>
              </React.Fragment>
            }>
            {title}
          </NIfString>
        </View>
        {right ?? (
          <NButton type="clear">
            <NIcons name="settings" />
          </NButton>
        )}
      </VStack>
      {children}
    </View>
  )
})

LayoutHeader.displayName = 'LayoutHeader'
