import { NativeStackNavigationProp } from 'react-native-screens/native-stack'
import { AppLayoutParams } from './views/AppLayout'
import { AppListingParams } from './views/AppListing'
import { RecordLayoutParams } from './views/RecordLayout'
import { RecordMutationLayoutParams } from './views/RecordMutationLayout'

export type AppViewerParamList = {} & AppLayoutParams &
  RecordLayoutParams &
  RecordMutationLayoutParams &
  AppListingParams

export type AppViewerNavigation = NativeStackNavigationProp<AppViewerParamList>
