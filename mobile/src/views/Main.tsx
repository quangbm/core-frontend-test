import * as React from 'react'
import { ActivityIndicator, View } from 'react-native'
import { colors } from '../components/GlobalStyles'
import { useAccessToken } from '../hooks/useAuth'
import { useSession } from '../hooks/useSession'
import { AuthStack } from './Auth/AuthStack'
import { PrivateStack } from './PrivateStack'

export function Main() {
  const { data: accessToken, isFetching: isLoading } = useAccessToken()
  const [{ isLoading: isSessionLoading }] = useSession({ enabled: Boolean(accessToken) })
  if (isLoading || isSessionLoading) {
    return (
      <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator color={colors.Primary700} />
      </View>
    )
  }
  if (!accessToken) {
    return <AuthStack />
  }
  return <PrivateStack />
}
