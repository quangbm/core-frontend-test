import * as React from 'react'
import { createNativeStackNavigator } from 'react-native-screens/native-stack'
import { ChangeLanguage } from './ChangeLanguage'
import { Settings } from './Settings'
import { SettingsParamList } from './SettingsParamList'

const { Navigator, Screen } = createNativeStackNavigator<SettingsParamList>()

export function SettingsStack() {
  return (
    <Navigator screenOptions={{ headerShown: false }}>
      <Screen {...Settings} />
      <Screen name="ChangeLanguage" component={ChangeLanguage} />
    </Navigator>
  )
}

SettingsStack.displayName = 'SettingsStack'
