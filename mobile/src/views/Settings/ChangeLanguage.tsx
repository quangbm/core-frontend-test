import * as React from 'react'
import { useTranslation } from 'react-i18next'
import { Text, TouchableOpacity, View } from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
import { colors } from '../../components/GlobalStyles'
import { NListItem } from '../../components/NListItem'

export function ChangeLanguage() {
  const { i18n } = useTranslation()
  return (
    <View>
      <TouchableOpacity onPress={() => i18n.changeLanguage('en')}>
        <NListItem right={i18n.language === 'en' && <Icon name="check" color={colors.Primary700} />}>
          <Text>English</Text>
        </NListItem>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => i18n.changeLanguage('vi')}>
        <NListItem right={i18n.language === 'vi' && <Icon name="check" color={colors.Primary700} />}>
          <Text>Tiếng Việt</Text>
        </NListItem>
      </TouchableOpacity>
    </View>
  )
}

ChangeLanguage.displayName = 'ChangeLanguage'
