import { SettingsParams } from './Settings'

export type SettingsParamList = {
  ChangeLanguage: undefined
} & SettingsParams
