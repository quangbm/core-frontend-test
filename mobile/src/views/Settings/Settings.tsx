import { RouteProp } from '@react-navigation/native'
import * as React from 'react'
import { Text, View } from 'react-native'
import { NativeStackNavigationProp } from 'react-native-screens/native-stack'
import { NButton } from '../../components/NButton'
import { useSignOut } from '../../hooks/useAuth'
import { SettingsParamList } from './SettingsParamList'

export const SettingsName = 'Settings' as const

export type SettingsParams = {
  [SettingsName]?: {}
}

type SettingsProps = {
  children?: React.ReactNode
  route: RouteProp<SettingsParamList, 'Settings'>
  navigation: NativeStackNavigationProp<SettingsParamList, 'Settings'>
}

function SettingsScreen({ navigation }: SettingsProps) {
  const { mutate } = useSignOut()
  return (
    <View style={{ padding: 16 }}>
      <NButton onPress={() => navigation.push('ChangeLanguage')}>
        <Text>Change language</Text>
      </NButton>
      <NButton type="danger" onPress={() => mutate()}>
        <Text>Log out</Text>
      </NButton>
    </View>
  )
}

export const Settings = {
  name: SettingsName,
  component: SettingsScreen,
}
