import { NavigationContainer } from '@react-navigation/native'
import * as React from 'react'
import { LogBox, StatusBar } from 'react-native'
import 'react-native-gesture-handler'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { enableScreens } from 'react-native-screens'
import { QueryClientProvider, setLogger } from 'react-query'
import { NKeyboardAvoidingView } from './components/NKeyboardAvoidingView'
import { API_HOST } from './env'
import './services/i18n'
import { queryClient } from './services/react-query'
import './utils/polyfills'
import { Main } from './views/Main'

// import { getBuildNumber, getBundleId, getVersion } from 'react-native-device-info'

// TODO: Init sentry
// install sentry
// init env for DSN
// Sentry.init({
//   dsn: SENTRY_DSN,
//   enabled: !__DEV__,
//   release: `${getBundleId()}:${getVersion()}:${getBuildNumber()}`,
//   dist: getBuildNumber(),
//   environment: __DEV__ ? 'Development' : 'Production',
//   enableNative: !__DEV__,
// })

setLogger({
  ...console,
  error: console.log,
})

LogBox.ignoreLogs(['Setting a timer for a long', 'RCTBridge required dispatch_async'])

enableScreens()

// Global set font family for Text/TextInput
// setFontFamily()

export const App = React.memo(function Node() {
  React.useEffect(() => {
    fetch(`${API_HOST}/v1/_ping`)
      .then(async res => ({
        status: res.status,
        data: await res.text(),
      }))
      .then(res => console.info('CHECK_SERVER', res))
  }, [])

  return (
    <NKeyboardAvoidingView>
      <StatusBar backgroundColor="transparent" translucent barStyle="dark-content" />
      <SafeAreaProvider>
        <QueryClientProvider client={queryClient}>
          <NavigationContainer>
            <Main />
          </NavigationContainer>
        </QueryClientProvider>
      </SafeAreaProvider>
    </NKeyboardAvoidingView>
  )
})

App.displayName = 'App'
