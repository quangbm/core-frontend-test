// eslint-disable-next-line @typescript-eslint/no-var-requires
const based = require('../.eslintrc')

module.exports = {
  ...based,
  extends: [...based.extends, '@react-native-community', 'plugin:react-hooks/recommended'],
  rules: {
    ...based.rules,

    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',

    'react-native/no-inline-styles': 'off',
  },
}
